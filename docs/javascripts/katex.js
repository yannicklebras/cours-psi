document$.subscribe(({ body }) => { 


  renderMathInElement(body, {
    delimiters: [
      { left: "$$",  right: "$$",  display: true },
      { left: "$",   right: "$",   display: false },
      { left: "\\(", right: "\\)", display: false },
      { left: "\\[", right: "\\]", display: true }
    ],
   macros: {"\\R": "\\mathbb{R}","\\C": "\\mathbb{C}","\\N": "\\mathbb{N}",
	    "\\tr": "\\mathrm{tr}","\\rg": "\\mathrm{rg}","\\K":"\\mathbb{K}",
	    "\\cl": "\\mathcal{L}","\\Ker":"\\mathrm{ker}","\\Im":"\\mathrm{im}",
	    "\\im":"\\mathrm{im}","\\E":"\\mathbb{E}","\\d":"\\mathrm{d}",
	    "\\id":"\\mathrm{id}","\\U":"\\mathbb{U}","\\cm":"\\mathcal{M}",
	    "\\co":"\\mathcal{O}","\\P":"\\mathbb{P}","\\ds":"\\displaystyle",
	    "\\sign":"\\mathrm{sign}","\\dfrac":"\\displaystyle\\frac","\\Sp":"\\mathrm{Sp}",
	    "\\diag":"\\mathrm{diag}"},
    displayMode:true,
  })
})
