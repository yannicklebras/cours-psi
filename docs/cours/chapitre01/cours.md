<style>
div.arithmatex {
	margin-top:-3em;
	margin-bottom:-3em;
}
.preuve:after{
	margin-bottom:-1em
}

.md-typeset .tabbed-content {
	margin-bottom:-1em
}
</style>

# Limites de suites et séries de fonctions
Dans ce chapitre on s'intéresse à des *suites de fonctions*, c'est-à-dire des familles de fonctions indicées sur $\ds \mathbb N$. Toutes les fonctions de ce chapitre sont définies sur un même intervalle $\ds I$. Une suite de fonctions peut avoir une limite : nous allons devoir donner un sens à cette notion de limite. Et une fois une *limite* déterminée, on peut se poser des questions naturelles : les propriétés des fonctions $\ds f_n$ de la suite se transfèrent-elles à leur limite. C'est à dire :

- Si toutes les $\ds f_n$ sont continues, $\ds f$ est-elle continue ?
- Si toutes les $\ds f_n$ sont dérivables/$\mathcal C^1$, $\ds f$ l'est-elle aussi ? Sa dérivée est-elle la limite des dérivées ?
- Si on peut intégrer les $\ds f_n$, peut-on intégrer $\ds f$ ? Quelle est la relation entre les intégrales des $\ds f_n$ et l'intégrale de $\ds f$ ?
- D'autres propriétés passent-elles à la limite (convexité, limite, caractère $\ds \mathcal C^k\dots$) ?

## Suites de fonctions

Afin de ne pas répéter à chaque fois, on considère dans cette partie (et la suite si ce n'est pas précisé) un intervalle $\ds I$ de $\ds \mathbb R$.

### Modes de convergence

#### Convergence simple

Le premier mode de convergence que nous découvrons est la *convergence simple*.

!!! definition "Définition : convergence simple"
 	Soit $\ds (f_n)_{n\in\mathbb N}$ une suite de fonctions définies sur $\ds I$. On dit que *$(f_n)$ converge simplement sur $\ds I$ vers $\ds f$* si (et seulement si) $\ds f$ est une fonction définie sur $\ds I$ et pour tout $\ds x\in I$, $\ds f_n(x)\xrightarrow[n\to+\infty]{} f(x)$. On notera souvent $\ds f_n\xrightarrow[n\to +\infty]{C.S.}f$.

La convergence simple est donc une convergence point à point.

!!! exemple
	=== "Énoncé"
		Pour $\ds n\in\mathbb N$, on pose $\ds f_n(t)=t^n$ définie sur $\ds [0,1]$. La suite $\ds (f_n)$ est donc une suite de fonctions définies sur $\ds [0,1]$. Étudier la convergence simple de cette suite de fonctions.  
	=== "Corrigé"
		Soit $\ds x\in[0,1[$, alors $\ds f_n(x)=x^n\xrightarrow[n\to+\infty]{}0$ et $\ds f_n(1)=1\xrightarrow[n\to+\infty]{}1$. La suite de fonctions $\ds (f_n)$ converge donc simplement vers la fonction $\ds f$ définie sur $\ds [0,1]$ par : $\ds f(x)=\begin{cases}0&\text{si }x<1\\1&\text{sinon}\end{cases}$.


On remarque au passage que cette notion de convergence ne suffit pas à assurer le transfert de la propriété de continuité.

!!! exemple
 	=== "Énoncé"
		Considérons les fonctions "triangles" $\ds f_n$ définies de la façon suivante pour $\ds n\in\mathbb N\setminus\{0,1\}$ : $\ds f(0)=0$, $\ds f(\frac 1n)=n$, $\ds f(x)=0$ si $\ds x\ge \frac 2n$ et $\ds f$ est affine entre $\ds 0$ et $\ds \frac 1n$ et entre $\ds \frac 1n$ et $\ds \frac 2n$. Étudier la convergence simple de la suite de fonctions $\ds (f_n)$. On appelle $\ds f$ l'éventuelle limite. Calculer $\ds \lim_{n\to +\infty} \int_0^1 f_n(t) \d t$ et $\ds \int_0^1 f(t)\d t$.
		![fonction triangle](images/fonctionTriangle.png)
	=== "Corrigé"
		Soit $\ds x\in]0,1]$, il existe obligatoirement un entier $\ds n_0$ tel que $\ds \frac 2{n_0}<x$. Alors pour tout $\ds n>n_0$, $\ds f_n(x)=0$. On en déduit que $\ds f_n(x)\xrightarrow[n\to +\infty]{}0$. De plus $\ds f_n(0)=0$ pour tout $\ds n\ge 2$. On en déduit que $\ds f_n$ converge simplement vers la fonction nulle. Par ailleurs $\ds \int_0^1f_n(t)\d t=1$ mais $\ds \int_0^1 f(t)\d t=0$.

Avec cet exemple, on montre que la convergence simple ne suffit pas à intervertir intégrale et limite.



!!! exemple
	=== "Énoncé"
		Soit $\ds (f_n)_{n\in\mathbb N}$ une suite de fonctions définies sur $\ds I$ toutes strictement positives, qui converge simplement vers une fonction $\ds f$ définie sur $\ds I$. Montrer que $\ds f$ est positive.
	=== "Corrigé"
		Soit $\ds x\in I$ et $\ds n\in\mathbb N$ alors $\ds f_n(x)>0$. Par passage à la limite dans l'inégalité on obtient $\ds f(x)\ge 0$. On en déduit que $\ds f$ est positive (mais pas strictement positive a priori).

!!! exemple
	=== "Énoncé"
		Soit $\ds (f_n)_{n\in\mathbb N}$ une suite de fonctions définies sur $\ds I$ toutes convexes, qui converge simplement vers une fonction $\ds f$ définie sur $\ds I$. Montrer que $\ds f$ est convexe.
	=== "Corrigé"
		Soit $\ds (x,y)\in I^2$ et $\ds \lambda\in[0,1]$. Pour tout entier naturel $\ds n$, par convexité de $\ds f_n$ on a $\ds f_n(\lambda x+(1-\lambda)y)\le \lambda f_n(x)+(1-\lambda)f_n(y)$. Par passage à la limite dans l'inégalité, on obtient $\ds f(\lambda x+(1-\lambda)y)\le \lambda f(x)+(1-\lambda) f(y)$ et donc $\ds f$ est convexe.

Ainsi, certaines propriétés semblent passer à la limite simple, mais pour la continuité, la dérivabilité, l'intégration, nous avons besoin d'un autre mode de convergence.

#### Convergence uniforme

!!! definition "Convergence uniforme"
	Soit $\ds (f_n)$ une suite de fonctions définies sur un intervalle $\ds I$. On dit que la suite $\ds (f_n)$ *converge uniformément* vers une fonction $\ds f$ sur $\ds I$ si (et seulement si) $\ds \sup_I|f_n-f|\xrightarrow[n\to+\infty]{}0$. On notera souvent $\ds f_n\xrightarrow[n\to+\infty]{C.U.}f$.

La notion de convergence uniforme est en quelques sortes une convergence "globale". On l'obtient grâce à une majoration uniforme sur $\ds I$ c'est à dire **indépendante** de la variable $\ds x$.

!!! exemple
	Reprenons un exemple précédent : les fonctions "triangles" convergent simplement vers la fonction nulle. Mais convergent-elle uniformément ? On a $\ds \sup_{[0,1]}|f_n-0|=n\to+\infty$ donc il n'y a pas convergence uniforme.


!!! propriete "Limite uniforme et limite simple"
	Soit $\ds (f_n)_{n\in\mathbb N}$ une suite de fonctions qui converge simplement vers $\ds f$ et uniformément vers $\ds g$. Alors $\ds f=g$.

!!! preuve
 	Soit $\ds x\in I$. Soit $\ds n\in\mathbb N$, on a :

	$$
	\begin{aligned}
	|f(x)-g(x)|&=|f(x)-f_n(x)+f_n(x)-g(x)|\\
	&\le |f(x)-f_n(x)|+|f_n(x)-g(x)|\\
	&\le |f(x)-f_n(x)|+\sup_I|f_n-g|
	\end{aligned}
	$$

	Or $\ds |f(x)-f_n(x)|\xrightarrow[n\to+\infty]{} 0$ et $\ds \sup_I|f_n-g|\to 0$. On en déduit par encadrement que $\ds |f(x)-g(x)|\xrightarrow[n\to+\infty]{}0$ c'est-à-dire $\ds f(x)=g(x)$. Cette égalité étant vraie pour tout $\ds x\in I$, on a bien $\ds f=g$.

Ainsi, la limite uniforme et la limite simple quand elles existent sont les mêmes.  Ainsi, avant de parler limite uniforme, on commence en général par déterminer la limite simple.

!!! remarque
	On a vu (très probablement) en première année que le sup sur les fonctions bornées définissait une norme. On notera parfois $\ds ||f_n-f||_\infty$ à la place de $\ds \sup_I|f_n-f|$.

!!! exemple
	=== "Énoncé"
		On pose, pour $\ds n\in\mathbb N$ et $\ds x\in\mathbb R^+$, $\ds f_n(x)=\frac{n}{1+n(1+x)}$. 	Étudier la convergence simple et uniforme de la suite de fonctions $\ds (f_n)$.
	=== "Corrigé"
		Soit $\ds x$ fixé dans $\ds \mathbb R^+$, on a $\ds \lim_{n\to+\infty}f_n(x)=\lim_{n\to+\infty}\frac{n}{n(1+x)}=\frac1{1+x}$. La suite de fonctions converge donc simplement vers la fonction $\ds f:x\mapsto \frac 1{1+x}$. Étudions la convergence uniforme. Soit $\ds n\in\mathbb N$ et $\ds x\in\mathbb R^+$. alors :

		$$
		\begin{aligned}
		|f_n(x)-f(x)|&=\left|\frac n{1+n(1+x)}-\frac 1{1+x}\right|\\
		&=\left|\frac{n+nx-1-n-nx}{(1+x)(1+n(1+x))}\right|\\
		&=\frac{1}{(1+x)(1+n(1+x))}\\
		&\le \frac 1n
		\end{aligned}
		$$

		On en déduit que pour tout $\ds x\ge 0$, $\ds |f_n(x)-f(x)|\le \frac 1n$, c'est-à-dire $\ds \sup_{\mathbb R^+}|f_n-f|\xrightarrow[n\to +\infty]{}0$ et donc la convergence est uniforme.

!!! exemple
	=== "Énoncé"
		On pose pour tout $\ds n\in\mathbb N$ et $\ds x\in]0,1]$, $\ds f_n(x)=nx^n\ln(x)$ et $\ds f_n(0)=0$. Étudier la convergence simple et la convergence uniforme de la suite de fonctions $\ds (f_n)$.
	=== "Corrigé"
		Soit $\ds x\in]0,1[$, par croissances comparées, $\ds nx^n\xrightarrow[n\to +\infty]{}0$. Donc $\ds f_n(x)\xrightarrow[n\to+\infty]{}0$. De plus $\ds f_n(0)=0$ et $\ds f_n(1)=0$. On en déduit la convergence simple de $\ds f_n$ vers la fonction $\ds f$ nulle.

		Étudions la convergence uniforme de cette suite de fonctions. Pour cela, étudions la dérivée de $\ds f_n-f=f_n$. $\ds f_n$ est dérivable par théorèmes généraux sur $\ds ]0,1]$. On a $\ds f_n'(x)=n^2x^{n-1}\ln(x)+nx^{n-1}=nx^{n-1}(n\ln(x)+1)$ qui s'annule en $\ds x_n=e^{-\frac 1n}$. L'étude des variations de $\ds f_n$ montre que $\ds x_n$ est un point de maximum de $\ds |f_n|$. De plus $\ds f_n(e^{-\frac 1n})=ne^{-1}\ln(e^{-\frac 1n})=-e^{-1}$ et on en déduit que $\ds \sup_{[0,1]}|f_n-f|=e^{-1}\not\to 0$. La convergence n'est donc pas uniforme sur $\ds [0,1]$.

		Cependant, si on considère $\ds a\in[0,1[$, alors il existe $\ds n$ tel que $\ds x_n>a$. À partir de ce rang, $\ds |f_n|$ est croissante sur $\ds [0,a]$. On a donc $\ds |f_n(x)-0|\le na^n|\ln(a)|\to 0$. On en déduit qu'il y a convergence uniforme sur tout segment de la forme $\ds [0,a]$... Mais pas sur $\ds [0,1]$.

On peut donc ne pas avoir convergence uniforme sur tout l'intervalle mais sur *plein* de sous-intervalles. Nous verrons que cela est parfois suffisant pour le transfert de propriétés.

!!! propriete "Condition nécessaire de convergence uniforme"
	Soit $\ds (f_n)_{n\in\mathbb N}$ une suite de fonctions qui converge uniformément vers $\ds f$ sur un intervalle $\ds I$. Soit $\ds (x_n)$ une suite d'éléments de $\ds I$, alors $\ds |f_n(x_n)-f(x_n)|\xrightarrow[n\to+\infty]{}0$. On utilise souvent la contraposée : s'il existe une suite de valeurs de $\ds I$ $\ds (x_n)$ telle que $\ds |f_n(x_n)-f(x_n)|\not\xrightarrow[n\to+\infty]{}0$, alors il n'y a pas convergence uniforme.

!!! preuve
	Soit donc $\ds (f_n)_{n\in\mathbb N}$ une suite de fonctions qui convergent vers $\ds f$ uniformément sur $\ds I$. Soit $\ds (x_n)_{n\in\mathbb N}$ une suite d'éléments de $\ds I$. Soit $\ds n\in\mathbb N$, alors $\ds |f_n(x_n)-f(x_n)|\le \sup_I|f_n-f|\xrightarrow[n\to+\infty]{}0$.

!!! exemple
	=== "Énoncé"
		Pour  $\ds n$  dans  $\ds \mathbb N^*$  et  $\ds x$  dans  $\ds \mathbb R^+$, soit  $\ds u_n(x)=\frac{x}{1+n^2x}$. Étudier la convergence simple et la convergence uniforme de  $\ds (u_n)_{n≥ 1}$  et de  $\ds (u_n')_{n≥ 1}$.
	=== "Corrigé"
		La suite de fonctions $\ds u_n$ converge simplement vers la fonction nulle (car $\ds u_n(0)=0$ et pour $\ds x$ quelconque non nul, $\ds n^2x\xrightarrow[n\to+\infty]{} +\infty$). 	Étudions sa convergence uniforme. Soit $\ds n\in\mathbb N^*$ et $\ds x\in\mathbb R^+_*$. On a $\ds u_n(x)=\frac{x}{1+n^2x}\le \frac x{n^2x}=\frac 1{n^2}$. On en déduit que $\ds \sup_{\mathbb R^+}|u_n-0|\le \frac 1{n^2}\xrightarrow[n\to+\infty]{}0$ donc $\ds u_n$ converge uniformément vers la fonction nulle.  
		Soit $\ds n\in\mathbb N^*$, $\ds u_n$ est dérivable et $\ds u_n'(x)=\frac{1}{(1+n^2x)^2}$ qui converge évidemment simplement vers $\ds 0$. Mais $\ds u_n(\frac 1{n^2})=\frac 14\not\to 0$ : il n'y a donc pas convergence uniforme.

On remarque dans cet exercice que la convergence uniforme d'une suite de fonctions ne se transfère a priori pas à la suite des fonctions dérivées.

#### Liens entre convergence uniforme et convergence simple

Nous avons vu que la convergence simple sur $\ds I$ n'implique pas toujours la convergence uniforme. Cependant :

!!! propriete "C.U.$\Rightarrow$ C.S."
	La convergence uniforme implique la convergence simple.

!!! preuve
	Soit $\ds (f_n)$ une suite de fonctions qui converge uniformément vers $\ds f$ sur un intervalle $\ds I$. Soit $\ds x\in I$. On a $\ds |f_n(x)-f(x)|\le\sup_I|f_n-f|\to 0$ car $\ds (f_n)$ converge uniformément. On en déduit donc par encadrement que $\ds f_n(x)\xrightarrow[n\to +\infty]{}f(x)$ et $\ds (f_n)$ converge donc simplement vers $\ds f$ sur $\ds I$.

Il est assez simple de montrer que la convergence uniforme sur tout segment de $\ds I$ entraîne la convergence simple. Cependant, comme on l'a vu précédemment, il est souvent plus facile de montrer une convergence simple qu'une convergence uniforme.

### Continuité des suites de fonctions

!!! theoreme "Continuité de la limite uniforme"
	Si une suite $\ds (f_n)$ de fonctions continues sur $\ds I$ converge
	uniformément vers $\ds f$ sur $\ds I$, alors $\ds f$ est continue sur $\ds I$.


!!! preuve
	Soit $\ds f_n$ une suite de fonctions définies et continues sur $\ds I$, qui converge
	uniformément vers une fonction $\ds f$ sur $\ds I$.

	Soit $\ds x_0\in I$ et $\ds \varepsilon>0$. $\ds f_n\overset{C.U.}\longrightarrow f$ donc il existe $\ds n_0$ tel que si $\ds n\ge n_0$
	alors $\ds ||f_n-f||_\infty\le \frac\varepsilon3$. Prenons un tel $\ds n_0$.

	Soit $\ds n\ge n_0$ fixé. $\ds f_n$ est continue en $\ds x_0$ donc il existe $\ds \eta>0$ tel que si $\ds |x-x_0|\le\eta$
	alors $\ds |f_n(x)-f_n(x_0)|\le \frac\varepsilon3$. Prenons un tel $\ds \eta$ et $\ds x\in I$ tel que $\ds |x-x_0|\le\eta$. On a :

	$$
	\begin{aligned}
	|f(x)-f(x_0)| &= |f(x)-f_n(x)+f_n(x)-f_n(x_0)+f_n(x_0)-f(x_0)|\\
	&\underset{IT}\le |f(x)-f_n(x)|+|f_n(x)-f_n(x_0)|+|f_n(x_0)-f(x_0)|\\
	&\le ||f-f_n||+|f_n(x)-f_n(x_0)|+||f_n-f||\\
	&\le \frac\varepsilon3+\frac\varepsilon3+\frac\varepsilon3\\
	&\le \varepsilon
	\end{aligned}
	$$

	Ainsi, $\ds f$ est continue en $\ds x_0$. Puisque $\ds x_0$ est quelconque dans $\ds I$ on en déduit que $\ds f$ est continue sur $\ds I$.


!!! exemple
	=== "Énoncé"
		On pose $\ds f_n(x)=\frac{1}{(1+x)^n}$ définies pour $\ds n\in\mathbb N$ sur $\ds [0,1]$. Déterminer la limite simple de la suite de fonctions $\ds f_n$. La convergence est-elle uniforme ?

	=== "Corrigé"
		Tout d'abord, les fonctions $\ds f_n$ sont continues sur $\ds [0,1]$ (par théorèmes généraux). Si $\ds x>0$ alors $\ds \lim_{n\to+\infty}(1+x)^n=+\infty$ et donc $\ds f_n(x)\underset{n\to+\infty}\to 0$. Et si $\ds x=0$, $\ds f_n(0)=1\to 1$. On en déduit que la suite de fonctions $\ds f_n$ converge simplement vers la fonction $\ds f:x\mapsto\begin{cases}1 &\text{si }x=0\\0&\text{si }x\in]0,1]\end{cases}$. Or si $\ds f_n$ converge uniformément, c'est obligatoirement vers cette même limite. Mais le théorème de continuité de la limite uniforme affirme que dans ce cas, $\ds f$ devrait être continue sur $\ds [0,1]$ (ce qui n'est évidemment pas le cas). On en déduit qu'il y a convergence simple, mais pas uniforme.

!!! exemple
	=== "Énoncé"
		On pose $\ds f_n(x)=\frac{x^n}{1+x^n}$ définies sur $\ds \mathbb R^+$. Étudier la convergence simple et uniforme de la suite de fonctions $\ds f_n$.
	=== "Corrigé"
		Tout d'abord, les fonctions $\ds f_n$ sont continues sur $\ds \mathbb R^+$ (par théorèmes généraux). Soit $\ds x>1$, alors $\ds f_n(x)=\frac{x^n}{1+x^n}\underset{n\to+\infty}\to1$. Et si $\ds x<1$, $\ds f_n(x)\to0$. $f_n(1)=\frac 12\to\frac 12$. On en déduit que $\ds f_n$ converge simplement vers la fonction $\ds f:x\mapsto \begin{cases}0&\text{si }x<1\\\frac 12&\text{si }x=1\\1&\text{si }x>1\end{cases}$. Cette fonction n'étant pas continue, par contraposée du théorème de continuité de la limite uniforme, la convergence vers $\ds f$ n'est pas uniforme.

!!! exemple
	=== "Énoncé"
		On pose $\ds f_n(x)=x^n$ définies sur $\ds I=[0,1[$. Étudier la convergence simple et uniforme de la suite de fonctions $\ds f_n$. La limite simple $\ds f$ est-elle continue ?
	=== "Corrigé"
		Tout d'abord, les fonctions $\ds f_n$ sont continues sur $\ds [0,1[$ (par théorèmes généraux). Soit $\ds x\in I$, alors $\ds f_n(x)=x^n\underset{n\to+\infty}\to0$. La suite de fonctions $\ds (f_n)$ converge donc simplement vers la fonction nulle qu'on note $\ds f$. Pour autant, la convergence n'est pas uniforme : en effet, $\ds \sup|f_n-f|=1\not\to 0$. Et pourtant, la limite est bien continue.

La notion de continuité étant une notion locale, on n'a pas besoin de la convergence uniforme sur l'intervalle tout entier pour obtenir la continuité : il suffit d'avoir *localement* convergence uniforme. On utilise donc la version locale :

!!! theoreme "Continuité de la limite uniforme locale"
	Si une suite $\ds (f_n)$ de fonctions continues sur $\ds I$ converge
	uniformément vers $\ds f$ sur tout segment $\ds [a,b]$ de $\ds I$, alors $\ds f$ est continue sur $\ds I$.

Il n'y a pas besoin de preuve, cela tient uniquement à la localité de la notion de continuité.

!!! exemple
	Considérons la suite de fonctions définie par $\ds f_n(x)=x^n$ sur $\ds [0,1[$. Nous avons vu qu'elle converge simplement vers la fonction nulle, mais pas uniformément. Cependant, si $\ds [a,b]$ est un segment de $\ds [0,1[$ alors $\ds |f_n(x)-0|=|x^n|\le b^n$. En passant au sup à gauche, on a $\ds ||f_n-0||_\infty\le b^n\to 0$. On en déduit que $\ds f_n$ converge uniformément vers la fonction nulle sur tout segment de $\ds [0,1[$.  




### Intégration sur un segment des suites de fonctions

!!! theoreme "Intégration sur un segment de la limite uniforme"
	Si une suite $\ds (f_n)$ de fonctions continues converge uniformément vers
	$f$ sur $\ds [a,b]$ alors :  
	$$
	\int_a^b f_n(t) \,\d t \xrightarrow[n \to +\infty]{}
	\int_a^b f(t) \,\d t
	.$$

!!! preuve
	Soit $\ds (f_n)$ une suite de fonctions continues sur $\ds [a,b]$ qui converge uniformément vers une fonction $\ds f$. Alors d'après le théorème de continuité de la limite uniforme, $\ds f$ est continue sur $\ds [a,b]$. L'intégrale $\ds \int_a^b f(t)dt$ est ainsi bien définie. On a alors :

	$$
	\begin{aligned}
	\left|\int_a^b f_n(t)\d t-\int_a^b f(t)\d t\right|&=\left|\int_a^b f_n(t)-f(t)\d t\right|\\
	&\le \int_a^b |f_n(t)-f(t)|\d t\\
	&\le\int_a^b ||f_n-f||_\infty \d t\\
	&\le(b-a)||f_n-f||_\infty
	\end{aligned}
	$$

	Or $\ds ||f_n-f||_\infty\to 0$ donc $\ds \int_a^b f_n(t)\d t\to\int_a^b f(t)\d t$.

On peut donc écrire, sous réserve de convergence uniforme :

$$
\lim_{n\to+\infty}\int_a^b f_n(t)\d t=\int_a^b\lim_{n\to +\infty}f_n(t)\d t
$$

!!! exemple
	=== "Énoncé"
		Montrer que la suite de fonctions $\ds (f_n)$ définies sur $\ds [0,1]$ par $\ds f_n(x)=n^2x^n(1-x)$ ne converge pas uniformément.
	=== "Corrigé"
		La suite de fonctions $\ds f_n$ converge simplement vers $\ds 0$ par croissances comparées.

		Or $\ds \int_0^1 f_n(x)\d x=n^2\left(\frac 1{n+1}-\frac 1{n+2}\right)=n^2\frac 1{(n+1)(n+2)}\to 1$. On a donc $\ds \displaystyle\lim_{n\to+\infty}\int_0^{1}f_n(x)\d x\ne\int_0^1 \lim_{n\to+\infty}f_n(x)\d x$.


!!! exemple
	=== "Énoncé"
		On pose pour $\ds n\in\mathbb N^*$ et $\ds x\in]0,1]$, $\ds f_n(x)=\frac{x^n\ln(x)}{1+x}$ et $\ds f_n(0)=0$. Montrer que $\ds f_n$ est continue en $\ds 0$. Calculer la limite de $\ds I_n=\int_0^1 f_n(t)\d t$.
	=== "Corrigé"
		On montre facilement par croissances comparées que le prolongement proposé est bien continu. On peut alors intégrer les $\ds f_n$. Cette suite de fonctions converge simplement vers la fonction nulle. Montrons que cette convergence est uniforme. On remarque que $\ds |f_n(x)|\le x^n|\ln(x)|$. Posons $\ds u_n(x)=-x^n\ln(x)$ : $\ds u_n'(x)=-nx^{n-1}\ln(x)-x^{n-1}=-x^{n-1}(1+n\ln(x))$ qui s'annule en $\ds x_n=e^{-\frac 1n}$. On a alors $\ds u_n(x_n)\le -(e^{-\frac 1n})^n \ln(e^{-\frac 1n})=\frac {e^{-1}}{n}$. On en déduit que $\ds |f_n(x)|\le \frac{e^{-1}}{n}$ et donc $\ds (f_n)$ converge uniformément vers $\ds 0$.


### Dérivabilité des suites de fonctions

!!! exemple
	=== "Énoncé"
		On définit une suite $\ds (f_n)$ de fonctions sur $\ds \mathbb R$ par $\ds f_n(x)=\sqrt{x^2+\frac 1n}$. Montrer que les $\ds f_n$ sont de classe $\ds \mathcal C^1$ sur $\ds \mathbb R$ et que la suite de fonctions $\ds (f_n)$ converge uniformément sur $\ds \mathbb R$ vers une fonction $\ds f$ à préciser. Cette fonction est-elle aussi de classe $\ds \mathcal C^1$ ?
	=== "Corrigé"
		La convergence simple se fait vers la fonction $\ds f:x\mapsto\sqrt{x^2}=|x|$. Montrons que la convergence est uniforme. Soit $\ds x\in\mathbb R$, $\ds \big|f_n(x)-|x|\big|=\frac{x^2+\frac 1{n}-x^2}{\sqrt{x^2+\frac 1{n}}+\sqrt{x^2}}\le \frac{\frac 1n}{\frac1{\sqrt n}}=\frac 1{\sqrt n}$. On en déduit la convergence uniforme. Cependant, $\ds x\mapsto |x|$ n'est pas $\ds \mathcal C^1$ sur $\ds \mathbb R$ (elle n'est pas dérivable en $\ds 0$.)

Cet exemple montre que la convergence uniforme ne suffit pas à transférer toutes les régularités. Il faut ajuster les contraintes pour prendre en compte la dérivée.

!!! theoreme "dérivabilité de la limite"
	Si une suite $\ds (f_n)$ de fonctions de classe $\ds \mathcal{C}^1$ sur un intervalle
	$I$ converge simplement sur $\ds I$ vers une fonction $\ds f$, et si la suite
	$(f'_n)$ converge uniformément sur $I$ vers une fonction $\ds g$, alors $\ds f$ est de
	classe $\ds \mathcal{C}^1$ sur $\ds I$ et $\ds f'=g$.

!!! preuve
	Soit $\ds (f_n)$ une suite de fonctions de classe $\ds \mathcal C^1$ sur un intervalle $\ds I$. Supposons que $\ds f_n\xrightarrow[n\to+\infty]{C.S.}f$ et que $\ds f_n'\xrightarrow[n\to+\infty]{C.U.}g$. D'après le théorème de continuité de la limite uniforme, on peut affirmer que $\ds g$ est continue. Soit $\ds (a,x)\in I^2$, d'après le théorème d'intégration de la limite uniforme, $\ds \lim_{n\to+\infty}\int_a^x f_n'(t)\d t=\int_a^x g(t)\d t$. Or $\ds \int_a^x f_n'(t)\d t=f_n(x)-f_n(a)$. On a donc $\ds f(x)=\int_a^x g(t)\d t+f(a)$ : on en déduit par théorèmes généraux que $\ds f$ est dérivable, de dérivée $\ds f'(x)=g(x)$.


!!! propriete "Convergence uniforme de la limite simple"
	Dans le théorème précédent, une conclusion supplémentaire est :  

	- $\ds (f_n)$ converge uniformément vers $\ds f$ sur tout segment de $\ds I$

!!! preuve
	On se place donc dans le cadre du théorème précédent. On pose $\ds g_n=f_n-f$. On a alors pour $\ds x\in[a,b]\subset I$ :

	$$
	\begin{aligned}
	|g_n(x)| &= |g_n(x)-g_n(a)+g_n(a)|\\
	&\le |g_n(x)-g_n(a)|+|g_n(a)|\\
	&\le \sup_{[a,b]}|g_n'||x-a|+|g_n(a)|\\
	&\le \sup_{[a,b]}|f_n'-f'|(b-a)+|f_n(a)-f(a)|
	\end{aligned}
	$$

	La majoration est donc uniforme (indépendante de $\ds x$) et le majorant convege vers $\ds 0$ (par convergence uniforme de la dérivée pour $\ds \sup_{[a,b]}|f_n'-f'|$ et par convergence simple pour $\ds |f_n(a)-f(a)|$). Ainsi, on a bien convergence uniforme de $\ds (f_n)$ vers $\ds f$ sur tout segment de $\ds I$.

On peut généraliser ce théorème aux suites de fonctions de classe $\ds \mathcal C^k$ :

!!! theoreme "Classe $\ds \mathcal C^k$ de la limite"
	Si une suite $\ds (f_n)$ de fonctions de classe $\ds \mathcal{C}^k$ sur un intervalle
	$I$ est telle que :  

	- pour tout $\ds 0\le j<k$, $\ds (f_n^{(j)})$ converge simplement vers une fonction $\ds \varphi_j$  
	- $\ds (f_n^{(k)})$ converge uniformément vers une application $\ds g$  
	Alors  
	- $\ds \varphi_0$ notée $\ds f$ est de classe $\ds \mathcal C^k$  
	- $\ds f^{(k)}=g$  
	- pour tout $\ds 0\le j<k$, $\ds \varphi_j=f^{(j)}$.

!!! preuve
	La preuve se fait simplement par récurrence à partir du théorème précédent.

En pratique, comme pour la continuité, on peut se contenter de la convergence uniforme sur tout sous-intervalle (ou sous-segment) de $\ds I$.




## Séries de fonctions

On s'intéresse maintenant à des séries de fonctions : on considère toujours une suite de fonctions $\ds (f_n)$ définies sur un intervalle $\ds I$, mais ce sont les régularités de $\ds \sum f_n$ qui nous intéressent.

### Modes de Convergence
Comme dans le cas des suites de fonctions, il existe pour les séries de fonctions différents modes de convergence.

!!! definition "convergence simple/absolue d'une série de fonctions"
	Soit $\ds (f_n)$ une suite de fonctions définies sur un  intervalle $\ds I$ de $\ds \mathbb R$. On dit que $\ds \sum f_n$ converge simplement vers $\ds f$ sur $\ds I$ si (et seulement si) pour tout $\ds x$ de $\ds I$, $\ds \sum_{n=0}^N f_n(x)\xrightarrow[N\to+\infty]{}f(x)$. On notera $\ds \sum_{n=0}^N f_n\xrightarrow[n\to+\infty]{C.S.}f$ ou $\ds \sum_{n=0}^{+\infty}f_n(x)=f(x)$.
	On dit que $\ds \sum f_n$ converge absolument sur $\ds I$ si $\ds \sum |f_n|$ converge simplement. Evidemment la convergence absolue implique la convergence simple.

!!! exemple
 	=== "Énoncé"
		On pose, pour $\ds x\in\mathbb R^+$, $\ds f_n(x)=\frac{x^n}{n!}$. Étudier la convergence simple de la série de fonctions $\ds \sum f_n$.
	=== "Corrigé"
		Soit $\ds x\in\mathbb R^+$, $\ds n^2f_n(x)=\frac{n^2x^{n}}{n!}\xrightarrow[n\to+\infty]{}0$ par croissances comparées. On en déduit que $\ds \frac{x^n}{n!}=o(\frac 1{n^2})$ donc par théorème de comparaison des séries numériques, $\ds \sum\frac{x^n}{n!}$ converge. On peut même retrouver sa limite grâce à l'inégalité de Taylor Lagrange, puisqu'on reconnaît le développement de l'exponentielle. Soit $\ds N\in\mathbb N$ :

		$$
		\begin{aligned}
		|e^x-\sum_{n=0}^N\frac{x^n}{n!}|&=\left|\int_0^xe^t\frac{(x-t)^n}{n!}\d t\right|\\
		&\le e^x\frac{x^{n+1}}{(n+1)!}\\
		&\xrightarrow[n\to+\infty]{}0
		\end{aligned}
		$$

		Donc $\ds \sum_{n=0}^{+\infty}\frac {x^n}{n!}=e^x$ pour tout $\ds x\in\mathbb R^+$. On a le même résultat pour $\ds x\in\mathbb R^-$ mais la majoration doit être plus détaillée (les bornes ne sont pas dans le bon sens pour la majoration de l'intégrale).

Dans le cas des séries de fonctions, il ne sera pas toujours évident de déterminer la limite. On saura dire s'il y a convergence simple grâce aux théorèmes de comparaison des séries numériques, mais pas forcément calculer la somme.


!!! exemple
	=== "Énoncé"
		Soit, pour  $\ds n\ge 2$  :  $\ds u_n:x\in[0,1]\to\frac{1}{\ln n} x^n(1-x)$ . Étudier la convergence de la série de fonctions de terme général  $\ds u_n$.
	=== "Corrigé"
		Soit $\ds x\in[0,1[$ et soit $\ds n\ge3$. Alors $\ds n^2u_n(x)=n^2\frac 1{\ln n}x^n(1-x)\le n^2x^n\to0$ par croissances comparées. On en déduit que $\ds u_n(x)=o(\frac 1{n^2})$ donc par théorèmes de comparaison, $\ds \sum u_n(x)$ converge. On en déduit la convergence simple de la série de fonctions $\ds \sum u_n$ : par contre, il est a priori impossible de déterminer la limite.

!!! propriete "condition nécessaire de convergence"
	Soit $\ds \sum f_n$ une série de fonctions définies sur un intervalle $\ds I$ de $\ds \mathbb R$ qui converge simplement vers $\ds f$. On note $\ds S_N(x)=\sum_{n=0}^Nf_n(x)$ la somme partielle de la série et $\ds R_N(x)=\sum_{n=N+1}^{+\infty}f_n(x)$. Alors la suite de fonctions $\ds (R_N)_{N\in\mathbb N}$ converge simplement vers $\ds 0$.

!!! preuve
	On a $|R_N(x)|=|f(x)-S_N(x)|\to 0$ donc $R_N$ converge simplement vers $0$. 


!!! definition "convergence uniforme d'une série de fonctions"
	Soit $\ds (f_n)$ une suite de fonctions définies sur un intervalle $\ds I$ de $\ds \mathbb R$. On dit que $\ds \sum f_n$ converge uniformément sur $\ds I$ vers $\ds f$ si (et seulement si) $\ds \sup_I\left|f-\sum_{n=0}^N f_n\right|\xrightarrow[N\to+\infty]{}0$.

Comme dans le cas des suites de fonctions, la limite simple et la limite uniforme sont les mêmes et on a un lien entre les deux convergences :

!!! propriete "limite simple et uniforme"
	Soit $\ds (f_n)$ une suite de fonctions définies sur un intervalle $\ds I$ de $\ds \mathbb R$.   

	- Si $\ds \sum f_n$ converge uniformément vers $\ds f$ sur $\ds I$ alors $\ds \sum f_n$ converge simplement vers $\ds f$ sur $\ds I$ ;  
	- si $\ds \sum f_n$ converge simplement et uniformément sur $\ds I$ alors c'est vers la même limite.

!!! preuve
	Pas besoin de preuve, c'est l'application des propriétés sur les suites de fonctions appliquées aux sommes partielles.



On peut déduire de ce lien entre limite simple et uniforme une caractérisation de la convergence uniforme :

!!! propriete "Caractérisation de la convergence uniforme par le reste"
	Soit $\ds (f_n)$ une suite de fonctions définies sur un intervalle $\ds I$ de $\ds \mathbb R$. $\ds \sum f_n$ converge uniformément sur $\ds I$ si et seulement si  

	- La série de fonctions $\ds \sum f_n$ converge simplement sur $\ds I$ ;  
	- La suite des restes converge uniformément vers la fonction nulle sur $\ds I$.  

!!! preuve
	Supposons que $\ds \sum f_n$ converge uniformément vers une fonction $\ds f$ sur $\ds I$. Soit $\ds x\in I$ et $\ds N\in\mathbb N$, on a $\ds |f(x)-\sum_{n=0}^Nf_n(x)|\le \sup_I|f-\sum_{n=0}^Nf_n|\xrightarrow[N\to+\infty]{C.U.}0$. Donc $\ds \sum_{n=0}^{N}f_n(x)\xrightarrow[N\to+\infty]{}f(x)$ c'est-à-dire que la série de fonctions converge simplement vers $\ds f$. La suite des restes existe donc et par la même majoration, pour tout $\ds x$ de $\ds I$, $\ds |R_N(x)|\le\sup_I|f-\sum_{n=0}^N f_n|$. Cette majoration étant indépendante de $\ds x$, on peut passer au sup à gauche : $\ds \sup_I|R_N|\le \sup_I|f-\sum_{n=0}^N f_n|\xrightarrow[N\to+\infty]{C.U.}0$. Donc la suite des reste converge uniformément vers $\ds 0$.

	Réciproquement, si $\ds \sum f_n$ converge simplement vers une fonction $\ds f$ et si la suite des restes converge uniformément vers $\ds 0$, alors pour tout $\ds N\in\mathbb N$ et tout $\ds x\in I$, $\ds |f(x)-\sum_{n=0}^Nf_n(x)|=|R_N(x)|\le \sup_I|R_N|$. Cette majoration étant uniforme, c'est à dire indépendante de $\ds x$, on peut passer au sup à gauche : $\ds \sup_I|f-\sum_{n=0}^N f_n|\le\sup_I|R_N|\xrightarrow[N\to+\infty]{C.U.}0$. On en déduit la convergence uniforme de la série de fonctions $\ds \sum f_n$.


On n'a donc pas besoin de connaître la limite de la série de fonctions pour pouvoir étudier la convergence uniforme : il suffit de savoir qu'elle converge simplement (grâce aux théorèmes de 1A sur les séries numériques) puis d'étudier la convergence uniforme du reste *vers $\ds 0$*.

!!! exemple
	=== "Énoncé"
		Pour $\ds n\ge 1$ et $\ds x\ge0$, on pose $\ds u_n(x)=(-1)^n\ln\left(1+\frac {x}{n(1+x)}\right)$. Ètudier la convergence simple et uniforme de la série de fonctions $\ds \sum u_n$.   
	=== "Corrigé"
		Soit $\ds x$ fixé positif, on peut affirmer que $\ds \ln\left(1+\frac x{n(x+1)}\right)$ est positif et on peut vérifier qu'elle est décroissante en $\ds n$. D'après le critère spécial des séries alternées, la série $\ds \sum u_n(x)$ (pour $\ds x$ fixé) est convergente. Toujours d'après ce critère, le reste $\ds R_n(x)$ est tel que $\ds |R_n(x)|\le|u_{n+1}(x)|=\ln\left(1+\frac x{(n+1)(x+1)}\right)=\ln\left(1+\frac 1{n+1}-\frac 1{(n+1)(x+1)}\right)\le \ln(1+\frac 1{n+1})\xrightarrow[n\to+\infty]{}0$. On a donc une majoration uniforme de $\ds R_n$ sur $\ds \mathbb R^+$ qui converge vers $\ds 0$ : la convergence est bien uniforme.


!!! exemple
 	=== "Énoncé"
		Pour $\ds n\in\mathbb N$ et $\ds x\in\mathbb R^+$, on pose $\ds f_n(x)=nx^2e^{-x\sqrt n}$. Étudier convergence simple et uniforme de la série $\ds \sum f_n$.
	=== "Corrigé"
		Soit $\ds x$ fixé dans $\ds \mathbb R^+$, par croissances comparées (sauf en $\ds 0$ où elle est nulle), $\ds n^3x^2e^{-x\sqrt n}\xrightarrow[n\to+\infty]{}0$ et donc $\ds f_n(x)=o\left(\frac 1{n^2}\right)$. On en déduit que la série $\ds \sum f_n(x)$ est convergente, toujours pour tout $\ds x$. On a donc convergence simple de la série.   
		Pour étudier la convergence uniforme, étudions le reste $\ds R_N$ de cette série. Le terme général de la série est positif. On peut donc écrire, pour $\ds x>0$, que $\ds R_N(x)=\sum_{n=N+1}^{+\infty}u_n(x)>u_{N+1}(x)$. Or l'étude de $\ds u_{N+1}$ montre qu'elle admet un maximum en $\ds \frac 2{\sqrt{N+1}}$ qui vaut $\ds 4e^{-2}$.	On a donc $\ds R_N\left(\frac2{\sqrt{N+1}}\right)=4e^{-2}\not\to 0$. Ainsi $\ds R_N$ ne converge pas uniformément vers $\ds 0$ : la convergence n'est pas uniforme sur $\ds \mathbb R^+$.

!!! definition "Convergence normale d'une série de fonctions"
	Soit $\ds (f_n)$ une suite de fonctions définie sur un intervalle $\ds I$ de $\ds \mathbb R$. On dit que la série $\ds \sum f_n$ converge normalement sur $\ds I$ si (et seulement si) la série numérique $\ds \sum||f_n||_\infty$ est convergente.

!!! propriete "C.N.$\Rightarrow$ C.U"
	Soit $\ds (f_n)$ une suite de fonctions définie sur un intervalle $\ds I$ de $\ds \mathbb R$. Si $\ds \sum f_n$ converge normalement sur $\ds I$ alors $\ds \sum f_n$ converge uniformément (et donc simplement) sur $\ds I$.

!!! preuve
	Soit donc une série de fonctions qui converge normalement sur $\ds I$. Soit $\ds x\in I$ alors pour tout entier $\ds n$, $\ds |f_n(x)|\le||f_n||_\infty$. Par théorème de comparaison et parce que $\ds \sum f_n$ est normalement convergente, on peut assurer que $\ds \sum |f_n|$ converge et donc $\ds \sum f_n$ converge simplement. On peut alors étudier le reste de la série : soit $\ds x\in I$ et $\ds N\in\mathbb N$, $\ds |R_N(x)|=\left|\sum_{n=N+1}^{+\infty}f_n(x)\right|\le\sum_{n=N+1}^{+\infty}|f_n(x)|\le\sum_{n=N+1}^{+\infty}||f_n||_\infty\xrightarrow[n\to +\infty]{}0$ par convergence normale. On a donc trouvé une majoration uniforme du reste qui converge vers $\ds 0$ : la suite de fonctions des restes converge donc uniformément vers $\ds 0$ c'est-à-dire $\ds \sum f_n$ converge uniformément.

La convergence normale est simple à montrer : il suffit de majorer $\ds |f_n(x)|$ par une suite $\ds M_n$ qui est le terme général d'une série convergente ($M_n$ NE DOIT PAS DÉPENDRE DE $\ds x$ !). Le plan d'attaque est donc le suivant :  

- On commence par montrer la convergence **normale**. Si elle est vérifiée, on a aussi convergence **uniforme**, **absolue** et **simple** ;  
- Sinon on cherche la convergence **simple**, ou **absolue** ;  
- S'il y a convergence **simple**, on s'intéresse à la convergence **uniforme**, souvent par l'étude des restes.

!!! exemple
	=== "Énoncé"
		Soit $\ds (f_n)$ une suite de fonctions définies sur $\ds \mathbb R$ par $\ds f_n(x)=\frac{\sin(nx^2)}{n^2}$. Montrer que la série de fonctions $\ds \sum f_n$ converge normalement sur $\ds \mathbb R$.
	=== "Corrigé"
		Soit $\ds n\in\mathbb N^*$ et $\ds x\in\mathbb R$, alors $\ds |f_n(x)|=\frac{|\sin(nx^2)|}{n^2}\le \frac 1{n^2}$. Cette majoration étant uniforme (indépendante de $\ds \mathbb R)$, on peut affirmer que $\ds ||f_n||_\infty\le\frac 1{n^2}$ et par théorème de comparaison des séries à termes positifs, $\ds \frac 1{n^2}$ étant le terme général d'une série de Riemann convergente, $\ds \sum||f_n||_\infty$ est convergente : ainsi $\ds \sum f_n$ est normalement convergente.   


!!! exemple
	=== "Énoncé"
		Soit, pour  $\ds n\in\mathbb N$,  $\ds f_n:x\in\mathbb R^+\mapsto\frac{x^n}{1+x^n}$. Étudier la convergence simple de la série de fonctions de terme général  $\ds f_n$. Sur quels intervalles y a-t-il convergence normale ?
	=== "Corrigé"
		On remarque que pour $\ds n\ge 1$, $\ds f_n(0)=0$ et la série des $\ds f_n(0)$ converge. Si $\ds 0<x<1$, alors $\ds f_n(x)\sim x^n$ et la série converge par comparaison à une série à termes positifs géométrique convergente. Si $\ds x=1$, $\ds f_n(1)=\frac 12$ et la série des $\ds f_n(x)$ est grossièrement divergente. Enfin si $\ds x>1$, alors $\ds f_n(x)\xrightarrow[n\to +\infty]{}1$ et la série des $\ds f_n(x)$ diverge grossièrement. On en déduit que la série de fonction converge simplement sur $\ds [0,1[$.  
		Étudions la convergence normale. Pour cela, dérivons $\ds f_n(x)$ : $\ds f_n'(x)=\frac{nx^{n-1}(1+x^n)-nx^{2n-1})}{(1+x^n)^2}=\frac{nx^{n-1}}{(1+x^n)^2}$. Ainsi $\ds f_n$ est croissante sur $\ds [0,1[$. On ne peut donc majorer $\ds f_n$ sur $\ds [0,1[$ que par la limite en $\ds 1$ c'est à dire $\ds \frac 12$ qui n'est pas du tout le terme général d'une série convergente. Soit $\ds a\in[0,1[$, soit $\ds x\in[0,a]$, on a donc $\ds f_n(x)\le f_n(a)=\frac {a^n}{1+a^n}\le a^n$. Or $\ds a^n$ est le terme général d'une série géométrique convergente. On a donc bien convergence normale sur $\ds [0,a]$. On a ainsi convergence normale sur tout $\ds [0,a]$ avec $\ds a\in[0,1[$ mais pas sur $\ds [0,1[$.



### Régularité des séries de fonctions

!!! theoreme "Continuité de la somme d'une série de fonctions"
	Si une série $\ds \sum f_n$ de fonctions continues sur $\ds I$ converge uniformément sur $\ds I$, alors sa somme est continue sur $\ds I$.

!!! preuve
	Il suffit d'appliquer le théorème de continuité des suites de fonctions à la somme partielle $\ds \sum_{n=0}^N f_n$.

!!! theoreme "Théorème de la double limite"
	Si une série $\ds \sum f_n$ de fonctions définies sur $\ds I$ converge uniformément sur $\ds I$ et si, pour tout $\ds n$, $\ds f_n$ admet une limite $\ds \ell_n$ en  $\ds a$ borne de $\ds I$ (éventuellement infinie), alors la série $\ds \sum \ell_n$ converge, la somme de la série admet une limite en $\ds a$ et :    

	$$
	\sum_{n=0}^{+\infty} f_n(x) \xrightarrow[x\to a]{} \sum_{n=0}^{+\infty} \ell_n.
	$$

!!! preuve
	Hors programme


!!! exemple
	=== "Énoncé"
		Soit, pour  $\ds n\in \mathbb N^*$,  $\ds f_n:x\mapsto\frac{x}{n ( 1+n^2x^2 )}$. Étudier la convergence simple de  $\ds \sum_{n\ge 1} f_n(x)$. La somme  $\ds S$  est-elle continue ? Donner un équivalent de  $\ds S(x)$  quand  $\ds x\to0^+$.
	=== "Corrigé"
		Soit $\ds x\in\mathbb R^*$, $\ds n^2f_n(x)=\frac{nx}{1+n^2x^2}\sim \frac1{nx}\to0$. Ainsi $\ds f_n(x)=o\left(\frac 1{n^2}\right)$ et la série $\ds \sum f_n(x)$ est convergente. Si $\ds x=0$, on a $\ds f_n(0)=0$ et donc la série converge aussi. On en déduit la convergence simple de $\ds \sum f_n$ sur tout $\ds \mathbb R$.   
		Calculons la dérivée de $\ds f_n(x)$ qui est dérivable par théorèmes généraux : $\ds f_n'(x)=\frac 1n\frac{(1+n^2x^2)-2n^2x^2}{(1+n^2x^2)^2}=\frac1n\frac{1-n^2x^2}{(1+n^2x^2)^2}$ qui s'annule en $\ds x_n=\frac 1n$. Or $\ds f_n(x_n)=\frac 1{2n^2}$ qui est le terme général d'une série convergente (série de Riemann convergente). On en déduit que la série $\ds \sum f_n$ est normalement convergente donc uniformément convergente. On peut appliquer le théorème de continuité de la somme d'une série de fonctions et $\ds S(x)$ est continue sur $\ds \mathbb R$. On en déduit par ailleurs que $\ds \lim_{x\to 0} S(x)=0$. On cherche à être plus précis.  
		Soit $\ds x\in\mathbb R$, la fonction $\ds t\mapsto\frac{x}{t(1+t^2x^2)}$ est décroissante. On a alors :  
		
		$$
		\int_{n}^{n+1}\frac{x}{t(1+t^2x^2)}\d t\le f_n(x)\le\int_{n-1}^n \frac {x}{t(1+t^2x^2)}\d t
		$$

		En notant $\ds S_N(x)=\sum_{n=1}^N f_n(x)$ on obtient donc :  
		
		$$
		\int_{1}^{N+1}\frac{x}{t(1+t^2x^2)}\d t\le \sum_{n=1}^Nf_n(x)\le \int_{1}^{N} \frac {x}{t(1+t^2x^2)}\d t+f_1(x)
		$$

		On s'intéresse à l'intégrale :

		$$
		\begin{aligned}
		 \int_{1}^{N} \frac {x}{t(1+t^2x^2)}\d t&\underset{u=xt}=x\int_{x}^{Nx}\frac{1}{u(1+u^2)}\d u\\
		 &= x\int_{x}^{Nx}\frac{1}{u}-\frac{u}{1+u^2}\d u\\
		 &= x\left[\ln(u)-\frac 12\ln(1+u^2)\right]_x^{Nx}\\
		 &= x\left(\ln(N)-\frac 12\ln(1+N^2x^2)+\frac 12\ln(1+x^2)\right)\\
		 &= x\left(\ln(N)-\ln(N)-\ln(x) -\frac 12\ln(1+\frac 1{N^2x^2})+\frac12\ln(1+x^2)\right)\\
		 &\xrightarrow[N\to+\infty]x\left(\frac12\ln(1+x^2)-\ln(x)\right)\\
		 &=\frac x2\ln\left(1+\frac 1{x^2}\right)
		\end{aligned}
		$$

		On obtient alors par PLI :

		$$
		\frac x2\ln\left(1+\frac 1{x^2}\right)\le S(x)\le \frac x2\ln\left(1+\frac 1{x^2}\right)+f_1(x)=\frac x2\ln\left(1+\frac 1{x^2}\right)+\frac{x}{1+x^2}
		$$

		les deux membres extrèmes de l'inégalité sont équivalents à $\ds \frac x2\ln\left(1+\frac 1{x^2}\right)$ en $\ds 0$ donc $\ds S(x)\sim_0 \frac x2\ln\left(1+\frac 1{x^2}\right)\sim_0-\frac x2\ln(x^2)$.

!!! exemple
	=== "Énoncé"
		Soit $\ds \alpha>0$. On étudie la fonction $\ds f$ donnée par

		$$
		f(x)=\sum_{n=0}^{\infty} f_n(x) \text { avec } f_n(x)=\mathrm{e}^{-n^\alpha x}
		$$

		Étudier l'ensemble de définition de $\ds f$ et sa continuité. Étudier la limite de $\ds f$ en $\ds +\infty$.
	=== "Corrigé"
		Tout d'abord si $\ds x\le 0$, la série numérique $\ds \sum f_n(x)$ diverge grossièrement. Si $\ds x>0$, on a $\ds n^2f_n(x)=e^{2\ln(n)-n^\alpha x}\to 0$ par croissances comparées. On en déduit que $\ds f_n(x)=o\left(\frac 1{n^2}\right)$ et par théorèmes de comparaison, la série $\ds \sum f_n(x)$ converge. On en déduit que la fonction $\ds f$ est définie sur $\ds \mathbb R^+_*$.  
		Soit maintenant $\ds a\in\mathbb R^+_*$ et $\ds x\in[a,+\infty[$. Soit $\ds n\in\mathbb N$, la fonction $\ds f_n$ est décroissante sur $\ds [a,+\infty[$. On en déduit que $\ds |f_n(x)|\le |f_n(a)|=e^{-n^\alpha a}$ et donc $\ds \sup_{[a,+\infty[}|f_n|\le |f_n(a)|$ qui est le terme général d'une série convergente. On en déduit que la série de fonctions $\ds \sum f_n$ converge normalement sur tout intervalle de la forme $\ds [a,+\infty[$ et donc uniformément aussi. D'après le théorème de continuité, on a alors $\ds f$ qui est continue sur tout intervalle de la forme $\ds [a,+\infty[$ et donc sur $\ds ]0,+\infty[$ : $\ds f$ est continue sur son ensemble de définition.
		Essayons d'appliquer le théorème de la double limite pour étudier la limite de $\ds f$ en $\ds +\infty$. On remarque tout d'abord que $\ds \lim_{x\to+\infty}f_n(x)=0$ pour $\ds n>0$ et $\ds f_0(x)=1$. Par ailleurs la série de fonction converge normalement donc uniformément sur, par exemple, $\ds [1,+\infty[$.  D'après le théorème de la double limite, on peut affirmer que $\ds \lim_{x\to+\infty}S(x)=\sum_{n=0}^{+\infty}\lim_{x\to+\infty}f_n(x)=1+0=1$. Donc $\ds \lim_{x\to+\infty}S(x)=1$.

!!! exemple
	=== "Énoncé"
		Pour $n\in\mathbb N$, on pose avec $x\in\mathbb R$, $\ds f_n(x)=\frac{e^{-nx}}{1+\sqrt n}$. Déterminer l'ensemble de définition de la série $f=\sum f_n$ puis la limite en $+\infty$ de $f$.
	=== "Corrigé"
		Si $x<0$, alors $f_n(x)\xrightarrow[n\to+\infty]{}+\infty$ donc la série $\sum f_n(x)$ diverge grossièrement. Pour $x=0$, $\ds f_n(x)=\frac1{1+\sqrt n}\sim\frac 1{\sqrt n}$ : c'est le terme général d'une série divergente et donc par théorème de comparaison, $\sum f_n(0)$ diverge. Enfin, pour $x>0$, $\ds f_n(x)=o\left(\frac 1{n^2}\right)$ et donc la série $\sum f_n(x)$ converge.  Ainsi, $f$ est définie sur $]0,+\infty[$. On s'intéresse à la limite en $+\infty$. On va donc utiliser le théorème de la double limite qui nécessite la convergence uniforme sur un intervalle donc l'une des bornes est $+\infty$. On va plutôt aller chercher la convergence normale. On se place sur l'intervalle $[1,+\infty[$. Sur cet intervalle, $x\mapsto f_n(x)$ est décroissante et donc pour tout $x\in [1,+\infty[$, $|f_n(x)|\le f_n(1)$. On en déduit que $\ds ||f_n||_\infty\le \frac{e^{-n}}{1+\sqrt n}$. On a donc une majoration uniforme par un terme général d'une série numérique convergente : il y a convergence normale sur $[1,+\infty[$. De plus, $f_0(x)\xrightarrow[x\to+\infty]{} 1=\ell_0$ et pour $n\ge 1$, $f_n(x)\xrightarrow[x\to+\infty]{}0=\ell_n$. D'après le théorème de la double limite, la série des $\ell_n$ converge (mais on le savait déjà) et $\ds \lim_{x\to+\infty} f(x)=\sum_{n=0}^{+\infty}\ell_n=1$.  

!!! exemple
	=== "Énoncé"
		Cet exercice montre comment s'en sortir lorsque le théorème de la double limite ne fonctionne pas. On définit pour $t\in\mathbb R^+$ et $n\in\mathbb N$, $\ds f_n(t)=\frac{t}{1+t^2n^2}$. Montrer la convergence simple de $\sum f_n$ et étudier la limite de la somme en $0^+$.
	=== "Corrigé"
		Si $t=0$ la somme est nulle. Si $t>0$, alors $\ds f_n(t)\sim\frac 1{tn^2}$ qui est le terme général d'une série convergente. On en déduit que $\sum f_n(t)$ converge pour tout $t\ge 0$ : il y a convergence simple sur $\mathbb R^+$ vers une fonction $f(t)$.  Étudions le reste de la série pour $t>0$.

		$$
		\begin{aligned}
		R_N(t) &= \sum_{n=N+1}^{+\infty} \frac t{1+n^2t^2}\\
		&\ge \sum_{n=N+1}^{2N}\frac{t}{1+n^2t^2}\\
		&\ge \sum_{n=N+1}^2N\frac{1}{1+(2N)^2t^2}\\
		&\ge \frac {tN}{1+4(tN)^2}
		\end{aligned}
		$$

		On a alors $\ds R_N(\frac 1N)\ge \frac 15$ et il n'y a donc pas convegence uniforme du reste vers $0$. La série de fonction $\sum f_n$ ne converge donc pas uniformément et on ne peut donc pas appliquer le théorème de la double limite.

		On va alors passer par une comparaison série/intégrale. On remarque que $\ds x\mapsto \frac t{1+x^2t^2}$ est décroissante en $x$ sur $[0,+\infty]$. On peut donc encadrer :

		$$
		\int_{n}^{n+1}\frac{t}{1+t^2x^2}\d x\le \frac 1{1+n^2t^2}\le\int_{n-1}^n\frac{t}{1+t^2x^2}\d x
		$$

		En sommant de $1$ à $N$, on a :

		$$
		\int_1^{N+1}\frac{t}{1+t^2x^2}\d x\le S_N(t)-t\le \int_{0}^{N}\frac{t}{1+t^2x^2}\d x
		$$

		C'est-à-dire

		$$
		\arctan((N+1)t)-\arctan(t)\le S_N(t)-t\le \arctan(Nt).
		$$

		On fait alors tendre $N$ vers $+\infty$ (on rappelle que $t>0$) et on obtient $\ds \frac \pi2-\arctan(t)\le f(t)-t\le \frac \pi2$ puis par encadrement lorsque $t\to 0$, $\ds\lim_{t\to 0}f(t)=\frac\pi2$ et on remarque au passage que $\ds\lim_{t\to 0}f(t)\ne f(0)$ : la fonction limite n'est pas continue en $0$.

!!! exemple
	=== "Énoncé"
		On pose pour $x\in\mathbb R^+$ et $n\in\mathbb N$, $f_n(x)=\ln(1+x^n)$.  Étudier l'ensemble de définition de $\sum f_n$, sa limite en $0^+$ et sa limite en $1^	-$. On donnera un équivalent en $1^-$.
	=== "Corrigé"
	 	Si $x\ge 1$, $f_n(x)\not\to_{n\to+\infty}0$ donc il y a divergence grossière. Pour $0\le x<1$, $\ln(1+x^n)\sim x^n$ qui est le terme général d'une série géométrique convergente. Ainsi, $\sum f_n$ converge simplement sur $[0,1[$ vers une fonction qu'on appellera $f$. Soit $a\in[0,1[$ et soit $x\in[0,a]$. On a $\ln(1+x^n)\le \ln(1+a^n)\le a^n$ qui est le terme général d'une série géométrique convergente. On en déduit la convergence normale donc uniforme sur tout $[0,a]$. On a par ailleurs $\ds\lim_{x\to 0^+}f_0(x)=\ln(2)=\ell_0$ et pour $n>0$, $\ds\lim_{x\to 0^+}f_n(x)=0=\ell_n$. Par théorème de la double limite, on a donc convergence de la série $\sum\ell_n$ (mais c'était trivial) et surtout $\ds \lim_{x\to 0^+}f(x)=\sum_{n=0}^{+\infty}\ell_n=\ln(2)$.  
		Étudions maintenant la situation en $1^-$. Pour $x\in[0,1[$, la fonction $t\mapsto \ln(1+x^t)$ est décroissante. On en déduit par comparaison série/intégrale que pour $N\in\mathbb N$,

		$$
		\int_{1}^{N+1} \ln(1+x^t)\d t\le \sum_{n=1}^N \ln(1+x^n)\le \int_0^N\ln(1+x^t)\d t
		$$

		Or $\ds\int_0^N\ln(1+x^t)\d t \underset{u=x^t}=\frac{-1}{\ln(x)}\int_{x^N}^1\frac 1u\ln(1+u)\d u$ et la fonction $u\mapsto\frac 1u\ln(1+u)$ est prolongeable par continuité en $0$ et donc $\int_0^N\ln(1+x^t)\d t\xrightarrow[N\to+\infty]{}\frac{-1}{\ln(x)}\int_0^1\frac 1u\ln(1+u)\d u$ et $\ds\int_1^{N+1}\ln(1+x^t)\d t=\frac{-1}{\ln(x)}\int_{x^{N+1}}^x\frac 1u\ln(1+u)\d u\xrightarrow[N\to+\infty]{}\frac{-1}{\ln(x)}\int_0^x\frac 1u\ln(1+u)\d u$. On a donc en passant à la limite,

		$$
		\frac {-1}{\ln(x)}\int_0^x\frac 1u\ln(1+u)\d u\le f(x)-\ln(2)\le \frac{-1}{\ln(x)}\int_0^1\frac 1u\ln(1+u)\d u
		$$

		Et par conservation de l'équivalent, $\ds f(x)\sim_{x\to 1^-}\frac {-1}{\ln(x)}\int_0^1\frac 1u\ln(1+u)\d u$.

### Intégration sur un segment, dérivation des séries de fonctions



!!! theoreme "Intégration de la somme d'une série de fonctions sur un segment"
	Si une série $\ds \sum f_n$ de fonctions continues converge uniformément sur $\ds [a,b]$
	alors la série des intégrales est convergente et :  

	$$  
	\int_a^b \sum_{n=0}^{+\infty} f_n (t) \,\mathrm{d} t =\sum_{n=0}^{+\infty} \int_a^b f_n(t) \,\mathrm{d} t.
	$$

!!! preuve
	Comme précédemment, il s'agit d'appliquer le théorème d'intégration de la limite uniforme à la suite des sommes partielles.

!!! exemple
	=== "Énoncé"
		Montrer que pour tout $t\in]-1,1[$, $\ds\sum_{n=1}^{+\infty}\frac{t^n}n=-\ln(1-x)$.
	=== "Corrigé"
		On sait que pour $x\in]-1;1[$, $\ds\sum_{k=0}^+\infty x^k=\frac 1{1-x}$ (convergence simple). Soit $t\in]-1,1[$ et $x\in[0,t]$ (ou $[t,0]$ en fonction du signe de $t$). On a alors $|x^n|\le|t|^n$ qui est une majoration uniforme par le terme général d'une série géométrique convergente. On peut donc appliquer le théorème d'intégration sur le segment $[0,t]$ :

		$$
		\int_0^t \frac 1{1-x}\d x = \sum_{k=0}^{+\infty}\int_0^tx^n\d x=\sum_{k=0}^{+\infty}\frac{t^{n+1}}{n+1}=\sum_{k=1}^{+\infty}\frac{t^n}{n}
		$$

		Or $\ds\int_0^t\frac 1{1-x}\d x=-\ln(1-t)$ ce qui permet d'écrire que pour $t\in]-1,1[$, $\ds\sum_{k=1}^{+\infty}\frac{t^n}{n}=-\ln(1-t)$.

		On sait déjà par ailleurs que $\ds\sum_{k=1}^{+\infty}\frac{(-1)^n}{n}=-\ln(2)$ et donc l'égalité précédente est en fait vraie sur $[-1,1[$.

!!! exemple
	=== "Énoncé"
		Montrer que $\ds\int_0^1\frac 1{2-x}\d x=\sum_{k=1}^{+\infty}\frac 1{n2^n}$.
	=== "Corrigé"
		Tout d'abord, $\ds\frac1{2-x}=\frac 12\times\frac{1}{1-\frac x2}=\frac 12	\sum_{k=0}^{+\infty}\left(\frac x2\right)^k=\sum_{k=1}^{+\infty}\frac{x^{k-1}}{2^k}$. On se pose alors la question de l'échange $\int\leftrightarrow \sum$ sur le segment $[0,1]$. On pose $\ds f_k(x)=\frac{x^{k-1}}{2^k}$ pour $k>0$ et $x\in[0,1]$. On a pour $k>0$ et $x\in[0,1]$, $\ds|f_k(x)|\le \frac 1{2^k}$ qui est une majoration uniforme par le terme général d'une série convergente. On a donc convergence uniforme de la série des $f_k$ sur $[0,1]$. On peut alors appliquer le théorème d'intégration : $\ds\int_0^1\frac 1{2-x}\d x=\sum_{k=1}^{+\infty}\int_0^1\frac{x^{k-1}}{2^k}\d x=\sum_{k=1}^{+\infty}\frac 1{k2^k}$.

!!! theoreme "Dérivation de la somme d'une série de fonctions"
	Si une série $\ds \sum f_n$ de classe $\ds \mathcal{C}^1$ converge simplement sur un intervalle $\ds I$ et si la série $\ds \sum f'_n$ converge uniformément sur $\ds I$, alors la somme $\ds \sum_{n=0}^{+\infty} f_n$ est de classe $\ds \mathcal{C}^1$ sur $\ds I$ et sa dérivée  est $\ds \displaystyle \sum_{n=0}^{+\infty} f'_n$.

!!! preuve
	Toujours comme précédemment, il s'agit d'appliquer le théorème de dérivation de la limite uniforme à la somme partielle de la série de fonctions.

!!! exemple
	=== "Énoncé"
		On pose $\ds S(x)=\sum_{n=0}^{+\infty}\frac{(-1)^n}{n+x}$. Montrer que $S$ est définie sur $]0,+\infty[$. Est-elle continue ? Est-elle de classe $\mathcal C^1$ ?
	=== "Corrigé"
		Posons $\ds f_n(x)=\frac{(-1)^n}{n+x}$ : les $f_n$ sont continus sur $]0,+\infty[$.  
		Tout d'abord, pour $x$ fixé dans $]0,+\infty[$, la convergence de $\sum f_n(x)$ est donnée par le CSSA. Ensuite pour $N>0$, notons $R_N(x)$ le reste d'ordre $N$ de la série $\sum f_n(x)$. On a, d'après le CSSA toujours, $\ds|R_N(x)|\le |f_{N+1}(x)|=\frac 1{N+1+x}\le \frac 1N$. On a donc une majoration uniforme du reste par un terme qui tend vers $0$ : la suite des restes converge uniformément vers $0$ sur $]0,+\infty[$, et donc la série de fonctions $\sum f_n$ converge uniformément vers $S$. On en déduit, par continuité de la limite uniforme, que $S$ est continue sur $]0,+\infty[$.  
		Soit $n\in\mathbb N$, $f_n$ est dérivable sur $]0,+\infty[$ et $\ds f_n'(x)=\frac{(-1)^{n+1}}{(n+x)^2}$. Pour $x>0$, on a alors $\ds|f_n'(x)|=\frac1{(n+x)^2}\le \frac 1{n^2}$ : c'est une majoration uniforme par un terme général d'une série convergente donc la série $\sum f_n$ converge normalement, donc uniformément sur $]0,+\infty[$. Par théorème de dérivation, on a alors $S$ qui est de classe $\mathcal C^1$ et $\ds S'(x)=\sum_{k=0}^{+\infty}\frac{(-1)^{k+1}}{(n+x)^2}$.  


!!! exemple
	=== "Énoncé"
		Montrer que pour $|x|<1$, $\ds\sum_{n=1}^{+\infty}nx^{n-1}=\frac1{(1-x)^2}$.
	=== "Corrigé"
		Posons $f_n(x)=x^n$. La série de fonctions $\sum f_n$ converge simplement vers $\frac 1{1-x}$ (série géométrique). La série des dérivées est de la forme $\sum g_n$ avec $g_n(x)=nx^{n-1}$. Soit $a\in[0,1[$ et $x\in [-a,a]$, alors $\ds|nx^{n-1}|\le na^{n-1}=o\left(\frac 1{n^2}\right)$. On a donc une majoration uniforme par le terme général d'une série convergente : il y a ainsi convergence normale, donc uniforme, sur tout $[-a,a]$. On en déduit par le théorème de dérivation d'une série de fonctions, que $\sum f_n$ est de classe $\mathcal C^1$ sur $]-1,1[$ et de plus sa dérivée est $\ds\sum_{n=1}^{+\infty}nx^{n-1}$ Or la dérivée de $\sum f_n$ est $\ds x\mapsto\frac 1{(1-x)^2}$. On en déduit que $\ds\frac 1{(1-x)^2}=\sum_{n=1}^{+\infty}nx^{n-1}$.


On a évidemment l'équivalent $\mathcal C^k$ comme pour les suites de fonctions :
!!! theoreme "Classe $\mathcal C^k$ d'une série de fonctions"
	Soit $\ds f=\sum f_n$ une série de fonctions toutes de classe $\ds \mathcal C^k$. Si pour tout $0\le j<k$, $\sum f_n^{(j)}$ converge simplement vers $g_j$ et si $\ds\sum f_n^{(k)}$ converge uniformément vers $g_k$ alors $f$ est de classe $\ds\mathcal C^k$ et pour tout $0\le j\le k$, $\ds f^{(j)}=g_j=\sum f_n^{(j)}$.


!!! exemple
	=== "Énoncé"
		*(fonction zeta de Riemann)* Pour $x>1$, on pose $\ds\zeta(x)=\sum_{n=1}^{+\infty}\frac1{n^x}$. Montrer que $\zeta$ est de classe $\mathcal C^\infty$.
	=== "Corrigé"
		Posons pour $n\ge 1$ et $x>1$, $\ds f_n(x)=\frac 1{n^x}$.  
		Soit $k\in\mathbb N$, $f_n$ est de classe $\mathcal C^k$ par théorèmes généraux et pour $0\le j\le n$, $f_n^{(j)}=\frac{(-\ln n)^j}{n^x}$. Soit $a>1$ et $x>a$, on a pour $0\le j\le k$,  $|f_n^{(j)}(x)|=\frac{ (\ln n)^j}{n^x}\le\frac{(\ln n)^j}{n^a}=o\left(\frac 1{n^{\frac {a+1}2}}\right)$. On a donc une majoration uniforme par un terme général d'une série convergente : il y a convergence normale donc uniforme de toutes les séries dérivées jusqu'au rang $k$ sur tout $[a,+\infty[$. On en déduit par le théorème de classe $\mathcal C^k$ des séries de fonctions que $\zeta$ est de classe $\mathcal C^k$ pour tout $k$ : ainsi $\zeta$ est de classe $\mathcal C^{\infty}$.

!!! remarque
	Tout ce que nous venons de voir se transfère aux fonctions à valeurs complexes en remplaçant la valeur absolue par un module.



## Exercices

### Issus de la banque CCINP
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-008.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-009.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-010.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-011.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-012.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-014.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-016.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-017.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-053.md" %}

### Annales d'oraux
{% include-markdown "../../../exercicesRMS/2022/RMS2022-909.md"%}
{% include-markdown "../../../exercicesRMS/2016/RMS2016-734.md"%}
{% include-markdown "../../../exercicesRMS/2016/RMS2016-736.md"%}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-710.md"%}
{% include-markdown "../../../exercicesRMS/2018/RMS2018-765.md"%}
{% include-markdown "../../../exercicesRMS/2018/RMS2018-660.md"%}
{% include-markdown "../../../exercicesRMS/2021/RMS2021-1019.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1447.md"%}



### Centrale python
{% include-markdown "../../../exercicesCentralePython/RMS2022-1110.md"  rewrite-relative-urls=false%}
{% include-markdown "../../../exercicesCentralePython/RMS2021-1018.md"  rewrite-relative-urls=false%}
{% include-markdown "../../../exercicesCentralePython/RMS2021-1075.md"  rewrite-relative-urls=false%}
