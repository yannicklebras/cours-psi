# Espaces préhilbertiens réels, espaces euclidiens

## Espaces Préhilbertiens réels

!!! definition "produit scalaire"
    Soit $\ds E$ un $\ds \mathbb R$-espace vectoriel. Soit $\ds \varphi$ une application de $\ds E\times E$ dans $\ds \mathbb R$. $\ds \varphi$ est un produit scalaire sur $\ds E$ si et seulement si :

    - $\ds \varphi$ est bilinéaire : pour tout $\ds y\in E$, $\ds x\mapsto \varphi(x,y)$ est une application linéaire sur $\ds E$ et pour tout $\ds x\in E$, $\ds y\mapsto \varphi(x,y)$ est une application linéaire sur $\ds E$.  
    - $\ds \varphi$ est symétrique : pour tout $\ds (x,y)\in E^2$, $\ds \varphi(x,y)=\varphi(y,x)$  
    - $\ds \varphi$ est défini positif : pour tout $\ds x\in E$, $\ds \varphi(x,x)\ge 0$ ^^et^^ $\ds \varphi(x,x)=0\Leftrightarrow x=0$.

    On notera souvent $\ds \langle x,y\rangle$ ou $\ds (x|y)$ ou encore $\ds x\cdot y$. 


!!! definition "espace préhilbertien réel, espace euclidien"
    On appelle espace préhilbertien réel tout $\ds \mathbb R$-espace vectoriel muni d'un produit scalaire. Si de plus $\ds E$ est de dimension finie, alors $\ds E$ est un espace euclidien.

!!! exemple 
    === "Énoncé"
        Montrer que sur $\ds \mathbb R^n$, l'application définie par $\ds \varphi(x,y)=\sum_{i=1}^nx_iy_i$ est un produit scalaire.
    === "Corrigé"
        On commence toujours par le caractère symétrique : $\ds \varphi(x,y)=\sum_{i=1}^nx_iy_i=\sum_{i=1}^ny_ix_i=\varphi(y,x)$.

        Soit $\ds (x,x',y)\in E^3$ et $\ds (\lambda,\mu)\in\mathbb R^2$, 
        
        $$
        \begin{aligned}
        \varphi(\lambda x+\mu x',y)&=\sum_{i=1}^n(\lambda x_i+\mu x'_i)y_i\\
        &=\sum_{i=1}^n\lambda x_iy_i+\mu x'_iy_i\\
        &=\lambda\sum_{i=1}^n x_iy_i+\mu \sum_{i=1}^nx'_iy_i\\
        &=\lambda \varphi(x,y)+\mu\varphi(x',y). 
        \end{aligned}
        $$

        On en déduit la linéarité à gauche, et par symétrie la linéarité à droite.

        Soit $\ds x\in E$, $\ds \varphi(x,x)=\sum_{i=1}^nx_i^2\ge 0$ et si $\ds \varphi(x,x)=0$, alors $\ds \sum_{i=1}^nx_i^2=0$. On a une somme nulle de termes positifs, donc tous les termes sont nuls, ce qui se traduit par $\ds x=0$.

        Finalement, $\ds \varphi$ est bien un produit scalaire sur $\ds \mathbb R^n$. 


!!! exemple
    === "Énoncé"
        Montrer que sur $\ds \mathcal M_n(\mathbb R)$, l'application définie par $\ds \varphi(A,B)=\tr(A^\top B)$ est un produit scalaire.
    === "Corrigé"
        Soient $\ds A$ et $\ds B$ deux éléments de $\ds \mathcal M_n(\mathbb R)$. On sait que la trace est invariante par transposée : 
        
        $$
        \varphi(A,B)=\tr(A^\top B)=\tr\left((A^\top B)^\top\right)=\tr(B^\top(A^\top)^\top)=\tr(B^\top A)=\varphi(B,A)
        $$

        Ainsi $\ds \varphi$ est symétrique. 

        Soient maintenant $\ds (A,A',B)\in\mathcal M_n(\mathbb R)^3$ et $\ds (\lambda,\mu)\in\mathbb R^2$ on a :

        $$
        \begin{aligned}
        \varphi(\lambda A+\mu A',B)&=\tr((\lambda A+\mu A')^\top B)\\
        &=\tr(\lambda A^\top B+\mu {A'}^\top B )\\
        &=\lambda \tr(A^\top)+\mu\tr({A'}^\top B)\\
        &=\lambda \varphi(A,B)+\mu\varphi(A',B)
        \end{aligned}
        $$

        Ainsi $\ds \varphi$ est linéaire à gauche, et pas symétrie à droite aussi. 

        Soit $\ds A\in\mathcal M_n(\mathbb R)$, on note ses coefficients $\ds a_{ij}$. On a alors :

        $$
        \begin{aligned}
        \varphi(A,A)&=\tr(A^\top A)\\
        &=\sum_{i=1}^n (A^\top A)_{ii}\\
        &=\sum_{i=1}^n \sum_{j=1}^n (A^\top)_{ij}a_{ji}\\
        &=\sum_{i=1}^n \sum_{j=1}^na_{ji}^2
        &\ge 0
        \end{aligned}
        $$

        De plus si $\ds \varphi(A,A)=0$ alors $\ds \sum_{i=1}^n\sum_{j=1}^na_ij^2=0$ : c'est une somme nulle de termes positifs : tous les termes sont nuls et donc $\ds A=0$. 

        $\ds \varphi$ est donc bien un produit scalaire.

!!! remarque 
    Les deux produits scalaires cités ci-dessus son appelés *produits scalaires canoniques*, car ils s'expriment en fonction des coordonnées dans la base canonique.


!!! exemple
    === "Énoncé"
        Montrer que sur $\ds \mathcal C^0([a,b],\mathbb R)$, l'application définie par $\ds \varphi(f,g)=\int_a^b f(t)g(t)\d t$ est un produit scalaire.
    === "Corrigé"
        La linéarité et la symétrie découlent clairement des propriété du produit et de l'intégrale. 

        Soit $\ds f\ni\mathcal C^0([a,b],\mathbb R)$. $\ds \varphi(f,f)=\int_a^b f^2\ge 0$ par positivité de l'intégrale. Si de plus $\ds \int_a^b f^2=0$, $\ds f^2$ étant une fonction *continue* et positive, alors $\ds f^2=0$ et donc $\ds f=0$. 

        $\ds \varphi$ est donc bien un produit scalaire.


!!! propriete "Inégalité de Cauchy-Schwarz"
    Soit $\ds E$ un espace préhilbertien réel muni d'un produit scalaire $\ds \langle \cdot,\cdot\rangle$. Soit $\ds (x,y)\in E^2$, alors :
    
    $$
    |\langle x,y\rangle|\le \sqrt{\langle x,x\rangle}\sqrt{\langle y,y\rangle}
    $$ 

    avec égalité si et seulement si $\ds x$ et $\ds y$ sont colinéaires. 

!!! preuve
    Posons $\ds f(t)=\langle x+ty|x+ty\rangle$. Par positivité du produit scalaire, $\ds f(t)\ge0$. Si $\ds y=0$, l'inégalité est vérifiée car $\ds |\langle x,y\rangle|=0$ et $\ds \sqrt{\langle x,x\rangle}\sqrt{\langle y,y\rangle}=0$ : on a donc égalité.

    Concentrons nous sur le cas $\ds y\ne 0$. Dans ce cas , on a $\ds f(t)=\langle x,x\rangle+2t\langle x,y\rangle+t^2\langle y,y\rangle$. Donc $\ds f$ est un polynôme de degré exactement $\ds 2$ et positif : son discriminant $\ds \Delta$ est donc négatif. Or $\ds \Delta=4\langle x,y\rangle^2-4\langle x,x\rangle\langle y,y\rangle$ et ainsi $\ds \Delta\le 0$ est équivalent à $\ds \langle x,y\rangle^2\le \langle x,x\rangle\langle y,y\rangle$. Tous les termes étant positifs, on passe à la racine carrée pour obtenir l'inégalité de Cauchy-Schwarz. 

    Il y a par ailleur égalité si et seulement si $\ds \Delta=0$, donc si et seulement si $\ds f(t)$ admet une racine. Cela se traduit par l'existence de $\ds t_0$ tel que $\ds f(t_0)=0$ ou encore $\ds \langle x+t_0y,x+t_0y\rangle=0$. Par caractère défini positif, on a alors $\ds x+t_0y=0$ et $\ds x$ et $\ds y$ sont colinéaires. 


!!! exemple
    === "Énoncé"
        Soient $\ds a_1,\dots,a_n$ $\ds n$ réels strictement positifs. Montrer que $\ds \left(\sum_{i=1}^n a_i\right)\left(\sum_{i=1}^n\frac 1{a_i}\right)\ge n^2$. 
    === "Corrigé"
        On applique l'inégalité de Cauchy-Schwarz aux familles $\ds (x_1,x_2,\dots,x_n)=(\sqrt{a_1},\dots,\sqrt{a_n})$ et $\ds (y_1,\dots,y_n)=(\frac 1{\sqrt{a_1}},\dots,\frac1{\sqrt{a_n}})$ prises comme familles de $\ds \mathbb R^n$ avec le produit scalaire canonique. Cela donne :

        $$
        \sum_{i=1}^n \sqrt{a_i}\frac 1{\sqrt{a_i}}\le \sqrt{\sum_{i=1}^n (\sqrt{a_i})^2\sum_{i=1}^n\frac1{\sqrt{a_i}^2}} 
        $$ 

        c'est à dire $\ds n\le\sqrt{\sum_{i=1}^n a_i\sum_{i=1}^n\frac1{a_i}}$ ou encore $\ds n^2\le\sum_{i=1}^n a_i\sum_{i=1}^n\frac1{a_i}$.

!!! definition "Norme du produit scalaire"
    Soit $\ds E$ un espace préhilbertien réel muni d'un produit scalaire $\ds \langle\cdot,\cdot\rangle$. La *norme associée au produit scalaire* est l'application $\ds N$ définie sur $\ds E$ par :

    $$
    \forall x\in E,\,N(x)=\sqrt{\langle x,x\rangle}
    $$ 

    souvent on notera $\ds ||x||$. 

!!! propriete "Inégalité triangulaire"
    Soit $\ds E$ un espace préhilbertien réel muni d'un produit scalaire $\ds \langle\cdot,\cdot\rangle$ et $\ds N$ la norme associée. Alors pour tout $\ds (x,y)\in E^2$ :

    $$
    N(x+y)\le N(x)+N(y)
    $$
    
    avec égalité si et seulement si $\ds x$ et $\ds y$ sont positivement colinéaires.

!!! preuve
    Soit $\ds (x,y)\in E^2$. 

    $$
    \begin{aligned}
    N(x+y)^2 &= ||x+y||^2\\
    &=\langle x+y,x+y\rangle\\
    &=\langle x,x\rangle+2\langle x,y\rangle + \langle y,y \rangle\\
    &=||x||^2+||y||^2+2\langle x,y\rangle\\
    &\le ||x||^2+||y||^2+|2\langle x,y\rangle|&\text{par propriété de la valeur absolue}\\
    &\le ||x||^2+||y||^2+2 ||x||||y||&\text{par inégalité de Cauchy-Schwarz}\\
    &\le (||x||+||y||)^2
    \end{aligned}
    $$

    on en déduit l'inégalité voulue, avec égalité si el seulement s'il y a égalité dans les deux majorations : l'égalité dans CS donne $\ds x$ et $\ds y$ colinéaires et l'égalité dans la majoration de la valeur absolue impose que la colinéarité soit positive : $\ds x$ et $\ds y$ doivent être positivement colinéaires.


!!! propriete "identités du produit scalaire"
    Soit $\ds E$ un espace préhilbertien réel muni d'un produit scalaire $\ds \langle\cdot,\cdot\rangle$ et $\ds ||\cdot||$ la norme associée. On a les identités suivantes :
    
    $$
    \begin{aligned}
    ||x+y||^2 = ||x||^2+||y||^2+2\langle x,y\rangle\\
    ||x-y||^2 = ||x||^2+||y||^2-2\langle x,y\rangle\\
    ||x+y||^2+||x-y||^2=2(||x||^2+||y||^2)&\text{ identité du parallèlograme}\\
    \langle x,y\rangle =\frac 12\left(||x+y||^2-||x||^2-||y||^2\right)&\text{ identités de polarisation}\\
    \langle x,y\rangle = \frac 14\left(||x+y||^2-||x-y||^2\right)
    \end{aligned}
    $$
!!! preuve
    Toutes ces identités découlent des deux premières qui sont des identités remarquables.


!!! exemple 
    === "Énoncé"
        Montrer que la norme $\ds \sup$ sur un segment $\ds [0,1]$ pour les fonctions continues n'est pas la norme d'un produit scalaire.
    === "Corrigé"
        Si la norme $\ds \sup$ était la norme d'un produit scalaire, on devrait avoir pour tout couple de fonction $\ds f,g$ continues sur $\ds [0,1]$, $\ds ||f+g||^2+||f-g||^2=2(||f||^2+||g||^2)$. Prenons $\ds f(x)=x$ et $\ds g(x)=x^2$. On a $\ds ||f||=1$, $\ds ||g||=1$, $\ds ||f+g||=2$ et $\ds ||f-g||=\frac 14$. On a donc $\ds ||f+g||^2+||f-g||^2=4+\frac 1{16}$ et $\ds 2(||x||^2+||y||^2)=4$. L'égalité n'est pas vérifiées donc la norme $\ds \sup$ n'est pas la norme d'un produit sclaire.


### Orthogonalité

!!! definition "Vecteurs orthogonaux"
    Soit $\ds E$ un espace préhilbertien réel muni d'un produit scalaire $\ds \langle\cdot,\cdot\rangle$. Deux vecteurs $\ds x$ et $\ds y$ de $E$ sont *orthogonaux* si et seulement si $\ds \langle x,y\rangle=0$. 


!!! exemple
    === "Énoncé"
        Pour le produit scalaire $\ds \langle f,g\rangle=\int_0^{\pi}f(t)g(t)\d t$, montrer que $\ds \sin$ et $\ds \cos$ sont deux vecteurs orthogonaux.
    === "Corrigé"
        On calcule $\ds \int_{0}^\pi \cos(t)\sin(t)\d t=\frac 12[\sin^2(t)]_0^\pi=0$.


!!! definition "Sous-espaces orthogonaux"
    Soit $\ds E$ un espace préhilbertien réel muni d'un produit scalaire $\ds \langle\cdot,\cdot\rangle$. Soient $\ds F$ et $\ds G$ deux sous-espaces vectoriels de $\ds E$. Alors $\ds F$ et $\ds G$ sont *deux sous-espaces orthogonaux* si et seulement si $\ds \forall (x,y)\in F\times G$, $\ds \langle x,y\rangle=0$. 


!!!definition "orthogonal d'un sev"
    Soit $\ds E$ un espace préhilbertien réel muni d'un produit scalaire $\ds \langle\cdot,\cdot\rangle$. Soit $\ds F$ un sous-espace vectoriel de $\ds E$. L'*orthogonal* de $\ds F$ est l'ensemble $\ds F^\bot=\{x\in E\big|\forall y\in F,\, \langle x,y\rangle=0\}$.

    Soit $\ds X$ une partie de $\ds E$. L'*orthogonal* de $\ds X$ est l'ensemble $\ds X^\bot=\{x\in E\big|\forall y\in X,\, \langle x,y\rangle=0\}$.


!!! exemple
    === "Énoncé"
        On considère $\ds E=C([0,1], \mathbb{R})$ muni du produit scalaire $\ds (f, g)=\int_0^1 f(t) g(t) d t$. Soit $\ds F=\{f \in E, f(0)=0\}$. Montrer que $\ds F^{\perp}=\{0\}$.
    === "Corrigé"
        Soit $\ds f\in F^\bot$, la fonction $\ds g:x\mapsto xf(x)$ est un élément de $\ds f$ donc $\ds f$ est orthogonale à $\ds g$. On a donc : $\ds \int_0^1 tf(t)^2\d t=0$. Or $\ds t\mapsto tf(t)^2$ est continue et positive sur $\ds [0,1]$, on en déduit que pour tout $\ds x\in[0,1]$, $\ds xf(x)^2=0$. On peut alors affirmer que $\ds f$ est nulle sur $\ds ]0,1]$ et par continuité, sur $\ds [0,1]$. Ainsi $\ds F^\bot=\{0\}$.

!!! propriete "Sous-espace orthogonal"
    L'ensemble $\ds X^\bot$ est un sous-espace vectoriel de $\ds E$.

!!! preuve
    $\ds 0$ est dans l'orthogonal de toute partie. Donc $\ds X^\bot$ est non-vide. Soient $\ds (a,b)\in (X^\bot)^2$ et $\ds (\lambda,\mu)\in\mathbb R^2$. Soit $\ds x\in X$, alors $\ds \langle x,\lambda a+\mu b\rangle=\lambda\langle x,a\rangle+\mu \langle x,b\rangle=\lambda 0+\mu 0=0$. Donc $\ds \lambda a+\mu b\in X^\bot$. Ainsi $\ds X^\bot$ est un sous-espace vectoriel de $\ds E$.



!!! definition "Famille orthogonale/orthonormale"
    Soit $\ds \mathcal F=(x_1,\dots,x_p)$ une famille d'éléments de $\ds E$. $\ds \mathcal F$ est *orthogonale* si et seulement si pour tout $\ds 1\le i<j\le p$, $\ds \langle x_i,x_j\rangle=0$. 

    Elle est orthonormale ou orthonormée si de plus, pour tout $\ds i$, $\ds \langle x_i,x_i\rangle=1$.  


!!! exemple
    === "Énoncé"
        Montrer que dans $\ds \mathcal M_n(\mathbb R)$ muni du produit scalaire canonique, la base canonique $\ds (E_{ij})$ est une famille orthonormée.
    === "Corrigé"
        On calcule pour $\ds i<j$, $\ds k<l$ et ($i\ne k$ ou $\ds j\ne l$ ):
        
        $$
        \begin{aligned}
        \langle E_{ij},E_{kl}\rangle &= \tr((E_{ij})^\top E_{kl})\\
        &= \tr(E_{ji}E_{kl})\\
        &= \tr(\delta_{ik}E_{jl})\\
        &= \delta_{ik}\tr(E_{jl})\\
        &= \delta_{ik}\delta_{jl}
        \end{aligned}
        $$

        Or soit $\ds i\ne k$ soit $\ds j\ne l$ donc $\ds \langle E_{ij},E_{kl}\rangle=0$. 

        Par ailleur, $\ds \langle E_{ij},E_{ij}\rangle=\tr((E_{ij})^\top E_{ij})=\tr(E_{ji}E_{ij})=\tr(E_{jj})=1$. Ainsi cette base est bien orthonormée. 

!!! propriete "Caractère libre d'une famille orthogonale"
    Toute famille orthogonale de vecteurs non nuls est libre. 

!!! preuve 
    Soit $\ds (b_1,\dots,b_p)$ une famille orthogonale de vecteurs non nuls (aucun vecteur n'est nul). Soit $\ds (\lambda_1,\dots,\lambda_p)\in \mathbb R^p$ tels que $\ds \lambda_1b_1+\cdots+\lambda_pb_p=0$. On a alors, en effectuant le produit scalaire par $\ds b_1$ par exemple : $\ds \langle b_1,\sum_{i=1}^p \lambda_ib_i\rangle =0$. Or $\ds \langle b_1,\sum_{i=1}^p \lambda_ib_i\rangle =\sum_{i=1}^p\lambda_i\langle b_1,b_i\rangle=\lambda _1\langle b_1,b_1\rangle+\sum_{i=2}^p\lambda_i\langle b_1,b_i\rangle=\lambda _1\langle b_1,b_1\rangle$.  On a donc $\ds \lambda_1 ||b_1||^2=0$ et puisque $\ds b_1\ne 0$, $\ds \lambda_1=0$. On montre de même que pour tout $\ds i\in\{1,\dots,p\}$, $\ds \lambda_i=0$ : la famille est bien libre. 


!!! theoreme "Théorème de Pythagore"
    Soit $\ds E$ un espace préhilbertien réel muni d'un produit scalaire $\ds \langle\cdot,\cdot\rangle$ et $\ds ||\cdot||$ la norme associée.

    Soit $\ds (x_1,\dots,x_n)$ une famille de vecteurs orthogonale, alors $\ds ||x_1+\cdots+x_n||^2=||x_1||^2+\cdots+||x_n||^2$. 

    Si $\ds n=2$, il y a équivalence.

!!! preuve 
    Soit $\ds (x_1,\dots,x_n)$ une famille de vecteurs orthogonale. On a alors :

    $$
    \begin{aligned}
    ||\sum_{i=1}^n x_i||^2 &= \langle \sum_{i=1}^n x_i,\sum_{j=1}^n x_j\rangle &\text{Attention à ne pas prendre les mêmes indices}\\
    &= \sum_{i=1}^n\sum_{j=1}^n \langle x_i,x_j\rangle\\
    &= \sum_{i=1}^n\langle x_i,x_i\rangle\\
    &= \sum_{i=1}^n ||x_i||^2
    \end{aligned}
    $$

!!! exemple
    === "Énoncé"
        On note $\ds E=\mathbb R^2$. On considère les trois vecteurs $\ds u=(1,0)$, $\ds v=(0,1)$ et $\ds w=(-1,1)$. Montrer que l'égalité du théorème de Pythagore est vérifiée alors que les trois vecteurs ne forment pas une famille orthogonale. 
    === "Corrigé"
        $\ds ||u+v+w||^2=||(0,2)||^2=4$ et $\ds ||u||^2+||v||^2+||w||^2=1+1+2=4$. On a donc bien $\ds ||u+v+w||^2=||u||^2+||v||^2+||w||^2$ alors que les trois vecteurs ne forment clairement pas une famille orthogonale. 

!!! algorithme "Algorithme d'orthonormalisation de Gram-Schmidt"
    Soit $\ds E$ un espace préhilbertien réel muni d'un produit scalaire $\ds \langle\cdot,\cdot\rangle$ et $\ds ||\cdot||$ la norme associée. Soit $\ds (x_1,\dots,x_n)$ une famille quelconque de $\ds E$. 
    On définit les deux familles $\ds (\epsilon_1,\dots,\epsilon_n)$ et $\ds (e_1,\dots,e_n)$ de la façon suivante : 

    - $\ds \epsilon_1=x_1$

    - $\ds \forall k>1,\, \epsilon_k=x_k-\sum_{i=1}^{k-1}\frac{\langle x_k,\epsilon_i\rangle}{||\epsilon_i||^2}\epsilon_i$.

    - $\ds \forall k\ge 1,\, e_k=\frac{\epsilon_k}{||\epsilon_k||}$.

    Alors la famille $\ds (e_1,\dots,e_n)$ ainsi construite est une famille orthonormée.

!!! preuve 
    Cette famille est normée, par construction. Montrons que la famille des $\ds \epsilon_k$ est orthogonale. 
    
    Pour cela on montre  par récurrence sur $\ds k$ que la famille $\ds (\epsilon_1,\dots,\epsilon_{k})$ est orthogonale. On ne traite ici que l'hérédité : par hypothèse de récurrence, la famille jusqu'à $\ds k-1$ est orthogonale. Il s'agit donc de montrer que $\ds \epsilon_k$ est orthogonal à tous les précédents. Par construction, $\ds \epsilon_k=x_k-\sum_{i=1}^{k-1}\frac{\langle{\epsilon_i|x_k}\rangle}{||\epsilon_i||^2}\epsilon_i$ donc en prenant $\ds 1\le j<k$, on a $\ds \langle{\epsilon_j|\epsilon_k}\rangle=\langle{\epsilon_j|x_k}\rangle-\sum_{i=1}^{k-1}\frac{\langle{\epsilon_i|x_k}\rangle}{||\epsilon_i||^2}\langle{\epsilon_j|\epsilon_i}\rangle\underset{H.R.}=\langle{\epsilon_j|x_k}\rangle-\frac{\langle{\epsilon_j|x_k}\rangle}{||\epsilon_j||^2}\langle{\epsilon_j|\epsilon_j}\rangle=0$. L'hérédité est démontrée.

!!! exemple
    === "Énoncé"
        Dans $\ds \mathscr{M}_2(\mathbb{R})$ muni du produit scalaire canonique $\ds \langle A, B\rangle=\operatorname{tr}\left(A^{\top} B\right)$, orthonormaliser la famille $\ds \left(A_1, A_2, A_3\right)$ avec
        
        $$
        A_1=\left(\begin{array}{ll}
        1 & 0 \\
        0 & 1
        \end{array}\right), \quad A_2=\left(\begin{array}{ll}
        1 & 1 \\
        1 & 1
        \end{array}\right) \quad \text { et } \quad A_3=\left(\begin{array}{ll}
        1 & 0 \\
        0 & 0
        \end{array}\right)
        $$

    === "Corrigé"
        On va orthonormaliser la famille en une famille $\ds (B_1,B_2,B_3)$. On pose $\ds B_1'=A_1$. Ensuite $\ds B'_2=A_2-\frac{\langle A_2,B'_1}{||B'_1||^2}B'_1$. Or $\ds ||B'_1||^2=\tr({B'_1}^\top B'_1)=2$ et $\ds \langle A_2,B'_1\rangle = \tr(A_2^\top B_1')=\tr(A_2)=2$. On a donc $\ds B'_2 = \begin{pmatrix}
        1 & 1 \\
        1 & 1
        \end{pmatrix}-I_2=\begin{pmatrix}0 & 1 \\1 & 0\end{pmatrix}$.

        Enfin, $\ds B'_3=A_3-\frac{\langle A_3,B'_1\rangle}{||B'_1||^2}B'_1-\frac{\langle A_3,B'_2\rangle}{||B'_2||^2}B'_2$. Or $\ds \langle A_3,B'_1\rangle = \tr(A_3^\top B'_1)\tr(A_3)=1$, $\ds \langle A_3,B'_2\rangle = \tr(A_3^\top B'_2)=\tr(\begin{pmatrix}0 & 1 \\ 0 & 0\end{pmatrix})=0$. On calcule tout de même la norme de $\ds B'_2$ : $\ds ||B'_2||^2=\tr({B'_2}^\top B'_2=\tr(I_2)=2$.
        
         On a alors $\ds B'_3=\begin{pmatrix} 1&0\\0&0\end{pmatrix}-\frac 12I_2=\frac{1}{2}\begin{pmatrix}1&0\\0&-1\end{pmatrix}$. On calcule la norme de $\ds B'_3$ : $\ds ||B'_3||^2=\frac 12$. On a finalement : 

        $$
        B_1=\frac 1{\sqrt 2}I_2\quad \text{et}\quad B_2=\frac 1{\sqrt 2}\begin{pmatrix}0 & 1 \\1 & 0\end{pmatrix}\quad \text{et}\quad B_3=\frac 1{\sqrt 2}\begin{pmatrix}1 & 0\\0 & -1\end{pmatrix}
        $$


### Bases orthonormées d'un espace euclidien

!!! propriete "Existence"
    Dans un espace euclidien $\ds E$, il existe une base orthonormée. 

!!! preuve
    $\ds E$ étant de dimension finie, il existe une base $\ds (x_1,\dots,x_n)$. En appliquant l'algorithme d'orthonormalisation de Gram-Schmidt à cette famille, on obtient une famille orthonormée, donc libre, de $\ds n$ éléments : c'est une base orthonormée de $\ds E$.

!!! theoreme "Théorème de la base orthonormée incomplète"
    Soit $\ds E$ un espace euclidien de dimension $\ds n$. Soit $\ds p\le n$ et $\ds (x_1,\dots,x_p)$ une famille orthonormée de vecteurs non-nuls de $\ds E$. Alors il existe $\ds x_{p+1},\dots,x_{n}$ tels que $\ds (x_1,\dots, x_n)$ soit une base orthonormée de $\ds E$. 

!!! preuve 
    On fait une preuve utilisant le principe d'orthonormalisation de Gram-Schmidt : il faut bien comprendre qu'appliqué à une famille déjà orthonormée, l'algorithme n'a pas d'effet. Prenons donc une famille $\ds (x_1,\dots,x_p)$ orthonormée dans $\ds E$, dont aucun vecteur n'est nul. Nous avons vu que cette famille est libre donc on peut lui appliquer le théorème de la base incomplète : il existe $\ds y_{p+1},\dots,y_n$ tels que $\ds (x_1,\dots,x_p,y_{p+1},\dots,y_n)$ soit une base de $\ds E$. Si on applique l'algorithme d'orthonormalisation de Gram-Schmidt à cette famille, les $\ds p$ premier vecteurs étant déjà orthonormés, on obtient une famille $\ds (x_1,\dots,x_p,x_{p+1},\dots,x_n)$ (les $\ds p$ premiers vecteurs sont les mêmes) qui est orthonormée : on a bien complété la famille $\ds (x_1,\dots,x_p)$ pour obtenir une base orthonormée. 


!!! propriete "Expressions en base orthonormale"
    Soit $\ds E$ un espace euclidien muni d'un produit scalaire $\ds \langle\cdot,\cdot\rangle$. On se donne une base orthonormale $\ds (e_1,\dots,e_n)$ et deux éléments $\ds x$ et $\ds y$ de $\ds E$. On a alors les égalités suivantes : 

    $$
    \begin{aligned}
    x&=\sum_{k=1}^n\langle e_k,x\rangle e_k&||x||&=\sqrt{\sum_{k=1}^n\langle e_k,x\rangle^2}&\langle x,y\rangle &=\sum_{k=1}^nx_ky_k
    \end{aligned}
    $$

    où $\ds x_k$ et $\ds y_k$ sont les coordonnées de $\ds x$ et $\ds y$ dans la base.

!!! preuve
    **Première égalité :** $\ds (e_1,\dots,e_n)$ étant une base de $\ds E$, on peut écrire $\ds x=\lambda_1e_1+\dots+\lambda_ne_n$. De plus, $\ds (e_1,\dots,e_n)$ est orthonormée : si on fait le produit scalaire de cette égalité avec $\ds e_1$ par exemple on obtient $\ds \langle x,e_1\rangle=\lambda_1\langle e_1,e_1\rangle+\sum_{i=2}^n \lambda_i\underset{0}{\underbrace{\langle e_1,e_i\rangle}}=\lambda_1||e_1||^2=\lambda_1$. On en déduit que $\ds \lambda_1=\langle x,e_1\rangle$. On montre de même que pour tout $\ds i\in\{1,\dots,n\}$, $\ds \lambda_i=\langle x,e_i\rangle$. Ainsi, 
    
    $$
    x = \sum_{i=1}^n \langle x,e_i\rangle e_i.
    $$

    **Deuxième égalité :** Passons par le carré. 

    $$
    \begin{aligned}
    ||x||^2&=\langle \sum_{i=1}^n x_ie_i,\sum_{j=1}^n x_je_j\rangle\\
    &=\sum_{i=1}^n\sum_{j=1}^nx_ix_j\langle e_i,e_j\rangle\\
    &=\sum_{i=1}^n\sum_{j=1}^nx_ix_j\delta_{ij}\\
    &=\sum_{i=1}^n x_i^2
    \end{aligned}
    $$

    **Troisième égalité :** On développe comme précédemment :

    $$
    \begin{aligned}
    \langle x,y\rangle &= \langle\sum_{i=1}^n x_ie_i,\sum_{j=1}^n y_je_j\rangle\\
    &=\sum_{i=1}^n\sum_{j=1}^n x_iy_j\langle e_i,e_j\rangle\\
    &=\sum_{i=1}^n\sum_{j=1}^n x_iy_j\delta_{ij}\\
    &=\sum_{i=1}^n x_iy_i
    \end{aligned}
    $$

    Ainsi, lorsqu'on est dans un espace euclidien, il vaut toujours mieux travailler dans une base orthonormée : tout y est plus simple !


!!! exemple
    === "Énoncé"
        On pose $\ds E=\mathbb R_3[X]$. Déterminer un produit scalaire pour lequel la famille $\ds (1,X-1,(X-1)^2,(X-1)^3)$ est orthonormée.
    === "Corrigé"
        On sait bien que cette famille est une base. 

        Supposons qu'on ait un tel produit scalaire. Soit $\ds P$ et $\ds Q$ dans $\ds E$ avec $\ds P=p_0+p_1(X-1)+p_2(X-1)^2+p_3(X-1)^3$ et $\ds Q=q_0+q_1(X-1)+q_2(X-1)^2+q_3(X-1)^3$, alors $\ds \langle P,Q\rangle=p_0q_0+p_1q_1+p_2q_2+p_3q_3$. Or on sait bien d'après la formule de Taylor pour les polynômes, que $\ds P=P(1)+P'(1)(X-1)+\frac {P''(1)}2(X-1)^2+\frac{P'''(1)}{6}(X-1)^3$. Cela donnerait donc : $\ds \langle P,Q\rangle= P(1)Q(1)+P'(1)Q'(1)+\frac 14P''(1)Q''(1)+\frac 1{36}P'''(1)Q'''(1)$. On vérifie facilement que cette application est bilinéaire, symétrique et positive. Et si $\ds \langle P,P\rangle=0$ alors $\ds P(1)=P'(1)=P''(1)=P'''(1)=0$ et d'après la formule de Taylor les polynômes, $\ds P=0$. On a donc bien ici un produit scalaire dont on peut vérifier qu'il répond à la contrainte imposée.

### Projection orthogonale sur un sous-espace en dimension finie

!!! propriete "Supplémentaire orthogonal"
    Soit $\ds E$ un espace euclidien. Soit $\ds F$ un sous-espace vectoriel de $\ds E$. Alors $\ds F^\bot$ est un supplémentaire de $\ds F$ appelé *orthogonal de $\ds F$*.

!!! preuve
    Soit $\ds p$ la dimension de $\ds F$.

    Nous avons déjà montré que $\ds F^\bot$ est un sous-espace vectoriel de $\ds E$. Montrons que c'est bien un supplémentaire de $\ds F$. Soit $\ds x\in F\cap F^\bot$. $\ds x\in F^\bot$ donc $\ds x$ est orthogonal à tout élément de $\ds F$. Or $\ds x\in F$ Donc $\ds x$ est orthogonal à lui même : $\ds \langle x,x\rangle=0$. Or le produit scalaire est défini positif donc $\ds x=0$. On en déduit que $\ds F\cap F^\bot=\{0\}$ : on en déduit que $\ds F$ et $\ds F^\bot$ sont en somme directe et ainsi $\ds n\ge \dim(F\oplus F^\bot)=\dim(F)+\dim(F^\bot)$. Donc $\ds \dim F^\bot\le n-p$.  
    
    Soit $\ds (b_1,\dots,b_p)$ une base orthonormée de $\ds F$ que l'on complète en une base orthonormée $\ds (b_1,\dots,b_p,b_{p+1},\dots,b_n)$ de $\ds E$. Les éléments $\ds b_{p+1},\dots,b_n$ sont tous orthogonaux à tous les éléments de $\ds (b_1,\dots,b_p)$ donc $\ds b_{p+1},\dots,b_n$ sont des éléments de $\ds F^\bot$. On en déduit que $\ds \dim F^\bot\ge n-p$. 

    Finalement, $\ds \dim F^\bot=n-p$ et donc $\ds \dim F+\dim F^\bot=\dim E$. Puisque $\ds F\cap F^\bot=\{0\}$, $\ds F$ et $\ds F^\bot$ sont supplémentaires dans $\ds E$. 


!!! exemple
    === "Énoncé"
        Montrer que, dans $\ds \mathcal M_n(\mathbb R)$ muni du produit scalaire canonique, $\ds \mathcal S_n(\mathbb R)$ et $\ds \mathcal A_n(\mathbb R)$ sont supplémentaires orthogonaux.
    === "Corrigé"
        On sait déjà que les matrices symétriques et antisymétriques forment deux espaces supplémentaires. Soit $\ds A$ antisymétrique et $\ds S$ symétrique. On rappelle que la trace est invariante par transposition. On a donc, puisque $\ds A$ est antisymétrique, $\ds \langle A,S\rangle=\tr(A^\top S)=\tr(-AS)=-\tr(AS)=-\tr(SA)=-\tr(S^\top A)$ car $\ds S$ est symétrique. On en déduit que $\ds \langle A,S\rangle = -\langle S,A\rangle=-\langle A,S\rangle$.  Ainsi $\ds 2\langle A,S\rangle=0$ et donc $\ds A$ et $\ds S$ sont orthogonaux. 



!!! definition "Projection orthogonale"
    Soit $\ds E$ un espace euclidien, soit $\ds F$ un sous-espace vectoriel de $\ds E$. Le projecteur $\ds p_F$ sur $\ds F$ parallèlement à $\ds F^\bot$  est appelé *projecteur orthogonal sur $\ds F$*. 

!!! propriete "Expression de $\ds p_F$"
    Si $\ds F$ est un sous-espace vectoriel d'un espace euclidien $\ds E$ et si $\ds e_1,\dots,e_p$ est une base orthonormée de $\ds F$, alors pour tout $\ds x\in E$, 

    $$
    p_F(x)=\sum_{i=1}^p \langle x,e_i\rangle e_i.
    $$

!!! preuve
    Soit $\ds (e_1,\dots,e_p)$ une base orthonormée de $\ds F$ complétée en une base orthonormée $\ds e_1,\dots,e_n$ de $\ds E$ ($(e_{p+1},\dots,e_n)$ est une base de $\ds F^\bot$). On a vu plus haut que $\ds x=\sum_{i=1}^n \langle x,e_i\rangle e_i$. Passons tout ceci à travers $\ds p_F$ :

    $$
    p_F(x)=\sum_{i=1}^n \langle x,e_i\rangle p_F(e_i)=\sum_{i=1}^p \langle x,e_i\rangle \underset{e_i}{\underbrace{p_F(e_i)}}+\sum_{i=p+1}^n \langle x,e_i\rangle \underset{0}{\underbrace{p(e_i)}}=\sum_{i=1}^p \langle x,e_i\rangle e_i.
    $$

!!! exemple
    === "Énoncé"
        Soit $\ds p$ la projection orthogonale sur $\ds S_n(\mathbb R)$. Soit $\ds M\in\mathcal M_n(\mathbb R)$. Déterminer $\ds p(M)$ en utilisant cette formule.
    === "Corrigé"
        Une base orthonormée (à vérifier) de $\ds S_n(\mathbb R)$ est donnée par la famille des $\ds E_ii$ pour $\ds i$ de $\ds 1$ à $\ds n$ et des $\ds \frac 1{\sqrt 2}\left(E_{ij}+E_{ji}\right)$ pour $\ds 1\le i<j\le n$. Calculons le produit scalaire de $\ds M$ avec chacun des vecteurs de la base. 

        $$
        \begin{aligned}
        \langle M,E_{ii}\rangle &= \tr(M^\top E_{ii})\\
        &= \sum_{k=1}^n \left(M^\top E_{ii}\right)_{kk}\\
        &= \sum_{k=1}^n \sum_{j=1}^n (M^\top)_{kj}(E_{ii})_{jk}\\
        &= \sum_{k=1}^n \sum_{j=1}^n M_{kj}\delta_{ij}\delta_{jk}\\
        &= \sum_{k=1}^n M_{kk}\delta_{ik}\\
        &= M_{ii}
        \end{aligned}
        $$

        $$
        \begin{aligned}
        \langle M,\frac1{\sqrt2}(E_{ij}+E_{ji})\rangle &= \frac 1{\sqrt2}\left(\tr(M^\top E_{ij}+M^\top E_{ji})\right)\\
        &=\frac 1{\sqrt 2}\left(\sum_{k=1}^n(M^\top E_{ij}+M^\top E_{ji})_{kk}\right)\\
        &=\frac 1{\sqrt 2}\left(\sum_{k=1}^n\sum_{\ell=1}^n(M^\top)_{k\ell}(E_{ij})_{\ell k}+ (M^\top)_{k\ell}(E_{ji})_{\ell k}\right)\\
        &=\frac 1{\sqrt 2}\left(\sum_{k=1}^n\sum_{\ell=1}^nM_{\ell k}\delta_{i\ell}\delta_{jk}+ M_{\ell k}\delta_{j\ell}\delta_{ik}\right)\\
        &=\frac 1{\sqrt 2}\left(\sum_{k=1}^n M_{ik}\delta_{jk}+\sum_{\ell=1}^{n} M_{\ell i}\delta_{j\ell}\right)\\
        &=\frac 1{\sqrt 2}(M_{ij}+M_{ji})
        \end{aligned}
        $$
        
        On en déduit que $\ds p(M)=\sum_{i=1}^n M_{ii}E_{ii}+\sum_{1\le i<j\le n}\frac 12(M_{ij}+M_{ji})(E_{ij}+E_{ji})$ c'est à dire $\ds p(M)=\frac 12(M+M^\top)$. 


        On aurait pu faire plus vite : on connaît en effet la décomposition d'une matrice $\ds M$ dans $\ds \mathcal S_n(\mathbb R)\oplus \mathcal A_n(\mathbb R)$ : $\ds M=\frac 12(M+M^\top)+\frac 12(M-M^\top)$. Donc $\ds p(M)=\frac 12(M+M^\top)$. 


!!! definition "Distance à un sous-espace"
    Soit $\ds E$ un espace euclidien, soit $\ds ||\cdot||$ la norme associée au produit scalaire de $\ds E$. Soit $\ds F$ un sous-espace vectoriel de $\ds E$ et $\ds x\in E$. On appelle *distance de $\ds x$ à $\ds F$* la quantité 

    $$
    d(x,F)=\inf_{y\in F}||x-y||.
    $$

!!! propriete "La distance est atteinte"
    Soit $\ds E$ un espace euclidien, soit $\ds ||\cdot||$ la norme associée au produit scalaire de $\ds E$. Soit $\ds F$ un sous-espace vectoriel de $\ds E$ et $\ds x\in E$. Alors $\ds d(x,f)=||p_f(x)-x||$ et si $\ds ||y-x||=d(x,F)$ alors $\ds y=p_F(x)$. 

!!! preuve 
    Soit $\ds y\in F$ alors $\ds x-p_F(x)\in F^\bot$ et $\ds y-p_F(x)\in F$. D'après le théorème de Pythagore, $\ds ||x-y||^2=||x-p_F(x)+p_F(x)-y||^2=||x-p_F(x)||^2+||y-p_F(x)||^2\ge ||x-p_F(x)||^2$ avec égalité si et seulement si $\ds ||y-p_F(x)||=0$ c'est à dire $\ds y=p_F(x)$. 

    On en déduit notamment que $\ds ||x-y||\ge ||x-p_F(x)||$ donc par passage à l'inf à gauche, $\ds d(x,F)\ge ||x-p_F(x)||$. Or $\ds p_F(x)\in F$ donc $\ds ||x-p_F(x)||\ge d(x,F)$. Par antisymétrie de la relation d'ordre on a donc $\ds d(x,F)=||x-p_F(x)||$ et d'après la remarque sur l'égalité juste au dessus, $\ds d(x,F)=||x-y||$ avec $\ds y\in F$ ssi $\ds y=p_F(x)$.


!!! exemple
    === "Énoncé"
        Calculer la distance de $\ds A=\begin{pmatrix} 1&2&3\\-1&1&1\\3&0&1\end{pmatrix}$ à $\ds S_n(\mathbb R)$ dans $\ds \mathcal M_n(\mathbb R)$ muni du produit scalaire canonique. 
    === "Corrigé"
        Soit $\ds p$ la projection orthogonale sur $\ds \mathcal S_n(\mathbb R)$. D'après le cours, $\ds d(A,\mathcal S_n(\mathbb R))=||A-p(A)||$. Or $\ds p(A)=\frac 12(A+A^\top)$ donc $\ds d(A,\mathcal S_n(\mathbb R))=||\frac 12 (A-A^\top)||$. On a $\ds \frac 12(A-A^\top)=\frac 12\begin{pmatrix}0 & 3 & 0\\-3 & 0 & 1\\0 & -1 & 0\end{pmatrix}$. Notons cette matrice $\ds B$. On cherche donc à calculer $\ds \sqrt{\tr(B^\top B)}$. On calcule d'abord $\ds B^\top B=\frac 14\begin{pmatrix}9 & 0 & -3\\0 & 10 & 0\\ -3 & 0 & 1\end{pmatrix}$. 

        Ainsi, $\ds d(A,\mathcal S_n(\mathbb R))=\sqrt{\tr(B^\top B)}=\sqrt{\frac 14\times 20}=\sqrt 5$. 


!!! propriete "Calcul de la distance à un hyperplan"
    Soit $\ds E$ un espace euclidien, soit $\ds ||\cdot||$ la norme associée au produit scalaire de $\ds E$. Soit $\ds u\in E$, on pose $\ds F=vect(u)^\bot$. Alors pour tout $\ds x$ dans $\ds E$, 

    $$
    p_F(x)=x-\frac {\langle x,u\rangle}{||u||^2}u\quad \text{et}\quad d(x,F)=\frac{|\langle x,u\rangle|}{||u||}
    $$ 

    
!!! preuve 
    On pose $\ds b = \frac u{||u||}$ et on associe $\ds b$ à une base de $\ds F$ pour faire une base de $\ds E$. On a alors d'après les propriétés de décomposition dans une base orthonormée : $\ds x=\langle x,b\rangle b+p_F(x)$ et donc $\ds p_F(x)=x-\langle x,b\rangle=x-\langle x,u\rangle\frac u{||u||^2}$. 

    On en déduit que $\ds d(x,F)=||x-p_F(x)||=\big|\big|\frac{\langle x,u\rangle u}{||u||^2}\big|\big|=\frac{|\langle x,u\rangle|}{||u||}$.

!!! exemple
    === "Énoncé"
        Dans $\ds \mathcal M_n(\mathbb R)$ muni du produit scalaire canonique, on note $\ds J$ la matrice dont tous les coefficients valent $\ds 1$. Donner la distance de $\ds J$ à $\ds \ker\tr$. 
    === "Corrigé"
        Un vecteur orthogonal à l'hyperplan $\ds \ker\tr$ est $\ds I_n$. On applique la formule : $\ds d(J,\ker\tr)=\frac{|\langle J,I_n\rangle|}{||I_n||}=\frac{|\tr(J^\top I_n)|}{\sqrt{\tr(I_n^\top I_n)}}=\frac{n}{\sqrt n}=\sqrt n$.



!!! theoreme "Théorème de représentation des formes linéaires"
    Soit $\ds \varphi$ une forme linéaire sur un espace euclidien $\ds E$. Alors il existe un unique $\ds a\in E$ tel que pour tout $\ds x\in E$, $\ds \varphi(x)=\langle a,x\rangle$. 

!!! preuve
    Supposons que $\ds \varphi(x)=\langle a,x \rangle=\langle b,x \rangle$ pour tout $\ds x$. On a alors $\ds \langle a-b|x\rangle=0$ pour tout $\ds x$ : $\ds a-b$ est donc orthogonal à tout élément de $\ds E$ : $\ds a-b=0$. Si un tel vecteur existe, il est donc unique. Montrons donc l'existence d'un tel vecteur. 

    $\ds \varphi$ est une forme linéaire donc $\ds \ker\varphi$ est de dimension $\ds n-1$. Prenons $\ds u$ un vecteur unitaire orthogonal à $\ds \ker\varphi$. Alors $\ds E=\ker\varphi\oplus Vect(u)$. 

    Analyse : supposons l'existence de $\ds a$ alors 
    pour tout $\ds x\in\ker\varphi$, $\ds \langle a|x\rangle=\varphi(x)=0$. Donc $\ds a\in(\ker\varphi)^{\bot}$. On en déduit que $\ds a=\lambda u$. On a alors $\ds \varphi(u)=\langle a,u\rangle=\lambda\langle u,u\rangle =\lambda$. Donc $\ds a=\varphi(u)u$. 

    Synthèse : Posons $\ds a=\varphi(u)u$. Soit $\ds x\in E$, on pose $\ds x=x_K+\lambda u$. On a alors : 
    $\ds \varphi(x)=\varphi(x_K)+\varphi(\lambda u)=\lambda\varphi(u)$. Et $\ds \langle a,x\rangle=\langle a,x_k+\lambda u\rangle =\langle a,x_k\rangle+\lambda \langle a,u\rangle=\lambda \varphi(u)\langle u,u\rangle=\lambda\varphi(u)$. Ainsi $\ds \varphi(x)=\langle a,x\rangle$.


!!! remarque
    Ce théorème est une version faible du Théorème de Riesz, plus général.


## Endomorphismes d'un espace euclidien

### Isométries vectorielles d'un espace euclidien
!!! definition "Isométrie vectorielle"
    Un endomorphisme $\ds u$ d'un espace euclidien $\ds E$ est une *isométrie vectorielle* si et seulement si :

    $$
    \forall x\in E,\, ||u(x)||=||x||
    $$
    

!!! propriete "bijectivité"
    Soit $\ds u$ une isométrie d'un espace euclidien $\ds E$, alors $\ds u$ est un automorphisme de $\ds E$.

!!! preuve
    Montrons que $\ds u$ est injective : un endomorphisme injectif est bijectif. 

    Soit $\ds x$ tel que $\ds u(x)=0$. Alors $\ds u$ étant une isométrie, $\ds ||u(x)||=||x||$ donc $\ds ||x||=0$ c'est à dire $\ds x=0$ : $\ds u$ est injective, donc bijective.

!!! exemple 
    === "Énoncé"
        Montrer qu'une symétrie orthogonale  d'un espace euclidien est une isométrie vectorielle.
    === "Corrigé"
        Soit $\ds s$ une symétrie orthogonale par rapport à un sous-espace vectoriel $\ds F$. Soit $\ds x\in E$, on a $\ds x=x_F+x_\bot$ avec $\ds x_F\in F$ et $\ds x_\bot\in F^\bot$. Alors $\ds s(x)=x_F-x_\bot$ par définition et d'après le théorème de Pythagore, $\ds ||s(x)||^2=||x_F||^2+||-x_\bot||^2=||x_F||^2+||x_\bot||^2=||x||^2$ ou encore $\ds ||s(x)||=||x||$. Ainsi $\ds s$ est une isométrie vectorielle.    

!!! propriete "Caractérisation des isométries"
    Soit $\ds E$ un espace euclidien, $\ds u$ un endomorphisme de $\ds E$. Alors il y a équivalence entre les trois propriétés suivantes :

    - $\ds u$ est une isométrie vectorielle ;  
    - pour tout couple $\ds (x,y)$ d'éléments de $\ds E$, $\ds \langle u(x),u(y)\rangle=\langle x,y\rangle$ ;  
    - l'image d'une base orthonormée par $\ds u$ est une base orthonormée.

!!! preuve
    Supposons que $\ds u$ soit une isométrie vectorielle. Soit $\ds (x,y)\in E^2$. D'après les formules de polarisation on a par exemple : 

    $$
    \begin{aligned}
    \langle u(x),u(y)\rangle&=\frac 14\left(||u(x)+u(y)||^2-||u(x)-u(y)||^2\right)&\text{formule de polarisation}\\
    &=\frac 14\left(||u(x+y)||^2-||u(x-y)||^2\right)&\text{linéarité}\\
    &=\frac 14\left(||x+y||^2-||x-y||^2\right)&u\text{ est une isométrie}\\
    &= \langle x,y\rangle&\text{formule de polarisation}
    \end{aligned}
    $$

    Ainsi $\ds u$ conserve le produit scalaire.

    Supposons maintenant que $\ds u$ conserve le produit scalaire. Soit $\ds (b_1,\dots,b_n)$ une base orthonormée de $\ds E$. Montrons que $\ds (u(b_1),\dots,u(b_n))$ est une famille orthonormée. En effet, $\ds u$ étant un automorphisme, on sait que $\ds (u(b_1),\dots,u(b_n))$ est une base. Tout d'abord, $\ds ||u(b_i)||^2=\langle u(b_i),u(b_i)\rangle=\langle b_i,b_i\rangle=1$. Donc la famille est bien bornée. Soit maintenant $\ds i\ne j$, $\ds \langle u(b_i),u(b_j)\rangle = \langle b_i,b_j\rangle=0$. Donc la famille est bien orthogonale. L'image d'un base orthonormée est donc bien une base orthonormée.


    Enfin, supposons que l'image d'une base orthonormée est une base orthonormée. Soit $\ds (b_1,\dots,b_n)$ une base orthonormée de $\ds E$. Soit $\ds x\in E$, on pose $\ds x=\sum_{i=1}^nx_ib_i$ :

    $$
    \begin{aligned}
    ||u(x)||^2 &= \langle u(x),u(x)\rangle \\
    &=\langle u(\sum_{i=1}^nx_ib_i),u(\sum_{j=1}^nx_jb_j)\\
    &=\sum_{i=1}^n\sum_{j=1}^nx_ix_j\langle u(b_i),u(b_j)\rangle\\
    &=\sum_{i=1}^n\sum_{j=1}^nx_ix_j\delta_{ij}\\
    &=\sum_{i=1}^n x_i^2\\
    &=||x||^2
    \end{aligned}
    $$

    donc $\ds u$ est une isométrie vectorielle.

!!! definition "Groupe orthogonal"
    On appelle *groupe orthogonal* l'ensemble des isométries vectorielles de $\ds E$ et on note $\ds O(E)$.


!!! propriete "structure de $\ds O(E)$"
    Tout élément de $\ds O(E)$ est inversible et $\ds O(E)$ est stable par composition et inverse.

!!! preuve
    Nous avons déjà montré que les éléments de $\ds O(E)$ sont inversibles (ce sont des automorphismes). Soit $\ds u$ et $\ds v$ deux éléments de $\ds O(E)$. Montrons que $\ds u^{-1}\in O(E)$ et que $\ds u\circ v\in O(E)$.

    Soit $\ds x\in E$, $\ds ||u^{-1}(x)|| = ||u(u^{-1}(x))||=||x||$, donc $\ds u^{-1}$ est une isométrie vectorielle.

    Soit $\ds x\in E$, $\ds ||u\circ v(x)||=||u(v(x))||=||v(x)||=||x||$ donc $\ds u\circ v$ est une isométrie vectorielle.



!!! propriete "Stabilité de l'orthogonal par $\ds O(E)$"
    Soit $\ds E$ un espace euclidien, $\ds F$ un sous-espace de $\ds E$ et $\ds u\in\mathcal L(E)$ tel que $\ds F$ est stable par $\ds u$. Alors $\ds F^\bot$ est stable par $\ds u$.

!!! preuve
    Soit donc $\ds E$ un espace euclidien, soit $\ds F$ un sous-espace vectoriel de $\ds E$. Soit $\ds u\in\mathcal L(E)$, on suppose que $\ds u(F)\subset F$. On veut montrer que $\ds u(F^\bot)\subset F^\bot$. 

    Tout d'abord, $\ds u$ étant un automorphisme, $\ds u(F)$ et $\ds F$ ont même dimension. Or $\ds u(F)\subset F$ donc $\ds u(F)=F$. Soit $\ds y\in u(F^\bot)$, on veut montrer que $\ds y$ est dans $\ds F^\bot$. Soit $\ds x\in F^\bot$ tel que $\ds y=u(x)$. Prenons $\ds y'\in F$. Puisque $\ds F=u(F)$, il existe $\ds x'\in F$ tel que $\ds y'=u(x')$. On a alors $\ds \langle y,y'\rangle = \langle f(x),f(x')\rangle = \langle x,x'\rangle = 0$ car $\ds x\in F^\bot$ et $\ds x'\in F$. Ainsi, si $\ds y\in u(F^\bot)$ alors $\ds y$ est orthogonal à tout élément de $\ds F$ : $\ds u(F^\bot)\subset F^\bot$. Et avec les même considérations de dimension, on a bien sûr finalement $\ds u(F^\bot)=F^\bot$. 


!!! propriete "Valeurs propres"
    Soir $\ds u$ une isométrie vectorielle. Soit $\ds \lambda$ une valeur propre réelle de $\ds u$. Alors $\ds \lambda =\pm 1$. 

!!! preuve
    Soit $\ds x$ un vecteur propre associé à $\ds \lambda$. $\ds u$ conserve la norme donc $\ds ||u(x)||=||x||$. Mais $\ds ||u(x)||=||\lambda x||=|\lambda|\,||x||$. On en déduit $\ds |\lambda|=1$. 



### Matrices orthogonales

!!! definition
    Une matrice $\ds A\in\mathcal M_n(\mathbb R)$ est *orthogonale* si et seulement si $\ds A^\top A=I_n$.


!!! propriete "Caractérisation par les lignes et les colonnes"
    Soit $\ds A\in\mathcal M_n(\mathbb R)$. On a équivalence entre les deux propriétés suivantes :

    - i) $\ds A$ est orthogonale ;  
    - ii) La famille des colonnes de $\ds A$ est une base orthonormée de $\ds \mathcal M_{n,1}(\mathbb R)$ ;  

!!! preuve
    Montrons i) $\ds \Leftrightarrow$ ii). Soit $\ds A$ orthogonale. Notons $\ds C_1,\dots,C_n$ ses colonnes. Alors le terme $\ds i,j$ de $\ds A^\top A$ est égal à $\ds \langle C_i,C_j\rangle$. Ainsi $\ds A^\top A=I_n$ si et seulement si $\ds \langle C_i,C_j\rangle =\delta_{ij}$ : on en déduit que la famille des colonnes est orthonormée si et seulement si $\ds A$ est orthogonale. 

    Pour montrer que i) $\ds \Leftrightarrow$ iii), on utilise le fait que $\ds A$ est inversible d'inverse $\ds A^\top$ pour écrire $\ds AA^\top=I_n$. On réutilise alors les mêmes arguments que précédemment. 


!!! propriete "Changement de base"
    Toute matrice de changement de base orthonormée est orthogonale.  
    Toute matrice orthogonale est une matrice de changement de base orthonormée.

!!! preuve
    Soient $\ds \mathcal B$ et $\ds \mathcal C$ deux bases orthonormées de $\ds E$ euclidien de dimension $\ds n$. On pose $\ds \mathcal B=(b_1,\dots,b_n)$ et $\ds \mathcal C=(c_1,\dots,c_n)$. La matrice  $\ds P$ de passage de $\ds \mathcal B$ à $\ds \mathcal C$ contient, en colonne, les coordonées de $\ds \mathcal C$ dans la base $\ds \mathcal B$. Ainsi, $\ds P_{ij}=\langle c_j,b_i\rangle$. 
    
    $$
    \left(P^\top P\right)_{ij}=\sum_{k=1}^n p_{ki}p_{kj}=\sum_{k=1}^n\langle e_k,e_i\rangle\langle e_k,e_j\langle=\big\langle \sum_{k=1}^n\langle e_k,e_i\rangle e_k,e_j\big\rangle=\langle e_i,e_j\rangle=\delta_{ij}
    $$

    On en déduit qu $\ds P^\top P=I_n$ et donc que $\ds P$ est orthogonale.

    Réciproquement, soit $\ds A$ une matrice orthogonale de $\ds O(n)$. Soit $\ds E=\mathcal M_{n,1}(\mathbb R)$ muni du produit scalaire canonique. Soit $\ds \mathcal B$ la base caononique de $\ds E$ et $\ds \mathcal C$ la famille des colonnes de $\ds A$.  La matrice $\ds A$ est donc la matrice de la famille $\ds \mathcal C$ dans la base $\ds \mathcal B$. D'après une remarque précédente, on sait que $\ds (A^\top A)_{ij}=\langle C_i,C_j\rangle=\delta_{ij}$ donc la famille $\ds \mathcal C$ est orthonormée : c'est une base orthonormée. 

!!! propriete "Caractérisation des isométries vectorielles"
    $\ds u\in\mathcal L(E)$ est une isométrie vectorielle de $\ds E$ si et seulement si sa matrice dans toute base orthonormée est une matrice orthogonale. On pourra parfois rencontrer le terme *automorphisme orthogonal* mais on préfèrera *isométrie vectorielle*

!!! preuve
    Soit $\ds u$ une isométrie vectorielle de $\ds E$. Soit $\ds \mathcal B=(b_1,\dots,b_n)$ une base orthonormée de $\ds E$. On sait que $\ds (u(b_1),\dots,u(b_n))$ est aussi une base orthonormée. La matrice de $\ds u$ dans la base $\ds \mathcal B$ est alors la matrice de $\ds (u(b_1),\dots,u(b_n))$ dans la base $\ds \mathcal B$, c'est à dire la matrice de passage de $\ds \mathcal B$ à $\ds (u(b_1),\dots,u(b_n))$ : c'est une matrice de changement de base orthonormée et donc une matrice orthogonale.

    Réciproquement, supposons que la matrice de $\ds u$ dans toute base orthonormée soit orthogonale. Montrons que $\ds u$ est une isométrie. Nous allons montrer que l'image de toute base orthonormée est une base orthonormée. 

    Soit donc $\ds \mathcal B$ une base orthonormée et $\ds \mathcal C$ l'image de $\ds \mathcal B$ par $\ds u$. On note $\ds c_i=u(b_i)$. Notons $\ds M$ la matrice de $\ds u$ dans $\ds \mathcal B$  : par hypothèse, $\ds M$ est orthogonale. On a alors :

    $$
    \begin{aligned}
    \langle c_i,c_j\rangle &= \big\langle\sum_{k=1}^n \langle c_i,b_k\rangle b_k,\sum_{\ell=1}^n \langle c_j,b_\ell \rangle b_\ell\big\rangle\\
    &= \big\langle\sum_{k=1}^n m_{ki} b_k,\sum_{\ell=1}^n m_{\ell j} b_\ell\big\rangle\\
    &= \sum_{k=1}^n\sum_{\ell=1}^nm_{ki}m_{\ell j}\langle b_k,b_\ell\rangle\\
    &= \sum_{k=1}^n\sum_{\ell=1}^n m_{ki}m_{\ell j} \delta_{k \ell}\\
    &= \sum_{k=1}^n m_{ki}m_{kj}\\
    &= \left(M^\top M\right)_{ij}\\
    &= \delta_{ij}
    \end{aligned}
    $$

    On en déduit que $\ds \mathcal C$ est une famille  orthonormée. Ainsi, l'image de toute famille orthonormée est orthonormée et donc $\ds u$ est une isométrie vectorielle. 

!!! definition "Groupe orthogonal"
    L'ensemble des matrices orthogonales de $\ds \mathcal M_n(\mathbb R)$ est appelé *groupe orthogonal* et est noté $\ds O(n)$ ou $\ds O_n(\mathbb R)$.

!!! propriete "Structure du groupe orthogonal"
    Le groupe orthogonal $\ds O(n)$ vérifie les propriétés suivantes : 

    - i) Toute matrice $\ds A$ de $\ds O(n)$ est inversible, d'inverse $\ds A^\bot$ ;  
    - ii) $\ds O(n)$ est stable par produit et inverse ;  
    - iii) Si $\ds M\in O(n)$ alors $\ds \det(M)=\pm 1$ ;  
    - iv) Si $\ds M\in O(n)$, alors $\ds \Sp(M)\subset\{-1,1\}$. 

!!! preuve 
    i) Si $\ds A$ est orthogonale, alors $\ds A^\top A=I_n$. Donc par définition $\ds A$ est inversible et son inverse est $\ds A^\top$. On a donc aussi $\ds AA^\top=I_n$ et on peut en déduire notamment que la famille des lignes de $\ds A$ est orthogonale. 

    ii) Puisque $\ds I_n=AA^\top={(A^\top)}^\top A^\top$. Donc $\ds A^\top$ est orthogonale, c'est à dire $\ds A^{-1}$ est orthogonale. Puis si $\ds A$ et $\ds B$ sont orthogonales, on pose $\ds C=AB$. On a alors $\ds C^\top C = (AB)^\top(AB)=B^\top A^\top AB=B^\top(A^\top A)B=B^\top(I_n)B=B^\top B=I_n$. Donc $\ds C$ est orthogonale c'est à dire $\ds AB$ est orthogonale. 

    iii) Soit $\ds M\in O(n)$, alors $\ds M^\top M=I_n$. En passant au déterminant, on obtient $\ds \det(A^\top A)=1$. Or $\ds \det(A^\top A)=\det(A^\top)\det(A)=\det(A)\det(A)=\det(A)^2$. On a donc $\ds \det(A)^2=1$ c'est à dire $\ds \det(A)=\pm 1$. 

    iv) Soit $\ds M\in O(n)$ et soit $\ds \lambda$ une valeur propre de $\ds M$ et $\ds X$ un vecteur propre non-nul associé. Alors $\ds MX=\lambda X$. On a alors $\ds X^\top M^\top MX=X^\top X=||X||^2$ mais aussi $\ds X^\top M^\top MX = (MX)^\top MX=(\lambda X)^\top (\lambda X)=\lambda^2X^\top X=\lambda^2 ||X||^2$. $\ds X$ étant non nul, on en déduit que $\ds \lambda^2=1$ et donc $\ds \Sp(M)\subset\{-1,1\}$. 


!!! exemple 
    === "Énoncé"
        Soit $\ds M\in O(n)$ de coefficients $\ds m_{ij}$. Montrer que $\ds \left|\sum_{i,j}a_{ij}\right|\le n$.
    === "Corrigé"
        On se place dans $\ds E=\mathcal M_{n,1}(\mathbb R)$. Soit $\ds X=\begin{pmatrix}1\\1\\\vdots\\1\end{pmatrix}$ et $\ds Y=AX$. On a alors pour le produit scalaire canonique, $\ds \langle X,Y\rangle=\sum_{i,j}a_{ij}$. De plus, $\ds ||X||^2=n$ et $\ds ||Y||^2=||AX||^2=||X||^2$ car $\ds A$ est orthogonale. On applique alors l'inégalité de Cauchy Schwarz : 

        $$
        \left|\langle X,Y\rangle\right|\le ||X||||Y||
        $$

        c'est à dire avec les remarques précédentes, $\ds \left|\sum_{i,j}a_{ij}\right|\le n$.

!!! definition "Groupe spécial orthogonal"
    Le sous-ensemble de $\ds O(n)$ composé des matrices de déterminant $\ds 1$ est appelé *groupe spécial orthogonal* et noté $\ds SO(n)$ ou $\ds SO_n(\mathbb R)$. 

!!! propriete "Structure du groupe spécial orthogonal"
    $\ds SO(n)$ est stable par inverse et produit.

!!! preuve
    On sait déjà que $\ds O(n)$ est stable par inverse et produit. Ce que l'on cherche à savoir c'est si le déterminant de l'inverse est bien $\ds 1$ et si le déterminant de $\ds AB$ est bien $\ds 1$. Soient $\ds A$ et $\ds B$ deux matrices de $\ds SO(n)$. On a $\ds \det(A^{-1})=\det (A^\top)=\det(A)=1$ d'où stabilité par inverse, et $\ds \det(AB)=\det(A)\det(B)=1\times 1=1$. d'où stabilité par produit.
 

!!! definition "Orientation, base orthonormée directe"
    Soit $\ds E$ un espace euclidien muni d'une base orthonormée $\ds \mathcal B$. On dit que $\ds E$ est *orienté*. Soit $\ds \mathcal B'$ une autre base orthonormée de $\ds E$. Alors $\ds M=Mat_{\mathcal B} (\mathcal B')$ est une matrice orthogonale. Si $\ds \det M=1$ on dira que c'est une base orthonormée directe. Si $\ds \det M=-1$ on dira que c'est une base orthonormée indirecte.

!!! exemple
    === "Énoncé"
        Soit $\ds E=\mathbb R^3$ muni du produit scalaire canonique et orienté par la base canonique $\ds (e_1,e_2,e_3)$. Montrer que $\ds (e_2,e_3,e_1)$ est orthonormée directe mais pas $\ds (e_1,e_3,e_2)$. 
    === "Corrigé"
        Il suffit de calculer les déterminants correcpondants.

!!! remarque 
    L'orientation d'un espace euclidien dépend d'une base fixée, arbitrairement choisie. Le «sens» d'une base orthonormée dépend donc toujours d'une convention posée arbitrairement. 


### Espace euclidien orienté de dimension 2 ou 3

!!! definition "Produit mixte"
    Soit $\ds E$ un espace euclidien de dimension $\ds 2$ muni d'une base orthonormée directe $\ds \mathcal B$. Soit $\ds (u,v)$ une famille d'éléments de $\ds E$. On appelle *produit mixte* de $\ds u$ et $\ds v$ la quantité :
    
    $$
    [u,v]=\det(Mat_{\mathcal B}(u,v)).
    $$

    Soit $\ds E$ un espace euclidien de dimension $\ds 3$ muni d'une base orthonormée $\ds \mathcal B$. Soit $\ds (u,v,w)$ une famille d'éléments de $\ds E$. On appelle *produit mixte* de $\ds u$, $\ds v$ et $\ds w$ la quantité :
    
    $$
    [u,v,w]=\det(Mat_{\mathcal B}(u,v,w)).
    $$

!!! propriete "indépendance de la base"
    Le produit mixte, en dimension $\ds 2$ ou $\ds 3$, est indépendant de la base orthonormée directe $\ds \mathcal B$ choisie.

!!! preuve
    Le passage de $\ds Mat_{\mathcal B}(u,v,w)$ à $\ds Mat_{\mathcal B'}(u,v,w)$ se fait par la formule de changement de base. Or le déterminant est invariant par similitude donc $\ds \det(Mat_{\mathcal B}(u,v,w))=\det(Mat_{\mathcal B'}(u,v,w))$. 


!!! definition "produit vectoriel"
    Soit $\ds u$ et $\ds v$ deux éléments d'un espace euclidien $\ds E$ de dimension $\ds 3$. Soit $\ds \varphi:x\mapsto [u,v,x]$ : $\ds \varphi$ est une forme linéaire, donc il existe un unique $\ds a$ tel que pour tout $\ds x\in E$, $\ds \varphi(x)=\langle a,x\rangle$. On appelle $\ds a$ le *produit vectoriel de $\ds u$ par $\ds v$* et on note $\ds a=u\wedge v$. 

!!! propriete "Propriétés du produit vectoriel"
    Soient $\ds u$ et $\ds v$ deux éléments d'un espace euclidien de dimension $\ds 3$. Alors :   
    - $\ds u\mapsto u\wedge v$ et $\ds v\mapsto u\wedge v$ sont des endomorphismes de $\ds E$ ;  
    - $\ds u\wedge v=-v\wedge u$ ;  
    - $\ds u\wedge v=0$ si et seulement si $\ds u$ et $\ds v$ sont colinéaires.  
    - Si $\ds u$ et $\ds v$ sont orthogonaux et normés, alors $\ds (u,v,u\wedge v)$ est une base orthonormée directe. 

!!! preuve 
    Soit $\ds v\in E$. Notons $\ds \varphi(u)=u\wedge v$. Soient $\ds u,u'$ deux éléments de $\ds E$, $\ds \lambda,\mu$ deux éléments de $\ds \mathbb R$. On a d'une part, pour tout $\ds x\in E$, par définition du produit vectoriel :

    $$
    \det(\lambda u+\mu u',v,x)=\langle (\lambda u+\mu u')\wedge v,x\rangle,
    $$

    et d'autre part, par linéarité du produit scalaire et du déterminant :

    $$
    \det(\lambda u+\mu u',v,x)= \lambda \det(u,v,x)+\mu\det(u',v,x)=\lambda \langle (u\wedge v),x\rangle +\mu \langle (u'\wedge v),x\rangle=\langle \lambda u\wedge v+\mu u'\wedge v,x\rangle
    $$

    et donc par unicité dans le théorème de représentation, $\ds (\lambda u+\mu u')\wedge v = \lambda u\wedge v+\mu u'\wedge v$.

    Soit maintenant $\ds x\in E$, on a $\ds \det(u,v,x)=\langle u\wedge v,x\rangle$ et $\ds \det(u,v,x)=-\det(v,u,x)=-\langle v\wedge u,x\rangle=\langle -v\wedge u,x\rangle$ et toujours par unicite, $\ds u\wedge v=-v\wedge u$. 

    Enfin $\ds u\wedge v=0$ si et seulement si pour tout $\ds x$, $\ds \langle u\wedge v,x\rangle=0$ c'est à dire si et seulement si pour tout $\ds x$, $\ds \det(u,v,x)=0$ ce qui se traduit pas le fait que $\ds u$ et $\ds v$ sont colinéaires. 

    Enfin, on a $\ds \langle u\wedge v,u\rangle=\det(u,v,u)=0$ donc $\ds u\wedge v$ est orthogonal à $\ds u$. De même, $\ds u\wedge v$ est orthogonal à $\ds v$. Soit $\ds w$ tel que $\ds (u,v,w)$ soit une base orthonormée directe, on a $\ds \det(u,v,w)=1$ donc $\ds \langle u\wedge v,w)\rangle=1$. Or $\ds u\wedge v$ et $\ds w$ sont colinéaires donc $\ds u\wedge v=w$. Ainsi, $\ds (u,v,u\wedge v)$ est une base orthonormée directe.



!!! propriete "expression du produit vectoriel"
    soient $\ds u$ et $\ds v$ deux éléments d'un espace euclidien de dimension $\ds 3$. Soit $\ds \mathcal B=(e_1,e_2,e_3)$ une base orthonormée directe de $\ds E$. On note $\ds \begin{pmatrix} u_1\\u_2\\u_3\end{pmatrix}$ et $\ds \begin{pmatrix}v_1\\v_2\\v_3\end{pmatrix}$ les matrices respectives de $\ds u$ et $\ds v$ dans $\ds \mathcal B$. Alors la matrice de $\ds u\wedge v$ dans la base $\ds \mathcal B$ est $\ds \begin{pmatrix}u_2v_3-u_3v_2\\u_3v_1-u_1v_3\\u_1v_2-u_2v_1\end{pmatrix}$. 

!!! preuve
    Soient $\ds u$ et $\ds v$ deux vecteurs d'un espace euclidien de dimension $\ds 3$ muni d'un produit scalaire et d'une base orthonormée $\ds \mathcal B=(e_1,e_2,e_3)$. On note $\ds \begin{pmatrix} u_1\\u_2\\u_3\end{pmatrix}$ et $\ds \begin{pmatrix}v_1\\v_2\\v_3\end{pmatrix}$ les matrices respectives de $\ds u$ et $\ds v$ dans $\ds \mathcal B$. Soit $\ds w=u\wedge v$ de coorodonnées $\ds \begin{pmatrix}w_1\\w_2\\w_3\end{pmatrix}$. On sait que, par exemple, $\ds w_1=\langle u\wedge v,e_1\rangle$, c'est à dire $\ds w_1=\det(u,v,e_1)$ et par développement par rapport à la dernière colonne dans le déterminant, on obtient $\ds w_1=u_2v_3-u_3v_2$. Et on fait de même avec $\ds e_2$ et $\ds e_3$ pour les deux autres coorodonnées. 


    
### Isométries vectorielles du plan

Intéressons nous maintenant plus particulièrement aux isométries d'un plan euclidien orienté.

!!! propriete "Groupe orthogonal $\ds O(2)$"
    Soit $\ds A$ une matrice de $\ds O(2)$. Alors il existe $\ds \theta$ tel que $\ds A=\begin{pmatrix}\cos\theta & -\sin\theta\\\sin\theta & \cos\theta\end{pmatrix}$ ($\det A=1$) ou $\ds A=\begin{pmatrix}\cos\theta & \sin\theta\\\sin\theta & -\cos\theta\end{pmatrix}$ ($\det A=-1$). 

!!! preuve
    Soit $\ds A$ une matrice de $\ds O(2)$. Notons $\ds C_1$ et $\ds C_2$ ses deux colonnes. $\ds A$ étant orthogonale, $\ds C_1$ est de norme $\ds 1$ et il existe donc $\ds \theta\in[0,2\pi[$ tel que $\ds C_1=\begin{pmatrix}\cos\theta\\\sin\theta\end{pmatrix}$. De plus $\ds C_2$ est orthogonale à $\ds C_1$, donc proportionnelle à $\ds \begin{pmatrix}-\sin\theta\\\cos\theta\end{pmatrix}$. Mais $\ds C_2$ est de norme 1 donc il n'y a que deux possibilités : $\ds C_2=\begin{pmatrix} -\sin\theta\\\cos\theta\end{pmatrix}$, soit $\ds C_2=\begin{pmatrix}\sin\theta\\-\cos\theta\end{pmatrix}$. Le premier cas mène bien à une matrice orthogonale, de déterminant $\ds 1$, le second vers une matrice orthogonale de déterminant $\ds -1$. 


!!! definition "rotations"
    Une matrice de $\ds SO(2)$ est donc de la forme $\ds \begin{pmatrix}\cos\theta & -\sin\theta\\\sin\theta & \cos\theta\end{pmatrix}$. Une telle matrice est appelée matrice de rotation d'angle $\ds \theta$.

    L'endomorphisme linéaire canoniquement associé à une telle matrice est appelé *rotation vectorielle*. 

!!! definition "angle orienté entre deux vecteurs"
    Soit $\ds u$ et $\ds v$ deux vecteurs normés de $\ds E$. Soient $\ds u'$ et $\ds v'$ tels que $\ds (u,u')$ et $\ds (v,v')$  soient deux bases orthonormées directes. La matrice de passage de $\ds (u,u')$ à $\ds (v,v')$ est donc une matrice de rotation, la valeur de $\ds \theta$ associée est une *mesure de l'angle orienté* entre $\ds u$ et $\ds v$. 

!!! propriete "Commutativité de $\ds SO(2)$"
    Le groupe spécial orthogonal en dimension 2 est commutatif.

!!! remarque  
    Cela traduit le fait que deux rotations de centre $\ds O$ commutent toujours en elles : une rotation d'angle $\ds \theta_1$ suivie d'une rotation d'angle $\ds \theta_2$ donne une rotation d'angle $\ds \theta_1+\theta_2$ tout comme une rotation d'angle $\ds \theta_2$ suivie d'une rotation d'angle $\ds \theta_1$. 

!!! preuve
    Soit deux matrices de $\ds SO(2)$ $\ds A$ et $\ds B$. On pose $\ds A=\begin{pmatrix} \cos\theta & -\sin\theta\\\sin\theta & \cos\theta\end{pmatrix}$ et $\ds B=\begin{pmatrix}\cos\phi & -\sin\phi\\\sin\phi & \cos\phi\end{pmatrix}$. Le produit $\ds AB$ donne : 

    $$
    \begin{aligned}
    AB &= \begin{pmatrix} \cos\theta & -\sin\theta\\\sin\theta & \cos\theta\end{pmatrix}\begin{pmatrix}\cos\phi & -\sin\phi\\\sin\phi & \cos\phi\end{pmatrix}\\
    &= \begin{pmatrix} \cos\theta\cos\phi-\sin\theta\sin\phi & -\cos\theta\sin\phi-\sin\theta\cos\phi \\ \sin\theta\cos\phi + \cos\theta\sin\phi & -\sin\theta\sin\phi +\cos\theta\cos\phi\end{pmatrix}\\
    &= \begin{pmatrix} \cos(\theta+\phi) & -\sin(\theta+\phi) \\ \sin(\theta+\phi) & \cos(\theta+\phi)\end{pmatrix}
    \end{aligned}
    $$

    Le rôle de $\ds \theta$ et $\ds \phi$ est clairement symétrique, on s'épargne donc les calculs dans l'autre sens et on obtient $\ds AB=BA$. 


!!! propriete "Réflexion"
    Soit $\ds A\in O(2)$ telle que $\ds \det A=-1$. Alors $\ds A$ est la matrice d'une symétrie orthogonale. 

!!! preuve
    Puisque $\ds A$ est dans $\ds O(2)$ avec $\ds \det A=-1$, on peut écrire $\ds A$ sous la forme $\ds \begin{pmatrix} \cos\theta & \sin\theta \\ \sin\theta & -\cos\theta\end{pmatrix}$ et alors :

    $$
    A^2=\begin{pmatrix} \cos\theta & \sin\theta \\ \sin\theta & -\cos\theta\end{pmatrix}\begin{pmatrix} \cos\theta & \sin\theta \\ \sin\theta & -\cos\theta\end{pmatrix}=\begin{pmatrix} \cos^2\theta+\sin^2\theta & \cos\theta\sin\theta-\sin\theta\cos\theta \\ \sin\theta\cos\theta-\cos\theta\sin\theta & \sin^2\theta+\cos^2\theta\end{pmatrix}=I_2
    $$

    On en déduit que $\ds A$ est la matrice d'une symétrie. Notons $\ds s$ cette symétrie. Sa matrice dans une base orthogonale étant orthogonale, c'est une isométrie vectorielle. On cherche à montrer que la direction de symétrie  ($\ker (s+id)$) et l'espace de symétrie ($\ker (s-id)$) sont orthogonaux. Soit $\ds x\in\ker(s+id)$ et $\ds y\in\ker(s-id)$, on a alors $\ds x=-s(x)$ et $\ds y=s(y)$. On a donc $\ds \langle x,y\rangle = \langle -s(x),s(y)\rangle = -\langle s(x),s(y)\rangle = -\langle x,y\rangle$. On en déduit que $\ds \ker(s+id)\bot\ker(s-id)$ et donc la symétrie est orthogonale. Comme on est en dimension $\ds 2$, si ce n'est pas l'identité, c'est une réflexion.  


!!! propriete "Classification des isométries vectorielles en dimension $\ds 2$"
    Une isométrie vectorielle en dimension $\ds 2$ est soit une rotation, soit une symétrie/réflexion orthogonale.

!!! preuve
    C'est simplement une compilation des résultats vus précédemment.

!!! exemple 
    === "Énoncé"
        Soit $\ds A=\frac 1{\sqrt2}\begin{pmatrix} 1 & 1 \\ 1 & -1\end{pmatrix}$. Montrer que $\ds A$ est une matrice de $\ds O(2)$ et que c'est la matrice d'une réflexion orthogonale. Soit $\ds s$ la réflexion canoniquement associée à $\ds A$, déterminer l'axe de symétrie.
    === "Corrigé"
        On a $\ds A^\top A = I_2$ et $\ds \det A=-1$ : c'est bien une matrice de réflexion. Pour trouver l'axe de symétrie, on chercher $\ds X=\begin{pmatrix} x_1\\x_2\end{pmatrix} tel que $\ds AX=X$ ce qui donne le système :

        $$
        \begin{cases} x_1+x_2=\sqrt 2x_1\\ x_1-x_2=\sqrt2 x_2\end{cases}
        $$

        dont les solutions sont les vecteurs de la forme $\ds \alpha\begin{pmatrix}1+\sqrt 2\\1\end{pmatrix}$. 




### Isométries vectorielles d'un espace euclidien de dimension 3

!!! propriete "Valeurs propres de $\ds SO(3)$"
    Soit $\ds M\in SO(3)$. Alors $\ds M$ admet $\ds 1$ comme valeur propre.

!!! preuve
    $\ds M$ est une matrice $\ds 3\times 3$ donc son polynôme caractéristique est réel et de degré $\ds 3$ : il admet une racine réelle au moins. $\ds M$ étant dans $\ds SO(3)$ on sait que ses valeur propres réelles sont $\ds +1$ et/ou $\ds -1$. Les deux autres valeurs propres de $\ds M$ sont soit des $\ds 1$ et $\ds -1$ soit deux complexes conjugués. la distribution de valeurs propres est donc l'une parmi celles-ci : $\ds (1,1,1)$, $\ds (1,-1,-1)$, $\ds (1,-1,1)$,$(1,\lambda,\bar\lambda)$, $\ds (-1,-1,-1)$ ou $\ds (-1,\lambda,\bar\lambda)$. On sait que le déterminant est le produit des valeurs propres et ici le déterminant est positif : les seuls cas possibles sont donc $\ds (1,1,1)$, $\ds (1,-1,-1)$, $\ds (1,\lambda,\bar\lambda)$ : dans tout ces cas, $\ds 1$ est valeur propre. 

!!! propriete "Description de $\ds SO(3)$"
    On note $\ds R_\theta=\begin{pmatrix} 1 & 0 & 0\\ 0 & \cos\theta & -\sin\theta \\ 0 & \sin\theta & \cos\theta\end{pmatrix}$. Toute matrice $\ds A$ de $\ds SO(3)$ est semblable à une matrice $\ds R_\theta$. 

!!! preuve 
    Si $\ds A$  est l'identité, c'est réglé avec $\ds \theta=0$. Sinon, considérons l'endomorphisme $\ds u$ de $\ds \mathbb R^3$ canoniquement associé à $\ds A\in SO(3)$. $\ds u$ admet $\ds 1$ comme valeur propre de multiplicité $\ds 1$ : Notons $\ds F=E_1(u)$, alors $\ds F$ est stable par $\ds u$. La matrice de $\ds u$ dans une base orthonormée adaptée à $\ds F$ est de la forme ($F$ et $\ds F^\bot$ sont stables par $\ds u$) : $\ds B=\begin{pmatrix} 1 & 0 \\ 0 & A'\end{pmatrix}$ avec $\ds A'$ nue matrice $\ds 2\times 2$. Évidemment $\ds B$ est orthogonale et $\ds I_3=B^\top B=\begin{pmatrix} 1 & 0 \\ 0 &  (A')^\top A'\end{pmatrix}$. On en déduit que $\ds A'\in O(2)$. De plus $\ds \det(B)=\det(A)=1=1\times \det (A')$. Donc $\ds \det(A')=1$ et on en déduit $\ds A'\in SO(2)$. Ainsi on peut écrire $\ds A'=\begin{pmatrix}\cos\theta & -\sin\theta\\\sin\theta & \cos\theta\end{pmatrix}$. On en déduit que $\ds B=R_\theta$. Or $\ds B$ est une matrice de $\ds u$ dans une base orthonormée donc $\ds A$ est bien semblable à une matrice de la forme $\ds R_\theta$.  

!!! definition "Matrice de rotation"
    On appelle $\ds R_\theta$ une *matrice de rotation*.

!!! definition "Rotation vectorielle d'un espace euclidien orienté de dimension $\ds 3$"
    Soit $\ds u$ une isométrie vectorielle de $\ds E$. Si $\ds \det(u)=1$ alors il existe une base orthonormée de $\ds u$ telle que la matrice de $\ds u$ dans cette base soit une matrice $\ds R_\theta$. On appelle alors $\ds u$ *rotation vectorielle*. Elle est définie par son axe qui engendre $\ds E_1(u)$ et par un angle $\ds \theta$ qui vérifie $\ds \tr(u)=1+2\cos\theta$. 

!!! exemple
    === "Énoncé"
        Soit $\ds M=\frac 13\begin{pmatrix}-1 & 2 & 2 \\ 2 & -1 & 2\\ 2 & 2 & -1\end{pmatrix}$. Montrer que $\ds M$ est la matrice d'une rotation dont on précisera les éléments caractéristiques (angle et axe).
    === "Corrigé"
        On calcule facilement $\ds M^\top M=I_3$ et $\ds \det M=1$. Donc $\ds M$ est la matrice d'une rotation d'axe $\ds u$ et d'angle $\ds \theta$. On a $\ds \tr(M)=1+2\cos\theta$ donc ici $\ds \cos\theta=-1$ : $\ds \theta=\pi$ convient. Cherchons l'axe en résolvant $\ds AX=X$ : 

        $$
        \begin{aligned}
        AX=X&\Leftrightarrow \begin{cases} -x+2y+2z=3x\\2x-y+2z=3y\\ 2x+2y-z=3z\end{cases}\\
        &\Leftrightarrow\begin{cases}-4x+2y+2z=0\\2x-4y+2z=0\\2x+2y-4z=0\end{cases}\\
        &\Leftrightarrow\begin{cases}x-2y+z=0\\-2x+y+z=0\\x+y-2z=0\end{cases}\\
        &\Leftrightarrow\begin{cases}x-2y+z=0\\-3y+3z=0\\3y-3z=0\end{cases}\\
        &\Leftrightarrow\begin{cases}x-2y+z=0\\y=z\end{cases}\\
        &\Leftrightarrow x=y=z
        \end{aligned}
        $$

        Le vecteur $\ds \begin{pmatrix} 1\\1\\1\end{pmatrix}$ est donc un vecteur directeur de l'axe de rotation.

!!! warning "Attention"
    $\ds SO(3)$, contrairement à $\ds SO(2)$, n'est pas commutatif.



### Réduction des endomorphismes autoadjoints et des matrices symétriques réelles

!!! propriete "Matrice d'un endomorphisme dans une base orthonormée"
    Soit $\ds u\in\mathcal L(E)$ où $\ds E$ est un espace euclidien muni d'une base orthonormée $\ds \mathcal B=(b_1,\dots,b_n)$. La matrice de $\ds u$ dans la base $\ds \mathcal B$ est la matrice $\ds M\in\mathcal M_n(\mathbb R)$ définie par $\ds m_{ij}=\langle u(e_j),e_i\rangle$. 



!!! definition "endomorphisme autoadjoint"
    Soit $\ds u\in\mathcal L(E)$ où $\ds E$ est un espace euclidien. $\ds u$ est un *automorphisme autoadjoint* si et seulement si, pour tout $\ds (x,y)\in E^2$, $\ds \langle u(x),y\rangle=\langle x,u(y)\rangle$. 
    On note $\ds \mathcal S(E)$ l'ensemble des automorphismes autoadjoints.

!!! propriete "Caractérisation matricielle"
    Soit $\ds u\in\mathcal L(E)$ où $\ds E$ est un espace euclidien.  Alors $\ds u$ est un endomorphisme autoadjoint si et seulement si la matrice de $\ds u$ dans toute base orthonormée est symétrique. 

!!! preuve
    Cela découle de la propriété précédente. En effet si $\ds M$ est la matrice de $\ds u$ dans une base orthonormée $\ds (b_1,\dots,b_n)$, alors $\ds M_{ij}=\langle u(b_j),b_i\rangle=\langle b_j,u(b_i)\rangle=M_{ji}$. Donc $\ds M$ est symétrique. Réciproquement, si $\ds M$ est symétrique, alors pour tout couple $\ds i,j$, $\ds \langle u(b_j),b_i\rangle =\langle b_j,u(b_i)\rangle$. Soit $\ds x$ et $\ds y$ deux éléments de $\ds E$ avec $\ds x=\sum_{i=1}^nx_ib_i$ et $\ds y=\sum_{j=1}^ny_jb_j$. Alors :
    
    $$  
    \langle u(x),y\rangle=\sum_{i=1}^n\sum_{j=1}^nx_iy_j\langle u(b_i),b_j\rangle=\sum_{i=1}^n\sum_{j=1}^nx_iy_j\langle b_i,u(b_j)\rangle=\langle x,u(y)\rangle.
    $$

    Ainsi $\ds u$ est bien autoadjoint.


!!! propriete "Structure du noyau et de l'image"
    Soit $\ds E$ un espace euclidien et $\ds u\in\mathcal S(E)$. Alors $\ds \ker u$ et $\ds \im u$ sont supplémentaires orthogonaux.

!!! preuve 
    Soit $\ds x\in \ker u$ et $\ds y\in\im u$. Alors $\ds u(x)=0$ et on peut écrire $\ds y=u(z)$ avec $\ds z\in E$. On a alors $\ds \langle x,y\rangle =\langle x,u(z)\rangle=\langle u(x),z\rangle=\langle 0,z\rangle=0$. Donc $\ds \ker u$ et $\ds \im u$ sont orthogonaux. Leur intersection est donc réduite à $\ds 0$ et par le théorème du rang, $\ds \dim(E)=\dim\ker u+\dim\im u$. Donc $\ds \ker u$ et $\ds \im u$ sont supplémentaires orthogonaux. 

!!! exemple 
    === "Énoncé"
        Montrer que $\ds p$ est un projecteur orthogonal de $\ds E$ si et seulement si $\ds p$ est autoadjoint et $\ds p\circ p=p$. 
    === "Corrigé"
        Si $\ds p$ tel que $\ds p\circ p=p$ alors c'est un projecteur. S'il est autoadjoint, alors son image et son noyau sont orthogonaux : $\ds p$ est un projecteur orthogonal. 

        Réciproquement, si $\ds p$ est un projecteur orthogonal, alors $\ds p\circ p=p$. De plus, son image et son noyaux sont orthogonaux. Soit $\ds x$ et $\ds y$ dans $\ds E$, on a :

        $$
        \begin{aligned}
        \langle p(x),y\rangle &= \langle p(x),p(y)+(y-p(y))\rangle\\
        &=\langle p(x),p(y)\rangle + \langle \underset{\in \im p}{\underbrace{p(x)}},\underset{\in\ker p}{\underbrace{y-p(y)}}\\
        &=\langle p(x),p(y)\rangle + 0
        \end{aligned}
        $$

        De la même manière, \langle x,p(y)\rangle = \langle p(x),p(y)\rangle$, donc $\ds p$ est bien autoadjoint. 
        

!!! propriete "Valeurs propres"
    Soit $\ds u\in\mathcal S(E)$, alors toutes les valeurs propres de $\ds u$ sont réelles. 

!!! preuve
    Soit $\ds A$ la matrice associée, $\ds A$ est symétrique. Soit $\ds \lambda$ une valeur propre de $\ds A$ éventuellement complexe et $\ds X$ un vecteur propre, éventuellement à coefficients complexes. $\ds A$ est à coefficients réelles. On a alors : $\ds {\bar X}^\top A X = \lambda {\bar X}^\top X$ et $\ds {\bar X}^\top AX=\overline{(A X)}^\top X = \bar\lambda {\bar X}^\top X$. $\ds X$ étant non nul, on en déduit que $\ds \lambda =\bar\lambda$ et donc $\ds \lambda\in\mathbb R$. 

    Ainsi toutes les valeurs propres sont bien réelles. 


!!! propriete "Sous-espaces propres"
    Soient $\ds \lambda$ et $\ds \mu$ deux valeurs propres réelles distinctes de $\ds u\in\mathcal S(E)$. Alors $\ds E_\lambda(u)$ et $\ds E_\mu(u)$ sont orthogonaux.

!!! preuve
    En effet, soit $\ds x$ un élément non nul de $\ds E_\lambda(u)$ et $\ds y$ un élément non nul de $\ds E_\mu(u)$. Alors $\ds \lambda\langle x,y\rangle =\langle \lambda x,y\rangle=\langle u(x),y\rangle =\langle x,u(y)\rangle=\mu\langle x,y\rangle$. On en déduit que $\ds (\lambda-\mu)\langle x,y\rangle=0$ et puisque $\ds \lambda$ et $\ds \mu$ sont distincts, $\ds \langle x,y\rangle=0$ : $\ds E_\lambda(u)$ et $\ds E_\mu(u)$ sont bien orthogonaux.

!!! theoreme "Théorème spectral"
    Tout endomorphisme autoadjoint est diagonalisable dans une base orthonormée.

!!! theoreme "Théorème spectral version matricielle"
    Soit $\ds A$ une matrice symétrique. Il existe $\ds P$ une matrice orthogonale telle que $\ds P^\top A P$ est diagonale. 

!!! preuve
    La preuve n'est pas au programme mais peut se faire par récurrence sur la dimension. 


!!! exemple
    === "Énoncé"
        Soit $\ds A=\begin{pmatrix} 1 & -1 & 3 & 2 \\ -1 & 6 & 7 & 4 \\ 3 & 7 & -8 & 3 \\ 2 & 4 & 3 & 2\end{pmatrix}$. Montrer que $\ds A$ est diagonalisable. 
    === "Corrigé"
        $\ds A$ est symétrique, donc diagonalisable.

!!! definition "Caractère positif"
    Soit $\ds u\in\mathcal S(E)$. $\ds u$ est positive si et seulement si toutes ses valeurs propres sont positives. On note $\ds u\in\mathcal S^+(E)$. 

    $\ds u$ est défini positif si et seulement si toutes ses valeurs propres sont strictement positives. On note $\ds u\in\mathcal S^{++}(E)$.

    On a les mêmes définitions pour les matrices, avec pour notations $\ds \mathcal S_n^+(\mathbb R)$ et $\ds \mathcal S_n^{++}(\mathbb R)$. 

!!! propriete "Caractérisation de $\ds \mathcal S^+(E)$"
    Soit $\ds u\in\mathcal S(E)$.

    $\ds u\in\mathcal S^+(E)$ si et seulement si pour tout $\ds x\in E$, $\ds \langle x,u(x)\rangle \ge 0$. 

    $\ds u\in\mathcal S^{++}(E)$ si et seulement si pour tout $\ds x\in E^*$, $\ds \langle x,u(x)\rangle > 0$. 

!!! preuve
    On se concentre sur la première propriété. 

    Supposons que pour tout $\ds x\in E$, $\ds \langle x,u(x)\rangle \ge 0$. Soit $\ds \lambda$ une valeur propre et soit $\ds x$ un vecteur propre associé. On a $\ds \langle x,u(x)\rangle\ge 0$. Or $\ds \langle u(x),x\rangle=\lambda \langle x,x\rangle = \lambda||x||^2$. On en déduit que $\ds \lambda\ge 0$. 

    Réciproquement, si $\ds u\in\mathcal S^+(E)$ alors toutes les valeurs propres sont positives et il existe une base de vecteurs propres $\ds (b_1,\dots,b_n)$ associés aux valeurs propres respectives $\ds \lambda_1,\dots,\lambda_n$ toutes positives. Soit $\ds x\in E$, posons $\ds x=x_1b_1+\dots+x_nb_n$. On a alors :

    $$
    \begin{aligned}
    \langle u(x),x\rangle&=\langle \sum_{i=1}^n x_iu(e_i),\sum_{j=1}^n x_je_j\rangle\\
    &= \sum_{i=1}^n\sum_{j=1}^n x_i x_j\langle u(e_i),e_j\rangle\\
    &= \sum_{i=1}^n\sum_{j=1}^n x_i x_j\langle \lambda_ie_i,e_j\rangle\\
    &= \sum_{i=1}^n\sum_{j=1}^n x_i x_j\lambda_i \delta_{ij}\\
    &= \sum_{i=1}^n \lambda_ix_i^2
    \end{aligned}
    $$

    et puisque tous les  $\ds \lambda_i$ sont positifs, cette somme est positive. Ainsi $\ds \langle u(x),x\rangle\ge 0$.
    
!!! propriete "Caractérisation de $\ds \mathcal S_n^+(\mathbb R)$"
    Soit $\ds A\in\mathcal S_n(\mathbb R)$.

    $\ds A\in\mathcal S_n^+(\mathbb R)$ si et seulement si pour tout $\ds X\in \mathcal M_{n,1}(\mathbb R)$, $\ds X^\top AX \ge 0$. 

    $\ds A\in\mathcal S_n^{++}(\mathbb R)$ si et seulement si pour tout $\ds X\in \mathcal M_{n,1}(\mathbb R)^*$, $\ds X^\top AX > 0$. 

!!! exemple
    === "Énoncé"
        Soit $\ds A\in\mathcal S_n^{++}(\mathbb R)$. Soit $\ds \varphi$ l'application définie sur $\ds \mathcal M_{n,1}(\mathbb R)^2$ par $\ds \varphi(X,Y)=X^\top A Y$. Montrer que $\ds \varphi$ est un produit scalaire.
    === "Corrigé"
        Il n'y a aucun problème pour la linéarité. Pour la symétrie, notons simplement que $\ds \varphi(X,Y)$ est un réel donc $\ds \varphi(X,Y)=\varphi(X,Y)^\top$. Or $\ds \varphi(X,Y)^\top = \left(X^\top AY\right)^\top = Y^\top A^\top X=Y^\top A X$ car $\ds A$ est symétrique. Ainsi donc, $\ds \varphi(X,Y)=\varphi(Y,X)$. 

        Enfin, $\ds A$ est définie positive donc pour tout vecteur colonne $\ds X$ non nul, on a $\ds \varphi(X,X)=\langle X,AX\rangle>0$ et donc égalité si et seulement si $\ds X=0$. On a donc bien ici un produit scalaire.


## Exercices

### Issus de la banque CCINP
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-063.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-066.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-068.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-076.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-077.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-078.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-079.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-080.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-081.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-082.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-092.md" %}


### Annales d'oraux
{% include-markdown "../../../exercicesRMS/2022/RMS2022-694.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-696.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-697.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-698.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-699.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-700.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-701.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-702.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-703.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1096.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1097.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1098.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1359.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1360.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1361.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1362.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1363.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1364.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1365.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1366.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1367.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1368.md" %}

### Centrale Python
{% include-markdown "../../../exercicesCentralePython/RMS2022-1099.md"   rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1166.md"   rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1179.md"   rewrite-relative-urls=false %}




