# Séries entières

## Rayon de convergence

!!! definition "Série entière"
	Soit $(a_n)_{n\in\mathbb N}$ une suite de nombres réels ou complexes. On appelle *série entière* de la variable $z\in \mathbb C$ la série de fonctions $\ds\sum u_n$ avec pour $n\in\mathbb N$ et $z\in\mathbb C$, $u_n(z)=a_nz^n$.

!!! propriete "Combinaison linéaire et produit"
	L'ensemble des séries entières sur $\mathbb K$ est un $\K$-espace vectoriel. Il est de plus stable par produit de Cauchy : $\ds\sum a_nz^n\times\sum b_nz^n=\sum c_nz^n$ et $\ds c_n=\sum_{p=0}^na_pb_{n-p}$.

!!! theoreme "Lemme d'Abel"
	si  la suite $\left(a_n z_0^n\right)$ est bornée alors, pour tout nombre complexe $z$ tel que $\vert z \vert <\vert z_0\vert$, la série $\displaystyle \sum a_n z^n$ est absolument convergente.

!!! preuve
	Supposons donc que la suite $(a_nz_0^n)$ est bornée. Soit $z\in\mathbb C$ tel que $|z|<|z_0|$ et $M$ un majorant de la suite $|a_nz_0^n|$. On a alors

	$$
	\begin{aligned}
	|a_nz^n|&=\left|a_nz^n\frac{z_0^n}{z_0^n}\right|\\
	&=|a_nz_0^n|\times {\left|\frac{z}{z_0}\right|}^n\\
	&=M\times {\left|\frac{z}{z_0}\right|}^n
	\end{aligned}
	$$

	Or $\ds {\left(\frac{z}{z_0}\right)}^n$ est le terme général d'une série géométrique convergente car $\ds \frac{z}{z_0}<1$. On en déduit par théorèmes de comparaison que la série de terme général $a_nz^n$ est absolument convergente (donc convergente).

!!! definition "??"
	Soit $\sum a_nz^n$ une série entière. On définit $R\left(\sum a_nz^n\right)$ par :

	$$
	R\left(\sum a_nz^n\right) = \sup\left\{r\in\mathbb R^+\vert (a_nr^n)_{n\in\mathbb N}\text{ est bornée}\right\}
	$$

	$R\left(\sum a_nz^n\right)$ peut être égal à $+\infty$.

S'il n'y a pas d'ambiguité, on notera simplement $R$.

!!! exemple
	=== "Énoncé"
		Posons pour tout $n\in\mathbb N$, $a_n=1$. La série entière $\ds\sum a_nz^n$ est telle que $R=1$.
	=== "Corrigé"
		En effet, si $r<1$, alors $a_nr^n=r^n<1$ donc la suite $(a_nr^n)$ est bornée. Si $r>1$, alors $r^n$ diverge vers $+\infty$. On en déduit donc que $R=1$.

!!! exemple
	=== "Énoncé"
		On note $a_n$ la $n$-ième décimale du nombre $\pi$. Que vaut $R$ pour la série entière $\sum a_nx^n$ ?
	=== "Corrigé"
		On a pour tout $n$, $|a_n|\le 9$ et donc si $|x|\le 1$, alors $a_nx^n$ est bornée. On en déduit que $R\ge 1$. Par ailleurs, $a_n\not\to 0$ ($\pi$ a une infinité de décimales) donc si $x>1$, alors $a_nx^n$ diverge vers $+\infty$. On en déduit qu'il y a divergence grossière de la série pour $x>1$. Ainsi $R\le 1$. Finalement, $R=1$.

!!! exemple
	=== "Énoncé"
		Soit $(a_n)$ une suite de réels qui ne tend pas vers $0$. Montrer que $R(\sum a_nx^n)\le 1$.
	=== "Corrigé"
		$a_n\not\to 0$ donc il existe $\varepsilon>0$ tel que pour tout $n_0\in\mathbb N$, il existe $n\ge n_0$ tel que $|a_n|>\varepsilon$. En prenant un tel $\varepsilon$, on peut donc extraire de $(a_n)$ une suite $(a_{\varphi(n)})$ telle que $\forall n\in\mathbb N$, $|a_{\varphi(n)}|>\varepsilon$. Pour $x>1$, la suite $|a_{\varphi(n)}x^{\varphi(n)}|$ diverge alors vers $+\infty$ et n'est donc pas bornée. Celle-ci est extraite de la suite $(a_nx^n)$ donc la suite $(a_nx^n)$ n'est pas bornée si $x>1$ : on en déduit que $R\le 1$.  

!!! exemple
	=== "Énoncé"
		On a  $R\left(\sum a_nz^n\right)=R\left(\sum na_nz^n\right)$.
	=== "Corrigé"
		Notons $R_1$ et $R_2$ les R respectifs de $\sum a_nz^n$ et $\sum na_nz^n$.
		Soit $z\in\mathbb C$ tel que $|z|< R_2$. Alors $na_nz^n$ est bornée : posons $M\in\mathbb R$ tel que pour tout $n$, $|na_nz^n|\le M$. On a alors $|a_nz^n|\le \frac Mn\to 0$. On en déduit que $a_nz^n$ est bornée : $R_2\le R_1$.  
		Soit maintenant $z$ tel que $|z|< R_1$. Soit $m$ tel que $|z|< m< R_1$. On a alors $|na_nz^n|=|na_nm^n\left(\frac{z}{m}\right)^n|=|a_nm^n||n\left(\frac{z}{m}\right)^n|$. Or $a_nm^n$ est bornée car $m<R_1$ et par croissances comparées, $|n\left(\frac{z}{m}\right)^n|\to 0$ car $|z|<m$. On en déduit que $na_nz^n\to 0$ et est donc bornée : $R_1\le R_2$.   
		On en déduit donc que $R_1=R_2$.


!!! propriete "Convergence d'une série entière, rayon de convergence"
	La série $\ds\sum a_n z^n$ converge absolument si $|z|<R\left(\sum a_nz^n\right)$, et elle diverge grossièrement si $|z|>R\left(\sum a_nz^n\right)$. Pour $z\in\mathbb C$ tel que $|z|<R\left(\sum a_nz^n\right)$, on appellera $S:z\mapsto \sum_{n=0}^{+\infty}a_nz^n$ *la somme* de la série entière. La quantité $R\left(\sum a_nz^n\right)$ est appelée le *rayon de convergence* de la série entière.

!!! preuve
	Soit $\ds\sum a_nz^n$ une série entière de rayon de convergence $R$. Soit $z\in\mathbb C$ tel que $|z|<R$. Par caractérisation de la borne sup, il existe $r\in\mathbb R$ tel que la suite $(a_n r^n)$ soit bornée et $|z|< r$. D'après le lemme d'Abel, la série $\ds\sum a_nz^n$ converge absolument.

	Soit maintenant $z\in\mathbb C$ tel que $|z|>R$. Alors par définition de $R$, $a_n|z|^n$ n'est pas bornée. On en déduit que $a_nz^n\underset{n\to+\infty}{\not\longrightarrow}0$ et la série $\ds\sum a_nz^n$ diverge grossièrement.

!!! exemple
	=== "Énoncé"
		Quel est le rayon de convergence et la somme de la série entière $\sum x^n$ ?
	=== "Corrigé"
		Cela fait évidemment penser à une série géométrique. On montre facilement que le rayon de convergence est $1$ (il y a convergence absolue si $|x|<1$ et divergence grossière si $|x|>1$). De plus on sait que pour tout $x$ tel que $|x|<1$, $\sum_{n=0}^{+\infty}x^n=\frac 1{1-x}$. La fonction $x\mapsto \frac1{1-x}$ est donc la somme de la série entière $\sum x^n$ sur $]-1,1[$.

!!! exemple
	=== "Énoncé"
		Soit $\alpha\in\mathbb R$. On a $\ds R(\sum n^\alpha x^n)=1$
	=== "Corrigé"
		En effet, par croissances comparées, si $x<1$ alors $n^\alpha x^n\xrightarrow[n\to +\infty]{}0$ donc la suite $(n^\alpha x^n)$ est bornée. De plus, si $x>1$ alors toujours par croissances comparées, $n^\alpha x^n$ diverge vers $+\infty$ donc la suite $(n^\alpha x^n)$ n'est pas bornée. On en déduit en effet que $\ds R(\sum n^\alpha x^n)=1$.

!!! exemple
	=== "Énoncé"
		Quel est le rayon de convergence de la série entière $\sum\frac{x^n}{n}$.
	=== "Corrigé"
		Soit $x<1$, alors $\frac {x^n}n\to 0$ donc la suite $\frac {x^n}{n}$ est bornée. On en déduit $R\ge 1$. Par ailleurs par croissances comparées, si $x>1$ alors $\frac {x^n}n\to+\infty$. On en déduit que $R\le 1$. Donc $R=1$.

!!! exemple
	=== "Énoncé"
		Quel est le rayon de convergence de la série entière $\sum \frac{x^n}{2^n}$ ?
	=== "Corrigé"
		Soit $x$ réel, la série $\sum\frac{x^n}{2^n}$ est une série géométrique de raison $\frac x2$. Elle converge donc si et seulement si $|\frac x2|<1$ c'est à dire $|x|<2$. Le rayon de convergence est donc $2$.   

!!! exemple
	=== "Énoncé"
		Soit $\alpha\in\mathbb R^{+}_*$ et $a_n=\frac {\cos(\alpha n)}{n}$. Quel est le rayon de convergence de $\sum a_nx^n$ ?
	=== "Corrigé"
		Si $x<1$, alors $|a_nx^n|\le \frac 1n$ et donc $a_nx^n$ est bornée. On en déduit que $R\ge 1$. Si $x>1$, on sait que $\cos(n\alpha)$ ne converge pas vers $0$. Ainsi il existe un réel strictement positif $\varepsilon$ tel qu'on puisse extraire de la suite $(\cos(n\alpha))$ une suite telle que $|\cos(\varphi(n)\alpha)|\ge \varepsilon$. On a alors pour $x>1$, $|a_{\varphi(n)}x^{\varphi(n)}|\ge \varepsilon \frac{x^{\varphi(n)}}{\varphi(n)}\to+\infty$. Donc la suite $a_nx^n$ n'est pas bornée, ce dont on déduit que $R\le 1$. Finalement, $R=1$.



!!! definition "intervalle/disque ouvert de convergence"
	Soit $\ds\sum a_nz^n$ une série entière de rayon de convergence $R$.  
	L'intervalle réel $]-R,R[$ est appelé *intervalle ouvert de convergence*. L'ensemble $\{z\in\mathbb C\text{ tq }|z|<R\}$ est appelé *disque ouvert de convergence.*   
	Évidemment, la série entière converge absolument sur son intervalle/disque ouvert de convergence.  

Si la série entière est définie sur son intervalle/disque ouvert de convergence, on peut se demander parfois ce qu'il se passe à la limite.

!!! exemple
	=== "Énoncé"
		Soit une série entière $\sum \frac {x^n}n$ de somme $S$. Quel est l'ensemble de définition réel de $S$ ?
	=== "Corrigé"
		Nous avons déjà vu que cette série entière avait un rayon de convergence de $1$. Donc $S$ est au moins définie sur $]-1,1[$. La question qui se pose est celle de la définition en $1$ et en $-1$. Or on sait que $\ds\sum \frac 1n$ diverge (série harmonique). Donc $S$ n'est pas définie en $1$. Par ailleurs la série $\ds\sum\frac{(-1)^n}{n}$ est une série alternée convergente : ainsi $S$ est bien définie en $-1$. Finalement, l'ensemble de définition de $S$ est $[-1,1[$.    

!!! exemple
	=== "Énoncé"
		Quel est l'ensemble de définition de $\ds f(x)=\sum_{n=1}^{+\infty}\ln\left(1+\frac 1n\right)x^n$ ?
	=== "Corrigé"
		Posons $a_n=\ln\left(1+\frac 1n\right)$. On a $a_n\sim \frac 1n$ et donc si $|x|<1$ alors $|a_nx^n|\sim \frac{|x|^n}{n}\to 0$ par croissances comparées. On en déduit que $R\ge 1$. Par ailleurs, toujours par croissances comparées, si $|x|>1$ alors $a_nx^n$ ne tend pas vers $0$. On en déduit que la série diverge grossièrement et donc $R\le 1$. Finalement, $R=1$ : la série converge absolument sur $]-1,1[$.

		On s'intéresse donc maintenant à ce qu'il se passe en $-1$ et en $1$. Pour $x=-1$, $a_nx^n=a_n(-1)^n$. Or la suite $(a_n)$ est décroissante et converge vers $0$. Par CSSA, $f$ est donc bien définie en $-1$. Pour $x=1$, on a $a_nx^n\sim\frac 1n$ qui est le terme général d'une série à termes positifs divergente : $\sum a_nx^n$ diverge donc. On en déduit que l'ensemble de définition de $f$ est $[-1,1[$.

		On peut vouloir aller plus loin et par exemple donner un équivalent en $1$. Écrivons $\ln(1+\frac 1n)=\frac 1n+\frac {u_n}{n^2}$ avec $(u_n)$ une suite bornée. On a alors

		$$
		\begin{aligned}
		\sum_{n=1}^{+\infty}\ln\left(1+\frac 1n\right)x^n&=\sum_{n=1}^{+\infty}\frac{x^n}{n}+\frac{u_n}{n^2}x^n\\
		&=\sum_{n=1}^{+\infty}\frac {x^n}{n}+\sum_{n=1}^{+\infty}\frac {u_n}{n^2}x^n\\
		&=-\ln(1-x)+\sum_{n=1}^{+\infty}\frac {u_n}{n^2}x^n
		\end{aligned}
		$$

		Or $\ds\left|\sum_{n=1}^{+\infty}\frac {u_n}{n^2}x^n\right|\le \sum_{n=1}^{+\infty}\frac M{n^2}\le \frac {M\pi^2}6$. On en déduit que $-\ln(1-x)+\sum_{n=1}^{+\infty}\frac {u_n}{n^2}x^n\underset{x\to 1}\sim -\ln(1-x)$ ce qui nous donne $f(x)\sim_{1}-\ln(1-x)$.
## Opérations sur les rayons de convergence

!!! propriete "comparaison de séries entières"
	Avec $R_a$ (resp. $R_b$) le rayon de convergence de $\displaystyle \sum a_n z^n$ (resp. $\displaystyle \sum b_n z^n$) :  
	- si $a_n = O (b_n)$, alors $R_a \geqslant R_b$ (Le résultat s'applique en particulier lorsque $a_n = o(b_n)$);  
	- si $a_n \sim b_n$, alors $R_a = R_b$.


!!! preuve
	Supposons que $a_n=O(b_n)$, alors la suite $\frac{a_n}{b_n}$ est bornée mais aussi $\frac{a_nz^n}{b_nz^n}$. On en déduit donc que $a_nz^n=O(b_nz^n)$ et par théorème de comparaison, si $\sum b_nz^n$ converge, alors $\sum a_nz^n$ converge : On en déduit que $R_a\ge R_b$.

	Si maintenant $a_n\sim b_n$, alors en particulier, $a_n=O(b_n)$ et $b_n=O(a_n)$ et donc $R_a\ge R_b$ et $R_b\ge R_a$. Par antisymétrie de la relation d'ordre, $R_a=R_b$.

!!! warning "attention"
	En particulier, si $|a_n|\le |b_n|$ alors $R_a\ge R_b$.

!!! exemple
	=== "Énoncé"
		On pose pour $n\in\mathbb N$, $a_n=\frac {n+1}{n+2}$. Quel est le rayon de convergence de la série entière $\sum a_nx^n$ ?
	=== "Corrigé"
		On a ici $a_n\sim 1$. Or la série entière $\sum x^n$ a pour rayon de convergence $1$. Donc ici $R(\sum a_nx^n)=1$.

!!! exemple
	=== "Énoncé"
		On pose pour $n\in\mathbb N$, $a_n=\frac{n!}{\sqrt n\prod_{k=0}^{n-1}2k+1}$. Quel est le rayon de convergence de $\sum a_nx^n$ ?
	=== "Corrigé"
		Soit $n\in\mathbb N$,

		$$
		\begin{aligned}
		a_n&=\frac{2^nn!n!}{\sqrt n(2n)!}\\
		&\sim\frac{2^n2\pi nn^{2n}e^{-2n}}{\sqrt n\sqrt{4\pi n}(2n)^{2n}e^{-2n}} \\
		&\sim\frac{\sqrt{\pi}}{2^n}
		\end{aligned}
		$$

		Or $\sum\frac{x^n}{2^n}$ est de rayon de convergence $2$ donc $\sum a_nx^n$ est de rayon de convergence $2$.

!!! propriete "Calcul du rayon de convergence"
	On peut appliquer la règle de d'Alembert et donc : soit $\sum a_nz^n$ une série entière. Si $\ds\frac{|a_{n+1}|}{|a_n|}\xrightarrow[n\to+\infty]{}\ell\in[0,+\infty[$ alors $R_a=\frac 1\ell$ si $\ell\in\mathbb R^+_*$ et $+\infty$ sinon.

!!! preuve
	Soit $\sum a_nz^n$ une série entière, on suppose que $\frac{|a_{n+1}|}{|a_n|}\to\ell\in\mathbb R^+_ *$. Soit $z<\frac 1\ell$, alors $\frac{|a_{n+1}z^{n+1}|}{|a_nz^n|}=|z|\frac{|a_{n+1}|}{|a_n|}\to z\ell<1$. D'après la règle de d'Alembert, la série $\sum a_nz^n$ converge. De même si $z>\frac 1\ell$, la limite est $>1$ et d'après le critère de d'Alembert, la série $\sum a_nz^n$ diverge grossièrement. On en déduit que $R_a=\frac 1\ell$.

	Si maintenant $\frac{|a_{n+1}|}{|a_n|}\xrightarrow[n\to+\infty]{}0^+$ alors $\frac{|a_{n+1}z^{n+1}|}{|a_nz^n|}\to r\ell=0<1$ : ainsi d'après le critère de d'Alembert, pour tout $r\in\mathbb R$, la série $\sum a_nz^n$ converge : le rayon de convergence est donc $+\infty$.


!!! exemple
	=== "Énoncé"
		Quel est le rayon de convergence de la série entière $\ds\sum \frac{n+1}{3^n}z^n$ ?
	=== "Corrigé"
		On utilise la règle de d'Alembert avec $a_n=\frac{n+1}{3^n}$ : $\left|\frac{(n+2)3^n}{(n+1)3^{n+1}}\right|=\frac{n+2}{3(n+1)}\to \frac 13$. Le rayon de convergence est donc $3$.

!!! exemple
	=== "Énoncé"
		Quel est le rayon de convergence de la série entière $\ds\sum\frac{n}{3^n+\frac 1n}z^{2n}$ ?
	=== "Corrigé"
		Une telle série est appelée *lacunaire* : il manque des termes, typiquement tous les termes de puissance impaire. Le critère de d'Alembert des séries entières ne peut pas s'appliquer tel quel. On revient au critère sur les séries : $\frac{(n+1)z^{2n+2}(3^n+\frac 1n)}{(3^{n+1}+\frac 1{n+1})nz^{2n}}\sim\frac{z^2}{3}$ et ainsi on a convergence si $|z|<\sqrt 3$ et divergence si $|z|>\sqrt 3$. Le rayon de convergence est donc $\sqrt 3$.

!!! exemple
	=== "Énoncé"
		Quel est le rayon de convergence de la série entière $\sum a_nz^n$ avec $a_n=\frac{n!}{1\times 3\times\cdots\times (2n+1)}$ ?
	=== "Corrigé"
		On utilise les manipulations classiques sur les factorielles :

		$$
		\begin{aligned}
		a_n&=\frac {n!}{1\times 3\times\cdots\times(2n+1)}\\
		&=\frac{n!\times 2\times 4\times\cdots\times (2n)}{1\times2\times3\times\cdots\times(2n)\times(2n+1)}\\
		&=\frac{n!\times 2^n\times n!}{(2n+1)!}
		\end{aligned}
		$$

		On a alors :

		$$
		\begin{aligned}
		\left|\frac{a_{n+1}}{a_n}\right|&=\frac{(n+1)!^2\times 2^{n+1}(2n+1)!}{n!^22^n(2n+3)!}\\
		&=\frac{2(n+1)^2}{2(n+1)(2n+3)}\\
		&=\frac{n+1}{2n+3}\\
		&\to \frac 12
		\end{aligned}
		$$

		D'après le critère de d'Alembert, on obtient un rayon de convergence de $2$.

!!! propriete "Rayon d'une combinaison linéaire"
	Soient deux séries entières $\ds\sum a_n z^n$ et $\ds\sum b_nz^n$ de rayon de convergence respectif $R_a$ et $R_b$. Soient $\lambda$ et $\mu$ deux nombres complexes. On note $c_n=\lambda a_n+\mu b_n$. Alors $R_c\ge \min(R_a,R_b)$, avec égalité ssi $R_a=R_b$.

!!! preuve
	Pour montrer ce résultat, il faut montrer que pour tout $z\le\min(R_a,R_b)$, $\sum c_nz^n$ converge.  Or si $z\le\min(R_a,R_b)$, alors $z\le R_a$ donc $\sum a_nz^n$ converge, et $z\le R_b$ donc $\sum b_nz^n$ convege. On en déduit que $\sum(\lambda a_n+\mu b_n)$ converge et donc $\sum c_n z^n$ converge.


	!!! exemple
		=== "Énoncé"
			*(exemple difficile)* On pose pour $n\in\mathbb N^*$ : $\ds a_n=\frac{1}{\sin(n\sqrt 3\pi)}$. Quel est le rayon de convergence de la série entière $\sum a_nx^n$ ?
		=== "Corrigé"
			Dans un premier temps, on a évidemment $\left|\frac1{\sin(\sqrt 3n\pi)}\right|\ge 1$ et donc $R\le 1$.

			Puis, $\sqrt 3$ étant irrationnel, il existe $p\in\mathbb N$ tel que $|n\sqrt 3-p|<\frac 12$. On a alors $|n\sqrt 3\pi-p\pi|<\frac \pi2$. Rappelons une inégalité classique (liée à la concavité de $\sin$ sur $[0,\frac\pi2]$) : si $x\in[0,\frac\pi2],\ |\sin(x)|\ge \frac 2\pi|x|$. On peut donc écrire ici deux choses :

			$$
			\begin{aligned}
			|\sin(n\sqrt 3\pi-p\pi)|&=|(-1)^n\sin(n\sqrt 3\pi)|\\
			&=|\sin(n\sqrt 3\pi)|\\
			|\sin(n\sqrt3\pi -p\pi)|&\ge\frac 2\pi|n\sqrt 3\pi-p\pi|\\
			&\ge 2|n\sqrt 3-p|
			\end{aligned}
			$$

			Or $|n\sqrt 3-p||n\sqrt 3+p|=|3n^2-p^2|\ge 1$ car $\sqrt 3$ est irrationnel (donc $3n^2-p^2$ est un entier différent de $0$). Donc en regroupant ces différents éléments : $\ds|\sin(n\sqrt 3\pi)|\ge \frac 2{n\sqrt3+p}$ ou encore $\ds\frac 1{|\sin(n\sqrt 3\pi)|}\le \frac 12|n\sqrt 3+p|$. Or par définition de $p$ ,$p\le n\sqrt3+1$. Donc $\ds\frac 1{|\sin(n\sqrt 3\pi)|}\le n\sqrt 3+\frac 12$. Or la série entière de terme général $n\sqrt 3x^n$ et la série entière de terme général $\frac 12x^n$ ont toutes les deux un rayon de convergence de $1$. Donc le rayon de convergence de $\sum a_nx^n$ est supérieur ou égal à $1$.

			Finalement, le rayon de convergence est de $1$.


!!! propriete "Rayon du produit de Cauchy"
 	Soient deux séries entières $\ds\sum a_n z^n$ et $\ds\sum b_nz^n$ de rayon de convergence respectif $R_a$ et $R_b$. Soient $\ds c_n=\sum_{p=0}^n a_pb_{n-p}$. Alors $R_c\ge \min(R_a,R_b)$ et pour $z\le \min(R_a,R_b)$ :

	$$
	\sum_{n=0}^{+\infty} c_nz^n=\left(\sum_{n=0}^{+\infty} a_nz^n\right)\left(\sum_{n=0}^{+\infty} b_nz^n\right).
	$$

!!! preuve
	Soient donc deux séries entières $\sum a_nz^n$ et $\sum b_nz^n$ de rayon de convergence respectif $R_a$ et $R_b$. Posons donc $\ds c_n=\sum_{p=0}^n a_pb_{n-p}$. Soit $z\le \min(R_a,R_b)$, alors $\sum a_nz^n$ et $\sum b_nz^n$ sont absolument convergentes. Donc leur produit de Cauchy est aussi absolument convergent et sa somme vaut $\ds\left(\sum_{n=0}^{+\infty} a_nz^n\right)\left(\sum_{n=0}^{+\infty} b_nz^n\right)$.




## Régularité de la somme d'une série entière de la variable réelle

!!! exemple
	=== "Énoncé"
		Pour $a>0$ et $n\in\mathbb N^*$, on pose $u_n=\ln\left(\frac{\sqrt n+(-1)^n}{\sqrt{n+a}}\right)$. Rayon de convergence de la série entière $\sum u_nx^n$, $x\in\mathbb R$ et convergence aux bornes ?
	=== "Corrigé"
		Travaillons tout d'abord sur $u_n$ :

		$$
		\begin{aligned}
		u_n &=\ln\left(\frac{\sqrt n+(-1)^n}{\sqrt{n+a}}\right)\\
		&=\ln\left(\frac {1+\frac{(-1)^n}{\sqrt n}}{\sqrt{1+\frac an}}\right)\\
		&=\ln\left(1+\frac{(-1)^n}{\sqrt n}\right)-\ln\left(\sqrt{1+\frac an}\right)\\
		&=\ln\left(1+\frac{(-1)^n}{\sqrt n}\right)-\frac 12\ln\left(1+\frac an\right)\\
		&=\frac{(-1)^n}{\sqrt n}-\frac 1{2n}-\frac a{2n}+O(\frac 1{n\sqrt n})\\
		&=\frac{(-1)^n}{\sqrt n}-\frac {1+a}{2n}+O(\frac 1{n\sqrt n})\\
		&\sim\frac{(-1)^n}{\sqrt n}
		\end{aligned}
		$$

		On peut alors utiliser la règle de d'Alembert : $\left|\frac{u_{n+1}}{u_n}\right|\sim \frac{\sqrt{n+1}}{\sqrt n}\to 1$. Donc le rayon de convergence de la série entière est $1$. On peut donc affirmer que la somme de la série entière est bien définie sur $]-1,1[$. Il faut se demander si elle est bien définie en $1$.

		D'après les calculs précédents, pour $x=1$, $u_nx^n=\frac{(-1)^n}{\sqrt n}-\frac {1+a}{2n}+O(\frac 1{n\sqrt n})$. Le premier terme est le terme général d'une série convergente. Le $O$ contient le terme général d'une série absolument convergente. On aura donc convergence de la série $\sum u_nx^n$ si et seulement si $\sum\frac{a+1}n$ est une série convergente, c'est à dire si $a=-1$. Ce qui est a priori interdit par les valeurs de $a$. On a donc divergence en $1$.

		Pour $x=-1$, on a $u_nx^n=(-1)^nu_n\sim \frac 1{\sqrt n}$ qui est le terme général d'une série divergente positive. Par théorèmes de comparaison des séries à termes positifs, la série est divergente. L'ensemble de définition est donc bien $]-1,1[$.

!!! propriete "Convergence normale sur tout segment de l'intervalle de convergence"
	Soit $\sum a_nx^n$ une série entière sur $\mathbb R$ de rayon de convergence $R$. On pose $u_n(x)=a_nx^n$. Soit $[a,b]\subset]-R,R[$. Alors la série de fonctions $\sum u_n$ converge normalement sur $[a,b]$.

!!! preuve
	Posons $c=\max\{|a|,|b|\}$. Soit $x\in [a,b]$, $|u_n(x)|=|a_nx^n|\le a_nc^n$ et $|c|<R$ donc par convergence absolue sur l'intervalle de convergence, on a une majoration uniforme par le terme général d'une série convergente : ainsi $\sum u_n$ converge normalement sur $[a,b]$.

!!! propriete "Continuité sur l'intervalle de convergence"
	Soit $\sum a_nx^n$ une série entière de rayon de convergence $R$, $x\mapsto\sum a_nx^n$ est continue sur son intervalle de convergence $]-R;R[$.

!!! preuve
	D'après la propriété précédente, la série entière est normalement convergente, donc uniformément convergente sur tout segment de l'intervalle ouvert de convergence. Par application du théorème de continuité des séries de fonctions, la série entière est continue sur l'intervalle ouvert de convergence.


!!! propriete "primitivation sur l'intervalle ouvert de convergence"
	Soit $\sum a_nx^n$ une série entière de rayon de convergence $R$. On pose pour $x\in]-R,R[$, $\ds f(x)=\sum_{n=0}^{+\infty}a_nx^n$ sa somme. Alors $f$ admet des primitives $F$ sur $]-R,R[$ et pour $x\in]-R,R[$, $\ds F(x)-F(0) = \sum_{n=0}^{+\infty} \frac{a_n}{n+1}x^{n+1}=\sum_{n=1}^{+\infty}\frac{a_{n-1}}{n}x^n$ qui est une série entière de rayon de convergence $R$.

!!! preuve
	Il y a beaucoup de choses à montrer ici. Posons $f_n(x)=a_nx^n$ pour $x\in]-R;R[$. On sait que $\sum f_n$ est uniformément convergente sur tout segment de $]-R,R[$. Soit $x\in]-R,R[$, alors d'après le théorème d'intégration terme à terme sur un segment, $\ds\int_0^x f(t)\d t=\sum_{n=0}^{+\infty}\int_0^x f_n(t)\d t=\sum_{n=0}^{+\infty} a_n\frac{x^{n+1}}{n+1}$. On en déduit $f$ admet des primitives de la forme $\ds F(x)=C+\sum_{n=1}^{+\infty}a_{n-1}\frac{x^n}{n}$. Ces primitives sont bien des séries entières.

	Montrons alors l'égalité des rayons de convergence. Notons $R_f$ le rayon de convergence de $\sum a_nx^n$ et $R_F$ celui de $\sum \frac{a_{n-1}}{n}x^n$. Si $|x|\le R_f$ alors pour $n$ assez grand, $\left|\frac{x}{n}\right|\le 1$ et $\left|\frac{a_{n-1}}{n}x^n\right|\le \left|a_{n-1}x^{n-1}\right|$. Par comparaison des séries à termes positifs, on en déduit que $\sum \frac{a_{n-1}}{n}x^n$ converge absolument, donc converge. On en déduit la convergence de la série entière $\sum \frac{a_{n-1}}nx^n$ sur $]-R_f,R_f[$ ce dont on déduit que $R_F\ge R_f$.

	Soit maintenant $|x|< R_F$, il existe $\rho$ tel que $|x|<\rho<R_f$. On a alors $\ds |a_nx^n|=|\frac{a_n\rho^{n+1}(n+1)}{\rho^{n+1}(n+1)}x^n|=\left|\frac{a_n\rho^{n+1}}{n+1}\times (n+1)\frac {x^n}{\rho^{n+1}}\right|$. Or par croissances comparées, $\ds (n+1)\frac {x^n}{\rho^{n+1}}\xrightarrow[n\to+\infty]{}0$ donc à partir d'un certain rang, $\ds |a_nx^n|\le \frac{a_n\rho^{n+1}}{n+1}$ qui est le terme général d'une série convergente puisque $|\rho|\le R_F$. Par théorème de comparaison on en déduit que $\sum a_nx^n$ est absolument convergente donc convergente sur $]-R_F,R_F[$ ce dont on déduit que $R_f\ge R_F$.

	Finalement, $R_f=R_F$ : une série entière et ses séries entières primitives ont le même rayon de convergence.

!!! exemple
	=== "Énoncé"
		Développer en série entière $f:x\mapsto \int_0^x\frac{1}{1+t+t^2}\d t$
	=== "Corrigé"
		La fonction $t\mapsto \frac 1{1+t+t^2}$ est continue sur $\mathbb R$ donc l'intégrale est bien définie et $f$ est définie et continue sur $\mathbb R$ de dérivée $f'(x)=\frac 1{1+x+x^2}$.  
		Or :

		$$
		\begin{aligned}
		\frac 1{1+x+x^2} &= \frac{1-x}{1-x^3}\\
		&=(1-x)\sum_{n=0}^{+\infty}x^{3n}\\
		&=\sum_{n=0}^{+\infty}x^{3n}-x^{3n+1}\\
		&=\sum_{n=0}^{+\infty}a_nx^n
		\end{aligned}
		$$

		avec $\ds a_n=\begin{cases}1&\text{si }n\equiv 0[3]\\-1&\text{si }n\equiv 1[3]\\0&\text{sinon}\end{cases}$. Le rayon de convergence est $1$. Par propriété de primitivation, $f(x)=C+\sum_{k=1}^{+\infty}\frac{a_{n-1}}{n}x^n$ et $f(0)=0$ donc $C=0$. On en déduit que $\ds f(x)=\sum_{k=1}^{+\infty}b_nx^n$ avec $\ds b_n=\begin{cases}\frac 1n&\text{si }n\equiv 1[3]\\-\frac1n&\text{si }n\equiv 2[3]\\0 &\text{si }n\equiv 0[3] \end{cases}$






!!! propriete "Caractère $\mathcal C^\infty$ d'une série entière"
	Soit $\sum a_nx^n$ une série entière de rayon de convergence $R$ et de somme $f$. Alors $f$ est $\mathcal C^\infty$ sur $]-R,R[$ et pour tout $x\in\mathbb ]-R,R[$ et tout $k\in\mathbb N$, $\ds f^{(k)}(x)=\sum_{n=k}^{+\infty}\frac{n!}{(n-k)!}a_nx^{n-k}$. Ainsi les dérivées de $f$ sont les sommes de séries entières de rayon de convergence $R$.

!!! preuve
	C'est une application directe du théorème de classe $\mathcal C^\infty$ des séries de fonctions uniformément convergentes, ici sur tout segment de $]-R,R[$. La remarque précédente justifie la propriété sur les rayons de convergence, par récurrence.

!!! exemple
	=== "Énoncé"
		Montrer que $f:x\mapsto\frac 1{(1-x)^2}$ est la somme de la série entière $\sum (n+1)x^n$. Quelle est la somme de la série entière $\sum nx^n$ ?
	=== "Corrigé"
		La fonction $f$ est la dérivée de la fonction $g:x\mapsto \frac 1{1-x}$. Or $\frac 1{1-x}=\sum_{n=0}^{+\infty} x^n$. On en déduit par théorème de dérivation que $\frac 1{(1-x)^2}=\sum_{n=1}^{+\infty}nx^{n-1}=\sum_{n=0}^{+\infty}(n+1)x^n$ pour tout $x\in]-1,1[$.  
		Concernant la seconde question, la série entière proposée est de rayon de onvergence $1$. On remarque que $n=n+1-1$. Or les séries entières de terme général $(n+1)x^n$ et $x^n$ sont convergentes sur leur intervalle ouvert de convergence $]-1,1[$. On peut donc écrire $\ds\sum_{n=0}^{+\infty}nx^n=\sum_{n=0}^{+\infty}(n+1)x^n-\sum_{n=0}^{+\infty}x^n=\frac 1{(1-x)^2}-\frac 1{1-x}=\frac x{(1-x)^2}$. (il y avait plus simple...)

!!! exemple
	=== "Énoncé"
		Soient $a\in\mathbb C^*$ et $p\in\mathbb N$. Montrer que $x\mapsto\frac1{(x-a)^{p+1}}$ est développable en série entière et calculer ce développement.
	=== "Corrigé"
		Tout d'abord, $f(x)=\frac 1{x-a}=-\frac 1a\frac1{1-\frac xa}$. Or $u\mapsto \frac 1{1-u}$ est développable en série entière avec rayon de convergence $1$ et $\frac 1{1-u}=\sum_{k=0}^{+\infty}u^k$. On a donc pour $|x|<|a|$, $\ds\frac 1{x-a}=-\frac 1a\sum_{k=0}^{+\infty}\frac{x^k}{a^k}=-\sum_{k=0}^{+\infty}\frac{x^k}{a^{k+1}}$. D'après le théorème de dérivation on sait que $f^{(p)}$ est développable en série entière avec $f^{(p)}(x)=-\sum_{k=p}^{+\infty}\frac{k!}{(k-p)!}\frac{x^{k-p}}{a^{k+1}}$. Or $f^{(p)}(x)=\frac{(-1)^pp!}{(x-a)^{p+1}}$. On en déduit :

		$$
		\frac{1}{(x-a)^{p+1}}=-\sum_{k=p}^{+\infty}\binom kp (-1)^p\frac{x^{k-p}}{a^{k+1}} =\sum_{k=0}^{+\infty}\binom{k+p}p (-1)^{p+1}\frac{x^k}{a^{k+p+1}}
		$$



!!! propriete "Relation coefficients/dérivées"
	Si $f$ est la somme d'une série entière $\sum a_nx^n$ de rayon de convergence $R>0$, alors pour tout entier $n\in\mathbb N$, $a_n=\frac{f^{(n)}(0)}{n!}$.

!!! preuve
	C'est une application directe de la propriété précédente. Le rayon de convergence étant strictement positif, les dérivées successives sont toutes définies en $0$ et $\ds f^{(k)}(x)=\sum_{n=k}^{+\infty}\frac{n!}{(n-k)!}a_nx^{n-k}$ d'où en $0$ : $f^{(k)}(0)=k!a_k$. On en déduit donc $a_k=\frac{f^{(k)}(0)}{k!}$.


## Fonctions d'une variable réelle développable en série entière au voisinage de $0$.

!!! definition "fonction développable en série entière au voisinage de $0$"
	Soit $f$ une fonction définie sur un intervalle $]-r,r[$, $r>0$. On dit que $f$ est développable en série entière au voisinage de $0$ s'il existe une série entière $\sum a_nx^n$ de rayon de convergence $R\ge r$ telle que pour tout $x\in]-r,r[$, $\ds f(x)=\sum_{n=0}^{+\infty}a_nx^n$.


On rappelle la formule de Taylor reste intégral :
!!! propriete "Taylor reste intégral"
	Soit $f$ définie sur $[a,b]$ de classe $\mathcal C^{n+1}$. Alors :

	$$
	f(b)=\sum_{k=0}^{(n)}\frac{f^{(k)}(a)}{k!}(b-a)^k \quad +\int_a^b f^{(n+1)}(t)\frac{(b-t)^{n}}{(n)!}\d t.
	$$

!!! preuve
	La preuve se fait facilement par récurrence. Posons pour tout $n\in\mathbb N$ la propriété $H(n)=$«Si $f$ est de classe $\mathcal C^{n+1}$ sur $[a,b]$ alors $\ds f(b)=\sum_{k=0}^{(n)}\frac{f^{(k)}(a)}{k!}(b-a)^k \quad +\int_a^b f^{(n+1)}(t)\frac{(b-t)^{n}}{(n)!}\d t$».

	^^Initialisation^^ : pour $n=0$, on suppose $f$ de classe $\mathcal C^1$. On a $\ds f(a)+\int_{a}^bf'(t)\frac{(b-t)^0}{0!}\d t=f(a)+\int_a^bf'(t)\d t=f(a)+f(b)-f(a)=f(b)$. Donc $H_0$ est vérifiée.

	^^Hérédité^^ : Soit $n\in\mathbb N$, supposons $H_n$ vraie, montrons $H_{n+1}$ vraie. Soit $f$ de classe $\mathcal C^{n+2}$, on calcule :

	$$
	\begin{aligned}
	\sum_{k=0}^{n+1}\frac{f^{(k)}(a)}{k!}(b-a)^k&+\int_a^bf^{(n+2)}(t)\frac{(b-t)^{n+1}}{(n+1)!}\d t\\
	&=\sum_{k=0}^{n}\frac{f^{(k)}(a)}{k!}(b-a)^k+\frac{f^{(n+1)}(a)}{(n+1)!}(b-a)^{n+1}+\int_a^bf^{(n+2)}(t)\frac{(b-t)^{n+1}}{(n+1)!}\d t\\
	&=\sum_{k=0}^{n}\frac{f^{(k)}(a)}{k!}(b-a)^k\\
	&+\frac{f^{(n+1)}(a)}{(n+1)!}(b-a)^{n+1}+{\left[f^{(n+1)}(t)\frac{(b-t)^{n+1}}{(n+1)!}\right]}_a^b+\int_a^bf^{(n+1)}(t)\frac{(b-t)^{n}}{n!}\d t\\
	&\underset{IPP}=\sum_{k=0}^{n}\frac{f^{(k)}(a)}{k!}(b-a)^k\\
	&+\frac{f^{(n+1)}(a)}{(n+1)!}(b-a)^{n+1}-f^{(n+1)}(a)\frac{(b-a)^{n+1}}{(n+1)!}+\int_a^bf^{(n+1)}(t)\frac{(b-t)^{n}}{n!}\d t\\
	&=\sum_{k=0}^{n}\frac{f^{(k)}(a)}{k!}(b-a)^k+\int_a^bf^{(n+1)}(t)\frac{(b-t)^{n}}{n!}\d t\\
	&\underset{H_n}=f(b)
	\end{aligned}
	$$

	Donc $H_{n+1}$ est vérifiée.


!!! definition "Série de Taylor d'une fonction $\mathcal C^\infty$"
	Soit $f$ une fonction de classe $\mathcal C^\infty$ au voisinage de $0$. On appelle *série de Taylor de $f$* la série entière $\sum a_nx^n$ avec $a_n=\frac{f^{(n)}(0)}{n!}$.

!!! remarque
	D'après la relation coefficients/dérivées, si $f$ est $\mathcal C^\infty$ et si $f$ est développable en série entière, alors son développement coïncide avec sa série de Taylor.

!!! exemple
	=== "Énoncé"
		Soit $f$ définie par $\ds f(x)=\begin{cases}0& \text{si }x=0\\\exp(-1/x^2)&\text{sinon}\end{cases}$. Montrer que $f$ est de classe $\mathcal C^\infty$ et que pour tout entier $n\in\mathbb N$, $f^{(n)}(0)=0$.
	=== "Corrigé"
		$f$ est de classe $\mathcal C^\infty$ sur $\mathbb R^*$ par théorèmes généraux. Montrons sur cet ensemble que la dérivée $n$è de $f$ est de la forme $f^{(n)}(x)=\frac {P_n(x)}{x^{3n}}e^{-\frac 1{x^2}}$ avec $P_n$ non nul en $0$ de degré $2n-2$. C'est évidemment vrai pour $n=1$. Montrons l'hérédité. 

		Soit $n$ un entier naturel, supposons que $f^{(n)}$ est de la forme $f^{(n)}(x)=\frac{P_n(x)}{x^{3n}}e^{-\frac1{x^2}}$.  On a alors $f^{(n+1)}(x)=\frac{P_n'(x)x^{3n}-3nP_n(x)x^{3n-1}}{x^{6n}}e^{-\frac1{x^2}}+\frac 2{x^3}\frac {P_n(x)}{x^{3n}}e^{\frac 1{x^2}}=\frac{P_{n+1}}{x^{3(n+1)}}e^{-\frac 1{x^2}}$ avec $P_{n+1}(x)=x^3P_n'(x)-3nx^2P_n(x)+2P_n(x)$ qui est de degré $2n+2$ et non nul en $0$. 

		On en déduit par croissances comparées que $f^{(n)}(x)$ converge vers $0$ en $0$ et par théorème de prolongement $\mathcal C^\infty$, $f$ est $\mathcal C^\infty$ et ses dérivées en $0$ sont toutes nulles.  

!!! remarque
	Cet exemple montre que plusieurs fonctions $\mathcal C^\infty$ peuvent avoir la même série de Taylor. Mais une fonction $\mathcal C^\infty$ n'est pas pour autant toujours développable en série entière. Ici, si la fonction $f$ était développable en série entière, alors son développement en série entière coïnciderait avec sa série de Taylor, qui est nulle. On aurait donc un intervalle $]-R,R[$ avec $R>0$ sur lequel la fonction $f$ serait nulle, ce qui n'est pas le cas.

!!! propriete "Développements usuels"
	Les fonctions suivantes sont de classe $\mathcal C^\infty$ au voisinage de $0$ et  donc développables en série entière :  
	- $\exp$ de développement $\ds\sum \frac {x^k}{k!}$, de rayon de convergence $+\infty$ ;  
	- $\cos$ de développement $\ds\sum \frac {(-1)^kx^{2k}}{(2k)!}$, de rayon de convergence $+\infty$ ;  
	- $\sin$ de développement $\ds\sum \frac {(-1)^kx^{2k+1}}{(2k+1)!}$, de rayon de convergence $+\infty$ ;  
	- $\cosh$ de développement $\ds\sum \frac {x^{2k}}{(2k)!}$, de rayon de convergence $+\infty$ ;  
	- $\sinh$ de développement $\ds\sum \frac {x^{2k+1}}{(2k+1)!}$, de rayon de convergence $+\infty$ ;  
	- $\arctan$ de développement $\ds\sum \frac{(-1)^kx^{2k+1}}{2k+1}$, de rayon de convergence $1$ ;  
	- $x\mapsto\ln(1+x)$ de développement $\ds\sum \frac{(-1)^{k+1}}{k}x^k$, de rayon de convergence $1$.


!!! preuve
	Traitons deux ou trois cas.   
	Dans le cas de la fonction exponentielle, on s'intéresse à la fonction à variable réelle $f_\alpha:x\mapsto\exp(\alpha x)$ avec $\alpha$ complexe (c'est un cas plus général). Cette fonction est $\mathcal C^\infty$ au voisinage de $0$ et $f_\alpha^{(k)}(0)=\alpha^k$. D'après la formule de Taylor reste intégral, on a pour tout $x\in\mathbb R$ et tout $n\in\mathbb N$, $\ds\left|\exp(x)-\sum_{k=0}^n\frac{\alpha^kx^k}{k!}\right|\le |\alpha|^{n+1}\left|\int_0^x \exp(\alpha t)\frac{(x-t)^n}{n!}\d t\right|$. Attention, avant de rentrer la valeur absolue dans l'intégrale, il faut faire attention aux bornes d'intégration. Calculons:

	$$
	\begin{aligned}
	\left|\int_0^x\exp(\alpha t)\frac{(x-t)^n}{n!}\d t\right|&\underset{u=\sign(x)t}=\left|\sign(x)\int_0^{|x|}\exp(\alpha \sign(x)u)\frac{(x-\sign(x)u)^n}{n!}\d u\right|\\
	&=\left|\sign(x)^{n+1}\int_0^{|x|}\exp(\alpha \sign(x)u)\frac{(|x|-u)^n}{n!}\d u\right|\\
	&=\left|\int_0^{|x|}\exp(\alpha \sign(x)u)\frac{(|x|-u)^n}{n!}\d u\right|\\
	&\le \int_0^{|x|}|\exp(\alpha \sign(x)u)|\frac{(|x|-u)^n}{n!}\d u\\
	&\le \int_0^{|x|}\exp(Re(\alpha) \sign(x)u)|\frac{(|x|-u)^n}{n!}\d u\\
	&\le \exp(|Re(\alpha)||x|)\int_0^{|x|}\frac{(|x|-u)^n}{n!}\d u\\
	&\le \exp(|Re(\alpha)||x|)\frac{|x|^{n+1}}{(n+1)!}\\
	&\xrightarrow[n\to+\infty]{} 0
	\end{aligned}
	$$

	On en déduit en passant à la limite en $n$ que pour tout $x\in\mathbb R$, $\ds\exp(\alpha x)=\sum_{k=0}^{+\infty}\frac{\alpha^kx^k}{k!}$ et le résultat voulu en prenant $\alpha=1$. D'après le critère de d'Alembert, on a $\frac{k!}{(k+1)!}\to0$ donc le rayon de convergence de cette série est $+\infty$.

	On obtient les résultats de $\cos$, $\sin$, $\cosh$ et $\sinh$ avec des raisonnements très similaires.

	Pour l'arctangente, on peut utiliser la propriété de primitivation, puisque l'on sait que si $|x|<1$, alors $\frac{1}{1+x^2}$ est développable en série entière de développement $\sum (-1)^kx^{2k}$. Par primitivation sur l'intervalle $]-1,1[$, on a alors $\arctan$ qui est développable en série entière, de développement $\sum \frac{(-1)^kx^{2k+1}}{2k+1}$. Son rayon de convergence est le même que celui de sa dérivée, c'est à dire $1$.

	Enfin pour $\ln(1+x)$ on peut aussi passer par la propriété de primitivation. On a $\frac 1{1+x}$ qui est développable en série entière, de développement $\sum(-1)^kx^k$ de rayon de convergence $1$. Par propriété de primitivation, $\ln(1+x)$ est développable en série entière, de développement $\sum\frac{(-1)^k}{k+1}x^{k+1}$, de rayon de convergence $1$.

!!! propriete "Développement de la puissance $\alpha$"
	Soit $\alpha$ un réel fixé n'appartenant pas à $\mathbb N$ (le cas $a\in\mathbb N$ est trivial). On pose $f_\alpha(x)=(1+x)^\alpha$. Alors $f$ est développable en série entière au voisinage de $0$ de rayon $1$ avec pour $x\in]-1,1[$ :

	$$
	f_\alpha(x)=\sum_{n=0}^{+\infty}\frac{\alpha(\alpha-1)(\cdots)(\alpha-n+1)}{n!}x^n.
	$$

!!! preuve
	Pour ce développement, nous allons utiliser une autre méthode qu'il faut savoir mettre en place. On remarque tout d'abord que $f_\alpha$ est ^^la solution^^ du problème de Cauchy $\begin{cases}(1+x)y'(x)-\alpha y(x)&=0\\y(0)&=1 \end{cases}$.

	Supposons maintenant qu'il existe une solution développable en série entière $\sum a_nx^n$ de rayon de convergence $R$ de ce problème de Cauchy. Notons $S$ sa somme. Par les propriétés de dérivation des séries entières, pour $x\in]-R,R[$ on a :

	$$
	\begin{aligned}
	(1+x)S'(x)-\alpha S(x)&=(1+x)\sum_{k=0}^{+\infty}(n+1)a_{n+1}x^{n}-\alpha\sum_{k=0}^{+\infty}a_nx^n\\
	&=\sum_{k=0}^{+\infty}(n+1)a_{n+1}x^{n}+\sum_{k=0}^{+\infty}(n+1)a_{n+1}x^{n+1}-\alpha\sum_{k=0}^{+\infty}a_nx^n\\
	&=\sum_{k=0}^{+\infty}(n+1)a_{n+1}x^{n}+\sum_{k=1}^{+\infty}na_{n}x^{n}-\alpha\sum_{k=0}^{+\infty}a_nx^n\\
	&=\sum_{k=0}^{+\infty}(n+1)a_{n+1}x^{n}+\sum_{k=0}^{+\infty}na_{n}x^{n}-\alpha\sum_{k=0}^{+\infty}a_nx^n\\
	&=\sum_{k=0}^{+\infty}\left[(n+1)a_{n+1}+(n-\alpha)a_{n}\right]x^{n}
	\end{aligned}
	$$

	Or $S$ est solution de l'équation différentielle homogène donc $\ds\sum_{k=0}^{+\infty}\left[(n+1)a_{n+1}+(n-\alpha)a_{n}\right]x^{n}=0$. On en déduit que pour tout $n\ge 0$, $(n+1)a_{n+1}-(\alpha -n)a_n=0$ c'est à dire $a_{n+1}=\frac{\alpha -n}{n+1}a_n$. De plus $S(0)=1$ donc finalement : $a_0=1$ et pour $n>0$, $\ds a_n=\frac{\alpha(\alpha-1)(\cdots)(\alpha-n+1)}{n!}$. Ainsi la seule solution développable en série entière est la série entière $\ds\sum\frac{\alpha(\alpha-1)(\cdots)(\alpha-n+1)}{n!}x^n$.

	Considérons donc cette série entière. Pour simplifier on pose $\ds a_n= \frac{\alpha(\alpha-1)(\cdots)(\alpha-n+1)}{n!}$. On a $\left|\frac{a_{n+1}}{a_n}\right|=\left|\frac{\alpha-n}{n+1}\right|\xrightarrow[n\to+\infty]{}1$. Elle est donc de rayon de convergence $1$. On vérifie facilement que cette série entière vérifie les conditions du problème de Cauchy précédent. Par unicité des solutions du problème de Cauchy, sa somme $S$ coïncide avec $f_\alpha$ sur $]-1,1[$ et on a donc pour tout $x\in]-1,1[$ : $\ds f_\alpha(x)=\sum_{n=0}^{+\infty} \frac{\alpha(\alpha-1)(\cdots)(\alpha-n+1)}{n!}x^n$


{% include-markdown "../../../exercicesRMS/2022/RMS2022-914.md"%}

!!! exemple
	=== "Énoncé"
		Rayon de convergence et somme de $\ds\sum\frac{\cos(\alpha n)}{n}x^n$.
	=== "Corrigé"
		Tout d'abord, $\left|\frac{\cos(\alpha n)}{n}\right|\le \frac 1n$ donc le rayon de convergence est $\ge 1$. De plus $\cos(n\alpha)\not\to 0$ et si $x>1$, $\frac{x^n}n\to+\infty$ par croissances comparées donc $R\le 1$. On en déduit que le rayon de convergence est $1$.  
		On peut aussi remarquer par propriété de primitivation que cette série a le même rayon que la série $\sum\cos(n\alpha)x^n$ dont le rayon est évidemment $1$.

		Posons maintenant pour $x\in]-1,1[$, $g(x)=\sum_{n=1}^{+\infty}\frac{\cos(\alpha n)}{n}x^n$. On remarque par dérivation terme à terme des séries entières que $\ds g'(x)=\sum_{n=1}^{+\infty}\cos(n\alpha)x^{n-1}=Re\left(e^{i\alpha}\sum_{n=0}^{+\infty}e^{i\alpha n}x^n\right)$. Or :

		$$
		\begin{aligned}
		e^{i\alpha}\sum_{n=0}^{+\infty}e^{i\alpha n}x^n&=\frac{e^{i\alpha}}{1-e^{i\alpha}x}\\
		&=\frac{e^{i\alpha}(1-e^{-i\alpha}x)}{|1-e^{i\alpha}x|^2}\\
		&=\frac{e^{i\alpha}-x}{1-2\cos(\alpha)x+x^2}\\
		&=\frac{\cos(\alpha)-x}{1-2\cos(\alpha)x+x^2}+i\frac{\sin(\alpha)}{1-2\cos(\alpha)x+x^2}
		\end{aligned}
		$$

		On en déduit $g'(x)=\frac{\cos(\alpha)-x}{1-2\cos(\alpha)x+x^2}=-\frac 12\frac{2x-2\cos(\alpha)}{x^2-2\cos(\alpha)x+1}$ dont une primitive est $-\frac 12\ln(x^2-2\cos(\alpha)x+1)$. Or $g(0)=0$ donc $g(x)=-\frac 12\ln(x^2-2\cos(\alpha)x+1)$.

!!! exemple
	=== "Énoncé"
		Quel est le développement en série entière de $\ds f(x)=\frac{1}{(x+2)(x-1)^2}$.
	=== "Corrigé"
		Pour se ramener à des choses connues, on développe en éléments simples :

		$$
		\frac{1}{(x+2)(x-1)^2}=\frac{1/9}{x+2}-\frac{1/9}{x-1}+\frac{1/3}{(x-1)^2}
		$$

		Or $\frac 1{x+2}=\frac 12\frac1{1+\frac x2}=\frac 12\sum_{n=0}^{+\infty}{\left(\frac {-1}{2}\right)}^nx^n$, $\frac 1{x-1}=-\sum_{n=0}^{+\infty}x^n$ et enfin $\frac1{(1-x)^2}=\sum_{n=0}^{+\infty}(n+1)x^n$. On en déduit que :

		$$
		\begin{aligned}
		\frac1{(x+2)(x-1)^2}&=\frac 1{18}\sum_{n=0}^{+\infty}{\left(\frac {-1}{2}\right)}^nx^n+\frac19\sum_{n=0}^{+\infty}x^n+\frac 13\sum_{n=0}^{+\infty}(n+1)x^n\\
		&=\sum_{n=0}^{+\infty}\left(\frac {(-1)^n}{9\times 2^{n+1}}+\frac 19+\frac {n+1}3\right)x^n\\
		&=\sum_{n=0}^{+\infty}\frac{(-1)^n+(6n+8)2^{n}}{9\times 2^{n+1}}x^n
		\end{aligned}
		$$


!!! exemple
	=== "Énoncé"
		Chercher une solution développable en série entière au voisinage de $0$ de $(1-x^2)y'-xy=2$.
	=== "Corrigé"
		Soit $\sum a_nx^n$ une solution de l'équation différentielle de rayon de convergence $R$ q'on suppose strictement positif. On a alors

		$$
		\begin{aligned}
		2&=\sum_{n=1}^{+\infty}na_n(1-x^2)x^{n-1}-\sum_{n=0}^{+\infty} a_nx^{n+1}\\
		&=\sum_{n=0}^{+\infty}(n+1)a_{n+1}x^n -\sum_{n=1}^{+\infty}(n-1)a_{n-1}x^n-\sum_{n=1}^{+\infty}a_{n-1}x^n\\
		&=a_1+\sum_{n=2}^{+\infty}\left((n+1)a_{n+1}-na_{n-1}\right)x^n
		\end{aligned}
		$$

		On en déduit par unicité que $a_1=2$. Puis pour tout $n\ge 1$, $(n+1)a_{n+1}-na_{n-1}=0$ c'est à dire $a_{n+1}=\frac n{n+1}a_{n-1}$.

		Prenons donc par exemple $a_0=0$ (on cherche *une* solution) : tous les coefficients d'indice pair seront nuls et pour les coefficients d'indice impair, on a : $a_{2p+1}=\frac{2p}{2p+1}a_{2p-1}=\frac{2p\times(2p-2)\times\dots\times2}{(2p+1)\times(2p-1)\times\dots\times 3\times1}a_1=\frac{(2^pp!)^2}{(2p+1)!}\times2=a_{2p+1}$. On a donc ici un bon candidat solution.

		Si on pose $u_p=a_{2p+1}x^{2p+1}$, on a $\left|\frac{u_{p+1}}{u_p}\right|=\frac{2p}{2p+1}x^2\to x^2$. On obtient donc d'après la règle de d'Alembert que le rayon de convergence est $1$ et est donc bien strictement positif. La fonction définie sur $]-1,1[$ par $x\mapsto 2\sum_{n=0}^{+\infty}\frac{(2^nn!)^2}{(2n+1)!}x^{2n+1}$ est l'unique solution du problème de Cauchy $\begin{cases}(1-x^2)y'-xy=2\\y(0)=0\end{cases}$.


## Cas complexe

On passe assez naturellement au cas des séries entières à valeurs complexes.

!!! propriete "Continuité sur le disque ouvert de convergence"
	Soit $\sum a_nz^n$ une série entière à valeurs complexes, de rayon de convergence $R$ et de somme $S$. Alors $S$ est continue sur le disque ouverte de convergence $D(0,R)$.

!!! propriete "Développements usuels"
	- Si $|z|<1$, alors $z\mapsto \frac 1{1-z}$ est développable en série entière de développement $\ds\sum_{k=0}^{+\infty}z^k$.
	- Si $z\in\mathbb C$, alors $z\mapsto \exp(z)$ est développable en série entière de développement $\ds\sum_{k=0}^{+\infty}\frac{z^k}{k!}$.

!!! preuve
	- le premier cas est une application directe des résultats sur les séries géométriques
	- le second cas découle d'un calcul précédent : on a montré que pour tout $z\in\mathbb C$ et tout $x\in\mathbb R$, $\ds\exp(zx)=\sum_{k=0}^{+\infty}\frac{\alpha^kx^k}{k!}$ donc en prenant $x=1$ on a bien $\ds\exp(z)=\sum_{k=0}^{+\infty}\frac{z^k}{k!}$.

!!! propriete "Propriétés de l'exponentielle"
	$\exp$ est continue sur $\mathbb R$, dérivable sur $\mathbb R$ et $\exp'=\exp$. De plus, pour $(z,z')\in\mathbb C$, $\exp(z+z')=\exp(z)\times\exp(z')$.

!!! preuve
	Les deux premiers points ne posent pas de problème. Pour le troisième, les séries étant absolument convergentes, on peut affirmer que $\ds\exp(z)\exp(z')=\sum_{n=0}^{+\infty}c_n$ avec $\ds c_n=\sum_{p=0}^n\frac{z^p}{p!}\times\frac{{z'}^{n-p}}{(n-p)!}=\frac 1{n!}\left(\sum_{p=0}^n\binom pnz^p{z'}^{n-p}\right)=\frac 1{n!}(z+z')^n$. On en déduit que $\exp(z)\exp(z')=\exp(z+z')$.


## Exercices

### Issus de la banque CCINP

{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-002.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-015.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-018.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-020.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-021.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-022.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-023.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-024.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-032.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-047.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-051.md" %}

### Annales d'oraux
{% include-markdown "../../../exercicesRMS/2022/RMS2022-575.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-577.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-582.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-620.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1114.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1104.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1212.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1214.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1215.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1216.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1218.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1220.md"%}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-741.md"%}









### Centrale python
{% include-markdown "../../../exercicesCentralePython/RMS2022-1058.md"  rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1113.md" %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1211.md"  rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1219.md" %}
