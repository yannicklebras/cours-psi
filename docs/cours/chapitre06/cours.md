# Réduction des endomorphismes et des matrices carrées

Dans tout ce chapitre, $\ds E$ désigne un espace vectoriel sur $\ds \mathbb K$ où $\ds \mathbb K$ désigne $\ds \mathbb R$ ou $\ds \mathbb C$.

## Éléments propres

!!! definition "Droite stable"
    Soit $\ds D$ une droite vectorielle de $\ds E$ de vecteur directeur $\ds a$. Soit $\ds u$ un endomorphisme de $\ds E$. $\ds D$ est une *droite stable par $\ds u$* si et seulement si $\ds u(D)\subset D$. 

!!! propriete "Caractérisation d'une droite stable"
    Soit $\ds D$ une droite stable par $\ds u$ de vecteur directeur $\ds a$. Alors il existe $\ds \lambda\in\mathbb K$ tel que $\ds u(a)=\lambda a$. 

!!! preuve 
    Soit donc $\ds D$ une droite stable par $\ds u$ de vecteur directeur $\ds a$.  Alors $\ds u(a)\in D$ et donc il existe $\ds \lambda\in\mathbb K$  tel que $\ds u(a)=\lambda a$. 

!!! definition "Valeur propre, vecteur propre"
    Soit $\ds u\in\mathcal L(E)$, soit $\ds \lambda\in\mathbb K$. $\ds \lambda$ est une *valeur propre* de $\ds u$ si et seulement si il existe un vecteur $\ds x\in E$ non nul tel que $\ds u(x)=\lambda x$. 

    On dit alors que $\ds x$ est un *vecteur propre*.

    On a les définitions équivalentes pour les matrices : soit $\ds A\in\mathcal M_n(\mathbb K)$, on dit que $\ds \lambda\in\mathbb K$ est une valeur propre de $\ds A$ si et seulement si il existe $\ds X\in\mathcal M_{n,1}(\mathbb K)$ tel que $\ds AX=\lambda X$. $\ds X$ est alors appelé vecteur propre de $\ds A$. 

!!! remarque
    Si $\ds \lambda$ est une valeur propre de $\ds u$ ou de $\ds A$, alors le vecteur nul est aussi un vecteur propre associé à $\ds \lambda$. 

!!! exemple
    === "Énoncé"
        Soit $\ds E=\mathbb K[X]$, déterminer les valeurs propres et les vecteurs propres de l'endomorphisme $\ds D:P\mapsto P'$. Déterminer un polynôme annulateur de la restriction de $\ds D$ à $\ds \mathbb K_n[X]$. 
    === "Corrigé"
        On cherche donc à résoudre *l'équation aux éléments propres* $\ds D(P)=\lambda P$ d'inconnues $\ds P\in\mathbb K[X]$ non nul et $\ds \lambda\in\mathbb K$. Soit donc $\ds P$ non nul et $\ds \lambda$ tels que $\ds D(P)=\lambda P$. On a alors $\ds P'=\lambda P$. Or $\ds P$ et $\ds P'$ sont degré distincts. La seule valeur de $\ds \lambda$ possible est donc $\ds \lambda=0$ ce qui entraîne $\ds P'=0$ et par conséquent $\ds P\in\mathbb K_0[X]$. 

        Réciproquement, on vérifie que pour tout polynôme $\ds P$ constant, $\ds D(P)=0=0\times P$. 

        La seule valeur propre de $\ds D$ est donc $\ds 0$ et les vecteurs propres sont les polynômes constants. 

        Si on se restreint à $\ds \mathbb K_n[X]$, on a alors $\ds D^{n+1}=0$ donc $\ds X^{n+1}$ est un polynôme annulateur de $\ds D$. On remarque que la seule racine de ce polynôme est $\ds 0$... Comme pour la valeur propre.

!!! exemple
    === "Énoncé"
        Soit $\ds A=\begin{pmatrix}1&2&3\\0&1&0\\2&1&0\end{pmatrix}$. Déterminer les valeurs et vecteurs propres de $\ds A$. Chercher un polynôme de degré $\ds 3$ annulateur de $\ds A$, déterminer ses racines.
    === "Corrigé"
        On cherche à résoudre $\ds \begin{pmatrix}1&2&3\\0&1&0\\2&1&0\end{pmatrix}\begin{pmatrix}x\\y\\z\end{pmatrix}=\lambda\begin{pmatrix}x\\y\\z\end{pmatrix}$. 

        $$
        \begin{aligned}
        S=\begin{pmatrix}1&2&3\\0&1&0\\2&1&0\end{pmatrix}\begin{pmatrix}x\\y\\z\end{pmatrix}=\lambda\begin{pmatrix}x\\y\\z\end{pmatrix} 
        & \Leftrightarrow \begin{cases} (1-\lambda)x+2y+3z=0\\(1-\lambda)y=0\\3x+y-\lambda z=0\end{cases}\\
        \end{aligned}
        $$

        ^^Si $\ds \lambda=1$ :^^ 
        
        $$
        \begin{aligned}
        S&\Leftrightarrow \begin{cases}2x+y-z=0\\2y+3z=0\end{cases}\\
        &\Leftrightarrow \begin{cases}y=-\frac 32 z\\ x=\frac 54z\end{cases}
        \end{aligned}
        $$

        Les solutions sont donc les vecteurs $\ds \begin{pmatrix}x\\y\\z\end{pmatrix} =\alpha\begin{pmatrix}5\\-6\\4\end{pmatrix}$.

        ^^Si $\ds \lambda\ne 1$ :^^

        Dans ce cas $\ds y=0$. On a alors :

        $$
        \begin{aligned}
        S&\Leftrightarrow\begin{cases} (1-\lambda)x+3z=0\\2x-\lambda z=0\\y=0\end{cases}\\
        &\Leftrightarrow \begin{cases} (1-\lambda)x+3z=0\\(\lambda^2-\lambda-6)x=0\\y=0\end{cases}\\
        &\Leftrightarrow \begin{cases} (1-\lambda)x+3z=0\\(\lambda+2)(\lambda-3)x=0\\y=0\end{cases}
        \end{aligned}
        $$

        ^^Si $\ds \lambda=-2$ :^^

        $$
        \begin{aligned}
        S&\Leftrightarrow\begin{cases} 3x+3z=0\\y=0\end{cases}\\
        &\Leftrightarrow\begin{cases}x=-z\\y=0\end{cases}
        \end{aligned}
        $$

        Les solutions sont les vecteurs $\ds \begin{pmatrix} x\\y\\z\end{pmatrix}=\beta\begin{pmatrix}1\\0\\-1\end{pmatrix}$.

        ^^Si $\ds \lambda=3$ :^^

        $$
        \begin{aligned}
        S&\Leftrightarrow\begin{cases}-2x+3z=0\\y=0\end{cases}
        &\Leftrightarrow\begin{cases}x=\frac 32 z\\y=0\end{cases}
        \end{aligned}
        $$

        Les solutions sont les vecteurs $\ds \begin{pmatrix} x\\y\\z\end{pmatrix}=\gamma\begin{pmatrix}3\\0\\2\end{pmatrix}$.

        ^^Sinon ($\lambda\notin\{1,-2,3\}$) :^^

        $$
        \begin{aligned}
        S&\Leftrightarrow x=y=z=0
        \end{aligned}
        $$
        
        Ce qui est un cas qui ne nous intéresse pas (le vecteur propre est non nul).

        On calcule ensuite $\ds A^2=\begin{pmatrix}7&7&3\\0&1&0\\2&5&6\end{pmatrix}$ et $\ds A^3=\begin{pmatrix}13&24&21\\0&1&0\\14&15&6\end{pmatrix}$. On cherche $\ds a, b, c$ tels que $\ds A^3+aA^2+bA+cI_3=0$. En se concentrant sur les coefficients $\ds (2,2)$, $\ds (3,1)$ et $\ds (3,3)$, on obtient le système suivant : 

        $$
        \begin{cases}a+b+c=-1\\2a+2b=-14\\6a+c=-6\end{cases}
        $$ 

        dont les solutions sont $\ds a=-2$, $\ds b=-5$ et $\ds c=6$. Un polynôme annulateur est donc $\ds X^3-2X^2-5X+6$. Ses racines sont $\ds 1$, $\ds -2$ et $\ds 3$, comme les valeurs propres.


!!! exemple
    === "Énoncé"
        Soit $\ds A=\begin{pmatrix}0 & 1 \\-1 & 0\end{pmatrix}$. Quelles sont les valeurs propres de $\ds A$ ? 
    === "Corrigé"
        On cherche à résoudre l'équation aux éléments propres $\ds AX=\lambda X$ avec $\ds X$ non nul. En posant $\ds X=\begin{pmatrix}x\\y\end{pmatrix}$, on obtient le système :

        $$
        AX=\lambda X\Leftrightarrow \begin{cases} y=\lambda x\\-x=\lambda y\end{cases}\Leftrightarrow \begin{cases}(1+\lambda^2) y=0\\x=-\lambda y\end{cases}
        $$ 

        Si $\ds y=0$ alors $\ds x=0$ et $\ds X=0$ ce qui est exclu. On a alors $\ds \lambda=\pm i$. 

        Si $\ds \lambda=i$, on obtient $\ds x=-iy$ et les vecteurs propres associés sont de la forme $\ds a\begin{pmatrix}-i\\1\end{pmatrix}$ et si $\ds \lambda=-i$ on a $\ds x=iy$ et les vecteurs propres associés sont de la forme $\ds b\begin{pmatrix}i\\1\end{pmatrix}$. Les deux valeurs propres sont donc $\ds i$ et $\ds -i$. 

!!! definition "Sous-espace propre"
    Soit $\ds u\in \mathcal L(E)$, soit $\ds \lambda$ une valeur propre de $\ds u$ (on suppose donc ici qu'une valeur propre existe). On appelle *sous-espace propre de $\ds u$ associé à $\ds \lambda$* l'ensemble des vecteurs propres associés à $\ds \lambda$. On note parfois $\ds E_\lambda(u)$. On remarque que $\ds E_\lambda(u)=\ker(u-\lambda \mathrm{id}_E)$.

    On a la même définition pour une matrice $\ds A$. 

!!! exemple
    === "Énoncé"
        Quel est le sous espace propre associé à la valeur propre $0$ pour la dérivation sur les polynômes ? 
    === "Corrigé"
        On cherche tous les polynômes $P$ tels que $P'=0$ : on a donc $E_0(D)=\mathbb R_0[X]$. 

!!! propriete "Sous-espace vectoriel de vecteurs propres" 
    Soit $\ds u\in\mathcal L(E)$ et $\ds \lambda$ une valeur propre de $\ds u$. Alors $\ds E_\lambda(u)$ est un sous-espace vectoriel de $\ds E$. 

!!! preuve
    On a vu dans une remarque précédente que le vecteur nul est toujours vecteur propre. Donc $\ds E_\lambda(u)$ est non vide. Soient $\ds x$ et $\ds y$ deux vecteurs propres de $\ds u$ associés à la valeur propre $\ds \lambda$ et $\ds \alpha$ et $\ds \beta$ deux éléments de $\ds \mathbb K$. Alors $\ds u(\alpha x+\beta y)=\alpha u(x)+\beta u(y)=\alpha\lambda x+\beta \lambda y=\lambda (\alpha x+\beta y)$. Donc $\ds \alpha x+\beta y\in E_\lambda(u)$. Ainsi $\ds E_\lambda(u)$ est un sev de $\ds E$. 


!!! propriete "Stabilité des sous-espaces propres"
    Soit $\ds u\in\mathcal L(E)$ et $\ds \lambda$ une valeur propre de $\ds u$. Soit $\ds v\in\mathcal L(E)$ tel que $\ds u\circ v=v\circ u$. Alors $\ds E_\lambda(u)$ est stable par $\ds v$. 


!!! preuve
    Posons $\ds f= u-\lambda \mathrm{id}_E$. Si $\ds u\circ v=v\circ u$ alors $\ds f\circ v=v\circ f$. On en déduit que le noyau de $\ds f$ est stable par $\ds v$ c'est à dire $\ds E_\lambda(u)$ est stable par $\ds v$. 


!!! remarque 
    En particulier, $E_\lambda(u)$ est stable par $u$ lui-même.

!!! definition "Spectre"
    Si $\ds u$ est un endomorphisme de $\ds E$ avec $\ds E$ de dimension finie, on appelle *spectre* de $\ds u$ et on note $\ds \Sp(u)$ l'ensemble des valeurs propres de $\ds u$. 

    On a la même définition pour une matrice.


!!! propriete "Somme directe de sous-espaces propres"
    Soit $\ds u\in\mathcal L(E)$, soient $\ds E_{\lambda_1}(u),E_{\lambda_2}(u),\dots,E_{\lambda_p}(u)$ $\ds p$ sous-espaces propres de $\ds u$ associés à $\ds p$ valeurs propres distinctes. Alors ces sous-espaces propres sont en somme directe. 

!!! preuve 
    Effectuons une preuve par récurrence.

    ^^Cas p=2 :^^ Soient $\ds E_{\lambda_1}(u)$ et $\ds E_{\lambda_2}(u)$ deux sous-espaces propres de $\ds u$. Avec deux sous-espaces on peut utiliser la caractérisation de la somme directe par l'intersection réduite à $\ds 0$. Soit $\ds x\in E_{\lambda_1}(u)\cap E_{\lambda_2}(u)$. Alors $\ds u(x)=\lambda_1x$ et $\ds u(x)=\lambda_2x$. Donc $\ds (\lambda_1-\lambda_2)x=0$. Or $\ds \lambda_1\ne \lambda_2$ donc $\ds x=0$ et les deux espaces sont bien en somme directe.

    ^^Hérédité :^^ Soit $\ds p\in\mathbb N$ on suppose qu'une somme de $\ds p$ sous-espaces propres est directe. Montrons qu'une somme de $\ds p+1$ sous-espaces propres est directe. Soient $\ds x_i\in E_{\lambda_i}(u)$ tels que $\ds x_1+\cdots+x_p+x_{p+1}=0$. Alors $\ds u(x_1+\cdots+x_p+x_{p+1})=0$ mais aussi $\ds u(x_1+\cdots+x_p+x_{p+1})=\lambda_1x_1+\cdots+\lambda_px_p+\lambda_{p+1}x_{p+1}$. On peut ainsi écrire : $\ds x_{p+1}=-x_1-x_2-\cdots-x_p$ et $\ds \lambda_{p+1}x_{p+1}=-\lambda_1x_1-\cdots-\lambda_px_p$. En multipliant la première par $\ds \lambda_{p+1}$ et en soustrayant les deux égalités on a : $\ds (\lambda_{p+1}-\lambda_1)x_1+\cdots+(\lambda_{p+1}-\lambda_p)x_p=0$. Par hypothèse de récurrence on obtient pour tout $\ds i\in\{1,\dots,p\}$, $\ds (\lambda_{p+1}-\lambda_i)x_i=0$. Or $\ds \lambda_{p+1}\ne \lambda_i$ donc $\ds x_i=0$ pour tout $\ds i\in\{1,\dots,p\}$. On en déduit alors que $\ds x_{p+1}=0$ et enfin que la somme des sous-espaces propres est directe.

!!! propriete "famille libre de vecteurs propres"
    Soient $\ds x_1,\dots,x_p$ $\ds p$ vecteurs propres non nuls de $\ds u\in\mathcal L(E)$ associés à des valeurs propres distinctes respectives $\ds \lambda_1,\dots,\lambda_p$. Alors la famille $\ds (x_1,\dots,x_p)$ est libre. 

!!! preuve
    Soient $\ds \alpha_1,\dots,\alpha_p$ tels que $\ds \alpha_1x_1+\cdots+\alpha_p x_p=0$. Or pour tout $\ds i$, $\ds \alpha_ix_i\in E_{\lambda_i}(u)$ et d'après la propriété précédente, les $\ds E_{\lambda_i}(u)$ sont en somme directe, donc $\ds \alpha_1x_1+\cdots+\alpha_p x_p=0$ implique que pour tout $\ds i\in\{1,\dots,p\}$, $\ds \alpha_ix_i=0$. Or les $\ds x_i$ sont non-nuls donc tous les $\ds \alpha_i$ sont nuls. 

    On en déduit que la famille $\ds (x_1,\dots,x_p)$ est libre. 

!!! propriete "Valeurs propres en dimension finie"
    Une conséquence directe de la propriété précédente est qu'en dimension finie, le nombre de valeurs propres est fini. Ainsi le spectre d'un endomorphisme $\ds u$ est de cardinal fini.  

!!! propriete "Valeurs propres et polynômes annulateurs"
    Soit $\ds u\in\mathcal L(E)$ et soit $\ds P$ un polynôme annulateur de $\ds u$. Si $\ds \lambda$ est une valeur propre de $\ds u$ alors $\ds \lambda$ est racine de $\ds P$.

!!! preuve
    Soit $\ds x$ un vecteur propre non nul de $\ds u$ associé à la valeur propre $\ds \lambda$. On remarque pour commencer que $\ds u^k(x)=\lambda^k x$. Soit $\ds P$ un polynôme annulateur de $\ds u$. On a alors $\ds P(u)=0$. Posons $\ds P=p_0+p_1X+\dots+p_nX^n$, alors $\ds P(u)(x)=p_0x+p_1u(x)+\cdots+p_nu^n(x)=p_0x+p_1\lambda x+\cdots+p_m\lambda^nx=P(\lambda)x$. Or $\ds x$ est non nul donc $\ds P(\lambda)=0$ : $\ds \lambda$ est racine de $\ds P$. 


!!! warning "Attention"
    La réciproque n'est pas vraie ! 


## Polynôme caractéristique

!!! definition "Polynôme caractéristique"
    Soit $\ds A\in\mathcal M_n(\mathbb K)$, on appelle *polynôme caractéristique* de $\ds A$ le polynôme $\ds \chi_A$ défini par $\ds \chi_A(\lambda)=\det(\lambda I_n-A)$. 

    Soit $\ds u\in\mathcal L(E)$, $\ds E$ de dimension finie, on appelle *polynôme caractéristique* de $\ds u$ le polynôme $\ds \chi_u$ défini par $\ds \chi_u(\lambda)=\det(\lambda \mathrm{id}_E-u)$. 

!!! exemple
    === "Énoncé"
        Calculer le polynôme caractéristique de $\begin{pmatrix}3 & -1 & 1\\-1 & 3 & 1\\-1 & 0 & 4\end{pmatrix}$.
    === "Corrigé"
        On peut en réalité donner le nom que l'on veut à la variable. Calculons :

        $$
        \begin{aligned}
        \det(XI_n-A) &= \begin{vmatrix} X-3& 1 & -1 \\ 1 & X-3 & -1 \\ 1 & 0 & X-4\end{vmatrix}\\
        &=\begin{vmatrix}X-4 & 4-X & 0 \\1 & X-3 & -1 \\ 1 & 0 & X-4\end{vmatrix}& L_1 \leftarrow L_1-L_2\\
        &=(X-4)\begin{vmatrix}1 & -1 & 0\\1 & X-3 & -1 \\ 1 & 0 & X-4\end{vmatrix}\\
        &=(X-4)\begin{vmatrix}1 & -1 & 0\\0 & X-2 & -1 \\ 0 & 1 & X-4\end{vmatrix}&L_2\leftarrow L_2-L_1\quad L_3\leftarrow L_3-L_1\\
        &=(X-4)\begin{vmatrix}X-2 & -1 \\ 1 & X-4 \end{vmatrix}\\
        &=(X-4)\begin{vmatrix}X-3 & -1 \\ X-3 & X-4\end{vmatrix}& C_1\leftarrow L_1+C_2\\
        &=(X-4)(X-3)\begin{vmatrix} 1 & -1 \\ 1 & X-4\end{vmatrix}\\
        &=(X-4)(X-3)^2
        \end{aligned}
        $$

!!! exemple
    === "Énoncé"
        Soit $A$ triangulaire. Calculer son polynôme caractéristique.
    === "Corrigé"
        Si $A$ est triangulaire, notons $a_1,a_2,\dots,a_n$ ses termes diagonaux. Alors $\chi_A=\prod_{i=1}^n(X-a_i)$. 

!!! propriete "Le polynôme caractéristique est bien un polynôme"
    Soit $\ds A\in\mathcal M_n(\mathbb K)$ et $\ds \chi_A$ son polynôme caractéristique. Alors $\ds \chi_A$ est un polynôme unitaire de degré $\ds n$. De plus son coefficient d'ordre $\ds 0$ vaut $\ds (-1)^n\det A$ et son coefficient d'ordre $\ds n-1$ vaut $\ds -\tr(A)$.

    On a évidemment la même propriété pour un endormorphisme d'un espace vectoriel de dimension finie.

Pour la démonstration nous aurons besoin du lemme suivant : 

!!! lemme 
    Soient $\ds A$ et $\ds B$ deux éléments de $\ds \mathcal M_n(\mathbb K)$. La fonction $\ds x\mapsto \det(xB-A)$ est une fonction polynômiale de degré inférieur ou égal à $\ds n$. 

!!! preuve "preuve du lemme"
    Montrons cette propriété par récurrence sur $\ds n$. 

    ^^Initialisation :^^ Si $\ds n=1$, il n'y a aucun problème.

    ^^Hérédité :^^ Soit $\ds n\in\mathbb N$. Supposons la propriété vérifiée au rang $\ds n$. Soit $\ds A$ et $\ds B$ deux matrices de taille $\ds n+1$. Pour tout couple $\ds i,j\in\{1,\dots,n+1\}^2$, on pose $\ds \alpha_{ij}$ et $\ds \beta_{ij}$ les coefficients $\ds (i,j)$ respectifs de $\ds A$ et $\ds B$, et $\ds A_{ij}$ et $\ds B_{ij}$ les matrices obtenues respectivement à partir de $\ds A$ et $\ds B$ en retirant la ligne $\ds i$ et la colonne $\ds j$. On fait de même pour la matrice $\ds C=xB-A$. On remarque qu'on a les relations suivantes : $\ds \gamma_{ij}=x\beta_{ij}-\alpha_{ij}$ et $\ds C_{ij}=xB_{ij}-A_{ij}$. 

    D'après la formue le développement par rapport à la première ligne, on peut écrire : 

    $$
    \begin{aligned}
    \det(C)&=\sum_{j=1}^{n+1}(-1)^{j+1}\gamma_{ij}\det(C_{ij})\\
    &=\sum_{j=1}^{n+1}(-1)^{j+1}(x\beta_{ij}-\alpha_{ij})\det(xB_{ij}-A_{ij})
    \end{aligned}
    $$  

    Or par hypothèse de récurrence, $\ds \det(xB_{ij}-A_{ij})$ est un polynôme de degré au plus $\ds n$. Donc $\ds \det(xB-A)$ est une somme de polynômes de degrés inférieur ou égal à $\ds n+1$ : c'est un polynôme de degré inférieur ou égal à $\ds n+1$. 

    Cela conclue la démonstration par récurrence. 

!!! preuve "preuve de la propriété"
    D'après le lemme, on sait que $\ds \det(xI_n-A)$ est un polynôme de degré inférieur ou égal à $\ds n$. On note $\ds C=xI_n-A$, $\ds A'$ la matrice obtenue à partir de $\ds A$ en supprimant la première ligne et la première colonne et comme précédemment, $\ds \Delta_j$ la matrice obtenue à partir de $\ds C$ en supprimant la première ligne et la jè colonne. On a alors, par un développement par rapport à la première ligne, $\ds \det(\lambda I_n-A)=(\lambda-a_{11})\det(\lambda I_{n-1}-A')+\sum_{j=2}^{n}(-1)^{j+1}\det\Delta_j$. 
    
    La matrice $\Delta_j$ a une forme particulière : sa première ligne ne comporte pas de $\lambda$. En développant $\Delta_j$ par rapport à cette ligne, on obtient donc, d'après le lemme, une somme de polynômes de degré au plus $n-2$ : $\Delta_j$ est donc un polynôme de degré au plus $n-2$, et par conséquent $\ds \sum_{j=2}^{n}(-1)^{j+1}\det\Delta_j$ aussi.

    Ainsi, les coefficients d'ordre $\ds n$ et $\ds n-1$ du déterminant de $C$ sont donnés par la quantité $\ds (X-a_{11})\det(A')$ et en itérant le processus à $A'$ et les matrices suivantes, les coefficients de degré $\ds n$ et $\ds n-1$ de $\ds \det(\lambda I_n-A)$ sont exactement ceux de $\ds \prod_{i=1}^n(\lambda-a_{ii})$ : le coefficient d'ordre $\ds n$ est $\ds 1$ et celui d'ordre $\ds n-1$ est $\ds \sum a_ii$ par relations coefficients racines, c'est donc $\ds \tr(A)$. 

    Enfin, le coefficient d'ordre $\ds 0$ est obtenu en calculant $\ds \chi_A(0)=\det(0I_n-A)=\det(-A)=(-1)^n\det(A)$ par les propriétés classiques du déterminant. 


Ainsi, on peut écrire le polynôme caractéristique sous la forme : 

$$
\chi_A(\lambda)=\lambda^n-\tr(A)\lambda^{n-1}+\cdots+(-1)^n\det A
$$

!!! propriete "Spectre et racines"
    Soit $\ds A\in\mathcal M_n(\mathbb K)$, notons $\ds Z(\chi_A)$ l'ensemble des racines de $\ds \chi_A$. Alors $\ds \Sp(A)=Z(\chi_A)$. 

    On a exactement la même propriété avec un endomorphisme $\ds u$ en dimension finie. 

!!! preuve
    Soit $\ds \lambda$ une valeur propre de $\ds A$. Alors $\ds \ker(\lambda I_n-A)$ n'est pas réduit à $\ds 0$. Ainsi, la matrice $\ds \lambda I_n-A$ n'est pas inversible et $\ds \det(\lambda I_n-A)=0$. On en déduit que $\ds \chi_A(\lambda)=0$ et donc $\ds \lambda\in Z(\chi_A)$. 

    Réciproquement, si $\ds \lambda\in Z(\chi_A)$ alors $\ds \det(\lambda I_n-A)=0$ et $\ds \lambda I_n-A$ n'est pas inversible : il existe $\ds X$ non nul tel que $\ds AX=\lambda X$ et donc $\ds \lambda\in\Sp(A)$. 


Ainsi, pour connaître les valeurs propres d'une matrice, ou d'un endomorphisme, il suffit de calculer et de factoriser son polynôme caractéristique. 

!!! exemple
    === "Énoncé"
        Donner les valeurs propres de $\ds A=\begin{pmatrix}6&-6&5\\-4&-1&10\\7&-6&4\end{pmatrix}$. 
    === "Corrigé"
        On calcule intelligemment le polynôme caractéristique. Il est souvent plus profitable de réfléchir à comment calculer le déterminant plutôt qu'à factoriser un déterminant complètement développé. 

        $$
        \begin{aligned}
        \det(X I_3-A) &= \begin{vmatrix}X-6 & 6 & -5\\4 & X+1 & -10 \\ -7 & 6 & X-4\end{vmatrix}\\
        &= \begin{vmatrix}X-5 & 6 & -5\\X-5 & X+1 & -10 \\ X-5 & 6 & X-4\end{vmatrix}&\text{en effectuant }C_1\leftarrow C_1+C_2+C_3\\
        &=(X-5)\begin{vmatrix}1 & 6 & -5\\1 & X+1 & -10 \\ 1 & 6 & X-4\end{vmatrix}\\
        &=(X-5)\begin{vmatrix}1 & 0 & 0 \\ 1 & X-5 & -5 \\ 1 & 0 & X+1\end{vmatrix}&C_2\leftarrow C_2-6C_1\quad C_3\leftarrow C_3+5C_1\\
        &=(X-5)\begin{vmatrix}X-5 & -5 \\ 0 & X-1\end{vmatrix}\\
        &=(X-5)^2(X+1)
        \end{aligned}
        $$

        Donc les valeurs propres sont $\ds 5$ et $\ds -1$. On dira que $\ds 5$ est valeur propre de multiplicité $\ds 2$ 


!!! definition "Multiplicité d'une valeur propre"
    La *multiplicité* d'une valeur propre $\ds \lambda$ est la multiplicté de $\ds \lambda$ en tant que racine du polynôme caractéristique.

!!! propriete "Polynôme Caractéristique et similitude"
    Deux matrices semblables ont le même polynôme caractéristique, donc les mêmes valeurs propres avec la même multiplicité.

!!! preuve 
    Cela découle directement de l'invariance par similitude du déterminant. En effet, soient $\ds A$ et $\ds B$ deux matrices de $\ds \mathcal M_n(\mathbb K)$ semblables. Soit $\ds P$ inversible telle que $\ds P^{-1}AP=B$. On a alors pour tout $\ds \lambda\in\mathbb K$, $\ds \chi_B(\lambda)=\det(\lambda I_n-B)=\det(\lambda P^{-1}P-P^{-1}AP)=\det(P^{-1}(\lambda I_n-A)P)=\det(P^{-1})\det(\lambda I_n-A)\det(P)=\det(P)^{-1}\det(P)\det(\lambda I_n-A)=\det(\lambda I_n-A)=\chi_A(\lambda)$.

!!! propriete "Polynôme caractéristique et complexes"
    Une matrice ou un endomorphisme dans $\ds \mathbb R$ n'admet pas toujours des valeurs propres réelles. Par contre, un endomorphisme ou une matrice sur $\ds \mathbb C$ admet toujours des racines sur $\ds \mathbb C$ et son polynôme caractéristique est scindé.

!!! preuve
    Cela découle du théorème de d'Alembert Gauss : les polynômes sont scindés dans $\ds \mathbb C$. 

!!! propriete "Polynôme caractéristique et transposée"
    Soit $\ds A\in\mathcal M_n(\mathbb K)$, alors $\ds \chi_{A^\top}=\chi_A$. Donc $\ds A$ et $\ds A^\top$ ont mêmes valeurs propres avec la même multiplicité.

!!! preuve
    Cela découle directement de l'invariance du déterminant par transposition.


!!! propriete "Polynôme caractéristique et sous-espaces stables"
    Soit $\ds u$ un endomorphisme de $\ds E$ en dimension finie. Soit $\ds F$ un sous-espace stable par $\ds u$. Notons $\ds v$ la restriction de $\ds u$ à $\ds F$. Alors $\ds \chi_v$ divise $\ds \chi_u$. 

!!! preuve
    Pour cette preuve, on passe par le point de vue matriciel : c'est à peu près la seule façon de travailler avec des déterminants d'endomorphismes. Puisque le déterminant est invariant par similitude, on peut choisir une base qui nous arrange. Choissons donc une base $\ds (e_1,\dots,e_p,e_{p+1},\dots,e_n)$ adaptée à $\ds F$ : $\ds (e_1,\dots,e_p)$ est une base de $\ds F$. Notons $\ds A$ la matrice de $\ds u$ dans cette base et $\ds A'$ la matrice de $\ds v$ dans la base $\ds (e_1,\dots,e_p)$.  
    D'après le point de vue matriciel des sous-espaces stables, la matrice de $\ds A$ peut s'écrire par blocs sous la forme : $\ds A=\begin{pmatrix}A'&B\\0&C\end{pmatrix}$. Le polynôme caractéristique de $\ds u$ est aussi celui de $\ds A$ et d'après les propriétés de déterminants par blocs, la matrice $\ds A$ étant triangulaire par blocs, on a $\ds \det(\lambda I_n-A)=\det(\lambda I_p-A')\det(\lambda I_{n-p}-C)$. On a donc $\ds \chi_A=\chi_{A'}\chi_C$ et donc $\ds \chi_{A'}$ divise $\ds \chi_A$, c'est à dire $\ds \chi_v$ divise $\ds \chi_u$. 

!!! propriete "multiplicité et dimension"
    Soit $\ds A$ une matrice de $\ds \mathcal M_n(\mathbb K)$. Soit $\ds \lambda$ une valeur propre de $\ds A$. Soit $\ds \mu$ la multiplicité de $\ds \lambda$ en tant que racine de $\ds \chi_A$. Alors $\ds \dim(E_\lambda(A))\le \mu$.

    Soit $\ds u$ un endomorphisme d'un espace vectoriel $\ds E$ de dimension finie. Soit $\ds \lambda$ une valeur propre de $\ds u$. Soit $\ds \mu$ la multiplicité de $\ds \lambda$ en tant que valeur propre de $\ds \chi_u$. Alors $\ds \dim(E_\lambda(u))\le\mu$. 

!!! preuve
    Nous allons nous concentrer sur le point de vue endomorphisme, mais en passant par la représentation matricielle. 

    On sait que $\ds u$ commute avec lui-même, donc $\ds E_\lambda(u)$ est stable par $\ds u$. D'après la propriété précédente, le polynôme caractéristique $\ds \chi_v$ de la restriction $\ds v$ de $\ds u$ à $\ds E_\lambda(u)$ divise $\ds \chi_u$. Or si $\ds x\in E_\lambda(u)$, alors $\ds u(x)=\lambda x$. On en déduit que la matrice de $\ds v$ est de la forme $\ds \lambda I_p$ où $\ds p$ est la dimension de $\ds E_\lambda(u)$. Or le polynôme caractéristique de $\ds \lambda I_p$ est $\ds (X-\lambda)^p$. On en déduit donc que $\ds (X-\lambda)^p$ divise $\ds \chi_u$. Ainsi, la mulitplicité de $\ds \lambda$ comme racine de $\ds \chi_u$ est supérieure ou égale à $\ds p$, c'est à dire $\ds \dim(E_\lambda(u))\le \mu$. 


!!! exemple 
    === "Énoncé"
        Soit $\ds A=\begin{pmatrix}2 & 2 & -3\\5 & 1 & -5\\ -3 & 4 & 0\end{pmatrix}$. Calculer $\ds \chi_A(A)$. 
    === "Corrigé"
        Le calcul développé du polynôme caractéristique donne $\ds \chi_A=X^3-3X^2+9X-47$. Calculons $\ds A^2$ et $\ds A^3$ : $\ds A^2=\begin{pmatrix}17 & 2 & -12\\30 & -9 & -10\\14 & -2 & -17\end{pmatrix}$ et $\ds A^3=\begin{pmatrix}80 & -12 & -27 \\45 & 11 & 15\\69 & -42 & -4\end{pmatrix}$. On calcule alors, si on a du courage, $\ds A^3-3A^2+9A-47I_3=0_3$. 

!!! exemple 
    === "Énoncé"
        Soit $\ds A=\begin{pmatrix}1 & 0 & 0 \\ 0 & 2 & 0 \\ 0 & 0 & 2\end{pmatrix}$. Calculer $\ds \chi_A$ et $\ds \chi_A(A)$. Existe-t-il un polynôme annulateur de $\ds A$ de degré inférieur ? 
    === "Corrigé"
        On calcule très simplement le polynôme caractéristique de $\ds A$ : $\ds \chi_A=(X-1)(X-2)^2=X^3-5X^2+8X-4$. On vérifie «rapidement» que $\ds \chi_A(A)=0$. On sait ensuite qu'un polynôme annulateur de $\ds A$ doit admettre comme racines ^^toutes^^ les valeurs propres de $\ds A$. Ici, les valeurs propres de $\ds A$ sont $\ds 1$ et $\ds 2$. Donc le seul polynôme annulateur possible de degré $\ds 2$ (il n'y en a pas de degré $\ds 1$) est le polynôme $\ds (X-1)(X-2)=X^2-3X+2$. Et on vérifie que $\ds A^2-3A+2I_3=0$. Donc le polynôme caractéristique semble être un polynôme annulateur mais ce n'est pas le *plus petit* possible. 

!!! propriete "Théorème de Cayley-Hamilton"
    Le polynôme caractéristique d'une matrice carrée ou d'un endomorphisme en dimension finie est un polynôme annulateur de cette matrice ou de cet endomorphisme.

!!! preuve
    La preuve est hors programme.


## Diagonalisation en dimension finie

$E$ est un espace vectoriel de dimension finie.

!!! definition "Endomorphisme diagonalisable"
    Un endomorphisme d'un espace vectoriel de dimension finie est dit diagonalisable s'il existe une base de $\ds E$ dans laquelle sa matrice est diagonale.

    Une matrice carrée est dite diagonalisable si elle est semblable à une matrice diagonale.

!!! propriete "base de vecteurs propres"
    Un endomorphisme $\ds u$ est diagonalisable si et seulement si il existe une base de $\ds E$ composée de vecteurs propres de $\ds u$. 

!!! preuve
    Si $\ds u$ est diagonalisable, alors il existe une base de $\ds E$ dans laquelle $\ds u$ est diagonale. Soit $\ds (e_1,\dots,e_n)$ cette base, on a donc par définition de la matrice d'un endomorphisme, $\ds u(e_i)=\lambda_i e_i$ et donc $\ds e_i$ est un vecteur propre.

    Réciproquement, s'il existe une base de vecteurs propres, alors dans cette base, la matrice de $\ds u$ est évidemment diagonale. 


!!! exemple
    === "Énoncé"
        Soit $A=\begin{pmatrix}1 & -1 & 0 \\ 0 & 2 & 0\\2 & 1 & 3\end{pmatrix}$. Calculer $A^n$ pour tout entier $n\ge 0$.
    === "Corrigé"
        On commence par vérifier si $A$ est diagonalisable. Si c'est le cas, tout sera plus facile. On a donc :
        
        $$
        \begin{aligned}
        \chi_A&=\begin{vmatrix}X-1 & 1 & 0 \\ 0 & X-2 & 0 \\ -2 & -1 & X-3\end{vmatrix}\\
        &=(X-3)\begin{vmatrix}X-1 & 1 \\ 0 & X-2 \end{vmatrix}\\
        &=(X-1)(X-2)(X-3)
        \end{aligned}
        $$

        On a donc $3$ racines simples qui correspondent à trois valeurs propres. Cherchons les sous-espaces propres associés : il y en a $3$ donc on sait déjà qu'ils sont de dimenion $1$ et en associant 1 vecteur de chaque sous-espaces propre, on aura une famille libre à trois éléments, donc une base de vecteurs propres : $A$ est diagonalisable.

        Diagonalisons donc la. On cherche une base de vecteurs propres.

        $$
        AX=X\Leftrightarrow \begin{cases}x-y=x\\2y=y\\2x+y+3z=z\end{cases}\Leftrightarrow \begin{cases}y=0\\x=-z\end{cases}
        $$

        donc un vecteur propre associé à la valeur propre $1$ est $\begin{pmatrix}1\\0\\-1\end{pmatrix}$.

        $$
        AX=2X\Leftrightarrow \begin{cases}x-y=2x\\2y=2y\\2x+y+3z=2z\end{cases}\leftrightarrow \begin{cases}y=-x\\z=-x\end{cases}
        $$

        donc un vecteur propre associé à la valeur propre $2$ est $\begin{pmatrix}-1\\1\\1\end{pmatrix}$.

        $$
        AX=3X\Leftrightarrow \begin{cases}x-y=3x\\2y=3y\\2x+y+3z=3z\end{cases}\leftrightarrow \begin{cases}x=0\\y=0\end{cases}
        $$

        donc un vecteur propre associé à la valeur propre $3$ est $\begin{pmatrix}0\\0\\1\end{pmatrix}$.

        Posons alors $P$ la matrice de cette famille : $P=\begin{pmatrix}1 & -1 & 0\\0 & 1  & 0\\-1 & 1 & 1\end{pmatrix}$. En appliquant les formules de changement de base on a (et on peut le vérifier) $A=P\begin{pmatrix} 1 & 0 & 0\\0 & 2 & 0\\0 & 0 & 3\end{pmatrix}P^{-1}$. On en déduit que $A^n=P\begin{pmatrix} 1 & 0 & 0\\0 & 2^n & 0\\0 & 0 & 3^n\end{pmatrix}P^{-1}$. Ce qui donne après calculs : 

        $$
        A^n = \begin{pmatrix} 1 & 1-2^n & 0\\0 & 2^n & 0\\-1+3^n & -1+2^n & 3^n\end{pmatrix}.
        $$

!!! propriete "Caractérisation des endomorphismes diagonalisables"
    Soit $\ds \Sp(u)=\{\lambda_1,\dots,\lambda_p\}$ le spectre de $\ds u$. Les trois propriétés suivantes sont équivalentes :
    
    - i) l'endomorphisme $\ds u$ est diagonalisable
    
    - ii) $\ds \ds\bigoplus_{i=1}^p E_{\lambda_i}(u)=E$
    
    - iii) $\ds \ds\sum_{i=1}^p\dim(E_{\lambda_i}(u))=\dim E$

!!! preuve
    Comme souvent dans ces cas-là, nous allons montrer $\ds i)\Rightarrow ii)\Rightarrow iii)\Rightarrow i)$. 

    Supposons i), montrons ii). $\ds u$ est donc diagonalisable et il existe une base $\ds (e_1,\dots,e_n)$ de vecteurs propres. Chacun de ces vecteurs est élément de l'un des $\ds \lambda_i$. L'espace engendré par ces vecteurs est donc inclu dans $\ds \ds\bigoplus_{i=1}^p E_{\lambda_i}(u)$. Or cet espace engendré est $\ds E$ : on a donc $\ds E\subset \ds\bigoplus_{i=1}^p E_{\lambda_i}(u)$ et ainsi $\ds E=\ds\bigoplus_{i=1}^p E_{\lambda_i}(u)$. 

    Supposons ii), montrons iii). On a $\ds \ds\bigoplus_{i=1}^p E_{\lambda_i}(u)=E$ et donc $\ds \sum_{i=1}^p \dim(E_{\lambda_i}(u))=\dim E$. 

    Supposons iii), montrons i). Prenons une base de chacun des $\ds E_{\lambda_i}(u)$ : l'union de ces bases est une famille libre de $\ds E$, et l'égalité des dimensions permet d'affirmer que cette famille libre est de cardinal $\ds n$, donc maximale : c'est une base de $\ds E$. Or elle est composée de vecteurs propres : $\ds u$ est diagonalisable.


!!! propriete "Caractérisation des matrices diagonalisables"
    Soit $\ds \Sp(A)=\{\lambda_1,\dots,\lambda_p\}$ le spectre de $\ds A\in\mathcal M_n(\mathbb K)$. Les trois propriétés suivantes sont équivalentes :
    
    - i) la matrice $\ds A$ est diagonalisable
    
    - ii) $\ds \ds\bigoplus_{i=1}^p E_{\lambda_i}(A)=\mathbb K^n$
    
    - iii) $\ds \ds\sum_{i=1}^p\dim(E_{\lambda_i}(A))=n$

!!! exemple
    === "Énoncé"
        Montrer que $A=\begin{pmatrix} 1 & 1 & 0 \\ 0 & 2 & 0 \\ 0 & 0 & 2\end{pmatrix}$ est diagonalisable.
    === "Corrigé"
        $A$ étant triangulaire supérieure, on lit ses valeurs propres sur la diagonale : $1$ et $2$. Intéressons-nous aux sous-espaces propres. $1$ étant valeur propre de multiplicité $1$, $E_1(A)$ est de dimension $1$.

        $$
        AX=2X\Leftrightarrow \begin{cases} x+y=2x\\2y=2y\\2z=2z\end{cases}\Leftrightarrow y=x
        $$

        Donc $E_2(A)$ est le plan d'équation $y=x$ qui est de dimension $2$. Donc $E_2(A)$ est de dimension $2$.

        On observe donc que $\dim(E_1(A))+\dim(E_2(A))=3=\dim E$. Donc $A$ est diagonalisable : elle est semblable à la matrice $\begin{pmatrix} 1 & 0 & 0 \\ 0 & 2 & 0\\ 0 & 0 & 2\end{pmatrix}$. 

!!! exemple
    === "Énoncé"
        Montrer que les projecteurs et les symétries, en dimension finie, sont diagonalisables.
    === "Corrigé"
        Soit $\ds p$ un projecteur non trivial (non nul et différent de l'identité), alors $\ds p\circ p=p$ donc $\ds p$ est annulé par le polynôme $\ds X^2-X$. Ses deux valeurs propres possibles sont donc $\ds 0$ et $\ds 1$. On a $\ds E_{0}(p)=\ker (p)$ et $\ds E_1(p)=\ker(\mathrm{id}_e-p)=\im(p)$. Or par définition d'un projecteur, $\ds E=\ker(p)\oplus\im(p)$. On en déduit par le point ii) que $\ds p$ est diagonalisable. On peut obtenir une matrice diagonale de la forme $\ds \mathrm{diag}(1,\dots,1,0,\dots,0)$.

        Soit $\ds s$ une symétrie, alors $\ds s\circ s=\mathrm{id}_E$. Un polynôme annulateur de $\ds s$ est donc $\ds X^2-1$ dont les racines sont $\ds 1$ et $\ds -1$ qui sont les deux valeurs propres possibles. Or par définition d'une symétrie, $\ds E=\ker(\mathrm{id}_E-s)\oplus\ker(\mathrm{id}_E+s)$ et donc toujours d'après le point ii), $\ds s$ est diagonalisable et on peut obtenir une matrice diagonale de la forme $\ds \mathrm{diag}(1,\dots,1,-1,\dots,-1)$. 


!!! propriete "Caractérisation par le polynôme caractéristique"
    Un endomorphisme est diagonalisable si et seulement si son polynôme caractéristique est scindé sur $\ds \mathbb{K}$ et si, pour toute valeur propre, la dimension du sous-espace propre associé est égale à sa multiplicité.

!!! preuve
    Si $\ds u$ est diagonalisable alors $\ds \ds E=\bigoplus_{i=1}^{n}E_{\lambda_i}(u)$. Notons $\ds n_i$ la dimension de $\ds E_{\lambda_i}(u)$, alors $\ds u_i=u_{\big| E_{\lambda_i}(u)}=\lambda_iI_{n_i}$. Or le polynôme caractéristique de chaque $\ds u_i$ divise celui de $\ds u$. Ces polynômes caractéristiques étant premiers entre eux car les $\ds \lambda_i$ sont distincts, on a $\ds \prod_{i=1}^p(X-\lambda_i)^{n_i}$ qui divise $\ds \chi_u$. Or $\ds \sum_{i=1}^pn_i=n$ donc $\ds \chi_u=\prod_{i=1}^p (X-\lambda_i)^{n_i}$ : le polynôme caractéristique est scindé et la multiplicité de chaque $\ds \lambda_i$ est égale à la dimension du sous-espace propre associé.

    Réciproquement, si $\ds \chi_u$ est scindé, notons $\ds \chi_u=\prod_{i=1}^p(X-\lambda_i)^{m_i}$. On suppose de plus que pour tout $\ds i$, $\ds m_i=\dim(E_{\lambda_i}(u))$. Or $\ds \chi_u$ étant scindé, $\ds \sum_{i=1}^p m_i=n$ c'est à dire $\ds \sum_{i=1}^p E_{\lambda_i}(u)=\dim E$ : $\ds u$ est diagonalisable (point iii)). 

!!! exemple
    === "Énoncé"
        Soit $A=\begin{pmatrix}1 & -1 & 1\\-1 & 1 & -1 \\ 1 & -1 & 1\end{pmatrix}$. Montrer que $A$ est diagonalisable.
    === "Corrigé"
        Calculons le polynôme caractéritique de $A$ :

        $$
        \begin{aligned}
        \chi_A&=\begin{pmatrix}X-1 & 1 & -1\\1 & X-1 & 1\\-1 & 1 & X-1\end{pmatrix}\\
        &=\begin{pmatrix}X & 1 & -1\\X & X-1 & 1\\0 & 1 & X-1\end{pmatrix}&C_1\leftarrow C_1+C_2\\
        &=X\begin{pmatrix}1 & 0 & -1\\1 & X & 1\\0 & X & X-1\end{pmatrix} & C_2 \leftarrow C_2+C_3\\
        &=X^2\begin{pmatrix}1 & 0 & -1\\1 & 1 & 1\\0 & 1 & X-1\end{pmatrix}\\
        &=X^2\begin{pmatrix}1 & 0 & 0 \\ 1 & 1 & 0 \\0 & 1 & X-3\end{pmatrix}& C_3\leftarrow C_3+C_1-2C_2\\
        &=X^2(X-3)
        \end{aligned}
        $$

        Ainsi $A$ admet deux valeurs propres $0$ et $3$. $3$ est de multiplicité $1$ donc $\dim(E_3(A))=1$. Pour avoir la diagonalisabilité, il faut se concentrer sur $E_0(A)$. Or si $X=\begin{pmatrix}x\\y\\z\end{pmatrix}$ alors $AX=0\Leftrightarrow x-y+z=0$ qui est l'équation d'un hyperplan. On en déduit que $E_0(A)$ es de dimension $2$, tout comme la multiplicité de $0$ comme racine du polynôme caractéristique : $A$ est donc diagonalisable. 


!!! propriete "valeurs propres distinctes"
    Un endomorphisme d'un espace vectoriel de dimension $\ds n$ admettant $\ds n$ valeurs propres distinctes est diagonalisable.

!!! preuve
    Soit $\ds u$ un endomorphisme de $\ds E$ de dimension $\ds n$. On suppose que $\ds u$ admet $\ds n$ valeurs propres distinctes. Alors son polynôme caractéristique est scindé à racines simples (il est de degré $\ds n$ et admet $\ds n$ racines distinctes). En général, si $\ds m_i$ est la multiplicité de $\ds \lambda_i$ en tant que racine de $\ds \chi_u$, on sait que $\ds \dim(E_{\lambda_i}(u))\le m_i$. Mais ici, les $\ds m_i$ sont tous égaux à $\ds 1$ et évidemment $\ds \dim(E_{\lambda_i}(u))\ge 1$. On a donc $\ds \dim(E_{\lambda_i}(u))=m_i=1$ et d'après la propriété précédente, $\ds u$ est diagonalisable. 


!!! remarque 
    Cette propriété permet de dire que si le polynôme caractéristique est scindé à racines simples alors $\ds u$ (ou $\ds A$) est diagonalisable.

!!! exemple
    === "Énoncé"
        Soit $A=\begin{pmatrix} 1 & 1 & 0\\0 & 2 & 0\\-2 & 2 & 3\end{pmatrix}$. Montrer que $A$ est diagonalisable. Donner un base de vecteurs propres.
    === "Corrigé"
        On calcule le polyôme caractéristique :

        $$
        \begin{aligned}
        \chi_A &= \begin{vmatrix}X-1 & -1 & 0\\0 & X-2 & 0 \\ 2 & -2 & X-3\end{vmatrix}\\
        &= (X-3)\begin{vmatrix} X-1 & -1 & 0 & X-2 \end{vmatrix}\\
        &= (X-1)(X-2)(X-3)
        \end{aligned}
        $$

        Le polynôme caractéristique est scindé à racines simples donc $A$ est diagonalisable. Cherchons des vecteurs propres associés : 

        $$
        AX=X \Leftrightarrow \begin{cases}x+y=x\\2y=y\\-2x+2y+3z=z\end{cases}\Leftrightarrow\begin{cases}y=0\\x=z\end{cases}
        $$

        donc un vecteur propre associé à la valeur propre $1$ est $\begin{pmatrix}1\\0\\1\end{pmatrix}$.

        $$
        AX=2X\Leftrightarrow \begin{cases}x+y=2x\\2y=2y\\-2x+2y+3z=2z\end{cases}\Leftrightarrow\begin{cases}z=0\\x=y\end{cases}
        $$

        donc un vecteur propre associé à la valeur propre $2$ est $\begin{pmatrix}1\\1\\0\end{pmatrix}$.

        $$
        AX=3X\Leftrightarrow \begin{cases}x+y=3x\\2y=3y\\-2x+2y+3z=3z\end{cases}\Leftrightarrow\begin{cases}x=0\\y=0\end{cases}
        $$

        donc un vecteur propre associé à la valeur propre $3$ est $\begin{pmatrix}0\\0\\1\end{pmatrix}$.

        Posons $P$ la matrice de cette famille : $P=\begin{pmatrix} 1 & 1 & 0 \\ 0 & 1 & 0 \\ 1 & 0 & 1\end{pmatrix}$, $P^{-1}=\begin{pmatrix} 1 & -1 & 0 \\ 0 & 1 & 0 \\ -1 & 1 & 1\end{pmatrix}$. On vérifie alors que $P^{-1}AP=\begin{pmatrix}1 & 0 & 0\\0 & 2 & 0\\0 & 0 & 3\end{pmatrix}$. 

## Diagonalisabilité et polynômes annulateurs

!!! propriete "Caractérisation par les polynômes annulateurs"
    Un endomorphisme est diagonalisable si et seulement s'il admet un polynôme annulateur scindé à racines simples.

!!! preuve
    Si $\ds u$ est diagonalisable, alors il existe une base $\ds \mathcal B$ telle que $\ds mat_{\mathcal B}(u)=\diag(\lambda_1,\dots,\lambda_1,\lambda_2,\dots,\dots,\lambda_p)$ où les $\ds \lambda_i$ sont distincts. Posons $\ds P=\prod_{i=1}^p(X-\lambda_i)$, on a $\ds P(mat_{\mathcal B}(u))=\diag(P(\lambda_1),\dots,P(\lambda_1),P(\lambda_2),\dots,\dots,P(\lambda_p))=0_n$. Donc $\ds P$ est bien un polynôme scindé à racines simples qui annule $\ds u$.

    La réciproque n'est pas au programme. 

!!! propriete "Diagonalisabilité de l'endormorphisme induit sur un sous-espace stable"
    Si $\ds u\in\mathcal L(E)$ est un endomorphisme diagonalisable et si $\ds F$ est un sous-espace de $\ds E$ stable par $\ds u$, alors $\ds u_{\big|F}$ est diagonalisable. 

!!! preuve
    $\ds u$ est diagonalisable, donc il existe $\ds P$ scindé à racines simples sur annule $\ds u$. Soit $\ds x\in F$, alors $\ds P(u_{\big|F})(x)=P(u)(x)=0$ donc $\ds u_{\big|F}$ est annulé par le polynôme $\ds P$ qui est scindé à racines simples : $\ds u_{\big |F}$ est bien diagonalisable.


!!! propriete "Plus petit polynôme annulateur"
    Un endomorphisme $\ds u$ est diagonalisable si et seulement s'il admet $\ds \prod_{\lambda \in S p(u)}(X-\lambda)$ pour polynôme annulateur.

!!! preuve 
    Si $\ds u$ admet $\ds \prod_{\lambda \in S p(u)}(X-\lambda)$ pour polynôme annulateur, alors $\ds u$ est annulé par un polynôme scindé à racines simples, $\ds u$ est donc diagonalisable.

    Si $\ds u$ est diagonalisable, on a montré dans la preuve de la propriété précédente que $\ds \prod_{\lambda \in S p(u)}(X-\lambda)$ annulait $\ds u$. 

!!! exemple 
    === "Énoncé"
        Soit $A=\begin{pmatrix}-1 & 4 & 0\\0 & 3 & 0\\-4 & 4 & 3\end{pmatrix}$. Montrer que $A$ est diagonalisable.
    === "Corrigé"
        Le calcul du polynôme caractéristique donne $\chi_A=(X+1)(X-3)^2$. Pour savoir si $A$ est diagonalisable, on va vérifier si $(X+1)(X-3)=X^2-2X-3$ est un polynôme annulateur de $A$. Tout d'abord $A^2=\begin{pmatrix}1 & 8 & 0\\0 & 9 & 0\\-8 & 8 & 9\end{pmatrix}$. On a alors (les calculs ne sont pas détaillés) $A^2-2A-3I_3=\dots=0_3$. Ainsi $A$ est bien diagonalisable. Cette méthode s'applique bien ici car il n'y a que deux valeurs propres. Au-delà, les calculs ne sont pas forcément pertinents.

## Trigonalisation en dimension finie

!!! definition "Endomorphisme trigonalisable"
    Un endomorphisme d'un espace vectoriel de dimension finie est dit trigonalisable s'il existe une base dans laquelle sa matrice est triangulaire.

    Une matrice carrée est dite trigonalisable si elle est semblable à une matrice triangulaire.


!!! propriete "Valeurs propres, trace, déterminant"
    Soit $\ds u$ un endomorphisme trigonalisable. Soit $\ds A$ une matrice triangulaire supérieure associée. Alors les valeurs propres de $\ds u$ sont les termes diagonaux de $\ds A$ et $\ds \tr(u)=\sum_{i=1}^p m_i\lambda_i$ et $\ds \det(u)=\prod_{i=1}^{p}\lambda_i^{m_i}$ où $\ds m_i$ est la multiplicité de $\ds \lambda_i$. 

!!! preuve
    Le déterminant étant invariant pas similitude, on a $\ds \chi_u=\chi_A=\prod_{i=1}^{n}(X-a_{ii})$. Or les racines de $\ds \chi_u$ sont les valeurs propres comptées avec leur multiplicité donc les coefficients diagonaux de $\ds A$ sont exactement les valeurs propres, comptées avec leur multiplicité. Les résultats sur la trace et le déterminant en découlent automatiquement.

!!! exemple
    === "Énoncé"
        Soit $\ds A=\begin{pmatrix}1 & 1 \\ 0 & 1\end{pmatrix}$. $\ds A$ est triangulaire supérieure donc évidemment trigonalisable. Montrer que $\ds A$ n'est pas diagonalisable.
    === "Corrigé"
        Le polynôme caractéristique de $\ds A$ est $\ds (X-1)^2$. Donc $\ds A$ est diagonalisable si et seulement si elle admet comme polynôme annulateur $\ds X-1$. Mais si $\ds (X-1)(A)=0$ alors $\ds A=I_2$ et ce n'est pas le cas. Donc $\ds A$ n'est pas diagonalisable. 

 


!!! propriete "Caractérisation de la trigonalisabilité"
    Un endomorphisme est trigonalisable si et seulement si son polynôme caractéristique est scindé sur $\ds \mathbb{K}$.
    
    Par conséquent, toute matrice de $\ds \mathscr{M}_n(\mathbb{C})$ est trigonalisable.


!!! preuve
    La démonstration n'est pas exigible. Le sens direct est cependant abordable, puisque nous l'avons traité dans la propriété précédente : le polynôme caractéristique de $\ds u$ est $\ds \prod_{i=1}^n(X-a_{ii})$ qui est bien un polynôme scindé. 

!!! exemple 
    === "Énoncé"
        Montrer que la matrice $\ds A=\begin{pmatrix}0&3&3\\-1&8&6\\2&-14&-10\end{pmatrix}$ est trigonalisable et la trigonaliser.
    === "Corrigé"
        Le calcul (laissé en exercice) du polynôme caractéristique donne $\ds \chi_A=X(X+1)^2$ donc les deux valeurs propres sont $\ds 0$ et $\ds -1$. Calculons les sous-espaces propres associés. 

        Pour $\ds \ker A$ il suffit de résoudre $\ds AX=0$ ce qui donne $\ds X$ de la forme $\ds \alpha\begin{pmatrix}2\\1\\-1\end{pmatrix}$. 

        Pour $\ds \ker(A+I_3)$ il faut résoudre $\ds AX=-X$ ce qui donne $\ds X$ de la forme $\ds \beta\begin{pmatrix}3\\3\\-4\end{pmatrix}$ On en déduit que $\ds E_{-1}(A)$ est de dimension $\ds 1<2$ où $\ds 2$ est la multiplicité de $\ds -1$ comme racine de $\ds \chi_A$. Ainsi, $\ds A$ n'est pas diagonalisable. 

        La famille $\ds \left(\begin{pmatrix}2\\1\\-1\end{pmatrix},\begin{pmatrix}3\\3\\-4\end{pmatrix}\right)$ peut être complétée en une base par le vecteur $\ds \begin{pmatrix}1\\0\\0\end{pmatrix}$ (il y a obligatoirement un vecteur de la base canonique qui fonctionne). Dans cette base, la matrice de l'endomorphisme $\ds u$ canoniquement associé à $\ds A$ aura alors la forme $\ds \begin{pmatrix}0 & 0 & a\\0 & -1 & b \\0 & 0 & c\end{pmatrix}$ avec obligatoirement $\ds c=-1$ pour respecter le polynôme caractéristique. La matrice est alors bien trigonalisée, et il suffit de faire les calculs pour obtenir $\ds a$ et $\ds b$ : on pose $\ds P=\begin{pmatrix}2&3&1\\1 & 3 & 0\\-1 & -4 & 0\end{pmatrix}$. On calcule alors $\ds P^{-1}=\begin{pmatrix}0 & 4 & 3\\0 & -1 & -1\\1 & -5 & -3\end{pmatrix}$. Puis $\ds P^{-1}AP=\begin{pmatrix}0 & 0 & 2\\0 & -1 & -1 \\0 & 0 & -1\end{pmatrix}$. 





## Exercices

### Issus de la banque CCINP
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-067.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-069.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-070.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-071.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-072.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-073.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-074.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-075.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-083.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-088.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-091.md" %}

### Annales d'oraux
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1349.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1350.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1351.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1352.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1353.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1354.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1355.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1356.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1357.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1358.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-312.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-684.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-685.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-686.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-688.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-689.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-690.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-691.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-692.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-693.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-700.md" %}

### Centrale Python
{% include-markdown "../../../exercicesCentralePython/RMS2017-1045.md" %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1136.md"   rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1157.md" %}
