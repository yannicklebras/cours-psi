# Intégration sur un intervalle quelconque

## Fonctions continues par morceaux

Dans tout ce chapitre, $\ds \mathbb K$ est un corps égal à $\ds \mathbb R$ ou $\ds \mathbb C$.

Vous avez parlé de fonctions en escalier l'année dernière. On donne ici quelques rappels et extensions autour de cette notion.

!!! definition "Subdivision d'un segment"
	Soit $\ds [a,b]$ un segment de $\ds \mathbb R$. On appelle *subdivision* de $\ds [a,b]$ la donnée, pour $\ds n\in\mathbb N$ de $\ds n+1$ réels $\ds (\sigma_0,\dots,\sigma_n)$ de $\ds [a,b]$ tels que :

	- $\ds \sigma_0=a$
	
	- $\ds \sigma_n=b$
	
	- $\ds \forall i\in\{0,\dots,n-1\},\, \sigma_i\le \sigma_{i+1}$
	
	On dit qu'une subdivision $\ds \sigma'$ est *plus fine* que $\ds \sigma$ lorsque tout point $\ds \sigma$ est inclu dans $\ds \sigma'$. 

!!! remarque 
	On peut toujours trouver une subdivision plus fine que deux subdivisions données.

!!! exemple
	Dans l'approximation des intégrales par la méthode des rectangles, les bases des rectangles forment une subdivision de la forme : $\ds \sigma_k=a+k\frac{b-a}n$ pour $\ds k\in\{0,\dots,n\}$.

!!! definition "Fonction en escalier"
	Soit $\ds [a,b]$ un segment de $\ds \mathbb R$. Soit $\ds (\sigma_0,\dots,\sigma_n)$ une subdivision de $\ds [a,b]$ et $\ds f$ une fonction de $\ds [a,b]$ dans $\ds \mathbb K$. On dit que $\ds f$ est une fonction en escalier si et seulement si il existe $\ds (c_0,\dots,c_{n-1})$ $\ds n$ éléments de $\ds \mathbb K$ tels que pour tout $\ds i\in \{0,\dots,n-1\}$, $\ds f_{\big|]\sigma_i,\sigma_{i+1}[}\equiv c_i$.

!!! warning "Attention"
	La définition n'impose rien aux valeurs de $\ds f$ aux points $\ds \sigma_i$ : on sait simplement que $\ds f$ y est bien définie.

!!! exemple
	=== "Énoncé"
		Montrer que la fonction partie entière est une fonction en escalier sur tout segment de $\ds \mathbb R$.

	=== "Corrigé"
		Soit $\ds [a,b]$ un segment non vide de $\ds \mathbb R$. Posons $\ds \sigma_0=a$, $\ds \sigma_1=\lfloor a\rfloor+1\,,\dots,\,\sigma_{n-1}=\lceil b\rceil-1,\,\sigma_n=b$ avec, donc, $\ds n=\lceil b\rceil-\lfloor a\rfloor$. Sur l'intervalle $\ds ]\sigma_0,\sigma_1[$, $\ds \lfloor x\rfloor = \lfloor a\rfloor$, sur l'intervalle $\ds ]\sigma_{n-1},\sigma_n[$, $\ds \lfloor x\rfloor=\lfloor b\rfloor$. Enfin sur tout intervalle $\ds ]\sigma_i,\sigma_{i+1}[$, $\ds \lfloor x\rfloor=\sigma_i$. La fonction partie entière est donc bien constante sur chaque intervalle de subdivision : c'est une fonction en escalier.
	
!!! definition "Subdivision adaptée"
	Soit $\ds f$ une fonction en escalier définie sur un segment $\ds [a,b]$. Soit $\ds \sigma=(\sigma_0,\sigma_1,\dots,\sigma_{n})$ une subdivision de $\ds [a,b]$. On dit que $\ds \sigma$ est adaptée à $\ds f$ si et seulement si pour tout $\ds i\in\{0,\dots,n-1\}$, $\ds f_{\big|]\sigma_i,\sigma_{i+1}[}$ est constante.

!!! remarque
	Étant données deux fonctions en escalier $\ds f$ et $\ds g$, on peut toujours trouver une subdivision adaptée aux deux fonctions.

!!! definition "Intégrale d'une fonction en escalier"
	Soit $\ds f$ une fonction en escalier sur un segment $\ds [a,b]$ et $\ds \sigma=(\sigma_0,\sigma_1,\dots,\sigma_n)$ une subdivision. On note $\ds c_i=f_{\big|]\sigma_i,\sigma_{i+1}[}$. On définit l'intégrale de $\ds f$ sur $\ds [a,b]$ par :

	$$
	\int_{[a,b],\sigma}f = \sum_{k=0}^{n-1}(\sigma_{i+1}-\sigma_i)c_i.
	$$

!!! propriete "Indépendance par rapport à la subdivision"
	L'intégrale de $\ds f$ en escalier ne dépend pas de la sudivision adaptée choisie. On peut donc écrire $\ds \int_{[a,b]}f$. 

!!! preuve
	Soit $\ds f$ une fonction en escalier sur $\ds [a,b]$ et $\ds \sigma$ et $\ds \gamma$ deux subdivisions adaptées à $\ds f$ telles que $\ds \gamma$ est plus fine que $\ds \sigma$. Notons $\ds \sigma=(\sigma_0,\sigma_1,\dots,\sigma_n)$ et $\ds \gamma=(\gamma_0,\gamma_1,\dots,\gamma_p)$, $\ds f_{\big|]\sigma_i,\sigma_{i+1}[}=c_i$ et $\ds f_{\big|]\gamma_j,\gamma_{j+1}[}=d_j$. $\ds \gamma$ étant plus fine, $\ds p\ge n$. Pour tout $\ds i\in \{0,\dots,n\}$, il existe $\ds j_i\in\{0,\dots,p\}$ tel que $\ds \gamma_{j_i}=\sigma_i$. Puisque $\ds \sigma$ est adaptée à $\ds f$, $\ds f$ est constante sur $\ds ]\sigma_i, \sigma_{i+1}[$ donc sur $\ds ]\gamma_{j_i},\gamma_{j_{i+1}}[$ et pour $\ds k\in\{j_i, j_{i+1}-1\}$, $\ds d_{k}=c_i$. On peut alors écrire : 

	$$
	\begin{aligned}
	\inf_{[a,b],\sigma}f &= \sum_{i=0}^{n-1}(\sigma_{i+1}-\sigma_i)c_i\\
	&=\sum_{i=0}^{n-1}(\gamma_{j_{i+1}}-\sigma_{j_i})c_i\\
	&=\sum_{i=0}^{n-1}\sum_{k=j_i}^{j_{i+1}-1}(\gamma_{k+1}-\gamma_k)c_i\\
	&=\sum_{i=0}^{n-1}\sum_{k=j_i}^{j_{i+1}-1}(\gamma_{k+1}-\gamma_k)d_k\\
	&=\sum_{k=0}^{p-1}(\gamma_{k+1}-\gamma_k)d_k\\
	&=\int_{[a,b],\gamma}f
	\end{aligned}
	$$

	Ainsi, il y a invariance de l'intégrale par une subdivision plus fine. Et si $\ds \sigma$ et $\ds \gamma$ sont adaptées à $\ds f$ mais sans relation de «plus fine», alors on prend une subdivision $\ds \delta$ plus fine que les deux. On aura alors 

	$$
	\int_{[a,b],\sigma}f\underset{\text{plus fine}}=\int_{[a,b],\delta}f\underset{\text{plus fine}}=\int_{[a,b],\gamma}
	$$ 

!!! exemple
	=== "Énoncé"
		On note $\ds f:x\mapsto \lfloor x\rfloor$. Calculer $\ds \ds\int_{[\frac 13,\frac {13}6]}$.
	=== "Corrigé"
		On considère la subdivision de $\ds [\frac 13,\frac 86]$ : $\ds \sigma=(\frac 13,1,2,\frac{13}{6})$. On a alors :

		$$
		\begin{aligned}
		\int_{[a,b]}f &= \sum_{k=0}^{2}(\sigma_{i+1}-\sigma_i)c_i\\
		&=(1-\frac 13)\times 0+(2-1)\times 1+(\frac{13}6-2)\times 2\\
		&=0+1+\frac 13\\
		&=\frac 43
		\end{aligned} 
		$$


!!! definition "Fonction continue par morceaux sur un segment"
	Soit $\ds f$ une fonction définie sur un segment $\ds [a,b]$ de $\ds \mathbb R$. On dit que $\ds f$ est *continue par morceaux* sur $\ds [a,b]$ s'il existe une subdivision $\ds \sigma=(\sigma_0,\dots,\sigma_n)$ de $\ds [a,b]$ telle que pour tout $\ds i\in\mathbb \{0,\dots,n-1\}$, $\ds f_{\big|]\sigma_i,\sigma_{i+1}[}$ est continue et prolongeable par continuité en $\ds \sigma_i$ et en $\ds \sigma_{i+1}$. 

!!! warning "Attention"
	La définition ne dit rien sur la valeur aux $\ds \sigma_i$.

En MPSI, on apprend à construire l'intégrale des fonctions continues par morceaux en les approchant par des fonctions en escalier à partir de la propriété suivante : 

!!! propriete "Approximation des fontions continues par morceaux"
	Soit $\ds f$ une fonction continue par morceaux sur $\ds [a,b]$ et $\ds \varepsilon>0$. Alors il existe une fonction $\ds \varphi$ en escalier telle que $\ds \sup_{[a,b]}|f-\varphi|\le \varepsilon$. 

Cette propriété n'est pas utile pour le cours de PSI. Vous avez tous manipulé l'intégrale des fonctions continues et c'est suffisant pour définir l'intégrale d'une fonction continue par morceaux. 

!!! definition "Intégrale d'une fonction continue par morceaux"
	Soit $\ds [a,b]$ un segment de $\ds \mathbb R$, soit $\ds f$ continue par morceaux sur $\ds [a,b]$ et $\ds \sigma=(\sigma_0,\dots,\sigma_n)$ une subdivision adaptée à $\ds f$. Pour $\ds i\in\{0,\dots,n-1\}$, on pose $\ds f_i$ le prolongement continu de $\ds f$ sur $\ds ]\sigma_i,\sigma_{i+1}[$. On définit alors :

	$$
	\int_{[a,b]}f=\sum_{i=0}^{n-1}\int_{[\sigma_i,\sigma_{i+1}]}f_i. 
	$$


L'intégrale des fonctions continues par morceaux sur un segment hérite de toutes les propriétés de l'intégrale des fonctions continues (linéarité, positivité, croissance, Chasles...) sauf la stricte positivité : 

!!! propriete "Stricte positivité de l'intégrale d'une fonction continue"
	Soit $\ds f$ une fonction continue positive et non nulle sur $\ds [a,b]$, alors $\ds \int_{[a,b]}f>0$.

!!! exemple
	Soit $\ds f$ la fonction nulle sur $\ds ]0,1]$ et qui vaut $\ds 1$ en $\ds 0$. Alors $\ds f$ est continue par morceaux, positive et non nulle et pourtant, $\ds \int_{[0,1]}f=0$. 



Enfin, voici la famille de fonctions qui va nous intéresser par la suite : 

!!! definition "Fonction continue par morceaux sur un intervalle"
	Soit $\ds f$ une fonction définie sur un intervalle $\ds I$. On dit que $\ds f$ est continue par morceaux sur $\ds I$ si et seulement si elle est continue par morceaux sur tout segment inclus dans $\ds I$. 

!!! exemple 
	- La fonction partie entière est continue par morceaux sur $\ds \mathbb R$ tout entier. 
	- La fonction $\ds x\mapsto x\times\lfloor x\rfloor$ est continue par morceaux sur $\ds \mathbb R$ tout entier.
	- La fonction $\ds \tan$ n'est pas continue par morceaux sur $\ds \mathbb R$ (car pas prolongeable par continuité en tout point).


## Intégrales généralisées sur un intervalle quelconque 

### Cas de l'intervalle $\ds [a,+\infty[$

Dans ce paragraphe, $\ds a$ désigne un réel quelconque. 

!!! definition "intégrale convergente"
	Soit $\ds f$ une fonction continue par morceaux sur $\ds [a,+\infty[$. Si $\ds x\mapsto \int_a^x f(t)\d t$ admet une limite quand $\ds x\to+\infty$, on dit que l'intégrale $\ds \int_{a}^{+\infty} f(t)\d t$ est *convergente* en $\ds +\infty$. On dit que sa *valeur* est égale à cette limite.

!!! exemple 
	=== "Énoncé"
		Montrer que $\ds \ds\int_1^{+\infty} \frac 1{t^2}\d t$ est convergente.
	=== "Corrigé"
		Soit $\ds x$ un réel tel que $\ds x>1$. On a $\ds \ds\int_1^{x}\frac 1{t^2}\d t=\left[-\frac1t\right]_1^x=1-\frac 1x$, dont la limite est $\ds 1$. On en déduit que l'intégrale est convergente (égale à $\ds 1$).

!!! exemple
	=== "Énoncé"
		Montrer que $\ds \ds\int_{1}^{+\infty}\frac 1{\sqrt t}\d t$ n'est pas convergente
	=== "Corrigé"
		Soit $\ds x$ un réel tel que $\ds x>1$. On a $\ds \ds\int_{1}^x\frac 1{\sqrt t}\d t=\left[2\sqrt t\right]_1^x=2(\sqrt x-1)$ dont la limite quand $\ds x\to+\infty$ est $\ds +\infty$. L'intégrale n'est donc pas convergente. 

!!! propriete "propriétés de l'intégrale sur $[a,+\infty[$"
	L'intégrale ainsi définie possède les propriétés classiques de l'intégrale sur un segment. Si $f$ et $g$ sont deux fonctions continues par morceaux sur $[a,+\infty[$ à valeurs dans $\mathbb K$, si $\alpha$ et $\beta$ sont deux éléments de $\mathbb K$ et si $c\in [a,+\infty[$ alors :
	- (**linéarité**) Si $\int_a^{+\infty} f$ et $\int_a^{+\infty}g$ sont convergentes, alors $\int_a^{+\infty} \alpha f+\beta g$ est convergente et $\int_a^{+\infty} \alpha f+\beta g=\alpha\int_a^{+\infty}f+\beta\int_a^{+\infty}g$ ;  
	- (**positivité**) Si $f\ge 0$ et si $\int_a^{+\infty}f$ est convergente alors $\int_a^{+\infty} f\ge 0$ ;  
	- (**croissance**) Si $f\le g$ et si $\int_a^{+\infty}f$ et $\int_a^{+\infty}g$ sont convergentes alors $\int_a^{+\infty}f\le\int_a^{+\infty}g$ ;  
	- (**relation de Chasles**) $\int_a^{+\infty}f$ et $\int_c^{+\infty}f$ ont même nature, et si elles sont convergentes alors $\int_a^{+\infty}f=\int_a^c f+\int_c^{+\infty}f$. 

!!! preuve
	**linéarité :**  Soit $x\in[a,+\infty[$, on sait que l'intégrale des fonctions continues par morceaux sur un segment est linéaire. On peut donc écrire :
	
	$$
	\int_a^x\alpha f(t)+\beta g(t)\d t= \alpha\int_a^x f(t)\d t+\beta\int_a^x g(t)\d t.
	$$

	Les deux intégrale de droite étant convergentes, on en déduit que $\int_a^x\alpha f(t)+\beta g(t)\d t$ admet une limite quand $x\to+\infty$ donc l'intégrale est convergente, et par unicité de la limite, $\ds \int_a^{+\infty}\alpha f(t)+\beta g(t)\d t= \alpha\int_a^{+\infty} f(t)\d t+\beta\int_a^{+\infty} g(t)\d t$. 

	**positivité :** Soit $x\in[a,+\infty[$, l'intégrale des fonctions continues par morceaux sur un segment étant positive, on sait que $\int_a^x f(t)\d t\ge 0$. L'intégrale étant convergente, par passage à la limite dans l'inégalité on obtient $\int_a^{+\infty}f(t)\d t\ge 0$. 

	**croissance :** Si $f\le g$ alors $g-f\ge 0$. D'après la positivité de l'intégrale, on a $\int_a^{+\infty}(g-t)(t)\d t\ge 0$ et par linéarité, $\int_a^{+\infty}f(t)\d t\le\int_a^{+\infty}g(t)\d t$. 

	**relation de Chasles :** On utilise la relation de Chasles de l'intégrale des fonction continues par morceaux sur un segment. Soit $x\in[a,+\infty[$ on sait que $\ds\int_a^x f(t)\d t=\int_a^cf(t)\d t+\int_c^xf(t)\d t$. Or $\ds\int_a^c f(t)\d t$ est finie donc $\ds\int_a^{+\infty}f(t)\d t$ et $\ds\int_c^{+\infty} f(t)\d t$ ont même nature et par unicité de la limite, $\ds\int_a^{+\infty}f(t)\d t=\int_a^c f(t)\d t+\int_c^{+\infty}f(t)\d t$.


!!! propriete "Caractérisation de la convergence pour les fonctions positives"
	Si $\ds f$ est continue par morceaux sur $\ds [a,+\infty[$ et à valeurs positives, alors $\ds \int_a^{+\infty} f(t) \mathrm{d} t$ converge si et seulement si $\ds x \mapsto \int_a^x f(t) \mathrm{d} t$ est majorée.

!!! preuve
	Soit $\ds f$ continue par morceaux sur $\ds [a,+\infty[$ à valeurs positives. Pour $\ds x\in[a,+\infty[$, posons $\ds \ds F(x)=\int_a^x f(t)\d t$. D'après la relation de Chasles et par positivité de l'intégrale, $\ds F$ est croissante : en effet, si $\ds x<y$, alors $\ds \ds\int_a^y f(t)\d t=\int_a^x f(t)\d t+\underset{\ge 0}{\underbrace{\int_x^yf(t)\d t}}\ge \int_a^xf(t)\d t$. Par ailleurs, $\ds F$ est majorée par hypothèse. Elle est donc croissante et majorée sur $\ds [a,+\infty[$ : par théorème de convergence monotone, elle converge en $\ds +\infty$. On en déduit que l'intégrale est convergente.

	Réciproquement, si $\ds \int_a^{+\infty} f(t)\d t$ est convergente, $\ds F$ étant croissante, elle est inférieure à sa limite et donc $\ds F$ est majorée. 


!!! theoreme "Théorème de comparaison"
	Si $\ds f$ et $\ds g$ sont deux fonctions continues par morceaux sur $\ds \left[a,+\infty\right[$  telles que $\ds 0 \leqslant f \leqslant g$, la convergence de $\ds \int_a^{+\infty} g$ implique celle de $\ds \int_a^{+\infty} f$.

!!! preuve
	on remarque que $\ds f$ et $\ds g$ sont positives : on va pouvoir utiliser la propriété précédente. Soit donc $\ds f$ et $\ds g$ deux fonctions continues par morceaux sur $\ds [a,+\infty[$ telles que $\ds 0\le f\le g$. On suppose que $\ds \int_a^{+\infty} g$ est convergente. Soit $\ds x\in[a,+\infty[$. Par propriété de positivité de l'intégrale, $\ds 0\le \int_a^x f(t)\d t\le \int_a^x g(t)\d t$. De plus, $\ds g$ étant positive, on peut affirmer que $\ds \int_a^x g(t)\d t\le\int_a^{+\infty} g(t)\d t$. On en déduit que pour tout $\ds x\in[a,+\infty[$, $\ds \int_a^{x}f(t) \d t\le \int_a^{+\infty}g(t)\d t$ qui est une majoration indépendante de $\ds x$. On en déduit que $\ds \int_a^x f(t)\d t$ est majorée, et donc d'après la propriété précédente, $\ds \int_a^{+\infty}f(t)\d t$ est convergente. 

!!! exemple
	=== "Énoncé"
		Montrer que $\ds \int_1^{+\infty}\frac 1{1+x^{3/2}}\d x$ est convergente.
	=== "Corrigé"
		Soit $\ds x\in[1,+\infty[$, $\ds \frac 1{1+x^{3/2}}\le \frac 1{x^{3/2}}$. Or $\ds \int_1^x\frac 1{t^{3/2}}\d t=2(1-\frac 1{\sqrt x})\xrightarrow[x\to+\infty]{}2$ donc $\ds \int_{1}^{+\infty}\frac 1{x^{3/2}}\d x$ est convergente. Par théorème de comparaison, on en déduit que $\ds \int_1^{+\infty}\frac 1{1+ x^{3/2}}\d x$ est convergente.


!!! exemple
	=== "Énoncé"
		Étudier la convergence de l'intégrale $\ds \int_0^{+\infty}e^{\alpha t}\d t$ en fonction de $\ds \alpha\in\mathbb R^*$.
	=== "Corrigé"
		Soit $\ds x\in\mathbb R^+$. On a $\ds \int_0^{x}e^{\alpha t}\d t=\frac 1\alpha(e^{\alpha x}-1)$ qui converge quand $\ds x\to+\infty$ si et seulement si $\ds \alpha<0$.

!!! exemple 
	=== "Énoncé"
		Étudier la convergence de l'intégrale $\ds \int_1^{+\infty}\frac 1{t^\alpha}\d t$ en fonction de $\ds \alpha\in\mathbb R^*$
	=== "Corrigé"
		Soit $\ds x>1$, si $\ds \alpha\ne 1$, on a $\ds \int_1^x \frac 1{t^\alpha}\d t=\frac{1}{\alpha -1}\left(\frac{1}{x^{\alpha-1}}-1\right)$ qui converge si et seulement si $\ds \alpha-1>0$ c'est à dire $\ds \alpha>1$. 

		Si $\ds \alpha=1$, on a $\ds \int_1^{x}\frac 1t\d t = \ln(x)$ qui diverge. On en déduit que $\ds \int_1^{+\infty}\frac 1{t^\alpha}\d t$ converge si et seulement si $\ds \alpha>1$. 

!!! exemple
	=== "Énoncé"
		On pose, pour $t\ge 1$, $f(t)=2\ln(t^{3/2}+1)-3\ln(t+1)$. Quelle est la nature de $\int_1^{+\infty}f$ ?
	=== "Corrigé"
		Tout d'abord, simplifions un peu $f$ pour se ramener proche de $0$. $f(t)=2\ln(t^{3/2})+2\ln(1+\frac 1{t^{3/2}})-3\ln(t)-3\ln(1+\frac 1t)=2\ln(1+\frac 1{t^{3/2}})-3\ln(1+\frac 1t)$. Or $\ln(1+\frac1{t^{3/2}}\le \frac 1{t^{3/2}}$ et $\ln(1+\frac 1t)\ge \frac 1t-\frac 1{2t^2}$. On en déduit que $f(t)\le \frac 1{t^{3/2}}-\frac3t+\frac3{2t^2}\le -\frac 3{2t}-\frac 3{2t}\left(1-\frac1{3t^{3/2}}-\frac 1{2t^2}\right)$. Or il existe $t_0$ tel que si $t>t_0$, alors $\left(1-\frac1{3t^{3/2}}-\frac 1{2t^2}\right)>0$. Donc pour $t\ge t_0$, $f(t)\le -\frac 3{2t}$. Or $\int_{t_0}^{+\infty} \frac 3{2t}\d t$ est divergente. Donc par théorème de comparaison, $\int_{t_0}^{+\infty}(-f)(t)\d t$   est divergente, c'est à dire que $\int_1^{+\infty} f(t)\d t$ est divergente.

### Cas d'un intervalle semi-ouvert

Dans ce paragraphe, on considère un intervalle de la forme $[a,b[$ avec $a\in\mathbb R$ et $b\in\mathbb R\cup\{+\infty\}$. Tous les résultats énoncés dans ce paragraphe pourront s'appliquer, en échangeant $a$ et $b$ à des intervalles du type $]a,b]$ : nous les traiterons en général en exemple.

!!! definition "integrale convergente en $b$"
	Soit $f$ une fonction continue par morceaux sur $[a,b[$, on dit que l'intégrale $\int_a^bf(t)\d t$ est convergente ssi $x\mapsto \int_a^x f(t)\d t$ admet une limite en $b$. 


!!! exemple
	=== "Énoncé"
		L'intégrale $\ds \int_0^{\frac\pi2}\tan(t)\d t$ est-elle convergente ?
	=== "Corrigé"
		On connaît une primitive sur $[0,\frac\pi2[$ de la tangente, sous la forme de la fonction $t\mapsto -\ln(\cos(t))$. Soit $x\in[0,\frac\pi2[$, on a donc $\ds\int_0^{x}\tan(t)\d t=\left[-\ln(\cos(t))\right]_0^x=-\ln(\cos(x))\xrightarrow[x\to\frac\pi2]{}+\infty$. L'intégrale n'est donc pas convergente. 

!!! exemple
	=== "Énoncé"
		Étudier la convergence de l'intégrale $\int_0^1\frac 1{t^\alpha}\d t$ en fonction de $\alpha$. 
	=== "Corrigé"
		L'ensemble de définition de $f:t\mapsto \frac 1{t^\alpha}$ est ici $]0,1]$ : on est donc dans le cas d'un intervalle semi ouvert à gauche. On s'intéresse donc à la fonction $g:x\mapsto \int_x^1\frac 1{t^\alpha}\d t$. Or si $\alpha\ne 1$ :
		
		$$
		\int_x^1\frac 1{t^\alpha}\d t=\frac 1{\alpha-1}\left[\frac 1{t^{\alpha-1}}\right]_x^1==\frac1{\alpha-1}\left(1-\frac 1{x^{\alpha-1}}\right)
		$$
		
		qui converge en $0$ si et seulement si $\alpha-1<0$ c'est à dire $\alpha<1$. 

		Par ailleurs, si $\alpha=1$, alors  :

		$$
		\int_x^1\frac 1{t^\alpha}\d t=\left[\ln(t)\right]_x^1=-\ln(x)\xrightarrow[x\to 0]{}+\infty.
		$$

		On en déduit que l'intégrale $\int_0^1\frac 1{t^\alpha}\d t$ est convergente si et seulement si $\alpha<1$. 

!!! exemple
	=== "Énoncé"
		Étudier la convergence de l'intégrale $\ds\int_0^1\ln(1-t)\d t$ puis de $\ds\int_0^1\ln(t)\d t$.
	=== "Corrigé"
		On connaît une primitive de $\ln$ via la fonction $t\mapsto t\ln(t)-t$. La fonction $t\mapsto\ln(1-t)$ est continue par morceaux sur $[0,1[$. On a pour $x\in[0,1[$ :

		$$
		\begin{aligned}
		\int_0^x\ln(1-t)\d t&=\left[(t-1)\ln(1-t)-(t-1)\right]_0^x\\
		&=(x-1)\ln(1-x)-(x-1)-1\\
		&\xrightarrow[x\to 1]{}-1&\text{par croissances comparées}
		\end{aligned}
		$$

		On fait de même pour la seconde : $\ln$ est définie sur $]0,1]$. Pour $x\in]0,1]$, on a :
		
		$$
		\begin{aligned}
		\int_x^1\ln(t)\d t&=\left[t\ln(t)-t\right]_x^1\\
		&=-1-x\ln(x)+x\\
		&\xrightarrow[x\to 0]{}-1
		\end{aligned}
		$$

		Les deux intégrales sont convergentes et valent $-1$.


On retrouve les principales propriétés de l'intégrale sur un segment. 

!!! propriete "Linéarité de l'intégrale"
	Soit $f$ et $g$ deux fonctions continues par morceaux sur un intervalle $[a,b[$ à valeurs dans $\mathbb K$ et $\alpha,\beta$ deux éléments de $\mathbb K$. Si les intégrales de $f$ et $g$ sont convergentes en $b$ alors l'intégrale de $\alpha f+\beta g$ est convergente en $b$ et $\int_a^b \alpha f(t)+\beta g(t)\d t=\alpha\int_a^b f(t)\d t+\beta\int_a^b g(t)\d t$. 

!!! propriete "Positivité de l'intégrale"
	Soit $f$ une fonction continue par morceaux sur un intervalle $[a,b[$ et positive. Si l'intégrale de $f$ converge en $b$ alors $\int_a^b f(t)\d t\ge 0$.

!!! propriete "Croissance de l'intégrale"
	Soit $f$ et $g$ deux fonctions continues par morceaux sur $[a,b[$ telles que $f\le g$. Si les intégrales de $f$ et $g$ sont convergentes en $b$ alors $\int_a^bf(t)\d t\le \int_a^b g(t)\d t$.

!!! propriete "Relation de Chasles"
	Soit $f$ une fonction continue par morceaux sur $[a,b[$ et $c\in[a,b[$. Alors les intégrales $\int_a^b f(t)\d t$ et $\int_c^b f(t)\d t$ ont même natures et si elles sont convergentes, alors $\int_a^b f(t)\d t=\int_a^c f(t)\d t+\int_c^bf(t)\d t$. 

### Cas d'un intervalle ouvert

On s'intéresse ici à un intervalle de la forme $]a,b[$ avec $-\infty\le a < b\le +\infty$.

!!! definition "Intégrale convergente sur $]a,b[$" 
	Soit $f$ continue par morceaux sur l'intervalle $]a,b[$. $\int_a^b f(t)\d t$ converge si et seulement si il existe $c\in]a,b[$ tel que $\int_a^c f(t)\d t$ et $\int_c^bf(t)\d t$ convergent.
	Dans un cas de convergence, la valeur de l'intégrale est alors donnée par $\int_a^bf(t)\d t=\int_a^cf(t)\d t+\int_c^bf(t)\d t$. 

!!! propriete "indépendance par rapport à c"
	La nature et la valeur de l'intégrale ne dépendent pas du $c$ choisi.

!!! preuve
	Soit $f$ une fonction continue par morceaux sur $]a,b[$ telle que son intégrale $\int_a^b f(t)\d t$ soit convergente. Soit alors, d'après la définition, $c\in]a,b[$ tel que $\int_a^cf(t)\d t$ et $\int_c^bf(t)\d t$ soient convergentes.  Soit maintenant $c'\in]a,b[$. Alors d'après la relation de Chasles pour les fonction continues par morceaux sur des intervalles semi-ouverts, on a $\int_a^cf(t)\d t$ (resp. $\int_c^bf(t)\d t$) et $\int_a^{c'}f(t)\d t$ (resp. $\int_{c'}^bf(t)\d t$) qui sont de même nature et :
	
	$$
	\begin{aligned}
	\int_a^{c'}f(t)\d t+\int_{c'}^b f(t)\d t&=\int_a^{c}f(t)\d t+\int_c^{c'}f(t)\d t+\int_{c'}^c f(t)\d t+\int_{c}^b f(t)\d t\\
	&=\int_a^{c}f(t)\d t+\underset{=0}{\underbrace{\left(\int_c^{c'}f(t)\d t+\int_{c'}^c f(t)\d t\right)}}+\int_{c}^b f(t)\d t\\
	&=\int_a^cf(t)\d t+\int_c^bf(t)\d t\\
	&=\int_a^b f(t)\d t
	\end{aligned}
	$$

!!! propriete "Calcul effectif de l'intégrale"
	Soit $f$ une fonction continue par morceaux sur $]a,b[$, soit $(c,x)\in]a,b[^2$, on pose $F(x)=\int_c^xf(t)\d t$. $\int_a^b f(t)\d t$ converge si et seulement si $F$ admet des limites en $a$ et $b$. On a alors 
	
	$$
	\int_a^bf(t)\d t=\lim_{x\to b}F(x)-\lim_{x\to a}F(x)
	$$

	on utilisera sans ambiguité la notation $\ds\int_a^bf(t)\d t=\left[F(x)\right]_a^b$.

	Par ailleurs, si $f$ est continue, $F$ est évidemment la primitive de $f$ qui s'annule en $c$. 

!!! preuve 
	Elle découle immédiatement de la définition.

!!! exemple
	=== "Énoncé"
		Calculer si elle est convergente, la valeur de $\int_0^1\frac 1{\sqrt t}+\ln(1-t)\d t$. 
	=== "Corrigé"
		On remarque que l'intégrande est définie sur $]0,1[$. Calculons $\int_{\frac 12}^{x}\frac 1{\sqrt t}+\ln(1-t)\d t$ : 
		
		$$
		\begin{aligned}
		\int_{\frac 12}^{x}\frac 1{\sqrt t}+\ln(1-t)\d t&=\left[2\sqrt t+(t-1)\ln(1-t)-t\right]_{\frac 12}^x\\
		&=2\sqrt x+(x-1)\ln(1-x)-x+C\\
		&=F(x)
		\end{aligned}
		$$

		où $C$ est une constante indépendante de $x$. Par croissances comparées, $F$ admet une limite en $1$ : $\lim_{x\to 1}F(x)=1+C$. De plus, par théorèmes généraux, $F$ admet une limite en $0$ : $\lim_{x\to 0}F(x)=C$. On en déduit que $\int_0^1\frac 1{\sqrt t}+\ln(1-t)\d t$ est convergente et $\int_0^1\frac 1{\sqrt t}+\ln(1-t)\d t=1+C-C=1$. 

!!! exemple 
	=== "Énoncé"
		Calculer, si elle est convergente, $\int_{-\infty}^{+\infty}\frac 1{1+t^2}\d t$. 
	=== "Corrigé"
		Soit $x\in\mathbb R$, $x\mapsto \frac 1{1+t^2}$ est continue par morceaux sur $[0,x]$ (ou $[x,0]$) : on pose $F(x)=\int_0^x\frac 1{1+t^2}\d t$. On connait la forme de $F$ : $F(x)=\arctan(x)-\arctan(0)=\arctan(x)$. Or $\lim_{x\to+\infty}\arctan(x)=\frac \pi2$ et $\lim_{x\to-\infty}\arctan(x)=-\frac \pi2$. On en déduit que l'intégrale est convergente et $\ds\int_{-\infty}^{+\infty}\frac 1{1+t^2}\d t=\frac\pi2-(-\frac\pi2)=\pi$. 


## Preuves de convergence et calculs

!!! theoreme "Théorème de comparaison des intégrale positives sur un intervalle quelconque"
	Soit $f,g$ continues par morceaux sur un intervalle $I$ de bornes $a<b$. On suppose que $0\le f\le g$ alors la convergence de $\int_a^bg$ implique celle de $\int_a^b f$.

!!! preuve 
	On se place sous les hypothèses de la propriété. Soit $c\in]a,b[$ et $x\in[c,b]$ par exemple. Alors par croissance de l'intégrale sur un intervalle semi ouvert, $\int_c^x f(t)\d t\le \int_c^x g(t)\d t$. Or $\int_a^bg$ est convergente donc $\int_c^b g$ convergente. On en déduit que $x\mapsto \int_c^x f(t)\d t$ est majorée. Or elle est croissante, donc elle admet une limite en $b$ : $\int_c^b f(t)\d t$ est convergente. On montre de même que $\int_a^c f$ est convergente et finalement, $\int_a^b f$ est convergente. 

!!! propriete "Intégration par parties"
	Soient $f$ et $g$ deux fonctions de classe $\mathcal C^1$ sur un intervalle $I$ de $\mathbb R$ de bornes $a<b$. Alors si $h=f\times g$ admet des limites en $a$ et $b$, $\int_a^b f'g$ et $\int_a^b fg'$ ont même nature, et si elles sont convergentes, alors :
	
	$$
	\int_a^bf'(t)g(t)\d t=\left[f(t)g(t)\right]_a^b-\int_a^bf(t)g'(t)\d t.
	$$

!!! preuve
	La preuve n'est pas exigible. C'est une conséquence directe de la formule d'intégration par partie sur les segments et des propriétés de passage à la limite. 


!!! exemple
	=== "Énoncé"
		Justifier la convergence de l'intégrale $\int_1^{+\infty}\frac{\ln(t)}{t^2}\d t$. 
	=== "Corrigé"
		On va passer par l'intégration par parties. On remarque qu'une primitive de $t\mapsto \frac 1{t^2}$ est $-\frac 1{t}$. Or $t\mapsto\frac{\ln(t)}{t}$ admet des limites en $1$ (où elle est bien définie) et en $+\infty$ par croissances comparées. On en déduit que $\int_1^{+\infty} \frac{\ln(t)}{t^2}\d t$ et $\int_1^{+\infty}-\frac 1{t^2}\d t$ ont même nature. Or l'intégrale $\int_1^{+\infty}\frac 1{t^2}\d t$ converge. Donc l'intégrale $\int_1^{+\infty}\frac{\ln(t)}{t^2}\d t$ est convergente. 

		Le calcul donne $\int_1^{+\infty}\frac{\ln(t)}{t^2}\d t=\left[-\frac {\ln(t)}t\right]_1^{+\infty}+\int_1^{+\infty}\frac 1{t^2}\d t=0+\int_1^{+\infty}\frac 1{t^2}\d t=1$.


!!! propriete "Changement de variable croissant"
	Si $\varphi:] \alpha, \beta[\rightarrow] a, b[$ est une bijection strictement croissante de classe $\mathcal{C}^1$, et si $f$ est continue sur  $]a, b[$, alors $\int_a^b f(t) \d t$ et $\int_\alpha^\beta(f \circ \varphi)(u) \varphi^{\prime}(u) \d u$ sont de même nature, et égales en cas de convergence.

!!! propriete "Changement de variable décroissant"
	Si $\varphi:] \alpha, \beta[\rightarrow] a, b[$ est une bijection strictement décroissante de classe $\mathcal{C}^1$, et si $f$ est continue sur  $]a, b[$, alors $\int_a^b f(t) \d t$ et $-\int_\alpha^\beta(f \circ \varphi)(u) \varphi^{\prime}(u) \d u$ sont de même nature, et égales en cas de convergence.

!!! preuve
	Encore une fois, la démonstration n'est pas exigible.

!!! remarque 
	On peut appliquer ce résultat sans justification dans les cas de changements de variable classiques ($u=-t\dots$). 

!!! exemple
	=== "Énoncé"
		Justifier la convergence et calculer $\int_0^{+\infty}\frac 1{1+t+t^2}\d t$. 
	=== "Corrigé"
		On remarque que $1+t+t^2=(t+\frac 12)^2+\frac 34=\frac 34\left(1+\left(\frac{2t+1}{\sqrt 3}\right)^2\right)$. Posons alors le changement de variable $\mathcal C^1$ et  croissant $\varphi(t) = \frac{2t+1}{\sqrt 3}$. On a alors $\frac 1{1+t+t^2}=\frac 2{\sqrt3}\frac{\frac{2}{\sqrt 3}}{1+\left(\frac {2t+1}{\sqrt 3}\right)^2}=\frac 2{\sqrt 3}\varphi'(t)f(\varphi(t))$ avec $f(u)=\frac 1{1+u^2}$. D'après le théorème de changement de variable (croissant), les intégrales $\int_0^{+\infty}\frac 1{1+t+t^2}\d t$ et $\frac 2{\sqrt  3}\int_{\frac 1{\sqrt3}}^{+\infty}\frac 1{1+u^2}\d u$ ont même nature. Or la seconde est bien connue et convergente donc ces deux intégrales sont égales. 

		Or $\frac 2{\sqrt  3}\int_{\frac 1{\sqrt3}}^{+\infty}\frac 1{1+u^2}\d u=\frac 2{\sqrt 3}\left[\arctan(u)\right]_{\frac 1{\sqrt 3}}^{+\infty}=\frac 2{\sqrt3}\left(\frac \pi2-\arctan(\frac 1{\sqrt3})\right)=\frac 2{\sqrt 3}(\frac\pi2-\frac\pi6)=\frac{2\pi}{3\sqrt 3}$.

## intégrales absolument convergentes et fonctions intégrables

Pour le moment, nous avons parlé d'intégrales convergentes, mais pas de fonctions intégrables sur un intervalle. 

!!! definition "Intégrale absolument convergente"
	Soit $f$ une fonction continue par morceaux sur un intervalle $I$ de bornes $a<b$ à valeurs dans $\mathbb K$. L'intégrale $\int_a^b f(t)\d t$ est dite *absolument convergente* sur $I$ si et seulement si $\int_a^b|f(t)|\d t$ est convergente.  
	La fonction $f$ sera dite *intégrable* sur $I$ ssi sont intégrale est abolument convergente.

!!! propriete "absolue convergence implique convergence"
	Soit $f$ une fonction à valeurs dans $\mathbb K$ continue par morceaux sur $I$ de bornes $a<b$. Si $\int_a^b f(t)\d t$ est absolunent convergente, alors elle est convergente. 

!!! preuve 
	**Cas $\mathbb K=\mathbb R$ :** Soit $f$ continue par morceaux sur $I$ et à valeurs dans $\mathbb R$. On suppose que $\int_a^b f(t)\d t$ est absolument convergente. Notons $f^+=\frac 12(|f|+f)$ et $f^-=\frac 12(|f|-f)$. On remarque que $f^+$ et $f^-$ sont positives et $f^+\le |f|$ et $f^-\le |f|$. Par théorème de comparaison, puisque $\int_a^b|f|$ converge alors $\int_a^bf^+$ et $\int_a^b f^-$ convergent.Or $f=f^+-f^-$ et par linéarité de l'intégrale, $\int_a^b f$ converge. 

	**Cas $\mathbb K=\mathbb C$ :** Soit $f$ continue par morceaux sur $I$ et à valeurs dans $\mathbb R$. On pose $f=f_R+if_I$ pour définir la partie réelle et la partie imaginaire de $f$. On a alors, d'après les propriétés sur les complexes, $|f_R|\le |f|$ et $|f_I|\le |f|$. Par théorème de comparaison des intégrales positives, on peut alors affirmer que $\int_a^bf_R$ et $\int_a^bf_I$ sont absolument convergentes. Or les fonctions $f_R$ et $f_I$ sont à valeurs dans $\mathbb R$, donc d'après le cas précédent, les intégrales sont convergentes. On en déduit par linéarité de l'intégrale ($\int_a^bf=\int_a^bf_R+i\int_a^bf_I$) que $\int_a^b f$ est convergente.

!!! exemple
	=== "Énoncé"
		Montrer que $\int_1^{+\infty}\frac{\cos(t)}{t^2}\d t$ est convergente
	=== "Corrigé"
		Nous allons montrer qu'elle est absolument convergente en utilisant les propriétés de l'intégrale sur les fonctions positives. Tout d'abord, $\left|\frac{\cos(t)}{t^2}\right|\le\frac 1{t^2}$. Or $\int_1^{+\infty}\frac1{t^2}\d t$ est convergente, donc par théorème de comparaison sur les intégrales positives, $\int_1^{+\infty}\frac{\cos(t)}{t^2}\d t$ est absolument convergente, donc convergente.

!!! exemple
	=== "Énoncé"
		Etudier la convergence de l'intégrale $\ds\int_0^{+\infty}\frac{\sin(t)}{t}\d t$.
	=== "Corrige"
		Notons tout d'abord qu'il n'y a pas de problème en $0$ : la fonction $t\mapsto \frac{\sin(t)}{t}$ est prolongeable par continuité en $0$ donc l'intégrale est *faussement impropre* en 0 : la fonction n'y est pas définie, mais il n'y a pas de problème d'intégration.

		Commençons par étudier la convergence absolue de cette intégrale. On calcule :

		$$
		\begin{aligned}
		\int_{k\pi}^{(k+1)\pi}\frac{|\sin(t)|}{t}\d t&\ge \frac 1{(k+1)\pi}\int_{k\pi}^{(k+1)\pi}|\sin(t)|\d t\\
		&\ge \frac 2{(k+1)\pi}\\
		\int_0^{N\pi}\frac{|\sin(t)|}t\d t&=\sum_{k=0}^{N-1}\int_{k\pi}^{(k+1)\pi}\frac {|\sin(t)|}{t}\d t\\
		&\ge \sum_{k=0}^{N-1}\frac 2{\pi(k+1)}\\
		&\ge \frac 2\pi\sum_{k=0}^{N-1}\frac 1{k+1}
		\end{aligned}
		$$

		Or $\sum\frac 1k$ diverge donc l'intégrale $\int_0^{+\infty}\frac{|\sin(t)|}{t}\d t$ diverge par théorème de minoration divergente. 

		Ainsi, l'intégrale $\int_0^{+\infty}\frac{\sin(t)}{t}\d t$ n'est pas absolument convergente.

		Cependant, on note que $\int_0^{\pi}\frac {\sin(t)}{t}\d t$ est bien définie car $t\mapsto\frac{\sin(t)}{t}$ est continue par morceaux sur $[0,\pi]$. On s'intéresse donc à $\int_\pi^{+\infty}\frac{\sin(t)}{t}\d t$. 

		$$
		\begin{aligned}
		\int_{\pi}^{+\infty}\frac{\sin(t)}t\d t&=\left[-\frac{\cos(t)}{t}\right]_\pi^{+\infty}-\int_\pi^{+\infty}\frac{\cos(t)}{t^2}\d t\\
		&=\frac {-1}{\pi}-\int_\pi^{+\infty}\frac{\cos(t)}{t^2}\d t
		\end{aligned}
		$$

		Or $\left|\frac{\cos(t)}{t^2}\right|\le \frac 1{t^2}$ qui est une fonction intégrable. On en déduit que $\int_\pi^{+\infty}\frac{\cos(t)}{t^2}\d t$ est absolument convergente donc convergente. Or par théorème d'intégration par parties, $\int_\pi^{+\infty}\frac{\sin(t)}{t}\d t$ et $\int_\pi^{+\infty}\frac{\cos(t)}{t^2}\d t$ sont de même nature. On en déduit que $\int_\pi^{+\infty}\frac{\sin(t)}{t}\d t$ est une intégrale convergente. 

		Il existe donc des intégrales qui sont convergentes mais pas absolument convergentes : on parlera de semi-convergence. 

!!! definition "Fonction intégrable"
	Une fonction est dite intégrable sur un intervalle $I$ si elle est continue par morceaux sur $I$ et son intégrale sur $I$ est absolument convergente.

	Si $I=[a,b[$ on parlera de fonction intégrable en $b$ et si $I=]a,b]$ on parlera de fonction intégrable en $a$. 


!!! propriete "Espace des fonctions intégrables"
	On note $L^1(I,\mathbb K)$ l'ensemble des fonctions intégrables sur $I$ à valeurs dans $\mathbb K$. Cet ensemble est un espace vectoriel.

!!! preuve
	Cela découle directement des propriétés de linéarité de l'intégrale et de l'inégalité triangulaire. En effet, la fonction nulle est clairement dans $L^1(I,\mathbb K)$. De plus, si $f$ et $g$ sont dans $L^1$ et si $\alpha$ et $\beta$ sont deux éléments de $\mathbb K$ alors $|\alpha f+\beta g|\le |\alpha||f|+|\beta||g|$ et par théorème de comparaison, on en déduit que $\int_a^b|\alpha f+\beta g|$ est convergente. Ainsi $\int_a^b \alpha f+\beta g$ est absolument convergente et donc dans $L^1$. On en déduit que $L^1$ est non vide et stable par combinaison linéaire : c'est un sev de l'ensemble des fonctions de $I$ dans $\mathbb K$ donc c'est un espace vectoriel.

!!! definition "Fonctions de référence"
	Pour montrer qu'une fonction est intégrable, l'idée sera de la comparer à des fonctions de référence dont on aura montré qu'elle sont intégrable. C'est le cas, déjà traité de la fonction $x\mapsto e^{-\alpha x}$ lorsque $\alpha>0$ en $+\infty$ et de la fonction $x\mapsto \frac 1{x^\alpha}$ lorsque $\alpha>1$ en $+\infty$ et lorsque $\alpha<1$ en $0$.  



!!! propriete "Stricte positivite de l'intégrale"
	Soit $f$ continue, intégrable et positive sur un intervalle $I$ de bornes $a<b$. Alors si $f$ est non nulle, $\int_a^b f(t)\d t>0$. Ou de manière équivalente, si $\int_a^b f(t)\d t=0$ alors $f=0$. 

!!! preuve
	Soit donc $f$ continue, intégrable et positive sur l'intervalle $I$. $f$ étant non nulle et positive, il existe $x_0\in I$ tel que $f(x_0)>0$. Et $f$ étant continue, il existe alors $\eta>0$ tel que si $x\in ]x_0-\eta,x_0+\eta[\cap I$, alors $\frac 12f(x_0)\le f(x)$. Si on pose $\varphi$ la fonction en escalier qui vaut $0$ partout sauf sur $]x_0-\eta,x_0+\eta[\cap I$ où elle vaut $\frac 12f(x_0)$, on peut écrire $f\ge \varphi$. Or $\varphi$ est intégrable, d'intégrale strictement positive (égale à $\frac 12f(x_0)\times\text{longueur}\left(]x_0-\eta,x_0+\eta[\cap I\right)$). On en déduit par croissance de l'intégrale que $\int_a^b f>0$.


!!! theoreme "Théorème de comparaison"
	Pour $f$ et $g$ deux fonctions continues par morceaux sur $[a,+\infty[$ :
	
	- si $f(t) \underset{t \rightarrow+\infty}{=} O(g(t))$, alors l'intégrabilité de $g$ en $+\infty$ implique celle de $f$.
	
	- si $f(t) \underset{t \rightarrow+\infty}{\sim} g(t)$, alors l'intégrabilité de $f$ en $+\infty$ est équivalente à celle de $g$.

	Le résultat s'applique notamment lorsque $f=o(g)$.

!!! preuve
	Soit donc $f$ et $g$ deux fonctions continues par morceaux sur $[a,+\infty[$. Supposons que $f(t) \underset{t \rightarrow+\infty}{=} O(g(t))$ et que $g$ est intégrable en $+\infty$. La relation de domination implique l'existence de $A\in\mathbb R$ et $M\in\mathbb R$ tels que si $x\ge A$ alors $|f(x)|\le M|g(x)|$. Soit $x>A$, on a alors : 

	$$
	\int_a^x |f(t)|\d t=\int_a^A |f(t)|\d t + \int_A^x |f(t)|\d t\le \int_a^A |f(t)|\d t + \int_A^x |g(t)|\d t\le \int_a^A |f(t)|\d t + \int_A^{+\infty} |g(t)|\d t
	$$ 

	la dernière inégalité s'obtenant par intégrabilité de $g$ et croissance de l'intégrale.

	Pour le deuxième point, on utilise le fait que l'équivalence implique la domination : si $f\sim_{+\infty}g$ alors $f=O(g)$ et $g=O(f)$ et donc l'intégrabilité de $g$ implique l'intégrabilité de $f$ et réciproquement. 

!!! theoreme "Théorème de comparaison sur un intervalle quelconque"
	Le théorème précédent s'étend très naturellement sur un intervalle $I$ quelconque. 

!!! exemple
	=== "Énoncé"
		Déterminer la nature de $\ds\int_{0}^{+\infty}\frac{2t+1}{(1+t^2)^2}\d t$.
	=== "Corrigé"
		Il n'y a pas de problème en $0$ puisque l'intégrande y est définie. On s'intéresse donc à l'intégrablilité en $+\infty$. Or en $+\infty$, $\frac{2t+1}{(1+t^2)^2}\sim \frac2{t^3}$. Or $t\mapsto \frac 2{t^3}$ est intégrable, donc $\frac{2t+1}{(1+t^2)^2}$ est intégrable et $\int_0^{+\infty}\frac {2t+1}{(1+t^2)^2}\d t$ est absolument convergente donc convergente.

!!! exemple
	=== "Énoncé"
		Déterminer la nature de $\ds\int_0^{+\infty}\frac {e^t}{e^{3t}+1}\d t$.
	=== "Corrigé"
		Pas de problème en $0$ où l'intégrande est définie. En $+\infty$, $\frac{e^t}{e^{3t}+1}\sim_{+\infty}e^{-2t}$. Or $t\mapsto e^{-2t}$ est intégrable donc $\frac{e^t}{e^{3t}+1}$ est intégrable. L'intégrale $\ds\int_{0}^{+\infty}\frac{e^t}{e^{3t}+1}\d t$ est donc absolument convergente donc convergente.

!!! exemple
	=== "Énoncé"
		Déterminer la nature de $\ds\int_0^1\ln(e^t-1)\d t$.
	=== "Corrigé"
		La fonction $t\mapsto \ln(e^t-1)$ est définie sur $]0,1]$ et continue par morceaux. De plus $\ln(e^t-1)=\ln(1+t+o_0(t)-1)=\ln(t+o_0(t))=\ln(t)+\ln(1+o_0(1))=\ln(t)+o_0(1)\sim \ln(t)$. Or la fonction $t\mapsto \ln(t)$ est intégrable en $0$ donc $t\mapsto \ln(e^t-1)$ est intégrable en $0$ et donc l'intégrale $\int_0^1\ln(e^t-1)\d t$ est convergence. 

!!! exemple 
	=== "Énoncé"
		Déterminer la nature de $\ds\int_0^1\frac{\ln(t)}{\sqrt{(1-t)^3}}\d t$.
	=== "Corrigé"
		La fonction $t\mapsto\frac{\ln(t)}{\sqrt{(1-t)^3}}$ est définie sur $]0,1[$. Au voisinage de $0$, $\frac{\ln(t)}{\sqrt{(1-t)^3}}\sim_0\ln(t)$. Or la fonction $\ln$ est intégrable en $0$ donc $t\mapsto\frac{\ln(t)}{\sqrt{(1-t)^3}}$ est intégrable en $0$. Au voisinage de $1$, $\frac{\ln(t)}{\sqrt{(1-t)^3}}=\frac{\ln(1-(1-t))}{\sqrt{(1-t)^3}}\sim_1-\frac{1-t}{\sqrt{(1-t)^3}}\sim_1-\frac 1{\sqrt{1-t}}$ qui est intégrable en $1$. Donc $t\mapsto\frac{\ln(t)}{\sqrt{(1-t)^3}}$ est intégrable en $1$. On en déduit que cette fonction est intégrable sur en $0$ et en $1$ donc $\int_0^1\frac{\ln(t)}{\sqrt{(1-t)^3}}\d t$ est convergente.




## Exercices

### Issus de la banque CCINP
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-019.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-028.md" %}

### Annales d'oraux
{% include-markdown "../../../exercicesRMS/2022/RMS2022-559.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-890.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-894.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-896.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-898.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1312.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1313.md" %}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-367.md" %}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-575.md" %}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-703.md" %}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-705.md" %}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-706.md" %}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-707.md" %}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-1157.md" %}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-1355.md" %}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-1356.md" %}


### Centrale Python

{% include-markdown "../../../exercicesCentralePython/RMS2015-838.md"  rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2017-1089.md" %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1203.md" %}
