# Variables aléatoires discrètes

## Life hacks sur les sommes dénombrables

L'année dernière, vous avez travaillé en probabilités sur des ensembles finis. Dans ce cas, on peut facilement définir une *probabilité* sur un *évènement*. Mais lorsque l'ensemble des issues possibles est *infini* chacune ne pourrait avoir qu'une probabilité nulle... Il faut donc changer de point de vue.

Nous allons cette année étendre nos compétences en passant aux probabilités sur des ensembles au plus dénombrables.

!!! definition "Ensemble dénombrable"
    Un ensemble est dit (resp. au plus) dénombrable s'il est en bijection avec (resp. une partie de) $\ds \mathbb{N}$, c'est-à-dire s'il peut être décrit en extension sous la forme $\ds \left\{x_i, i \in I\right\}$ où $\ds I=\mathbb{N}$ (resp. $\ds I \subset \mathbb{N}$) avec des $\ds x_i$ distincts.

!!! exemple 
    === "Énoncé"
        Montrer que $\ds \mathbb Z$ est dénombrable.
    === "Corrigé"
        Soit $\ds \varphi$ l'application définie sur $\ds \mathbb N$ à valeurs dans $\ds \mathbb Z$ par : $\ds \varphi(n)=\begin{cases}\frac n2&\text{si n pair}\\\frac {-1-n}2&\text{si n est impair}\end{cases}$. Montrons que cette application est injective et surjective.  
        
        **injectivité :** Soient $\ds p$ et $\ds q$ deux entiers naturels tels que $\ds \varphi(p)=\varphi(q)$. Notons $\ds n=\varphi(p)$. Si $\ds p$ est pair, alors $\ds n=\frac p2\ge 0$ et donc $\ds \varphi(q)$ est positif, ce qui implique que $\ds q$ est pair aussi. On a donc $\ds \varphi(q)=\frac q2$ est donc $\ds \frac q2=\frac p2$ d'où $\ds p=q$.  

        De même si $\ds p$ est impair, on a $\ds \varphi(p)=\frac {-1-p}2<0$. On en déduit que $\ds \varphi(q)<0$ et donc que $\ds q$ est impair. On a alors $\ds \varphi(q)=\frac{-1-q}{2}$ et ainsi $\ds p=q$. L'application $\ds \varphi$ est donc injective.

        **surjectivité :** Soit $\ds n\in\mathbb Z$. Si $\ds n\ge0$, on pose $\ds p=2n$ et on a $\ds \varphi(p)=\frac p2=n$ donc $\ds n$ admet un antécédent. Si $\ds n<0$, on pose $\ds p=-1-2n$. $\ds n<0$ donc $\ds -1-2n\ge 1$ et $\ds \varphi(p)=n$. Donc $\ds n$ admet aussi un antécédent. On a traité tous les cas donc $\ds \varphi$ est bien surjective. 

        On en déduit que $\ds \mathbb Z$ est dénombrable.

!!! exemple
    === "Énoncé"
        Soient $\ds E$ et $\ds F$ deux ensembles dénombrables. Montrer que $\ds E\times F$ est dénombrable. 
    === "Corrigé"
        Soient $\ds E$ et $\ds F$ dénombrables et soit $\ds \varphi_E$ une bijection de $\ds E$ vers $\ds \mathbb N$ et $\ds \varphi_F$ une bijection de $\ds F$ vers $\ds \mathbb N$. Soit $\ds (e,f)\in E\times F$. on défini $\ds \varphi$ sur $\ds E\times F$ dans $\ds \mathbb N^*$ par $\ds \varphi((e,f))=(2\varphi_E(e)+1)2^{\varphi_F(f)}$. Puisque tout entier non nul s'écrit de manière unique sous la forme $\ds n=(2p+1)2^q$ ($q$ est la $\ds 2$-valuation de $\ds n$), l'application $\ds \varphi$ est bijective de $\ds E\times F$ dans $\ds \mathbb N^*$. Et puisqu'il existe une bijection entre $\ds \mathbb N^*$ et $\ds \mathbb N$, on a bien une bijection entre $\ds E\times F$ et $\ds \mathbb N$ : $\ds E\times F$ est dénombrable. 

        Ce résultat se généralise à un produit cartésien d'un nombre fini d'ensembles (par récurrence par exemple).

!!! propriete "Union dénombrable"
    Une union au plus dénombrable d'ensembles dénombrables est au plus dénombrable.


!!! preuve
    Soit $\ds I$ un ensemble dénombrable, et $\ds \varphi_I$ une bijection de $\ds I$ vers $\ds \mathbb N$. Pour tout $\ds i\in I$, on définit un ensemble $\ds A_i$ dénombrable et $\ds \varphi_i$ une bijection de $\ds A_i$ vers $\ds \mathbb N$ . Posons $\ds A=\bigcup_{i\in I}A_i$. Définissons $\ds \varphi$ de $\ds A$ dans $\ds \mathbb N^2$ de la manière suivante : soit $\ds a\in A$, on pose $\ds m(a)=\min\{i\in I|a\in A_i\}$ et $\ds \varphi(a)=(m(a),\varphi_{m(a)}(a))$. Montrons que cette application est bien injective. 

    **injectivité :** Soient $\ds a$ et $\ds b$ deux éléments de $\ds A$ tels que $\ds \varphi(a)=\varphi(b)$. On a donc $\ds m(a)=m(b)$ et donc $\ds a$ et $\ds b$ appartiennent au même ensemble $\ds A_{m(a)}$. Notons $\ds m=m(a)$  pour simplifier : on peut appliquer à $\ds a$ et $\ds b$ la fonction $\ds \varphi_m$. On a alors $\ds \varphi_m(a)=\varphi_m(b)$ or $\ds \varphi_m$ est injective. Donc $\ds a=b$ : $\ds \varphi$ est injective. 

    $\ds \varphi$ est donc un injection de $\ds A$ dans $\ds \mathbb N^2$ et c'est donc une bijection de $\ds A$ dans une partie de $\ds \mathbb N^2$. Or $\ds \mathbb N^2$ est dénombrable donc $\ds A$ est au plus dénombrable dénombrable.


!!! exemple
    === "Énoncé"
        Montrer que l'ensemble des parties de $\ds \mathbb N$ noté $\ds \mathcal P(\mathbb N)$ n'est pas dénombrable. 
    === "Corrigé"
        Supposons que cet ensemble soit dénombrable et proposons une bijection $\ds \varphi$ de $\ds \mathcal P(\mathbb N)$ dans $\ds \mathbb N$. Soit $\ds A=\{k^in \mathbb N| k\notin\varphi(k)\}$. $\ds A$ est une partie de $\ds \mathbb N$ donc il existe $\ds k_0$ tel que $\ds A=\varphi(k_0)$. On cher à déterminer si $\ds k_0$ est dans $\ds A$ ou non. Supposons que $\ds k_0\in A$, alors $\ds k_0\in\varphi(k_0)$ et donc $\ds k_0\notin A$... Supposons que $\ds k_0\notin A$, alors $\ds k_0\notin \varphi(k_0)$ et donc $\ds k_0\in A$. Il est donc impossible de répondre à cette question, on tombe sur un paradoxe, qui est une contradiction logique et on en déduit que notre hypothèse de départ, l'existence d'une bijection entre $\ds \mathbb N$ et $\ds \mathcal P(\mathbb N)$, est fausse : $\ds \mathcal P(\mathbb N)$ est donc non-dénombrable.

!!! exemple 
    === "Énoncé"
        Montrer que $\ds \{0,1\}^{\mathbb N}$ (l'ensemble des suites de $\ds 0$ et de $\ds 1$) est non-dénombrable.
    === "Corrigé"
        On peut facilement créer une bijection entre $\ds \{0,1\}^{\mathbb N}$ et $\ds \mathcal P(\mathbb N)$ : soit $\ds u$ une suite de $\ds 0$ et de $\ds 1$, on lui associe la partie de $\ds \mathbb N$ notée $\ds A$ suivante : Si $\ds u_i=0$, alors $\ds i\notin A$ et si $\ds u_i=1$, $\ds i\in A$. Cette application est bien bijective. Donc si $\ds \{0,1\}^{\mathbb N}$ est dénombrable, alors $\ds \mathcal P(\mathbb N)$ est dénombrable : c'est absurde, donc $\ds \{0,1\}^\mathbb N$ est non-dénombrable.


Jusqu'à présent, nous savions traiter des sommes indicées sur $\ds \mathbb N$. Mais on pourrait vouloir indicer des sommes sur $\ds \mathbb Z$, $\ds \mathbb N^2$... La formalisme suivant permet d'indicer des sommes sur n'importe quel ensemble au plus dénombrable. 

Nous noterons $\ds [0,+\infty]$ l'ensemble $\ds \mathbb R^+$ auquel on ajoute $\ds +\infty$, supérieur à tous les autres réels positifs, et absorbant pour l'addition et la multiplication (c'est à dire $\ds \forall x\in\mathbb R^+,\, x+\;+\infty=+\infty$ et $\ds x\times+\infty=+\infty$).

!!! definition "Somme d'une famille au plus dénombrable"
    Soit $\ds I$ un ensemble au plus dénombrable et soit $\ds {(x_i)}_{i\in I}$ une famille de $\ds [0,+\infty]$. On notera, sans trop se poser de questions à ce sujet, 

    $$
    \sum_{i\in I}x_i
    $$

    la somme des éléments de la famille $\ds {(x_i)}_{i\in I}$. Cette somme est soit finie, soit infinie. On dira que la famille est sommable si sa somme est finie.


!!! propriete "Sommation par paquets (admis)"
    Soit $\ds I$ un ensemble au plus dénombrable, soit $\ds {(x_i)}_{i\in I}$ une famille de $\ds [0,+\infty]$. Soit $\ds (I_n)_{n\in\mathbb N}$ une partition de $\ds I$ c'est à dire que les $\ds I_n$ sont d'intersection nulle et leur réunion vaut $\ds I$. Alors $\ds \sum_{i\in I}x_i=\sum_{n\in\mathbb N}\sum_{i\in I_n}x_i$. 

!!! exemple
    === "Énoncé"
        La famille $\ds {(\frac i{2^j})}_{(i,j)\in\mathbb N}$ est-elle sommable ? 
    === "Corrigé"
        Dans la pratique on fait les calculs et on décide ensuite si c'est sommable ou pas. On a ici $\ds I=\mathbb N^2$. Pour $\ds n\in\mathbb N$, posons $\ds I_n=\{n\}\times N$. La famille des $\ds I_n$ est clairement une partition de $\ds I$ et on a donc : 

        $$
        \begin{aligned}
        \sum_{(i,j)\in\mathbb N^2}\frac i{2^j}&=\sum_{n\in\mathbb N}\sum_{(i,j)\in\{n\}\times\mathbb N}\frac{i}{2^j}\\
        &=\sum_{n\in\mathbb N}\sum_{j\in\mathbb N}\frac n{2^j}\\
        &=\sum_{n\in\mathbb N}n\sum_{j\in\mathbb N}\frac 1{2^j}\\
        &=\sum_{n\in\mathbb N}n\times \frac 1{1-\frac 12}\\
        &=\sum_{n\in\mathbb N}2n\\
        &=+\infty
        \end{aligned}
        $$ 

        Donc la famille n'est pas sommable. 


!!! exemple
    === "Énoncé"
        Soit $\ds q\in[0,1[$, la famille $\ds {(q^{|n|})}_{n\in\mathbb Z}$ est-elle sommable ? 
    === "Corrigé"
        Pareil, on calcule et on verra bien. Ici, $\ds I=\mathbb Z$. Pour $\ds n\in\mathbb N^*$, posons $\ds I_n=\{-n,n\}$ et $\ds I_0=\{0\}$. La famille des $\ds I_n$ forme une partition de $\ds I$. On a donc  :

        $$
        \begin{aligned}
        \sum_{n\in\mathbb Z}q^{|n|}&=\sum_{n\in\mathbb N}\sum_{k\in I_n}q^{|k|}\\
        &=\sum_{n\in\mathbb N^*}\sum_{k\in\{-n,n\}}q^{|k|}\quad+1\\
        &=\sum_{n\in\mathbb N^*}2q^n \quad+1\\
        &=\frac {2q}{1-q}+1\\
        &=\frac{q+1}{q-1}
        \end{aligned}
        $$

        La somme est finie, donc la famille est sommable. 

!!! theoreme "Tout va Bien dans $\ds [0,+\infty]$"
    Dans le  positif, on peut découper, calculer, majorer les sommes directement. Le résultat obtenu vaut preuve de sommabilité.


!!! definition "Sommabilité d'une famille"
    Soit $\ds I$ un ensemble au plus dénombrable et $\ds {(x_i)}_{i\in I}$ une famille de $\ds \mathbb C$. On dit que cette famille est *sommable* si et seulement si la famille $\ds {(|x_i|)}_{i\in I}$ et sommable

    Pour $\ds I=\mathbb N$, la sommabilité est équivalente à la convergence absolue de la série asociée.

!!! propriete "Théorème de comparaison"
    Soit $\ds I$ un ensemble au plus dénombrable, $\ds {(x_i)}_{i\in I}$ une famille de $\ds \mathbb C$ et  $\ds {(y_i)}_{i\in I}$ une famille de $\ds [0,+\infty]$. Si pour tout $\ds i\in I$, $\ds |x_i|\le y_i$ alors la sommabilité de $\ds {(y_i)}_{i\in I}$ implique celle de $\ds {(x_i)}_{i\in I}$. 


!!! theoreme "Tout va bien même dans $\ds \mathbb C$"
    Sous réserve de sommabilité, les sommes se manipulent naturellement : 
    
    - combinaison linéaire : $\ds \sum_{i\in I} \lambda x_i+\mu y_i=\lambda\sum_{i\in I}x_i+\mu\sum_{i\in I y_i}$ ;  
    - sommation par paquets : Si $\ds I = \bigcup_{n\in \mathbb N}I_n$, les $\ds I_n$ étant disjoints, alors $\ds \sum_{i\in I}x_i=\sum_{n\in\mathbb N}\sum_{i\in I_n}x_i$ ;  
    - théorème de Fubini : Soient $\ds I, J$ deux ensembles finis ou dénombrables et $\ds \left(x_{i, j}\right)_{(i, j) \in I \times J}$ une famille de réels indexée par $\ds I \times J$. La famille $\ds \left(x_{i, j}\right)_{(i, j) \in I \times J}$ est sommable si et seulement si pour tout $\ds i \in I,\left(x_{i, j}\right)_{j \in J}$ est sommable et $\ds \left(\sum_{j \in J} x_{i, j}\right)_{i \in I}$ est sommable et, dans ce cas, on a $\ds \sum_{(i, j) \in I \times J} x_{i, j}=\sum_{i \in I}\left(\sum_{j \in J} x_{i, j}\right)$.

    - produit de Cauchy : Soient $\ds \sum_{p \in \mathbb{N}} x_p$ et $\ds \sum_{q \in \mathbb{N}} y_q$ deux séries absolument convergentes. Soit $\ds w_n=\sum_{p+q=n} x_p y_q$ Alors $\ds \sum w_n$ est absolument convergente et $\ds \sum_{n=0}^{+\infty} w_n=\left(\sum_{p=0}^{+\infty} x_p\right)\left(\sum_{q=0}^{+\infty} y_q\right)$.



!!! exemple
    === "Énoncé"
        Calculer $\ds \sum_{p,q\ge 0}\frac{z^p}{q!}$ avec $\ds |z|<1$.
    === "Corrigé"
        On va appliquer le théorème de Fubini. Pour $\ds p$ fixé, $\ds \sum_{q\ge0}\frac{z^p}{q!}$ est sommable de somme $\ds e z^p$. Or $\ds \sum ez^p$ est sommable donc $\ds \sum_{p,q\ge 0}\frac{z^p}{q!}$ est sommable et $\ds \sum_{p,q\ge 0}\frac{z^p}{q!}=\sum_{p\ge 0}z^p\sum_{q\ge 0}\frac 1q!=\frac 1{1-z}\times e=\frac e{1-z}$.  


!!! exemple
    === "Énoncé"
        Soit $\ds a\in\mathbb C$ tel que $\ds |a|<1$. Montrer que $\ds \frac 1{(1-a)^2}=\sum_{n=0}^{+\infty}(n+1)a^n$.
    === "Corrigé"
        On note que $\ds (n+1)=\sum_{k=0}^n 1$ donc $\ds \sum_{n=0}^{+\infty}(n+1)a^n=\sum_{n=0}^{+\infty}\sum_{i=0}^na^n=\sum_{n=0}^{+\infty}\sum_{i=0}^na^ka^{n-k}$. On reconnait alors un produit de Cauchy de la série $\ds \sum a^k$ par elle-même. Or $\ds |a|<1$ donc cette série converge absolument donc le produit de cauchy converge absolument et $\ds \sum_{n=0}^{+\infty}\sum_{i=0}^na^ka^{n-k}=\left(\sum_{n=0}^{+\infty} a^n\right)^2=\left(\frac 1{1-a}\right)^2=\frac 1{(1-a)^2}$. 


## Probabilités, variables aléatoires discrètes et lois usuelles

On souligne ici l'importance d'un formalisme lorsqu'on s'attaque à des probabilités discrètes non finies. Le meilleur exemple est celui du jeu du pile ou face infini : une partie de ce jeu peut être représentée par un élément de $\ds \{0,1\}^{\mathbb N}$ et chaque partie de ce jeu aura une probalité nulle. Mais dans ce cas, tout évènement devrait présenter une probabililté nulle, ce qui n'est a priori pas le cas quand on s'intéresse à des probabilités précises : par exemple quelle est la probabilité d'avoir pile au $\ds n$è lancer ? 

On définit donc un formalisme qui permet de traiter ces cas.

### Univers, événements, variables aléatoires discrètes

!!! definition "Tribu"
    Soit $\ds \Omega$ un ensemble, appelé univers. Soit $\ds \mathcal A$ une partie de $\ds \mathcal P(\Omega)$. $\ds \mathcal A$ est une *tribu* sur $\ds \Omega$ si et seulement si : 

    - i) $\ds \Omega\in \mathcal A$ ;  
    - ii) pour tout $\ds A\in\mathcal A$, $\ds \Omega\setminus A\in A$ ;  
    - iii) pour toute suite $\ds {(A_n)}_{n\in\mathbb N}$ d'éléments de $\ds \mathcal A$, on a $\ds \bigcup_{n\in\mathbb N}A_n\in A$. 

    Ainsi une tribu est une partie de $\ds \mathcal P(\Omega)$ qui contient $\ds \Omega$ et **stable par complémentaire et union dénombrable**.

    La donnée de $\ds \Omega$ associé à une tribu $\ds \mathcal A$, noté $\ds (\Omega,\mathcal A)$ est un *espace probabilisable*

!!! definition "Évènements"
    Les éléments d'une tribu son appelé *évènements*.



!!! propriete "Stabilité par intersection dénombrable"
    Soit $\ds {(A_n)}_{n\in\mathbb N}$ une suite d'évènements de $\ds \mathcal A$. Alors $\ds \bigcap_{n\in\mathbb N} A_n\in \mathcal A$. 

!!! preuve
    Les $\ds A_n$ étant dans $\ds \mathcal A$, par stabilité par complémentaire, $\ds \bar{A_n}$ est dans $\ds \mathcal A$ et par stabilité par réunion, $\ds \bigcup_{n\in\mathbb N}\bar{A_n}$ est dans $\ds \mathcal A$. Or encore une fois $\ds \mathcal A$ est stable par complémentaire donc $\ds \overline{\bigcup_{n\in\mathbb N}\bar{A_n}}$. Or  $\ds \overline{\bigcup_{n\in\mathbb N}\bar{A_n}}=\bigcap_{n\in\mathbb N}{A_n}$. Donc $\ds \bigcap_{n\in\mathbb N}{A_n}\in \mathcal A$.

!!! remarque
    l'évènement $\ds \bigcap_{n\in\mathbb N}{A_n}$ se réalise si et seulement si $\ds \exists w\in \Omega\text{ tq }\forall n\in\mathbb N,\, w\in A_n$. 


!!! exemple
    === "Énoncé"
        Soit $\ds \Omega=\mathbb{Z}$. On considère $\ds \mathcal{T}$ la tribu engendrée par les ensembles $\ds S_n=\{n, n+1, n+2\}$ avec $\ds n \in \mathbb{Z}$. Quels sont les éléments de la tribu $\ds \mathcal{T}$ ?
    === "Corrigé"
        On remarque que $\ds S_{n-2}\cap S_n=\{n\}$. Une tribu étant stable par intersections, on en déduit que $\ds \{n\}\mathcal T$. Mais $\ds \mathcal T$ est stable par union dénombrable et à partir des singletons, on peut générer toutes les parties de $\ds \mathbb Z$ par unions dénombrables. Donc $\ds \mathcal T=\mathcal P(\mathbb Z)$. 

!!! exemple 
    === "Énoncé"
        Soit $\ds \Omega$ un ensemble muni d'une tribu $\ds A$, soit $\ds {(A_n)}_{n\in\mathbb N}$ une suite d'évènements. Soit :

        $$
        D : \text{«tous les évènements de la suite }{(A_n)}_{n\in\mathbb N}\text{ se réalisent sauf un nombre fini»}
        $$

        Montrer que $\ds D$ est un évènement.
    === "Corrigé"
        On peut reformuler $\ds D$ sous la forme :
        
        $$
        D : \text{«Parmi les évènements }\bar{A_n}\text{ seul un nombre fini se réalisent»}
        $$

        Soit $\ds \omega\in\Omega$. Si $\ds \omega\in D$ alors $\ds w$ n'appartient qu'à un nombre fini de $\ds A_n$ : l'ensemble $\ds \{n\in\mathbb N|w\in A_n\}$ est borné. Notons $\ds p$ un majorant, alors pour tout $\ds n\ge p$, $\ds w\in A_n$. On en déduit que $\ds D\subset \bigcup_{p\in\mathbb N}\bigcap_{n\ge p}A_n$. 

        Réciproquement, si $\ds w\in \bigcup_{p\in\mathbb N}\bigcap_{n\ge p}A_n$ alors il existe $\ds p$ tel que $\ds w\in\bigcap_{n\ge p}A_n$. On en déduit que pour que $\ds \omega\in \bar{A_q}$, il faut que $\ds q\le p$ et il n'y a donc qu'un nombre fini de choix. Donc $\ds w\in D$. 

        Finalement, $\ds D= \bigcup_{p\in\mathbb N}\bigcap_{n\ge p}A_n$. Or une tribu est stable par intersection et union dénombrables : $\ds D\in \mathcal A$. 

!!! definition "Variable aléatoire discrète"
    Une *variable aléatoire discrète* $\ds X$ est une application définie sur $\ds \Omega$, telle que $\ds X(\Omega)$ est au plus dénombrable et, pour tout $\ds x \in X(\Omega),\, X^{-1}(\{x\})$ est un événement.

    On notera $\ds X^{-1}(\{x\})$ sous la forme $\ds (X=x)$ ou $\ds \{X=x\}$ et $\ds X^{-1}(A)$ sous la forme $\ds (X\in A)$.

    Lorsque $\ds X$ est à valeurs réelles, on autorisera la notation $\ds (X\ge x)$ par exemple.

!!! propriete "Image réciproque d'une partie"
    Soit $\ds X$ une variable aléatoire discrète sur $\ds (\Omega,\mathcal A)$ à valeurs dans $\ds E$. Soit $\ds A\subset E$, alors $\ds X^{-1}(A)\in \mathcal A$. 

!!! preuve
    Par définition d'une variable aléatoire, on sait que si $\ds A$ est restreint à un élément, $\ds X^{-1}(A)\in\mathcal A$. Dans le cas contraire, notons $\ds A'=A\cap X(\Omega)$. On a évidemment $\ds X^{-1}(A)=X^{-1}(A')$. Dès lors, $\ds X^{-1}(A)=\bigcup_{a\in A} X^{-1}(\{a\})$ et $\ds X(\Omega)$ étant dénombrable, on a ici une union dénombrable d'éléments de $\ds \mathcal A$. Par stabilité par union dénombrable, $\ds X^{-1}(A)\in\mathcal A$. 


!!! exemple
    === "Énoncé"
        On suppose $\ds \Omega$ au plus dénombrable, que l'on munit de la tribu $\ds \mathcal P(\Omega)$. Montrer que toute application définie sur $\ds \Omega$ est une variable aléatoire. 
    === "Corrigé"
        Soit $\ds f$ une application définie sur $\ds \Omega$ à valeur dans $\ds E$. $\ds \Omega$ étant au plus dénombrable, $\ds f(\Omega)=\bigcup_{\omega\in \Omega}\{f(\omega)\}$ : c'est une union au plus dénombrable d'ensembles finis, donc elle est au plus dénombrable. Ainsi, $\ds f(\Omega)$ est au plus dénombrable. Soit $\ds A\subset E$, alors $\ds f^{-1}(A)\subset \Omega$ c'est à dire $\ds f^{-1}(A)\in\mathcal P(\omega)$ qui est la tribu choisie. Donc $\ds f^{-1}(\Omega)$ est bien un évènement : $\ds f$ est une variable aléatoire discrète.

### Probabilité

!!! definition "Probabilité"
    Soit $\ds \Omega$ un ensemble muni d'une tribu $\ds \mathcal A$. Une *probabilité* sur $\ds (\Omega,\mathcal A)$ est une application $\ds \mathbb P:\mathcal A\to [0,1]$ telle que :
    
    - i) $\ds \mathbb P(\Omega)=1$  
    - ii) **$\sigma$-additivité :** Si $\ds {(A_n)}_{n\in\mathbb N}$ est une suite d'évènements deux àdeux incompatibles, alors la  série $\ds \sum \mathbb P(A_n)$ converge et on a 

    $$
    \P\left(\bigcup_{n\in\mathbb N}A_n\right)=\sum_{n=0}^{+\infty}\P(A_n).
    $$  

    $\ds (\Omega,\mathcal A,\mathbb P)$ est un *espace probabilisé*.

!!! propriete "Propriétés de calcul"
    Soit $\ds (\Omega,\mathcal A, \mathbb P)$ un espace probabilisé, soit $\ds A,B$ deux éléments de $\ds \mathcal A$, alors :

    - si $\ds A$ et $\ds B$ sont incompatibles, alors $\ds \mathbb P(A\cup B)=\mathbb P(A)+\mathbb P(B)$ ;  
    - Si $\ds B\subset A$, alors $\ds \mathbb P(A\setminus B)=\mathbb P(A)-\mathbb P(B)$ ;  
    - $\ds \mathbb P(\bar A)=1-\mathbb P(A)$ ;
    - si $\ds A$ et $\ds B$ sont quelconques, alors $\ds \mathbb P(A\cup B)=\mathbb P(A)+\mathbb P(B)-\mathbb P(A\cap B)$. 

!!! preuve
    - Si $\ds A$ et $\ds B$ sont incompatibles, alors par $\ds \sigma$-additivité, $\ds \mathbb P(A\cup B)=\mathbb P(A)+\mathbb P(B)$.

    - On a $\ds A=(A\setminus B)\cup B$, cette union étant disjointe : les évènements $\ds A\setminus B$ et $\ds B$ sont incompatibles. Alors d'après le point précédent, $\ds \mathbb P(A)=\mathbb P(A\setminus B)+\mathbb P(B)$ et donc $\ds \mathbb P(A\setminus B)=\mathbb P(A)-\mathbb P(B)$. 

    - Ce point est un cas particulier du précédent : $\ds A\subset \Omega$ donc $\ds \mathbb P(\bar A)=\mathbb P(\Omega\setminus A)=\P(\Omega)-\mathbb P(A)=1-\P(A)$.

    - On passe par une réécriture : On note que $\ds A\cup B=(A\setminus (A\cap B))\cup (B\setminus(A\cap B))\cup (A\cap B)$ cette union étant une union d'évènements incompatibles. Ainsi, $\ds \P(A\cup B)=\P(A\setminus (A\cap B))+\P(B\setminus(A\cap B))+\P(A\cap B)=\P(A)-\P(A\cap B)+\P(B)-\P(A\cap B)+\P(A\cap B)=\P(A)+\P(B)-\P(A\cap B)$. 

!!! propriete "Croissance de la probabilité"
    Soient $\ds A$ et $\ds B$ deux évènements. Si $\ds A\subset B$ alors $\ds \P(A)\le \P(B)$.

!!! preuve
    Si $\ds A\subset B$ alors $\ds B=A\cup(B\setminus A)$, l'union étant disjointe. Alors $\ds \P(B)=\P(A)+\P(B\setminus A)$. Or $\ds \P(B\setminus A)\ge 0$ donc $\ds \P(B)\ge \P(A)$. 


!!! propriete "Continuité monotone"
    Soit $\ds (\Omega,\mathcal A, \P)$ un espace probabilisé.

    - Soit $\ds {(A_n)}_{n\in\mathbb N}$ une suite d'évènements de $\ds \mathcal A$. Si pour tout $\ds n$, $\ds A_n\subset A_{n+1}$ (on dit que la suite est croissante) alors $\ds \P(\bigcup_{n\in\mathbb N}A_n)=\lim_{n\to+\infty}\mathbb P(A_n)$. 

    - Soit $\ds {(A_n)}_{n\in\mathbb N}$ une suite d'évènements de $\ds \mathcal A$. Si pour tout $\ds n$, $\ds A_{n+1}\subset A_{n}$ (on dit que la suite est décroissante) alors $\ds \P(\bigcap_{n\in\mathbb N}A_n)=\lim_{n\to+\infty}\mathbb P(A_n)$. 

!!! preuve
    On démontre ici la continuité croissante. On suppose donc que pour tout entier $\ds n$, $\ds A_n\subset A_{n+1}$. Définissons la famille $\ds B_n=A_{n}\setminus A_{n-1}$ et $\ds B_0=A_0$. Alors cette famille est une famille d'évènements incompatibles. Par $\ds \sigma$-additivité, $\ds \P(\bigcup_{n\in\mathbb N}B_n)=\sum_{n=0}^{+\infty}\P(B_n)$. Or $\ds \P(B_n)=\P(A_{n})-\P(A_{n-1})$ et donc par télescopage, $\ds \sum_{n=0}^N\P(B_n)=\P(A_{N+1})$. On en déduit  $\ds \P(\bigcup_{n\in\mathbb N}B_n)=\lim_{n\to+\infty}\P(A_n)$. Or $\ds \bigcup_{n\in\mathbb N}B_n=\bigcup_{n\in\mathbb N}A_n$ donc $\ds \P(\bigcup_{n\in\mathbb N}A_n=\lim_{n\to+\infty}\P(A_n)$. 

    On montre de même la continuité décroissante en travaillant sur les complémentaires.


!!! exemple
    === "Énoncé"
        Soit $\ds {(A_n)}_{n\in\mathbb N}$ une suite d'évènements quelconques. Déterminer $\ds \lim_{n\to+\infty}\P\left(\bigcup_{k=0}^{n}A_k\right)$ et $\ds \lim_{n\to+\infty}\P\left(\bigcap_{k=1}^{n}A_k\right)$.
    === "Corrigé"
        Le but est de se ramener à nue histoire de continuité monotone. Dans le premier cas, posons $\ds B_n=\bigcup_{k=0}^nA_k$. On remarque que $\ds B_k\subset N_{k+1}$. La suite d'évènements $\ds B_k$ est une suite de $\ds \mathcal A$ par stabilité par union, croissante. Par ailleurs, $\ds \bigcup _{k=0}^n B_k=\bigcup_{k=0}^n A_k$. Alors par continuité croissante, $\ds \P\left(\bigcup_{k\in \mathbb N}B_k\right)=\lim_{n\to+\infty}\P(B_n)$. Or $\ds \P\left(\bigcup_{k\in\mathbb N}B_k\right)=\P\left(\bigcup_{k\in\mathbb N}A_k\right)$ et $\ds \lim_{n\to+\infty}\P(B_n)=\lim_{n\to+\infty}\P\left(\bigcup_{k=0}^nA_k\right)$. On en déduit que $\ds \lim_{n\to+\infty}\P\left(\bigcup_{k=0}^{n}A_k\right)=\P\left(\bigcup_{k\in\mathbb N}A_k\right)$.

        De même si  on prend $\ds C_n=\bigcap_{k=0}^nA_k$ on obtient une suite décroissante d'évènements de $\ds A$ et par continuité décroissante, $\ds \lim_{n\to+\infty}\P(C_n)=\P\left(\bigcap_{n\in\mathbb N}C_n\right)$ ce qu'on peut réécrire sous la forme $\ds \lim_{n\to+\infty}\P\left(\bigcap_{k=1}^n A_k\right)=\P\left(\bigcap_{k\in\mathbb N}A_k\right)$.
    

!!! propriete "Sous-additivité"
    Soit $\ds (\Omega,\mathcal A,\P)$ un espace probabilisé. Soit $\ds {(A_n)}_{n\in\mathbb N}$ une suite d'évènements de $\ds \mathcal A$. Alors :

    - cas fini : pour tout entier $\ds n$, $\ds \P(A_1\cup\cdots\cup A_n)\le\P(A_1)+\cdots+\P(A_n)$.  
    - cas dénombrable : Si la série $\ds \sum\P(A_n)$ converge alors $\ds \P\left(\bigcup_{n\in\mathbb N}A_n\right)\le\sum_{n=0}^{+\infty}\P(A_n)$. 

!!! preuve
    Le cas fini se traite par récurrence. On pose $\ds H_n$ la propriété «Si $\ds A_1,\dots,A_n$ est une famille de $\ds \mathcal A$ alors $\ds \P(A_1\cup\cdots\cup A_n)\le\P(A_1)+\cdots+\P(A_n)$.»

    ^^initialisation :^^ trivial pour $\ds n=1$

    ^^hérédité :^^ Soit $\ds n$ un entier naturel, supposons que $\ds H_n$ est vraie, montrons $\ds H_{n+1}$. Soit $\ds A_1,\dots,A_n,A_{n+1}$ des éléments de $\ds \mathcal A$. D'après l'hypothèse de récurrence, $\ds \P(A_1\cup\cdots\cup A_n\cup A_{n+1})\le\P(A_1)+\cdots+\P(A_{n-1})+\P(A_n\cup A_{n+1})$. Or $\ds \P(A_n\cup A_{n+1})=\P(A_n)+\P(A_{n+1})-\P(A_n\cap A_{n+1})\le \P(A_n)+\P(A_{n+1})$. On en déduit $\ds \P(A_1\cup\cdots\cup A_n\cup A_{n+1})\le\P(A_1)+\cdots+\P(A_{n-1})+\P(A_n)+\P(A_{n+1})$ et donc $\ds H_{n+1}$ est vérifiée. 

    Pour le cas dénombrable, on utilise la continuité croissante. En effet par continuité croissante, $\ds \P\left(\bigcup_{k=0}^n A_k\right)\to \P\left(\bigcup_{k\in\mathbb N}A_k\right)$. Par ailleurs, d'après le cas fini, $\ds \P\left(\bigcup_{k=0}^n A_k\right)\le\sum_{k=0}^n \P(A_k)$. Or la série $\ds \sum\P(A_n)$ converge donc $\ds \P\left(\bigcup_{k=0}^n A_k\right)\to\sum_{k=0}^{+\infty}\P(A_k)$. On en déduit par unicité de la limite que $\ds \P\left(\bigcup_{k\in\mathbb N}A_k\right)=\sum_{k=1}^{+\infty}\P(A_k)$.


!!! definition "Évènement presque sûr/négligeable"
    Un évènement est *presque sûr* s'il est de probabililté $\ds 1$. Un évènement est négligeable s'il est de probabilité $\ds 0$.

    Des évènements forment un *système quasi complet* s'ils sont deux à deux incompatibles et si leur union est un évènement presque sûr. 

!!! exemple
    === "Énoncé"
        Soit $\ds (\Omega,\mathcal A,\P)$ un espace probabilisé. Montrer qu'une union finie ou dénombrable d'évènements négligeables est négligeable. 
    === "Corrigé"
        Le cas fini est simple, concentrons-nous sur le cas dénombrable.

        Soit $\ds {(A_n)}_{n\in\mathbb N}$ une suite d'évènements de $\ds \mathcal A$.  Alors pour tout entier $\ds n$, $\ds \P(A_n)=0$. On en déduit que $\ds \sum\P(A_n)$ converge et que sa somme vaut $\ds 0$. Par sous-additivité, $\ds \P(\bigcup_{k\in\mathbb N}A_k)\le\sum_{k=0}^n\P(A_n)=0$. Donc $\ds \P\left(\bigcup_{k\in\mathbb N}A_k\right)=0$ : la réunion dénombrable est bien négligeable.

!!! exemple
    === "Énoncé"
        Soit $\ds A$ et $\ds B$ deux évènements, avec $\ds B$ presque sûr. Montrer que $\ds \P(A\cap B)=\P(A)$. 
    === "Corrigé"
        On a $\ds \P(A\cap B)=\P(A)+\P(B)-\P(A\cup B)$. Or $\ds B\subset A\cup B$ donc $\ds \P(B)\le \P(A\cup B)\le 1$ et donc $\ds \P(A\cup B)=1$. On a alors $\ds \P(A\cap B)=\P(A)+\P(B)-\P(A\cup B)=\P(A)+1-1=\P(A)$. 

!!! exemple
    === "Énoncé"
        Soit $\ds A$ et $\ds B$ deux évènements, avec $\ds B$ négligeable. Montrer que $\ds \P(A\cup B)=\P(A)$. 
    === "Corrigé"
        On a $\ds \P(A\cup B)=\P(A)+\P(B)-\P(A\cap B)$. Or $\ds A\cap B\subset B$ donc $\ds 0\le \P(A\cap B)\le \P(B)$ et donc $\ds \P(A\cap B)=0$. On a alors $\ds \P(A\cup B)=\P(A)+\P(B)-\P(A\cap B)=\P(A)+0-0=\P(A)$. 




### Probabilités conditionnelles

!!! definition "Probabilité conditionnelle"
    Si $\ds \P(B)>0$, la probabilité conditionnelle de $\ds A$ sachant $\ds B$ est définie par la relation $\ds \P(A \mid B)=\P_B(A)=\frac{\P(A \cap B)}{\P(B)}$.

!!! propriete "Probabilité"
    Si $\ds (\Omega,\mathcal A,\P)$ est un espace probabilisé et $\ds B$ un évènement de $\ds \mathcal A$ tel que $\ds \P(B)>0$, alors $\ds (\Omega,\mathcal A,\P_B)$ est un espace probabilisé. 

!!! preuve
    Il s'agit de montrer que $\ds \P_B$ est bien une probabilité sur $\ds (\Omega,\mathcal A)$. Tout d'abord, $\ds \P_B(\Omega)=\frac{\P(\Omega\cap B)}{\P(B)}=1$. Puis si $\ds {(A_n)}_{n\in\mathbb N}$ est une suite d'évènements deux à deux incompatibles, posons $\ds B_n=A_n\cap B$. La suite d'évènements $\ds B_n$ est une suite d'évènements deux à deux incompatibles. Par $\ds \sigma$-additivité de $\ds \P$, $\ds \P(\bigcup_{n\in\mathbb N}A_n\cap B)=\sum_{n=0}^{+\infty}\P(A_n\cap B)$.En divisant par $\ds \P(B)$, on a $\ds \P_B(\bigcup_{n\in\mathbb N}A_n)=\sum_{n=0}^{+\infty}\P_B(A_n)$ ce qui donne la $\ds \sigma$-additivité de $\ds \P_B$. 

!!! propriete "Formule des probabilités composées"
    Soit $\ds (\Omega,\mathcal A,\P)$ un espace probabilisé. On pose $\ds n\ge2$. Soit $\ds A_1,\dots,A_n$ $\ds n$ évènements de $\ds \mathcal A$ vérifiant $\ds \P(A_1\cap\cdots\cap A_n)\ne 0$. Alors $\ds \P(\bigcap_{k=1}^n A_k)=\P(A_1)\prod_{k=2}^n\P(A_k|A_1\cap\cdots\cap A_{k-1})$.

!!! preuve
    revoir la preuve de première année.

!!! exemple
    === "Énoncé"
        Une urne contient $\ds n$ boules blanches et $\ds n$ boules rouges.  
        On tire successivement et sans remise $\ds n$ boules dans cette urne.  
        Déterminer la probabilité qu'au moins une boule rouge figure dans ce tirage.  
    === "Corrigé"
        Notons $\ds B_i$ l'évènement «la boule $\ds i$ est blanche» et $\ds A$ l'évènement «Au moins une boule rouge est dans le tirage». On a $\ds A=\overline{B_1\cap\dots\cap B_n}$. Calculons donc $\ds \P(B_1\cap\dots\cap B_n)$ avec la formle des probabilités composées. On a en effet $\ds \P(B_k|B_1\cap\dots\cap B_{k-1})=\frac{n-k+1}{2n-k+1}$ et donc 
        
        $$
        \begin{aligned}
        \P(B_1\cap\dots\cap B_n)&=\P(B_1)\P(B_2|B_1)\cdots\P(B_n|B_1\cap\cdots\cap B_{n-1})\\
        &=\frac n{2n}\times \frac{n-1}{2n-1}\dots\frac{1}{n+1}\\
        &=\frac{n!n!}{(2n)!}
        \end{aligned}
        $$

        On en déduit que $\ds P(A)=1-\frac1{\binom{2n}n}$.

!!! propriete "Formule des probabilités totales"
    Soit $\ds (\Omega,\mathcal A,\P)$ un espace probabilisé et $\ds {(A_n)}_{n\in\mathbb N}$ un système quasi complet d'évènements. Soit $\ds B$ un évènement de $\ds \mathcal A$, alors $\ds \sum \P(A_n\cap B)$ converge et :

    $$
    P(B)=\sum_{n=0}^{+\infty}\P(B\cap A_n)
    $$

!!! preuve
    Attention, le fait que $\ds (A_n)$ soit un système quasi-complet d'évènements ne permet pas d'écrire $\ds B=B\cap\bigcup_{n\in\mathbb N}A_n$ : en effet, a priori $\ds \bigcup_{n\in\mathbb N}A_n\ne\Omega$. Posons d'ailleurs $\ds C=\Omega\setminus\left(\bigcup_{n\in\mathbb N}A_n\right)$. Cette fois-ci, on a bien $\ds \Omega=\bigcup_{n\in\mathbb N}A_n\cup C$, les unions étant disjointes. De plus $\ds \P(C)=0$ : $\ds C$ est un évènement négligeable. On a alors $\ds B=B\cap\left(\bigcup_{n\in\mathbb N} A_n\cup C\right)=\left(\bigcup_{n\in\mathbb N}(A_n\cap B)\right)\cup (B\cap C)$ et par $\ds \sigma$-additivité, $\ds \P(B)=\sum_{n=0}^{+\infty}\P(A_n\cap B) \quad+\P(C\cap B)$. Or $\ds C$ est négligeable donc $\ds \P(C\cap B)=0$ d'où :

    $$
    \P(B)=\sum_{n=0}^{+\infty}\P(A_n\cap B).
    $$



!!! exemple
    === "Énoncé"
        Soient $\ds N+1$ urnes numérotées de 0 à $\ds N$. L'urne numéro $\ds k$ contient $\ds k$ boules blanches et $\ds N-k$ boules noires. On choisit une urne au hasard. Dans l'urne choisie, on tire, avec des tirages indépendants, des boules avec remise. Soit $\ds n \in \mathbb{N}$. Quelle est la probabilité $\ds p_N(n)$ que la $\ds (n+1)$-ème boule tirée soit blanche sachant que les $\ds n$ précédentes l'étaient toutes? Déterminer $\ds \lim _{N \rightarrow+\infty} p_N(n)$.
    === "Corrigé"
        Notons $\ds B_n$ l'évènement «la $\ds n$è boule est blanche» et $\ds U_k$ l'évènement «on choisit l'urne $\ds k$». Le choix de l'urne est équiprobable et on choisi forcément une urne donc les $\ds U_k$ forment un système complet d'évènements. On peut alors appliquer la formule des probabilités totales : 

        $$
        \P(B_1\cap\cdots\cap B_i)=\sum_{k=0}^N \P(B_1\cap\cdots\cap B_i|U_k)\P(U_k)=\sum_{k=0}^N\left(\frac kN\right)^n\times 1{N+1}
        $$

        et donc 

        $$
        \P(B_{n+1}|B_1\cap\dots\cap B_n)=\frac{\P(B_1\cap\dots\cap B_{n+1})}{\P(B_1\cap\dots\cap B_n)}=\frac{\sum_{k=O}^{N}\left(\frac{k}{N}\right)^{n+1}}{\sum_{k=O}^{N}\left(\frac{k}{N}\right)^{n}}
        $$

        On reconnait au numérateur et au dénominateur des sommes de Riemann : $\ds \frac 1N\sum_{k=0}^N\left(\frac kN\right)^n\to \int_0^1x^n\d x=\frac 1{n+1}$ et  $\ds \frac 1N\sum_{k=0}^N\left(\frac kN\right)^{n+1}\to \int_0^1x^{n+1}\d x=\frac 1{n+2}$. On en déduit que $\ds p_N(n)\xrightarrow[N\to+\infty]{}\frac {n+1}{n+2}$.

!!! propriete "Formule de Bayes"
    Soit $\ds (\Omega,\mathcal A,\P)$ un espace probabilisé. Soit $\ds A$ et $\ds B$ deux éléments de $\ds \mathcal A$ de probabilité non nulle, alors 

    $$
    \P(A|B)=\frac{\P(B|A)\P(A)}{\P(B)}.
    $$

!!! preuve
    Par définition de la probabilité conditionnelle, $\ds \P(A|B)=\frac{\P(A\cap B}{\P(B)}$ et $\ds \P(B|A)=\frac{\P(A\cap B}{\P(A)}$. En combinant ces deux égalités, sachant que $\ds P(B)\ne 0$, on obtient la formule de Bayes.


!!! exemple 
    === "Énoncé"
        Une boite contient deux dés. L'un est équilibré, l'autre donne $\ds 6$ une fois sur deux. On choisit un dé au hasard, on le lance et on obtient $\ds 6$. Quelle est la probabilité que le dé choisi soit le dé déséquilibré ?

    === "Corrigé"
        On note $\ds A$ l'évènement «on choisit le dé pipé» et $\ds B$ l'évènement «on obtient un $\ds 6$». On connaît $\ds \P(A)=\frac 12$ et $\ds \P(B|A)=\frac 12$ et $\ds \P(B|\bar A)=\frac 16$. D'après la formule des probabilités totales, on a $\ds \P(B)=\P(A)\P(B|A)+\P(\bar A)\P(B|\bar A)=\frac 12 \times \frac 12+\frac 12\times\frac 16=\frac 13$. D'après la formule de Bayes, on a donc $\ds \P(A|B)=\frac{\P(B|A)\P(A)}{\P(B)}=\frac{\frac 12\times\frac 12}{\frac 13}=\frac 34$. 

    

### Loi d’une variable aléatoire discrète

!!! definition "Loi d'une variable aléatoire"
    Soit $\ds X$ une variable aléatoire discrète sur un espace probabilisé $\ds (\Omega,\mathcal A,\P)$. Soit $\ds \P_X$ l'application définie de $\ds \mathcal P(X(\Omega))$ dans $\ds [0,1]$ par $\ds \forall A\subset X(\Omega),\, \P_X(A)=\P(X\in A)$ est une probabilité sur l'espace probabilisable $\ds (X(\Omega),P(X(\Omega)))$. Cette loi est appelée *loi de $\ds X$*.

    Si $\ds X$ et $\ds Y$ suivent la même loi (ou ont la même loi), on notera $\ds X\sim Y$. 

!!! remarque 
    $\ds X(\Omega)$ est au plus dénombrable, donc $\ds \mathcal P(X(\Omega))$ est naturellement une tribu sur $\ds X(\Omega)$. 


!!! remarque 
    Pour connaître $\ds \P_X$ il suffit de connaître $\ds \P(X=x)$ pour tout $\ds x\in X(\Omega)$. 


!!! propriete "Composition"
    Soit $\ds X$ une variable aléatoire discrète sur $\ds (\Omega,\mathcal A,\P)$. Soit $\ds f$ une application définie sur $\ds X(\Omega)$. Alors $\ds f\circ X$ est une variable aléatoire  notée $\ds f(X)$ dont la loi est définie par :
    
    $$
    \forall y \in X(\Omega), \mathbb P(f(X)=y)=\sum_{x\in f^{-1}(\{y\})}P(X=x).
    $$

!!! preuve
    Le programme de PSI demande explicitement de ne pas soulever de difficulté à ce sujet.

!!! propriete "Variables de même loi"
    Si $\ds X$ et $\ds Y$ sont deux variables aléatoires discrètes de même loi sur un espace probabilisé $\ds (\Omega,\mathcal A,\P)$. Soit $\ds f$ une application définie sur $\ds X(\Omega)$. Alors $\ds f(X)\sim f(Y)$. 

!!! preuve
    Soient $\ds X$ et $\ds Y$  deux variables aléatoires discrètes sur $\ds (\Omega,\mathcal A,\P)$ de même loi. Soit $\ds y\in X(\Omega)$, on a $\ds \P(f(X)=y)=\sum_{x\in f^{-1}(\{y\})}P(X=x)\underset{X\sim Y}=\sum_{x\in f^{-1}(\{y\})}P(Y=x)=P(f(Y)=x)$. Donc $\ds f(X)\sim F(Y)$. 


!!! definition "Variable géométrique"
    Soit $\ds p\in]0,1[$, on dit que $\ds X$ suit une variable géométrique de paramètre $\ds p$ et on note $\ds X\sim \mathcal G(p)$ lorsque :

    $$
    \forall k\in\mathbb N^*,\,\mathbb P(X=k)=p(1-p)^{k-1}.
    $$ 


!!! exemple
    === "Énoncé"
        Si $\ds X\sim \mathcal G(p)$, montrer que pour tout $\ds k\in\mathbb N^*$, $\ds \P(X>k)=(1-p)^k$. Étudier la réciproque.
    === "Corrigé"
        L'évenèment $\ds \{X>k\}$ peut s'écrire : $\ds \{X>k\}=\bigcup_{j=k+1}^{+\infty}\{X=j\}$. C'est donc une union dénombrable et incompatible d'évènements. Par $\ds \sigma$-additivité, $\ds \P(X>k)=\sum_{j=k+1}^{+\infty}\P(X=j)=\sum_{j=k+1}^{+\infty}p(1-p)^{j-1}=p(1-p)^k\frac 1{1-(1-p)}=(1-p)^k$. 

        Réciproque : Supposons que pour tout $\ds k\in\mathbb N^*$, $\ds \P(X>k)=(1-p)^k$ alors $\ds \P(X=k)=\P(X>k-1)-\P(X>k)=(1-p)^{k-1}-(1-p)^k=(1-(1-p))(1-p)^{k-1}=p(1-p)^{k-1}$. 

!!! exemple
    === "Énoncé"
        Soit $\ds (\Omega,\mathcal A,\P)$ un  espace probabilisé, soit $\ds {(X_k)}_{k\in\mathbb N^*}$ une suite de variables aléatoires de Bernoulli indépendantes sur $\ds \Omega$, de même paramètre $\ds p\in]0,1[$. On définit $\ds X$ sur $\ds \Omega$ par : 

        $$
        X(\omega) = \min\{k\in \mathbb N|U_k(\omega)=1\}
        $$  

        avec la convention $\ds \min\emptyset=+\infty$.  Montrer que $\ds X$ est une variable aléatoire qui suit une loi géométrique de paramètre $\ds p$
    === "Corrigé"
        $\ds X(\Omega)=\mathbb N^*\cup\{+\infty\}$ donc $\ds X(\Omega)$ est au plus dénombrable. Soit $\ds x\in\mathbb N^*$, $\ds X^{-1}(\{x\})=\bigcap_{k=1}^{x-1} (X_k=0)\cap (X_x=1)$. Donc $\ds X^{-1}(\{x\})$ est une intersection finie d'évènements : c'est un évènement. De plus $\ds X^{-1}(+\infty)=\bigcap_{k=1}^{+\infty}(X_k=0)$ est une intersection dénombrable d'évènements : c'est un évènement. Ainsi, $\ds X$ est bien une variable aléatoire discrète.

        Par ailleurs, pour $\ds n\in\mathbb N^*$, $\ds \P(X=n)=\P(\bigcap_{k=1}^{n-1} (X_k=0)\cap (X_n=1))\underset{ind.}=\prod_{k=1}^{n-1}\P(X_k=0)\times \P(X_n=1)=(1-p)^{n-1}p$ : $\ds X$ suit effectivement une variable géométrique de paramètre $\ds p$.

        **remarque :** Formellement, $\ds X(\Omega)$ n'est pas égal à $\ds \mathbb N^*$ mais à $\ds \mathbb N^*\cup\{+\infty\}$. Cependant, on a ici $\ds \P(X\in\mathbb N^*)=\sum_{k=1}^{+\infty}p(1-p)^{k-1}=p\frac{1}{1-(1-p)}=1$ et donc $\ds \P(X=+\infty)=0$. C'est donc un évènement négligeable.

!!! remarque
    C'est un schéma qu'il faut savoir reconnaître : si on étudie une suite de variables de Bernoulli et qu'on s'intéresse à l'apparition du premier succès, ou du premier échec, alors la variable aléatoire associée suit une loi géométrique. 


!!! definition "Variable de Poisson"
    Soit $\ds \lambda>0$, on dit que $\ds X$ suit une loi de Poisson de paramètre $\ds \lambda$ lorsque $\ds X(\omega)=\mathbb N$ et 

    $$
    \forall k\in\mathbb N,\, \P(X=k)=\frac{\lambda^k}{k!}e^{-\lambda}.
    $$

    On notera $\ds X\sim \mathcal P(\lambda)$.

!!! remarque
    On a bien $\ds \P(X\in\mathbb N)=\left(\sum_{k=0}^{+\infty}\frac{\lambda^k}{k!}\right)e^{-\lambda}=e^\lambda e^{-\lambda}=1$. 

!!! remarque
    Merci de bien mettre une majuscule à Poisson. Cela ne se traduit pas en anglais par "fish law"...


!!! exemple
    === "Énoncé"
        Soit $\ds X$ une variable aléatoire suivant une loi de Poisson de paramètre $\ds \lambda>0$. Déterminer la probabilité que la valeur de $\ds X$ soit paire.
    === "Corrigé"
        On a pour tout $\ds n\in\mathbb N$, $\ds \P(X=n)=\frac{\lambda^n}{n!}e^{-\lambda}$. On cherche donc à calculer $\ds \sum_{p=0}^{+\infty}\P(X=2p)=\sum_{p=0}^{+\infty}\frac{\lambda^{2p}}{(2p)!}e^{-\lambda}=e^{-\lambda}\cosh(\lambda)$. 


!!! exemple
    === "Énoncé"
        Soit $\ds X$ une variable aléatoire discrète suivant une loi de Poisson de paramètre $\ds \lambda$. Quelle est la valeur la plus probable de $\ds X$ ?
    === "Corrigé"
        Soit $\ds k\in\mathbb N$, on a $\ds \frac{\P(X=k+1)}{\P(X=k)}=\frac{\lambda}{k+1}$. On en déduit que la suite des $\ds \P(X=k)$ est croissante tant que $\ds \lambda>k$ et décroissante lorsque $\ds \lambda<k$. On a donc un maximum autour de $\ds \lambda$. Si $\ds \lambda$ est entier, on a $\ds \frac{\P(X=\lambda)}{\P(X=\lambda-1)}=1$ donc le maximum est atteint en $\ds \lambda-1$ et $\ds \lambda$. Si $\ds x\notin \mathbb N$, le maximum n'est atteint qu'en $\ds \lfloor\lambda\rfloor$.  

!!! definition "Couple de variables aléatoires"
    Un couple de variables aléatoires est une variabe aléatoire à valeurs dans un ensemble produit. On notera $\ds \P((X,Y)=(x,y))$ par $\ds \P(X=x,Y=y)$. 

    On appelle loi conjointe la loi de la variable aléatoire $\ds (X,Y)$.

    On appelle lois marginales les lois de $\ds X$ et de $\ds Y$.

!!! definition "Loi conditionnelle"
    Soit $\ds Y$ une variable aléatoire discrète sur $\ds (\Omega,\mathcal A,\P)$. Soit $\ds A\in\mathcal A$. La loi conditionnelle de $\ds Y$ sachant $\ds A$ est la loi de $\ds Y$ sur l'espace probabilisé $\ds (\Omega,\mathcal A,\P_B)$. 

!!! remarque
    On a donc, pour tout $\ds A\subset Y(\Omega)$, $\ds \P_B(Y\in A)=\frac{\P((X\in A)\cap B)}{\P(B)}$.


!!! exemple
    === "Énoncé"
        Soient $\ds X$ et $\ds Y$ deux variables aléatoires à valeurs dans $\ds \mathbb{N}$.
        On suppose que $\ds X$ suit une loi de Poisson de paramètre $\ds \lambda>0$ et que la loi de $\ds Y$ sachant $\ds X=n$ est binomiale de paramètres $\ds n$ et $\ds p \in]0;1[$.  
        (a) Déterminer la loi conjointe de $\ds X$ et $\ds Y$.  
        (b) Reconnaître la loi de $\ds Y$.
    === "Corrigé"
        La variable aléatoire $\ds (X,Y)$ évolue dans $\ds \mathbb N\times \mathbb N$. On cherche donc à calculer, pour tout $\ds (x,y)\in\mathbb N^2$ la valeur de $\ds P(X=x,Y=y)$. On sait que $\ds P(Y=y|X=x)=\binom xy p^y(1-p)^{x-y}$ et $\ds P(X=x)=\frac{\lambda^x}{x!}e^{-\lambda}$. On en déduit que $\ds P(X=x,Y=y)= \binom xy p^y(1-p)^{x-y}\frac{\lambda^x}{x!}e^{-\lambda}$. Si $\ds y>x$ on a $\ds \P(X=x,Y=y)=0$.

        On a alors par la formule des probabilités totales :

        $$
        \begin{aligned}
        P(Y=y) &= \sum_{x=y}^{+\infty}\binom xy p^y(1-p)^{x-y}\frac{\lambda^x}{x!}e^{-\lambda}\\
        &= p^ye^{-\lambda}  \sum_{x=0}^{+\infty}\binom{x+y}y(1-p)^x\frac{\lambda^{x+y}}{(x+y)!}\\
        &=\frac{p^y}{y!}\lambda^ye^{-\lambda}  \sum_{x=0}^{+\infty}\frac{((1-p)\lambda)^{x}}{x!}\\
        &=\frac{p^y}{y!}\lambda^ye^{-\lambda}e^{(1-p)\lambda}\\
        &=\frac{(p\lambda)^y}{y!}e^{-p\lambda}
        \end{aligned}
        $$

        Donc $\ds Y$ suit une loi de Poisson de paramètre $\ds \lambda p$. 



### Événements indépendants

!!! definition "Évènements indépendants"
    Soit $\ds (\Omega,\mathcal A,\P)$ un espace probabilisé. Deux évènements sont dits *indépendants*  lorsque $\ds \P(A\cap B)=\P(A)\P(B)$. Si $\ds \P(A)\ne 0$, cela revient à $\ds \P_A(B)=\P(B)$. 

    Une famille finie $\ds (A_1,\dots,A_n)$ d'évènements est mutuellement indépendante si pour toute sous-famille d'évènements $\ds (A_{i_1},\dots,A_{i_p})$ on a :

    $$
    \P(A_{i_1}\cap\cdots\cap A_{i_p})=\prod_{k=1}^p \P(A_{i_k}).
    $$


!!! exemple
    === "Énoncé"
        On considère un lancer deux dés indépendants et équilibrés. On note $\ds A$ l'évènement «le dé numéro 1 est pair», $\ds B$ l'évènement «le dé numéro 2 est impair» et $\ds C$ l'évènement «la somme des dés est impaire». Montrer que les évènements sont deux à deux indépendants mais pas mutuellement indépendants.
    === "Corrigé"
        On peut facilement calculer les différentes probabilités par comptage : le tableau suivant représente les lancers possibles :
        
        | D1,D2 | 1 | 2 | 3 | 4  | 5  | 6  |
        |-------|---|---|---|----|----|----|
        | 1     | 2 | 3 | 4 | 5  | 6  | 7  |
        | 2     | 3 | 4 | 5 | 6  | 7  | 8  |
        | 3     | 4 | 5 | 6 | 7  | 8  | 9  |
        | 4     | 5 | 6 | 7 | 8  | 9  | 10 |
        | 5     | 6 | 7 | 8 | 9  | 10 | 11 |
        | 6     | 7 | 8 | 9 | 10 | 11 | 12 |
        
        on a $\ds \P(A)=\frac 12$, $\ds \P(B)=\frac 12$, $\ds \P(C)=\frac 12$, $\ds \P(A\cap B) = \frac 14=\P(A)\P(B)$, $\ds \P(A\cap C)=\frac 14=\P(B)\P(C)$, $\ds P(B\cap C)=\frac 14=\P(B)\P(C)$ (donc les évènements sont indépendants deux à deux) et enfin $\ds P(A\cap B\cap C)=\frac 14\ne \P(A)\P(B)\P(C)$ : les évènements ne sont pas mutuellement indépendants. 


!!! propriete "Passage au contraire"
    Si $\ds A$ et $\ds B$ sont deux évènements indépendants, alors $\ds A$ et $\ds \bar B$ sont indépendants.

!!! preuve 
    Soient $\ds A$ et $\ds B$ deux évènements indépendants. Alors :
    
    $$
    \begin{aligned}
    \P(A\cap \bar B)&=\P(A\setminus (A\cap B))\\
    &=\P(A)-\P(A\cap B)\\
    &=\P(A)-\P(A)\P(B)\
    &=\P(A)(1-\P(B))\\
    &=\P(A)\P(\bar B)
    \end{aligned}
    $$

    Donc $\ds A$ et $\ds \bar B$ sont bien inépendants. 

!!! propriete "Généralisation"
    Si $\ds (A_1,\dots,A_n)$ est une famille mutuellement indépendante, soit pour tout $\ds i\in\{1,\dots,n\}$ $\ds B_i\in\{A_i,\bar{A_i}\}$, alors la famille $\ds (B_1,\dots,B_n)$ est mutuellement indépendante. 


### Variables aléatoires indépendantes

!!! definition "Variables aléatoires indépendantes"
    Deux variables aléatoires discrètes $\ds X$ et $\ds Y$ sur un même espace probabilisé $\ds (\Omega,\mathcal A,\P)$ sont *indépendantes* si et seulement si :

    $$  
    \forall (x,y)\in X(\Omega)\times Y(\Omega),\,\P((X=x)\cap(Y=y))=\P(X=x)\P(Y=y).
    $$

    On notera $\ds X\perp\!\!\!\perp Y$.

    De manière analogue, $\ds n$ variables aléatoires $\ds (X_1,\dots,X_n)$ sont *mutuellement indépendantes* si pour tout $\ds (x_1,\dots,x_n)\in X_1(\Omega)\times\cdots\times X_n(\Omega)$, on a $\ds \P(\cap_{i=1}^n (X_i=x_i))=\prod_{i=1}^n \P(X_i=x_i)$. 


!!! exemple
    === "Énoncé"
        Soit $\ds X$ et $\ds Y$ deux variables aléatoires indépendantes suivant chacun une loi de Poisson de paramètre respectif $\ds \lambda$ et $\ds \mu$. Quelle est la loi suivie par $\ds X+Y$ ?
    === "Corrigé"
        $\ds X$ et $\ds Y$ étant à valeurs dans $\ds \mathbb N$, on peut écrire $\ds P(X+Y=n)=\sum_{k=0}^n \P(X=k\cap Y=n-k)$. Or $\ds X$ et $\ds Y$ sont indépendantes donc pour tout $\ds k$ et tout $\ds n$, $\ds \P(X=k\cap Y=n-k)=\P(X=k)\P(Y=n-k)=\frac{\lambda^k}{k!}e^{-\lambda}\times\frac{\mu^{n-k}}{(n-k)!}e^{-\mu}$. On a donc :

        $$
        \P(X+Y=n)=e^{-(\lambda+\mu)}\sum_{k=0}^n\frac{\lambda^k\mu^{n-k}}{k!(n-k)!}=\frac{e^{-(\lambda+\mu)}}{n!}\sum_{k=0}^n\binom nk\lambda^k\mu^{n-k}=\frac{(\lambda+\mu)^n}{n!}e^{\lambda+\mu}.
        $$

        Ainsi $\ds X+Y$ suit une loi de Poisson de paramètre $\ds \lambda+\mu$. 


!!! propriete "Autre caractérisation"
    Une famille $\ds (X_1,\dots,X_n)$ de variables aléatoires discrètes sur un espace probabilisé $\ds (\Omega,\mathcal A,\P)$ est indépendante si et seulement si pour toutes parties $\ds A_1\subset X_1(\Omega),\dots, A_n\subset X_n(\Omega)$, on a :

    $$
    \P((X_1\in A_1)\cap\cdots\cap(X_n\in A_n))=\prod_{i=1}^n\P(X_i\in A_i).
    $$

!!! preuve
    La réciproque est triviale en prenant simplement $\ds A_i=\{x_i\}$. Traitons donc l'implication directe. 

    Soit $\ds A_i$ une partie de $\ds X_i(\Omega)$. On a la relation suivante : 

    $$
    \bigcap_{i=1}^n(X_i\in A_i) = \bigcup_{x_1\in A_1,\dots,x_n\in A_n}\bigcup_{i=1}^n(X_i=x_i)=\bigcup_{(x_1,\dots,x_n)\in A_1\times\cdots\times A_n}\bigcap_{i=1}^n(X_i=x_i)
    $$

    Or les $\ds A_i$ sont au plus dénombrables donc leur produit cartésien aussi et par $\ds \sigma$-additivité, :

    $$
    \begin{aligned}
    \P(\bigcap_{i=1}^n(X_i\in A_i))&=\P(\bigcup_{(x_1,\dots,x_n)\in A_1\times\cdots\times A_n}\bigcap_{i=1}^n(X_i=x_i))\\
    &= \sum_{(x_1,\dots,x_n)\in A_1\times\cdots\times A_n}\P(\bigcap_{i=1}^n(X_i=x_i))\\
    &= \sum_{(x_1,\dots,x_n)\in A_1\times\cdots\times A_n}\prod_{i=1}^n\P(X_i=x_i)&\text{par indépendance}\\
    &= \prod_{i=1}^n\left(P(X_i\in A_i)\right)
    \end{aligned}
    $$

    Ainsi la propriété est bien vérifiée.

On remarque qu'il n'est pas question ici de sous-familles finies. L'exemple ci-dessous explique pourquoi.

!!! exemple
    === "Énoncé"
        Soit $\ds X_1,\dots,X_n$ une suite de variables aléatoires discrètes indépendantes sur un espace probabilisé  $\ds (\Omega,\mathcal A,\P)$. Alors toute sous-famille finie est indépendante
    === "Corrigé"
        Soit $\ds I\subset \{1,\dots,n\}$, posons pour tout $\ds i\in I$ posons $\ds A_i\subset X_i(\Omega)$ et pour $\ds i\notin I$, $\ds A_i=X_i(\Omega)$. On a alors, puisque les variables aléatoires $\ds X_1,\dots,X_n$ sont indépendantes, $\ds \P(\cap_{i=1}^n (X_i\in A_i))=\prod_{i=1}^n\P(X_i\in A_i)$. Mais si $\ds i\notin I$, $\ds (X_i\in A_i)=(X_i\in X_i(\Omega))=\Omega$ et $\ds P(X_i\in A_i)=1$. Donc $\ds \P(\cap_{i=1}^n (X_i\in A_i))=\P(\cap_{i\in I} (X_i\in A_i))$ et $\ds \prod_{i=1}^n\P(X_i\in A_i)=\prod_{i\in I}\P(X_i\in A_i)$. Donc $\ds \P(\cap_{i\in I} (X_i\in A_i))=\prod_{i\in I}\P(X_i\in A_i)$.  Donc $\ds {(A_i)}_{i\in I}$ est indépendante. 

        Ainsi, on a pas besoin de la partie "pour toute sous famille finie" que l'on a dans la définition de l'indépendances des évènements.




!!! definition "Suites de variables indépendantes"
    Soit $\ds {(X_n)}_{n\in\mathbb N}$ une suite de variables aléatoires discrètes sur un espace probabilisé $\ds (\Omega,\mathcal A,\P)$. Cette suite est une suite de variables aléatoires indépendantes si et seulement si toute famille finie extraite de cette suite est mutuellement indépendante. 

    Si de plus chaque variable suit une même loi, on dira qu'il s'agit d'une suite de variables aléatoires indépendantes identiquement distribuées (noté $\ds i.i.d$).


!!! exemple
    La modélisation d'un jeu de pile ou face infini requiert l'utilisation d'une suite de variables aléatoires indépendantes identiquement distribuées suivant un loi de Bernoulli. L'étude de l'existence d'une telle suite n'est pas triviale, mais n'est pas l'objet du programme de PSI.

!!! propriete "Fonctions de variables indépendantes"
    Soit $\ds X$ et $\ds Y$ deux variables aléatoires discrètes sur un espace probabilisé $\ds (\Omega,\mathcal A,\P)$. Soit $\ds f$ une fonction définie sur $\ds X(\Omega)$ et $\ds g$ une fonction définie sur $\ds Y(\Omega)$. Alors $\ds f(X)$ et $\ds g(Y)$ sont deux variables aléatoires discrètes indépendantes. 

    De manière plus générale, si $\ds (X_1,\dots,X_n)$ sont des variables aléatoires discrètes indépendantes et $\ds f_1,\dots,f_n$ des fonctions définies sur $\ds X_1(\Omega),\dots,X_n(\Omega)$ alors les variables aléatoires discrètes $\ds (f_1(X_1),\dots,f_n(X_n))$ sont indépendantes.  

!!! preuve  
    Traitons le cas général. Soit $\ds (x_1,\dots,x_n)\in f_1(X_1(\Omega))\times\cdots\times f_n(X_n(\Omega))$. Notons que $\ds (f_k(X_k)=x_k)=(X_k\in f_k^{-1}(\{x_k\}))$. On a alors :

    $$
    \begin{aligned}
    \P(\bigcap_{i=1}^n(f_k(X_k)=x_k))&=\P(\bigcap_{i=1}^n(X_k\in f_k^{-1}(x_k)))\\
    &=\prod_{i=1}^n\P(X_k\in f_k^{-1}(x_k))&\text{par indépendance des }X_i\\
    &=\prod_{i=1}^n\P(f_k(X_k)=x_k)
    \end{aligned}
    $$ 

    Donc les $\ds f_k(X_k)$ sont indépendants.

!!! theoreme "Lemme des coalitions"
    Si les variables aléatoires $\ds X_1, \ldots, X_n$ sont indépendantes, alors $\ds f\left(X_1, \ldots, X_m\right)$ et $\ds g\left(X_{m+1}, \ldots, X_n\right)$ le sont aussi.

!!! exemple
    === "Énoncé"
        Soit $\ds X$, $\ds Y$ et $\ds Z$ des variables aléatoires mutuellement indépendantes. Montrer que $\ds X+Y$ et $\ds Z$ sont indépendantes.
    === "Corrigé"
        On prend $\ds f(u,v)=u+v$ et $\ds g(w)=w$. D'après le lemme des coalitions, $\ds f(X,Y)$ et $\ds g(Z)$ sont indépendantes, c'est à dire $\ds X+Y$ et $\ds Z$ sont indépendantes. 





## Espérance et variance

Dans toute cette partie, on travaille sur un espace probabilisé $\ds (\Omega,\mathcal A,\P)$. Les variables aléatoires sont toutes à valeurs réelles ou complexes.

### Espérance d’une variable aléatoire discrète réelle ou complexe

!!! definition "Espérance d'une variable aléatoire positive"
    Soit $\ds X$ une variable aléatoire discrète à valeurs dans $\ds [0,+\infty]$. On adopte comme convention lorsque $\ds x=+\infty$ : $\ds \P(X=x)=0$ et $\ds x\P(X=x)=0$. On définit l'espérance de $\ds X$ par :

    $$
    E(X)=\sum_{x\in X(\Omega)}x\P(X=x).
    $$

!!! definition "Espérance d'une variable aléatoire"
    Soit $\ds X$ une variable aléatoire discrète à valeurs réelles ou complexes. On dit que $\ds X$ est d'espérance finie si la famille $\ds {(x\P(X=x))}_{x\in X(\Omega)}$ est sommable.  Dans ce cas, on définit l'espérance de $\ds X$ par :

    $$
    E(X)=\sum_{x\in X(\Omega)}x\P(X=x).
    $$

    Si $\ds E(X)$ existe et vaut $\ds 0$, on dira que $\ds X$ est une *variable centrée*.

!!! propriete "Variable à valeurs entières"
    Soit $\ds X$ une variable aléatoire discrète à valeurs dans $\ds \mathbb N\cup\{+\infty\}$. Alors 

    $$
    E(X)=\sum_{n=1}^{+\infty}P(X\ge n)
    $$

!!! preuve
    Comme on manipule ici une famille à valeur positive, on peut effectuer les calculs sans se poser de questions. On a en négligeant la valeur $\ds +\infty$ d'après nos conventions :
    
    $$
    \begin{aligned}
    \sum_{n=1}^{+\infty}\P(X\ge n)&=\sum_{n=1}^{+\infty}\sum_{k\ge n}P(X=k)\\
    &=\sum_{k=1}^{+\infty}\sum_{n=1}^kP(X=k)\\
    &=\sum_{k=1}^{+\infty}kP(X=k)\\
    &=\sum_{k=0}^{+\infty}kP(X=k)
    &= E(X)
    \end{aligned}
    $$

!!! exemple
    === "Énoncé"
        Calculer l'espérance d'une variable géométrique de paramètre $\ds p$.
    === "Corrigé"
        On calcule :

        $$  
        \begin{aligned}
        E(X)&=\sum_{k=1}^{+\infty}kp(1-p)^{k-1}\\
        &=p\sum_{k=1}^{+\infty}k(1-p)^{k-1}\\
        &=p\frac 1{(1-(1-p))^2}\\
        &=p\frac 1{p^2}\\
        &=\frac 1p
        \end{aligned}
        $$

!!! exemple
    === "Énoncé"
        Calculer l'espérance d'une variable de Poisson de paramètre $\ds \lambda$.
    === "Corrigé"
        On calcule :

        $$
        \begin{aligned}
        E(X)&=\sum_{k=0}^{+\infty}k\frac{\lambda^k}{k!}e^{-\lambda}\\
        &=\lambda e^{-\lambda}\sum_{k=1}^{+\infty}\frac{\lambda^{k-1}}{(k-1)!}\\
        &=\lambda e^{-\lambda}e^{\lambda}\\
        &=\lambda.
        \end{aligned}
        $$

!!! propriete "Formule de transfert"
    Soit $\ds f$ une application définie sur $\ds X(\Omega)$. $\ds f(X)$ est d'espérance finie si et seulement si la famille $\ds (f(x) \P(X=x))_{x \in X(\Omega)}$ est sommable. Dans ce cas :

    $$
    E(f(X))=\sum_{x \in X(\Omega)} f(x) \P(X=x) .
    $$

!!! preuve
    Supposons que $\ds f(X)$ est d'espérance finie, alors par définition la famille $\ds (f(x) P(X=x))_{x \in X(\Omega)}$ est sommable et 

    $$
    \begin{aligned}
    E(f(X))&=\sum_{y\in f(X(\Omega))} y\P(f(X)=y)\\
    &=\sum_{y\in f(X(\Omega))} \sum_{x\in f^{-1}(\{y\})} y\P(X=x)\\
    &=\sum_{y\in f(X(\Omega))} \sum_{x\in f^{-1}(\{y\})} f(x)\P(X=x)\\
    &=\sum_{x\in X(\Omega)}f(x)\P(X=x)
    \end{aligned}
    $$

    en remarquant que $\ds X(\Omega)=\bigcup_{y\in f(X(\Omega))}f^{-1}(\{y\})$, l'union étant disjointe.


!!! propriete "Linéarité"
    L'espérance est linéaire, c'est à dire que si $\ds X$ et $\ds Y$ sont des variables aléatoires discrètes. On suppose que $\ds X$ et $\ds Y$ admettent une espérance. Alors $\ds \lambda X+Y$ admet une espérance et $\ds E(\lambda X+ Y)=\lambda E(X)+ E(Y)$.

!!! preuve
    Soient $\ds X$ et $\ds Y$ des variables aléatoires discrètes. Notons $\ds X(\Omega)=\{x_i,i\in I\}$ avec $\ds I$ une partie de $\ds \mathbb N$ et $\ds Y(\Omega)=\{y_j|j\in J\}$ avec $\ds J$ une partie de $\ds \mathbb N$. La famille $\ds ((Y=y_j))_{j\in J}$ est une famille complète d'évènements donc pour tout $\ds i\in I$, $\ds \P(X=x_i)=\sum_{j\in J}\P(X=x_i,Y=y_j)$. 

    Soit $\ds i\in I$, la famille $\ds (x_i\P(X=x_i,Y=y_j))_{j\in J}$ est sommable : en effet, $\ds \sum_{j\in J}\left|x_i\P(X=x_i,Y=y_j)\right|=\sum_{j\in J}|x_i|\P(X=x_i,Y=y_j)=|x_i|\P(X=x_i)$. On a alors $\ds \sum_{j\in J}x_i\P(X=x_i,Y=y_j)=x_iP(X=x_i)$. Or la famille $\ds (x_i\P(X=x_i))_{i\in I}$ est sommable car par hypothèse, $\ds X$ admet une espérance. On en déduit d'après le théorème de Fubini que :

    $$
    \sum_{(i,j)\in I\times J}x_i\P(X=x_i,Y=y_j)=\sum_{i\in I}x_i\P(X=x_i)=E(X).
    $$

    On montre de même que $\ds E(Y)=\sum_{(i,j)\in I\times J}y_j\P(X=x_i,Y=y_j)$. On a alors $\ds \lambda E(X)+E(Y)=\sum_{(i,j)\in I\times J}(\lambda x_i+y_j)\P(X=x_i,Y=y_j)$. 

    Par ailleurs, en posant $\ds f(x,y)=\lambda x+y$ et en appliquant la formule de transfert à la variable aléatoire $\ds Z=(X,Y)$ on obtient :
    
    $$
    \begin{aligned}
    E(\lambda X+Y)&=\sum_{(i,j)\in I\times J}(\lambda x_i+ y_j)\P(X=x_i,Y=y_j)\\
    &=\lambda E(X)+E(Y).
    \end{aligned}
    $$

    Donc l'espérance est bien linéaire.

!!! propriete "Comparaison"
    Soient $\ds X$ et $\ds Y$ deux variable aléatoire discrètes, $\ds X$ à valeurs complexes et $\ds Y$ à valeurs positives. Si $\ds |X|\le Y$ et si $\ds E(Y)<+\infty$ alors $\ds X$ admet une espérance finie.

!!! preuve
    On reprend les notations de la preuve précédente. Soit $\ds i\in I$, $\ds |x_i|\P(X=x_i)=\sum_{j\in J}|x_i|\P(X=x_i,Y=y_j)$ (formule des probabilités totales). Soit $\ds w\in(X=x_i)\cap(Y=y_j)$, on a $\ds |X(w)|\le Y(w)$ donc ici $\ds |x_i|\le y_j$ et on a donc $\ds |x_i|\P(X=x_i,Y=y_j)\le y_j\P(X=x_i,Y=y_j)$ (si un tel $\ds w$ n'existe pas, la probabilité est nulle et donc l'inégalité est bien respectée). Ainsi $\ds |x_i|\P(X=x_i)\le \sum_{j\in J}y_j\P(X=x_i,Y=y_j)$.  

    Or $\ds E(Y)$ existe et $\ds y$ est à termes positifs donc on fait un peu ce que l'on veut dans les calculs : 

    $$
    \begin{aligned}
    E(Y)&=\sum_{j\in J}y_j\P(Y=y_j)\\
    &=\sum_{j\in J}\sum_{i\in I}y_j\P(X=x_i,Y=y_j)\\
    &=\sum_{i\in I}\sum_{j\in J}y_j\P(X=x_i,Y=y_j).
    \end{aligned}
    $$

    On en déduit (Fubini) que la famille $\ds (\sum_{j\in J}y_j\P(X=x_i,Y=y_j))_{i}$ est sommable et donc par comparaison la famille $\ds |x_i|\P(X=x_i)$ est sommable. Donc $\ds X$ admet une espérance.

!!! propriete "Positivité de l'espérance, croissance"
    **positivité :** Soit $\ds X$ une variable aléatoire discrète à valeurs positives. Si $\ds X$ admet une espérance alors $\ds E(X)\ge 0$. 

    **croissance :** Soient $\ds X$ et $\ds Y$ deux variables aléatoires telles que $\ds X\le Y$ et admettant une espérance. Alors $\ds E(X)\le E(Y)$.

!!! preuve
    Si $\ds X$ est à valeur positive et admet une espérance, alors $\ds E(X)=\sum_{x\in X(\Omega)}x\P(X=x)\ge 0$ car pour tout $\ds x\in X(\Omega)$, $\ds x\ge 0$. 

    Si $\ds X\le Y$ alors $\ds Y-X\ge 0$. Or $\ds X$ et $\ds Y$ admettent une espérance, donc $\ds Y-X$ ausi par linéarité et $\ds E(Y-X)=E(Y)-E(X)$. Or $\ds E(Y-X)\ge 0$ donc $\ds E(X)\le E(Y)$. 


!!! exemple
    === "Énoncé"
        Soit $\ds X$ une variable aléatoire discrète à valeurs positives qui admet une espérance et telle que $\ds E(X)=0$. Montrer que $\ds P(X=0)=1$. 
    === "Corrigé"
        Notons $\ds X(\Omega)=\{x_i|i\in I\}$ avec $\ds I$ une partie de $\ds \mathbb N$. On a ici $\ds E(x)=\sum_{i\in I}x_i\P(X=x_i)$. Or les $\ds x_i\P(X=x_i)$ sont positifs et leur somme vaut $\ds 0$ donc pour tout $\ds i\in I$, $\ds x_i\P(X=x_i)=0$ et donc si $\ds x_i\ne 0$, $\ds \P(X=x_i)=0$. On en déduit que $\ds \P(X=0)=1-\sum_{i\in I,x_i\ne 0}\P(X=x_i)=1$. Donc $\ds \P(X=0)=1$. 


!!! propriete "Produit de variables indépendantes"
    Soit $\ds X$ et $\ds Y$ deux variables aléatoires discrètes à valeurs complexes et indépendantes. Alors :

    $$
    E(XY)=E(X)E(Y).
    $$

    On peut généraliser à $\ds n$ variables aléatoires indépendantes : 

    $$
    E(X_1X_2\dots X_n)=E(X_1)E(X_2)\cdots E(X_n).
    $$

!!! preuve
    On ne présente la preuve que pour deux variables.

    Notons $\ds X(\Omega)=\{x_i|i\in I\}$ et $\ds Y(\Omega)=\{y_j|j\in J\}$ avec $\ds I$ et $\ds J$ des parties de $\ds \mathbb N$. Soit $\ds f(x,y)=xy$. Montrons que la famille $\ds (x_iy_j\mathbb P(X=x_i,Y=y_j))_{(i,j)\in I\times J}$ est sommable.

    Pour cela, on va appliquer le théorème de Fubini. Soit $\ds i\in I$ et $\ds j\in J$, on a $\ds x_iy_j\P(X=x_i,Y=y_j)=x_i\P(X=x_i)y_j\P(Y=y_j)$. Or $\ds Y$ admet une espérance donc la famille $\ds (y_j\P(Y=y_j))_{j\in J}$ est sommable, donc par multiplication par un réel fixé, la famille $\ds (x_i\P(X=x_i)y_j\P(Y=y_j))_{j\in J}$ est sommable. Considérons maintenant la famille $\ds (\sum_{j\in J}x_iy_j\P(X=x_i,Y=y_j))_{i\in I}$. On a pour $\ds i$ fixé, $\ds \sum_{j\in J}x_iy_j\P(X=x_i,Y=y_j)=\sum_{j\in J}x_iy_j\P(X=x_i)\P(Y=y_j)=x_i\P(X=x_i)E(Y)$. Or la famille $\ds (x_i\P(X=x_i))_{i\in I}$ est sommable car $\ds X$ admet une espérance. Donc par multiplication par un réel $\ds E(Y)$, la famille $\ds (\sum_{j\in J}x_iy_j\P(X=x_i,Y=y_j))_{i\in I}$ est sommable. D'après le théorème de Fubini, la famille double $\ds (x_iy_j\P(X=x_i,Y=y_j))_{(i,j)\in I\times J}$ est sommable et :

    $$
    \begin{aligned}
    \sum_{(i,j)\in I\times J}x_iy_j\P(X=x_i,Y=y_j)&=\sum_{(i,j)\in I\times J}x_iy_j\P(X=x_i)\P(Y=y_j)\\
    &=\sum_{i\in I}x_i\P(X=x_i)\sum_{j\in J}y_j\P(Y=y_j)\\
    &=E(X)E(Y).
    \end{aligned}
    $$ 

    D'après le théorème de transfert, on a donc :

    $$
    E(XY)=E(f(X,Y))=\sum_{(i,j)\in I\times J}x_iy_j\P(X=x_i,Y=y_j)=E(X)E(Y).
    $$

!!! exemple
    === "Énoncé"
        Soit $\ds X$ suivant une loi géométrique de paramètre $\ds p\in]0,1[$. Justifier que $\ds \frac 1X$ est d'espérance finie et calculer cette espérance.
    === "Corrigé"
        On rappelle que $\ds X$ est à valeurs dans $\ds \mathbb N^*$ et que $\ds \P(X=k)=p(1-p)^{k-1}$. Notons $\ds f$ la fonction définie par $\ds f(x)=\frac 1x$ sur $\ds ]0,+\infty[$. On cherche donc à justifier l'existence de $\ds E(\frac 1x)$ et on utilise pour cela le théorème de transfert. On s'intéresse à la famille $\ds (\frac 1k\P(X=k))_{k\in \mathbb N^*}$ qui est à valeur positive : on peut donc faire les calculs et voir si le résultat est fini.

        $$
        \sum_{k\in\mathbb N^*}\frac 1k\P(X=k)=\sum_{k\in\mathbb N^*}\frac 1kp(1-p)^{k-1}=\frac p{1-p}\sum_{k\in\mathbb N^*}\frac{(1-p)^k}{k}=-\frac p{1-p}\ln(p)
        $$

        Puisque $\ds p\in]0,1[$ la famille est donc sommable et d'après le théorème de transfert, $\ds E(\frac 1X)=\frac p{p-1}\ln (p)$. 


### Variance d’une variable aléatoire discrète réelle, écart type et covariance

!!! propriete "espérance du carré"
    Soit $\ds X$ une variable aléatoire discrète. Si $\ds X^2$ est d'espérance finie, alors $\ds X$ est d'espérance finie. 

!!! preuve
    On a la relation classique $\ds |X|\le\frac 12(1+|X|^2)$. Or $\ds X^2$ est d'espérance finie donc par linéarité, $\ds \frac 12(1+|X|^2)$ est d'espérance finie. Par comparaison on en déduit que $\ds X$ est d'espérance finie.

!!! remarque 
    Si $\ds X^2$ admet une espérance finie, alors d'après le théorème de transfert, la famille $\ds (f(x)\P(X=x))_{x\in X(\Omega)}$ est sommable et dans ce cas on a :
    
    $$
    E(X^2)=\sum_{x\in X(\Omega)}x^2\P(X=x).
    $$


!!! propriete "Inégalité de Cauchy-Schwarz"
    Soit $\ds X$ et $\ds Y$ deux variables aléatoires réelles discrète telles que $\ds E(X^2)$ et $\ds E(Y^2)$ existent et sont finie. Alors $\ds XY$ est d'espérance finie et :

    $$
    E(XY)^2\le E(X^2)E(Y^2)
    $$

!!! preuve
    On a toujours l'inégalité classique $\ds |XY|\le \frac 12(X^2+Y^2)$. Or $\ds X^2$ et $\ds Y^2$ sont d'espérance finie donc $\ds \frac 12(X^2+Y^2)$ est d'espérance finie. On en déduit par comparaison que $\ds |XY|$ est d'espérance finie et ainsi par théorème de transfert, $\ds E(XY)$ existe et $\ds E(XY)=\sum_{(x,y)\in X(\Omega)\times Y(\omega)}xy\P(X=x,Y=y)$ (mais ça ne va pas nous intéresser ici).

    On sait donc que $\ds X^2$, $\ds Y^2$ et $\ds XY$ sont d'espérance finie. Posons $\ds \P(\lambda)=\lambda^2E(X^2)+2\lambda E(XY)+E(Y^2)=E((\lambda X+Y)^2$ (par linéarité). $\ds P$ est un polynôme en $\ds \lambda$ positif, donc son discriminant $\ds \Delta$ est négatif. Or $\ds \Delta=4E(XY)^2-4E(X^2)E(Y^2)$ donc $\ds E(XY)^2\le E(X^2)E(Y^2)$. Il y a égalité si et seulement si le discriminant est nul, ce qui donne l'existance de $\ds \lambda$ tel que $\ds \lambda X=-Y$.

!!! definition "Variance, écart type"
    Si $\ds X^2$ admet une espérance finie, on définit la *variance* de $\ds X$ par $\ds V(X)=E((X-E(X))^2)$. 
    Dans les mêmes conditions, l'*écart type* $\ds \sigma(X)$ est défini par $\ds \sigma(X)=\sqrt{V(X)}$.

    Une variable est dite *réduite* si sa variance vaut $\ds 1$.

!!! propriete "Formule de König-Huygens"
    Si $\ds X$ possède une variance, alors $\ds V(X)=E(X^2)-E(X)^2$. 

!!! preuve
    Dans ces conditions, on a $\ds V(X)=E((X-E(X))^2)=E(X^2-2XE(X)+E(X)^2)=E(X^2)^2-2E(X)^2+E(X)^2=E(X^2)-E(X)^2$. 


!!! propriete "Non linéarité de la Variance"
    Si $\ds X$ possède une variance, et si $\ds \alpha$ et $\ds \beta$ sont des réels, alors $\ds V(\alpha X+\beta)=\alpha^2V(X)$. 

!!! preuve
    On calcule simplement : 

    $$
    V(\alpha X+\beta)=E((\alpha X+ \beta-E(\alpha X+\beta))^2)=E((\alpha X-\alpha E(x))^2)=\alpha^2E((X-E(X))^2)=\alpha V(X)
    $$

!!! exemple 
    === "Énoncé"
        Montrer que si $\ds \sigma(X)>0$ alors $\ds Y=\frac {X-E(X)}{\sigma(X)}$ est une variable centrée et réduite.
    === "Corrigé"
        $\ds E(Y)=\frac 1{\sigma(X)}E(X-E(X))=0$ et $\ds V(Y)=V(\frac 1{\sigma(X)}X-\underset{\text{constante}}{\underbrace{\frac 1\sigma(X)E(X)}})=\frac 1{\sigma(X)^2}V(x)=1$. 


!!! exemple
    === "Énoncé"
        Soit $\ds X$ qui suit une loi géométrique de paramètre $\ds p\in]0,1[$. Montrer que $\ds X$ admet une espérance et calculer $\ds V(X)$.
    === "Corrigé"
        $\ds X$ suit une loi géométrique et est donc à valeurs dans $\ds \mathbb N^*$. On cherche à montrer que $\ds X$ admet une variance, donc que $\ds E(X^2)$ existe. Pour cela, d'après le théorème de transfert, il suffit de montrer que la famille $\ds (k^2\P(X=k))_{k\in\mathbb N^*}$ est sommable. Or ici, $\ds k^2\P(X=k)=k^2p(1-p)^{k-1}=o(\frac 1{k^2})$ Donc la série est absolument convergente et la famille associée est sommable. $\ds X$ admet donc une variance. 

        $$
        \begin{aligned}
        E(X^2)-E(X)^2&=\sum_{k=1}^{+\infty}k^2p(1-p)^{k-1}-\frac 1{p^2}\\
        &=p(1-p)\underset{\text{série dérivée seconde}}{\underbrace{\sum_{k=2}^{+\infty}k(k-1) (1-p)^{k-2}}}+p\underset{\text{série dérivée}}{\underbrace{\sum_{k=1}^{+\infty} k(1-p)^{k-1}}}\quad-\frac 1{p^2}\\
        &=p(1-p)\times \frac2{(1-(1-p))^3}+p\times\frac 1{(1-(1-p))^2}-\frac 1{p^2}\\
        &=\frac {2(1-p)}{p^2}+\frac 1p-\frac 1{p^2}\\
        &=\frac {1-p}{p^2}
        \end{aligned}
        $$

!!! exemple
    === "Énoncé"
        Soit $\ds X$ qui suit une loi de Poisson de paramètre $\ds \lambda$. Montrer que $\ds X$ admet une espérance et calculer $\ds V(X)$.
    === "Corrigé"
        La famille $\ds (k^2\frac{\lambda^k}{k!}e^{-\lambda})_{k\in \mathbb N}$ est sommable car la série associée est absolument convergente (Par d'Alembert par exemple). On en déduit que $\ds E(X^2)$ existe et donc $\ds V(X)$ aussi. 

        $$
        \begin{aligned}
        V(X)&=E(X^2)-E(X)^2\\
        &= \sum_{k\in \mathbb N}k^2\frac {\lambda^k}{k!}e^{-\lambda}-\lambda^2\\
        &= \lambda\sum_{k=1}^{+\infty} k\frac{\lambda^{k-1}}{(k-1)!}e^{-\lambda}-\lambda^2\\
        &= \lambda\sum_{k=2}^{+\infty} (k-1)\frac{\lambda^{k-1}}{(k-1)!}e^{-\lambda}+\lambda\sum_{k=1}^{+\infty}\frac{\lambda^{k-1}}{(k-1)!}e^{-\lambda}-\lambda^2\\
        &=\lambda^2\sum_{k=0}^{+\infty} \frac{\lambda^k}{k!}e^{-\lambda}+\lambda\sum_{k=0}^{+\infty}\frac{\lambda^k}{k!}e^{-\lambda}-\lambda^2\\
        &= \lambda^2e^{\lambda}e^{-\lambda}+\lambda e^{\lambda}e^{-\lambda}-\lambda^2\\
        &= \lambda
        \end{aligned}
        $$

!!! definition "Covariance"
    Soient $\ds X$ et $\ds Y$ deux variables aléatoires possédant des variances. On appelle *covariance de $\ds X$ et $\ds Y$* la quantité :

    $$
    Cov(X,Y)=E\left((X-E(X))(Y-E(Y))\right).
    $$

!!! exemple
    === "Énoncé"
        Montrer que la covariance est bien définie.
    === "Corrigé"
        $\ds X$ et $\ds Y$ admettent des variances donc $\ds X-E(X)$ et $\ds Y-E(Y)$ aussi et d'après l'inégalité de Cauchy-Schwarz, $\ds (X-E(X))(Y-E(Y))$ admet une espérance finie.

!!! remarque
    On a évidemment $\ds V(X)=Cov(X,X)$. 

!!! propriete 
    On a la relation suivante :

    $$
    Cov(X,Y)=E(XY)-E(X)E(Y)
    $$

    et en particulier si $\ds X$ et $\ds Y$ sont indépendantes, alors $\ds Cov(X,Y)=0$.

!!! preuve
    $\ds X$ et $\ds Y$ admettent des variance, donc une espérance. De plus d'après l'inégalité de Cauchy-Schwarz, $\ds XY$ admet aussi une espérance finie. Il s'agit ensuite simplement de calculs : 

    $$
    \begin{aligned}
    Cov(X,Y) &= E((X-E(X))(Y-E(Y)))\\
    &= E(XY-XE(Y)-YE(X)+E(X)E(Y))\\
    &= E(XY)-E(X)E(Y)-E(Y)E(X)+E(X)E(Y)\\
    &= E(XY)-E(X)E(Y)
    \end{aligned}
    $$

!!! propriete "bilinéarité de la covariance"
    Soient $\ds X$, $\ds Y$, $\ds Z$ trois variables aléatoires admettant des variances, $\ds a$ et $\ds b$ deux réels, alors $\ds Cov(aX+bY,Z)=aCov(X,Z)+bCov(Y,Z)$. On a le même résultat à droite.

!!! preuve
    Immédiate par linéarité de l'espérance.


!!! propriete "Variance d'une somme finie"
    Si $\ds (X_1,\dots,X_n)$ est une famille finie de variables aléatoires discrètes réelles admettant des variances, alors :

    $$
    V(X_1+\cdots+X_n)=\sum_{(i,j)\in\{1,\dots,n\}^2}Cov(X_i,X_j).
    $$

!!! preuve
    immédiate par bilinéarité de la covariance.


### Fonctions génératrices
Dans cette partie, on ne considère que des variables aléatoires à valeurs entières

!!! definition "Fonction génératrice"
    Soit $\ds X$ une variable aléatoire discrète à valeurs entières, on appelle *fonction génératrice de $\ds X$* la série entière :

    $$
    G_X(t)=\sum_{n=0}^{+\infty}\P(X=n)t^n.
    $$

!!! propriete "Convergence et régularité"
    Le rayon de convergence d'une fonction génératrice est supérieur ou égal à $\ds 1$. De plus la fonction génératrice est définie en $\ds 1$.

    Une fonction génératrice est $\ds \mathcal C^\infty$ sur son disque ouverte de convergence et on a en particulier $\ds \P(X=n)=\frac 1{n!}G_x^{(n)}(0)$. 

!!! preuve
    Le terme général $\ds \P(X=n)$ étant borné, le rayon de convergence est supérieur ou égal à $\ds 1$. De plus la série de terme général $\ds \mathbb P(X=n)$ est convergente, de somme $\ds 1$ donc $\ds G_X(1)$ est bien définie.

    Par propriété des séries entières, $\ds G_X$ est $\ds \mathcal C^\infty$ sur son disque ouvert de convergence et par la formule de Taylor, on trouve ici $\ds \P(X=n)=\frac 1{n!}G_X^{(n)}(0)$. 

!!! remarque 
    Cette propriété permet d'affirmer qu'une variable aléatoire à valeurs entière est entièrement déterminée par sa série génératrice.

!!! propriete "Espérance et fonction génératrice"
    La variable aléatoire $\ds X$ est d'espérance finie si et seulement si $\ds G_X$ est dérivable en 1 ; dans ce cas $\ds \mathrm{E}(X)=G_X{ }^{\prime}(1)$

!!! preuve   
    Seul le sens direct est exigible. 
    
    On suppose $\ds X$ d'espérance finie. Dans ce contexte, $\ds E(X)=\sum_{k=0}^{+\infty}n\P(X=n)$. Ainsi, le segment $\ds [0,1]$ est inclu dans le disque de convergence et pour une série entière, il y a convergence normale sur tout segment du disque de convergence. On en déduit que la série entière $\ds \sum n\P(X=n)t^n$ est normalement convergente sur $\ds [0,1]$. Posons $\ds f_n(t)=\P(X=n)t^n$. La série des $\ds f_n$ est $\ds \mathcal C^1$ et converge simplement sur $\ds [0,1]$. La série des $\ds f_n'$ converge normalement donc uniformément sur $\ds [0,1]$. on en déduit que $\ds \sum f_n$ est $\ds \mathcal C^1$ sur $\ds [0,1]$ et sa dérivée est $\ds \sum f_n'$. Traduction ici : $\ds G_X$ est $\ds \mathcal C^1$ sur $\ds [0,1]$ et $\ds G_X$ est donc dérivable en $\ds 1$ à gauche et sa dérivée vaut $\ds G_x'(1)=\sum_{n=0}^{+\infty} n\mathbb P(X=n)=E(X)$. 


!!! remarque   
    D'après la formule de transfert, $\ds G''_X(1)=\sum_{n=2}^{+\infty}n(n-1)\P(X=n)=E(X(X-1))$. On en déduit donc que $\ds E(X(X-1))=G''_X(1)$ et donc $\ds V(X)=E(X^2)-E(X)^2=G''_X(1)+G_X'(1)-G_X'(1)^2$. 

!!! exemple 
    === "Énoncé"
        Soient $\ds p \in] 0 ; 1[$ et $\ds X$ une variable aléatoire à valeurs naturelles dont la loi est donnée par
        
        $$
        \mathrm{P}(X=k)=a(k+1) p^k \quad \text { pour tout } k \in \mathbb{N} .
        $$
        
        En employant la fonction génératrice de $\ds X$, déterminer $\ds a$ et calculer l'espérance et la variance de $\ds X$.
    === "Corrigé"
        La fonction génératrice de $\ds X$ est $\ds G_X(t)=\sum_{k=0}^{+\infty} a(k+1)p^kt^k=a\sum_{k=0}^{+\infty}(k+1)(pt)^k=\frac {a}{(1-pt)^2}$. On doit avoir $\ds G_X(1)=1$ c'est à dire $\ds \frac a{(1-p)^2}=1$ ou encore $\ds a=(1-p)^2$. 

        Pour calculer l'espérance on utilise la relation $\ds E(X)=G_X'(1)=\frac{2ap}{(1-p)^3}=\frac{2p}{1-p}$ et $\ds V(X)=G_X''(1)+G'_X(1)-G'_X(1)^2=\frac{2p}{(1-p)^2}$. 

!!! propriete "Fonction génératrice d'une somme"
    Soient $\ds X$ et $\ds Y$ deux variables aléatoire discrètes indépendantes à valeurs dans $\ds \mathbb N$, Alors $\ds G_{X+Y}=G_XG_Y$ quand c'est possible.

!!! preuve
    On remarque que $\ds G_X(t)=E(t^X)$ et d'après le lemme des coalitions, $\ds X$ et $\ds Y$ étant indépendantes, $\ds t^X$ et $\ds t^Y$ aussi. Soit $\ds t$ tel que $\ds G_X(t)$ et $\ds G_Y(t)$ soient bien définies : $\ds t^X$ et $\ds t^Y$ sont d'espérance finie et donc leur produit aussi. On a alors $\ds G_X(t)G_Y(t)=E(t^X)E(t^Y)=E(t^X\times t^Y)=E(t^{X+Y})=G_{X+Y}(t)$. 

 
!!! exemple
    === "Énoncé"
        Déterminer la fonction génératrice d'une variable aléatoire suivant une loi de Bernoulli.
    === "Corrigé"
        On prend une variable aléatoire $\ds X$ suivant une loi de Bernoulli : $\ds X(\Omega)=\{0,1\}$ et $\ds \P(X=1)=p$ et $\ds \P(X=0)=1-p$. On a alors $\ds G_X(t)=\sum_{k=0}^{+\infty}\P(X=n)t^n=\P(X=0)+\P(X=1)t=(1-p)+pt$. 

!!! exemple
    === "Énoncé"
        Déterminer la fonction génératrice d'une variable aléatoire suivant une loi binômiale.
    === "Corrigé"
        Soit $\ds X$ une variable aléatoire qui suit une loi binomiale de paramètre $\ds n$ et $\ds p$. Alors $\ds X$ est une somme de $\ds n$ variables aléatoires indépendantes suivant une loi de Bernoulli de paramètre $\ds p$. D'après la propriété liée à la fonction génératrice d'une somme indépendante, on a : $\ds G_X(t)=((1-p)+pt)^n$.

!!! exemple
    === "Énoncé"
        Déterminer la fonction génératrice d'une variable aléatoire suivant une loi géométrique.
    === "Corrigé"
        Soit $\ds X$ une variable aléatoire suivant une loi géométrique de paramètre $\ds p$. Alors $\ds G_X(t)=\sum_{k\in\mathbb N^*}p(1-p)^{k-1}t^k=pt\sum_{k\in\mathbb N}(1-p)^{k-1}t^{k-1}$ qui converge si $\ds t<\frac{1}{1-p}$ vers $\ds G_X(t)=\frac{pt}{1-(1-p)t}$. 

!!! exemple
    === "Énoncé"
        Déterminer la fonction génératrice d'une variable aléatoire suivant une loi de Poisson.
    === "Corrigé"
        Soit $\ds X$ une variable aléatoire suivant une loi de Poisson de paramètre $\ds \lambda$. Alors $\ds G_X(t)=\sum_{k\in\mathbb N}\frac{\lambda^k}{k!}e^{-\lambda}t^k=\sum_{k=0}^{+\infty}\frac 1{k!}(\lambda t)^ke^{-\lambda}=e^{\lambda t}e^{-\lambda}=e^{\lambda(t-1)}$. 



### Inégalités probabilistes

!!! propriete "Inégalité de Markov"
    Soit $\ds X$ une variable aléatoire discrète réelle à valeurs positive et qui admet une espérance. Alors, pour tout réel $\ds a>0$, on a :
    $$
    \mathbb{P}(X \geqslant a) \leqslant \frac{{E}(X)}{a} .
    $$

!!! preuve
    C'est la même inégalité qu'en première année. Et le même genre de démonstration.

    Soit $\ds X$ donc une variable aléatoire discrète réelle à valeurs positives. On node $\ds I = X(\Omega)\cap [0,a[$ et $\ds J= X(\Omega)\cap [a,+\infty[$. $\ds X$ admet une espérance, donc la famille $\ds (x\P(X=x))_{x\in X(\Omega)}$ est sommable et on peut sommer par paquets (tout est positif, donc c'est facile). On a donc : 
    
    $$
    E(X)=\underset{\ge 0}{\underbrace{\sum_{x\in I}x\P(X=x)}}+\sum_{x\in J}\underset{\ge a}{\underbrace{x}}\P(X=x)\ge 0+\sum_{x\in J}a\P(X=x)=a\P(x\ge a). 
    $$

    On en déduit l'inégalité de Markov puisque $\ds a>0$. 

!!! exemple
    === "Énoncé"
        Soit $\ds n \geq 1$ un entier et $\ds X$ une variable aléatoire suivant la loi géométrique $\ds \mathcal{G}(\frac 1n)$. Montrer que $\ds P\left(X \geq n^2\right) \leq \frac{1}{n}$.
    === "Corrigé"
        On applique l'inégalité de Markov. On rappelle que $\ds E(X)=n$. On a donc $\ds \P(X\ge n^2)\le\frac {n}{n^2}=\frac 1n$. 

!!! propriete "Inégalité de Bienaymé-Tchebychev"
    Soit $\ds X$ une variable aléatoire réelle discrète admettant telle que $\ds X^2$ admet une espérance. Alors, pour tout réel $\ds \varepsilon>0$
   
    $$
    \mathbb{P}(|X-{E}(X)| \geqslant \varepsilon) \leqslant \frac{\mathbb{V}(X)}{\varepsilon^2} .
    $$

!!! preuve
    On applique l'inégalité de Markov à la variable aléatoire $\ds Z=|X-E(X)|^2$. En effet celle-ci est telle $\ds E(Z)=V(X)$ et $\ds Z\ge0$. On a alors, pour $\ds \varepsilon>0$, d'après l'inégalité de Markov, 

    $$
    \P(Z\ge \varepsilon^2)\le \frac{E(Z)}{\varepsilon^2}=\frac {V(X)}{\varepsilon^2}
    $$

    Or évidemment $\ds \{Z\ge\varepsilon^2\}=\{|X-E(X)|\ge \varepsilon\}$. Donc $\ds \P(|X-E(X)|\ge\varepsilon)\le \frac {V(X)}{\varepsilon^2}$. 

!!! remarque 
    L'inégalite de Bienaymé-Tchebychev permet d'évaluer la probabilité de s'éloigner de la moyenne.

!!! exemple
    === "Énoncé"
        Soit $\ds X$ une variable aléatoire vérifiant $\ds E(X)=0$ et $\ds V(X)=2$. Déterminer $\ds \alpha>0$ tel que $\ds \P(|X|\ge \alpha)\le \frac 34$. 
    === "Corrigé"
        D'après l'inégalité de Bienaymé-Tchebychev, $\ds \P(|X|\ge\alpha)\le \frac{V(X)}{\alpha^2}=\frac 2{\alpha^2}$. Il suffit donc de prendre $\ds \alpha$ tel que $\ds \frac 2{\alpha^2}\le\frac 34$ donc $\ds \alpha\ge \frac{2\sqrt2}{\sqrt3}$. 



!!! exemple
    === "Énoncé"
        On lance autant de fois que l'on veut une pièce équilibré. Combien de lancers sont-ils nécessaires pour assurer avec une marge d'erreur inférieure à 5% que la proportion de piles obtenus est égale à $\ds \frac 12$ à $\ds 10^{-2}$ près ?
    === "Corrigé"
        On identifie une situation dans laquelle on peut utiliser l'inégalité de Bienaymé-Tchebychev. Soit $\ds P_n$ le nombre de «pile» obtenus après $\ds n$ lancers : $\ds P_n$ suit une loi binomiale de paramètres $\ds \frac 12$ et $\ds n$. On cherche donc $\ds \P(|P_n-\frac n2|\ge n\times 10^{-1})\le 0.05$. Or d'après l'inégalité de Bienaymé-Tchebychev, $\ds \P(|P_n-\frac n2|\ge n\times 10^{-1})\le \frac{V(X)}{(n\times 10^{-2})^2}$. Or ici $\ds V(X)=np(1-p)\le \frac n4$ donc il suffit de prendre $\ds n$ tel que $\ds \frac{n}{4n^210^{-4}}\le0.05$, c'est à dire $\ds n\ge 50000$. 


!!! propriete "Loi faible des grands nombres"
    si $\ds \left(X_n\right)_{n \geqslant 1}$ est une suite i.i.d. de variables aléatoires de variance finie, alors en notant $\ds S_n=\sum_{k=1}^n X_k$ et $\ds m=\mathrm{E}\left(X_1\right)$, pour tout $\ds \varepsilon>0$ :
    
    $$
    P\left(\left|\frac{S_n}{n}-m\right| \geqslant \varepsilon\right) \underset{n \rightarrow+\infty}{\longrightarrow} 0 .
    $$


!!! preuve
    On pose $\ds X=\frac{S_n}{n}$. On a alors $\ds E(X)=\frac 1n\sum_{k=1}^{n}E(X_i)$ et les variables étant identiquement distribuées, pour tout $\ds i$, $\ds E(X_i)=E(X_1)$. On obtient donc $\ds E(X)=E(X_1)=m$. Par ailleurs, les variables étant indépendantes $\ds V(X)=V(\frac 1n(X_1+\cdots+X_n))=\frac 1{n^2}(V(X_1)+\cdots+V(X_n))=\frac 1nV(X_1)$. 

    On applique alors l'inégalité de Bienaymé-Tchebychev à $\ds X$ : $\ds \P(|X-E(X)|\ge \varepsilon)\le \frac {V(X)}{\varepsilon^2}=\frac{V(X_1)}{n\varepsilon^2}$.

    On a donc montré que $\ds P\left(\left|\frac {S_n}{n}-m\right|\ge\varepsilon\right)\le \frac{\sigma^2}{n\varepsilon^2}\xrightarrow[n\to+\infty]{} 0$. 





### Issus de la banque CCINP
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-096.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-097.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-099.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-100.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-101.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-102.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-103.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-106.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-108.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-110.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-111.md" %}


### Annales d'oraux

1474,1476,1482,1484?

{% include-markdown "../../../exercicesRMS/2022/RMS2022-747.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-748.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-749.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-751.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-752.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-753.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-754.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-756.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-757.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-758.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-759.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-760.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-761.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-762.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1392.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1474.md" %}




