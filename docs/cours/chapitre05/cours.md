# Compléments d'algèbre linéaire

Dans tout le chapitre on suppose maîtrisées les notions au programme de première année. La notation $\ds \mathbb K$ désigne soit $\ds \mathbb R$, soit $\ds \mathbb C$.


## Produits et sommes d'espaces vectoriels

!!! rappel "Produit Cartésien"
    On rappelle la définition du *produit cartésien* : Si $\ds E_1,\dots,E_p$ sont $\ds p$ensembles quelconques, $\ds E_1\times E_2\times \cdots\times E_p$ désigne l'ensemble des $\ds p$-uplets $\ds (x_1,\dots,x_p)$ tels que $\ds x_1\in E_1,\ x_2\in E_2,\dots,\ x_p\in E_p$. 

!!! definition "Produit d'un nombre fini d'espaces vectoriels"
    Soient $\ds ((F_1,+_1,\cdot_1),\dots,(F_p,+_p,\cdot_p))$ une famille d'espaces vectoriels sur un même corps $\ds \mathbb K$. Alors $\ds F_1\times\cdots\times F_p$ est un $\ds \mathbb K$-espace vectoriel lorsqu'il est associé aux opérations suivantes : 

    - **addition interne $\ds +$** : Si $\ds (x,y)\in(F_1\times\cdots\times F_p)^2$, on pose $\ds x=(x_1,x_2,\dots,x_p)$ et $\ds y=(y_1,y_2,\dots,y_p)$, alors $\ds x+y=(x_1+_1y_1,\dots,x_p+_py_p)$. L'élément neutre est $\ds (0_1,0_2,\dots,0_p)$, où $\ds 0_i$ est le neutre de $\ds F_i$.
    
    - **multiplication externe $\ds \cdot$** : Si $\ds x\in F_1\times F_2\times \cdots\times F_p$ et si $\ds \lambda\in\mathbb K$, en notant $\ds x=(x_1,\dots,x_p)$, $\ds \lambda\cdot x=(\lambda \cdot_1x_1,\dots,\lambda \cdot_px_p)$.  

!!! exemple
    L'exemple principal que nous manipulerons cette année est $\ds \mathbb R^n$, ou encore $\ds \mathbb C^n$. 

!!! propriete "Dimension d'un produit"
    Dans le cas où $\ds F_1$, $\ds \dots$, $\ds F_p$ sont des $\ds \mathbb K$-espaces vectoriels de dimension finie respectives $\ds n_1,n_2,\dots,n_p$, alors $\ds F_1\times\cdots\times F_p$ est un $\ds \mathbb K$-espace vectoriel de dimension finie $\ds n_1+\dots+n_p$. 

!!! preuve
    Notons $\ds (e_1^{(i)},\dots,e_{n_i}^{(i)})$ une base de $\ds F_i$. Pour plus de facilités, notons $\ds F=F_1\times\cdots\times F_p$ et lorsqu'on aura un élément $\ds x\in F$ on utilisera sans justification la décomposition de $\ds x$ sous la forme $\ds x=(x_1,\dots,x_p)$. 

    Soit $\ds x\in F$, $\ds x_i\in F_i$ donc on peut décomposer $\ds x_i$ dans la base $\ds (e_1^{(i)},\dots,e_{n_i}^{(i)})$ : $\ds x_i=\sum_{k_i=1}^{n_i}\lambda^{(i)}_{k_i}e^{(i)}_{k_i}$. On a alors :


    $$
    \begin{aligned}
    x&=(x_1,\dots,x_n)\\
    &=(x_1,0,0,\cdots,0)+(0,x_2,0,\dots,0)+\cdots+(0,0,\dots,0,x_n)\\
    &=\sum_{i=1}^{p}(0,\dots,0,\underset{\ds\overset{\uparrow}{i}}{x_i},0,\dots,0)\\
    &=\sum_{i=1}^{p}\sum_{k_i=1}^{n_i}\lambda_{k_i}^{(i)}(0,\dots,0,\underset{\ds\overset{\uparrow}{i}}{e_{k_i}^{(i)}},0,\dots,0)
    \end{aligned}
    $$

    dont on déduit que la famille des $\ds (0,\dots,0,\underset{\ds\overset{\uparrow}{i}}{e_{k_i}^{(i)}},0,\dots,0)$ pour $\ds i\in\{1,\dots,p\}$ et $\ds k_i\in\{1,\dots,n_i\}$ est génératrice de $\ds F_1\times\cdots\times F_p$. 

    Montrons qu'elle est libre. Soient des coefficients de $\ds \mathbb K$ $\ds {\left(\lambda_{k_i}^{(i)}\right)}_{\substack{i\in\{1,\dots,p\}\\k_i\in\{1,\dots,n_i\}}}$ tels que $\ds \sum_{i=1}^{p}\sum_{k_i=1}^{n_i}\lambda_{k_i}^{(i)}(0,\dots,0,\underset{\ds\overset{\uparrow}{i}}{e_{k_i}^{(i)}},0,\dots,0)=(0,0,\dots,0)$. On a :

    $$
    \sum_{i=1}^{p}\sum_{k_i=1}^{n_i}\lambda_{k_i}^{(i)}(0,\dots,0,\underset{\ds\overset{\uparrow}{i}}{e_{k_i}^{(i)}},0,\dots,0)=(\sum_{k_1=1}^{n_1}\lambda_{k_1}^{(1)}e_{k_1}^{(1)},\dots,\sum_{k_p=1}^{n_p}\lambda_{k_p}^{(p)}e_{k_p}^{(p)})
    $$

    L'égalité à $\ds (0,\dots,0)$ implique que pour tout $\ds i\in\{1,\dots,p\}$, $\ds \sum_{k_i=1}^{n_i}\lambda_{k_i}^{(i)}e_{k_i}^{(i)}=0$. Or $\ds (e_1^{(i)},\dots,e_{n_i}^{(i)})$ est une base de $\ds F_i$ donc $\ds \lambda_1^{(i)}=\dots=\lambda_{n_i}^{(i)}=0$. On en déduit que tous les $\ds \lambda_k^{(i)}$ sont nuls et donc que la famille est bien libre.

    On a ainsi trouvé une base de $\ds F_1\times\cdots\times F_p$ qui contient $\ds n_1+\cdots+n_p$ vecteurs : c'est la dimension de $\ds F$.  


!!! definition "Somme d'un nombre fini de sous-espaces vectoriels"
    Soit $\ds E$ un espace vectoriel sur un corps $\ds \mathbb K$. Soit $\ds (F_1,F_2,\dots,F_p)$ une famille de sous-espaces vectoriels de $\ds E$. On note $\ds F_1+F_2+\cdots+F_p=\{x_1+x_2+\cdots+x_p\big|\forall i\in\{1,\dots,p\},\,x_i\in F_i\}$ la *somme* des $\ds F_i$.

    On dit que la somme est directe si et seulement si pour tout $\ds (x_1,\dots,x_p)\in F_1\times\cdots\times F_p$ et $\ds (y_1,\dots,y_p)\in F_1\times\cdots\times F_p$, $\ds x_1+\cdots+x_p=y_1+\cdots y_p$ implique $\ds x_1=y_1$ et $\ds x_2=y_2$ et $\ds \dots$ $\ds x_p=y_p$. En français : toute décomposition dans $\ds F_1+\cdots+F_p$ est unique. On notera cette somme $\ds F_1\oplus F_2\oplus\cdots\oplus F_p$ ou encore $\ds \bigoplus_{k=1}^{p}F_k$.

!!! exemple
    === "Énoncé"
        Soit $\ds E$ un espace vectoriel de dimension finie. Soit $\ds e_1,\dots,e_n$ une base de $\ds E$. Soit $\ds p\in\{1,\dots,n\}$, on pose $\ds F_1=Vect(e_1,\dots,e_p)$ et $\ds F_2=Vect(e_{p+1},\dots,e_n)$. Montrer que $\ds E=F_1\oplus F_2$. 
    === "Corrigé"
        On doit montrer plusieurs choses : Le fait que tout élément de $\ds E$ se décompose en un élément de $\ds F_1$ plus un élément de $\ds F_2$, puis que la décomposition est bien unique. Soit donc $\ds x\in E$, $\ds (e_1,\dots,e_n)$ est une base de $\ds E$ donc elle est en particulier génératrice. Il existe donc des scalaires $\ds \lambda_1,\dots,\lambda_n$ tels que $\ds x=\lambda_1e_1+\cdots+\lambda_ne_n=\underset{\in F_1}{\underbrace{\lambda_1e_1+\dots+\lambda_pe_p}}+\underset{\in F_2}{\underbrace{\lambda_{p+1}e_{p+1}+\dots+\lambda_ne_n}}\in F_1+F_2$. Ainsi, on a montré que $\ds E\subset F_1+F_2$. Par définition on sait que $\ds F_1+F_2\subset E$. Donc $\ds E=F_1+F_2$. 

        Montrons que la décomposition est unique. Supposons que $\ds x=x_1+x_2=x_1'+x_2'$ avec $\ds (x_1,x_1')\in F_1^2$ et $\ds (x_2,x_2')\in F_2^2$. Posons $\ds x_1=\lambda_1 e_1+\dots+\lambda_p e_p$, $\ds x_2=\lambda_{p+1}e_{p+1}+\dots+\lambda_n e_n$, $\ds x_1'=\lambda_1' e_1+\dots+\lambda_p' e_p$, $\ds x_2=\lambda_{p+1}'e_{p+1}+\dots+\lambda_n' e_n$. L'égalité $\ds x_1+x_2=x_1'+x_2'$ permet d'écrire :

        $$
        (\lambda_1-\lambda_1')e_1+\dots+(\lambda_p-\lambda_p')e_p+(\lambda_{p+1}-\lambda_{p+1}')e_{p+1}+\dots+(\lambda_n-\lambda_n')e_n=0
        $$ 

        Or la famille $\ds (e_1,\dots,e_n)$ est libre donc $\ds \lambda_1=\lambda_1',\lambda_2=\lambda_2',\dots,\lambda_n=\lambda_n'$ et ainsi $\ds x_1=x_1'$ et $\ds x_2=x_2'$ : la décomposition est bien unique. 

!!! warning "Attention"
    Cet exemple que nous avons traité avec une partition de la base en 2 peut se s'appliquer à un nombre quelconque, mais fini, de parties.

!!! propriete "Dimension d'une somme"
    Soit $\ds E$ un $\ds \mathbb K$-espace vectoriel, soient $\ds F_1,\dots,F_p$ $\ds p$ sous-espaces vectoriels de $\ds E$ de dimension finie. Alors :
    
    $$
    \dim(F_1+\dots+F_p)\le \sum_{i=1}^p\dim(F_i).
    $$

    avec égalité si et seulement si les sev sont supplémentaires. 


!!! preuve

    Considérons l'application *linéaire* $\ds \varphi$ définie de $\ds F_1\times\cdots\times F_p$ dans $\ds F_1+\cdots+F_p$ par $\ds \varphi((x_1,\dots,x_p))=x_1+\dots+x_p$. On remarque assez facilement que $\varphi$ est surjective. On a donc, d'après le théorème du rang, $\ds\dim(F_1\times F_2\times\cdots\times F_p)=\dim(F_1+\dots F_p)+\dim(\ker\varphi)$. On en déduit que $\ds \dim(F_1+\dots+F_p)\le\dim(F_1\times\cdots\times F_p)=\dim(F_1)+\cdots+\dim(F_p)$. 
    
    Montrons que les espaces sont en somme directe si et seulement si $\ds \varphi$ est injective. 

    Supposons $\ds \varphi$ injective. Soit $\ds x\in F_1+\dots+F_p$ admettant deux décompositions : $\ds x=x_1+\cdots+x_p=x_1'+\cdots+x_p'$. On a alors $\ds \varphi((x_1,\dots,x_p))=\varphi((x_1',\dots,x_p'))$. Or $\ds \varphi$ est injective donc $\ds x_1=x_1',x_2=x_2',\dots,x_p=x_p'$ et la décomposition est donc unique : les espaces sont en somme directe.

    Supposons maintenant les espaces en somme directe. Soit $\ds (x_1,\dots,x_p)$ tel que $\ds \varphi((x_1,\dots,x_p))=0$ (on utilise ici la caractérisation de l'injectivité des applications linéaires). On a alors $\ds x_1+\dots+x_p=0$, or $\ds 0\in F_1\oplus\dots\oplus F_p$ et sa seule décomposition est $\ds 0+0+\cdots+0$. On en déduit que pour tout $\ds i$, $\ds x_i=0$ et donc $\ds \varphi$ est injective. 


    Appliquons maintenant le théorème du rang à $\ds \varphi$ : $\ds \dim(F_1\times\cdots\times F_p)=\dim(F_1+\cdots+F_p)+\dim(\ker \varphi)$. Or $\ds \dim(F_1\times\cdots\times F_p)=\dim(F_1)+\cdots+\dim(F_p)$. D'où $\ds \dim(F_1+\cdots+F_p)=\dim(F_1)+\cdots+\dim(F_p)-\dim(\ker\varphi)$.

    On a ainsi l'inégalité voulue, avec égalité si et seulement si $\ds \ker\varphi=0$ c'est à dire ssi $\ds \varphi$ est injective, ou encore ssi les $\ds F_i$ sont en somme directe. 

!!! warning "Attention"
    On n'a pas ici de caractérisation simple de la somme directe par les noyaux comme dans le cas de deux sous-espaces vectoriels.

!!! exemple
    === "Enoncé"
        Soit $\ds E$ un espace vectoriel et $\ds (e_1,e_2)$ une famille libre de $\ds E$. On pose $\ds F_1=vect(e_1)$, $\ds F_2=vect(e_2)$ et $\ds F_3=vect(e_1+e_2)$. Montrer qe $\ds F_1$, $\ds F_2$ et $\ds F_3$ ne sont pas en somme directe. Montrer que $\ds F_1\cap F_2=F_1\cap F_3=F_2\cap F_3=\{0\}$.
    === "Corrigé"
        On peut remarque que le vecteur $\ds e_1+e_2$ admet plusieurs décompositions : $\ds e_1+e_2=x+y+z$ avec $\ds x=e_1\in F_1$, $\ds y=e_2\in F_2$ et $\ds z=0\in F_3$ ou bien $\ds e_1+e_2=x'+y'+z'$. Donc les sev ne sont pas en somme directe. 
        Soit maintenant $\ds x\in E_1\cap E_2$, alors $\ds x=\lambda e_1$ et $\ds x=\mu e_2$. On a donc 0=x-x=\lambda e_1-\mu e_2$. Or $\ds (e_1,e_2)$ est une famille libre donc $\ds \lambda=\mu=0$ : $\ds x=0$. Soit $\ds y\in E_1\cap E_3$ alors $\ds y=\lambda e_1$ et $\ds y=\mu(e_1+e_2)$. Dès lors $\ds 0=\lambda e_1-\mu(e_1+e_2)=(\lambda - \mu)e_1-\mu e_2$. La famille $\ds (e_1,e_2)$ étant (toujours) libre, $\ds \lambda-\mu=0$ et $\ds \mu=0$ c'est à dire $\ds \lambda=\mu=0$ et $\ds y=0$. On montre de même que $\ds E_2\cap E_3=\{0\}$.  


!!! propriete "Caractérisation de la somme directe"
    Soient $\ds F_1,\dots,F_p$ des sous-espaces propres d'un espace vectoriel $\ds E$. Les $\ds F_i$ sont en somme directe ssi $\ds \forall (x_1,\dots,x_p)\in F_1\times \cdots\times F_p$, $\ds \sum_{i=1}^p x_i=0\Rightarrow \forall i\in\{1,\dots,p\},\,x_i=0$.

!!! preuve
    Supposons les sev en somme directe. Soit $\ds x_1\in F_1, \dots,x_p\in F_p$, tels que $\ds x_1+\cdots+x_p=0$. Alors $\ds x_1+\cdots+x_p\in F_1\oplus F_2\oplus\cdots\oplus F_p$ et évidemment $\ds 0\in F_1\oplus F_2\oplus\cdots\oplus F_p$ et par unicité de la décomposition, $\ds x_1=0,x_2=0,\dots,x_p=0$. 

    Réciproquement, soit deux décompositions $\ds x_1+\cdots+x_p=y_1+\cdots+y_p$ alors $\ds (x_1-y_1)+\cdots+(x_p-y_p)=0$. Or $\ds (x_i-y_i)\in F_i$ donc pour tout $\ds i\in\{1,\dots,p\}$, $\ds x_i=y_i$ et la somme est bien directe.

!!! definition "Base adaptée"
    Soient $\ds F_1$, $\ds F_2$, $\ds \dots$, $\ds F_p$ $\ds p$ sous-espaces vectoriel de dimension finie  en somme directe d'un espace vectoriel $\ds E$. Posons $\ds F=F_1\oplus F_2\oplus\cdots\oplus F_p$. Une base $\ds (f_1,\dots,f_n)$ de $\ds F$ est dite *adaptée à la décomposition $\ds F=F_1\oplus\cdots\oplus F_p$* lorsqu'il existe $\ds i_0=0\le i_1\le \dots\le i_{p-1}\le n=i_{p}$ tels que pour tout $\ds i$, $\ds (f_{i_{k-1}+1},\dots,f_{i_k})$ est une base de $\ds F_i$. 


!!! propriete "Existence d'une base adaptée"
    Soit $\ds E$ un espace vectoriel de dimension finie et $\ds F_1,\dots,F_p$ $\ds p$ sev de $\ds E$ tels que $\ds E=\bigoplus_{i=1}^pF_i$. On note $\ds n_i$ la dimension du sev $\ds F_i$ et on pose $\ds (e_1^{(i)},\dots,e_{n_i}^{(i)})$ une base de $\ds F_i$. Alors la famille $\ds (e_1^{(1)},\dots,e_{n_1}^{(1)},e_1^{(2)},\dots,\dots,e_{n_p}^{(p)})$ est une base de $\ds E$. 

!!! preuve
    On suppose construites les différentes bases. La famille $\ds (e_1^{(1)},\dots,e_{n_1}^{(1)},e_1^{(2)},\dots,\dots,e_{n_p}^{(p)})$ contient $\ds n_1+\cdots+n_p$ éléments. Or $\ds E=\bigoplus F_i$ donc $\ds n_1+\cdots+n_p$ d'après le cas d'égalité de la dimension d'une somme. Si on montre qu'elle est soit libre, soit génératrice, on aura donc une base.

    Soit $\ds x\in E$, alors on peut écrire $\ds x=x_1+\cdots+x_p$ avec pour tout $\ds i$, $\ds x_i\in F_i$. On peut alors décomposer les $\ds x_i$ dans les bases respectives : pour $\ds i\in\{1,\dots,p\}$, $\ds x_i=\lambda_1^{(i)}e_1^{(i)}+\dots+\lambda_{n_i}^{(i)}e_{n_i}^{(i)}$. On a alors :

    $$
    x=\sum_{i=1}^{p}\sum_{k=1}^{n_i}\lambda_k^{(i)}e_k^{(i)}
    $$ 

    et la famille $\ds (e_1^{(1)},\dots,e_{n_1}^{(1)},e_1^{(2)},\dots,\dots,e_{n_p}^{(p)})$ est bien génératrice : c'est un base.

!!! exemple
    === "Énoncé"
        Soit $\ds E$ de dimension finie, soit $\ds F$ et $\ds G$ deux sous-espaces vectoriels de $\ds E$ tels que $\ds E=F\oplus G$ et soit $\ds p$ le projecteur sur $\ds F$ parallèlement à $\ds G$. Montrer qu'il existe une base de $\ds E$ dans laquelle la matrice de $\ds p$ est de la forme $\ds diag(1,\dots,1,0,\dots,0)$.
    === "Corrigé"
        Prenons une base $\ds (e_1,\dots,e_p,e_{p+1},\dots,e_n)$ adaptée à la décomposition $\ds E=F\oplus G$. Si $\ds x\in F$, par définition d'un projecteur, $\ds p(x)=x$ et si $\ds x\in G$, $\ds p(x)=0$. On a donc pour tout $\ds 1\le i\le p$, $\ds p(e_i)=e_i$ et pour $\ds p+1\le j\le n$, $\ds p(e_j)=0$. Ainsi dans cette base, la matrice de $\ds p$ est bien de la forme souhaitée.


On sait qu'une application linéaire est entièrement déterminée par l'image d'une base. On peut énoncer la la propriété similaire suivante.

!!! propriete "Détermination d'une application linéaire"
    Soit $\ds E$ un espace vectoriel et $\ds F_1,\dots,F_p$ des sev de $\ds E$ tels que $\ds E=F_1\times \cdots\times F_p$. Soient, pour tout $\ds i\in\{1,\dots,p\}$, $\ds u_i$ une application linéaire de $\ds F_i$ dans $\ds E$. Il existe un unique endomorphisme $\ds u$ de $\ds E$ tel que pour tout $\ds i\in\{1,\dots,p\}$, $\ds u_{\big|F_i}=u_i$. Autrement dit, un endomorphisme de $\ds E$ est entièrement déterminé par ses restrictions aux sev $\ds F_i$. 

!!! preuve
    Pour la démonstration, notons $\ds p_i$ le projecteur sur $\ds F_i$ parallèlement à $\ds \bigoplus_{k\ne i}F_k$. Si $\ds x$ se décompose dans $\ds F_1\oplus \cdots\oplus F_p$ en $\ds x=x_1+\cdots+x_p$, alors $\ds x_i=p_i(x)$.
    
    Montrons l'unicité : soit $\ds u$ tel que pour tout $\ds i$, $\ds u_{\big|F_i}=u_i$. Soit $\ds x\in E$, on pose comme d'habitude $\ds x=x_1+\dots+x_p$. On  a alors $\ds u(x)=u(x_1+\cdots+x_p)=u(x_1)+\cdots+u(x_p)=u_{\big|F_1}(x_1)+\cdots+u_{\big|F_p}(x_p)=u_1(x_1)+\cdots+u_p(x_p)=u_1(p_1(x))+\cdots+u_p(p_p(x))=(\sum_{i=1}^{p}u_i\circ p_i)(x)$. On a ainsi $\ds u=\sum_{i=1}^{p}u_i\circ p_i$ : cela justifie l'unicité.

    Montrons l'existence : On reprend l'application linéaire $\ds u=\sum_{i=1}^{p} u_i\circ p_i$ et on vérifie facilement que si $\ds x\in F_i$, alors $\ds u(x)=u_i(x)$ et ainsi $\ds u_{\big|F_i}=u_i$.

## Matrices par blocs et sous-espaces stables


!!! definition "Matrice définie par blocs"
    On peut écrire une matrice par *blocs* : on écrit par exemple $\ds A=\begin{pmatrix} A_{11} & A_{12}\\ A_{21} & A_{22} \end{pmatrix}$ où $\ds A_{11}$ est une matrice de taille  $\ds (p_1,q_1)$, $\ds A_{22}$ une matrice de taille $\ds (p_2,q_2)$. Evidemment $\ds A_{12}$ est de taille $\ds (p_1,q_2)$ et $\ds A_{21}$ est de taille $\ds (p_2,q_1)$. 

Les principales opérations matricielles sont compatibles avec la représentation par bloc

!!! propriete "combinaison linéaire par blocs"
    Soit $\ds A=\begin{pmatrix} A_{11} & A_{12}\\ A_{21} & A_{22} \end{pmatrix}$ et $\ds B=\begin{pmatrix} B_{11} & B_{12}\\ B_{21} & B_{22} \end{pmatrix}$ deux matrices de $\ds \mathcal M_{pq(\mathbb K)}$ avec, pour tout $\ds (i,j)$ la taille de $\ds A_{ij}$ et celle de $\ds B_{ij}$ identiques. Alors pour tous scalaires $\ds \lambda$ et $\ds \mu$, 
    
    $$
    \lambda A+\mu B=\begin{pmatrix}\lambda A_{11}+\mu B_{11} & \lambda A_{12}+\mu B_{12} \\\lambda A_{21}+\mu B_{21} &\lambda A_{22}+\mu B_{22} \end{pmatrix}
    $$

!!! propriete "transposition par blocs"
    Soit $\ds A=\begin{pmatrix} A_{11} & A_{12}\\ A_{21} & A_{22} \end{pmatrix}$ alors $\ds A^\top=\begin{pmatrix} A^\top_{11} & A^\top_{21}\\ A^\top_{12} & A^\top_{22} \end{pmatrix}$

!!! propriete "multiplication par blocs"   
    Le produit matriciel peut se faire par blocs. Si $\ds A$ et $\ds B$ sont deux matrices de la forme $\ds A=\begin{pmatrix} A_{11} & A_{12}\\ A_{21} & A_{22} \end{pmatrix}$ et $\ds B=\begin{pmatrix} B_{11} & B_{12}\\ B_{21} & B_{22} \end{pmatrix}$ avec les conditions suivantes : 

    - le nombre de colonnes de $\ds A_{11}$ et $\ds A_{21}$ est le même que le nombre de lignes de $\ds B_{11}$ et $\ds B_{12}$ ;

    - le nombre de colonnes de $\ds A_{12}$ et $\ds A_{22}$ est le même que le nombre de lignes de $\ds B_{21}$ et $\ds B_{22}$.

    On a alors $\ds AB$ qui est de la forme (par blocs) :

    $$
    \begin{pmatrix}
    A_{11}B_{11}+A_{12}B_{21} & A_{11}B_{12}+A_{12}B_{22} \\
    A_{21}B_{11}+A_{22}B_{21} & A_{21}B_{12}+A_{22}B_{22} \\
    \end{pmatrix}.
    $$


!!! exemple
    === "Énoncé"
        Soit $\ds A$ une matrice carrée diagonale de coefficients diagonaux $\ds \lambda_1,\dots,\lambda_n$. Expliciter la matrice $\ds A^k$ pour $\ds k$ un entier naturel.
    === "Corrigé"
        C'est un cas très (trop) particulier de multiplication par blocs. On a $\ds A^k=diag(\lambda_1^k,\dots,\lambda_n^k)$.

!!! propriete "Déterminant par blocs"
    Soit $\ds M=\begin{pmatrix} A & B \\ 0 & C\end{pmatrix}$ une matrice carrée. Alors $\ds \det(M)=\det(A)\times\det(C)$. 

!!! preuve
    D'après la propriété de produit par blocs, on remarque que $\ds M= \begin{pmatrix} I & 0 \\ 0 & C\end{pmatrix} \cdot\begin{pmatrix}A & B \\ 0 & I \end{pmatrix}$. 

    On a alors $\ds \det M = \det \begin{pmatrix} I & 0 \\ 0 & C\end{pmatrix}\times \det\begin{pmatrix}A & B \\ 0 & I \end{pmatrix}$. Or $\ds \det \begin{pmatrix} I & 0 \\ 0 & C\end{pmatrix} = \det C$ en développant plusieurs fois par rapport à la première colonne par exemple ; et $\ds \det \begin{pmatrix}A & B \\ 0 & I \end{pmatrix}=\det A$. D'où $\ds \det M = \det A\det C$.



!!! warning "Attention"
    Les propriétés que l'on a démontrées se généralisent à plus de blocs, à condition de toujours faire attention aux contrainte de dimensions. La propriété suivante est la plus utile dans ce contexte.

!!! propriete "Déterminant d'une matrice triangulaire par blocs"
    <center>
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="279px" viewBox="-0.5 -0.5 279 271" style="max-width:100%;max-height:271px;">
    <defs/><g>
    <rect x="15" y="10" width="30" height="30" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="none"/>
    <rect x="45" y="40" width="50" height="50" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="none"/>
    <rect x="95" y="90" width="40" height="40" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="none"/>
    <rect x="135" y="130" width="40" height="40" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" stroke-dasharray="3 3" pointer-events="none"/>
    <rect x="175" y="170" width="30" height="30" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="none"/>
    <rect x="205" y="200" width="60" height="60" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="none"/>
    <g transform="translate(-0.5 -0.5)"><switch><foreignObject style="overflow: visible; text-align: left;" pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 58px; height: 1px; padding-top: 26px; margin-left: 1px;"><div style="box-sizing: border-box; font-size: 0px; text-align: center;" data-drawio-colors="color: rgb(0, 0, 0); "><div style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: none; white-space: normal; overflow-wrap: normal;"><div>A<sub>1</sub></div></div></div></div></foreignObject><text x="30" y="30" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="12px" text-anchor="middle">A1</text></switch></g><g transform="translate(-0.5 -0.5)"><switch><foreignObject style="overflow: visible; text-align: left;" pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 58px; height: 1px; padding-top: 65px; margin-left: 41px;"><div style="box-sizing: border-box; font-size: 0px; text-align: center;" data-drawio-colors="color: rgb(0, 0, 0); "><div style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: none; white-space: normal; overflow-wrap: normal;"><div>A<sub>2</sub></div></div></div></div></foreignObject><text x="70" y="69" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="12px" text-anchor="middle">A2</text></switch></g><g transform="translate(-0.5 -0.5)"><switch><foreignObject style="overflow: visible; text-align: left;" pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 58px; height: 1px; padding-top: 110px; margin-left: 86px;"><div style="box-sizing: border-box; font-size: 0px; text-align: center;" data-drawio-colors="color: rgb(0, 0, 0); "><div style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: none; white-space: normal; overflow-wrap: normal;"><div>A<sub>3</sub></div></div></div></div></foreignObject><text x="115" y="114" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="12px" text-anchor="middle">A3</text></switch></g><g transform="translate(-0.5 -0.5)"><switch><foreignObject style="overflow: visible; text-align: left;" pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 58px; height: 1px; padding-top: 230px; margin-left: 206px;"><div style="box-sizing: border-box; font-size: 0px; text-align: center;" data-drawio-colors="color: rgb(0, 0, 0); "><div style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: none; white-space: normal; overflow-wrap: normal;"><div>A<sub>p</sub></div></div></div></div></foreignObject><text x="235" y="234" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="12px" text-anchor="middle">Ap</text></switch></g><g transform="translate(-0.5 -0.5)rotate(45 155 150)"><switch><foreignObject style="overflow: visible; text-align: left;" pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 58px; height: 1px; padding-top: 150px; margin-left: 126px;"><div style="box-sizing: border-box; font-size: 0px; text-align: center;" data-drawio-colors="color: rgb(0, 0, 0); "><div style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: none; white-space: normal; overflow-wrap: normal;">...</div></div></div></foreignObject><text x="155" y="154" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="12px" text-anchor="middle">...</text></switch></g><g transform="translate(-0.5 -0.5)"><switch><foreignObject style="overflow: visible; text-align: left;" pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 58px; height: 1px; padding-top: 185px; margin-left: 161px;"><div style="box-sizing: border-box; font-size: 0px; text-align: center;" data-drawio-colors="color: rgb(0, 0, 0); "><div style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: none; white-space: normal; overflow-wrap: normal;"><div>A<sub>p-1</sub></div></div></div></div></foreignObject><text x="190" y="189" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="12px" text-anchor="middle">Ap-1</text></switch></g><g transform="translate(-0.5 -0.5)"><switch><foreignObject style="overflow: visible; text-align: left;" pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 58px; height: 1px; padding-top: 200px; margin-left: 36px;"><div style="box-sizing: border-box; font-size: 0px; text-align: center;" data-drawio-colors="color: rgb(0, 0, 0); "><div style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: none; white-space: normal; overflow-wrap: normal;"><font style="font-size: 30px;">0</font></div></div></div></foreignObject><text x="65" y="204" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="12px" text-anchor="middle">0</text></switch></g><g transform="translate(-0.5 -0.5)"><switch><foreignObject style="overflow: visible; text-align: left;" pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 58px; height: 1px; padding-top: 55px; margin-left: 176px;"><div style="box-sizing: border-box; font-size: 0px; text-align: center;" data-drawio-colors="color: rgb(0, 0, 0); "><div style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: none; white-space: normal; overflow-wrap: normal;">X</div></div></div></foreignObject><text x="205" y="59" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="12px" text-anchor="middle">X</text></switch></g><path d="M 14 0 L 14 0 Q -6 135 14 270 L 14 270 Q -6 135 14 0 Z" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 278 0 L 278 0 Q 258 135 278 270 L 278 270 Q 258 135 278 0 Z" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" transform="rotate(-180,273,135)" pointer-events="none"/></g><switch><g requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"/><a transform="translate(0,-5)" xlink:href="https://www.drawio.com/doc/faq/svg-export-text-problems" target="_blank"><text text-anchor="middle" font-size="10px" x="50%" y="100%">Text is not SVG - cannot display</text></a></switch></svg></center>

    Si une matrice est triangulaire par blocs, alors son déterminant est le produit des déterminants de chaque bloc diagonal. On en déduit qu'un matrice triangulaire par blocs est inversible si et seulement si chacun des blocs diagonaux est inversible.

!!! preuve
    Une preuve se fait par récurrence en utilisant la propriété précédente. 


!!! definition "Sous -espace vectoriel stable par un endomorphisme"
    Soit $\ds u$ un endomorphisme d'un espace vectoriel $\ds E$. Soit $\ds F$ un sous-espace vectoriel de $\ds E$. On dit que $\ds F$ est stable par $\ds u$ si et seulement si $\ds u(F)\subset F$. Dans ce cas, la restriction de $\ds u$ à $\ds F$ est un endomorphisme de $\ds F$ que l'on appelle *endomorphisme induit*. 

!!! exemple
    === "Énoncé"
        Soit $\ds u$ un endomorphisme de $\ds E$ qui laisse stables toutes les droites de $\ds E$. Montrer que $\ds u$ est une homothétie.
    === "Corrigé"
        Si $\ds E$ est $\ds \{0\}$, alors tous les endomorphismes sont en particulier des homothéties.

        Sinon, soit $\ds x\in E$, $\ds vect(x)$ est stable par $\ds u$ donc il existe $\ds \lambda$ tel que $\ds u(x)=\lambda x$. Soit $\ds y\in E$
        
        - Si $\ds y\in\mathrm{vect}(x)$ alors $\ds y=\alpha x$ et $\ds u(y)=\alpha u(x)=\alpha\lambda x=\lambda \alpha x=\lambda y$.
        
        - Si $\ds y\notin \mathrm{vect}(x)$ alors $\ds (x,y)$ est libre. $\ds \mathrm{vect}(y)$ est stable donc $\ds u(y)=\mu y$. Mais $\ds \mathrm{vect}(x+y)$ est stable aussi donc $\ds u(x+y)=\gamma(x+y)$. Mais $\ds u(x+y)=u(x)+u(y)=\lambda x+\mu y$. On en déduit que $\ds \gamma x+\gamma y=\lambda x+\mu y$ ou encore $\ds (\lambda -\gamma)x+(\mu-\gamma)y=0$. Mais la famille $\ds (x,y)$ étant libre, on en déduit $\ds \lambda=\gamma$ et $\ds \mu=\gamma$ dont il découle $\ds \mu=\lambda$. Ainsi $\ds u(y)=\lambda y$. 

        Ainsi dans tous les cas, $\ds u(y)=\lambda y$ : $\ds u$ est une homothétie.

!!! exemple
    === "Énoncé"
        Soit $\ds u\in\mathcal L(E)$ injectif, et soit $\ds F$ un sous-espace vectoriel de $\ds E$ stable par $\ds u$ de dimension finie. Montrer que $\ds u_{\big|F}$ est bijectif. 
    === "Corrigé"
        $\ds u$ étant injective sur $\ds E$, $\ds u_{\big|F}$ est bien évidemment aussi injectif. De plus, $\ds u_{\big|F}$ est un endomorphisme linéaire de $\ds F$. Or $\ds F$ est de dimension finie et on en conclut que $\ds u_{\big|F}$ est bijectif.

!!! propriete "Interprétation matricielle"
    Soit $\ds E=F_1\oplus\cdots\oplus F_p$ en dimension finie.
    
    - si pour tout $\ds i\in\{1,\dots,p\}$, $\ds F_i$ est stable par $\ds u$, alors dans une base adaptée à la décomposition, la matrice de $\ds u$ est diagonale par blocs. 

    - de façon plus générale, si $\ds F$ est stable par $\ds u$, alors dans une base adaptée à $\ds F$, la matrice de $\ds u$ possède un bloc diagonal en haut à gauche.

    - si pour tout $\ds i\in\{1,\dots,p\}$, $\ds F_1\oplus\cdots\oplus F_i$ est stable par $\ds u$, alors dans une base adaptée à la décomposition, la matrice de $\ds u$ est triangulaire supérieure par blocs. 

!!! preuve
    Ce résultat est intuitif et la démonstration pénible : on zappe.



!!! propriete "Stabilité et commutativité"
    Soit $\ds u$ et $\ds v$ deux endomorphismes de $\ds E$ tels que $\ds u$ et $\ds v$ commutent ($u\circ v=v\circ u$). Alors le noyau de $\ds u$ est stable par $\ds v$ et l'image de $\ds u$ est stable par $\ds v$.

!!! preuve
    Soit $\ds u$ et $\ds v$ deux endormorphismes de $\ds E$ qui commutent. Soit $\ds y\in\v(\ker u)$, il existe $\ds x\in\ker u$ tel que $\ds y=v(x)$. Calculons $\ds u(y)$ dont on veut montrer qu'il est nul. 

    $$
    u(y)=u(v(x))\underset{\text{commutativité}}=v(u(x))=v(0)=0.
    $$

    Donc $\ds v(\ker u)\subset\ker u$ c'est à dire que $\ds \ker u$ est stable par $\ds v$. 

    Soit maintenant $\ds y\in\im(u)$ alors il existe $\ds x\in E$ tel que $\ds y=u(x)$. On a alors $\ds v(y)=v(u(x))=u(v(x))\in\im(u)$. On en déduit que $\ds v(\im(u))\subset\im(u)$ et donc l'image de $\ds u$ est stable par $\ds v$. 

!!! remarque 
    Évidemment, $\ds \ker v$ et $\ds \im v$ sont aussi stables par $\ds u$. 



!!! exemple
    === "Énoncé"
        Soit $\ds E$ un espace vectoriel et $\ds u$ un endomorphisme de $\ds E$. Soit $\ds p$ un projecteur de $\ds E$. Montrer que :
        
        $$
        p\circ f=f\circ p\Leftrightarrow \im(p)\text{ et }\ker(p)\text{ sont stables par }f.
        $$

    === "Corrigé"
        L'implication directe correspond à la propriété que nous venons de démontrer. C'est l'implication réciproque qui est intéressante.  Supposons donc que $\ds \im(p)$ et $\ds \ker(p)$ sont stables par $\ds f$. Soit $\ds x\in E$, on sait par définition du projecteur, que $\ds x=x_K+x_I$ avec $\ds x_k\in\ker p$ et $\ds x_I\in\im p$. On a alors 

        $$
        f\circ p(x)=f(p(x_K+x_I))=f(p(x_K)+p(x_I))=f(p(x_I))
        $$

        or $\ds p$ est un projecteur et $\ds x_I\in\im p$ donc $\ds p(x_I)=x_I$. donc $\ds f\circ p(x)=f(x_I)$. Dans l'autre sens :

        $$  
        p\circ f(x)=p(f(x_K)+f(x_I))=p(f(x_K))+p(f(x_I))
        $$ 

        Or $\ds \ker p$ est stable par  $\ds f$ donc $\ds f(x_K)\in\ker p$ et $\ds p(f(x_K))=0$. De même $\ds \im(p)$ est stable par $\ds f$ donc $\ds f(x_I)\in\im p$ et ainsi $\ds p(f(x_I))=f(x_I)$. On en déduit que $\ds p\circ f(x)=f(x_I)=f\circ p(x)$ : $\ds p$ et $\ds f$ commutent.

## Trace


!!! definition "Trace d'une matrice carrée"
    Soit $\ds A\in\mathcal M_{n}(\mathbb K)$ une matrice carrée de coefficients $\ds (a_{i,j})_{1\le i,j\le n}$. On définit la *trace* de $\ds A$ par :
    
    $$
    \tr(A)=\sum_{i=1}^na_{ii}
    $$

!!! propriete "Statut de la trace"
    L'opérateur de trace est une forme linéaire sur $\ds E$

!!! preuve
    Tout d'abord, $\ds \tr$ est bien définie sur $\ds \mathcal M_{n}(\mathbb K)$ à valeurs dans $\ds \mathbb K$. Ensuite, prenons $\ds A$ et $\ds B$ deux matrices de $\ds \mathcal M_n(\mathbb K)$ et $\ds \lambda$ et $\ds \mu$ deux éléments de $\ds \mathbb K$. On pose $\ds C=\lambda A+\mu B$, on a donc pour tout entier $\ds 1\le i\le n$, $\ds C_{ii}=(\lambda A+\mu B)_{ii}=\lambda A_{ii}+\mu B_{ii}$. Dès lors :

    $$
    \begin{aligned}
    \tr(C) &= \sum_{i=1}^nC_{ii}\\
    &=\sum_{i=1}^{n}\lambda A_{ii}+\mu B_{ii}\\
    &=\lambda\sum_{i=1}^n A_{ii}+\mu\sum_{i=1}^nB_{ii}\\
    &=\lambda\tr(A)+\mu\tr(B)
    \end{aligned}
    $$

    On en déduit que $\ds \tr$ est bien une application linéaire de $\ds E$ dans $\ds \mathbb K$ : c'est une forme linéaire. 

!!! exemple
    === "Énoncé"
        Préciser la dimension de $\ds \ker\tr$, une base de $\ds \ker\tr$ ainsi qu'un supplémentaire simple de $\ds \ker\tr$.
    === "Corrigé"
        Puisque $\ds \tr$ est une forme linéaire sur $\ds \mathcal M_n(\mathbb K)$, on a (d'après le théorème du rang par exemple) : $\ds \dim\ker\tr=n^2-1$. Une base de $\ds \ker\tr$ est par exemple composée des matrices élémentaires $\ds E_{ij}$ pour $\ds i\ne j$ et $\ds E_{ii}-E_{jj}$ tojours avec $\ds i\ne j$. Cela donne bien $\ds n^2-1$ matrices qui forment une famille libre. L'espace $\ds vect(I_n)$ est un bon supplémentaire de cet ensemble. 

!!! propriete "Trace d'une transposée"
    Soit $\ds A$ une matrice carrée de $\ds \mathcal M_n(\mathbb K)$. Alors $\ds \tr(A^\top)=\tr(A)$. 

!!! preuve
    La preuve est assez simple. La matrice $\ds A^{\top}$ est définie par ses coefficients : $\ds (A^\top)_{ij}=A_{ji}$. Mais sur les coefficients diagonaux, cela donne : $\ds (A^\top)_{ii}=A_{ii}$. On a donc :

    $$
    \tr(A^\top)=\sum_{i=1}^n(A^\top)_{ii}=\sum_{i=1}^nA_{ii}=\tr(A).
    $$


!!! propriete "Symétrie de la trace"
    Soient $\ds A$ et $\ds B$ deux matrices carrées, alors $\ds \tr(AB)=\tr(BA)$

!!! remarque
    La propriété marche aussi avec des matrices non carrées à condition de respecter des contraintes naturelles de dimension.

!!! preuve
    On note $\ds a_{ij}$ les coefficients de $\ds A$ et $\ds b_{ij}$ les coefficients de $\ds B$. On a alors :

    $$
    \begin{aligned}
    \tr(AB) &=\sum_{i=1}^{n}(AB)_{ii}\\
    &=\sum_{i=1}^n \sum_{k=1}^{n}a_{ik}b_{ki}\\
    &=\sum_{k=1}^n\sum_{i=1}^na_{ik}b_{ki}\\
    &=\sum_{k=1}^n\sum_{i=1}^nb_{ki}a_{ik}\\
    &=\sum_{k=1}^n(BA)_{kk}\\
    &=\tr (BA)
    \end{aligned}
    $$
    
    Ainsi $\ds \tr(AB)=\tr(BA)$. 


!!! definition "Matrices semblables"
    Soit $\ds A$ et $\ds B$ deux matrices de $\ds \mathcal M_n(\mathbb K)$. On dit que $\ds A$ et $\ds B$ sont semblables si et seulement si il existe une matrice inversible $\ds P\in\mathcal Gl_n(\mathbb K)$ telle que $\ds P^{-1}AP=B$. 

!!! exemple 
    Deux matrices d'un même endomorphisme dans deux bases différentes sont semblables (formule du changement de base).

!!! propriete "Invariance par similitude de la trace"
    Soient $\ds A$ et $\ds B$ deux matrices carrées semblables. Alors $\ds \tr(A)=\tr(B)$.

!!! preuve 
    Soient $\ds A$ et $\ds B$ semblables dans $\ds \mathcal M_n(\mathbb K)$, soit $\ds P\in\mathcal Gl_n(\mathbb K)$ telles que $\ds P^{-1}AP=B$. Alors : 
    
    $$
    \tr(B)=\tr(P^{-1}AP)=\tr(P^{-1}(AP))=\tr((AP)P^{-1})=\tr(A(P^{-1}))=\tr(AI_n)=\tr(A)
    $$

    et ainsi la trace est invariante par similitude.

!!! definition "Trace d'un endomorphisme"
    Soit $\ds u$ un endomorphisme d'un espace vectoriel $\ds E$. L'invariance de la trace par similitude permet de définir la trace de $\ds u$ comme étant la trace de n'importe qu'elle matrice représentative de $\ds u$ dans une base de $\ds E$.

!!! exemple 
    === "Énoncé"
        Soit $\ds p$ un projecteur de $\ds E$ de dimension fini. On pose $\ds F=\im(p)$. Alors $\ds \tr(p)=\dim(F)$.  
    === "Corrigé"
        On l'a vu précédemment, dans une base adaptée à la décomposition $\ds E=\im(p)\oplus\ker p$ la matrice de $\ds p$ contient $\ds \dim(F)$ 1 sur la diagonale suivis de $\ds 0$. Or la trace de cette matrice est exactement $\ds \dim(F)$. Donc pour résumer $\ds \tr(p)=\rg(p)$. 

!!! propriete "Symétrie de la trace d'un endomorphisme"
    On a pour tous endomorphismes $\ds u$ et $\ds v$ de $\ds E$, $\ds \tr(u\circ v)=\tr(v\circ u)$. 

!!! preuve
    Soit $\ds \mathcal B$ une base de $\ds E$. Soient $\ds A=mat_{\mathcal B}u$ et $\ds B=mat_{\mathcal B}u$. Alors $\ds AB=mat_{\mathcal B}(u\circ v)$ et $\ds BA=mat_{\mathcal B}(v\circ u)$. On a alors : 

    $$
    \tr(u\circ v)=\tr(mat_{\mathcal B}(u\circ v))=\tr(AB)=\tr(BA)=\tr(mat_{\mathcal B}(v\circ u)=\tr(v\circ u).
    $$

!!! exemple
    === "Énoncé"
        Soit $\ds \varphi$ l'application de $\ds \mathcal M_n(\mathbb K)$ dans $\ds \mathcal M_n(\mathbb K)$ définie par $\ds \varphi(M)=M+\tr(M)I_n$. Calculer la trace et le déterminant de $\ds \varphi$.  
    === "Corrigé"
        Pour calculer la trace et le déterminant d'une application linéaire, le plus simple est d'exprimer sa matrice dans une base bien choisie. Une matrice de $\ds \varphi$ sera de taille $\ds n^2\times n^2$. On prend comme base de $\ds \mathcal M_n(\mathbb K)$ une base adaptée à la décomposition $\ds \mathcal M_n(\mathbb K)=Vect(I_n)\oplus\ker\tr$ sous la forme $\ds (I_n,E_1,\dots,E_{n^2-1})$. Pour tout $\ds i\in\{1,\dots,n^2-1\}$, $\ds \varphi(E_i)=E_i+\tr(E_i)I_n=E_i$ et $\ds \varphi(I_n)=I_n+\tr(I_n)I_n=(n+1)I_n$. On en déduit que la matrice de $\ds \varphi$ dans cette base est diagonale, avec pour termes diagonaux $\ds ((n+1),1,1,\dots,1)$. Sa trace vaut alors $\ds n+1+(n^2-1)\times 1=n(n+1)$ et son déterminant vaut $\ds (n+1)\times1\times\cdots\times 1=n+1$. 
 

## Polynômes d'endomorphismes et de matrices carrées

!!! definition "Notations"
    Soit $\ds E$ un espace vectoriel sur un corps $\ds \mathbb K$. Soit $\ds u\in\mathcal L(E)$. On note $\ds u^0=\mathrm{id}_E$ et pour tout entier $\ds n$, $\ds u^{n+1}=u\circ u^n=u^n\circ u$.

!!! definition "Polynôme d'endomorphisme/de matrice"
    Soit $\ds E$ un $\ds \mathbb K$-espace vectoriel, soit $\ds u\in\mathcal L(E)$ et $\ds P\in\mathbb K[X]$. On pose $\ds P=a_dX^d+a_{d-1}X^{d-1}+\cdots+a_1X+a_0$. On note :
    
    $$
    P(u) = a_{d}u^d+a_{d-1}u^{d-1}+\cdots+a_1u+a_0\mathrm{id}_E.
    $$

    $\ds P(u)$ est donc un endormorphisme de $\ds E$.

    Si $\ds A\in\mathcal M_n(\mathbb K)$, on note :

    $$
    P(A) = a_dA^d+a_{d-1}A^{d-1}+\cdots+a_1A+a_0I_n.
    $$

    C'est un élément de $\ds \mathcal M_n(\mathbb K)$. 


!!! exemple
    Si $\ds P=X^2+3X+1$, alors $\ds P(u)=u^2+3u+\mathrm{id}_E$

!!! exemple
    === "Énoncé"
        Soit $\ds D$ une matrice diagonale de coefficients $\ds \lambda_1,\dots,\lambda_n$. Soit $\ds P$ un polynôme de $\ds \mathbb K[x]$, expliciter $\ds P(D)$.
    === "Corrigé"
        D'après les règles de produit matriciel, la matrice $\ds D^k$ est une matrice diagonale dont les coefficients diagonaux son $\ds \lambda_1^k,\lambda_2^k,\dots,\lambda_n^k$. Par linéarité, on en déduit que $\ds P(D)$ est une matrice diagonale de coefficients $\ds P(\lambda_1),P(\lambda_2),\dots,P(\lambda_n)$. 


!!! propriete "règles de calcul"
    Soient $\ds E$ un espace vectoriel sur $\ds \mathbb K$, $\ds P$ et $\ds Q$ deux polynômes de $\ds \mathbb K[X]$ et $\ds u\in\mathcal L(E)$. Soient $\ds \lambda$ et $\ds \mu$ deux éléments de $\ds \mathbb K$. Alors :

    - $\ds (\lambda P+\mu Q)(u)=\lambda P(u)+\mu Q(u)$

    - $\ds (PQ)(u)=P(u)\circ Q(u)$. 

!!! preuve
    Le premier point ne pose pas de problème. Pour le second : 

    $$
    \begin{aligned}
    (PQ)(u)&=\left((\sum_{k=0}^np_kX^k)\times(\sum_{j=0}^mq_jX^j)\right)(u)\\
    &=\left(\sum_{k=0}^n\sum_{j=0}^mp_kq_jX^{k+j}\right)(u)\\
    &=\sum_{k=0}^n\sum_{j=0}^mp_kq_ju^{k+j}\\
    &=\sum_{k=0}^n\sum_{j=0}^mp_kq_ju^k\circ u^j\\
    &=\sum_{k=0}^np_ku^k\circ(\sum_{j=0}^mq_ju^j)\\
    &=(\sum_{k=0}^np_ku^k)\circ(Q(u))\\
    &=P(u)\circ Q(u)
    \end{aligned}
    $$

    les dernières lignes étant des conséquences de la linéarité de $\ds u$. 


!!! propriete "Commutativité"
    Une conséquence directe de la propriété précédente est que deux polynômes d'un endomorphisme $\ds u$ commutent.

!!! preuve
    En effet, $\ds P(u)\circ Q(u)=(PQ)(u)=(QP)(u)=Q(u)\circ P(u)$.


!!! propriete "Stabilité par $\ds P(u)$"
    L'image et le noyau de $\ds u$ sont stables par $\ds P(u)$. L'image et le noyau de $\ds P(u)$ sont stables par $\ds u$. 

!!! preuve
    Par définition de $\ds P(u)$, $\ds u$ et $\ds P(u)$ commutent. D'après une propriété précédente, $\ds \im(u)$ et $\ds \ker(u)$ sont stables par $\ds P(u)$. 

!!! definition "Polynôme annulateur"
    Soit $\ds u\in\mathcal L(E)$ et $\ds P$ un polynôme non nul. On dit que $\ds P$ est un *polynôme annulateur de $\ds u$* si et seulement si $\ds P(u)=0_{\mathcal L(E)}$. 

    Si $\ds A\in\mathcal M_n(\mathbb K)$, on dit que $\ds P$ est un *polynôme annulateur de $\ds A$* si et seulement si $\ds P(A)=0_{\mathcal M_n(\mathbb K)}$. 
 

!!!warning "Attention"
    Un polynôme annulateur n'est pas unique : si $\ds P$ est annulateur de $\ds u$, alors pour tout polynôme $\ds Q$, $\ds QP$ est aussi annulateur de $\ds u$ !

!!! exemple
    === "Énoncé"
        Soit $\ds A=\begin{pmatrix} 2 & 1 \\ 3 & 2\end{pmatrix}$. Trouver un polynôme annulateur de $\ds A$.
    === "Corrigé"
        On calcule $\ds A^2=\begin{pmatrix}7 & 4 \\ 12 & 7\end{pmatrix}$ et on remarque que $\ds A^2-4A+I_2=0$. Donc $\ds X^2-4X+1$ est un polynôme annulateur de $\ds A$

!!! exemple
    === "Énoncé"
        Soit $\ds p$ un projecteur de d'un espace vectoriel $\ds E$. Donner un polynôme annulateur de $\ds p$.
    === "Corrigé"
        $\ds p$ étant un projecteur, on sais que $\ds p\circ p=p$. On en déduit $\ds p^2-p=0$ et donc $\ds X^2-X$ est un polynôme annulateur de $\ds p$. 


!!! propriete "Existence en dimension finie"
    Soit $\ds u\in\mathcal L(E)$ avec $\ds E$ de dimension finie $\ds n$. Alors il existe un polynôme $\ds P\in\mathbb K[X]$ tel que $\ds P(u)=0$. 
    
    Soit $\ds A\in\mathcal M_n(\mathbb K)$. Alors il existe un polynôme $\ds P\in\mathbb K[X]$ tel que $\ds P(A)=0$. 
    
!!! preuve
    On se contente de la preuve sur les matrices par exemple. Considérons la famille $\ds (I_n,A,A^2,A^3,\dots,A^{n^2})$. Cette famille possède $\ds n^2+1$ éléments dans un espace vectoriel ($\mathcal M_n(\mathbb K)$) de dimension $\ds n^2$. Elle n'est donc pas libre et il exste $\ds (a_0,a_1,\dots,a_{n^2})\in\mathbb K^{n^2+1}$ tels que $\ds a_0I_n+a_1A+\cdots+a_{n^2}A^{n^2}=0$. Posons $\ds P=a_0+a_1X+\cdots+a_{n^2}X^{n^2}$, on a bien $\ds P\in\mathbb K[X]$ et $\ds P(A)=0$. 

!!! warning "Attention" 
    Si $\ds E$ n'est pas de dimension finie rien n'assure l'existence d'un polynôme annulateur pour un endomorphisme $\ds u$.

!!! exemple
    === "Énoncé"
        Considérons l'application linéaire définie de $\ds E=\mathbb K[X]$ dans $\ds \mathbb K[X]$ par $\ds u(P)=XP$. Montrer que $\ds u$ n'admet pas de polynôme annulateur.
    === "Corrigé"
        Considérons l'application linéaire définie de $\ds E=\mathbb K[X]$ dans $\ds \mathbb K[X]$ par $\ds u(P)=XP$. On remarque alors que $\ds u^k(P)=X^kP$. Supposons que $\ds u$ admette un polynôme annulateur $\ds \chi$. On a alors $\ds \chi(u)=0$. En posant $\ds \chi = a_0+a_1X+\cdots+a_nX^n$, on a donc $\ds a_0\mathrm{id}_E+a_1u+\dots+a_nu^n=0$. Appliquons cela à $\ds P=1$, on obtient : $\ds a_0+a_1u(1)+a_2u^2(1)+\cdots+a_nu^n(1)=0$ ou encore $\ds a_0+a_1X+\cdots+a_nX^n=0$. Or un polynôme est nul si et seulement si ses coefficients sont nuls : $\ds a_0=a_1=\dots=a_n=0$. 

!!! propriete "Caractérisation d'inversibilité"
    Soit $\ds A$ une matrice et soit $\ds P$ un polynôme annulateur de $A$. Alors $\ds A$ est inversible si et seulement si il existe un polynôme annulateur $\ds P_0$ tel que $\ds P_0(0)\ne 0$.

!!! preuve
    Soit $\ds A$ une matrice non nulle possédant un polynôme annulateur $\ds P$.

    Supposons que $\ds P_0(0)\ne 0$. Posons $\ds P_0=a_dX^d+\cdots+a_1X+a_0$. Puisque $\ds P_0(0)\ne 0$, on a $\ds a_0\ne 0$. La propriété $\ds P_0(A)=0$ permet d'écrire : $\ds A(a_dA^{d-1}+\cdots+a_1I_n)=-a_0I_n$. On en déduit que $\ds A$ est inversible, d'inverse $\ds a_dA^{d-1}+\cdots+a_1I_n$.

    Supposons que $\ds A$ est inversible et soit $P$ annulateur de $A$. Posons $P=X^kQ$ avec $Q(0)\ne 0$ ($k$ est la multiplicité de $0$ en tant que racine de de $P$). On a alors $P(A)=0$ et $P(A)=A^kQ(A)$ et ainsi $A^kQ(A)=0$. Or $A$ est inversible donc $Q(A)=0$. Ainsi $Q$ est annulateur de $A$ et $Q(A)\ne0$. 


!!! exemple
    === "Énoncé"
        Soit $\ds A=\begin{pmatrix} 2 & 1 \\ 3 & 2\end{pmatrix}$. Montrer que $\ds A$ est inversible et exprimer $\ds A^{-1}$ en fonction de $\ds A$. 
    === "Corrigé"
        On a montré précédemment que $\ds A^2-4A+I_2=0$. On en déduit que $\ds I_2=A(4I_2-A)$ et donc $\ds A$ est inversible et son inverse est $\ds 4I_2-A$. 

!!! exemple
    === "Énoncé"
        Soit $\ds A$ une matrice de $\ds \mathcal M_n(\mathbb K)$ telle que $\ds A^2-3A+2I_n$. Calculer $\ds A^n$ pour tout $\ds n\in\mathbb N$ en fonction de $\ds A$ et $\ds I_n$.
    === "Corrigé"
        Soit $\ds n\in\mathbb N$ avec $\ds n>2$ (on connaît déjà $\ds A^0$, $\ds A^1$ et $\ds A^2$). On pose $\ds P=X^2-3X+2$. Les racines de $\ds P$ sont $\ds 2$ et $\ds 1$. On calcule le reste de la division euclidienne de $\ds X^n$ par $\ds P$ :  $\ds X^n=Q\times P+R$ avec $\ds R$ de degré $\ds 1$ : on pose $\ds R=aX+b$. On prend cette équation en $\ds 2$ et en $\ds 1$ : $\ds 1^n=Q(1)P(1)+R(1)=R(1)=a+b$ et $\ds 2^n=Q(2)P(2)+R(2)=0+R(2)=2a+b$.

        On résout alors $\ds \begin{cases} a+b=1\\2a+b=2^n\end{cases}$ dont les solutions sons $\ds a=2^n-1$ et $\ds b=2-2^n$. On peut donc écrire : $\ds X^n=QP+(2^n-1)X+2-2^n$. En prenant cette relation en $\ds A$ avec les propriétés de calculs vues précédemment, on obtient $\ds A^n=0+(2^n-1)A+(2-2^n)I_2$. Cette relation est cohérente avec le cas $\ds n=2$. 

!!! exemple
    === "Énoncé"
        Soit $\ds A=\begin{pmatrix}2&1&1\\1&2&1\\1&1&2\end{pmatrix}$. Calculer $\ds A^n$ pour tout $\ds n\in\mathbb N$. 
    === "Corrigé"
        On calcule $\ds A^2=\begin{pmatrix}6&5&5\\5&6&5\\5&5&6\end{pmatrix}=5A-4I_2$. Alors $\ds P=X^2-5X+4$ est un polynôme annulateur de $\ds A$. P admet comme racines $\ds 4$ et $\ds 1$. Le reste de la division euclidienne de $\ds X^n$ par $\ds P$ est de la forme $\ds aX+b$ avec $\ds a$ et $\ds b$ qui vérifient : $\ds \begin{cases} a+b=1\\4a+b=4^n\end{cases}$ c'est-à-dire $\ds a=\frac 13(4^n-1)$ et $\ds b=\frac 43(1-4^{n-1})$. et on obtient $\ds A^n=\frac 13(4^n-1)A+\frac 43(1-4^{n-1})I_2$. 

        On pourrait traiter cela différemment en remarquant que $\ds A=I_3+J$ où $\ds J$ est la matrice $\ds 3\times 3$ dont tous les coefficients valent $\ds 1$. On sait (ou on calcule) $\ds J^k=3^{k-1}J$. On a alors $\ds A^n=\sum_{k=0}^{n}\binom nk J^k=\sum_{k=1}^n\binom nk 3^{k-1}J+I_2=\frac 13\sum_{k=1}^{n}\binom nk 3^k J+I_3=\frac13(4^n-1)J+I_3=\frac 13(4^n-1)(A-I_3)+I_3=\frac 13(4^n-1)A+\frac 13(4-4^n)I_3$. 


## Interpolation de Lagrange


Nous allons étudier ici une base de polynômes particulière. On se place sur $\ds \mathbb K_n[X]$. On se donne dans tout le paragraphe $\ds (x_0,\dots,x_n)$ $\ds n+1$ éléments **distincts** de $\ds \mathbb K$. 

!!! definition "Polynômes de Lagrange"
    Pour tout $\ds i\in\{à,\dots,n\}$ on définit : $\ds \ds L_i=\prod_{\substack{k=0\\k\ne i}}^{n}\frac{X-x_k}{x_i-x_k}$. $\ds L_i$ est appelé le *iè polynôme de Lagrange*.

!!! propriete "caractéristiques des polynômes de Lagrange"
    Soit $\ds L_0,L_1,\dots,L_n$ les $\ds n+1$ polynômes de Lagrange associés au scalaires $\ds x_0,x_1,\dots,x_n$, alors :

    - pour tout $\ds (i,j)\in\{0,\dots,n\}$, $\ds L_i(x_j)=\delta_{ij}$ (symbole de Kronecker)

    - $\ds (L_0,L_1,\dots,L_n)$ est une base de $\ds \mathbb K_n[X]$.

!!! preuve
    **Premier point :** Si $\ds i=j$ alors $\ds L_i(x_j)=L_i(x_i)=\prod_{\substack{k=0\\k\ne i}}^{n}\frac{x_i-x_k}{x_i-x_k}=1$.

    Si $\ds i\ne j$ alors $\ds L_i(x_j)=\prod_{\substack{k=0\\k\ne i}}^{n}\frac{x_j-x_k}{x_i-x_k}=\frac{x_j-x_j}{x_i-x_j}\prod_{\substack{k=0\\k\ne i\\k\ne j}}^{n}\frac{X-x_k}{x_i-x_k}=0$.

    On a bien $\ds L_i(x_j)=\delta_{ij}$.

    Montrons à l'aide de cette propriété, que la famille est libre : comme elle contient $\ds n+1$ vecteur, ce sera alors une base. 

    Soit $\ds \lambda_0,\lambda_1,\dots,\lambda_n$ ds scalaires tels que $\ds \lambda_0L_0+\lambda_1L_1+\cdots+\lambda_nL_n=0$. On prend cette expression en $\ds x_0$ par exemple. On obtient $\ds \lambda_0L_0(x_0)+\lambda_1L_1(x_0)+\cdots+\lambda_nL_n(x_0)=0$. Or $\ds L_1(x_0)=L_2(x_0)=\dots=L_n(x_0)=0$ donc $\ds \lambda_0L_0(x_0)=0$. Mais $\ds L_0(x_0)=1$ donc $\ds \lambda_0=0$. En itérant ce processus, on montre alors que $\ds \lambda_0=\lambda_1=\dots=\lambda_n=0$. La famille est donc bien libre, et comme on l'a expliqué précédemment, c'est donc une base. 

!!! definition "Polynôme d'interpolation"
    Soit $\ds (x_0,y_0),(x_1,y_1),\dots,(x_n,y_n)$ $\ds n+1$ couples d'éléments de $\ds \mathbb K$. On pose $\ds P = \sum_{k=0}^{n}y_kL_k$. Alors $\ds P$ verifie la propriété suivante :
    
    $$
    deg(P)\le n\quad\text{et}\quad \forall i\in\{0,\dots,n\},\,P(x_i)=y_i
    $$

    et c'est le seul polynôme vérifiant cette propriété. $\ds P$ est appelé *polynôme d'interpolation de Lagrange aux points* $\ds (x_i,y_i)$.

!!! preuve
    Regardons le comportement de $\ds P$ en $\ds x_0$ par exemple. 

    $$
    P(x_0)=\sum_{k=0}^ny_kL_k(x_0)=\sum_{k=0}^ny_k\delta_{k0}=y_0
    $$

    et on fait de même pour les autres. Soit $\ds Q$ un autre polynôme de $\ds \mathbb K_n[X]$ tel que pour tout $\ds k$, $\ds Q(x_k)=y_k$. Alors le polynôme $\ds R=P-Q$ est tel que $\ds R\in\mathbb K_n[X]$ et pour tout $\ds i\in\{0,\dots,n\}$, $\ds R(x_i)=P(x_i)-Q(x_i)=y_i-y_i=0$. Ainsi $\ds R$ est un polynôme de degré au plus $\ds n$ mais possédant $\ds n+1$ racines (on rappelle que les $\ds x_i$ sont **distincts**). Le seul polynôme vérifiant cela est le polynôme nul donc $\ds P=Q$ : il y a bien unicité.


!!! propriete "Somme de polynômes de Lagrange"
    Avec toujours les même notations, on a $\ds \sum_{i=0}^nL_i=1$. 

!!! preuve 
    c'est un cas particulier de la définition précédente en prenant les couples $\ds (x_i,1)$. On pose $\ds P=\sum_{i=0}^{n}L_i$. On a alors pour tout $\ds i\in\{0,\dots,n\}$, $\ds P(x_i)=1$. Le polynôme $\ds P-1$ possède alors $\ds n+1$ racines alors qu'il est de degré au plus $\ds n$ : $\ds P-1=0$ c'est-à-dire $\ds P=1$. On a donc bien $\ds \sum_{i=0}^nL_i=1$. 



!!! exemple
    === "Énoncé"
        Soit $\ds A$ une matrice diagonale de $\ds \mathcal M_{n+1}(\mathbb K)$ dont les coefficients $\ds a_0,\dots,a_n$ sont tous distincts. Soit $\ds B$ une deuxième matrice diagonale de coefficients $\ds b_0,\dots,b_n$ quelconques dans $\ds \mathbb K$. Montrer qu'il existe un polynôme $\ds P$ tel que $\ds P(A)=B$. 
    === "Corrigé"
        Notons $\ds P$ le polynôme d'interpolation de Lagrange associé aux points $\ds (a_i,b_i)$. Alors $\ds P(A)$ est la matrice diagonale dont les coefficients diagonaux sont les $\ds P(a_i)=y_i$. Donc $\ds P(A)=B$.

!!! definition "Matrice de Vandermonde"
    Résoudre le problème de l'interpolation, c'est chercher les coefficients $\ds a_0,a_1,\dots,a_n$ tels que pour tout $\ds i\in\{0,\dots,n\}$, $\ds a_0+a_1x_i+a_2x_i^2+\dots+a_nx_n^n=y_i$. Ce qui se traduit sous forme matricielle par :

    $$
    \begin{pmatrix} 
    1 & x_0 & x_0^2& \cdots & x_0^n\\
    1 & x_1 & x_1^2& \cdots & x_1^n\\
    \vdots &\vdots & \vdots & \ddots & \vdots\\
    1 & x_n & x_n^2 & \cdots & x_n^n
    \end{pmatrix} 
    $$

    Cette matrice, ou plus fréquemment sa transposée, est appelée *matrice de Vandermonde*.

!!! propriete "Déterminant de Vandermonde"
    On appelle *déterminant de Vandermonde* le déterminant

    $$
    V(x_0,x_1,\dots,x_n)=    \begin{vmatrix} 
    1 &  1 & \cdots & 1\\
    x_0 & x_1 &  \cdots & x_n\\
    x_0^2 & x_1^2 &  \cdots & x_n^2\\
    \vdots &\vdots & \ddots & \vdots\\
    x_0^n & x_1^n & \cdots & x_n^n
    \end{vmatrix} 
    $$

    On a $\ds V(x_0,x_1,\dots,x_n)=\prod_{0\le i<j\le n}(x_j-x_i)$.  La matrice est inversible si et seulement si les $\ds x_i$ sont distincts.

!!! preuve
    Le déterminant $\ds V(x_0,x_1,\dots,x_{n-1},X)$ est un polynôme en $\ds X$. En remplaçant $\ds X$ par $\ds x_i$ pour $\ds i\in\{0,\dots,n-1\}$ on obtient un déterminant ayant deux lignes identiques : ce déterminant est nul. De plus en développant par rapport à la dernière colonne, on obtient un polynôme en $\ds X$ de degré $\ds n$ ayant pour coefficient dominant $\ds V(x_0,\dots,x_{n-1})$. On a donc identifié les $\ds n$ racines de ce polynôme :$(x_0,\dots,x_{n-1})$. On en déduit que $\ds V(x_0,\dots,x_{n-1},X)=V(x_0,\dots,x_{n-1})\prod_{i=0}^{n-1}(X-x_i)$ et donc :

    $$
    V(x_0,\dots,x_n) = V(x_0,\dots,x_{n-1})\prod_{i=0}^{n-1}(x_n-x_i)
    $$

    En itérant cela, on obtient effectivement $\ds V(x_0,\dots,x_n)=\prod_{0\le i<j\le n}(x_j-x_i)$. 


## Exercices

### Issus de la banque CCINP
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-059.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-060.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-062.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-064.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-071.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-085.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-087.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-088.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-090.md" %}


### Annales d'oraux
{% include-markdown "../../../exercicesRMS/2022/RMS2022-678.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-679.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-681.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-694.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-678.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1275.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1277.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1291.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1398.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1347.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1348.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1402.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1405.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1406.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1090.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1137.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1400.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-775.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-777.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-786.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-787.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-793.md" %}
{% include-markdown "../../../exercicesRMS/2019/RMS2019-1137.md" %}


### Centrale Python
{% include-markdown "../../../exercicesCentralePython/RMS2016-1012.md"  rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2018-928.md" %}
{% include-markdown "../../../exercicesCentralePython/RMS2021-1002.md" %}
