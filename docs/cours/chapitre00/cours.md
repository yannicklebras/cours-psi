# Compléments sur les séries numériques

## Programme de PCSI

Un chapitre sur les séries numériques a été traité en première année. Voici les points au programme de PCSI :



### Convergence et divergence
<table>
<tr>
<td width=50%>Sommes partielles d'une série numérique.

Convergence, divergence, somme.
</td><td>
La série est notée $\ds \ds{\sum u_n}$.

En cas de convergence, sa somme est notée
$\ds\sum_{n=0}^{+\infty}u_n$.
</td>
</tr>
<tr>
<td>
Linéarité de la somme.
</td><td></td>
</tr>
<tr>
<td>
Le terme général d'une série convergente tend vers 0.
</td>
<td>
Divergence grossière.
</td></tr>
<tr><td>
Reste d'une série convergente.</td><td></td></tr><tr><td>

Lien suite-série.</td><td>
La suite $\ds (u_n)$ et la série télescopique
$\ds{\sum (u_{n+1}-u_n)}$ sont de même nature.</td></tr><tr><td>

Séries géométriques :
condition nécessaire et suffisante de convergence, somme.</td><td></td></tr><tr><td>

Relation
$\ds{ e^z=\sum_{n=0}^{+\infty}\frac{z^n}{n!}}$ pour $\ds z\in \C$.</td><td></td></tr>
</table>

### Séries à termes positifs ou nuls
<table>
<tr>
<td width=50%>
Convention de calcul et relation d’ordre dans $\ds [0, +\infty]$.</td><td>
On note $\ds \ds\sum_{n=0}^{+\infty} u_n = +\infty$ si la série
$\ds \sum u_n$ d'éléments de $\ds \mathbb{R}^+$ diverge.
</td></tr><tr><td>

Une série à termes positifs converge si et seulement si la suite de
ses sommes partielles est majorée.</td><td></td></tr><tr><td>
Si $\ds 0 \leqslant u_n \leqslant v_n$ pour tout $\ds n$, la convergence de
$\ds \sum v_n$ implique celle de $\ds \ds \sum u_n$.</td><td></td></tr><tr><td>

Si $\ds (u_n)_{n \in \N}$ et $\ds (v_n)_{n \in \N}$ sont positives et
si $\ds u_n \sim v_n$, les séries $\ds \ds \sum u_n$ et
$\ds \sum v_n$ sont de même nature.</td><td></td></tr><tr><td>

Si $\ds f$ est monotone, encadrement des sommes partielles
de $\ds \ds \sum f (n)$ à l'aide de la méthode des rectangles.</td><td>
Application à l'étude de sommes partielles.</td></tr><tr><td>
Séries de Riemann.</td><td></td></tr></table>


### Séries absolument convergentes à termes réels ou  complexes, suites sommables
<table>
<tr>
<td width=50%>
Convergence absolue de la série numérique $\ds \ds\sum u_n$,
encore appelée sommabilité de la suite $\ds (u_n)$.
</td><td>
Notation $\ds \ds\sum_{n=0}^{+\infty}|u_n|<+\infty$.

Le critère de Cauchy et la notion de semi-convergence
sont hors programme.</td></tr><tr><td>  

Une série numérique absolument convergente est convergente.</td><td>Somme d'une suite sommable.</td></tr><tr><td>

Si $\ds (u_n)$ est une suite complexe,
si $\ds (v_n)$ est une suite d'éléments de $\ds \R^+$,
si $\ds u_n=O(v_n)$ et si $\ds \ds{\sum v_n}$ converge,
alors $\ds \ds{\sum u_n}$ est absolument convergente donc convergente.
</td><td></td></tr></table>

## Compléments sur les séries numériques

### Comparaison Série-Intégrale
Le principe de la comparaison série/intégrale est de considérer un calcul de somme comme étant un cas particulier de calcul d'intégrale. Plutôt que d'écrire des propriétés pénibles, étudions plutôt des exemples.

Considérons la série numérique $\ds \sum\frac 1{1+n^2}$. Vous savez dire que cette série converge, mais quel est son comportement ?


!!! info inline ""
	![](images/comp_serie_integrale1.svg)

Dans le dessin ci-contre, on a représenté une fonction décroissante (le dessin commence à 1 car souvent les fonctions ne sont pas définies en $\ds 0$, ce qui n'est pas le cas pour nous). Les rectangles rouges sont définis entre $\ds k$ et $\ds k+1$ et ont pour aire $\ds f(k)*(k+1-k)=f(k)$. La fonction étant décroissante, on observe que la somme des aires des rectangles est plus grande que l'intégrale sous la courbe. On a donc

$$
\int_0^{n}f(t)\d t\le\sum_{k=0}^{n-1}f(k)
$$



!!! info inline end ""
	![](images/comp_serie_integrale2.svg)

Dans ce deuxième dessin ci-contre, les rectangles sont définis entre $\ds k-1$ et $\ds k$ et de hauteur $\ds f(k)$ (valeur à droite). Ils sont donc chacun d'aire $\ds f(k)$ et on observe, la fonction étant décroissante, que la somme de ces aires est inférieure à l'aire sous la courbe. Ainsi :

$$
\sum_{k=1}^{n}f(k)\le \int_{0}^nf(t)\d t
$$

Voilà, il ne faut pas savoir beaucoup plus sur la comparaison série/intégrale. Voyons comment cela s'applique à notre série : dans notre cas, la fonction $\ds f$ est la fonction $\ds x\mapsto \frac 1{1+x^2}$. D'après les inégalités établies précédemment, on peut écrire :

$$
\int_0^{n+1}f(t)\d t\le\sum_{k=0}^{n}f(k)\le f(0)+\int_0^{n}f(t)\d t.
$$

Or ici, $\ds \int_{0}^{x}f(t)\d t=\arctan(x)$. On a donc la relation suivante :

$$
\arctan(n+1)\le \sum_{k=0}^{n}\frac1{1+k^2}\le 1+\arctan(n)
$$

On pourra donc affirmer que la limite de la série est comprise entre $\ds \frac \pi2$ et $\ds \frac\pi2+1$. Mais c'est surtout sur le reste que les résultats sont intéressants. Soit $\ds n\in\mathbb N$ et $\ds N> n$ un deuxième entier. On a toujours, pour tout $\ds k$ entier naturel, $\ds \int_{k}^{k+1}f(t)\d t\le f(k)\le \int_{k-1}^{k}f(t)\d t$. En sommant de $\ds n+1$ à $\ds N$ dans notre cas, on obtient :

$$
\int_{n+1}^{N+1}f(t)\d t\le \sum_{k=n+1}^{N} \frac 1{1+k^2}\le \int_{n}^{N}f(t)\d t.
$$

En passant à la limite en $\ds N\to+\infty$, on a $\ds \frac \pi2-\arctan(n+1)\le R_n\le \frac\pi2-\arctan(n)$. Or $\ds n$ étant positif, on sait d'après un résultat classique sur l'arctangente, que $\ds \frac\pi2-\arctan(n)=\arctan(\frac 1n)$ et $\ds \frac\pi2-\arctan(n+1)=\arctan(\frac1{n+1})$. De plus, $\ds \arctan(\frac 1n)\sim\frac 1n$ et $\ds \arctan(\frac 1{n+1})\sim \frac 1{n+1}\sim\frac 1n$. On a donc :

$$
\arctan(\frac1{n+1})\le R_n\le \arctan(\frac1n)
$$

et par conservation de l'équivalent par encadrement, $\ds R_n\sim \frac 1n$. Ainsi, la comparaison série/intégrale nous a permis d'établir un équivalent du reste : on sait donc *comment* la série converge. Pourtant, on ne connaît pas sa limite.

!!! warning "Attention"
	la difficulté de la comparaison série/intégrale réside dans la gestion des indices !


Voyons un autre exemple : on considère la série $\ds \sum \frac 1n$ dont vous savez qu'elle diverge.

Dans ce cas, la fonction $\ds f$ sera définie par $\ds f(x)=\frac 1x$ et ce n'est pas défini en $\ds 0$. On devra donc commencer intégrales et sommes à $\ds 1$. La fonction $\ds f$ est décroissante : pour $\ds x\in [k,k+1]$, $\ds f(x)\le f(k)$ et ainsi $\ds \int_{k}^{k+1}f(t)\d t\le f(k)$. De même sur $\ds [k-1,k]$, $\ds f(x)\ge f(k-1)$ et $\ds \int_{k-1}^k f(t)\d t\le f(k-1)$. On somme alors de $\ds 1$ à $\ds n$ la première inégalité et de $\ds 2$ à $\ds n$ la seconde :

$$
\int_{1}^{n+1}f(t)\d t\le \sum_{k=1}^{n} \frac 1k\le 1+ \int_{1}^{n} f(t)\d t
$$

Or $\ds \int_{1}^{n+1}f(t)\d t=\ln(n+1)$ et $\ds \int_{1}^{n}f(t)\d t=\ln(n)$, on a donc  :

$$
\ln(n+1)\le \sum_{k=1}^{n} \frac 1k\le 1+ \ln(n)
$$

et puisque $\ds \ln(n+1)=\ln(n)+\ln(1+\frac 1n)\sim \ln(n)$, on peut affirmer, par conservation de l'équivalent par encadrement, $\ds \sum_{k=1}^{n} \frac 1k\sim \ln(n)$. Ainsi la comparaison série/intégrale permet (dans certains cas) de trouver un équivalent des sommes partielles lorsque celles-ci divergent.


!!! exemple
	=== "Énoncé"
		Calculer la limite $\ds \ds\lim_{n\to+\infty}\frac 1n\prod_{k=1}^n(3k-1)^{\frac 1n}$
	=== "Corrigé"
		Comme à chaque fois qu'il y a un produit, on passe par le log. Posons $\ds P_n=\frac 1n\prod_{k=1}^n(3k-1)^{\frac 1n}$.

		$$
		\ln(P_n) = \frac 1n\sum_{k=1}^{n}\ln(3k-1) -\ln(n)\\
		$$

		Posons $\ds f:x\mapsto \ln(3t-1)$. Cette fonction est croissante mais ça n'empêche pas de faire une comparaison série/intégrale. Sur l'intervalle $\ds t\in[k,k+1]$, $\ds f(t)\ge f(k)$ et donc $\ds \int_{k}^{k+1}f(t)\d t\ge f(k)$. De même sur $\ds t\in[k-1,k]$, $\ds f(t)\le f(k)$ et donc $\ds \int_{k-1}^kf(t)\d t\le f(k)$. On somme la première inégalité entre $\ds 1$ et $\ds n$ et la deuxième entre $\ds 2$ et $\ds n$. On obtient :

		$$
		\int_{1}^n f(t)\d t +\ln(2)\le \sum_{k=1}^{n}\ln(3k-1) \le \int_{1}^{n+1}f(t)\d t
		$$

		Or $\ds \int_1^{x}f(t)\d t = \frac 13\left[(3t-1)\ln(3t-1)-3t+1\right]_1^x=\frac 13\left((3x-1)\ln(3x-1)-3x+1-2\ln(2)+5\right)=\frac 13\left((3x-1)\ln(3x-1)-3x+C\right)$. On a donc $\ds \int_{1}^n f(t)\d t =\frac 13\left((3n-1)\ln(3n-1)-3n+C\right)$ et $\ds \int_{1}^{n+1}f(t)=\frac 13\left((3n+2)\ln(3n+2)-3n+C'\right)$. On a ainsi $\ds \ln(P_n)=\frac 1n\sum_{k=1}^{n}\ln(3k-1) -\ln(n)$ et donc :

		$$
		\underset{u_n}{\underbrace{\frac 1{3n}\left((3n-1)\ln(3n-1)-3n+C''\right)-\ln(n)}}\le\ln(P_n)\le\frac 1{3n}\left((3n+2)\ln(3n+2)-3n+C'\right)-\ln(n)
		$$

		Concentrons nous sur le premier membre :

		$$
		\begin{aligned}
		u_n&=\frac 1{3n}\left((3n-1)\ln(3n-1)-3n+C''\right)-\ln(n)\\
		&=\frac 1{3n}\left((3n-1)(\ln(3)+\ln(n)+\ln(1-\frac 1{3n})-3n+C''-3n\ln(n)\right)\\
		&=\frac1{3n}\left(3n\ln3+3n\ln(1-\frac 1{3n})-\ln(3)-\ln(n)-\ln(1-\frac 1{3n})-3n+C''\right)\\
		&=\frac1{3n}\left(3n(\ln(3)-1)+o(n)\right) \\
		&=(\ln(3)-1)+o(1)
		\end{aligned}
		$$

		et de même pour le membre de droite. On en déduit que $\ds P_n\xrightarrow[n\to+\infty]{}\exp(\ln(3)-1)=\frac 3e$.


### Établir des convergences

#### Formule de Stirling

Certaines séries sont inaccessibles avec les outils classiques de PCSI. Pour traiter les termes généraux comportant des factorielles, on introduit la :
!!! propriete "Formule de Stirling"
	Pour $\ds n\in\mathbb N$, on a l'équivalent de $\ds n!$ suivant :

	$$
	n!\sim \sqrt{2\pi n}n^ne^{-n}
	$$


!!! exemple
	=== "Énoncé"
		Quelle est la nature de la série $\ds \ds\sum \frac{n^n}{e^nn!}$ ?
	=== "Corrigé"
		D'après la formule de Stirling, $\ds \frac{n^n}{e^nn!}\sim \frac{n^n}{e^n\sqrt{2\pi n}n^ne^{-n}}=\frac{1}{\sqrt{2\pi n}}$ qui est le terme général d'une série numérique divergente (série de Riemann de paramètre $\ds \frac 12\le 1$). Donc par théorèmes de comparaison des séries à termes positifs, la série $\ds \sum \frac{n^n}{e^nn!}$ diverge.

!!! exemple
	=== "Énoncé"
		On pose $\ds u_n=\frac{(2n)!}{(2^nn!)^2}$. Quelle est la nature de la série $\ds \sum u_n$ ?
	=== "Corrigé"
		D'après la formule de Stirling, appliquée à $\ds (2n)!$ et $\ds n!$,

		$$
		\begin{aligned}
		\frac{(2n)!}{(2^nn!)^2}&\sim \frac{\sqrt{2\pi 2n}(2n)^{2n}e^{-2n}}{(2^n \sqrt{2\pi n}n^ne^{-n})^2}\\
		&\sim \frac{2\sqrt{\pi n}4^nn^{2n}e^{-2n}}{4^n 2\pi nn^{2n}e^{-2n}}\\
		&\sim \frac{1}{\sqrt{\pi n}}\\
		\end{aligned}
		$$

		qui est le terme général d'une série positive divergente. Par théorèmes de comparaison des séries à termes positifs, la série diverge.


#### Règle de d'Alembert

Vous connaissez les théorèmes de comparaison qui permettent d'établir la convergence d'une série. Parfois, le critère de d'Alembert permet de s'en sortir plus rapidement.

!!! propriete "Critère de d'Alembert"
 	Soit $\ds \sum u_n$ une série numérique, les $\ds u_n$ étant réels ou complexes et ne s'annulant pas à partir d'un certain rang. On suppose que $\ds \left|\frac{u_{n+1}}{u_n}\right|$ converge vers une limite $\ds \ell\in\mathbb R$.

	- si $\ds \ell<1$ alors la série converge ;
	
	- si $\ds \ell>1$ alors la série diverge ;
	
	- si $\ds \ell=1$ on ne peut pas conclure.

!!! preuve
	Commençons par le cas indéterminé. En effet, pour $\ds u_n=\frac 1{n^2}$ on a bien $\ds \frac{u_{n+1}}{u_n}\to 1$ et la série converge. Et pour $\ds v_n=\frac 1n$ on a aussi $\ds \frac{v_{n+1}}{v_n}\to 1$ et la série diverge. C'est donc bien un cas indéterminé.

	Traitons le premier cas. On suppose donc que $\ds \ell<1$. Il existe alors $\ds N\in\mathbb N$ tel que si $\ds n\ge N$, alors $\ds \left|\frac{u_{n+1}}{u_n}\right|<\frac{1+\ell}{2}$. On a alors pour $\ds n\ge N$ :

	$$
	\begin{aligned}
	|u_n|&=\left|\frac{u_n\times u_{n-1}\times\cdots \times u_{N+1}}{u_{n-1}\times\cdots\times u_N}\times u_N\right|\\
	&\le \left(\frac {\ell+1}2\right)^{n-N+1}u_n
	\end{aligned}
	$$

	qui est le terme général d'un série géométrique convergente (car $\ds \frac{\ell +1}{2}<1$). Par théorèmes de comparaison, la série $\ds \sum u_n$ est absolument convergente donc convergente.

	Enfin, si $\ds \ell>1$, on fait le même travail mais avec une minoration par le terme général d'une série géométrique divergente.

!!! exemple
	=== "Énoncé"
		Avec la règle de d'Alembert, étudier la convergence de la série $\ds \sum \frac{n!}{n^n}$.
	=== "Corrigé"
		Posons $\ds u_n=\frac{n!}{n^n}$. On a $\ds \frac{u_{n+1}}{u_n}=\frac{(n+1)!n^n}{n!(n+1)^{n+1}}=\frac{1}{\left(1+\frac 1n\right)^n}\to\frac 1e<1$. D'après le critère de d'Alembert, la série converge donc.


!!! exemple
	=== "Énoncé"
		Quelle est la nature de la série de terme général $\ds \sum\frac{n}{2^n}$ ?
	=== "Corrigé"
		Posons $\ds u_n=\frac{n}{2^n}$. On a $\ds \frac{u_{n+1}}{u_n}=\frac{(n+1)2^n}{2^{n+1}n}=\frac{n+1}{2n}\to \frac 12$. D'après le critère de d'Alembert, la série converge.

!!! exemple
 	=== "Énoncé"
		Quelle est la nature de la série de terme général $\ds \sum\frac{4^{n+1}((n+1)!^2}{(2n-1)!}$ ?
	=== "Corrigé"
		Posons $\ds u_n=\frac{4^{n+1}((n+1)!^2}{(2n-1)!}$. On calcule :

		$$
		\frac{u_{n+1}}{u_n}=\frac{4^{n+2}((n+2)!^2(2n-1)!}{(2n+1)!4^{n+1}((n+1)!^2}=\frac{4(n+2)^2}{(2n+1)2n}\to1.
		$$

		La règle de d'Alembert ne permet pas de conclure.

		On peut passer par la formule de Stirling pour conclure :

		$$
		\begin{aligned}
		u_n &=\frac{4^{n+1}((n+1)!^2}{(2n-1)!}\\
		&=8n(n+1)^2\frac{4^{n}((n!)^2}{(2n)!}\\
		&\sim 8n^3 \frac{4^n 2\pi n n^{2n}e^{-2n}}{\sqrt{4\pi n}(2n)^{2n} e^{-2n}}\\
		&\sim 8n^3\sqrt{n\pi}
		\end{aligned}
		$$

		Ça ne tend pas vers $\ds 0$ donc il y a divergence grossière de la série.

!!! exemple
	=== "Énoncé"
		(Règle de Raabe-Duhamel)  
		Soient $\ds (u_n)$ et $\ds (v_n)$ deux suites de réels strictement positives.  On suppose qu'à partir d'un certain rang, $\ds \frac{u_{n+1}}{u_n}\le \frac{v_{n+1}}{v_n}$. Montrer que $\ds u_n=O(v_n)$. On suppose maintenant que $\ds \frac{u_{n+1}}{u_n}=1-\frac\alpha n +o(\frac 1n)$. Montrer que si $\ds \alpha>1$ alors la série $\ds \sum u_n$ converge. Montrer que si $\ds \alpha<1$ alors $\ds \sum u_n$ diverge.
	=== "Corrigé"
		Soit $\ds n_0$ un rang à partir duquel $\ds \frac{u_{n+1}}{u_n}\le\frac{v_{n+1}}{v_n}$. On a alors pour $\ds n\ge n_0$ :

		$$
		u_n=\frac{u_n}{u_{n-1}}\times \frac{u_{n-1}}{u_{n-2}}\times\cdots\times \frac{u_{n_0+1}}{u_{n_0}}\times u_{n_0}\le\frac{v_n}{v_{n-1}}\times \frac{v_{n-1}}{v_{n-2}}\times\cdots\times \frac{v_{n_0+1}}{v_{n_0}}\times u_{n_0}\le v_n\frac{u_{n_0}}{v_{n_0}}
		$$

		On en déduit que pour $\ds n\ge n_0$, $\ds 0\le \frac{u_n}{v_n}\le \frac{u_{n_0}}{v_{n_0}}$ c'est à dire $\ds u_n=O(v_n)$.

		Supposons maintenant $\ds \alpha>1$ et $\ds \frac{u_{n+1}}{u_n}=1-\frac\alpha n+o(\frac 1n)$. Posons $\ds \beta$ un réel tel que $\ds 1<\beta<\alpha$ et $\ds v_n=\frac1{n^\beta}$. On a alors $\ds \frac{v_{n+1}}{v_n}=1-\frac\beta n+o(\frac 1n)$ et $\ds \frac{u_{n+1}}{u_n}-\frac{v_{n+1}}{v_n}\sim\frac{\beta-\alpha}n$. par conservation du signe par équivalence, puisque $\ds \beta-\alpha<0$, on a bien $\ds \frac{u_{n+1}}{u_n}\le \frac{v_{n+1}}{v_n}$ à partir d'un certain rang. Alors d'après la première question, $\ds u_n=o(\frac{1}{n^\beta})$ qui est le terme général d'une série convergente. Donc $\ds \sum u_n$ converge.

		Si $\ds \alpha<1$, on prend cette fois $\ds \beta$ tel que $\ds \alpha<\beta<1$. On a alors avec les mêmes calculs $\ds \frac{u_{n+1}}{u_n}-\frac{v_{n+1}}{v_n}\sim\frac{\beta-\alpha}{n} >0$. Donc cette fois-ci, $\ds v_n=o(u_n)$. Or ici $\ds \sum v_n$ converge ($\beta<1$) donc par comparaison, $\ds \sum u_n$ converge.

#### Théorème spécial des séries alternées

Souvent, les séries étudiées sont à termes positifs. Le théorème ou critère spécial des séries alternées permet de traiter certaines séries dont le terme général n'est pas de signe constant.

!!! theoreme "Critère spécial des séries alternées"
	Soit $\ds (u_n)$ une suite à termes positifs. Si cette suite est décroissante et converge vers $\ds 0$, alors la série $\ds \sum (-1)^nu_n$ converge.

!!! preuve
	Notons $\ds S_n$ la somme partielle n'ordre $\ds n$ de cette série. On a :

	$$
	S_{ 2n+1}-S_{2n-1}=-u_{2n+1}+u_{2n}
	$$

	Or la suite $\ds (u_n)$ est décroissante donc $\ds u_{2n}-u_{2n-1}\ge0$. On en déduit que la suite des sommes partielles d'indice impair est croissante.

	$$
	S_{2n+2}-S_{2n}=u_{2n+2}-u_{2n+1}\le0
	$$

	Donc la suite des sommes partielles d'indice pair est décroissante. Enfin, $\ds S_{2n}-S_{2n-1}=u_{2n}\to 0$. Finalement, la suite $\ds (S_{2n})$ et la suite $\ds (S_{2n+1})$ sont adjacentes. Elles convergent donc vers la même limite. Comme ce sont des suites complémentaires de la suite $\ds (S_n)$, on en déduit que la suite $\ds (S_n)$ converge.

!!! propriete "Majoration du reste d'une série alternée"
	Soit $\ds \sum{(-1)^nu_n}$ une série alternée : $\ds u_n$ est positif, décroît et tend vers $\ds 0$. Notons $\ds R_n$ le reste d'ordre $\ds n$ de cette série. Alors $\ds R_n$ est du signe de $\ds (-1)^{n+1}$ et $\ds |R_n|\le u_{n+1}$.

!!! preuve
	Soit donc une telle série. On sait d'après le CSSA qu'elle converge vers une limite $\ds \ell$. On sait donc que ses restes convergent vers $\ds 0$.
	D'après la preuve précédente, $\ds S_{2p+1}\le \ell\le S_{2n}$. On en déduit $\ds R_{2n}=\ell-S_{2n}\le 0$ : $\ds R_{2n}$ est négatif donc du signe de $\ds (-1)^{2n+1}$. On montre de même que $\ds R_{2n+1}\ge 0$. On a alors :

	$$
	|R_{2p}|=S_{2p}-\ell\le S_{2p}-S_{2p+1}=-(-1)^{2p+1}u_{2p+1}=u_{2p+1}
	$$

	Donc $\ds |R_{2p}|\le u_{2p+1}$. On montre de même que $\ds |R_{2p+1}|\le u_{2p+2}$.

!!! exemple
	=== "Énoncé"
		Montrer que la série $\ds \sum \frac{(-1)^n}{n+1}$ converge.
	=== "Corrigé"
		La série est de la forme $\ds \sum(-1)^nu_n$ avec $\ds u_n=\frac 1{n+1}$. $\ds u_n$ est donc positive, décroissante et tend vers $\ds 0$. La série est donc une série alternée et d'après le critère spécial des séries alternées, elle converge. On peut même déterminer sa limite.

		En effet, on remarque que $\ds \frac{(-1)^n}{n+1}=-(\frac{x^{n+1}}{n+1})(-1)$. Posons $\ds f_N(x)=\sum_{k=0}^N x^k$, et notons $\ds F_N$ sa primitive qui s'annule en $\ds 0$ : $\ds F_N(x)=\sum_{k=0}^{N}\frac{x^{k+1}}{k+1}$. Or $\ds f_N(x)=\frac{1-x^{k+1}}{1-x}$ et :

		$$
		\begin{aligned}
		F_N(-1) &= \int_0^{-1}\frac{1-t^{N+1}}{1-t}\d t\\
		&= -\ln(2) -\int_0^{-1} \frac{t^{N+1}}{1-t}\d t\\
		&= -\ln(2) -\frac1{N+2}\left[t^{N+2}\frac1{1-t}\right]_0^{-1}-\frac 1{N+2}\int_{0}^{-1}\frac{t^{N+2}}{(1-t)^2}d t
		\end{aligned}
		$$

		les termes en $\ds \frac 1{N+2}$ tendent vers $\ds 0$ donc $\ds F_{N}(-1)$ converge vers $\ds -\ln(2)$ et donc la série converge vers $\ds \ln(2)$.

!!! exemple
	=== "Énoncé"
		Donner une approximation de $\ds \ln(2)$ à $\ds 10^{-2}$ près.
	=== "Corrigé"
		La série précédente converge vers $\ds \ln(2)$. Les sommes partielles convergent donc vers $\ds \ln(2)$ et la qualité de l'approximation est donnée par le reste. Or pour $\ds n$ donné et d'après le CSSA, $\ds |R_n|\le \frac 1{n+1}$. En prenant $\ds n$ tel que $\ds \frac 1{n+1}\le 10^{-2}$, c'est à dire $\ds n\ge100$, on aura une approximation à $\ds 10^{-2}$ près. Une telle approximation de $\ds \ln(2)$ est donc par exemple $\ds \sum_{k=0}^{100}\frac{(-1)^n}{n+1}$.

!!! exemple
	=== "Énoncé"
		On admet que $\ds \sum_{n=0}^{+\infty}\frac{(-1)^n}{2n+1}=\frac \pi4$. Montrer que $\ds \frac 83\le \pi\le \frac{52}{15}$.
	=== "Corrigé"
		La série est une série alternée (ce qui justifie sa convergence). D'après le critère des séries alternées et ses conséquences, $\ds S_1\le \frac \pi4\le S_2$. Or $\ds S_1=\frac 1-\frac 13=\frac 23$ et $\ds S_2=1-\frac 13+\frac 15=\frac{13}{15}$. On en déduit l'inégalité voulue.

#### Produit de séries numériques

On connaît beaucoup de choses sur les combinaisons linéaires de séries numériques, mais peu de choses sur les produits :

!!! propriete "Produit de Cauchy de séries absolument convergentes"
 	Soient $\ds \sum a_n$ et $\ds \sum b_n$ deux séries absolument convergentes, on pose $\ds c_n=\sum_{k=0}^{n}a_pb_{n-p}$. Alors la série $\ds \sum c_n$ converge et $\ds \sum_{n=0}^{+\infty}c_n=\sum_{n=0}^{+\infty}a_n\times \sum_{n=0}^{+\infty}b_n$.

!!! preuve
	La preuve n'est pas au programme.


!!! exemple
	=== "Énoncé"
		On rappelle que $\ds \ds\sum_{k=0}^{+\infty}\frac {z^n}{n!}=e^z$. Montrer que $\ds e^{z_1+z_2}=e^{z_1}e^{z_2}$ pour tous nombres complexes $\ds z_1$ et $\ds z_2$.
	=== "Corrigé"
		La série $\ds \ds\sum\frac{z^n}{n!}$ est une série absolument convergente. On a donc par produit de Cauchy :

		$$
		\begin{aligned}
		e^{z_1}e^{z_2}&=\sum_{n=0}^{+\infty}\sum_{p=0}^{n}\frac{z_1^pz_2^{n-p}}{p!(n-p)!}\\
		&=\sum_{n=0}^{+\infty}\frac1{n!}\sum_{p=0}^{n}\frac{z_1^pz_2^{n-p}n!}{p!(n-p)!}\\
		&=\sum_{n=0}^{+\infty}\frac1{n!}\sum_{p=0}^n\binom npz_1^pz_2^{n-p}\\
		&=\sum_{n=0}^{+\infty}\frac 1n!(z_1+z_2)^n\\
		&=exp(z_1+z_2)
		\end{aligned}
		$$

## Hors-programme... mais bon à savoir

Voici deux propriétés d'équivalence qui sont importantes, bien que hors programme en PSI.

!!! propriete "Équivalence et restes"
	Soit $\ds (u_n)$ et $\ds (v_n)$ deux suites à termes positifs. On suppose $\ds u_n\sim v_n$ et $\ds \sum u_n$ converge. Alors $\ds \sum_{k=n+1}^{+\infty}u_k\sim\sum_{k=n+1}^{+\infty}v_k$.

!!! preuve
	Soit $\ds \varepsilon>0$. $\ds u_n\sim v_n$ donc il existe un rang $\ds n_0$ tel que si $\ds n\ge n_0$, alors $\ds -\varepsilon u_n\le u_n-v_n\le \varepsilon u_n$. Soit donc $\ds n\ge n_0$ et $\ds N\ge n$, on a en sommant de $\ds n+1$ à $\ds N$ :

	$$
	-\varepsilon \sum_{k=n+1}^{N}u_k\le \sum_{k=n+1}^{N}v_n-\sum_{k=n+1}^N u_n\le\varepsilon \sum_{k=n+1}^Nu_k
	$$

	et en passant à la limite en $\ds N$ (les restes convergent), on obtient pour $\ds n\ge n_0$ :

	$$
	-\varepsilon \sum_{k=n+1}^{+\infty}u_k\le \sum_{k=n+1}^{+\infty}v_k - \sum_{k=n+1}^{+\infty}u_k\le \varepsilon\sum_{k=n+1}^{+\infty}u_k
	$$

	c'est-à-dire $\ds \sum_{k=n+1}^{+\infty}u_k\sim\sum_{k=n+1}^{+\infty}v_k$

!!! propriete "Équivalence des sommes partielles"
	Soit $\ds (u_n)$ et $\ds (v_n)$ deux suites à termes positifs. On suppose $\ds u_n\sim v_n$ et $\ds \sum u_n$ diverge. Alors $\ds \sum_{k=0}^{N}u_k\sim\sum_{k=0}^{N}v_k$.

!!! preuve
	Soit $\ds \varepsilon>0$. $\ds u_n\sim v_n$ donc il existe un rang $\ds n_0$ tel que si $\ds n\ge n_0$, alors $\ds -\frac \varepsilon2 u_n\le u_n-v_n\le \frac \varepsilon2 u_n$.  Soit donc $\ds n\ge n_0$, on a en sommant de $\ds 0$ à $\ds n$ :

	$$
	\sum_{k=0}^{n_0-1} v_k - \sum_{k=0}^{n_0-1} u_k-\frac\varepsilon2 \sum_{k=n_0}^n u_k\le \sum_{k=0}^n v_k - \sum_{k=0}^n u_k\le \sum_{k=0}^{n_0-1} v_k - \sum_{k=0}^{n_0-1} u_k+ \frac\varepsilon2\sum_{k=n_0}^{n}u_k
	$$

	En notant $\ds A=\sum_{k=0}^{n_0-1} v_k - (1-\frac \varepsilon2)\sum_{k=0}^{n_0-1} u_k$ et $\ds B=\sum_{k=0}^{n_0-1} v_k - (1+\frac\varepsilon2)\sum_{k=0}^{n_0-1} u_k$ et en notant $\ds S_n(u)$ et $\ds S_n(v)$ les sommes partielles respectives de $\ds \sum u_n$ et $\ds \sum v_n$, on a donc, pour $\ds n\ge n_0$ :

	$$
	A-\frac\varepsilon2 S_n(u)\le S_n(v)-S_n(u)\le B+\frac \varepsilon2 S_n(u)
	$$

	Or $\ds S_n(u)\xrightarrow[n\to+\infty]{}+\infty$. Donc il existe un rang $\ds n_1$ tel que si $\ds n\ge n_1$, $\ds -\frac\varepsilon2 S_n(u)\le A$ et $\ds B\le\frac\varepsilon2 S_n(u)$. On a alors pour $\ds n\ge \max(n_0,n_1)$ :

	$$
	-\varepsilon S_n(u)\le S_n(v)-S_n(u)\le \varepsilon S_n(u)
	$$

	c'est-à-dire $\ds S_n(v)\sim S_n(u)$.

La dernière propriété, notamment, permet de se passer, dans certains cas, du Théorème de Cesàro souvent vu en première année. En effet, si $\ds u_n\to \ell\ne 0$ alors $\ds u_n\sim \ell$ et d'après la propriété d'équivalence des sommes partielles dans le cas divergent, $\ds \sum_{k=0}^{n}u_k\sim n\ell$.




## Exercices

### Issus de la banque CCINP

{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-005.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-006.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-007.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-008.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-046.md" %}


### Annales d'oraux
{% include-markdown "../../../exercicesRMS/2022/RMS2022-712.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-713.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-714.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1307.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1308.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1369.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1371.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1383.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1435.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1436.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1437.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1438.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1440.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-523.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-524.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-527.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-529.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-869.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-870.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1191.md" %}

### Centrale Python

{% include-markdown "../../../exercicesCentralePython/RMS2022-1038.md"  rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1103.md" %}
{% include-markdown "../../../exercicesCentralePython/RMS2019-1015.md" %}
{% include-markdown "../../../exercicesCentralePython/BEOS2017-3549.md"  rewrite-relative-urls=false  %}
