# Espaces vectoriels normés

Dans ce chapitre, nous travaillons avec des espaces vectoriels sur un corps $\ds \mathbb K$ avec $\ds \mathbb K=\mathbb R$ ou $\ds \mathbb C$. 

## Normes

!!! definition "Norme"
    Soit $\ds E$ un espace vectoriel réel ou complexe. Soit $\ds N$ une application de $\ds E$ dans $\ds \mathbb R^+$. $\ds N$ est une *norme* si et seulement si $\ds N$ vérifie les trois propriétés suivantes : 

    - **homogénéité :** $\ds \forall (\lambda,x)\in\mathbb K\times E,\,N(\lambda x)=|\lambda| N(x)$

    - **inégalité triangulaire :** $\ds \forall (x,y)\in E^2,\, N(x+y)\le N(x)+N(y)$

    - **séparation :** $\ds \forall x\in E,\, N(x)=0\Rightarrow x=0$

!!! remarque 
    La notation $\ds |\lambda|$ désigne la valeur absolue si $\ds \mathbb K=\mathbb R$ et le module si $\ds \mathbb K= \mathbb C$. 

!!! definition "Normes usuelles sur $\ds \mathbb K^n$"
    Soit $\ds E=\mathbb K^n$. Les applications suivantes sont des normes sur $\ds E$ : 

    - $\ds ||x||_1=\sum_{i=1}^n|x_i|$ (*norme  1*)  
    - $\ds ||x||_2=\sqrt{\sum_{i=1}^n|x_i|^2}$ (*norme 2*)  
    - $\ds ||x||_\infty=\sup_{i\in\{1,\dots,n\}}\{|x_i|\}$ (*norme infinie*)

!!! preuve
    **norme 1 :** c'est clairement une application de $\ds E$ dans $\ds \mathbb R^+$. Soit $\ds x$ et $\ds y$ deux éléments de $\ds E$. Soit $\ds \lambda\in\mathbb K$, $\ds ||\lambda x||_1=\sum_{i=1}^n|\lambda x_i|=\sum_{i=1}^n|\lambda||x_i|=\lambda ||x||_1$ donc la propriété d'homogénéité est vérifiée. $\ds ||x+y||_1=\sum_{i=1}^n|x_i+y_i|\le\sum_{i=1}^n |x_i|+|y_i|=\sum_{i=1}^n|x_i|+\sum_{i=1}^n|y_i|=||x||_1+||y||_1$ donc la propriété d'inégalité triangulaire est bien vérifiée. Enfin si $\ds N(x)=0$ alors $\ds \sum_{i=1}^n|x_i|=0$ : on a une somme nulle de termes positifs, donc tous les termes sont nuls et donc $\ds x=0$. La propriété de séparation est vérifiée. Ainsi, $\ds ||\cdot||_1$ est une norme sur $\ds E$. 

    **norme 2 :** Encore une fois c'est bien une application de $\ds E$ dans $\ds \mathbb R^+$. Soient $\ds x$ et $\ds y$ deux éléments de $\ds E$. Soit $\ds \lambda\in\mathbb K$. $\ds ||\lambda x||_2=\sqrt{\sum_{i=1}^n|\lambda x_i|^2}=\sqrt{|\lambda|^2\sum_{i=1}^n|x_i|^2}=|\lambda|\sqrt{\sum_{i=1}^n|x_i|^2}=|\lambda|\;||x_i||_2$. Donc la propriété d'homogénéité est vérifiée. 
    
    $$
    \begin{aligned}
    ||x+y||_2^2&={\sum_{i=1}^n|x_i+y_i|^2}\\
    &={\sum_{i=1}^n|x_i|^2+|y_i|^2+2|x_iy_i|}\\
    &={\sum_{i=1}^n|x_i|^2+|y_i|^2+2\sum_{i=1}^n|x_iy_i|}\\
    &\le{||x||_2^2+||y||_2^2+2\sqrt{\sum_{i=1}^n|x_i|^2\sum_{i=1}^n|y_i|^2}}&\text{Inégalité de Cauchy Schwarz}\\
    &\le||x||_2^2+||y||_2^2+2||x||\;||y||\\
    &\le (||x||+||y||)^2
    \end{aligned}
    $$

    On en déduit alors $\ds ||x+y||_2\le ||x||_2+||y||_2$, ainsi la propriété d'inégalité triangulaire est vérifiée.

    Enfin si $\ds ||x||_2=0$ alors $\ds \sum_{i=1}^n|x_i|^2=0$ et on a comme souvent une somme nulle de termes positifs, donc tous les termes sont nuls et $\ds x=0$ : la propriété de séparation est vérifiée.

    **norme infinie :** C'est évidemment une application de $\ds E$ dans $\ds \mathbb R^+$. Soient $\ds x$ et $\ds y$ deux éléments de $\ds E$ et $\ds \lambda\in\mathbb K$ qu'on prendra non nul. Soit $\ds i\in\{1,\dots,n\}$, on a $\ds |\lambda x_i|=|\lambda||x_i|\le |\lambda| \max_i|x_i|$. En passant au max à gauche, on obtient $\ds \max_i|\lambda x_i|\le|\lambda|\max_i|x_i|$. En utilisant cette inéaglité, il vient par ailleurs : $\ds |\lambda|\max_i |x_i|= |\lambda| \max_i \frac 1{|\lambda|}|\lambda x_i|\le|\lambda|\frac 1{|\lambda|}\max_i|\lambda x_i|$. On en déduit que $\ds |\lambda|\max_i|x_i|\le \max_i|\lambda x_i|$. Donc en combinant les deux inégalités, on a $\ds ||\lambda x||_\infty = |\lambda| ||x||_\infty$. La propriété d'homogénéité est bien vérifiée.

    Montrons l'inégalité tiangulaire : $\ds ||x+y||_\infty=\max_i|x_i+y_i|$. Or pour tout $\ds i$, $\ds |x_i+y_i|\le |x_i|+|y_i|\le \max_k|x_k|+\max_k|y_k|\le ||x||_\infty+||y||_\infty$. En passant au max à gauche, on obtient $\ds \max_i|x_i+y_i|\le ||x||_\infty+||y||_\infty$, c'est à dire $\ds ||x+y||_\infty\le||x||_\infty+||y||_\infty$. L'inégalité triangulaire est bien vérifiée.

    Enfin si $\ds ||x||_\infty=0$ alors pour tout $\ds i$, $\ds |x_i|\le 0$, dont on déduit que $\ds x=0$ : la propriété de séparation est bien vérifiée. 

!!! definition "Norme usuelle sur $\ds \mathcal C^0([a,b],\mathbb R)$"
    Soit $\ds f$ une application continue de $\ds [a,b]$ dans $\ds \mathbb R$. On pose $\ds ||f||_\infty=\sup_{[a,b]}|f|$. Alors $\ds ||\cdot||_\infty$ est une norme sur $\ds \mathcal C^0([a,b],\mathbb R)$. 

!!! preuve
    Tout d'abord, les fonctions étant continues sur $\ds [a,b]$ qui est un segment, la notion de $\ds sup$ a du sens et l'application est bien vérifiée.

    Commençons par montrer que pour toute partie $\ds A$ non vide de $\ds \mathbb R$ et tout réel $\ds k$ positif, $\ds \sup kA=k\sup A$. 

    Si $\ds k=0$ c'est trivial, on se concentre sur $\ds k>0$. 

    Soit $\ds x\in kA$, alors il existe $\ds a\in A$ tel que $\ds x=ka$. On a alors : $\ds x=ka\le k\sup A$ car $\ds k\ge 0$. En passant au sup à gauche, on a donc $\ds \sup kA\le k\sup A$. Si $\ds k>0$ alors $\ds \frac 1k>0$. On applique de nouveau ce résultat : $\ds k\sup A=k\sup \frac 1kkA\le k\frac1k\sup kA=\sup kA$. Par antisymétrie de la relation d'ordre, on a finalement $\ds \sup kA=k\sup A$. 
    
    De la même manière, $\ds \sup_{[a,b]} |\lambda f|=|\lambda|\sup_{[a,b]} |f|$ donc $\ds ||\lambda f||_\infty=|\lambda|||f||_\infty$ : la propriété d'homogénéité est vérifiée. 

    Soient $\ds f$ et $\ds g$ deux fonctions continues sur $\ds [a,b]$. soit $\ds x\in [a,b]$, on a $\ds |(f+g)(x)|=|f(x)+g(x)|\le |f(x)|+|g(x)|\le \sup_{[a,b]}|f|+\sup_{[a,b]}|g|$. En passant au sup à gauche, on obtient $\ds \sup_{[a,b]}|f+g|\le\sup_{[a,b]}|f|+\sup_{[a,b]}|g|$ c'est à dire $\ds ||f+g||_\infty\le ||f||_\infty+||g||_\infty$. L'inégalité triangulaire est donc bien vérifiée.

    Enfin, si $\ds ||f||_\infty=0$ alors pour tout $\ds x\in[a,b]$, $\ds |f(x)|\le 0$ et donc $\ds f$ est nulle : la propriété de séparation est vérifiée.



!!! definition "Espace vectoriel normé"
    Un espace vectoriel sur $\ds \mathbb R$ ou $\ds \mathbb C$ muni d'une norme est appelé *espace vectoriel normé*.

!!! remarque
    Nous avons démontré dans le chapitre précédent que la norme associée à un produit scalaire sur un espace préhilbertien réel est ... une norme

!!! definition "Norme usuelle sur $\ds \mathcal M_n(\mathbb R)$"
    Soit $\ds A\in\mathcal M_n(\mathbb R)$, l'application $\ds N$ définie de $\ds \mathcal M_n(\mathbb R)$ dans $\ds \mathbb R$ par $\ds N(A)=\tr(A^TA)$ est une norme. De plus, pour tout couple $\ds A,B$ de matrices de $\ds \mathcal M_n(\mathbb R)$, $\ds ||AB||\le||A||||B||$

!!! preuve
    Cette norme est la norme associée au produit scalaire canonique donc elle est bien une norme. Ce qui nous intéresse ici est l'inégalité. Soient donc $\ds A$ et $\ds B$ deux matrices de $\ds \mathcal M_n(\mathbb R)$.  On rappelle que pour une matrice $\ds M$ carrée, $\ds ||M||^2=\sum_{i,j}m_{ij}^2$, donc ici :

    $$
    \begin{aligned}
    ||AB||^2 &= \sum_{ij}(AB)_{ij}^2\\
    &= \sum_{ij}\left(\sum_{k=1}^n a_{ik}b_{kj}\right)^2\\
    &\le \sum_{ij}\left(\sum_{k=1}^n a_{ik}^2\sum_{k=1}^nb_{kj}^2\right)&\text{par Cauchy Schwarz}\\
    &= \sum_{i,k}a_{ik}^2\sum_{j,k}b_{kj}^2\\
    &= ||A||^2||B||^2
    \end{aligned}
    $$

    donc $\ds ||AB||\le ||A||||B||$.


!!! definition "Vecteur unitaire"
    Soit $\ds E$ un espace vectoriel normé. Un vecteur $\ds x$ de $\ds E$ est dit *unitaire* si et seulement si $\ds ||x||=1$.

!!! definition "Distance associée"
    Soit $\ds E$ un espace vectoriel normé, l'application $\ds d:(x,y)\mapsto||x-y||$ est appelée *distance associée à la norme*.

!!! definition "Boules, sphère"
    - La *boule ouverte* de centre $\ds w$ et de rayon $\ds r$ est l'ensemble $\ds \mathcal B(w,r)=\{x\in E|d(x,w)<r\}$.

    - La *boule fermée* de centre $\ds w$ et de rayon $\ds r$ est l'ensemble $\ds \mathcal B_F(w,r)=\{x\in E|d(x,w)\le r\}$.

    - La *sphère* de centre $\ds w$ et de rayon $\ds r$ est l'ensemble $\ds S(w,r)=\{x\in E|d(x,w)=r\}$. 

!!! remarque 
    $\ds S(w,r)=B_f(w,r)\setminus B(w,r)$

!!! exemple
    === "Énoncé"
        Dessiner, pour les trois normes usuelles sur $\ds \mathbb R^2$ la boule unité dans le plan. 
    === "Corrigé"
        Dans l'ordre pour les normes $\ds 1$, $\ds 2$ et $\ds \infty$ :
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="646px" viewBox="-0.5 -0.5 646 216" style="max-width:100%;max-height:216px;"><defs/><g><ellipse cx="321.99" cy="107" rx="69.375" ry="69.375" fill="#f5f5f5" stroke="#666666" pointer-events="none"/><path d="M 322 207 L 322 13.37" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 322 8.12 L 325.5 15.12 L 322 13.37 L 318.5 15.12 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 222 107 L 415.63 107" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 420.88 107 L 413.88 110.5 L 415.63 107 L 413.88 103.5 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 107 37 L 177 107 L 107 177 L 37 107 Z" fill="#f5f5f5" stroke="#666666" stroke-miterlimit="10" pointer-events="none"/><path d="M 107 207 L 107 13.37" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 107 8.12 L 110.5 15.12 L 107 13.37 L 103.5 15.12 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 7 107 L 200.63 107" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 205.88 107 L 198.88 110.5 L 200.63 107 L 198.88 103.5 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><rect x="467" y="37" width="140" height="140" fill="#f5f5f5" stroke="#666666" pointer-events="none"/><path d="M 537 207 L 537 13.37" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 537 8.12 L 540.5 15.12 L 537 13.37 L 533.5 15.12 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 437 107 L 630.63 107" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/><path d="M 635.88 107 L 628.88 110.5 L 630.63 107 L 628.88 103.5 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="none"/></g></svg>

!!! definition "Partie convexe"
    Une partie $\ds A$ d'un espace vectoriel normé $\ds E$ est dite *convexe* si et seulement si, pour tout couple $\ds x,y$ d'éléments de $\ds E$ et tout réel $\ds \lambda\in[0,1]$, $\ds (1-\lambda)x+\lambda y\in A$. L'ensemble $\ds \{(1-\lambda)x+\lambda y,\ \lambda\in[0,1]\}$ est le segment d'extrémités $x$ et $y$.

!!! propriete "Les boules sont convexes"
    Soit $\ds E$ un espace vectoriel normé et $\ds \mathcal B(w,r)$ une boule ouverte de centre $\ds w$ et de rayon $\ds r$. Alors $\ds \mathcal B(w,r)$ est une partie convexe de $\ds E$. 

    De même avec les boules fermées.

!!! preuve
    Soit $\ds x$ et $\ds y$ deux éléments de $\ds \mathcal B(w,r)$. Soit $\ds \lambda\in[0,1]$. Alors $\ds ||(1-\lambda)x+\lambda y-w||=||(1-\lambda)(x-w)+\lambda (y-w)||\le (1-\lambda)||x-w||+\lambda||y-w||< (1-\lambda)r+\lambda r<r$. Ainsi $\ds (1-\lambda)x+\lambda y$ est bien un élément de $\ds \mathcal B(w,r)$ et ainsi $\ds \mathcal B(w,r)$ est une partie convexe.


!!! exemple 
    === "Énoncé"
        Soit $\ds E$ un espace vectoriel normé, et $\ds F$ un sous-espace de $\ds E$. Montrer que $\ds F$ est convexe.
    === "Corrigé"
        Soient $\ds x$ et $\ds y$ deux éléments de $\ds F$. Soit $\ds \lambda\in[0,1]$ alors $\ds \lambda x+(1-\lambda)y$ est une combinaison linéaire d'éléments de $\ds F$ donc est dans $\ds F$ : $\ds F$ est convexe.


!!! definition "Caractère borné"
    Soit $\ds E$ un espace vectoriel normé. 

    - une partie $\ds A$ de $\ds E$ est bornée s'il existe $\ds M\in\mathbb R^+$ tel que $\ds \forall a\in A,\,||a||\le M$.

    - une suite $\ds (u_n)_{n\in\mathbb N}$ d'éléments de $\ds E$ est bornée s'il existe $\ds M\in\mathbb R^+$ tel que $\ds \forall n\in\mathbb N,\,||u_n||\le M$.

    - une fonction $\ds f$ d'un ensemble $\ds X$ à valeurs dans $\ds E$ est dite bornée s'il existe $\ds M\in\mathbb R^+$ tel que $\ds \forall x\in X,\,||f(x)||\le M$.


!!! exemple
    === "Énoncé"
        On munit $\ds \mathcal M_n(\mathbb R)$ de sa norme usuelle $\ds ||A||=\tr(A^\top A)$. Montrer que $\ds O(n)$ est borné.
    === "Corrigé"
        Soit $\ds A\in O(n)$, alors $\ds A^\top A=I_n$ donc $\ds ||A||^2=n$ ou encore $\ds ||A||=\sqrt n$. Or $\ds n$ est fixé donc $\ds O(n)$ est borné par $\ds \sqrt n$. 


## Suites d'éléments d'un espace vectoriel normé

La notion de *suite* d'éléments d'un espace vectoriel normé est la même que dans $\ds \mathbb R$. On retrouve d'ailleurs des notions similaires.

!!! definition "Convergence, divergence d'une suite"
    Soit $\ds E$ un espace vectoriel normé, soit $\ds {(u_n)}_{n\in\mathbb N}$ une suite d'éléments de $\ds E$. On dit que $\ds u$ est convergente s'il existe $\ds \ell\in E$ tel que $\ds ||u_n-\ell||\xrightarrow[n\to+\infty]{}0$. Si un tel $\ds \ell$ n'existe pas, on dira que la suite est divergente. 

!!! warning "Attention"
    La *divergence vers $\ds \pm\infty$* n'a pas de sens dans un espace normé quelconque.

!!! exemple
    === "Énoncé"
        Soit $\ds E=\mathcal M_n(\mathbb R)$ et $\ds ||A||=\sqrt{\tr(A^\top A)}$. Soit $\ds A_k$ une suite d'éléments de $\ds E$ dont on suppose qu'elle converge vers une matrice $\ds B$. Montrer que pour tout couple $\ds (i,j)$, $\ds (A_k)_{ij}$ converge vers $\ds B_{ij}$. 
    === "Corrigé"
        On rappelle que $\ds \tr(A^\top A)=\sum_{1\le i, j\le n}a_{ij}^2$. Donc ici, $\ds ||A_k-B||^2=\sum_{1\le i,j\le n}((A_k)_{ij}-B_{ij})^2$. C'est une somme finie de termes positifs qui converge vers $\ds 0$ donc chacun des termes converge vers $\ds 0$. On en déduit que pour tout $\ds (i,j)$, $\ds (A_k)_{ij}\xrightarrow[k\to+\infty]{}B_{ij}$. 

        Ainsi, la convergence vers $\ds B$ est une convergence *terme à terme*. 


!!! exemple
    === "Énoncé"
        Soit $\ds A$ une matrice diagonalisable telle que toutes ses valeurs propres sont de module strictement inférieur à $\ds 1$. Montrer que $\ds (A^k)_{k\in\mathbb N}$ converge et préciser sa limite.
    === "Corrigé"
        A est diagonalisable. Soit $\ds D$ diagonale et $\ds P$ inversible telles que $\ds A=P^{-1}DP$. On a $\ds A^k=P^{-1}D^kP$. Or $\ds D^k=\mathrm{diag}(\lambda_1^k,\dots,\lambda_n^k)\xrightarrow[k\to+\infty]{0_n}$. On en déduit, par unicité de la limite que $\ds A^k$ converge vers $\ds 0$. 

!!! propriete "Unicité de la limite"
    Soit $\ds E$ un espace vectoriel normé et $\ds {(u_n)}_{n\in\mathbb N}$ une suite d'éléments de $\ds E$. Si il existe $\ds \ell_1$ et $\ds \ell_2$ tels que $\ds ||u_n-\ell_1||\to 0$ et $\ds ||u_n-\ell_2||\to 0$ alors $\ds \ell_1=\ell_2$.

!!! preuve
    On a par inégalité triangulaire $\ds ||\ell_1-\ell_2||\le ||\ell_1-u_n||+||\ell_2-u_n||$. Chacun des termes converge vers $\ds 0$ donc la suite $\ds (||\ell_1-\ell_2||)_{n\in\mathbb N}$ est une suite constante qui converge vers $\ds 0$ : cette suite est constante égale à $\ds 0$ et ainsi $\ds ||\ell_1-\ell_2||=0$. Par propriété de séparation, $\ds \ell_1-\ell_2=0$ c'est à dire $\ds \ell_1=\ell_2$. 

!!! propriete "Opérations sur les limites"
    Soit $\ds E$ un espace vectoriel normé, soient $\ds {(u_n)}_{n\in\mathbb N}$ et $\ds {(v_n)}_{\mathbb N}$ deux suites d'éléments de $\ds E$ et $\ds \lambda$ et $\ds \mu$ deux éléments de $\ds \mathbb K$. On suppose que $\ds u$ converge vers $\ds \ell_1$ et que $\ds v$ converge vers $\ds \ell_2$. Alors $\ds \lambda u+\mu v$ converge vers $\ds \lambda \ell_1+\mu\ell_2$. 

!!! warning "Attention"
    Il n'y a a priori pas de propriété sur l'inverse ou le produit, puisqu'il n'y a pas de multiplication dans un espace vectoriel normé.

!!! preuve
    Par linéarité et homogénéité, 

    $$
    ||\lambda u_n+\mu v_n -(\lambda\ell_1+\mu\ell_2)||=||\lambda(u_n-\ell_1)+\mu(v_n-\ell_2)||\le |\lambda|||u_n-\ell_1||+|\mu|||v_n-\ell_2||\to 0
    $$

    d'après les propriétés des limites sur $\ds \mathbb R$. Ainsi $\ds \lambda u_n+\mu v_n$ converge vers $\ds \lambda \ell_1+\mu\ell_2$. 

!!! propriete "caractère borné"
    Toute suite convergente d'un espace vectoriel normé est bornée.

!!! preuve
    Soit $\ds {(u_n)}_{n\in\mathbb N}$ une suite convergente d'un espace vectoriel normé $\ds E$. Soit $\ds \ell\in E$ sa limite. On a alors pour tout entier naturel $\ds n$, $\ds ||u_n||=||u_n-\ell+\ell||\le ||u_n-\ell||+||\ell||$. Or $\ds {(||u_n-\ell||)}_{n\in\mathbb N}$ est une suite réelle qui converge vers $\ds 0$ : elle est bornée. Soit $\ds M_0\in\mathbb R$ tel que pour tout entier naturel $\ds n$, $\ds ||u_n-\ell||\le M_0$. On a alors, toujours pour tout entier naturel $\ds n$, $\ds ||u_n||\le M_0+||\ell||$ et donc en posant $\ds M=M_0+||\ell||$, $\ds \forall n\in\mathbb N,\, ||u_n||\le M$ : la suite est bornée.

!!! definition "Rappel - suite extraite"
    Soit $\ds E$ un espace vectoriel normé, soit $\ds {(u_n)}_{n\in\mathbb N}$ et $\ds {(v_n)}_{n\in\mathbb N}$ deux suites d'éléments de $\ds E$. On dit que $\ds v$ est une suite extraite de $\ds u$ s'il existe $\ds \varphi:\mathbb N\to \mathbb N$ strictement croissante telle que pour tout entier naturel $\ds \mathbb N$, $\ds v_n=u_{\varphi(n)}$. 

!!! propriete "convergence des suites extraites"
    Toute suite extraite d'une suite convergente est convergente.

!!! preuve
    C'est très simple : si $\ds v$ est extraite de $\ds u$  alors $\ds ||v_n-\ell||$ est une suite extraite de $\ds ||u_n-\ell||$. On utilise le résultat sur les suites réelles : $\ds ||u_n-\ell||$ converge vers $\ds 0$ donc $\ds ||v_n-\ell||$ converge vers $\ds 0$ et ainsi $\ds v_n\to \ell$. 

!!! warning "Attention" 
    il ne suffit pas d'avoir l'existence d'une suite extraite pour pouvoir conclure à la convergence de la suite. En effet, la suite de terme général d'ordre $\ds n$ $\ds (-1)^n$ ne converge pas et pourtant, la suite des termes impairs est convergente. 



## Comparaison des normes

!!! definition "Normes équivalentes"
    Soit $\ds E$ un $\ds \mathbb K$-espace vectoriel. Soient $\ds N_1$ et $\ds N_2$ deux normes sur $\ds E$. On dit que $\ds N_1$ et $\ds N_2$ sont *équivalentes* si et seulement si il existe deux réels strictement positifs $\ds \alpha$ et $\ds \beta$ tels que pour tout $\ds x\in E$, 

    $$
    \alpha N_1(x)\le N_2(x)\le \beta N_1(x).
    $$

!!! exemple
    === "Énoncé"
        Montrer que sur $\ds \mathbb R^n$, la norme infinie et la norme 2 sont équivalentes.
    === "Corrigé"
        Soit $\ds x\in E$. On a $\ds ||x||^2_2=\sum_{i=1}^n|x_i|^2$. Or chaque $\ds |x_i|$ est inférieur à $\ds \max_i|x_i|=||x||_\infty$. Donc $\ds ||x||^2_2\le n||x||^2_\infty$ et donc $\ds ||x||_2\le\sqrt n||x||_\infty$. Par ailleurs, $\ds ||x||_\infty=\max_i|x_i|=\sqrt{\max_i |x_i|^2}\le \sqrt{\sum_{i=1}^n |x_i|^2}=||x||_2$. Finalement on a $\ds ||x||_\infty\le ||x||_2\le\sqrt n||x||_\infty$ : les deux normes sont équivalentes.

!!! exemple 
    === "Énoncé"
        Montrer que sur $\ds \mathbb R^n$ la norme 1 et la norme 2 sont équivalentes.
    === "Corrigé"
        Soit $\ds x\in E$, $\ds ||x||_1=\sum_{i=1}^n |x_i|=\sum_{i=1}^n 1\times |x_i|\underset{C.S.}\le \sqrt n\sqrt{\sum_{i=1}^n x_i^2}\le \sqrt n||x||_2$.

        Par ailleurs, $\ds ||x||_2^2=\sum_{i=1}^n |x_i|^2\le \sum_{i=1}^n|x_i|^2+2\sum_{i\ne j}|x_i||x_j|=\left(\sum_{i=1}^n|x_i|\right)^2=||x||1^2$ d'où $\ds ||x_2||\le ||x_1||$. On a bien montré que $\ds ||\cdot||_2$ et $\ds ||\cdot||_1$ sont équivalentes. 


!!! exemple
    === "Énoncé"
        Montrer que sur $\ds \mathbb R^n$, la norme 1 et la norme infinie sont équivalentes.
    === "Corrigé"
        Soit $\ds x\in E$, $\ds ||x||_1=\sum_{i=1}^n|x_i|\le \sum_{i=1}^n||x||_\infty\le n||x||_\infty$. Par ailleurs, $\ds ||x||_\infty=\max_i|x_i|\le\sum_{i=1}^n|x_i|=||x||_1$. On en déduit que $\ds ||\cdot||_\infty$ et $\ds ||\cdot||_1$ sont équivalentes. 

!!! propriete "Propriétés des normes équivalentes"
    Soit $\ds E$ un espace vectoriel muni de deux normes $\ds N_1$ et $\ds N_2$ équivalentes. Alors toute partie (et donc suite, application) bornée pour $\ds N_1$ est bornée pour $\ds N_2$ et réciproquement.

    De plus, toute suite convergente pour $\ds N_1$ est convergente pour $\ds N_2$ avec la même limite.

!!! preuve
    Commençons par traduire l'équivalence des normes : soient $\ds \alpha$ et $\ds \beta$ deux réels strictement positifs tels que $\ds \alpha N_2\le N_1\le \beta N_2$. 

    Soit $\ds A$ une partie bornée pour $\ds N_1$. Soit $\ds M$ tel que pour tout $\ds a\in A$, $\ds N_1(a)\le M$. On a alors $\ds N_2(a)\le \frac 1\alpha N_1(a)\le \frac M\alpha$. Ainsi $\ds A$ est bornée pour $\ds N_2$. On peut faire la même chose avec une suite bornée, une fonction bornée.

    Soit maintenant $\ds u$ une suite convergente pour $\ds N_1$, de limite $\ds \ell$. On a alors $\ds N_2(u_n-\ell)\le \frac 1\alpha N_1(u_n-\ell)\xrightarrow[n\to+\infty]{}0$. Donc $\ds N_2(u_n-\ell)\to 0$ et ainsi la suite $\ds u$ converge vers $\ds \ell$ pour la norme $\ds N_2$. 

!!! exemple
    === "Énoncé"
        On pose $\ds E=\mathbb R[X]$ et on définit sur $\ds E$ deux normes : pour un polynôme $\ds P\in\mathbb R[X]$, on pose $\ds N_1(P)=\sqrt{\int_0^1P^2(t)\d t}$ et $\ds N_2(P)=|P(0)|+N_1(P')$. Montrer que ce sont deux normes, puis qu'elles ne sont pas équivalentes.
    === "Corrigé"
        La norme $\ds N_1$ est la norme des fonctions continues sur $\ds [0,1]$. En découlent la propriété d'homogénéité et d'inégalité triangulaire. La séparation, pas tout à fait. En effet, Si $\ds N_1(P)=0$ alors $\ds \forall x\in [0,1],\, P(x)=0$. Il faut pousser un peu pour avoir $\ds P=0$ : $\ds P$ admet tous les réels de $\ds [0,1]$ comme racine. Or un polynôme qui a une infinité de racines est obligatoirement le polynôme nul. Donc $\ds P=0$. 

        Concentrons nous sur $\ds N_2$ qui est évidemment une applicatoin de $\ds E$ dans $\ds \mathbb R^+$. On utilise les propriétés de $\ds N_1$. Soit $\ds \lambda\in\mathbb R$ et $\ds P,Q$ de polynômes de $\ds \mathbb R[X]$. On a :
        
        $$
        N_2(\lambda P)=|(\lambda P)(0)|+N_1((\lambda P)')=|\lambda||P(0)|+|\lambda|N_1(P')=|\lambda|N_2(P)
        $$

        Donc la propriété d'homogénéité est vérifiée. 

        De plus :

        $$N_2(P+Q)=|P(0)+Q(0)|+N_1(P'+Q')\le |P(0)|+|Q(0)|+N_1(P)+N_1(Q)=N_2(P)+N_2(Q)
        $$

        donc l'inégalité triangulaire est vérifiée.

        Enfin, si $\ds N_2(P)=0$ alors $\ds |P(0)|=0$ et $\ds N_1(P')=0$. Par caractère défini positif de $\ds N_1$, on a donc $\ds P'=0$ et donc $\ds P$ constant. Mais $\ds P(0)=0$ donc $\ds P=0$ : $\ds N_2$ est bien définie positive.

        Ainsi $\ds N_1$ et $\ds N_2$ sont bien des normes sur $\ds E$. Prenons la suite de polynômes $\ds P_n=X^n$ pour $\ds n\ge 1$. On calcule : $\ds N_1(P_n)=\sqrt{\int_0^1t^{2n}\d t}=\frac 1{\sqrt{2n+1}}$ et $\ds N_2(P_n)=0+\sqrt{\int_0^1 n^2t^{2n-2}\d t}=\frac{n}{\sqrt {2n-1}}$. Ainsi pour $\ds N_1$, $\ds P_n$ converge vers $\ds 0$ et est donc bornée alors qu'elle n'est pas bornée pour $\ds N_2$ : les deux normes ne sont pas équivalentes. 

!!! exemple
    === "Énoncé"
        Soit $\ds E=\mathcal C([0,1],\mathbb R)$, on définit $\ds ||f||_\infty=\sup_{[0,1]}|f|$ et $\ds ||f||_1=\int_0^1|f(t)|\d t$. On admet que ce sont bien des normes. Sont-elles équivalentes ? 
    === "Corrigé"
        Soit $\ds f\in E$, alors $\ds ||f||_1=\int_0^1|f(t)|\d t\le \int_0^1||f||_\infty \d t=||f||_\infty$. 

        Prenons maintenant $\ds f_n(x)=n\times x^n$. Alors $\ds ||f||_\infty=n$ et $\ds \int_0^1|f(t)|\d t=1$.  La suite des $\ds f_n$ est donc bornée pour $\ds ||\cdot ||_1$ mais pas pour $\ds ||\cdot||_\infty$ : les deux normes ne sont pas équivalentes.
## Topologie d'un espace vectoriel normé

Dans toute cette partie on prend $\ds E$ un espace vectoriel normé.

!!! definition "Point intérieur"
    Soit $\ds X$ une partie de $\ds E$. Un élément $\ds x\in X$ est un point intérieur à $\ds X$ si et seulement si il existe un réel $\ds r$ tel que $\ds \mathcal B(x,r)\subset X$. 

!!! definition "Ouvert d'un espace normé"
    Une partie $\ds X$ de $\ds E$ est un ouvert de $\ds E$ si tout point de $\ds X$ est un point intérieur à $\ds X$. 

!!! propriete "Boules ouvertes"
    Les boules ouvertes de $\ds E$ sont des ouverts de $\ds E$.

!!! preuve
    Soit $\ds \mathcal B$ une boule ouverte de $\ds E$ de centre $\ds w$ et de rayon $\ds r$. Soit $\ds x\in \mathcal B$. Notons $\ds d=||w-x||$ et $\ds s=\frac {r-d}2$. Montrons que $\ds \mathcal B(x,s)\subset\mathcal B(w,r)$. Soit $\ds z\in \mathcal B(x,s)$, on a $\ds ||w-z||\le ||w-x||+||x-z||\le d+\frac{r-d}{2}=\frac{r+d}{2}\le \frac{r+r}{2}=r$. On en déduit que $\ds z\in\mathcal B(w,r)$ et donc $\ds \mathcal B(x,s)\subset \mathcal B(w,r)$.

!!! propriete "Stabilité des ouverts par réunion"
    Soit $\ds \mathcal O$ un ensemble d'ouverts. Alors $\ds \bigcup_{O\in\mathcal O}O$ est un ouvert. 

!!! preuve
    Notons $\ds \mathbb O=\bigcup_{O\in\mathcal O}O$. Soit $\ds x\in \mathbb O$, alors il existe $\ds O\in\mathcal O$ tel que $\ds x\in O$. $\ds O$ est un ouvert donc il existe un réel positif $\ds r$ tel que $\ds \mathcal B(x,r)\subset O$. Or $\ds O\in \mathbb O$ donc $\ds \mathcal B(x,r)\subset \mathbb O$. On en déduit que $\ds \mathbb O$ est bien un ouvert.

!!! propriete "stabilité par intersection finie"
    Soit $\ds \{O_1,\dots,O_p\}$ un ensemble fini d'ouverts de $\ds E$. Alors $\ds \bigcap_{i=1}^p O_i$ est un ouvert de $\ds E$. 

!!! preuve
    Notons $\ds \mathbb O=\bigcap_{i=1}^p O_i$. Soit $\ds x\in\mathbb O$. Alors $\ds x$ appartient à chacun des $\ds O_i$. Or chacun des $\ds O_i$ est un ouvert : il existe donc $\ds r_i$ tel que $\ds \mathcal B(x,r_i)\subset O_i$. Notons $\ds r=\min_i(r_i)$ alors pour tout $\ds i$, $\ds \mathcal B(x,r)\subset \mathcal B(x,r_i)\subset O_i$. On en déduit que $\ds \mathcal B(x,r)\subset\mathbb O$ et donc $\ds \mathbb O$ est un ouvert.


!!! definition "Fermé d'un espace normé"
    Soit $\ds X$ une partie de $\ds E$. $\ds X$ est un fermé de $\ds E$ si et seulement si le complémentaire de $\ds X$ est un ouvert.

!!! propriete "Caractérisation séquentielle des fermés"
    $\ds X$ est un fermé de $\ds E$ si et seulement si les suites d'éléments de $\ds X$ qui convergent dans E ont leur limite dans $\ds X$. 

!!! preuve
    Il y a deux sens. Supposons que $\ds X$ est fermé. Alors $\ds E\setminus X$ est un ouvert. Soit $\ds (x_n)$ une suite d'éléments de $\ds X$ qui converge vers une limite $\ds \ell$. Montrons que $\ds \ell$ n'est pas dans $\ds E\setminus X$ : pour cela, il suffit de montrer qu'aucune boule centrée en $\ds \ell$ n'est inclue dans $\ds E\setminus X$. Soit $\ds r>0$, $\ds x_n$ converge vers $\ds \ell$ donc par définition il existe $\ds n_0$ tel que si $\ds n\ge n_0$, $\ds ||x_n-\ell||<r$. Cette condition implique que si $\ds n\ge n_0$, alors $\ds x_n\in\mathcal B(\ell,r)$ donc $\ds \mathcal B(\ell,r)\not\subset E\setminus X$. Ainsi, $\ds \ell$ n'est pas un point intérieur de $\ds E\setminus X$ qui est un ouvert : $\ds \ell\notin E\setminus X$. 

    Pour la réciproque, on passe par la contraposée : on suppose que $\ds X$ n'est pas fermé et on montre l'existence d'une suite d'éléments de $\ds x$ convergente mais qui ne converge pas dans $\ds X$. 

    $\ds X$ n'est pas fermé donc $\ds E\setminus X$ n'est pas un ouvert : il existe un élément $\ds y\in E\setminus X$ tel que toute boule centrée en $\ds y$ rencontre $\ds X$. Aini, pour $\ds n>0$, il existe $\ds x_n\in X$ tel que $\ds x_n\in\mathcal B(y,\frac 1n)$. On construit ainsi une suite d'éléments de $\ds X$ qui converge vers $\ds \ell\notin X$. Cela conclut la démonstration. 

!!! exemple
    === "Énoncé"
        Montrer que $\ds \mathcal O(n)$ est un fermé de $\ds \mathcal M_n(\mathbb K)$.
    === "Corrigé"
        Soit $\ds A_n$ une suite d'éléments de $\ds \mathcal O(n)$ qui converge vers une matrice $\ds A$. Alors $\ds A_n^\top A_n$ converge évidemment vers $\ds A^\top A$. Or $\ds A_n^\top A_n=I_n$ donc la suite $\ds A_n^\top A_n$ est une suite constante. Par unicité de la limite, $\ds A^\top A=I_n$ et donc $\ds A\in \mathcal O(n)$. 

!!! propriete "Les boules fermées sont fermées"
    Toute boule fermée est fermée.

!!! preuve
    On utilise la caractérisation séquentielle. Soit $\ds \mathcal B_F(w,r)$ une boule fermée. Soit $\ds (x_n)$ une suite d'éléments de $\ds \mathcal B_F(w,r)$ qui converge vers une limite $\ds \ell$. On a donc pour tout $\ds n$, $\ds ||x_n-w||\le r$. On en déduit $\ds ||\ell-w||\le ||\ell-x_n||+||x_n-w||\le||\ell -x_n||+r$. Or $\ds ||\ell-x_n||\to 0$ Donc par passage à la limite dans l'inégalité, $\ds ||\ell-w||\le  r$ et donc $\ds \ell\in\mathcal B_F(w,r)$ : $\ds \mathcal B_F(w,r)$ est un fermé.

!!! propriete "Les sphères sont fermées"
    Toute sphère est fermée.

!!! preuve
    Encore une fois on utilise la caractérisation séquentielle. Soit $\ds S=S(w,r)$ une sphère de $\ds E$. Soit $\ds (x_n)$ une suite d'éléments de $\ds S$ qui converge vers une limite $\ds \ell$. Soit $\ds \varepsilon>0$, il existe $\ds n_0$ tel que si $\ds n\ge n_0$, $\ds ||x_n-\ell||\le \varepsilon$.

    Soit un tel $\ds n$, $\ds ||\ell-w||=||\ell-x_n+x_n-w||\le||\ell-x_n||+||x_n-w||\le \varepsilon +r$. De même, $\ds r=||x_n-w||\le||x_n-\ell||+||\ell-w||\le \varepsilon +||\ell-w||$. On a donc, pour tout $\ds \varepsilon>0$, $\ds -\varepsilon\le ||\ell-w||-r\le \varepsilon$. On en déduit que $\ds ||\ell-w||=r$ et donc $\ds \ell\in S$. 


!!! exemple
    === "Énoncé"
        Montrer que dans $\ds \mathbb R$ muni de la norme de la valeur aboslue, $\ds \mathbb R$ est à la fois ouvert et fermé.
    === "Corrigé"
        Soit $\ds x\in \mathbb R$, prenons $\ds r=1$ alors $\ds \mathcal B(x,r)=]x-1,x+1[\subset \mathbb R$ donc $\ds \mathbb R$ est un ouvert.

        Soit $\ds (x_n)$ une suite d'éléments de $\ds \mathbb R$ qui converge vers une limite $\ds \ell$, alors $\ds \ell\in\mathbb R$ et donc $\ds \mathbb R$ est un fermé.

        Ainsi, *fermé* et *ouvert* ne sont pas des notions complémentaires.


!!! propriete "Stabilité par réunion finie"
    Soit $\ds \{F_1,\dots,F_n\}$ un ensemble fini de fermés de $\ds E$. Alors $\ds \bigcup_{i=1}^n F_i$ est un fermé de $\ds E$. 
    
!!! preuve
    Notons $\ds \mathbb F=\bigcup_{i=1}^n F_i$. Soit $\ds (x_k)$ une suite d'éléments de $\ds \mathbb F$ qui converge vers $\ds \ell$. Montrons que $\ds \ell\in\mathbb F$. Soit $\ds i\in\{1,\dots,n\}$, alors pour tout entier $\ds k$, $\ds x_k\in F_i$. Ainsi la suite $\ds (x_k)$ est une suite convergente d'éléments de $\ds F_i$ et $\ds F_i$ est fermé donc $\ds \ell\in F_i$. Ainsi $\ds \ell$ est dans tout $\ds F_i$ et donc dans $\ds \mathbb F$ : $\ds \mathbb F$ est un fermé de $\ds E$.


!!! propriete "Stabilité par intersection quelconque"
    Soit $\ds \mathcal F$ un ensemble de fermés de $\ds E$. Alors $\ds \bigcap_{F\in\mathcal F}F$ est un fermé. 

!!! preuve
    Notons $\ds \mathbb F=\bigcap_{F\in\mathcal F}F$. Soit $\ds (x_n)$ une suite d'éléments de $\ds \mathbb F$ qui converge vers une limite $\ds \ell$. Soit $\ds F\in\mathcal F$, $\ds (x_n)$ est une suite d'éléments de $\ds \mathbb F$ donc de $\ds F$. Or elle converge vers $\ds \ell$ et $\ds F$ est fermé donc $\ds \ell\in F$. Ainsi $\ds \ell$ est dans chaque $\ds F$ et donc dans $\ds \mathbb F$ : $\ds \mathbb F$ est un fermé. 

!!! definition "Adhérence"
    Soit $\ds X$ une partie de $\ds E$ et $\ds x\in E$. On dit que $\ds x$ est un point adhérent à $\ds X$ si et seulement si toute boule centrée en $\ds x$ rencontre $\ds X$. 

    L'ensemble des points adhérents à $\ds X$ est appelé l'adhérence de $\ds X$ et est noté $\ds Adh(X)$ ou parfois $\ds \bar X$.

!!! remarque
    $\ds X$ est dans sa propre adhérence

!!! exemple
    === "Énoncé"
        Quelle est l'adhérence de $\ds ]0,1[$ ?
    === "Corrigé"
        L'adhérence de l'ensemble $\ds ]0,1[$ est le segment $\ds [0,1]$.

!!! exemple
    === "Énoncé"
        Quelle est l'adhérence de $\ds \mathcal B(w,r)$ ? 
    === "Corrigé"
        L'adhérence de $\ds \mathcal B(w,r)$ est $\ds \mathcal B_F(w,r)$.



!!! propriete "Caractérisation séquentielle de l'adhérence"
    L'adhérence d'une partie $\ds X$ de $\ds E$ est exactement l'ensemble des limites des suites convergentes d'éléments de $\ds X$.

!!! preuve
    Soit $\ds (x_n)$ une suite d'éléments de $\ds X$ qui converge vers une limite $\ds \ell$. Soit $\ds \mathcal B(\ell,r)$ une suite  centrée en $\ds \ell$. $\ds x_n\to \ell$ donc il existe un rang $\ds n_0$ tel que si $\ds n\ge n_0$ alors $\ds x_n\in\mathcal B(\ell,r)$. Ainsi, $\ds \mathcal B(\ell,r)$ rencontre $\ds X$ : $\ds \ell$ est dans l'adhérence de $\ds X$ et donc toute limite de suite d'éléments de $\ds X$ est dans l'adhérence de $\ds X$. 

    Soit maintenant un point $\ds \ell$ de l'adhérence de $\ds X$. Notons $\ds B_n=\mathcal B(\ell,\frac 1n)$. $\ds \ell$ est dans l'adhérence de $\ds X$ donc il existe un élément $\ds x_n$ de $\ds X$ qui appartient à $\ds B_n$. On construit donc ainsi une suite d'éléments de $\ds X$ qui verifie pour tout $\ds n$ : $\ds ||\ell-x_n||\le \frac 1n$ et donc par comparaison, $\ds ||\ell-x_n||\to 0$ et donc $\ds \ell$ est bien la limite d'une suite d'éléments de $\ds X$. 

!!! remarque 
    C'est souvent cette caractérisation que l'on utilise. 


!!! exemple
    === "Énoncé"
        On pose $\ds E=\mathcal M_n(\mathbb R)$. Montrer que l'ensemble des matrices trigonalisables est un fermé de $\ds E$. Montrer que l'adhérence des matrices diagonalisables est l'ensemble des matrices trigonalisables.
    === "Corrigé"
        On pose $\ds T_n$ l'ensemble des matrices trigonalisables et $\ds D_n$ l'ensemble des matrices diagonalisables. Soit $\ds A_k$ une suite de matrices trigonalisables qui converge vers $\ds A$, montrons que $\ds A$ est trigonalisable, c'est à dire que l'ensemble des valeurs propres de $\ds A$ sont réelles. 

        Tout d'abord, notons que $\ds \det(\lambda I_n-A_k)$ s'écrit en fonction des coefficients de $\ds A_k$. Or ceux-ci convergent un par un vers les coefficients de $\ds A$ donc $\ds \det(\lambda I_n-A_k)\xrightarrow[k\to +\infty]{}\det(\lambda I_n-A)$. Ou encore $\ds \chi_{A_n}(\lambda)\to \chi_A(\lambda)$. 

        Soit $\ds \lambda$  une valeur propre de $\ds A$, éventuellement complexe. $\ds A_k$ étant trigonalisable, on peut écrire son polynôme caractéristique sous la forme $\ds \chi_{A_k}=\prod_{i=1}^n(X-\lambda_i^{(k)})$ où les $\ds \lambda_i^{(k)}$ sont les valeurs propres de $\ds A_k$ comptées avec leur multiplicité. On a alors $\ds |\chi_{A_k}(\lambda)|=\prod_{i=1}^n|\lambda-\lambda_i^{(k)}|\ge \prod_{i=1}^n|Im(\lambda-\lambda_i^{(k)})|= \prod_{i=1}^nIm(\lambda)=Im(\lambda)^n$ car les $\ds \lambda_i^{(k)}$ sont réelles.  Or $\ds \chi_{A_k}(\lambda)\to\chi_A(\lambda)=0$ d'où $\ds Im(\lambda)=0$ et $\ds \lambda$ est donc réelle. 

        Ainsi toutes les valeurs propres de $\ds A$ sont réelles : $\ds A$ est trigonalisable.

        Montrons maintenant que l'adhérence de $\ds D_n$ est $\ds T_n$. Puisqu'une matrice diagonalisable est en particulier trigonalisable et que $\ds T_n$ est fermé, on peut dors et déjà affirmer que $\ds adh(D_n)\subset T_n$. Montrons maintenant que toute matrice trigonalisable est limite d'une suite de matrices diagonalisables. Soit $\ds A$ trigonalisable, soit $\ds P$ telle que $\ds A=P^{-1}\mathrm{diag}(\lambda_1,\dots,\lambda_n)P$. Posons $\ds A_k=P^{-1}\mathrm{diag}(\lambda_1+\frac 1k,\lambda_2+\frac 2k+\dots+\lambda_n+\frac nk)P$. Alors pour $\ds k$ assez grand, les $\ds \lambda_i+\frac ik$ sont distincts deux à deux et la matrice $\ds A_k$ est donc diagonalisable ($n$ valeurs propres distinctes). On en déduit que $\ds A$ est bien limite d'une suite de matrices diagonalisables et donc $\ds adh(D_n)=T_n$. 



!!! definition "Partie dense"
    Soit $\ds X$ et $\ds Y$ deux parties de $\ds E$. On dit que $\ds X$ est dense dans $\ds Y$ si et seulement si $\ds Y=adh(X)$. 


!!! exemple
    === "Énoncé"
        Montrer que $\ds \mathcal Gl_n(\mathbb K)$ est dense dans $\ds \mathcal M_n(\mathbb K)$.
    === "Corrigé"
        Soit $\ds A$ une matrice de $\ds \mathcal M_n(\mathbb K)$. Soit $\ds A_k=A-\frac 1k I_n$. Alors $\ds \det(A_k)=(-1)^n\chi_A(\frac 1k)$. Or $\ds \chi_A$ admet un nombre fini de racines, donc il existe un rang $\ds k_0$ à partir duquel $\ds \chi_A(\frac 1k)\ne 0$ et donc à partir duquel les matrices $\ds A_k$ sont inversibles. Or $\ds A_k\to A$ donc $\ds A$ est bien limite d'une suite de matrices inversibles : $\ds \mathcal Gl_n(\mathbb K)$ est dense dans $\ds \mathcal M_n(\mathbb K)$.

!!! propriete "Invariance des notions de topologie par normes équivalentes"
    Les notions de topologie sont invariantes par passage à une norme équivalente, c'est à dire, si $\ds N_1$ et $\ds N_2$ sont deux normes équivalentes :  
    - les fermés de $\ds N_1$ sont exactement les fermés de $\ds N_2$ ;    
    - les ouverts de $\ds N_1$ sont exactement les ouverts de $\ds N_2$ ;    
    - l'adhérence pour $\ds N_1$ est exactement l'adhérence pour $\ds N_2$.  

!!! preuve
    Cela est immédiat en passant par les caractérisations séquentielles puisqu'on ne change pas la nature d'une suite en passant à une norme équivalente.


## Limite et continuité 

On se donne ici deux espace vectoriels normés $\ds E$ et $\ds F$ sur un même corps $\ds \mathbb K$. On notera $\ds ||\cdot||_E$ la norme sur $\ds E$ et $\ds ||\cdot||_F$ la norme sur $\ds F$.

!!! definition "Limite d'une fonction en un point"
    Soit $\ds f$ une fonction de $\ds X\subset E$ dans $\ds F$. Soit $\ds a\in adh(X)$ On dit que $\ds f$ admet une limite $\ds \ell$ en $\ds a$ si et seulement si :

    $$
    \forall\varepsilon>0,\,\exists \eta>0\ tq\ \forall x\in X,\, ||x-a||_E\le \eta \Rightarrow ||f(x)-\ell||_F\le \varepsilon.
    $$ 

!!! propriete "la limite est dans l'adhérence"
    Si $\ds f$ tend vers $\ds \ell$ en $\ds a\in adh(X)$, alors $\ds \ell\in adh(f(X))$.

!!! preuve
    Soit $\ds f$ de $\ds X$ dans $\ds F$ qui tend vers $\ds \ell$ en $\ds a\in adh(X)$. Soit une boule de centre $\ds \ell$ et de rayon $\ds r>0$, alors par définition de la convergence, il existe $\ds \eta>0$ tel que pour tout $\ds x\in X$, si $\ds ||x-a||_E\le \eta$ alors $\ds ||f(x)-\ell||_F\le r$. Prenons un tel $\ds \eta$. $\ds a\in adh(X)$ donc il existe $\ds x\in X$ tel que $\ds ||x-a||_E\le\eta$. On en déduit que $\ds ||f(x)-\ell||_F\le r$, c'est à dire que $\ds f(x)\in\mathcal B(\ell,r)$. Or $\ds f(x)\in f(X)$ donc toute boule centrée en $\ds \ell$ de rayon strictement positif rencontre $\ds f(X)$ : $\ds \ell\in adh(f(X))$. 



!!! propriete "Opérations sur les limites"
    Soient $\ds f$ et $\ds g$ deux applications de $\ds X\subset E$ dans $\ds F$, $\ds \lambda$ et $\ds \mu$ deux éléments de $\ds \mathbb K$. Soit $\ds a\in adh(X)$, on suppose que $\ds f$ et $\ds g$ admettent une limite (resp. $\ds \ell_f$ et $\ds \ell_g$) en $\ds a$. Alors $\ds \lambda f+\mu g$ tend vers $\ds \lambda\ell_f+\mu\ell_g$ en $\ds a$. 

    Soit $\ds G$ un troisième espace vectoriel normé. Soit $\ds f$ une application de $\ds X$ dans $\ds F$ et $\ds g$ une application définie sur $\ds f(X)$ à valeurs dans $\ds G$. Soit $\ds a\in adh(X)$, on suppose que $\ds f$ admet une limite $\ds \ell\in adh(f(X))$ en $\ds a$ et que $\ds g$ admet une limite $\ds \ell'$ en $\ds \ell$. Alors $\ds g\circ f$ tend vers $\ds \ell'$ en $\ds a$.

!!! preuve
    Ces démonstrations sont les mêmes que dans le cas des fonctions numériques. 

!!! propriete "Caractère borné"
    Soit $\ds f$ une application de $\ds E$ dans $\ds F$ admettant une limite en un point $\ds a\in E$. Alors il existe $\ds r>0$ tel que $\ds f$ est bornée sur $\ds \mathcal B(a,r)$. 

!!! preuve
    En effet, $\ds f$ admet comme limite $\ds \ell$ en $\ds x$. Soit $\ds \varepsilon=1$, il existe donc $\ds \eta>0$ tel que si $\ds x\in E$ et si $\ds ||x-a||\le \eta$ alors $\ds ||f(x)-\ell||\le1$. La condition $\ds ||x-a||\le \eta$ est équivalente à $\ds x\in\mathcal B(a,\eta)$ et donc si $\ds x$ est dans cette boule, on a $\ds ||f(x)||=||f(x)-\ell+\ell||\le||f(x)-\ell||+||\ell||\le 1+||\ell||$ et donc $\ds f$ est bien bornée sur cette boule.


!!! propriete "Caractérisation séquentielle"
    Soit $\ds f$ une fonction définie sur $\ds X\subset E$ dans $\ds F$ et soit $\ds a\in adh(X)$. Alors $\ds f$ admet comme limite $\ds \ell$ en $\ds a$ si et seulement si, pour toute suite d'éléments de $\ds X$ $\ds {(x_n)}_{n\in\mathbb N}$ qui converge vers $\ds a$ on a $\ds f(x_n)\xrightarrow[n\to+\infty]{}\ell$. 

!!! preuve
    Supposons que $\ds f$ admette une limite $\ds \ell$ en $\ds a\in adh(X)$. Soit $\ds (x_n)$ une suite d'éléments de $\ds X$ qui converge vers $\ds a$. Soit $\ds \varepsilon>0$, il existe $\ds \eta>0$ tel que pour tout $\ds x\in X$, si $\ds ||x-a||_E\le \eta$ alors $\ds ||f(x)-\ell||_F\le\varepsilon$. Prenons un tel $\ds \eta$. $\ds x_n\to a$ donc il existe un rang $\ds n_0$ tel que si $\ds n\ge n_0$ alors $\ds ||x_n-a||_E\le \eta$. Prenons un tel $\ds n_0$ puis prenons $\ds n\ge n_0$. Alors $\ds ||x_n-a||_E\le\eta$ et donc $\ds ||f(x_n)-\ell||_F\le\varepsilon$. Ainsi, à partir du rang $\ds n_0$, $||f(x_n)-\ell||_F\le\varepsilon$ : la suite $\ds (f(x_n))$ converge vers $\ds \ell$. 

    Pour la réciproque, on passe par la contraposée. Montrons que si $\ds f$ ne converge pas vers $\ds \ell$ en $\ds a$ alors il existe une suite d'éléments de $\ds X$ qui converge vers $\ds a$ telle que $\ds f(x_n)$ ne converge pas vers $\ds \ell$. Soit $\ds \ell\in F$, supposons que $\ds f$ ne converge pas vers $\ds \ell$ en $\ds a$ : il existe donc $\ds \varepsilon>0$ tel que pour tout $\ds \eta>0$, il existe $\ds x\in X$ tel que $\ds ||x-a||_F\le\eta$ et $\ds ||f(x)-\ell||_F>\varepsilon$. Prenons donc un tel $\ds \varepsilon$ et un entier $\ds n>0$ et posons $\ds \eta=\frac 1n$. On peut donc trouver un élément de $\ds X$ qu'on appelle $\ds x_n$ tel que $\ds ||x_n-a||_F\le\frac 1n$ et $\ds ||f(x_n)-\ell||_F>\varepsilon$. La suite $\ds (x_n)$ ainsi construite converge donc vers $\ds a$ mais la suite de ses images ne converge pas vers $\ds \ell$ : la contraposée est bien démontrée.

!!! propriete "Comportement par rapport au produit matriciel"
    Soit $\ds f$ et $\ds g$ deux applications de $\ds E$ dans $\ds \mathcal M_n(\mathbb R)$ (muni de sa norme usuelle) qui admettent une limite en $\ds a\in E$, respectivement $\ds \ell_f$ et $\ds \ell_g$. Alors $\ds fg$ admet comme limite $\ds \ell_f\ell_g$. 

!!! preuve
    $\ds g$ admet une limite en $\ds a$ donc il existe une boule $\ds \mathcal B(a,r)$ sur laquelle $\ds g$ est borné par $\ds M\in\mathbb R^+$.Soit $\ds x_n$ une suite d'éléments de $\ds E$ qui converge vers $\ds a$, on a :

    $$
    \begin{aligned}
    ||f(x_n)g(x_n)-\ell_f\ell_g||& =||(f(x_n)-\ell_f)g(x_n)+\ell_f(g(x_n)-\ell_g)||\\
    &\le ||(f(x_n)-\ell_f)g(x_n)||+|\ell_f|||g(x_n)-\ell_g||&\text{par inégalité triangulaire}\\
    &\le ||f(x_n)-\ell_f||||g(x_n)||+|\ell_f|||g(x_n)-\ell_g||&\text{en utilisant }||AB||\le||A||||B||\\
    &\le ||f(x_n)-\ell_f||M+|\ell_f|||g(x_n)-\ell_g||\\
    &\to 0
    \end{aligned}
    $$

    Donc on a bien $\ds f(x_n)g(x_n)\to \ell_f\ell_g$ et donc $\ds fg$ admet comme limite $\ds \ell_f\ell_g$ en $\ds a$. 


!!! definition "Continuité en un point"
    Soit $\ds f$ une application définie de $\ds X\subset E$ dans $\ds F$. Soit $\ds a\in X$. $\ds f$ est continue en $\ds a$ si et seulement si $\ds f$ admet une limite en $\ds a$. Cette limite est nécessairement $\ds f(a)$. 

!!! propriete "Caractérisation séquentielle de la continuité"
    $\ds f$ définie sur $\ds X\subset E$ est continue en $\ds a\in X$ si et seulement si pour toute suite $\ds (x_n)$ d'éléments de $\ds E$, $\ds f(x_n)$ converge (vers $\ds f(a)$).

!!! remarque 
    les propriétés de calcul des limites s'appliquent à la continuité, et notamment le comportement par rapport au produit matriciel.


!!! definition "Continuité sur une partie"
    Soit $\ds f$ définie sur $\ds X\subset E$, $\ds f$ est continue sur $\ds X$ si et seulement si $\ds f$ est continue en tout point de $\ds X$.


!!! propriete "Propriétés de calculs"
    Si $\ds f$ et $\ds g$ sont deux fonctions continues sur $\ds X\subset E$ alors pour tous scalaires $\ds \lambda$ et $\ds \mu$, $\ds \lambda f+\mu g$ est continue sur $\ds X$. 

    Si $\ds f$ est continue sur $\ds X\subset E$ à valeur dans $\ds F$ et si $\ds g$ est continue sur $\ds f(X)$ à valeurs dans $\ds G$ alors $\ds g\circ f$ est continue sur $\ds X$, à valeurs dans $\ds G$.

    Si $\ds f$ et $\ds g$ sont continues sur $\ds E$ à valeurs dans $\ds \mathcal M_n(R)$ alors $\ds fg$ est continue sur $\ds E$. Cas particulier utile: si $\ds n=1$ on assimile $\ds M_1(\mathbb R)$ à $\ds \mathbb R$.

!!! preuve
    Cela découle des propriétés de la continuité en un point

!!! propriete "Image réciproque continue"
    Soit $\ds f$ une fonction continue sur $\ds E$. Soit $\ds O$ un ouvert de $\ds F$ et $\ds C$ un fermé de $\ds F$. Alors $\ds f^{-1}(O)$ est un ouvert de $\ds E$ et $\ds f^{-1}(C)$ est un fermé de $\ds E$. 

!!! preuve
    Soit $\ds f$ continue sur $\ds E$. Soit $\ds O$ un ouvert de $\ds F$. Montrons que $\ds f^{-1}(O)$ est un ouvert de $\ds E$. Soit $\ds a\in f^{-1}(O)$, $\ds f(a)\in O$ donc il existe $\ds \varepsilon>0$ tel que $\ds \mathcal B(f(a),\varepsilon)\subset O$ : prenons un tel $\ds \varepsilon$. Or $\ds f$ est continue en $\ds a$ donc il existe $\ds \eta>0$ tel que pour tout $\ds x$ de $\ds E$, si $\ds ||x-a||\le \eta$, alors $\ds ||f(x)-f(a)||\le \varepsilon$. Prenons un tel $\ds \eta$, on en déduit que si $\ds x\in\mathcal B(a,\eta)$ alors $\ds f(x)\in\mathcal B(f(a),\varepsilon)\subset O$. Ainsi, $\ds \mathcal B(a,\eta)\subset f^{-1}(O)$ et donc $\ds f^{-1}(O)$ est un ouvert.

    Soit maintenant $\ds C$ un fermé de $\ds F$. Soit $\ds (x_n)$ une suite d'éléments de $\ds f^{-1}(C)$ qui converge vers une limite $\ds \ell\in E$. Alors $\ds f(x_n)$ est une suite d'éléments de $\ds C$  et puisque $\ds f$ est continue sur $\ds E$, $\ds f$ est en particulier continue en $\ds \ell$. Ainsi par caractérisation séquentielle de la continuité, $\ds f(x_n)$ converge vers $\ds f(\ell)$. Or $\ds C$ étant fermé et $\ds (f(x_n))$ étant une suite de $\ds C$, on en déduit que $\ds f(\ell)$ est un élément de $\ds C$ et donc $\ds \ell\in f^{-1}(C)$ : $\ds f^{-1}(C)$ est un fermé.

!!! exemple
    === "Énoncé"
        Soit $\ds f$ définie de $\ds \mathbb R$ (muni de la valeur absolue) dans $\ds \mathbb R^2$ (muni de la norme 2) par $\ds f(x)=(x,0)$. Montrer que $\ds f$ est continue. Montrer que l'image de l'ouvert $\ds R$ est un fermé. Ainsi, l'image directe d'un ouvert n'est pas toujours un ouvert.
    === "Corrigé"
        Tout d'abord $\ds f$ est continue en tout point. En effet, soit $\ds a\in\mathbb R$ et soit $\ds a_n$ une suite d'éléments de $\ds \mathbb R$ qui converge vers $\ds a$. Alors $\ds f(a_n)=(a_n,0)$ converge clairement vers $\ds (a,0)=f(a)$. Donc $\ds f$ est continue en tout point de $\ds \mathbb R$ donc sur $\ds \mathbb R$. Par ailleurs, $\ds f(\mathbb R)=\mathbb R\times\{0\}$. Montrons que cet ensemble n'est pas un ouvert. Prenons par exemple $\ds (0,0)$ qui est un point de $\ds \mathbb R\times\{0\}$. Soit $\ds \varepsilon >0$, le point $\ds (0,\frac\varepsilon2)$ est dans la boule de centre $\ds (0,0)$ et de rayon $\ds \varepsilon$ et n'est pourtant pas dans $\ds \mathbb R\times\{0\}$. Ainsi, $\ds \mathbb R\times\{0\}$ ne contient aucune boule centrée en $\ds 0$ : ce n'est pas un ouvert.

!!! exemple
    === "Énoncé"
        On considère la partie de $\ds \mathbb R^2$ $\ds X=\{(x,\frac 1x)|x>0\}$. Montrer que cette partie est fermée. Soit $\ds f$ définie de $\ds \mathbb R^2$ dans $\ds \mathbb R$ par $\ds f((x,y))=x$. Montrer que $\ds f$ est continue. Que peut-on dire de $\ds f(X)$ ?
    === "Corrigé"
        Soit $\ds (y_n)$ une suite d'éléments de $\ds X$ qui converge vers $\ds \ell\in\mathbb R^2$. On pose $\ds y_n=(x_n,\frac 1{x_n})$ et $\ds \ell=(\ell_1,\ell_2)$. $\ds y_n$ converge vers $\ds \ell$ donc $\ds (x_n-\ell_1)^2+(\frac 1{x_n}-\ell_2)^2\to0$. Cela implique que $\ds (x_n-\ell_1)\to 0$ et $\ds (\frac 1{x_n}-\ell_2)\to 0$. On en déduit que $\ds x_n\to \ell_2$ et $\ds \frac 1{x_n}\to \ell_2$. Par unicité de la limite, on en déduit que $\ds \ell_2=\frac 1{\ell_1}$ et donc $\ds \ell\in X$ : $\ds X$ est donc fermé. 

        Montrons que $\ds f$ est continue. Soit $\ds (a,\frac 1a)\in X$ et  soit $(x_n,\frac 1{x_n})$ une suite d'éléments de $\ds a$ qui converge vers $\ds (a,\frac 1a)$ toujours au sens de la norme $\ds 2$. On en déduit que $\ds x_n\to a$ et donc que $\ds f((x_n,\frac 1{x_n}))$ converge vers $\ds f((a,\frac 1a))$ : $\ds f$ est continue en $\ds (a,\frac 1a)$. Ainsi $\ds f$ est continue en tout point de $\ds X$ : $\ds f$ est continue sur $\ds X$. Mais $\ds f(X)=]0,\infty[$ qui n'est pas un fermé (la suite $\ds \frac 1n$ converge, mais pas dans $\ds ]0,\infty[$). Donc l'image d'un fermé n'est pas toujours un fermé.

!!! propriete "Cas particulier"
    Soit $\ds f$ une application de $\ds E$ dans $\ds \mathbb R$. L'ensemble défini par $\ds f(x)>0$ est un ouvert et les ensembles définis par $\ds f(x)=0$ et $\ds f(x)\ge 0$ sont des fermés.

!!! preuve
    C'est une application directe de la propriété précedente.

!!! definition "Fonction lipschitzienne"
    Soit $\ds f$ une application de $\ds X\subset E$ dans $\ds F$, $\ds f$ est $\ds k$-lipschitzienne si pour tout couple $\ds (x,y)$ d'éléments de $\ds E$, $\ds ||f(x)-f(y)||_F\le k||x-y||_E$.

!!! propriete "lipschizité implique continuité"
    Toute fonction lipschitzienne sur $\ds X$ est continue.

!!! preuve
    Passons, une fois n'est pas coutume, par la caractérisation séquentielle. Soit $\ds a\in X$ et $\ds x_n$ une suite d'éléments de $\ds X$ qui converge vers $\ds a$. Alors $\ds ||f(x_n)-f(a)||_F\le k||x_n-a||\to 0$. Donc par théorème de comparaison, $\ds ||f(x_n)-f(a)||\to 0$ et donc $\ds f(x_n)\to f(a)$. Par caractérisation séquentielle de la continuité, $\ds f$ est continue en $\ds a$ et puisque $\ds a$ est un élément quelconque de $\ds X$, $\ds f$ est continue sur $\ds X$. 

!!! exemple
    === "Énoncé"
        Soit $\ds A$ une partie d'un espace vectoriel normé $\ds E$. Soit $\ds \varphi$ l'application de $\ds E$ dans $\ds \mathbb R^+$ définie par $\ds \varphi(x)=d(x,A)$. Montrer que $\ds \varphi$ est continue sur $\ds E$. 
    === "Corrigé"
        On rappelle que $\ds d(x,A)=\inf_{a\in A}||x-a||$.

        Soit $\ds x$ et $\ds y$ deux éléments de $\ds E$ et soit $\ds a\in A$. Alors :

        $$
        ||x-a||=||x-y+y-a||\le||x-y||+||y-a||
        $$

        Or $\ds ||x-a||\ge d(x,A)$ donc 

        $$
        d(x,A)\le ||x-y||+||y-a||
        $$

        On a une minoration indépendante de $\ds a$ donc on peut passer à l'inf à droite : 

        $$
        d(x,A)\le ||x-y||+d(y,A)\Leftrightarrow d(x,A)-d(y,A)\le ||x-y||
        $$

        On montre de même que $\ds d(y,A)-d(x,A)\le ||x-y||$ et donc $\ds |d(x,A)-d(y,A)|\le ||x-y||$ : l'application distance est $\ds 1$-lipschitzienne donc continue.


## Espaces vectoriels normés de dimension finie.

Ici, $\ds E$ est un espace vectoriel normé de dimension finie $\ds n$. $\ds F$ est un espace vectoriel normé quelconque.

!!! propriete "Équivalence des normes en dimension finie"
    En dimension finie, toutes les normes sont équivalentes.

!!! preuve
    La démonstration n'est pas au programme. 

!!! exemple
    === "Énoncé"
        Soit $\ds E$ de dimension finie et $\ds (e_1,\dots,e_n)$ une base de $\ds E$. Soit $\ds N$ une norme sur $\ds E$ et soit $\ds N_\infty$ la norme inifinie sur $\ds E$ définie pour $\ds x=\sum_{i=1}^n x_ie_i$ par $\ds N_\infty(x)=\max_i|x_i|$. Montrer qu'il existe $\ds \alpha\in\mathbb R$ tel que pour tout $\ds x\in E$, $\ds N(x)\le \alpha N_\infty(x)$.

    === "Corrigé"
        On a :

        $$
        N(x)=N(\sum_{i=1}^nx_ie_i)\le\sum_{i=1}^n|x_i|N(e_i)\le N_\infty(x)\sum_{i=1}^nN(e_i)
        $$

        et en posant $\ds \alpha=\sum_{i=1}^nN(e_i)$ on obtient pour tout $\ds x$ $\ds N(x)\le\alpha N_\infty(x)$.

!!! exemple
    === "Énoncé"
        Soit $\ds E$ de dimension finie et $\ds (e_1,\dots,e_n)$ une base de $\ds E$. Soit $\ds N$ une norme sur $\ds E$ et soit $\ds N_1$ la norme 1 sur $\ds E$ définie pour $\ds x=\sum_{i=1}^n x_ie_i$ par $\ds N_1(x)=\sum_{i=1}^n|x_i|$. Montrer qu'il existe $\ds \alpha\in\mathbb R$ tel que pour tout $\ds x\in E$, $\ds N(x)\le \alpha N_1(x)$.
    
    === "Corrigé"
        On a :

        $$
        N(x)=N(\sum_{i=1}^nx_ie_i)\le\sum_{i=1}^n|x_i|N(e_i)\le \max_i(N(e_i))\sum_{i=1}^n|x_i|\le \max_i(N(e_i)) N_1(x)
        $$

        et en posant $\ds \alpha=\max_{i}(N(e_i))$ on obtient pour tout $\ds x$ $\ds N(x)\le\alpha N_1(x)$.

!!! exemple
    === "Énoncé"
        Soit $\ds E$ de dimension finie et $\ds (e_1,\dots,e_n)$ une base de $\ds E$. Soit $\ds N$ une norme sur $\ds E$ et soit $\ds N_2$ la norme 2 sur $\ds E$ définie pour $\ds x=\sum_{i=1}^n x_ie_i$ par $\ds N_2(x)=\sum_{i=1}^n|x_i|^2$. Montrer qu'il existe $\ds \alpha\in\mathbb R$ tel que pour tout $\ds x\in E$, $\ds N(x)\le \alpha N_2(x)$.
    
    === "Corrigé"
        On a :

        $$
        N(x)=N(\sum_{i=1}^nx_ie_i)\le\sum_{i=1}^n|x_i|N(e_i)\underset{C.S.}\le\sqrt{\sum_{i=1}^n|x_i|^2\sum_{i=1}^nN(e_i)^2}\le\sqrt{\sum_{i=1}^nN(e_i)^2}N_2(x)
        $$

        et en posant $\ds \alpha=\sqrt{\sum_{i=1}^nN(e_i)^2}$ on obtient pour tout $\ds x$ $\ds N(x)\le\alpha N_2(x)$.


!!! warning "Remarque importante"
    L'équivalence des normes indique qu'on peut choisir la norme avec laquelle on travaille. Typiquement, cela dit que la convergence d'une suite ou d'une fonction, la continuité, en dimension finie équivaut à celle de chacune de ses coordonnées dans une base (en utilisant l'équivalence à la norme infinie).


!!! theoreme "Théorème des bornes atteintes"
    Toute fonction réelle continue sur une partie non vide fermée bornée d'un espace vectoriel normé de dimension finie est bornée et atteint ses bornes.

!!! preuve 
    La démonstration est hors programme

!!! exemple
    === "Énoncé"
        Soit $\ds A$ une partie non vide fermée et bornée de $\ds E$ normé de dimension finie. Soit $\ds x\in E$. Montrer qu'il existe un $\ds a_0\in A$ tel que $\ds d(x,A)=||x-a_0||$. 
    === "Corrigé"
        Soit $\ds \varphi$ définie de $\ds A$ dans $\ds \mathbb R$ telle que $\ds \varphi(a)=d(a,x)$. L'application distance étant continue, $\ds \varphi$ est continue sur un fermé borné. D'après le théorème des bornes atteintes, elle est bornée et atteint ses bornes. Il existe donc $\ds a_0\in A$ tel que $\ds d(a_0,x)=\inf_{a\in A}d(a,x)=d(x,A)$. 

!!! propriete "Continuité des applications linéaires"
    Soit $\ds E$ un espace vectoriel normé de dimension finie et $\ds F$ un espace vectoriel normé quelconque. Toute application linéaire de $\ds E$ dans $\ds F$ est continue.

!!! preuve
    Soit $\ds (e_1,\dots,e_n)$ une base de $\ds E$. Soit $\ds u\in\mathcal L(E,F)$. Soit $\ds x\in E$, on écrit $\ds x=\sum_{i=1}^nx_ie_i$. Travaillons avec la norme $\ds 1$ puisque toutes les normes sont équivalentes sur $\ds E$.  On note $\ds N_F$ la norme sur $\ds F$. On a alors :
    
    $$
    N_F(u(x))=N_F(\sum_{i=1}^n x_iu(e_i))\le \sum_{i=1}^n|x_i|N_F(u(e_i))\le \max_i(N_F(u(e_i)))N_1(x) 
    $$

    En posant $\ds k=\max_i(N_F(u(e_i)))$, on a $\ds N_F(u(x))\le kN_1(x)$ et donc $\ds u$ est $\ds k$-lipschitzienne et donc continue.


!!! exemple
    === "Énoncé"
        Montrer que l'application $\ds \tr$ et l'application transposée sont continues.
    === "Corrigé"
        Ces deux applications sont linéaires, donc continues. 

!!! propriete "Continuité des applications multilinéaires"
    Soit $\ds E_1,\dots,E_p$ des espaces vectoriels normés de dimension finie et $\ds F$ un espace vectoriel normé quelconque. Toute application multilinéaire sur $\ds E_1\times\cdots\times E_p$ dans $\ds F$ est continue.

!!! preuve
    Soit $\ds \varphi$ multilinéaire sur $\ds E=E_1\times\cdots\times E_p$. Puisque les $\ds E_i$ sont de dimension finie, $\ds E$ est de dimension finie. Soit $\ds i\in\{1,\dots,p\}$ on définit $\ds (b^{(i)}_1,\dots b^{(i)}_{n_i})$ une base de $\ds E_i$ et pour $\ds x\in E$, on note $\ds x_{ij}$ sa coordonnée selon $\ds (0,\dots,0,\underset{\overset{\uparrow}{\text{pos. }i}}{b^{(i)}_j},0,\dots,0)$. On définit sur $\ds E$ la norme infinie par $\ds N_\infty(x)=\max_{i,j}|x_{ij}|$. On a alors :

    $$
    \begin{aligned}
    \varphi(x)&=\sum_{\substack{1\le j_1\le n_1\\\vdots\\1\le j_p\le n_p}}x_{1j_1}\cdots x_{pj_p}\varphi(b^{(1)}_{i_1},\dots,b^{(n)}_{i_n})\\
    \end{aligned}
    $$ 

    Or les applications $\ds x\mapsto x_{ij}$ sont continues. Donc $\ds \varphi$ est une somme de produits de fonction continues : elle est continue. 

!!! exemple
    === "Énoncé"
        Le produit matriciel est une application bilinéaire continue.
    === "Corrigé"
        C'est une application directe du résultat précédent.

!!! propriete "Continuité des applications polynômiales"
    Toute application polynômiale en les coefficients de $\ds x$ est continue.

!!! preuve
    Cela découle de la propriété du produit.

!!! exemple
    === "Énoncé" 
        Le déterminant est une application continue.
    === "Corrigé"
        En effectuant des développements successifs par rapport aux lignes et aux colonnes, on montre que le déterminant est polynômial en les coefficients de la matrice, et est à ce titre continu.

        On en déduit de même en particulier que $\ds M\mapsto \chi_M$ est continue.
    

### Issus de la banque CCINP
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-034.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-035.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-036.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-037.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-039.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-044.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-045.md" %}


### Annales d'oraux

{% include-markdown "../../../exercicesRMS/2022/RMS2022-704.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-705.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-706.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-707.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-708.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-709.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-710.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1433.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1301.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1101.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1102.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1302.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1181.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1182.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1183.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-855.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-857.md" %}



### Centrale Python

{% include-markdown "../../../exercicesCentralePython/RMS2022-1027.md"   rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2019-1009.md"   rewrite-relative-urls=false %}
