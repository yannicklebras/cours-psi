# Calcul différentiel
Ce chapitre a pour but de généraliser des notions vue ces deux dernières années. Dans tout ce chapitre, lorsque ça n'est pas précisé, $n$ désigne un entier naturel non nul.

## Dérivabilité des fonctions vectorielles

On s'intéresse ici aux fonctions à variable réelle et à valeur vectorielles. Typiquement on s'intéressera à des fonctions de $\mathbb R$ dans $\mathbb R^n$. 

!!! definition "courbe paramétrée"
    Soit $f$ une fonction définie sur un intervalle $I$ de $\mathbb R$ et à valeurs dans $\mathbb R^n$. L'ensemble des points de $\mathbb R^n$  $\{f(t),t\in I\}$ est appelé *courbe paramétrée* ou *arc paramétré*. On peut faire le lien avec la notion de trajectoire assez naturelle dans $\mathbb R^2$ et $\mathbb R^3$. 

!!! warning "Attention"
    L'étude et le tracé des arcs paramétrés est hors programme. 

!!! exemple
    === "Énoncé"
        Tracer la courbe paramétrée définie par 
        
        $$
        \left\{\begin{array}{rcl}x(t)&=&\cos(3t)\\y(t)&=&\sin(2t)\end{array}\right.
        $$

    === "Corrigé"
        Notons $f(t)=(x(t),y(t))$. Les deux fonctions $x(t)$ et $y(t)$ sont $2\pi$ périodiques, donc on peut restreindre l'intervalle d'étude à l'ensemble $[-\pi,\pi]$. De plus $f(-t)=(x(t),-y(t))$. On en déduit que $f(-t)$ est le symétrique de $f(t)$ par rapport à l'axe des abscisses. Ainsi, si on sait tracer la courbe de $f$ sur $[0,\pi]$ on aura son tracer sur $[-\pi,0]$ par symétrie. Enfin, $f(\pi-t)=(-x(t),-y(t))=-f(t)$. Donc $f(\pi-t)$ est le symétrique de $f(t)$ par rapport à $(0,0)$. Ainsi si on sait tracer $f$ sur $[0,\frac\pi2]$ on saura tracer $f$ sur $[\frac\pi2,\pi]$ par symétrie de centre $0$. On a donc restreint l'intervalle d'étude à $[0,\frac\pi2]$. 

        On a alors un tableau de variations conjoint de $x$ et $y$ :
        
        ![tableau de variations](images/tab.png)

        On obtient la tangente en $(0,0)$ en remarquant qu'en ce point, $2x'+3y'=0$ ce qui donne le vecteur tangent $(-3,2)$. La courbe a alors l'allure suivante : 

        ![allure de la courbe](images/courbe.png)


!!! definition "Dérivabilité en un point"
    Soit $f$ une fonction d'un intervalle $I$ de $\mathbb R$ dans $\mathbb R^n$ et $t_0\in I$. On dit que $f$ est dérivable en $t_0$ si et seulement si il existe $\ell\in\mathbb R^n$ tel que  $\frac 1{t-t_0}(f(t)-f(t_0))\xrightarrow[t\to t_0]{} \ell$. $\ell$ est alors appelée la dérivée de $f$ en $t_0$ et notée $f'(t_0)$. 

!!! remarque
    On est dans $\mathbb R^n$, la convergence se fait donc au sens de n'importe quelle norme, et on prend évidemment la norme canonique.

!!! propriete "Dérivabilité des coordonnées"
    Soit $f$ une fonction d'un intervalle $I$ de $\mathbb R$ dans $\mathbb R^n$, pour $t\in I$ on note $f_i(t)$ la $i$-ème coordonnée de $f(t)$. Alors la dérivabilité de $f$ en $t_0$ est équivalente à la dérivabilité de chacune des $f_i$ en $t_0$. 

!!! preuve
    En effet, $\frac 1{t-t_0}(f(t)-f(t_0))=(\frac{f_i(t)-f_i(t_0)}{t-t_0})_{1\le i\le n}$ et l'équivalence en découle immédiatement.

!!! propriete "Développement limité d'ordre $1$"
    Soit $f$ une fonction d'un intervalle $I$ de $\mathbb R$ dans $\mathbb R^n$, et soit $t_0\in I$. $f$ est dérivable en $t_0$ si et seulement si il existe $\ell\in\mathbb R^n$ tel que $f(t)=f(t_0)+(t-t_0)\ell+o(t-t_0)$.

!!! preuve 
    Il ne s'agit que de réécriture.


!!! definition "Vecteur tangent"
    Si $f$ est dérivable en $t_0$ alors $f'(t_0)$ est un vecteur tangent à la courbe (ou à la trajectoire).

!!! definition "Dérivabilité sur un intevalle"
    On dira que $f:I\to \mathbb R^n$ est dérivable sur $I$ si elle est dérivable en tout point de $I$.

!!! propriete "composition par une application linéaire"
    Soit $f$ définie sur un intervalle $I$ de $\mathbb R$ à valeurs dans $\mathbb R^n$ et soit $L$ une application linéaire définie sur $R^n$. Si $f$ est dérivable sur $I$ alors $t\mapsto L(f(t))$ est dérivable sur $I$.

!!! preuve
    Soit $f$ et $L$ définis dans la propriété. $L$ est une application linéaire à valeurs dans un espace vectoriel $\mathbb R^p$. On sait que $L$ est continue. Posons $g=L(f)$, on a pour $t_0\in I$, $\frac1{t-t_0}(g(t)-g(t_0))=L(\frac{1}{t-t_0}(f(t)-f(t_0)))\xrightarrow[t\to t_0]{}L(f'(t_0))$ par continuité de $L$. Ainsi $g$ est dérivable et $g'(t)=L(f'(t))$. 

!!! propriete "Cas bilinéaire"
    Soit $f$ et $g$ définies sur un intervalle $I$ de $\mathbb R$ à valeurs dans $\mathbb R^n$ et soit $B$ une application bilinéaire définie sur $R^n$. Si $f$ et $g$ sont dérivables sur $I$ alors $t\mapsto B(f(t),g(t))$ est dérivable sur $I$ de dérivée $t\mapsto B(f'(t),g(t))+B(f(t),g'(t))$. 

!!! preuve
    C'est le même travail que précédemment. 


!!! exemple
    === "Énoncé"
        Soit $A(t)$ et $B(t)$ deux fonctions de $\mathbb R$ dans $M_n(\mathbb R)$ dérivables. Alors $C:t\mapsto A(t)B(t)$ est dérivable. Préciser sa dérivée.
    === "Corrigé"
        Le produit matriciel est bilinéaire. On a donc d'après la propriété, $C$ qui est dérivable avec $C'(t)=A'(t)B(t)+A(t)B'(t)$ (attention, il n'y a pas commutativité a priori).


!!! exemple
    === "Énoncé"
        Soit $f$ et $g$ deux fonctions dérivables de $\mathbb R$ dans $E=\mathbb R^n$ et $<,>$ un produit scalaire sur $E$. Montrer que $h:t\mapsto <f(t),g(t)>$ est dérivable et préciser sa dérivée. En faire de même avec $n:t\mapsto||f(t)||$ si $f$ ne s'annule pas.

    === "Corrigé"
        La dérivabilité vient de la propriété et $h'(t)=<f'(t),g(t)>+<f(t),g'(t)>$. Pour la seconde, on passe par une composition : $n(t)=\sqrt{<f(t),f(t)>}$ et par propriétés classiques sur les dérivées, $n'(t)=\frac{<f(t),f'(t)>}{||f(t)||}$.  


!!! propriete "Cas multilinéaire"
    Soit $f_1,\dots,f_p$ définies sur un intervalle $I$ de $\mathbb R$ à valeurs dans $\mathbb R^n$ et soit $M$ une application $p$-linéaire définie sur $R^n$. Si les $f_i$ sont dérivables sur $I$ alors $t\mapsto M(f_1(t),\dots,f_p(t))$ est dérivable sur $I$ de dérivée $t\mapsto \sum_{i=1}^nM(f_1(t),\dots,f_i'(t),\dots,f_p(t))$.

!!! preuve
    Non exigible, c'est toujours le même principe.


!!! exemple
    === "Énoncé"
        Pour $n \in \mathbb{N}^*$ et $x$ réel, on pose:
        
        $$
        D_n(x)=\left|\begin{array}{ccccc}
        x & 1 & & & (0) \\
        x^2 / 2 ! & x & 1 & & \\
        x^3 / 3 ! & x^2 / 2 ! & x & \ddots & \\
        \vdots & & \ddots & \ddots & 1 \\
        x^n / n ! & \cdots & \cdots & x^2 / 2 ! & x
        \end{array}\right| .
        $$

        (a) Montrer que $D_n$ est une fonction dérivable et exprimer $D_n^{\prime}(x)$ en fonction de $D_{n-1}(x)$ pour $n \geq 2$ et $x \in \mathbb{R}$.  
        (b) En déduire l'expression de $D_n(x)$ pour tous $n \in \mathbb{N}^*$ et $x \in \mathbb{R}$.
    
    === "Corrigé"
        Notons $C_i(x)$ la $i$-ème colonne de $D_n(x)$. La fonction $x\mapsto C_i(x)$ est clairement dérivable et pous $i<n$, $C_i'(x)=C_{i+1}(x)$. $D_n(x)=\det(C_1(x),\dots,C_n(x))$ est $n$-linéaire donc $D_n$ est dérivable et $D_n'(x)=\det(C_1'(x),C_2(x),\dots,C_n(x))+\cdots+\det(C_1(x),\dots,C_n'(x))=\det(C_2(x),C_2(x),\dots,C_n(x))+\cdots+\det(C_1(x),\dots,C_n'(x))=\det(C_1(x),\dots,C_n'(x))=D_{n-1}(x)$ par développement par rapport à la dernière colonne. On a donc la relation $D_n'(x)=D_{n-1}(x)$. On peut alors montrer par récurrence que $D_n(x)=\frac{x^n}{n!}$.  

!!! propriete "Composition"
    Soit $f$ définie et dérivable sur un intervalle $I$ de $\mathbb R$ à valeurs dans $\mathbb R^n$ et soit $\varphi$ définie sur $\mathbb R$ à valeurs dans $\mathbb R$, dérivable. Alors $f\circ \varphi$ est définie et dérivable et $(f\circ \varphi)'=\varphi'\cdot f'(\varphi)$.

!!! preuve
    Cela découle directement de la composition sur chaque coordonnée.

!!! remarque 
    On définit naturellement les classes $\mathcal C^k$ et $\mathcal C^\infty$. 

## Fonctions de plusieurs variables

On s'intéresse dans cette partie aux fonctions définies sur un ouvert $\Omega$ de $\mathbb R^p$ à valeurs dans $\mathbb R$. 

### Fonctions de classe $\mathcal C^1$

!!! definition "Dérivée selon un vecteur"
    Soit $f$ définie sur $\mathbb R^p$ à valeurs dans $\mathbb R$ et $a$ un point de $\mathbb R^p$ et $v$ un vecteur de $\mathbb R^p$. On dit qu $f$ admet une dérivée en $a$ selon le vecteur $v$ si et seulement si la fonction $t\mapsto f(a+tv)$ admet une dérivée en $0$. On note cette dérivée $D_vf(a)$.

!!! definition "Dérivées partielles d'ordre $1$"
    Soit $(e_1,\dots,e_p)$ la base canonique de $\mathbb R^p$. On appelle *$i$ème dérivée partielle d'ordre $1$ de $f$ en $a$* et on note $\frac{\partial f}{\partial x_i}(a)$ ou $\partial_if(a)$ la dérivée selon le vecteur $e_i$ en $a$. 

!!! definition "fonctions de classe $\mathcal C^1$"
    Une fonction $f$ définie sur un ouvert $\Omega$ de $\mathbb R^p$ est de *classe $\mathcal C^1$ sur $\Omega$* si et seulement si elle admet des dérivées partielles d'ordre $1$ en tout point de $\Omega$ et toutes ces dérivées partielles sont continues.

    L'espace des fonctions de classe $\mathcal C^1$ est un espace vectoriel stable par produit et inverse lorsque les fonctions ne s'annulent pas.

!!! exemple 
    === "Énoncé"
        Étudier la continuité, l'existence et la continuité des dérivées partielles premières de la fonction $f: \mathbb{R}^2 \rightarrow \mathbb{R}$ définie par :
        
        $$
        f(x, y)=\left\{\begin{array}{ll}x^2 y^2 \ln \left(x^2+y^2\right) & \text { si }(x, y) \neq(0,0) \\ 0 & \text { sinon }\end{array}\right.
        $$
        
    === "Corrigé"
        La fonction $f$ est de classe $\mathcal C^1$ sur $\mathbb R^2\setminus\{(0,0)\}$ par théorèmes généraux. Soit $\varepsilon>0$, et $(x,y)$ tel que $||(x,y)||\le\varepsilon$. On a alors $|f(x,y)-f(0,0)|=x^2y^2\ln(x^2+y^2)\le \varepsilon ^4|\ln(\varepsilon^2)|\xrightarrow[\varepsilon\to 0]{}0$. Donc $f$ est continue en $0$. 

        Les dérivées partielles en dehors de $0$ existent par théorèmes généraux et sont continues. Pour l'existence de la dérivée partielle selon $x$ en $0$, on étudie $\frac{f(t,0)-f(0,0)}{t-0}=0\xrightarrow[t\to 0]{}0$ donc $\frac{\partial f}{\partial x}(0,0)$ existe et vaut $0$. De même pour $\frac{\partial f}{\partial y}(0,0)$. 

        Etudions la continuité de $\frac{\partial f}{\partial x}$ en $(0,0)$. Soit $\varepsilon>0$ et $(x,y)\ne(0,0)$ tel que $||(x,y)||\le \varepsilon$. On a alors $|\frac{\partial f}{\partial x}(x,y)-0|=|2xy^2\ln(x^2+y^2)+\frac{2x^3y^2}{x^2+y^2}|\le 2|x|y^2(|\ln(x^2+y^2)|+1)\le 2\varepsilon^3(|\ln(\varepsilon)|+1)\xrightarrow[\varepsilon\to0]{}0$. Donc $\frac{\partial f}{\partial x}$ est continue en $0$. On montre de même la continuité de $\frac{\partial f}{\partial y}$ sur $\mathbb R^2$ tout entier.

        On en déduit donc que $f$ est de classe $\mathcal C^1$ sur $\mathbb R^2$.  

!!! propriete "Existence d'un DL"
    Une fonction de classe $\mathcal{C}^1$ sur $\Omega$ admet en tout point $a$ de $\Omega$ un développement limité d'ordre 1 de la forme, pour $h=(h_1,h_2,\dots,h_p)$ :
    
    $$
    f(a+h) = f(a) + \sum_{i=1}^p \frac{\partial f}{\partial x_i}(a)h_i+o(h).
    $$

!!! preuve
    La preuve n'est pas exigible en PSI

!!! définition "Différentielle"
    Lorsque les dérivées partielles existent, on appelle *différentielle de $f$ en $a$* l'application notée $df(a)$ définie au voisinage de $0$ par 

    $$
    df(a) : h \mapsto \sum_{i=1}^p \frac{\partial f}{\partial x_i}(a)h_i.
    $$

    $df(a)$ est donc une forme linéaire. On s'autorise la notation $df(a)(h)=df(a).h$. 

!!! exemple
    === "Énoncé"
        Soit $n$ l'application définie sur $\mathbb R^n$ par $n(x)=||x||$. Montrer que $n$ admet une différentielle en tout point de $(\mathbb R^n)^*$ et donner cette différentielle.
    === "Corrigé"
        Soit $x$ non nul. On commence par montrer l'existence des dérivées partielles en $x$ : $||x+te_i||-||x||=\sqrt{<x+te_i,x+te_i>}-||x||=\sqrt{||x||^2+2t<x,e_i>+t^2} -||x||=||x||\left(1+\frac{<x,e_i>}{||x||^2}t+o(t)-1\right)=\frac{x_i}{||x||}t+o(t)$. On en déduit $\lim_{t\to 0}\frac{||x+te_i||-||x||}{t}=\frac{x_i}{||x||}$ et donc la $i$-ème dérivée partielle existe. On en déduit l'existence de la différentielle en $x$ avec $dn(x):h\mapsto \sum_{i=1}^n \frac{x_ih_i}{||x||}$. 

!!! propriete "Caractérisation de la differentielle"
    Soit $f$ de classe $\mathcal C^1$ et $a\in\Omega$. S'il existe une forme linéaire $\ell$ telle que pour tout $h\in\mathbb R^n$, $f(a+h)=f(a)+\ell(h)+o(h)$ alors $\ell=df(a)$. 

!!! preuve
    Prenons $h=te_i$ avec $t$ tel que $a+te_i\in\Omega$. On a alors $f(a+te_i)=f(a)+t\ell(e_i)+o(t)$ et donc $\ell(e_i)=\frac{f(a+te_i)-f(a)}{t}+o(1)$ et ainsi $\ell(e_i)=\frac{\partial f}{\partial x_i}(a)=df(a).e_i$. On en déduit que $\ell$ et $df(a)$ coïncident sur la base canonique et sont donc égales. 


!!! exemple
    === "Énoncé"
        On note $f$ l'application de $\mathcal M_n$ dans $\mathbb R$ définie par $f(M)=\mathrm{tr}(M^2)$. Montrer que $f$ est de classe $\mathcal C^1$ et calculer sa différentielle
    === "Corrigé"
        $f$ est polynomiale en les coefficients de $M$. Elle est donc de classe $\mathcal C^1$. Par ailleurs :
        
        $$
        f(M+H)=\mathrm{tr}((M+H)^2)=\mathrm{tr}(M^2+MH+HM+H^2)=\mathrm{tr}(M^2)+2\mathrm{tr}(MH)+o(H).
        $$

        Or l'application $H\mapsto 2\mathrm{tr}(MH)$ est une forme linéaire, c'est donc bien la différentielle de $f$ en $M$. 

!!! definition "Gradient"
    Dans $\mathbb R^p$ muni de sa structure euclidienne canonique, le gradient d'une fonction $f$ de classe $\mathcal C^1$ en un point $a$ est défini et noté par : $grad f(a)=\nabla f(a)=(\partial_1 f(a),\dots,\partial_p f(a))$. On remarque alors que $df(a).h=<\nabla f(a),h>$.


!!! exemple
    D'après un exemple précédent, si $f(x)=||x||$ alors $\nabla f(a)=\frac 1{||x||}x$. 


### Règle de la chaîne

Dans ce paragraphe, $f$ est une fonction de classe $\mathcal C^1$ définie sur un ouvert $\Omega$ de $\mathbb R^p$ dans $\mathbb R$.

!!! propriete "Composition"
    Soit $x$ une fonction dérivable de $\mathbb R$ dans $\Omega$ de coordonnées $x_1,\dots,x_p$. On pose $g(t)=f(x(t))$. Alors $g$ est dérivable de dérivée :

    $$
    g'(t)=\sum_{i=1}^px_i'(t)\partial_if(x(t)).
    $$

!!! preuve
    On a $g(t+h)-g(t)=f(x(t+h))-f(x(t))\underset{x \text{dérivable}}{=}f(x(t)+hx'(t)+o(h))-f(x(t))=df(x(t)).(hx'(t))+o(h)=hdf(x(t)).x'(t)$. On en déduit que $g'(t)$ existe et vaut $g'(t)=df(x(t)).x'(t)=\sum_{i=1}^p x_i'(t)\partial_if(x(t))$. 

!!! propriete "Règle de la chaîne"
    On considère maintenant $x$ une fonction de $\mathbb R^n$ dans $\mathbb R$ de classe $\mathcal C^1$ (c'est à dire que chacune de ses coordonnées est de classe $\mathcal C^1$). Alors $g(u_1,\dots,u_n)=f(x(u_1,\dots,u_n))$ est de classe $\mathcal C^1$ et :

    $$
    \partial_i g(u_1,\dots,u_n)=\sum_{j=1}^p \partial_j f(x(u_1,\dots,u_n))\partial_i x_j(u_1,\dots,u_n).
    $$

!!! remarque 
    On reconnaît quelque chose qui ressemble à un produit matriciel. On appelle matrice jacobienne de $x$ en $(u_1,\dots,u_n)$ la matrice de terme général $\partial_j x_i(u_1,\dots,u_n)$. On remarque ici que :

    $$
    (\partial_1 g(u),\partial_2 g(u),\dots,\partial_n g(u))=(\partial_1 f(x(u)),\dots,\partial_p f(x(u)))\begin{pmatrix}\partial_1x_1(u) & \partial_2 x_1(u)& \dots& \partial_n x_1(u)\\ \partial_1 x_2(u) & \dots & \dots & \vdots \\ \vdots & \dots & \dots & \vdots \\ \partial_1 x_p(u)& \dots & \dots & \partial_n x_p(u)\end{pmatrix}.
    $$

!!! exemple
    === "Énoncé"
        Soient $f: \mathbb{R}^2 \rightarrow \mathbb{R}$ une fonction différentiable et $g: \mathbb{R}^2 \rightarrow \mathbb{R}$ définie par
        
        $$
        g(u, v)=f\left(u^2+v^2, u v\right) .
        $$
        
        En employant la règle de la chaîne et sous réserve d'existence, exprimer les dérivées partielles de $g$ en fonction des dérivées partielles de $f$.
    === "Corrigé"
        On a $x(u,v)=(u^2+v^2,uv)$. On a donc $\frac{\partial g}{\partial u}(u,v)=2u\partial_1f(u^2+v^2,uv)+v\partial_2f(u^2+v^2,uv)$ et $\frac{\partial g}{\partial v}(u,v)=2v\partial_1f(u^2+v^2,uv)+u\partial_2f(u^2+v^2,uv)$. 



### Applications géométriques

!!! definition "Courbe du plan"
    Soit $f$ une fonction de classe $\mathcal C^1$ définie sur $\mathbb R^2$. On appelle *ligne de niveau $M$* l'ensemble des points $(x,y)$ du plan tels que $f(x,y)=M$. 

    En particulier si $M=0$, l'ensemble $\{(x,y)\in\mathbb R^2| f(x,y)=0\}$ est appelée *courbe du plan définie par l'équation $f(x,y)=0$*. 

    Un point régulier d'une courbe du plan est un point $(x,y)$ tel que $f(x,y)=0$ et $\nabla f(x,y)\ne 0$. 

!!! theoreme "fonctions implicites"
    Soit $f$ de classe $\mathcal C^1$ sur $\mathbb R^2$ et soit $(x,y)$ un point régulier de la courbe du plan $f(x,y)=0$. Alors il existe une fonction $\gamma$ définie de $[-1,1]$ dans $\mathbb R^2$ de classe $\mathcal C^1$ et telle que $f(\gamma(t))=0$ pour tout $t$ avec en particulier $\gamma(0)=(x,y)$. On dit que $\gamma$ est une paramétrisation de  la courbe au voisinage de $(x,y)$. 

!!! preuve 
    ce théorème est admis en PSI

!!! definition "tangente en un point"
    Soit $\gamma$ une paramétrisation de la courbe $f(x,y)=0$. La tangente en un point $(x_0,y_0)$ de la courbe est la droite passant par $(x_0,y_0)$ et de vecteur directeur $\gamma'(0)$.

!!! propriete "propriété du gradient"
    Le gradient est normal à la tangente en un point régulier.

!!! preuve
    Soit $\gamma$ une paramétrisation de la courbe au voisinage de $(x_0,y_0)$ point régulier. On a donc pour tout $t\in[-1,1]$, $f(\gamma(t))=0$. La fonction $g:t\mapsto f(\gamma(t))$ est donc constante, et sa dérivée est nulle. Or $g'(t)=\gamma_1'(t)\partial_1f(\gamma(t))+\gamma_2'(t)\partial_2f(\gamma(t))$ et pour $t=0$ on obtient $<\gamma'(0),\nabla f(x_0,y_0)>=0$ c'est à dire que le gradient est orthogonal à la tangente. 

!!! propriete "Équation de la tangente"
    L'équation de la tangente à $f(x,y)=0$ passant par un point régulier $(x_0,y_0)$ est $\partial_1f(x_0,y_0) (x-x_0)+\partial_2f(x_0,y_0)(y-y_0)=0$. 

!!! preuve
    En effet, l'ensemble des points de la tangente est l'ensemble des points tels que le vecteur $(x-x_0,y-y_0)$ soit orthogonal à $\nabla f(x_0,y_0)$ c'est à dire vérifiant $<(x-x_0,y-y_0),\nabla f(x_0,y_0)>=0$, ce qui donne immédiatement l'équation proposée.

!!! propriete "Cas général"
    Lorsqu'il est non nul, le gradient de $f$ est orthogonal aux lignes de niveau et orienté dans le sens des valeurs croissantes de $f$.

!!! exemple
    === "Énoncé"
        Soit $f(x,y)=y-g(x)$ avec $g$ une fonction de classe $\mathcal C^1$ de $\mathbb R$ dans $\mathbb R$, retrouver l'équation de la tangente en un point de la courbe représentative de $g$.
    === "Corrigé"
        On s'intéresse à la courbe du plan définie par l'équation $f(x,y)=y-g(x)=0$. L'équation de la tangente en $(x_0,y_0)$ est $\partial_1f(x_0,y_0)(x-x_0)+\partial_2f(x_0,y_0)(y-y_0)=0$. Or $\partial_1 f(x_0,y_0)=-g'(x_0)$ et $\partial_2 f(x_0,y_0)=1$ d'où l'équation de tangente $-g'(x_0)(x-x_0)+(y-y_0)=0$ ou encore $y=g'(x_0)(x-x_0)+y_0$, ce qui correspond au résultat que l'on connaissait.

!!! exemple
    === "Énoncé"
        L'équation cartésienne d'une ellipse est de la forme $\left(\frac xa\right)^2+\left(\frac yb\right)^2=1$. C'est donc une courbe plane associée à la fonction $f(x,y)= \left(\frac xa\right)^2+\left(\frac yb\right)^2-1$. Donner l'équation de la tangente en un point $(x_0,y_0)$ de l'ellipse

    === "Corrigé"
        L'équation de la tangente en un point $(x_0,y_0)$ est $0=\frac {2x_0}{a^2}(x-x_0)+\frac{2y_0}{b^2}(y-y_0)=\frac {2x_0}{a^2}x+\frac{2y_0}{b^2}y-2\frac{x_0^2}{a^2}-2\frac{y_0^2}{b^2}=\frac {2x_0}{a^2}x+\frac{2y_0}{b^2}y-2$. Et donc une équation de la tangente est $\frac{xx_0}{a^2}+\frac{yy_0}{b^2}=1$. 


!!! definition "Surface"
    Soit $f$ de classe $\mathcal C^1$ sur $\mathbb R^3$ à valeurs dans $\mathbb R$. L'ensemble des points $(x,y,z)$ de $\mathbb R^3$ vérifiant $f(x,y,z)=0$ est appelé *surface* définie par l'équation $f(x,y,z)=0$. 

    Un point régulier de la surface est un point où le gadient est non nul.

    Le plan tangent à la surface en un point $(x_0,y_0,z_0)$ est le plan passant par $(x_0,y_0,z_0)$ et orthogonal au gradient. 

!!! definition "courbe tracée sur une surface"
    Soit $S$ une surface définie par l'équation $f(x,y,z)=0$. Soit $\gamma$ une application d'un intervalle $I$ dans $\mathbb R^3$. Si $\gamma(I)\subset S$ on dit que l'ensemble $(I,\gamma)$ est une courbe tracée sur la surface $S$.

!!! propriete "Tangente et gradient"
    Si $(I,\gamma)$ est une courbe tracée sur la surface $S$ et si $\gamma$ est de classe $\mathcal C^1$, alors pour tout $t\in I$, $<\nabla f(\gamma(t))|\gamma'(t)>=0$. On en déduit que la tangente à la courbe est incluse dans le plan tangent à la surface. 

!!! exemple
    === "Énoncé"
        Soit $S$ la surface d'équation $x^2-y^2-z=1$,
        $P$ le plan d'équation $x+2y-z=0$.
        Donner l'ensemble des points $M$ de $S$ tels que le plan tangent à $S$ en
        $M$ soit parallèle à $P$.
    === "Corrigé"
        On pose $f(x,y,z)=x^2-y^2-z-1$. Soit $M$ un point de $S$ de coordonnées $(x_0,y_0,z_0)$ et vérifiant donc $x_0^2-y_0^2-z_0=1$. Le plan tangent en $M$ à $S$ est donc orthogonal au gradient. Or $\nabla f(M)=(2x_0,-2y_0,-1)$. Pour que le plan tangent à $S$ en $M$ soit parallèle à $P$ il faut donc que $(1,2,-1)$ et $(2x_0,-2y_0,-1)$ soient colinéaires, c'est à dire $x_0=\frac 12$, $y_0=-1$ et puisque $z_0=x_0^2-y_0^2-1$, $z_0=-\frac 74$. Il n'y a donc qu'un seul point, le point $M(\frac 12,-1,-\frac 74)$ .

### Fonctions de classe $\mathcal C^2$

!!! definition "Dérivées partielles d'ordre 2"
    Soit $f$ une fonction de $\mathbb R^p$ dans $\mathbb R$ de classe $\mathcal C^1$. On appelle dérivées partielles d'ordre 2 de $f$ les dérivées partielles, lorsqu'elles existent, des dérivées partielles de $f$. On note par exemple $\frac{\partial^2 f}{\partial x_j\partial x_i}$ la $j$-ème dérivée partielle de la $i$-ème dérivée partielle de $f$. 

!!! definition "Classe $\mathcal C^2$"
    $f$ est dite de classe $\mathcal C^2$ sur $\Omega$ si toutes ses dérivées partielles d'ordre $2$ existent et sont continues sur $\Omega$. 


!!! exemple
    === "Énoncé"
        Calculer toutes les dérivées partielles secondes de la fonction $(x,y)\mapsto e^{xy^2}$.
    === "Corrigé"
        Notons $f$ cette fonction. On calcule $\partial_1 f(x,y)=y^2e^{xy^2}$ et $\partial_2 f(x,y)=2xye^{xy^2}$. Chacune de ces deux fonctions admet évidemment des dérivées partielles et $\frac{\partial^2 f}{\partial x\partial x}(x,y)=\frac{\partial^2 f}{\partial x^2}(x,y)=y^4e^{xy^2}$, $\frac{\partial^2f}{\partial y\partial x}(x,y)=2ye^{xy^2}+2xy^3e^{xy^2}$, $\frac{\partial^2 f}{\partial x\partial y}(x,y)=2ye^{xy^2}+2xy^3e^{xy^2}$ et enfin $\frac{\partial^2 f}{\partial y^2}(x,y)=2xe^{xy^2}+4x^2y^2e^{xy^2}$.

        On pourra affirmer que $f$ est de classe $\mathcal C^2$ sur $\mathbb R$. 

!!! theoreme "Théorème de Schwarz"
    Si $f$ est de classe $\mathcal C^2$ sur un ouvert $\Omega$ de $\mathbb R^p$ alors pour tout $x$ de $\Omega$ et tout couple $(i,j)$, $\frac{\partial^2 f}{\partial x_i\partial x_j}(x,y)=\frac{\partial^2 f}{\partial x_j\partial x_j}(x,y)$. 

!!! preuve
    La preuve n'est pas exigible en PSI.

!!! exemple
    === "Énoncé"
        On considère l'application $f: \mathbb{R}^2 \rightarrow \mathbb{R}$ définie pour $(x, y) \in \mathbb{R}^2$ par
        
        $$
        f(x, y)= \begin{cases}y^2 \sin \left(\frac{x}{y}\right) & \text { si } y \neq 0 \\ 0 & \text { sinon. }\end{cases}
        $$
        
        Étudier la continuité de $f$. Étudier l'existence et éventuellement la valeur des dérivées premières et secondes de $f$.
    === "Corrigé"
        Tout d'abord, $f$ est $\mathcal C^2$ sans problème en $(x,y)$ tel que $y\ne 0$. On s'intéresse donc essentiellement à ce qui se passe en $\{0\}\times\mathbb R$.

        Soit $x_0\in\mathbb R$, $x\in\mathbb R$ et $y\ne 0$ alors $|f(x,y)-f(x_0,0)|=y^2|\sin(\frac xy)|\le y^2\frac{|x|}{|y|}=|xy|\xrightarrow[(x,y)\to (x_0,0)]{} 0$ donc $f$ est continue en $(x,0)$.

        Les dérivées partielles existent en tout point $(x,y)$ tel que $y\ne 0$. Sinon, soit $x_0\in \mathbb R$, on a $f(x_0,0)=0$ donc $\frac{f(x_0+t,0)-f(x_0,0)}{t}=0\xrightarrow[t\to 0]{}0$ donc $\frac{\partial f}{\partial x}(x_0,0)$ existe et vaut $0$. Par ailleurs $\frac{f(x_0,t)-f(x_0,0)}{t}=t\sin\frac {x_0}t\xrightarrow[t\to 0]{}0$ donc de même $\frac{\partial f}{\partial y}(x_0,0)$ existe et vaut $0$. Vérifions la continuité des dérivées partielles.

        Pour $(x,y)$ tel que $y\ne 0$ et $x_0\in\mathbb R$, on a $\left|\frac{\partial f}{\partial x}(x,y)-\frac{\partial f}{\partial x}(x_0,0)\right|=|y\cos\frac xy|\le |y|\xrightarrow[(x,y)\to (x_0,0)]{}0$ donc $\frac{\partial f}{\partial x}$ est continue sur $\mathbb R^2$. De même, $\left|\frac{\partial f}{\partial y}(x,y)-\frac{\partial f}{\partial y}(x_0,0)\right|=2y\sin(\frac xy)-x\cos(\frac xy)$ qui ne converge pas vers $0$ si $x_0\ne O$. Ainsi la deuxième dérivée partielle de $f$ n'est pas continue en $(x_0,0)$ si $x_0\ne 0$ mais elle l'est en $(0,0)$. 

        Etudions l'existence de dérivées partielles secondes croisées en $(0,0)$. 

        $\frac{\frac{\partial f}{\partial x}(0,t)-\frac{\partial f}{\partial x}(0,0)}{t}= 1\xrightarrow[t\to 0]{}1$. donc $\frac{\partial^2f}{\partial y\partial x}(0,0)$ existe et vaut $1$. 

        $\frac{\frac{\partial f}{\partial y}(t,0)-\frac{\partial f}{\partial x}(0,0)}{t}= 0\xrightarrow[t\to 0]{}0$. donc $\frac{\partial^2f}{\partial x\partial y}(0,0)$ existe et vaut $0$.

        On a donc $\frac{\partial^2f}{\partial x\partial y}(0,0)\ne\frac{\partial^2f}{\partial y\partial x}(0,0)$ et la fonction n'est donc pas $\mathcal C^2$ en $(0,0)$.


!!! definition "Matrice Hessienne"
    Soit $f$ une fonction de classe $\mathcal C^2$ sur un ouvert $\Omega$ de $\mathbb R^p$. Soit $(x,y)$ un élément de $\Omega$, on appelle *matrice hessienne* de $f$ en $(x,y)$ la matrice de $\mathcal M_p(\mathbb R)$ notée $H_f(x,y)$ de terme général $\frac{\partial^2 f}{\partial x_i\partial x_j}$. 

!!! remarque
    D'après le théorème de Schwarz, la matrice Hessienne est symétrique, donc diagonalisable dans $\mathbb R$ et dans une base orthonormée.

!!! exemple
    === "Énoncé"
        On considère la fonction numérique de deux variables définie sur $\mathbf{R}^2$ par $f(x, y)=x^3+y^4-y x^2-x y^2$. Donner la matrice hessienne de $f$ en $(x,y)$.
    === "Corrigé"
        $f$ étant polynômiale, elle est forcéement $\mathcal C^2$. On a :

        $$
        H_f(x,y)=\begin{pmatrix}6x-2y & -2x-2y \\ -2x-2y & 12y^2-2x\end{pmatrix}.
        $$


!!! definition "Fomule de Taylor-Young à l'ordre $2$"
    Si $f$ est de classe $\mathcal C^2$ sur un ouvert de $\mathbb R^p$ alors :
    
    $$
    f(a+h) \underset{h \rightarrow 0}{=} f(a)+\nabla f(a)^{\top} h+\frac{1}{2} h^{\top} H_f(a) h+\mathbf{o}\left(\|h\|^2\right)
    $$

    que l'on peut aussi écrire :

    $$
    f(a+h) \underset{h \rightarrow 0}{=} f(a)+<\nabla f(a), h>+\frac{1}{2} <h, H_f(a) h>+\mathbf{o}\left(\|h\|^2\right) .
    $$



### Extremums d'une fonction de $\mathbb R^p$ dans $\mathbb R$

On sait depuis la première année, du moins sur $\mathbb R^2$ que les extremums sur un ouvert sont atteints en des points critiques, c'est à dire où le gradient s'annule. Rechercher les extremums c'est donc rechercher tout d'abord les points critique puis vérifier leur statut.

!!! exemple
    === "Énoncé"
        Soit $f(x,y)=x^4+y^4-4xy$. Déterminer les extremums de $f$ sur $\mathbb R^2$.
    === "Corrigé"
        On a les dérivées partielles : $\partial_1f(x,y)=4x^3-4y$ et $\partial_2f(x,y)=4y^3-4x$. Les points critiques sont donc $(1,1)$, $(0,0$ et $(-1,-1)$. 

        On a $f(0,0)=0$ et $f(t,t)-f(0,0)=2t^4-2t^2=2t^2(t^2-1)\le0$ pour $|t|<1$ donc au voisinage de $0$. Ainsi, $(0,0)$ représente un minimum dans la direction $(1,1)$. Mais $f(t,-t)-f(0,0)=2t^4+t^2\ge0$ donc $(0,0)$ est un minimum dans la direction $(1,-1)$. On en déduit que $(0,0)$ n'est pas un extremum. 

        On a $f(1,1)=f(-1,-1)=-2$. Or $f(x,y)-(-2)=x^4+y^4-4xy+2=x^4+y^4-2x^2y^2+2x^2y^2-4xy+2=(x^2-y^2)^2+2(xy+1)^2\ge 0$. On en déduit que $(1,1)$ et $(-1,-1)$ sont des minimums globaux.


!!! remarque
    Les notions d'*extremum local et global* ainsi que la notion de *point critique* pour une application de classe $\mathcal C^1$ se généralisent sur $\mathbb R^p$. 


!!! propriete "Caractérisation d'ordre $1$"
    Soit $f$ définie sur un ouvert $\Omega$ de $\mathbb R^p$. Si $f$ admet un extremum local ou global en $a$ alors $a$ est un point critique de $f$ c'est à dire $\nabla f(a)=0$. 

!!! preuve
    Sans perte de généralité, supposons que $f$ admette un minimum en $a\in\Omega$. Alors l'application $t\mapsto f(a+te_i)$ définie sur un ouvert centré en $0$ (car $\Omega$ est un ouvert) admet un minimum en $0$. C'est une fonction de $\mathbb R$ dans $\mathbb R$ donc sa dérivée en $0$ est nulle, c'est à dire $\frac{\partial f}{\partial x_i}(a)=0$. Ceci étant vrai pour tout $i$, on en déduit que $\nabla f(a)=0$. 


!!! propriete "Caractérisation d'ordre 2"
    Si $f$ est une fonction de classe $\mathscr{C}^2$ sur un ouvert de $\mathbb{R}^p$ et $a$ un point critique de $f$ :
    
    - si $H_f(a) \in \mathcal{S}_p^{++}(\mathbb{R})$, alors $f$ atteint un minimum local strict en $a$;
    
    - si $H_f(a) \notin \mathcal{S}_p^{+}(\mathbb{R})$, alors $f$ n'a pas de minimum en $a$.


!!! preuve
    Supposons que $H_f(a)\in\mathcal S_p^{++}$, alors les valeurs propres de $H_f(a)$ sont strictement positives. Supposons sans perte de généralité que $\lambda_1$ soit la valeur propre minimale. Soit $(e_1,\dots,e_p)$ une base de vecteurs propres de $H_f(a)$, on a pour tout $i\in\{1,\dots,p\}$ $e_i^\bot H_f(a)e_i=\lambda_i\ge \lambda_1$ et donc pour tout $x$, $x^\bot H_f(a)x=\sum_{i=1}^p\lambda_ix_i^2\ge \lambda 1||x||^2$. D'après le DL à l'ordre $2$ on a donc :

    $$
    \begin{aligned}
    f(a+h) &\underset{h \rightarrow 0}{=} f(a)+<\nabla f(a), h>+\frac{1}{2} <h, H_f(a) h>+\mathbf{o}\left(\|h\|^2\right)\\
    &\underset{h \rightarrow 0}{=}f(a)+\frac{1}{2} <h, H_f(a) h>+o(||h||^2)
    \end{aligned}
    $$

    et donc $\frac{f(a+h)-f(a)}{||h||^2}=\frac{1}{2||h||^2} <h, H_f(a) h>+o(1)$. Or $\frac{1}{2||h||^2} <h, H_f(a) h>\ge\lambda_1>0$ donc il existe un voisinage de $0$ sur lequel $\frac{f(a+h)-f(a)}{||h||^2}>0$. On en déduit que $f(a)$ est bien un minimum. 


    Si maintenant $H_f(a)\notin \mathcal S_p^+$ alors $H_f(a)$ admet une valeur propre strictement négative $\lambda$. Soit $h$ vecteur propre associé à cette valeur propre, on a $\frac{f(a+h)-f(a)}{||h||^2}=\lambda+o(1)<0$ ce qui permet de dire que $f(a)$ est un maximum dans la direction de $h$.


!!! exemple 
    === "Énoncé"
        Reprenons l'exemple précédent $f(x,y)=x^4+y^4-4xy$. Étudier avec cette dernière proporiété les extremums de $f$.
    === "Corrigé"
        Dans ce cas, la matrice Hessienne en $(x,y)$ est de la forme $H_f(x, y)= \begin{pmatrix}12 x^2 & -4 \\-4 & 12 y^2\end{pmatrix}$ ce qui donne en $(0,0)$ : $H_f(0,0)=\begin{pmatrix}0 & -4 \\ -4 & 0\end{pmatrix}$. Cette matrice a une trace nulle et un déterminant strictement négatif : ses valeurs propres sont donc de signe strictement opposé et il n'y a pas d'extremum.

        En $(1,1)$, on a $H_f(1,1)=\begin{pmatrix}12&-4\\-4&12\end{pmatrix}$ dont la trace et le déterminant sont strictement positifs : les deux valeurs propres sont strictement positives et on en déduit que $(1,1)$ représente un minimum local, dont on a montré précédemment qu'il était aussi global.

!!! propriete "Cas de la dimension $2$"
    Si $p=2$, alors on sait que $\tr(H_f(a))=\lambda_1+\lambda_2$ et $\det A=\lambda_1\lambda_2$. Ainsi, si $\det A>0$ alors les valeurs propres sont de même signe et le signe de la trace précise ce signe : on a alors un minimum ou un maximum local. Si $\det A< 0$ alors les valeurs propres sont de signe opposé et il n'y a pas d'extremum. Si $\det A=0$ on ne peut pas conclure.


!!! exemple
    === "Énoncé"
        Trouver les extremums locaux et globaux de la fonction $f: \mathbb{R}^2 \rightarrow \mathbb{R}$ donnée par

        $$
        f(x, y)=x^4+y^4-2(x-y)^2 .
        $$

    === "Corrigé"
        d'après ddmaths

        La fonction $f$ est de classe $\mathscr{C}^{\infty}$ sur l'ouvert $\mathbb{R}^2$.
        Déterminons ses points critiques. Pour $(x, y) \in \mathbb{R}^2$,
        
        $$
        \begin{array}{rlrl}
        \frac{\partial f}{\partial x}(x, y)=0 & & \Leftrightarrow\left\{\begin{array}{l}
        4 x^3-4(x-y)=0 \\
        \frac{\partial f}{\partial y}(x, y)=0
        \end{array}\right. \\
        & \Leftrightarrow\left\{\begin{array}{l}
        x^3=-y^3 \\
        x^3=x-y
        \end{array}\right. \\
        & \Leftrightarrow\left\{\begin{array}{l}
        y=-x \\
        x^3=2 x
        \end{array}\right.
        \end{array}
        $$

        On obtient trois points critiques: $(0,0),(\sqrt{2},-\sqrt{2})$ et $(-\sqrt{2}, \sqrt{2})$.
        La matrice hessienne de $f$ en $(x, y)$ est
        
        $$
        H_f(x, y)=\left(\begin{array}{cc}
        12 x^2-4 & 4 \\
        4 & 12 y^2-4
        \end{array}\right)
        $$

        Étude en $(0,0)$.
        La matrice hessienne de $f$ en $(0,0)$ est
        
        $$
        H_f(0,0)=\left(\begin{array}{cc}
        -4 & 4 \\
        4 & -4
        \end{array}\right)
        $$

        Son déterminant est nul, on ne peut pas conclure à partir de celle-ci. Cependant, $f(0,0)=0$ et l'on remarque
        
        $$
        f(1 / n, 0) \underset{n \rightarrow+\infty}{\sim}-2 / n^2<0 \quad \text { et } \quad f(1 / n, 1 / n) \underset{n \rightarrow+\infty}{\sim} 2 / n^4>0 .
        $$

        Il n'y a pas d'extremum local en $(0,0)$.

        Étude en $(\sqrt{2},-\sqrt{2})$.
        La matrice hessienne de $f$ en $(\sqrt{2},-\sqrt{2})$ est
        
        $$
        H_f(x, y)=\left(\begin{array}{cc}
        20 & 4 \\
        4 & 20
        \end{array}\right)
        $$

        Celle-ci est de déterminant strictement positif et de trace strictement positive, la fonction $f$ présente un minimum local en $(\sqrt{2},-\sqrt{2})$.
        Plus précisément,
        
        $$
        \begin{aligned}
        f(x, y)-f(\sqrt{2},-\sqrt{2}) & =x^4+y^4-2(x-y)^2+8 \\
        & =\left(x^2-2\right)^2+\left(y^2-2\right)^2+4\left(x^2+y^2\right)-2(x-y)^2 \\
        & =\left(x^2-2\right)^2+\left(y^2-2\right)^2+2\left(x^2+y^2\right)+4 x y \\
        & =\left(x^2-2\right)^2+\left(y^2-2\right)^2+2(x+y)^2 \geq 0
        \end{aligned}
        $$

        Ainsi, $(\sqrt{2},-\sqrt{2})$ est un minimum global.
        Étude en $(-\sqrt{2}, \sqrt{2})$.
        
        C'est identique et d'ailleurs on peut remarquer que $f(x, y)=f(y, x)$.


### Issus de la banque CCINP
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-030.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-031.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-032.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-033.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-042.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-052.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-057.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-074.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-075.md" %}


### Annales d'oraux

{% include-markdown "../../../exercicesRMS/2022/RMS2022-740.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-742.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-743.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-744.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-951.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-952.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-953.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-954.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-955.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-956.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-957.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-958.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-960.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-962.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-963.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1121.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1246.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1249.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1335.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1470.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1471.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1472.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1336.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1473.md" %}



