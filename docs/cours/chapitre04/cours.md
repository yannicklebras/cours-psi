# Théorèmes d'interversion


Le but de ce chapitre est de préciser les conditions d'interversion de l'intégrale généralisée avec les symboles et régularités classiques. Les théorèmes sont complexes, avec beaucoup d'hypothèses à vérifier, dont certaines sont primordiales. Vous devez surtout apprendre ces théorèmes et savoir les utiliser. On ne peut pas réussir aux concours sans les connaître.

## Suites et séries de fonctions intégrables

!!! theoreme "Théorème de convergence dominée"
    si une suite $\ds \left(f_n\right)$ de fonctions continues par morceaux sur $\ds I$ converge simplement vers une fonction $\ds f$ continue par morceaux sur $\ds I$ et s'il existe une fonction $\ds \varphi$ intégrable sur $\ds I$ vérifiant $\ds \left|f_n\right| \leqslant \varphi$ pour tout $\ds n$, alors les fonctions $\ds f_n$ et $\ds f$ sont intégrables sur $\ds I$ et :
    
    $$
    \int_I f_n(t) \mathrm{d} t \underset{n \rightarrow+\infty}{\longrightarrow} \int_I f(t) \mathrm{d} t
    $$

!!! preuve
    la preuve n'est pas au programme.


!!! remarque 
    Contrairement au théorème d'interversion limite/intégrale déjà vu, ici $\ds I$ n'est pas nécessairement un *segment* ^^et^^ la convergence n'est pas uniforme, mais juste simple. C'est l'hypothèse de domination qui rend ce théorème si fort.

!!! exemple
    === "Énoncé"
        Déterminer la limite, quand $\ds n\to+\infty$ de $\ds \int_{0}^{\frac\pi4}(\tan(t))^n\d t$. 
    === "Corrigé"
        Posons $\ds f_n(t)=(\tan(t))^n$. Bien qu'il s'agisse d'une intégration sur un segment, les fonctions $\ds f_n$ convergent vers une fonction $\ds f$ qui est nulle sur $\ds [0,\frac\pi4[$ et vers $\ds 1$ en $\ds \frac\pi4$ : cette fonction n'étant pas continue, on n'a pas convergence uniforme et donc le théorème d'interversion uniforme ne s'applique pas. Vérifions les hypothèses du théorème de convergence dominée : $\ds f_n$ est continue par morceaux et converge simplement vers la fonction continue par morceaux $\ds f$ citée précédemment. De plus, pour tout $\ds t\in[0,\frac\pi4]$, $\ds \tan(t)\le 1$ et la fonction constante égale à $\ds 1$ est intégrable sur $\ds [0,\frac\pi4]$. Par théorème de convergence dominée, $\ds \int_0^{\frac\pi4}(\tan(t))^n\d t\xrightarrow[n\to+\infty]{}\int_0^{\frac\pi4}f(t)\d t=0$. 

!!! exemple
    === "Énoncé"
        Déterminer la limite, quand $\ds n\to+\infty$ de $\ds \int_1^{+\infty}\frac1{1+t^n}\d t$.
    === "Corrigé"
        Posons $\ds f_n(t)=\frac1{1+t^n}$. On veut intervertir limite et intégrale sur un intervalle quelconque donc on passe par la convergence dominée. Les fonctions $\ds f_n$ sont continues par morceaux et convergent simplement sur $\ds [1,+\infty[$ vers la fonction $\ds f$ définie par $\ds f(t)=\begin{cases}\frac 12&\text{si }t=1\\0&\text{sinon}\end{cases}$. Cette fonction est bien continue par morceaux. De plus $\ds f_n(t)=\frac 1{1+t^n}\le\frac 1{t^2}$  pour $n\ge 2$ qui est intégrable sur $\ds [1,+\infty[$. D'après le théorème de convergence dominée, on en déduit que :
        
        $$
        \lim_{n\to+\infty}\int_1^{+\infty}\frac 1{1+t^n}\d t=\int_1^{+\infty}f(t)\d t=0.
        $$

!!! exemple 
    === "Énoncé"
        Déterminer la limite, quand $\ds n\to+\infty$ de $\ds \int_0^{+\infty}\frac{e^{-x/n}}{1+x^2}\d x$.
    === "Corrigé" 
        Posons $\ds f_n(x)=\frac{e^{-x/n}}{1+x^2}$. Les fonctions $\ds f_n$ sont continues par morceaux et convergent simplement sur $\ds ]0,+\infty[$ vers la fonction $\ds f(x)=\frac 1{1+x^2}$. Cette fonction est continue par morceaux sur $\ds ]0,+\infty[$. De plus, $\ds x$ étant positif, pour tout $\ds n\in\mathbb N^*$, $\ds |f_n(t)|\le \frac 1{1+t^2}$ qui est intégrable sur $\ds ]0,+\infty[$. On a finalement, d'après le théorème de convergence dominée, 
        
        $$
        \lim_{n\to+\infty} \int_0^{+\infty}\frac{e^{-x/n}}{1+t^2}\d t=\int_0^{+\infty}\frac 1{1+t^2}\d t=\left[\arctan(x)\right]_0^{+\infty}=\frac \pi2.
        $$

!!! exemple
    === "Énoncé"
        Déterminer la limite, quand $\ds n\to+\infty$, de $\ds \int_0^{+\infty}\frac{e^{-x}}{(1+x)^n}\d x$.
    === "Corrigé"
        On pose $\ds f_n(x)=\frac{e^{-x}}{(1+x)^n}$. Les fonction $\ds f_n$ sont continues par morceaux sur $\ds ]0,+\infty[$ et convergent sur cet intervalle vers la fonction nulle (qui est continue par morceaux). De plus pour $\ds t\in]0,+\infty[$, $\ds |f_n(x)|\le e^{-x}$ qui est intégrable sur $\ds ]0,+\infty[$. On en déduit par théorème de convergence dominée que :

        $$
        \lim_{n\to+\infty}\int_0^{+\infty}\frac{e^{-x}}{(1+x)^n}\d x=\int_0^{+\infty}0\d x=0.
        $$  


!!! theoreme "Théorème d'intégration terme à terme"
    si une série $\ds \sum f_n$ de fonctions intégrables sur $\ds I$ converge simplement, si sa somme est continue par morceaux sur $\ds I$, et si la série $\ds \sum \int_I\left|f_n(t)\right| \mathrm{d} t$ converge, alors $\ds \sum_{n=0}^{+\infty} f_n$ est intégrable sur $\ds I$ et :
    
    $$
    \int_I\sum_{n=0}^{+\infty} f_n(t) \mathrm{d} t=\sum_{n=0}^{+\infty} \int_I f_n(t) \mathrm{d} t .
    $$

!!! preuve
    La preuve n'est pas au programme


!!! remarque
    Les hypothèses importantes lors d'une rédaction sont celles relatives à la convergence simple et à la domination. On ne fera pas les démonstrations de continuité par morceaux. 

!!! exemple
    === "Énoncé"
        Montrer que $\ds \int_0^{+\infty}\frac t{e^t-1}\d t=\sum_{n=1}^{+\infty}\frac 1{n^2}$.
    === "Corrigé"
        Posons $\ds f(t)=\frac t{e^t-1}$. On a $\ds \lim_{t\to0}f(t)=1$ donc l'intégrale est faussement impropre en $\ds 0$ et $\ds t^2f(t)=\frac{t^3}{e^t-1}\sim t^3e^{-t}\to 0$ donc $\ds f(t)=o(\frac 1{t^2})$ et est intégrable en $\ds +\infty$. L'intégrale proposée est donc bien convergente.

        On remarque que pour $\ds t>0$, $\ds \frac t{e^t-1}=te^{-t}\times\frac 1{1-e^{-t}}=te^{-t}\sum_{n=0}^{+\infty} e^{-nt}=\sum_{n=1}^{+\infty}te^{-nt}$. Posons $\ds f_n(t)=te^{-nt}$. Par construction la série $\ds \sum f_n$ converge simplement vers $\ds f$ et chaque fonction est intégrable. La fonction $\ds f$ est continue par morceaux sur $\ds \mathbb R^+_*$ et intégrable.

        Calculons $\ds \int_0^{+\infty}|f_n(t)|\d t$.

        $$
        \begin{aligned}
        \int_0^{+\infty}|f_n(t)|\d t&=\int_0^{+\infty}te^{-nt}\d t\\
        &=\left[\frac{te^{-nt}}{-n}\right]_0^{+\infty}+\frac 1n\int_0^{+\infty}e^{-nt}\d t\\
        &=0+\frac1n\int_0^{+\infty}e^{-nt}\d t\\
        &=\frac 1{n^2}\left[-e^{-nt}\right]_0^{+\infty}\\
        &=\frac 1{n^2}
        \end{aligned}
        $$

        Ainsi la série $\ds \sum\int_I|f_n|$ est convergente. On en déduit d'après le théorème d'intégration terme à terme l'intégrabilité de $\ds \sum f_n$ avec 

        $$
        \int_0^{+\infty}\sum_{n=1}^{+\infty}te^{-nt}\d t=\sum_{n=1}^{+\infty}\frac 1{n^2}.
        $$

        Or $\ds \sum_{n=1}^{+\infty} te^{-nt}=\frac t{e^t-1}$ donc $\ds \int_0^{+\infty}\frac t{e^t-1}\d t=\sum_{n=1}^{+\infty}\frac 1{n^2}$.

!!! exemple
    === "Énoncé"
        Établir que pour tout $\ds x>0$
        
        $$
        \int_0^1 t^{x-1} \mathrm{e}^{-t} \mathrm{~d} t=\sum_{n=0}^{+\infty} \frac{(-1)^n}{n !(x+n)} .
        $$
    === "Corrigé"
        Notons que $\ds \int_0^1 t^{x-1} \mathrm{e}^{-t} \mathrm{~d} t$ est bien définie.
        Pour tout $\ds t \in] 0 ; 1]$,

        $$
        t^{x-1} \mathrm{e}^{-t}=\sum_{n=0}^{+\infty} \frac{(-1)^n t^{n+x-1}}{n !}
        $$

        donc

        $$
        \int_0^1 t^{x-1} \mathrm{e}^{-t} \mathrm{~d} t=\int_0^1 \sum_{n=0}^{+\infty} f_n .
        $$
        
        Les fonctions $\ds f_n$ sont continues par morceaux, $\ds \sum f_n$ converge simplement sur $\ds ] 0 ; 1]$ et est de somme $\ds t \mapsto t^{x-1} \mathrm{e}^{-t}$ continue par morceaux.
        Les fonctions $\ds f_n$ sont intégrables sur $\ds ] 0 ; 1$ ] et
        
        $$
        \int_0^1\left|f_n(t)\right| \mathrm{d} t=\frac{1}{n !(x+n)} .
        $$
        
        La série $\ds \sum \int_0^1\left|f_n\right|$ converge donc on peut intégrer terme à terme
        
        $$
        \int_0^1 t^{x-1} \mathrm{e}^{-t} \mathrm{~d} t=\sum_{n=0}^{+\infty} \frac{(-1)^n}{n !(x+n)} .
        $$

!!! exemple
    === "Énoncé"
        Calculer $\ds \int_{0}^{+\infty}\cos(\sqrt x)e^{-x}\d x$.
    === "Corrigé"
        On décompose en série entière : 

        $$
        \cos(\sqrt x)e^{-x}     = \sum_{n=0}^{+\infty}(-1)^n\frac{x^n}{(2n)!}e^{-x}
        $$ 

        Posons $\ds f_n(x)=(-1)^n\frac{x^n}{(2n)!}e^{-x}$ et $\ds f(x)=\cos(\sqrt x)e^{-x}$. Alors $\ds \sum f_n$ converge simplement vers $\ds f$ et $\ds f$ est continue par morceaux. Calculons $\ds \int_0^{+\infty}|f_n(t)|\d t$ :

        $$
        \int_0^{+\infty}|f_n(t)|\d t=\frac 1{(2n)!}\int_0^{+\infty}x^ne^{-x}\d x
        $$

        On montre par des intégrations par parties successive que $\ds \int_0^{+\infty}x^ne^{-x}\d x=n!$. Ainsi,  $\ds \int_0^{+\infty}|f_n(t)|\d t=\frac{n!}{(2n)!}$ dont on peut montrer, via le critère de d'Alembert par exemple, que c'est le terme général d'une série convergente. 
        
        D'après le théorème d'intégration terme à terme, $\ds f$ est intégrable et 

        $$
        \int_0^{+\infty}f(t)\d t=\sum_{n=0}^{+\infty}\int_0^{+\infty}f_n(t)\d t=\sum_{n=0}^{+\infty}(-1)^n\frac{n!}{(2n)!}
        $$


!!! exemple
    === "Énoncé"
        Montrer que $\ds \sum_{n=0}^{+\infty}\frac{(-1)^n}{2n+1}=\int_0^1\frac{\d t}{1+t^2}$.
    === "Corrigé"
        Pour $\ds t\in]0,1[$ on peut écrire $\ds \frac 1{1+t^2}=\sum_{n=0}^{+\infty}(-t^2)^n$. Posons $\ds f_n(t)=(-1)^nt^{2n}$. On a $\ds \int_0^1|f_n(t)|\d t=\frac1{2n+1}$ qui est le terme général d'une série divergente donc le théorème d'intégration terme à terme ne s'applique pas !

        Il va falloir repasser par le théorème de convergence dominée sur les sommes partielles. On pose $\ds S_n(t)=\sum_{k=0}^{n}(-1)^kt^{2k}$. La suite de fonctions $\ds (S_n)$ converge simplement vers $\ds f:t\mapsto\frac 1{1+t^2}$ sur $\ds ]0,1[$, chaque fonction $\ds S_n$ est ontinue par morceaux et $\ds f$ est continue par morceaux. Il nous reste à montrer l'hypothèse de domination. 

        $$
        |S_n(t)|= \left|\sum_{k=0}^n(-1)^kt^{2k}\right|=\left| \frac{1-(-t^2)^{n+1}}{1+t^2}\right|\le \frac2{1+t^2}=\varphi(t)
        $$

        et $\ds \varphi$ est intégrable sur $\ds ]0,1[$. 

        On peut donc appliquer le théorème de convergence dominée : 

        $$
        \lim_{n\to+\infty}\int_0^1 S_n(t)\d t=\int_0^1\lim_{n\to+\infty}S_n(t)\d t
        $$

        c'est-à-dire

        $$
        \lim_{n\to+\infty}\sum_{k=0}^n\frac{(-1)^k}{2k+1}=\int_0^1\frac 1{1+t^2}\d t=[\arctan(t)]_0^1=\frac\pi4
        $$

## Régularité d'une fonction définie par une intégrale à paramètre

!!! theoreme "Théorème de continuité"  
    si $\ds A$ et $\ds I$ sont deux intervalles de $\ds \mathbb{R}$ et $\ds f$ une fonction définie sur $\ds A \times I$, telle que :  
    - pour tout $\ds t \in I, x \mapsto f(x, t)$ est continue sur $\ds A$;  
    - pour tout $\ds x \in A, t \mapsto f(x, t)$ est continue par morceaux sur $\ds I$;  
    - il existe une fonction $\ds \varphi$ intégrable sur $\ds I$, telle que pour tout $\ds (x, t) \in A \times I$, on ait $\ds |f(x, t)| \leqslant \varphi(t)$;  
    alors la fonction $\ds x \mapsto \int_I f(x, t) \mathrm{d} t$ est définie et continue $\ds \operatorname{sur} A$.

!!! preuve
    Pour la suite, posons $\ds g(x)=\int_I f(x,t)\d t$.

    Soit $\ds a\in A$. Montrons que $\ds g$ est continue en $\ds a$. Soit $\ds (x_n)$ une suite d'éléments de $\ds A$ qui converge vers $\ds a$. On pose $\ds f_n(t)=f(x_n,t)$. On remarque que, par continuité de $\ds x\mapsto f(x,t)$ en $\ds A$, pour tout $\ds t\in I$, $\ds f_n(t)\xrightarrow[n\to+\infty]{}f(a,t)$. Notons $\ds f_a:t\mapsto f(a,t)$ : on a convergence simple de la suite $\ds f_n$ vers $\ds f_a$. Par ailleurs, les $\ds f_n$ ainsi que $\ds f_a$ sont continues par morceaux. Enfin, $\ds |f_n(t)|=|f(x_n,t)|\le\varphi(t)$. Donc par théorème de convergence dominée, les $\ds f_n$ et $\ds f_a$ sont intégrables sur $\ds I$ et 

    $$
    \int_I f_n(t)\d t\xrightarrow[n\to+\infty]{}\int_If(a,t)\d t
    $$

    C'est à dire $\ds g(x_n)\xrightarrow[n\to+\infty]{}g(a)$. Par caractérisation séquentielle de la continuité, $\ds g$ est continue en $\ds a$ et donc sur $\ds A$. 




!!! remarque 
    Comme la continuité est une notion locale, on se contente parfois de vérifier l'hypothèse de domination sur des sous-segment ou des sous-intervalles de $\ds A$. 


!!! exemple
    === "Énoncé"
        On pose $\ds g(x)=\int_0^x\frac{\cos(t)}{x+t}\d t$. Montrer que $\ds g$ est continue sur $\ds ]0,+\infty[$.
    === "Corrigé"
        Commençons par nous débarrasser du $\ds x$ sur la borne : on pose $\ds xu=t$ qui est un changement de variable $\ds \mathcal C^1$ croissant et bijectif si $\ds x>0$. 

        $$
        \int_0^{x}\frac{\cos(t)}{x+t}\d t=\int_0^1\frac{\cos(xu)}{1+u}\d u
        $$

        Posons $\ds f(x,t)=\frac{\cos(xt)}{1+t}$.  
        - pour $\ds t>0$ fixé, $\ds x\mapsto f(x,t)$ est continue
        - pour $\ds x>0$ fixé, $\ds t\mapsto f(x,t)$ est continue par morceaux sur $\ds [0,1]$
        - Soit $\ds x>0$, $\ds |f(x,t)|=\frac{|\cos(tx)|}{1+t}\le\frac 1{1+t}\le 1$ qui est intégrable sur $\ds [0,1]$.
        On peut alors appliquer le théorème de continuité des intégrales à paramètre : $\ds g$ est définie et continue sur $\ds \mathbb R^+_*$.

!!! exemple 
    === "Énoncé"
        On pose $\ds f(x)=\int_0^1 \frac {t^{x-1}}{1+t}\d t$. Montrer que $\ds f$ est définie et continue sur $\ds \mathbb R^+_*$. 
    === "Corrigé"
        On pose $\ds g(x,t)=\frac{t^{x-1}}{1+t}$. Si $\ds x\in \mathbb R^+_*$, alors $\ds x-1>-1$ et pour $\ds x$ fixé, $\ds g(x,t)\sim_{t\to0}t^{x-1}$ qui est alors intégrable sur $\ds ]0,1]$ (pas de problème de définition en $\ds 1$. 

        Ainsi $\ds f$ est bien définie sur $\ds \mathbb R^+_*$. On énonce ensuite les hypothèses du théorème :  
        - Pour $\ds t$ fixé, $\ds x\mapsto g(x,t)$ est continue sur $\ds \mathbb R^+_*$ ;   
        - Pour $\ds x$ fixé, $\ds t\mapsto g(x,t)$ est continue par morceaux sur $\ds ]0,1]$ ;

        Mais on remarque qu'on ne peut pas majorer $\ds |g(x,t)|$ sur $\ds \mathbb R^+_*$ tout entier : en effet la fonction $\ds x\mapsto g(x,t)$ est décroissante car $\ds t\in[0,1]$. On en déduit que la seule majoration globale possible serait par $\ds g(0,t)=\frac{1}{t(1+t)}\sim_0\frac 1t$ qui n'est pas intégrable. **Mais** la continuité étant une propriété locale, on va s'intéresser à la continuité sur tout segment $\ds [a,b]$ inclus dans $\ds ]0,+\infty[$ : Soit donc $\ds [a,b]\subset]0,+\infty[$ et $\ds x\in [a,b]$, on a $\ds |g(x,t)|\le \frac {t^{a-1}}{1+t}\le t^{a-1}$ qui est intégrable sur $\ds ]0,1]$.

        D'après le théorème de continuité des intégrales à paramètres, la fonction $\ds f$ est donc continue sur $\ds [a,b]$ et donc sur $\ds \mathbb R^+_*$ tout entier. 



!!! exemple   
    === "Énoncé"  
        Définition et continuité de $\ds g:x\mapsto \int_0^{ \frac\pi2}\sin^x(t)\d t$.  
    === "Corrigé"  
        Posons $\ds f(x,t)=\sin^x(t)$. Il n'y a pas de problème de définition en $\ds \frac\pi2$ donc on se concentre sur l'étude en $\ds t=0$. On a $\ds \sin^x(t)=\exp(x\ln(\sin(t)))=\exp(x\ln(t+o(t)))=\exp(x\ln(t)+x\ln(1+o(1)))=t^xe^{o(1)}\sim t^x$. Cette fonction est intégrable en $\ds 0$ si et seulement si $\ds x>-1$. On en déduit que $\ds g$ est définie sur $\ds ]-1,+\infty[$. 

        On vérifie les hypothèses du théorème de continuité des intégrales à paramètre :   
        - pour tout $\ds t\in]0,\frac\pi2[$, $\ds x\mapsto \sin^x(t)$ est continue ;  
        - pour tout $\ds x\in]-1,+\infty[$, $\ds t\mapsto \sin^x(t)$ est continue par morceaux sur $\ds ]0,\frac\pi2[$ ;  
        - soit $\ds a\in]-1,+\infty[$, pour tout $\ds x\in[a,+\infty[$ et $\ds t\in]0,\frac\pi2[$, on a $\ds |f(x,t)|=|\sin^x(t)|\le\sin^a(t)$ qui est intégrable sur $\ds ]0,\frac\pi2$.

        On en déduit que $\ds g$ est continue sur tout intervalle de la forme $\ds [a,+\infty[$ et $\ds g$ est donc continue sur $\ds ]0,+\infty[$.




!!! theoreme "Théorème de convergence dominée à paramètre continu"  
    si $\ds A$ et $\ds I$ sont deux intervalles de $\ds \mathbb{R}$, $\ds a$ une borne de $\ds A$ et $\ds f$ une fonction définie sur $\ds A \times I$ telle que :  
    - pour tout $\ds t \in I,\ f(x, t) \underset{x \rightarrow a}{\longrightarrow} \ell(t)$;  
    - pour tout $\ds x \in A,\ t \mapsto f(x, t)$ et $\ds t \mapsto \ell(t)$ sont continues par morceaux sur $\ds I$;  
    - il existe une fonction $\ds \varphi$ intégrable sur $\ds I$, telle que pour tout $\ds (x, t) \in A \times I$, on ait $\ds |f(x, t)| \leqslant \varphi(t)$; alors $\ds \ell$ est intégrable sur $\ds I$ et :  

    $$
    \int_I f(x, t) \mathrm{d} t \underset{x \rightarrow a}{\longrightarrow} \int_I \ell(t) \mathrm{d} t .
    $$

!!! preuve
    Comme précédemment, soit $\ds (x_n)$ une suite d'éléments de $\ds A$ qui converge vers $\ds a$. Pour $\ds t\in I$ et $\ds n\in\mathbb N$, on pose $\ds f_n(t)=f(x_n,t)$. Alors par hypothèse, $\ds f_n(t)\xrightarrow[n\to+\infty]{}\ell(t)$. De plus, toujours par hypothèse, les $\ds f_n$ et $\ds \ell$ sont continues par morceaux. Enfin, pour tout $\ds n$ et tout $\ds t\in I$, $\ds |f_n(t)|\le \varphi(t)$ qui est intégrable. Par théorème de convergence dominée, les $\ds f_n$ sont intégrables et $\ds \ell$ est intégrable $\ds \int_If_n(t)\d t\xrightarrow[n\to+\infty]{}\int\ell(t)\d t$. Par caractérisation séquentielle de la limite, on en déduit que $\ds \int_If(x,t)\d t\xrightarrow[x\to a]{}\int_I\ell(t)\d t$. 

!!! exemple
    === "Énoncé"
        Soit $\ds f(x)=\int_0^{+\infty}\frac1{1+x^3+t^2}\d t$. Calculer la limite de $\ds f$ quand $\ds x\to+\infty$. 
    === "Corrigé"
        Posons $\ds g(x,t)= \frac 1{1+x^3+t^2}$. $\ds f$ est définie sur $\ds \mathbb R^+$ ($t\mapsto g(x,t)$ est continue en $\ds 0$ et équivalente à $\ds \frac 1{t^2}$ en $\ds +\infty$. Enonçons les hypothèse du théorème de convergence dominée à paramètre continu :   
        - Soit $\ds t\in[0,+\infty[$, $\ds g(x,t)\xrightarrow[x\to+\infty]{}0=\ell(t)$  
        - Soit $\ds x\in\mathbb R^+$, $\ds t\mapsto g(x,t)$ est continue par morceaux et $\ds \ell$ aussi (c'est la fonction nulle)  
        - Soit $\ds x\in\mathbb R^+$ et $\ds t\in [0,+\infty[$, $\ds |g(x,t)|=\frac1{1+x^3+t^2}\le\frac 1{1+t^2}$ qui est intégrable sur $\ds [0,+\infty[$.

        D'après le théorème de convergence dominée à paramètre continu, $\ds \lim_{x\to+\infty}f(x)=\int_0^{+\infty}0\d t=0$. 
        
!!! exemple
    === "Énoncé"
        Soit $\ds f(x)=\int_0^1\frac{\cos(xt)}{1+t}\d t$. Déterminer la limite quand $\ds x\to 0^+$ de $\ds f$. 
    === "Corrigé"
        On a déjà montré plus haut que cette fonction est définie sur $\ds \mathbb R^+$. Posons $\ds g(x,t)=\frac{\cos(xt)}{1+t}$, on a pour $\ds t\in[0,1]$, $\ds \lim_{x\to0}\frac{\cos(xt)}{1+t}=\frac 1{1+t}$. Or $\ds g(x,t)$ et $\ds t\mapsto \frac 1{1+t}$ sont continues par morceaux sur $\ds [0,1]$ et pour $\ds x\in\mathbb R^+$ et $\ds t\in[0,1]$, $\ds |g(x,t)|=\frac{|\cos(xt)|}{1+t}\le1$ qui est intégrable sur $\ds [0,1]$. D'après le théorème de convergence dominée à paramètre continu, on peut affirmer que $\ds \lim_{x\to0^+} f(x)=\int_0^1\frac 1{1+t}\d t=\ln2$. 


!!! exemple
    === "Énoncé"
        On pose $\ds f(x)=\int_0^{+\infty}\frac 1{1+t^x}\d t$. On admet ici la définition et la continuité de $\ds f$ sur $\ds ]1,+\infty[$. Déterminer la limite de $\ds f$ en $\ds +\infty$.
    === "Corrigé"
        Posons $\ds g(x,t)=\frac 1{1+t^x}$ définie sur $\ds [2,+\infty[\times[0,+\infty[$ (on peut commencer à 2 car c'est la limite en $\ds +\infty$ qui nous intéresse). Soit $\ds t\in[0,+\infty[$. Si $\ds t\in[0,1[$, alors $\ds g(x,t)\xrightarrow[x\to+\infty]{}1$. Si $\ds t=1$, $\ds g(x,t)=\frac 12$ et si $\ds t>1$ alors $\ds g(x,t)\xrightarrow[x\to+\infty]{}0$. Posons $\ds \ell$ la fonction définie sur $\ds [0,+\infty[$ par : $\ds \ell(t)=\begin{cases}1&\text{si }t<1\\\frac 12&\text{si }t=1\\0&\text{si }t>1\end{cases}$ alors $\ds g(x,t)\xrightarrow[x\to+\infty]{}\ell(t)$. Pour tout $\ds x\in[2,+\infty[$, $\ds g(x,\cdot)$ et $\ds \ell$ sont continues par morceaux sur $\ds [0,+\infty[$. Nous allons devoir majorer en deux morceaux : pour tout couple $\ds (x,t)\in[2,+\infty[\times[0,+\infty[$, $\ds |g(x,t)|\le \begin{cases}1&\text{si }t\le 1\\\frac 1{t^2}&\text{si }t>1\end{cases}=\varphi(t)$. La fonction $\ds \varphi$ est intégrable sur $\ds [0,+\infty[$ donc par théorème de convergence dominée à paramètre continu, 

        $$  
        \lim_{x\to+\infty}f(x)=\int_0^{+\infty}\ell(t)\d t=1.
        $$ 


!!! theoreme "Théorème de dérivation"  
    si $\ds A$ et $\ds I$ sont deux intervalles de $\ds \mathbb{R}$ et $\ds f$ une fonction définie sur $\ds A \times I$, telle que :  
    - pour tout $\ds t \in I, x \mapsto f(x, t)$ est de classe $\ds \mathscr{C}^1$ sur $\ds A$;  
    - pour tout $\ds x \in A, t \mapsto f(x, t)$ est intégrable sur $\ds I$;  
    - pour tout $\ds x \in A, t \mapsto \frac{\partial f}{\partial x}(x, t)$ est continue par morceaux sur $\ds I$;  
    - il existe une fonction $\ds \varphi$ intégrable sur $\ds I$, telle que pour tout $\ds (x, t) \in A \times I$, on ait $\ds \left|\frac{\partial f}{\partial x}(x, t)\right| \leqslant \varphi(t)$; 
    
    
    alors la fonction $\ds g: x \mapsto \int_I f(x, t) \mathrm{d} t$ est de classe $\ds \mathscr{C}^1$ sur $\ds A$ et vérifie :  

    $$
    \forall x \in A, \quad g^{\prime}(x)=\int_I \frac{\partial f}{\partial x}(x, t) \mathrm{d} t
    $$

!!! preuve
    Soit $\ds a\in A$, on se place sur $\ds A^-=A\cap]-\infty;a[$. 

    Posons $\ds F(x)=\int_If(x,t)\d t$, $\ds \Delta(x)=\frac{F(x)-F(a)}{x-a}$ et pour $\ds t\in I$ et $\ds x\in A^-$, $\ds \delta(x,t)=\frac{f(x,t)-f(a,t)}{x-a}$. On cherche à montrer que $\ds \Delta$ admet une limite en $\ds a$. 

    $\ds a$ est une borne de $\ds A^-$, $\ds \delta$ est définie sur $\ds A^-\times I$,  
    - pour $\ds t\in I$, $\ds \delta(x,t)\xrightarrow[x\to a]{}\frac{\partial f}{\partial x}(a,t)$   
    - pour $\ds x\in A^-$, $\ds t\mapsto \delta(x,t)$ et $\ds t\mapsto\frac{\partial f}{\partial x}(a,t)$ sont continues par morceaux sur $\ds I$  
    - Soit $\ds x\in A^-$ et $\ds t\in I$, d'après le théorème des accroissements finis, $\ds f(\cdot,t)$ étant $\ds \mathcal C^1$, il existe $\ds c\in[x,a]$ tel que $\ds f(x,t)-f(a,t)=\frac{\partial f}{\partial x}(c,t)(x-a)$. On en déduit que $\ds \delta(x,t)=\frac{\partial f}{\partial x}(c,t)\le \varphi(t)$.

    On peut donc appliquer le théorème de convergence dominée à paramètre continu : $\ds \lim_{x\to a}\int_I\delta(x,t)\d t=\int_I \frac{\partial f}{\partial x}(a,t)\d t$. Or $\ds \int_I\delta(x,t)\d t=\Delta(x)$ et donc $\ds \Delta$ admet une limite en $\ds a$ et ainsi $\ds F$ est dérivable en $\ds a$ et sa dérivée est $\ds \int_I\frac{\partial f}{\partial x}(a,t)\d t$ : on peut dériver sous l'intégrale.


!!! exemple
    === "Énoncé"
        On pose pour $\ds x\in]0,+\infty[$, $\ds f(x)=\int_0^{\frac\pi2}\frac{\cos(t)}{t+x}\d t$. Montrer que $\ds f$ est de classe $\ds \mathcal C^1$. 
    === "Corrigé"
        Posons $\ds g(x,t)=\frac{\cos(t)}{t+x}$. 
        - Soit $\ds t\in [0,\frac\pi2]$, par théorèmes généraux, la fonction $\ds x\mapsto \frac{\cos(t)}{t+x}$ est de classe $\ds \mathcal C^1$ car le dénominateur ne s'annule pas.  
        - Soit $\ds x\in]0,+\infty[$, $\ds t\mapsto g(x,t)$ est intégrable sur $\ds [0,\frac\pi2]$ (pas de difficulté ici, elle est continue sur un segment)  
        - On a $\ds \frac{\partial g}{\partial x}(x,t)=-\frac{\cos(t)}{(t+x)^2}$ qui est une fonction continue par morceaux par rapport à $\ds t$
        
        Les trois première hypothèse sont bien vérifiées. On cherche alors à majorer indépendamment de $\ds x$ la dérivée partielle : $\ds \left|\frac{\partial g}{\partial x}(x,t)\right|\le\frac 1{(t+x)^2}\le\frac 1{t^2}$ mais cette dernière n'est pas intégrable en $\ds 0$. On va donc se placer sur un sous-intervalle : soit $\ds x\in[a,+\infty[$ avec $\ds a>0$, alors $\ds \left|\frac{\partial g}{\partial x}(x,t)\right|\le\frac 1{(t+a)^2}$ qui est indépendante de $\ds x$ et intégrable, car continue, sur $\ds [0,\frac\pi2]$. 

        D'après le théorème de dérivation des intégrales à paramètre, la fonction $\ds f$ est de classe $\ds \mathcal C^1$ sur tout $\ds [a,+\infty[$ et donc sur $\ds ]0,+\infty[$. Et on a de plus pour tout $\ds x>0$, $\ds f'(x)=-\int_0^{\frac\pi2} \frac{\cos(t)}{(t+x)^2}\d t\le 0$. Donc $\ds f$f est décroissante.

!!! exemple
    === "Énoncé"
        On pose $\ds f(x)=\int_0^{+\infty}\frac{\ln(1+xt)}{1+t^2}\d t$. Montrer que $\ds f$ est définie et continue sur $\ds \mathbb R^+$ et de classe $\ds \mathcal C^1$ sur $\ds \mathbb R^+_*$. 
    === "Corrigé"
        On remarque qu'on ne peut pas se contenter du caractère $\ds \mathcal C^1$ car on cherche à montrer aussi la continuité en $\ds 0$. On fait donc définition et continuité d'une part puis caractère $\ds \mathcal C^1$. 

        Continuité :   
        - pour tout $\ds t\in]0,+\infty[$, $\ds x\mapsto g(x,t)$ est continue sur $\ds \mathbb R^+$
        - pour tout $\ds x\in\mathbb R^+$, $\ds t\mapsto g(x,t)$ est bien continue par morceaux sur $\ds ]0,+\infty[$
        - Soit $\ds x\in[0,b]$ avec $\ds b\in \mathbb R^+$ alors $\ds |g(x,t)|\le \frac{\ln(1+bt)}{1+t^2}$ qui est intégrable sur $\ds ]0,+\infty[$

        On en déduit que $\ds f$ est définie et continue sur $\ds [0,b]$ pour tout $\ds b>0$ donc $\ds f$ est définie et continue sur $\ds \mathbb R^+$. 

        Caractère $\ds \mathcal C^1$ :  
        - pour tout $\ds t\in]0,+\infty[$, $\ds x\mapsto g(x,t)$ est de classe $\ds \mathcal C^1$ par théorèmes généraux  
        - pour tout $\ds x\in\mathbb R^+_*$, $\ds t\mapsto g(x,t)$ est intégrable sur $\ds ]0,+\infty[$  
        - Soit $\ds 0<a<b$ et $\ds x\in[a,b]$, on a $\ds \frac{\partial g}{\partial x}(x,t)=\frac{x}{(1+tx)(1+t^2)}$ qui est continue par morceaux 
        - et $\ds \left|\frac{\partial g}{\partial x}(x,t)\right|\le \frac x{(1+tx)(1+t^2)}\le \frac b{(1+at)(1+t^2)}$ qui est intégrable sur $\ds ]0,+\infty[$.

        On en déduit par théorème de dérivation des intégrales à paramètre que $\ds f$ est de classe $\ds \mathcal C^1$ sur tout $\ds [a,b]$ et donc sur $\ds ]0,+\infty[$ et $\ds f'(x)=\int_0^{+\infty}\frac x{(1+tx)(1+t^2)}\d t$.
        






!!! theoreme "Théorème de classe $\ds \mathcal C^k$"
    si $\ds A$ et $\ds I$ sont deux intervalles de $\ds \mathbb{R}$ et $\ds f$ une fonction définie sur $\ds A \times I$, telle que :  
    - pour tout $\ds t \in I,\ x \mapsto f(x, t)$ est de classe $\ds \mathscr{C}^k$ sur $\ds A$;  
    - pour tout $\ds 0\le j<k$ et tout $\ds x \in A,\ t \mapsto \frac{\partial^jf}{\partial x^j}(x, t)$ est continue par morceaux et intégrable sur $\ds I$;  
    - pour tout $\ds x \in A, t \mapsto \frac{\partial^k f}{\partial x^k}(x, t)$ est continue par morceaux sur $\ds I$;  
    - il existe une fonction $\ds \varphi$ intégrable sur $\ds I$, telle que pour tout $\ds (x, t) \in A \times I$, on ait $\ds \left|\frac{\partial^k f}{\partial x^k}(x, t)\right| \leqslant \varphi(t)$; 
    
    alors la fonction $\ds g: x \mapsto \int_I f(x, t) \mathrm{d} t$ est de classe $\ds \mathscr{C}^k$ sur $\ds A$ et vérifie :  

    $$
    \forall 0\le j\le k, \forall x \in A, \quad g^{(j)}(x)=\int_I \frac{\partial^j f}{\partial x^j}(x, t) \mathrm{d} t
    $$

!!! preuve
    Il suffit de faire un raisonnement par récurrence en utilisant le théorème précédent.

!!! remarque
    Comme pour la continuité, la dérivation est une notion locale : on peut donc ne vérifier l'hypothèse de domination que sur tout segment inclu dans $\ds A$. 



!!! exemple 
    Exemple TRÈS IMPORTANT : fonction GAMMA
    === "Énoncé"
        On pose $\ds \Gamma(x)=\int_0^{+\infty}t^{x-1}e^{-t}\d t$. Quel est le domaine de définition de $\ds \Gamma$ ? Montrer que sur son domaine de définition, $\ds \Gamma$ est $\ds \mathcal C^\infty$ et calculer les $\ds \Gamma^{(k)}$. 
    === "Corrigé"
        On fixe $\ds x$ dans $\ds \mathbb R$. En $\ds 0$, $\ds t^{x-1}e^{-t}\sim_{t\to0}t^{x-1}$ qui est intégrable en $\ds 0$ si et seulement si $\ds x-1>-1$ c'est à dire $\ds x>0$. Si on fixe maintenant $\ds x>0$, alors $\ds t^2t^{x-1}e^{-t}=t^{x+1}e^{-t}\to_{t\to+\infty}0$. Donc $\ds t^{x-1}e^{-t}=o(\frac 1{t^2})$. On en déduit que $\ds \Gamma$ est bien définie sur $\ds \mathbb R^+_*$.

        Pour la suite, on peut se concentrer sur le théorème de classe $\ds \mathcal C^k$ des intégrales à paramètre. Nous allons avoir besoin de calculer les dérivées par rapport à $\ds x$ de la fonction $\ds g(x,t)=t^{x-1}e^{-t}$. 

        $\ds \frac{\partial g}{\partial x}(x,t)=\ln(t)t^{x-1}e^{-t}$ et de façon plus générale, $\ds \frac{\partial^jg}{\partial x^j}(x,t)=(\ln(t))^jt^{x-1}e^{-t}$.
        
        Fixons $\ds k\in\mathbb N$. Nous allons montrer que $\ds \Gamma$ est de classe $\ds \mathcal C^k$ sur tout segment $\ds [a,b]\subset]0,+\infty[$. 

        Il faut donc montrer que toutes les dérivées $\ds j$è sont continues par morceaux (ce n'est pas un problème)pour $\ds j\le k$ mais surtout intégrables pour $\ds j<k$ et il faut  ensuite majorer la dérivée $\ds k$è. 


        Soit $\ds j<k$, $\ds t^2(\ln(t))^kt^{x-1}e^{-t}=(\ln(t))^kt^{x+1}e^{-t}\xrightarrow[t\to+\infty]{} 0$ par croissances comparées. Donc $\ds t\mapsto\frac{\partial^kf}{\partial x^k}(x,t)$ est intégrable en $\ds +\infty$. 
        
        En $\ds 0$,  c'est plus délicat. Soit $\ds x\in[a,b]$. Si $\ds t<1$, alors $\ds |\ln(t)|^jt^{x-1}e^{-t}\le t^{a-1}|\ln(t)|^j$. Or $\ds a-1>-1$ c'est à dire $\ds 1-a<1$ : prenons $\ds c$ tel que $\ds 1-a<c<1$. Alors $\ds t^ct^{a-1}\ln(t)^k\xrightarrow[t\to0]{}0$ et on en déduit l'intégrabilité en $\ds 0$. 


        Ainsi les dérivées $\ds j$è sont bien intégrables sur $\ds ]0,+\infty[$. 
        
        On a de plus une majoration indépendante de $\ds x$ pour $\ds t<1$. Pour $\ds t>1$, alors $\ds |ln(t)|^kt^{x-1}e^{-t}\le |\ln(t)|^kt^{b-1}e^{-t}$ qui est un $\ds o(\frac 1{t^2})$ en $\ds +\infty$ On a donc obtenu un majoration de la dérivée $\ds k$è par une fonction $\ds \varphi(t)$ définie sur tout $\ds ]0,+\infty[$. On en déduit que $\ds \Gamma$ est bien $\ds \mathcal C^k$ et ce pour tout $\ds k$ : $\ds \Gamma$ est $\ds \mathcal C^\infty$ et en plus, $\ds \Gamma^{(k)}(x)=\int_0^{+\infty}\ln(t)^kt^{x-1}e^{-t}\d t$. 



## Exercices

### Issus de la banque CCINP

{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-025.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-026.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-027.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-029.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-030.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-049.md" %}
{% include-markdown "../../../exercicesBanquesCCINP/ccinp2023-050.md" %}

### Annales d'oraux
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1384.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1385.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1387.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1388.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1389.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1390.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-723.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-724.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-725.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-726.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-727.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-728.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-730.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-731.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-732.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-734.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-735.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-736.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-738.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1224.md" %}
{% include-markdown "../../../exercicesRMS/2022/RMS2022-1229.md" %}

### Centrale Python
{% include-markdown "../../../exercicesCentralePython/RMS2021-975.md"  rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1116.md"  rewrite-relative-urls=false %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1223.md" %}
{% include-markdown "../../../exercicesCentralePython/RMS2022-1227.md"  rewrite-relative-urls=false %}
