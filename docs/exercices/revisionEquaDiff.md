# Equations différentielles

!!! Exercice "RMS24-962"
    === "Énoncé"
        On note $(E)$ l'équation différentielle $x(1-x) y^{\prime \prime}+(1-3 x) y^{\prime}-y=0$.  
        a) Déterminer les solutions de $(E)$ non nulles développables en série entière. Préciser le rayon de convergence.  
        b) Déterminer l'ensemble des solutions de $(E)$ sur un intervalle raisonnable.  
        c) Les raccorder entre elles.


!!! Exercice "RMS24-963"
    === "Énoncé"  
        963. On note $(E)$ l'équation différentielle $x^2 y^{\prime \prime}-2 x y^{\prime}+2 y=2(1+x)$.  
        a) Trouver les solutions de l'équation homogène associée de la forme $x \mapsto x^\alpha$, où $\alpha \in \mathbb{R}$.  
        b) Trouver une solution particulière de $(E)$, d'abord sur $] 0,+\infty[$, puis sur $]-\infty, 0[$.  
        Ind. On la cherchera sous la forme $x \alpha(x)+x^2 \beta(x)$, où $\alpha$ et $\beta$ sont des fonctions de classe $\mathcal{C}^1$ telles que $x \alpha^{\prime}(x)+x^2 \beta^{\prime}(x)=0$.  
        c) L'équation $(E)$ admet-elle des solutions sur $\mathbb{R}$ ?




!!! Exercice "RMS24-968"
    === "Énoncé"  
        968. a) Résoudre $\left(1-t^2\right) y^{\prime \prime}-2 t y^{\prime}=0$ sur $\left.I=\right]-1,1[$.  
        b) Soit $f$ de classe $\mathcal{C}^2$ sur $I$ à valeurs dans $\mathbb{R}$. On pose $g(x, y)=f\left(\frac{\cos (2 x)}{\operatorname{ch}(2 y)}\right)$.

        Déterminer l'ensemble des fonctions $f$ telles que $g$ soit non constante et de laplacien nul, c'est-à-dire telles que $\frac{\partial^2 g}{\partial x^2}(x, y)+\frac{\partial^2 g}{\partial y^2}(x, y)=0$.


!!! Exercice "RMS24-1487"
    === "Énoncé"  
        1487. [CCINP] Soient $E=\mathcal{C}^2(\mathbb{R}, \mathbb{R}), P$ (resp. $I$ ) le sous-espace des fonctions paires (resp. impaires) de $E$.  
        a) Montrer que $E=P \oplus I$.  
        b) Résoudre l'équation différentielle $y^{\prime \prime}-y=\operatorname{ch} x$.  
        c) Trouver les fonctions $f \in E$ telles que $f^{\prime \prime}(x)-f(-x)=\operatorname{ch} x$ pour tout $x \in \mathbb{R}$.



!!! Exercice "RMS24-1575"
    === "Énoncé"  
        1575. [CCINP] On considère l'équation différentielle $(E): t^2 y^{\prime \prime}+t y^{\prime}+y=\frac{1}{t}+t \operatorname{sur} \mathbb{R}^{+*}$.  
        a) Énoncer le théorème de Cauchy linéaire.  
        b) On pose $g: x \mapsto f\left(e^x\right)$. Montrer que $f$ est solution de $(E)$ sur $\mathbb{R}^{+*}$ si et seulement si $g$ est solution d'une équation différentielle du second ordre que l'on déterminera.  
        c) Déterminer l'ensemble des solutions de $(E)$.  



!!! Exercice "RMS24-1577"
    === "Énoncé"  
        1577. [CCINP] Soient $A=\left(\begin{array}{ccc}-2 & -2 & 0 \\ 2 & 3 & 0 \\ 1 & 0 & 3\end{array}\right)$ et $(S)$ le système différentiel $X^{\prime}=A X$.  
        a) Montrer que $A$ est diagonalisable.  
        b) Expliciter $P$ et $D$ telles que $A=P D P^{-1}$.  
        c) On note $U=P^{-1} X$. Déterminer le système différentiel vérifié par $U$ et le résoudre.

        Déterminer alors les solutions de $(S)$.  
        d) Soit $\left(S^{\prime}\right)$ le système différentiel $X^{\prime \prime}=A X$. Déterminer les solutions réelles de $\left(S^{\prime}\right)$.  
        e) Soit $E$ l'ensemble des solutions réelles bornées de $\left(S^{\prime}\right)$. Montrer que $E$ est un espace vectoriel et déterminer sa dimension.


!!! Exercice "RMS24-1639"
    === "Énoncé"  
        1639. [ENSEA] Résoudre $y^{\prime \prime}-5 y^{\prime}+4 y=e^t \sin t$.
