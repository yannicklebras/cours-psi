

!!! exercice "RMS2022-1274"  
	=== "Enoncé"  
		 **IMT**   . Soit $P=X^n-a_{n-1}X^{n-1}+\cdots-a_0$ avec $a_0>0$ et $a_1,\dots,a_{n-1}$ réels positifs.

		- a) Montrer que $P$ admet une unique racine sur $\R^+$, que l'on note $\rho$.
		- b) Soit $z$ une racine complexe de $P$. Montrer que $|z|\leq \rho$.
		- c) Montrer que $\rho\leq \max(1, a_0+\cdots+a_{n-1})$
		- d) Montrer que $\rho\leq 1+\max (a_0,\dots ,a_{n-1})$.




	=== "Corrige"  

		On a $P(x)=x^n-a_{n-1}x^{n-1}-\dots-a_1x-a_0=x^n(1-\frac{a_{n-1}}{x}-\dots-\frac {a_0}{x^n})$ la fonction entre parenthèses est strictement croissante sur $\mathbb R^+$ et diverge vers $-\infty$ en $0$ et vers $1$ en $+\infty$. Or elle est continue sur $\mathbb R^+_*$ et d'après le TVi et ses corollaires, elle passe une unique fois par $0$.

		Ainsi, $P$ admet une unique racine positive (strictement).  Notons $\rho$ cette racine réelle strictement positive.

		Soit $z$ une racine complexe de $P$. On a alors $z^n=a_{n-1}z^{n-1}+\dots+a_1z+a_0$ et donc $|z|^n\le a_{n-1}|z|^{n-1}+\dots+a_0$. On en déduit que $P(|z|)\le 0$. Or $|z|$ est un réel positif et $P$ est croissante sur $\mathbb R^+$. On en déduit que $|z|\le \rho$.

		Tout d'abord $\rho\le 1$, c'est réglé. Concentrons nous sur le cas $\rho>1$. On doit alors montrer que $\rho\le a_{n-1}+\dots+a_1+a_0$. Puisque $P(\rho)=0$, on a $\rho^n=a_{n-1}\rho^{n-1}+\dots+a_0$. On divise par $\rho^{n-1}$ (puisque $\rho>0$) et on a  $\rho=a_{n-1}+a_{n-2}\rho^{-1}+\dots+a_0\rho^{1-n}$. Or pour une puissance $k$ négative, $\rho^{k}<1$ d'où $\rho<a_{n-1}+a_{n-2}+\dots+a_0$. On en déduit l'inégalité voulue.

		Enfin, notons $M = \max(a_0,a_1,\dots,a_{n-1})$ et $A=1+M$. On a

		$$
		\begin{align*}
		P(A)&=A^n-a_{n-1}A^{n-1}-\dots-a_1A-a_0\\
		&=M(\frac 1M A^n-\frac{a_0}{M}A^{n-1}-\dots-\frac{a_1}{M}A-\frac{a_0}{M}&\text{or }\frac {a_i}M\le 1\\
		&\ge M(\frac 1M A^n-A^{n-1}-\dots-A-1)\\
		&\ge A^n-M(A^{n-1}+\dots+A+1)\\
		&\ge A^n-M\times \frac {A^n-1}{A-1}\\
		&\ge A^n-A^n+1\\
		&\ge 1\\
		&>0
		\end{align*}
		$$

		On en déduit que $P(A)>0$ et donc $A>\rho$. Ainsi $\rho<1+M$.


!!! exercice "RMS2022-1275"  
	=== "Enoncé"  
		**CCINP**  
		Soit $a \in \R$. Pour tout $i \in [\![ 0,n]\!]$, on note $P_i=(X-a)^i$.

		  - a) Montrer que $(P_0,\dots ,P_n)$ est une base de $\R_n[X]$.
		  - b) Soit $f:P\in \R_n[X]\mapsto (X-a)(P'(X)-P'(a)) -2(P(X)-P(a))$. Montrer que $f$ est un endomorphisme. Trouver son noyau et son image.


	=== "Corrige"  

		Tout d'abord les $P_i$ forment une famille de polynômes de $\mathbb R_n[X]$, de cardinal $n+1$. Ils sont de plus échelonnés donc c'est une famille libre : c'est donc une base.

		L'application $f$ est clairement linéaire. De plus si $\mathop{deg}(P)\le n$, alors $\mathop{deg}(f(P))\le \max(\mathop{deg}((X-a)(P'-P'(a)),P-P(a))\le n$. Donc l'espace d'arrivée est bien $\mathbb R_n[X]$. On a donc défini ici un endormophisme de $\mathbb R_n[X)$.

		Soit $P$ un élément du noyau de $f$. On note $k$ son degré et $p_k$ son coefficient dominant.

		Si $P$ est constant, alors $f(P)=0$ donc $P\in\ker f$.

		Si $P$ est de degré $1$, alors $f(P)=-2p_1(X-a)$ et donc $P$ n'est pas dans le noyaux.

		Si $P$ est de degré supérieur à $2$, alors le coefficient dominant de $(X-a)(P'-P(a))$ est $kp_k$ et le coefficient de $2(P-P(a))$ est $2p_k$. Or $f(P)=0$ donc $kp_k=2p_k$ et $P$ est de degré $2$. On écrit alors $P=p_2X^2+p_1X+p_0$ et on a

		$$
		\begin{align*}
		f(P)&=(X-a)(2p_2X+p_1-2p_2a-p_1)-2(p_2X^2+p_1X+p_0-p_2a^2-p_1a-p_0)\\
		&=2p_2(X-a)(X-a)-2(p_2X^2+p_1X-p_2a^2-p_1a)\\
		&=2p_2X^2-4p_2Xa+2p_2a^2-2p_2X^2-2p_1X+2p_2a^2+2p_1a\\
		&=-2(2p_2a+p_1)X+2a(2p_2a+p_1)\\
		&=(2p_2a+p_1)(-X+a)
		\end{align*}
		$$

		Ainsi $P\in\ker f$ si et seulement si $2p_2a+p_1=0$. $P$ est donc de la forme $\alpha X^2-2a\alpha X+c$. On en déduit que le noyau de $f$ est de dimension $2$, engendré par $(X-a)^2$ et les polynômes constants.

		De plus, pour $k$ différent de $2$ et $0$, $f((X-a)^k)=k(X-a)^k-2(X-a)^k=(k-2)(X-a)^k$. Or la famille des $(X-a)^k$ est une base donc l'image de $f$ est engendrée par les $f((X-a)^k)$. On en déduit que $\mathrm{im} f=Vect((X-a)^n,(X-a)^{n-1},\dots,(X-a)^3,X-a)$.


!!! exercice "RMS2022-1276"  
	=== "Enoncé"  
		   **CCINP**  .   Calculer les dimensions de $\mathcal{A}_n(\K)$ et $\mathcal{S}_n(\K)$, sous-espaces des matrices antisymétriques et symétriques de ${\cal M}_n(\K)$. En déduire le déterminant de l'endomorphisme $u$ de $\mathcal{M}_n(\K)$ défini par $u(M)=M^{T }$.


	=== "Corrige"  

		On vérifie assez facilement que $A_n(\K)$ est engendré par les $E_{ij}-E_{ji}$ pour $i<j$. Cette famille étant évidemment libre, la dimension de $A_n(\K)$ est $\frac{n(n-1)}2$.

		De même $S_n(\K)$ est engendrée par les $E_{ij}+E_{ji}$ pour $i<j$ et les $E_{ii}$. Cette famille est une famille libre donc la dimension de $S_n(\K)$ est $\frac{n(n+1)}{2}$.

		On sait de plus que $A_n(\K)$ et $S_n(\K)$ sont supplémentaires dans $M_n(\K)$. Dans une base adaptée à cette décomposition, la matrice de l'application transposée est diagonale avec $\frac{n(n+1)}{2}$ nombres $1$ et $\frac{n(n-1)}{2}$ nombres $-1$. Le déterminant étant indépendant de la base de représentation, le déterminant de l'application transposeé est donc $(-1)^{\frac{n(n-1)}{2}}$.


!!! exercice "RMS2022-1277"  
	=== "Enoncé"  
		 **IMT**   . Soient $A$ et $B$ dans $\mathcal{M}_n(\R)$ telles que $AB=0$.

		- a) A-t-on nécessairement $BA=0$ ?
		- b) Montrer que $\tr \big((A+B)^p\big)=\tr (A^p)+\tr (B^p)$ pour tout $p\geq 1$.
		- c) Déterminer une relation entre $\rg(A)$ et $\rg(B)$.


	=== "Corrige"  

		On n'a pas forcément $BA=0$ (on peut trouver facilement un contre-exemple).

		Soit $p\ge1$, l'enchainement suivant n'est peut-être pas optimal :

		$$
		\begin{align*}
		\tr(B^{p})=&\tr((A+B-A)B^{p-1})\\
		&=\tr((A+B)B^{p-1}-AB^{p-1})\\
		&=\tr((A+B)B^{p-1}+0)\\
		&=\tr((A+B)(B+A-A)B^{p-2})\\
		&=\tr((A+B)^2B^{p-2}-AB^{p-2})\\
		&=\tr((A+B)^2B^{p-2}-0)\\
		&=\dots\\
		&=\tr((A+B)^{p-1}B)
		\end{align*}
		$$

		On a de même $\tr(A^p)=\tr(A(A+B)^{p-1})$. On en déduit $\tr(A^p)+\tr(B^p)=\tr((A+B)^{p-1}B)+\tr(A(A+B)^{p-1})=\tr((A+B)^{p-1}B)+\tr((A+B)^{p-1}A)=\tr((A+B)^{p-1}(A+B))=\tr((A+B)^p)$.

		Puisque $AB=0$, alors $\mathrm{im}(B)\subset\ker(A)$. On en déduit que $\rg(B)\le n-\rg(A)$ c'est à dire $\rg(A)+\rg(B)\le n$.


!!! exercice "RMS2022-1278"  
	=== "Enoncé"  
		 **St-Cyr**  . Soient $A,B\in \mathcal{M}_n(\R)$, avec $\rg B=1$.

		Montrer que  $\det(A+B)\det(A-B)\leq \det(A)^2$.

	=== "Corrige"  

		Tout d'abord, étudions le cas des matrices de rang $1$. Soit $b$ l'endomorphisme canoniquement associé à $B$. On note $c$ un vecteur qui engendre $\mathrm{im}(b)$. Puisque $\mathrm{rg}(b)=1$, on sait que $c \ne0$. Complétons en une base : $(c,e_2,\dots,e_n)$. La matrice de $b$ dans cette base est de la forme $\begin{pmatrix} \lambda_1& \lambda_2&\cdots &\lambda_n\\0 & 0 & \cdots &0\\\vdots &  &  & \vdots \\  0 & 0 & \cdots &0\end{pmatrix}$.

		Concentrons-nous donc sur le cas où $B$ est une matrice ligne. On a alors $\det(A+B)=\det(A)+\det(A')$ où $A'$ est la matrice $A$ dans laquelle on remplace la première ligne par la ligne de $B$. On a de la même façon $\det(A-B)=\det(A)-\det(A')$. Alors $\det(A+B)\det(A-B)=\det(A)^2-\det(A')^2\le \det(A)^2$.


!!! exercice "RMS2022-1279"  
	=== "Enoncé"  
		 **IMT**  
		Soient $A,B\in{\cal M}_ n(\C)$. On suppose $A$ inversible, $B$ nilpotente et
		$AB=BA$.
		Montrer que $A-B$ et $A+B$ sont inversibles.

	=== "Corrige"  

		 On a $AB=BA$ donc on va obligatoirement avoir une identité remarquable quelque part. Notons $k$ l'indice de nilpotence de $B$.

		On a : $A^k=A^k-B^k=(A-B)(\sum_{p=0}^{k-1}A^pB^{k-p-1})$. Or $A$ est inversible donc $(A-B)(\sum_{p=0}^{k-1}A^{p-k}B^{k-p-1})=I_n$. On en déduit que $A-B$ est bien inversible.

		Notons $k'$ le premier entier impair supérieur à $k$. On a alors $A^{k'}=A^{k'}+B^{k'}=A^{k'}-(-B)^{k'}=(A+B)((\sum_{p=0}^{k-1}A^p(-B)^{k-p-1}))$. On fait la même manipulation que précédentepour montrer que $A+B$ est bien inversible.


!!! exercice "RMS2022-1291"  
	=== "Enoncé"  
		**CCINP**  .  
		Le corps de base $\K$ est $\R$ ou $\C$. Soit $E$ un $\K$-espace vectoriel de dimension $3$, $f \in \cl(E)$ tel que $f^2 \ne 0$, $f^3 = 0$.
		 Montrer que dans une certaine base de $E$ la matrice de $f$ est $\small\begin{pmatrix}0&1&0\\0&0&1\\0&0&0\end{pmatrix}$.

	=== "Corrige"  

		Exercice très classique. $f^2 \ne0$ donc il existe $x$ tel que $f^2(x) \ne0$. On considère la famille $(f^2(x),f(x),x)$ dont on montre facilement qu'elle est libre, et contenant $3$ éléments, c'est une base. Dans cette base, la matrice de $f$ a la forme voulue.


!!! exercice "RMS2022-1293"  
	=== "Enoncé"  
		**CCINP**  .  
		Soit $E$ un $\R$-espace vectoriel de dimension finie $ n \ge 2$, $f \in \cl(E)$.

		- a) Donner un exemple d'endomorphisme pour lequel noyau et image ne sont pas supplémentaires.

		- b) Montrer que si $f$ est diagonalisable, $\Im f$ et $\Ker f$ sont supplémentaires. Que dire de la réciproque~?

		- c)
			- i) Montrer que la suite $(\dim \Ker f^k)_{k \in  }$ est croissante.

			- ii) Montrer qu'il existe $k_0$ tel que $\forall k >k_0, \Ker f^k = \Ker f^{k_0}$.

			- iii) Montrer que $\Ker f^{k_0}$ et $\Im f^{k_0}$ sont supplémentaires.

	=== "Corrige"  

		 L'endomorphisme de $\mathbb R_n[X]$  $P\mapsto P'$ a pour image $\mathbb R_{n-1}[X]$ et comme noyau $R_0[X]$ : l'un est inclus das l'autre donc ils ne sont pas supplémentaires.

		 Si l'endomorphisme est diagonalisable, alors les sous-espaces propres sont en somme directe. Ainsi le sep associé à $0$ est supplémentaire de tous les autres, qui engendrent l'image.

		 Si $x\in\ker f^k$ alors $f^{k+1}(x)=f(f^k(x))=f(0)=0$. Donc $x\in\ker f^{k+1}$. On a ainsi en particulier $\dim f^k\le \dim f^{k+1}$ : la suite est croissante.

		 C'est une suite croissante d'entiers majorée par $n$ donc elle est constante à partir d'un certain rang $k_0$. On a alors pour tout $k>k_0$, $\ker f^{k_0}\subset \ker f^{k}$ et par stabilité de la suite, on a alors $\ker f^{k_0}=\ker f^k$.

		 Il suffit de montrer que l'intersection est réduite à $0$, on aura les dimensions par théorème du rang.

		 Soit $x\in\ker f^{k_0}\cap \im f^{k_0}$. On pose $x=f^{k_0}(x_0)$. On a alors $f^{k_0}(f^{k_0}(x_0))=0$ et ainsi $f^{2k_0}(x_0)=0$. Or $\ker f^{k_0}=\ker f^{2k_0}$ donc $f^{k_0}(x_0)=0$ c'est à dire $x=0$.

		 Ainsi $\ker f^{k_0}\cap \mathrm{im} f^{k_0}=\{0\}$. On en déduit le caractère supplémentaire.


!!! exercice "RMS2022-1295"  
	=== "Enoncé"  
		**CCINP**  .  
		Pour $A,B \in \mathcal{M}_n(\R)$, on pose $\left< A,B\right>=\mathrm{Tr}(A^T  B)$.

		  - a) Montrer que l'on définit là un produit scalaire de $\mathcal{M}_n(\R)$.
		  - b) Trouver une base orthonormée pour ce produit scalaire.
		  - c) Montrer que, pour tout $A \in \mathcal{M}_n(\R)$, $\left|\mathrm{Tr}(A)\right|\leqslant \sqrt{n} \|A\|$.
		  - d) Soient $M \in \mathcal{M}_n(\R)$ et $G= \R\, I_n$. Déterminer $G^{\perp}$ et $\d(M,G^{\perp})$.

	=== "Corrige"  

		 Aucun problème pour montrer le produit scalaire. Pour le côté défini positif, il faut savoir que $\mathrm{tr}(A^TA)=\sum_{i,j}a_{ij}^2$. Dès lors si $\mathrm{tr}(A^TA)$ est nul, c'est que tous les termes de cette somme sont nuls et donc que $A=0$.

		 La famille des $E_{ij}$ est évidemment BON pour ce produit scalaire. En effet : $\mathrm{tr}(E_{ij}^TE_{ij})=\mathrm{tr}(E_{jj})=1$ et si $i \ne k$ et $j \ne l$ alors $\mathrm{tr}(E_{ij}^TE_{kl})=\mathrm{tr}(E_{ji}E_{kl})=\mathrm{\delta_{ik}E_{jl}}=\delta_{ik}\mathrm{tr}(E_{jl})=\delta_{ik}\delta_{jl}=0$.

		 On a alors $|\mathrm{tr}(A)|=|<A,I_n>|\le ||A||||I_n||=\sqrt n||A||$ par l'inégalité de Cauchy-Schwarz.

		 Soit maintenant $A\in G^\bot$, alors pour tout réel $\lambda$, $\lambda\mathrm{tr}(A)=0$. On en déduit que $G^\bot=\ker(\mathrm{tr})$. La distance à un sev est atteinte au projeté orthogonal. Or $\mathbb R I_n$ est de dimension $1$ et $\frac1{\sqrt n}I_n$ en est une base orthonormée. Le projeté orthogonal $P$ d'une matrice $M$ sur $G$ est donc $\frac 1n<M|I_n>I_n=\frac{\mathrm{tr}(M)}nI_n$. La distance de $M$ à $G$ est ainsi

		$$
		\begin{align*}
		||M-\mathrm{tr}(M)I_n||&=\mathrm{tr}(M^\top M -\frac{\mathrm{tr}(M)}{n} (M+M^T)+\left(\frac{\mathrm{tr}(M)}{n}\right)^2I_n)\\
		&=||M||^2-2\frac{\mathrm{tr}(M)^2}{n}+\frac{\mathrm{tr}(M)^2}{n}\\
		&=||M||^2-\frac{\mathrm{tr}(M)^2}{n}
		\end{align*}
		$$



!!! exercice "RMS2022-1301"  
	=== "Enoncé"  
		**IMT**   .
		On note $E=\R[X]$. Pour $n \in  $ et $P \in E$, on note $\theta _n(P)=\int  _0^1P(t)\, t^n\,\mathrm d t$.

		  - a) Montrer que, pour tout $P \in E$, $N(P)=\sup \limits _{n \in  } \left|\theta _n(P)\right|$ est bien défini.
		  - b) Montrer que $N$ est une norme sur $E$.


	=== "Corrige"  

		Soit $n\in\mathbb N$, alors $\left|\int_0^1P(t)t^ndt\right|\le\int_0^1|P(t)||t^n|dt\le \int_0^1|P(t)|dt$. On a  une majoration indépendante de $n$, on en déduit que l'ensemble $\{|\theta_n(P)|,n\in\mathbb N\}$ est une partie non vide et majorée de $\mathbb R$ : elle admet donc une borne sup.

		Pour montrer qu'il s'agit d'une norme, on doit montrer la positivité, l'homogénéité et l'inégalité triangulaire.

		Pour l'homogénéité : soit $\lambda \ne0$ (le cas $0$ est trivial), pour $n\in\mathbb N$, $|\theta_n(\lambda P)|=|\lambda||\theta_n(P)$. On a donc $\frac 1{|\lambda|}\theta_n(\lambda P)=|\theta_n(P)|$. En passant au sup à droite, $\frac1{|\lambda|}|\theta_n(\lambda P)|\le N(P)$. On a alors $|\theta_n(\lambda P)|\le |\lambda|N(P)$. La majration ne dépend pas de $n$, on peut donc passer au usup à gauche : $N(\lambda P)\le |\lambda|N(P)$.

		Pour la positivité : $N(P)$ est évidemment positive. De plus, si $N(P)=0$ alors pour tout $n$, $\theta_n(P)=0$. Soit $P=\sum_{k=0}^d p_iX^i$, alors $\sum_{k=0}^d p_i \theta_n(P)=\int_0^1P(t)^2dt$ et puisque tous les $\theta_n(P)$ sont nuls, $\int_0^1P(t)^2dt=0$. $P$ étant continue, on en déduit par stricte positivité de l'intégrale que $P=0$ sur $[0,1]$. Ainsi $P$ est un polynôme admettant une infinité de racines : $P=0$.

		Pour l'inégalité triangulaire : Soit $P$ et $Q$ deux polynômes et soit $n\in\mathbb N$, on remarque que $\theta_n(P+Q)=\theta_n(P)+\theta_n(Q)$ et donc :
		$|\theta_n(P+Q)|\le|\theta_n(P)|+|\theta_n(Q)|\le N(P)+N(Q)$. par passage au sup à droite. On a donc une majoration de $|\theta_n(P+Q)|$ indépendante de $n$ : on peut passer au sup à gauche pour obtenir $N(P+Q)\le N(P)+N(Q)$.


!!! exercice "RMS2022-1304"  
	=== "Enoncé"  
		 **IMT**   . Soient $a>0$ et $\alpha\in \R$. Nature de la série $\sum\frac{n^{\alpha}}{a(1+a)\cdots (1+a^n)}$ ?

	=== "Corrige"  




!!! exercice "RMS2022-1305"  
	=== "Enoncé"  
		 **Navale**  Soit $f\in \mathcal{C}^0(\R^+, \R)$ telle que $f(0)=1$ et $\forall x>0, \; 0\leq f(x)<1$.

		On définit $(u_n)$ par $u_0\in \R^+$ et, pour $n\in $, $u_{n+1}=u_n\,f(u_n)$.

		Montrer que $u_n\mathop{\longrightarrow}\limits_{n \to +\infty}0$. Montrer que, si $f$ est dérivable en $0$ avec $f'(0) \neq 0$, alors $\sum u_n^2$ converge.

	=== "Corrige"  

		Tout d'abord, l'intervalle $[0,1[$ est stable par $f$. Montrons que pour tout $n$, si $u_n\ge 0$. En effet, $u_0\ge 0$ et si $u_n\ge 0$ alors $u_{n+1}=u_nf(u_n)$ Or $u_n\ge 0$ et $f(u_n)\ge 0$. Donc $u_{n+1}\ge 0$.

		Ainsi, la suite est bien définie. De $f(u_n)<1$ donc $u_{n+1}<u_n$ : la suite est décroissante et positive donc convergente vers une limite $\ell$ définit par $\ell=\ell f(\ell)$, c'est à dire $\ell = 0$ ou $f(\ell)=1$. Or $f(0)=1$ et pour tout $x>0$, $f(x)<1$. On en déduit que dans tous les cas, $\ell=0$.

		Ainsi, $u_n\to 0$.

		Supposons maintenant que $f$ est dérivable en $0$ et que $f'(0) \ne0$. On a $f(u_n)=f(0)+u_n f'(0)+o(u_n)$ ou encore $u_nf(u_n)=u_n+u_n^2f'(0)+o(u_n^2)$. On en déduit $u_{n+1}-u_n=u_n^2f'(0)+o(u_n^2)$. On a ainsi $u_n^2\sim \frac1{f'(0)}(u_{n+1}-u_n)$. Posons $v_n= \frac1{f'(0)}(u_{n+1}-u_n)$ : la série de terme général $v_n$ est convergente (série télescopique) et de terme général de signe constant car $u_n$ est décroissante. On en déduit par théorèmes de comparaison des séries à termes de signe constant que $\sum u_n^2$ est convergente.


!!! exercice "RMS2022-1306"  
	=== "Enoncé"  
		**CCINP**  
		Soit $u \in \R^{ }$ définie par $u_0=1$ et $\forall n \in  , \; u_{n+1}=\sin (u_n)$.
		Pour $k \in \Z$, déterminer la nature de la série $\sum (u_n)^k$.

	=== "Corrige"  

		Tout d'abord on peut montrer par récurrence que pour tout $n\in\mathbb n$, $u_n\in[0,1]$. De plus, pour tout $x\ge 0$, $\sin(x)\le x$. On en déduit $u_{n+1}\le u_n$ : la suite est décroissante, positive, donc convergente vers un point fixe de $\sin$ : $u_n\to 0$.   

		On utilise alors une astuce courrante sur ce genre de problème : cherchons $\alpha$ tel que $u_{n+1}^\alpha-u_n^\alpha$ converge vers un réel non nul. On a $u_{n+1}^\alpha-u_n^\alpha = \sin(u_n)^\alpha -u_n^\alpha =(u_n-\frac{u_n^3}{6}+o(u_n^3))-u_n^\alpha = u_n^\alpha\left((1-\frac{u_n^2}{6}+o(u_n^2))^\alpha-1\right)\sim- u_n^\alpha\frac{\alpha}{6}u_n^2=-\frac \alpha6u_n{\alpha+2}$. En prenant $\alpha=-2$, on trouve $u_{n+1}^{-2}-u_n^{-2}\to \frac 13$. Ainsi la série des $u_{n+1}^{-2}-u_n^{-2}$ est divergente de terme général équivalent à $\frac 13$ : pour les séries divergentes, on a équivalence des sommes partielles : on trouve alors $u_N=\sum_{n=0}^{N-1}(u_{n+1}^{-2}-u_n^{-2}\sim \sum_{n=0}^{N-1}  \frac 13=\frac13N$. On en déduit $u_n^2\sim\frac{3}{N}$ c'est à dire $u_n\sim\frac{\sqrt3 }{\sqrt n}$. On a alors $u_n^k\sim A\times \frac 1{n^{k/2}}$ et par théorème de comparaison des séries à termes positifs, il y a convergence si et seulement si $k/2>1$ c'est à dire $k>2$.


!!! exercice "RMS2022-1307"  
	=== "Enoncé"  
		**IMT**   .
		Déterminer la nature de la série $\sum u_n$, avec $u_n =  \frac{(-1)^n }{n^{3/4} + \sin n}$.

	=== "Corrige"  

		 On a :

		$$
		\begin{align*}
		u_n=\frac{(-1)^n}{n^{3/4}(1+\sin(n)n^{-3/4}}\\
		&=\frac{(-1)^n}{n^{3/4}}(1-\frac{\sin(n)}{n^{3/4}}+o(\frac1{n^{3/4}})\\
		&=\underset{v_n}{\underbrace{\frac{(-1)^n}{n^{3/4}}}}+\underset{w_n}{\underbrace{\frac{(-1)^n\sin(n)}{n^{3/2}}}}+o(\frac 1{n^{3/2}}
		\end{align*}
		$$

		Or $v_n$ est le terme général d'une série convergente (par critère des séries alternées)  et $w_n$ est le terme général d'une série absolument convergente (par comparaison à une série de Riemann $\sum\frac 1{n^{3/2}}$ convergente). Donc $\sum u_n$ converge.


!!! exercice "RMS2022-1308"  
	=== "Enoncé"  
		  **CCINP**  .  
		On donne $\alpha>0$, $u_1>0$, puis $\forall n \in  ^*, u_{n+1} = \frac{(-1)^{n+1}}{(n+1)^{\alpha}} \sum_{k=1}^n u_k$, et on note $S_n = \sum_{k=1}^n u_k$.

		- a) Justifier l'existence, pour tout $n$, de $\ln(S_{n+1})$, et l'exprimer à l'aide de $\ln(S_n)$.
		- b) Donner un développement asymptotique à deux termes de $\ln\left(1 + \frac{(-1)^n}{n^{\alpha}}\right)$.
		- c) En déduire que la série $\sum u_n$ converge si $\alpha >\frac 12$.
		- d) Pour $\alpha \le \frac 12$, déterminer la limite de $\big(\ln(S_{n+1})\big)$; conclure sur la nature de la série $\sum_n u_n$.



	=== "Corrige"  

		 On a pour tout $n>0$, $u_{n+1}=\frac{(-1)^{n+1}}{(n+1)^\alpha}S_n$ et ainsi, $S_{n+1}=u_{n+1}+S_n=S_n\underset{>0}{\underbrace{(1+\frac{(-1)^n}{(n+1)^\alpha})}}$. On peut montrer alors par récurrence sur $S_n>0$ pour tout $n$. Ce qui donne  $\ln(S_{n+1})=\ln(S_n)+\ln(1+\frac{(-1)^n}{(n+1)^\alpha})$.

		 On a alors $\ln(1+\frac{(-1)^n}{(n+1)^\alpha})=\frac{(-1)^n}{n^\alpha}+\frac{1}{2n^{2\alpha}}+o(\frac 1{n^{2\alpha}})$.  

		 Si $\alpha >\frac 12$, le développement fait apparaître la somme du terme général d'un série alternée convergente et d'une série absolument convergente. On en déduit que la série de terme général $\ln(1+\frac{(-1)^n}{n^\alpha})$ est convergente. Or par une propriété du cours, cette série est de même nature que la suite $\ln(S_{n})$ : ainsi, $\ln(S_n)$ converge vers un réel $\ell$ et donc $S_n$ converge vers $e^\ell$.

		 Dans le cas où $\alpha\le \frac 12$, on a $\left(\ln(S_{n+1})-\ln(S_n)\right)-\frac{(-1)^n}{n^\alpha}\sim\frac 1{2n^{2\alpha}}$ qui est alors le terme général d'une série divergente. On en déduit que la série de terme général $\left(\ln(S_{n+1})-\ln(S_n)\right)-\frac{(-1)^n}{n^\alpha}$ est divergente vers $+\infty$. Or la série de terme général $\frac{(-1)^n}{n^\alpha}$ est convergente (par CSSA) donc la série de terme général $\ln(S_{n+1})-\ln(S_n)$ est divergente vers $+\infty$. On en déduit que $\ln(S_n)$ diverge vers $+\infty$ et de même pour $S_n$. La série des $\sum u_k$ est donc divergente..


!!! exercice "RMS2022-1309"  
	=== "Enoncé"  
		**IMT**   .
		Pour $n\in   ^*$, soit $u_n = \frac{(-1)^{\frac{n(n-1)}2}}{\sqrt{n(n+1)}}$. Nature de la série $\sum u_n$~?

		*Ind*. On pourra sommer par paquets.

	=== "Corrige"  

		La famille n'est pas à termes de signe constant et n'est pas sommable. La notion de sommation par paquets ne me semble pas s'appliquer.

		Notons $S_n$ une somme partielle et $q$ et $r$ tels que $n=4q+r$. On a alors $S_n=S_{4q}+u_{4q+1}+\dots+u_{4q+r}$. Or $S_{4q}=u_0+\sum_{k=0}^{q-1}u_{4k+1}+u_{4k+2} +\sum_{j=1}^{q-1}u_{4k+3}+u_{4k+4}$. Or :

		$$
		\begin{align*}
		u_{4k+1}+u_{4k+2} &= \frac{1}{\sqrt{4k+1}\sqrt{4k+2}}-\frac{1}{\sqrt{4k+2}\sqrt{4k+3}}\\
		&=\frac1{\sqrt{4k+2}\sqrt{4k+1}}\left(1-\frac1{\sqrt{1+\frac 2{4k+1}}}\right)\\
		&=\frac1{\sqrt{4k+2}\sqrt{4k+1}}\left(1-\frac1{\sqrt{1+\frac 2{4k+1}}}\right)\\
		&\sim\frac1{4k}\times \frac 1{4k}\\
		&\sim\frac{1}{16k^2}
		\end{align*}
		$$

		qui est le terme général d'une série convergente. De plus, $u_{4q+1}+\dots+u_{4q+r}\to 0$ car il y a un nombre fini de termes. Donc $S_n$ converge.

		De la même manière pour $\sum_{j=1}^{q-1}u_{4k+3}+u_{4k+4}$ qui converge. Ainsi $\sum u_n$ est la somme de deux séries convergentes donc elle est convergente.


!!! exercice "RMS2022-1310"  
	=== "Enoncé"  
		**Dauphine**
		Soit $f : \R \to \R$ une fonction périodique continue et $t \in \R$. Montrer qu'il existe $x \in \R$ tel que $f(x+t)=f(x)$.

	=== "Corrige"  

		Soit $f$ définie sur $\R$ périodique et continue de période $T$. Soit $t\in\mathbb R$, on cherche à montrer l'existence de $x\in\mathbb R$ tel que $f(x+t)=f(x)$. Posons $g$ la fonction définie par $g(x)=f(x+t)-f(x)$. $f$ étant périodique et continue, $g$ est aussi périodique et continue. Notons $T$ une période de $f$. $f$ est continue sur $[0,T]$ et admet donc un maximum sur $[0,T]$ en un point qu'on note $m$. Par continuiuté et périodicité, ce maximum est un maximum global. On a alors $f(m+t)-f(m)\le 0$. Mais $f(m-t+t)-f(m-t)\ge 0$. On en déduit que $g(m)$ et $g(m-t)$ sont de signes opposés. Il existe donc $x$ dans l'intervalle $[m-t,m]$ ou $[m,m-t]$ tel que $g(x)=0$ c'est à dire $f(x+t)=f(x)$.


!!! exercice "RMS2022-1336"  
	=== "Enoncé"  
		   **Dauphine**
		Soit $n \geqslant  1$. On munit $\R^n$  de sa structure euclidienne canonique.

		Soient $A \in \mathcal{S}_n(\R)$ et  $q:x\in E\mapsto  x^T   A x$. On note $C(q)=\left\{x \in E, \; q(x)=0\right\}$.

		  - a) Calculer $ abla (q)$.
		  - b)
		   Trouver le minimum et le maximum de $q$ sur la sphère unité.


	=== "Corrige"  

		Soit $x\in E$. Notons $e_i$ le $i$ème vecteur de la base canonique. On a $q(x+te_i)=(x+te_i)^\top A(x+te_i)=x^\top Ax+te_i^\top Ax+tx^\top Ae_i+t^2e_i^\top Ae_i=q(x)+2te_i^\top Ax+o(t^2)$. On en déduit $\partial_i q(x)=2e_i^\top Ax$.

		$A$ est symétrique donc diagonalisable dans une base orthogonale. Soit $(x_1,\dots,x_n)$ cette base, où $x_i$ est associé à la valeur propre $\lambda_i$. Soit $x$ un vecteur quelconque de la sphère unité et $x=\mu_1x_1+\dots+\mu_n x_n$. Le calcul donne : $q(x)=\sum_{i=1}^n \lambda_i\mu_i^2$. Notons $\lambda_+$ la plus grande valeur propre et $\lambda_-$ la plus petite valeur propre. On a alors $q(x)\le \lambda_+\sum_{i=1}^n \mu_i^2\le\lambda_+$ et $q(x)\ge\lambda_-\sum_{i=1}^n \mu_i^2\ge\lambda_-$. Ces valeurs étant atteintes pour les vecteurs propres associés, on a bien trouvé un minimum et un maximum sur la sphère unité.


!!! exercice "RMS2022-1341"  
	=== "Enoncé"  
		 **Navale**  . Soient $X$ et $Y$ deux variables aléatoires à valeurs dans un ensemble fini inclus dans $\R$. On suppose que, pour tout $k\in $, $\E  (X^k)=\E  (Y^k)$. Montrer que $X$ et $Y$ suivent la même loi.

	=== "Corrige"  

		 Tout d'abord, les valeurs $E(X^k)$ et $E(Y^k)$ sont des valeurs finies.

		 Notons $x_1,\dots,x_n$ les valeurs prises par $X$ et $y_1,\dots,y_p$ les valeurs prises paar $Y$. On pose de même $\{z_1,z_2,\dots,z_q\}$ l'union des valeurs prises par $X$ et $Y$. On peut écrire (quitte à rajouter des probas nulles) :

		 $$
		 E(X^k)=\sum_{i=1}^q z_i^q P(X=z_i)\quad \text{et}\quad E(Y^k)=\sum_{i=1}^q z_i^q P(Y=z_i).
		 $$

		 Or $E(X^k)=E(Y^k)$ donc finalement, pour tout entier naturel $k$,

		$$
		\sum_{i=1}^q z_i^q(P(Y=z_i)-P(X=z_i))=0.
		$$

		 En ne gardant que les valeurs de $k$ de $0$ à $q-1$, on obtient un système dont la matrice est une matrice de Vandermonde. Or les $z_i$ sont $q$ réels distinct donc cette matrice est inversible. La seule solution au système homogène est donc la solution nulle, ce qui se traduit ici par $P(X=z_i)=P(Y=z_i)$ pour tout $i$, les variables aléatoires $X$ et $Y$ suivent donc la même loi.


!!! exercice "RMS2022-1342"  
	=== "Enoncé"  
		**IMT**   .
		On considère une urne contenant $n$ boules numérotées de $1$ à $n$. On tire $k$ boules en même temps dans l'urne. On note
		$X$ la variable aléatoire prenant pour valeur le numéro de la plus petite boule tirée.

		- a) Déterminer la loi de $X$.
		- b) Calculer $\sum_{i=1}^{n-k+1} \binom{n-i}{k-1}$.
		- c) Calculer l'espérance de $X$.

	=== "Corrige"  

		 $X$ peut prendre des valeurs de $1$ à $n-k+1$. Pour construire un tirage correspondant à $X=i$, on choisit la boule $i$ puis les $k-1$ autres boules sont choisies parmi les boules plus grandes : $\binom{n-i}{k-1}$. On a donc $P(X=i)=\frac{\binom{n-i}{k-1}}{\binom nk}$.

		 On remarque que :

		$$
		 \begin{align*}
		 \sum_{i=1}^{n-k+1}\binom{n-i}{k-1}&=\sum_{i=1}^{n-k+1}\frac{(n-i)!}{(k-1)!(n-i-k+1)!}\\
		 &=\sum_{j=k-1}^{n-1}\binom j{k-1}\\
		 &=\sum_{j=k}^{n-1}\binom{j+1}{k}-\binom{j}{k} +1\\
		 &=\binom{n}k
		 \end{align*}
		$$

		 on en déduit que $\sum_{i=1}^{n-k+1}P(X=i)=\frac 1{\binom nk}\sum_{i=1}^{n-k+1}\binom{n-i}{k-1}=1$. ce qui nous rassure sur le raisonnement précédent.

		 On cherche maintenant à calculer l'espérance de $X$.

		$$
		 \begin{align*}
		\sum_{i=1}^{n-k+1}i\binom{n-i}{k-1}&=\sum_{i=1}^{n-k}i\left(\binom{n-i+1}{k}-\binom{n-i}{k}\right)+(n-k+1)\\
		&=\sum_{i=1}^{n-k}i\binom{n-i+1}{k}-\sum_{i=1}^{n-k}i\binom{n-i}{k}+(n-k+1)\\
		&=\sum_{i=0}^{n-k-1}(i+1)\binom{n-i}{k}-\sum_{i=1}^{n-k}i\binom{n-i}{k}+(n-k+1)\\
		&=\sum_{i=1}^{n-k-1}\binom{n-i}{k}-(n-k)+(n-k+1)+\binom nk\\
		&=\sum_{i=1}^{n-k-1}\binom{n-i}{k}+1+\binom nk\\
		&=\sum_{i=1}^{n-k-1}\binom{n-i}{k}+\binom{n-(n-k)}k+\binom nk\\
		&=\sum_{i=1}^{n-k}\binom{n-i}k+\binom nk\\
		&=\binom{n}{k+1}+\binom nk
		\end{align*}
		$$

		On en déduit que $E(X)=\frac{\binom{n}{k+1}+\binom nk}{\binom nk}=\frac{n+1}{k+1}$


!!! exercice "RMS2022-1346"  
	=== "Enoncé"  
		 **IMT**Soit $x\in\R$.  Calculer $\sum_{k=0}^n {n\choose k}\cos(kx)$.

	=== "Corrige"  

		On passe en complexes en remplaçant $\cos(kx)$ par $e^{ikx}$.

		$$
		\begin{align*}
		\sum_{k=0}^n \binom nke^{ikx}&=(1+e^{ix})^n\\
		&=e^{i\frac n2x}(2\cos(\frac x2))^n\\
		\end{align*}
		$$

		Et ainsi $\sum_{k=0}^n \binom nk \cos(kx)=2^n\cos(\frac{nx}2)\cos^n(\frac x2)$.


!!! exercice "RMS2022-1347"  
	=== "Enoncé"  
		  **CCINP**  
		Soient $E$ un espace vectoriel de dimension finie, $p$ et $q$ deux endomorphismes de $E$ tels que $p+q=\id  $ et $\rg p+\rg q\leq \dim E$.

		   - a)   Montrer que $E=\im p \oplus \im q$.
		   - b)   Montrer que $p$ et $q$ sont des projecteurs.


	=== "Corrige"  

		Soit $x\in E$, alors $x=p(x)+q(x)\in\mathrm{im}(p)+\mathrm{im}(q)$. Donc $E=\mathrm{im}(p)+\mathrm{im}(q)$.

		Or $\dim(E)=\dim(\mathrm{im}(p)+\mathrm{im}(q))=\dim(\mathrm{im}(p))+\dim(\mathrm{im}(q))-\dim(\mathrm{im}(p)\cap\mathrm{im}(q))\le \dim(E)-\dim(\mathrm{im}(p)\cap\mathrm{im}(q))$. On en déduit que $\dim(\mathrm{im}(p)\cap\mathrm{im}(q))\le 0$. Or une dimension est positive donc $\dim(\mathrm{im}(p)\cap\mathrm{im}(q))=0$ et la somme est bien directe.

		On a la relation $p=id-q$ donc $p^2=(id-q)-q+q^2$ c'est à dire $p^2-p=q^2-q$ : $p$ est un projecteur ssi $q$ est un projecteur.

		Montrons donc que $p$ est un projecteur : l'exercice nous pousse en quelques sortes à montrer que $p$ est le projecteur sur $\mathrm{im}(p)$ parallèlement à $\mathrm{im}(q)$.

		Soit $x\in E$, on pose $x=x_p+x_q$ avec $x_p\in\mathrm{im}(p)$ et $x_q\in\mathrm{im}(q)$. On a alors $p(x)=p(x_p)+p(x_q)$

		Soit $x\in\mathrm{im}(p)$, notons $x=p(x_p)$. On a alors $p(x)=p(p(x_p))=p(x_p)-q(p(x_p))$ et donc $q(p(x_p))=p(x_p)-p(x)$. Puisque $\mathrm{im}(p)\cap\mathrm{im}(q)=\{0\}$, on peut affirmer que $q(p(x_p))=0$ c'est à dire $q(x)=0$.

		On a alors $p(x)=x-q(x)=x$.


		Si maintenant $x\in\mathrm{im}(q)$, on montre de même que $p(x)=0$. On a ainsi, si $x=x_p+x_q$, $p(x)=p(x_p)+p(x_q)=x_p+0=x_p$ : c'est la défoinition du projecteur sur $\mathrm{im}(p)$ parallèlement à $\mathrm{im}(q)$


!!! exercice "RMS2022-1348"  
	=== "Enoncé"  
		  **CCINP**  .  
		Soient $a\in\R$ et  $\Phi:P\in\R[X]\mapsto (X-a)(P'-P'(a))-2(P-P(a))$.

		   - a)   Montrer que $\Phi$ est un endomorphisme de $\R[X]$.
		   - b)   Montrer qu'il existe un entier $k\in ^*$ tel que $(X-a)^k$ divise $\Phi(P)$ pour tout $P\in\R[X]$. Trouver le plus grand entier $k$ qui vérifie cette condition.
		   - c)   Déterminer le noyau et l'image de $\Phi$.


	=== "Corrige"  

		Pour tout polynôme $P$, $(X-a)|P-P(a)$ ($a$ est racine). On en déduit en particulier ici que $(X-a)$ divise toujours $\Phi(P)$. Donc $k=1$ convient.

		Soit $i$ un entier naturel $>1$. On a $\Phi((X-a)^i)=i(X-a)^i-2(X-a)^i=(i-2)(X-a)^i$. On a de plus $\Phi(1)=0$ et $\Phi(X-a)=-2(X-a)$. La seule valeur non nul de $k$ qui convient est $k=1$.

		D'après les calculs précédents, on trouve que $1$ et $(X-a)^2$ sont dans le noyau et que pour $i \notin\{0,2\}$, $\Phi((X-a)^i)=(i-2)(X-a)^i$ qui forment une famill échelonnée. On a donc un noyau de dimension au moins 2 et une image de dimension au moins $n-1$. La somme de ces dimensions étant $n+1$, on a en fait égalité et $\ker(\phi)=Vect(1,(X-a)^2)$ et $\mathrm{im}(\Phi)=Vect((X-a),(X-a)^3,\dots,(X-a)^n)$.


!!! exercice "RMS2022-1352"  
	=== "Enoncé"  
		  **CCINP**  

		   - a)   Montrer que $f:M\mapsto M-{M^T}$ est un endomorphisme de $\mathcal{M}_n(\R)$.
		   - b)   Déterminer le noyau de $f$ et préciser sa dimension. Est-ce que $f$ est bijectif?
		   - c)   Calculer $f(M)$ pour $M$ antisymétrique.
		   - d)   Montrer que $\mathcal{M}_n(\R)=\mathcal{S}_n(\R)\oplus \mathcal{A}_n(\R)$.
		   - e)   Étudier la diagonalisabilité de $f$.

	=== "Corrige"  

		 La première question est triviale. Le noyau de $f$ est égal à l'ensemble des matrices symétriques : il est de dimension $\frac{n(n+1)}{2}$. $f$ n'est donc pas bijective.

		 Si $M$ est antisymétrique alors $f(M)=2M$.

		 la question $d$ c'est du cours.

		 On se place alors dans une base adaptée à cette décomposition : Si $M\in S_n(\mathbb R)$, alors $f(M)=0$ et si $M\in A_n(\mathbb R)$ alors $f(M)=2M$ Donc dans une base adaptée, la matrice de $f$ est diagonale avec $\frac{n(n+1)}{2}$ fois la valeur $0$ et $\frac{n(n-1)}2$ fois la valeur $2$.


!!! exercice "RMS2022-1359"  
	=== "Enoncé"  
		  **CCINP**  .  
		Soit $E=\R_2[X]$.

		 Pour  $P$, $Q\in E$, on pose  $f(P,Q)=P(1)\, Q(1)+P'(1)\, Q'(1)+P''(1)\, Q''(1)$.

		   - a)   Montrer que $f$ est un produit scalaire sur $E$.
		   - b)   Déterminer une base orthonormée de $E$.
		   - c)   Exprimer les coordonnées d'un polynôme $P\in E$ dans cette base. Que remarque-t-on?

	=== "Corrige"  

		C'est un produit scalaire : pour $P\in E$, on a $f(P,P)=P(1)^2+P'(1)^2+P''(1)^2$ et donc $f(P,P)=0$ ssi $P(1)=P'(1)=P''(1)=0$. On en déduit que $1$ est racine triple et puisque $P\in E$, alors $P=0$.

		On applique l'algorithme d'orthonormalisation de Schmidt. On note $R_0,R_1,R_2$ la base orthonormée.

		$R_0=0$.

		$R_1^* = X-<X|1>1=X-1$ et $R_1=\frac{X-1}{1}=X-1$

		$R_2^* = X^2-<X^2|X-1>(X-1)-<X^2|1>1=X^2-2(X-1)-1=X^2-2X+1=(X-1)^2$ et $R_2=\frac 12(X-1)^2$.


		Soit $P$ un polynôme de $R_2[X]$, on obtient les coordonnées en calculant $<P|R_i>$ : $<P|1>=P(1)$, $<P|X-1>=P'(1)$ et $<P|\frac1{2}(X-1)^2>=P''(1)$. On obtient les coefficients de la formule de Taylor pour les polynômes.


!!! exercice "RMS2022-1360"  
	=== "Enoncé"  
		 **CCINP**

		- a) Montrer que $(A,B)\mapsto \tr(A^T B)$ est un produit scalaire sur ${\cal M}_2(\R)$.

		- b) Montrer que ${\cal E}=\left\{ \small\begin{pmatrix}a&b\\-b&a\end{pmatrix};\ (a,b)\in\R^2\right\}$ est un sous-espace
		vectoriel de ${\cal M}_2(\R)$.

		- c) Donner une base orthonormée de ${\cal E}^{\perp}$.

		- d) Déterminer la distance de $\small\begin{pmatrix}1&1\\1&1\end{pmatrix}$ à ${\cal E}^{\perp}$.

	=== "Corrige"  

		 Les deux premières questions sont des questions de cours.

		 Cherchons une base orthonormée de $E^\bot$. Soit $B=\begin{pmatrix} \alpha & \beta \\ \gamma & \delta\end{pmatrix}$ orthogonale à $E$, alors en particulier, $\tr(I_n B)=0$ donc $\alpha+\delta=0$ et $\tr(\begin{pmatrix}0&-1\\1&0\end{pmatrix} B)=0$ donc $-\gamma+\beta=0$. Ainsi, $E$ est de dimension $2$, $E^\bot$ est de dimension 2, $\begin{pmatrix}1 & 0 \\ 0 & -1\end{pmatrix}$ et $\begin{pmatrix}0 & 1 \\ 1 & 0\end{pmatrix}$ sont deux matrices de $E^\bot$ formant une famille libre : C'est une base de $E^\bot$. Une BON est donc donnée par : $\left(\frac 1{\sqrt2}\begin{pmatrix}1 & 0 \\ 0 & -1\end{pmatrix},\frac1{\sqrt2}\begin{pmatrix}0 & 1 \\ 1 & 0\end{pmatrix}\right)$.

		 On a enfin $A=\begin{pmatrix} 1&1\\1&1\end{pmatrix}=\underset{\mathcal E}{\underbrace{I_2}}+\underset{\mathcal E^\bot}{\underbrace{\begin{pmatrix}0 & 1\\ 1 & 0\end{pmatrix}}}$. Donc $d(A,\mathcal E^\bot)=||I_2||=\sqrt2$.


!!! exercice "RMS2022-1361"  
	=== "Enoncé"  
		 **IMT**Soit $E=\R_n[X]$. On considère $n+1$ nombres réels distincts $a_0,\ldots,a_n$ et on pose, pour $P,Q\in E$,
		$\phi(P,Q)=\sum_{k=0}^n P(a_k)\, Q(a_k)$.


		- a) Montrer que $\phi$ définit un produit scalaire sur $E$.

		- b) On pose $F=\left\{P\in E,\ P(a_0)+\cdots +P(a_n)=0\right\}$. Déterminer $F^{\perp}$.

		- c) Soit $P\in E$. Déterminer la distance de $P$ à $F$.

	=== "Corrige"  

		 On trouve que c'est un roduit scalaire : pour le caractère défini positif, on a $n+1$ racines pour un polynôme de degré $n$ : c'est trop.

		 $F$ est clairement le noyau de la forme linéaire : $P\mapsto P(a_0)+\cdots+P(a_n)$. Son orthogonal est donc de dimension $1$. Soit $P\in F$ et soit $Q=1$, alors $<P|Q>=P(a_0)+\cdots+P(a_n)=0$. On en déduit $F^\bot=Vect(1)=\mathbb R$. Calculons pour un polynôme $P$ quelconque, $<P|1> =\sum P(a_i)$. On en déduit que $d^2(P,F)=||P-\frac{<P|1>}{||1||^2}||^2=||P-\frac{<P|1>}{||1||^2}||^2=||P||^2-\frac 2{n+1}<P|1>^2+\frac{<P|1>^2}{n+1}=||P||^2-\frac{<P|1>^2}{n+1}$.


!!! exercice "RMS2022-1363"  
	=== "Enoncé"  
		  **IMT**   .   
		Montrer que, pour toute matrice $A\in\mathcal{S}_n(\R)$, $\tr(A)^2\leq \rg(A)\tr(A^2).$


	=== "Corrige"  

		On va devoir admettre un résultat de deuxième année : $A$ étant symétrique, elle est diagonalisable, c'est à dire qu'il existe une matrice inversible $P$ telle que $P^{-1}AP=D$ avec $D$ diagonale. Or la trace et le rang sont invariants par similitude donc l'inégalité $\tr(A)^2\leq \rg(A)\tr(A^2)$ est strictement équivalente dans ce cas à l'inégalité $\tr(D)^2\leq \rg(D)\tr(D^2)$. Notons $\lambda_1,\dots,\lambda_n$ les coefficients diagonaux de $D$ et $r$ le nombre de $\lambda_i$ non nuls : le rang de $D$ est donné par $r$. On a de plus $\tr(D)=\sum\lambda_i$ et $\tr(D^2)=\sum\lambda_i^2$. Posons $\varepsilon_i=\begin{cases}1&\text{si }\lambda_i \ne0\\0&\text{sinon}\end{cases}$. On a :

		$$
		\begin{align*}
		\tr(D)^2&=\left(\sum_{i=1}^n\lambda_i\right)^2\\
		&=\left(\sum_{i=1}^n\varepsilon_i\lambda_i\right)^2\\
		&\underset{\text{Cauchy Schwarz}}{\le} \sum_{i=1}^n\lambda_i^2\sum_{i=1}^n\varepsilon_i^2\\
		&\le (\tr(D))^2\rg(D)
		\end{align*}
		$$

		D'où l'inégalité recherchée.



!!! exercice "RMS2022-1366"  
	=== "Enoncé"  
		 **IMT**

		- a) Montrer que la matrice d'une projection orthogonale dans une base orthonormée est une matrice symétrique.

		- b) Montrer que toute matrice $A\in{\cal M}_n(\R)$ vérifiant $A^2=A$ et $A^T=A$ est la matrice d'une projection orthogonale
		dans la base canonique de $\R^n$.

		- c) Que dire de la matrice $M=\small\frac{1}{14}\begin{pmatrix}1&2&3\\2&4&6\\3&6&9 \end{pmatrix}$ ?

	=== "Corrige"  

		 Soit $p$ une projection orthogonale sur un sev $F$. Soit $e_1,\dots,e_n$ une base orthonormée. Alors $p(e_k)=<p(e_k)|e_1>e_1+\cdots+<p(e_k)|e_n>e_n$.  Soit $A$ la matrice de $p$ dans la base $(e_1,\dots,e_n)$ alors $a_{ij}=<p(e_j)|e_i>$ et $a_{ji}=<p(e_i)|e_j>$. Pour $i\in\{1,\dots,n\}$, notons $e_i=e_i^F+e_i^\bot$ sa décomposition dans $F\oplus F^\bot$.  On a $p(e_i)=e_i^F$ et $<p(e_i)|e_j>=<e_i^F|e_j^F+e_j^\bot>=<e_i^F|e_j^F>$. Par symétrie on a aussi $<p(e_j)|e_i>=<e_j^F|e_i^F>=<e_i^F|e_j^F>=<p(e_i)|e_j>$. On en déduit ainsi que $a_{ij}=a_{ji}$ et donc que $A$ est symétrique.


		 Soit maintenant $A$ telle que $A^2=A$ : on sait que $A$ est la matrice d'une projection $p$. On cherche donc à montrer que le fait que $A^\top=A$ permet d'affirmer que cette projection est orthogonale.
		 Soit $x\in \mathbb R^n$, on note $X$ la matrice colonne correspondant à ses coordonnées dans la base canonique. Prenons donc $x$ et $y$ dans $\mathbb R^n$ de matrice colonne associées $X$ et $Y$ et supposons $x\in\ker p$ et $y\in\im p$ : on a donc $AY=Y$ et $AX=0$. Calculons $<x|y>=X^\top Y=X^\top AY=X^\top A^\top Y=(AX)^\top Y=0Y=0$. Donc le noyau et l'image de $p$ sont orthogonaux : $p$ est une projection orthogonale.

		 La matrice $M$ est symétrique et $M^2=M$ donc $M$ est la matrice d'une projection orthogonale. Son noyau a pour équation $x+2y+3z=0$ : c'est un plan, et son image est donc l'orthogonal de ce plan à savoir le vecteur $(1,2,3)$.


!!! exercice "RMS2022-1368"  
	=== "Enoncé"  
		 **CCINP** Soit $A\in{\cal M}_n(\R)$ telle que $A(A^T A)^2=I_n$.

		- a) Montrer que $A$ est inversible.

		- b) Montrer que $A$ est symétrique.

		- c) En déduire que $A=I_n$.


	=== "Corrige"  

		$A$ est clairement inversible, d'inverse $(A^\top A)^2$.

		Passons l'égalité à la transposée :

		$$
		\begin{align*}
		(A^{-1})^\top &= (A^\top A)^2)^\top\\
		&=(A^\top AA^\top A)^\top\\
		&=(A^\top AA^\top A)\\
		&=A^{-1}
		\end{align*}
		$$

		Ainsi $A^{-1}$ est symétrique donc $A$ est symétrique.

		La condition de l'énoncé devient alors $A^5=I_n$. On a besoin d'un résultat de deuxième année : $A$ étant symétrique, elle est diagonalisable : il existe $P$ inversible réelle telle que $P^{-1} AP=D$. En multipliant à gauche par $P^{-1}$ et à droite par $P$, l'équation $A^5=I_n$ devient $D^5=I_n$ (c'est une équation équivalente. Si on note $\lambda_1,\dots,\lambda_n$ les coefficients diagonaux de $D$, on a sur chaque terme diagonal $\lambda_i^5=1$ c'est à dire $\lambda_i=1$. $A$ est donc semblable à la matrice identité : $A=I_n$.


!!! exercice "RMS2022-1369"  
	=== "Enoncé"  
		 **IMT**
		Nature de $\sum_{n\ge 2} u_n$, où $u_n=\arctan\left(\frac{n+1}{n-1}\right)-\frac{\pi}{4}$.

	=== "Corrige"  

		Tout d'abord, $u_n\to 0$ donc il n'y a pas divergence grossière.

		On remarque que $\tan(u_n)=\frac{\frac{n+1}{n-1}-1}{1+\frac{n+1}{n-1}}=\frac 1n$. Or $u_n\to 0$ donc $\tan(u_n)\sim u_n$ : $u_n\sim\frac 1n$ et par théorème de comparaison des séries à termes positifs, $\sum u_n$ diverge.


!!! exercice "RMS2022-1371"  
	=== "Enoncé"  
		 **IMT**Trouver un équivalent de $\sum_{k=n+1}^{2n} \frac{1}{\sqrt{k}}$ en utilisant :

		- i) une comparaison série-intégrale,

		- ii) les sommes de Riemann.


	=== "Corrige"  

		la fonction $x\mapsto \frac 1{\sqrt x}$ est décroissante. On a donc de façon assez classique $\int_{k}^{k+1}\frac 1{\sqrt x}dx\le \frac 1{\sqrt k}\le \int_{k-1}^k\frac 1{\sqrt x}dx$. En sommant de $n+1$ à $2n$, on obtient :
		$$
		\int_{n+1}^{2n}\frac 1{\sqrt x}dx\le \sum_{k=n+1}^{2n}\frac 1{\sqrt k}\le \int_{n}^{2n-1}\frac 1{\sqrt x}dx
		$$

		Or $\int_{n+1}^{2n}\frac 1{\sqrt x}dx=2(\sqrt{2n}-\sqrt{n+1})=2\sqrt n(\sqrt 2-\sqrt{1+\frac 1n})\sim2\sqrt n(\sqrt 2-1)$ et $\int_{n}^{2n-1}\frac 1{\sqrt x}dx=2(\sqrt{2n-1}-\sqrt n)=2\sqrt n(\sqrt{2-\frac 1n}-1)\sim2\sqrt n(\sqrt 2-1)$. Par conservation de l'équivalent par encadrement, $\sum_{k=n+1}^{2n}\frac 1{\sqrt k}\sim 2\sqrt n(\sqrt 2-1)$.

		par les sommes de Riemann, on a :

		$$
		\begin{align*}
		\sum_{k=n+1}^{2n}\frac 1{\sqrt k}&=\sum_{k=1}^n\frac1{\sqrt{k+n}}\\
		&=\frac 1{\sqrt n}\sum_{k=1}^n\frac 1{\sqrt{1+\frac kn} }\\
		&=\sqrt n\frac 1n\sum_{k=1}^n\frac 1{\sqrt{1+\frac kn} }\\
		\end{align*}
		$$

		Or par somme de Riemann, $\frac 1n\sum_{k=1}^n\frac 1{\sqrt{1+\frac kn} }\to \int_0^1\frac1{\sqrt{1+x}}dx=2(\sqrt 2-1)$. On en déduit que $\sum_{k=n+1}^{2n}\frac 1{\sqrt k}\sim2\sqrt n(\sqrt 2-1)$.


!!! exercice "RMS2022-1373"  
	=== "Enoncé"  
		 **IMT**
		Soit $f\, :\, x\mapsto x+\ln(1+x)$.

		- a) Montrer que $f$ est une bijection de son ensemble de définition sur un intervalle à préciser. On note $g$ la bijection réciproque.

		- b) Donner $g(0)$ et $g'(0)$.

		- c) Montrer que $g$ admet un développement limité à tout ordre en $0$.

		- d) Calculer le développement limité de $g$ à l'ordre $3$ en $0$.

	=== "Corrige"  

		$f$ est définie sur l'intervalle $]-1,+\infty[$. Elle est dérivable comme somme de fonctions dérivables et $f'(x)=1+\frac 1{1+x}>0$ sur cet intervalle. On en déduit que $f$ est strictement croissante donc injective. De plus $\lim_{x\to -1}f(x)=-\infty$ et $\lim_{x\to +\infty}f(x)=+\infty$. Ainsi $f$ réalise une bijection de $]-1,+\infty[$ dans $\mathbb R$.

		Soit $g$ la bijection réciproque de $f$. On a $f(0)=0$ donc $g(0)=g(f(0))=id(0)=0$. D'après les résultats habituels d'analyse, $f$ étant dérivable en $0$ alors $g$ est dérivable en $f(0)=0$ de dérivée : $g'(0)=\frac1{f'\circ f(0)}=\frac 1{f'(0)}=\frac 12$.

		Enfin, $f$ est en fait $\mathcal C^{\infty}$ sur $]-1,+\infty[$ et sa dériviée ne s'annule pas sur cet intervalle. On en déduit que $g$ est aussi $\mathcal C^{\infty}$ et en particulier en $0$. Donc $g$ admet un DL en $0$ à tout ordre.

		Ecrivons un DL de $g$ en $0$ à l'ordre $3$ :

		$$
		g(x)=0+\frac 12x+ax^2+bx^3+o(x^3).
		$$

		Ecrivons le DL de $f$ à l'ordre $3$ :

		$$
		f(x)=x+\ln(1+x)=x+(x-\frac 12x^2+\frac 13x^3+o(x^3))=2x-\frac 12x^2+\frac 13x^3+o(x^3).
		$$

		Par composition on obtient un DL de $f\circ g$ :

		$$
		\begin{align*}
		f\circ g(x)&=2(\frac 12x+ax^2+bx^3)-\frac12(\frac 12x+ax^2+bx^3)^2+\frac 13(\frac 12x+ax^2+bx^3)^3+o(x^3)\\
		&=x+ax^2+bx^3-\frac 12(\frac 14x^2+ax^3)+\frac 1{24}x^3+o(x^3)\\
		&=x+(a-\frac 18)x^2+(b-\frac a2+\frac 1{24})x^3+o(x^3)
		\end{align*}
		$$

		Or $f\circ g(x)=x$, on doit donc avoir :

		$$\begin{cases}
		a-\frac 18 &=0\\
		b-\frac a2+\frac 1{24}&=0
		\end{cases}
		$$

		c'est à dire $a=\frac 18$ et $b=\frac 1{48}$. Le DL à l'ordre $3$ de $g$ est donc

		$$g(x)=\frac 12 x+\frac 18 x^2+\frac 1{48} x^3+o(x^3).$$



!!! exercice "RMS2022-1383"  
	=== "Enoncé"  
		 **CCINP**
		On pose pour $n\in $ : $I_n=\int_0^{\pi/4} (\tan x)^n \mathrm d x$.

		- a) Déterminer $\lim_{n\to +\infty}I_n$.

		- b) Calculer $I_n+I_{n+2}$.

		- c) En déduire $\sum_{n=0}^{+\infty} \frac{(-1)^n}{2n+1}$. Ind. Faire apparaître un télescopage.

		- d) Prouver la convergence de $\sum (-1)^n I_n$ et calculer sa somme.

	=== "Corrige"  

		 Posons $u=\tan(x)$, on a $du = (1+u^2)dx$. Appliquons ce changement de variable :

		$$
		\begin{align*}
		I_n&=\int_{0}^{\frac\pi4}(\tan x)^n dx\\
		&=\int_{0}^1 \frac{u^n}{1+u^2}du\\
		&=\left[\frac1{n+1}\frac{u^{n+1}}{1+u^2}\right]_0^1-\frac 1{n+1}\int_0^1\frac{u^{n+1}}{(1+u^2)^2}du
		\end{align*}
		$$

		Or $\int_0^1\frac{u^{n+1}}{(1+u^2)^2}du\le\int_0^1\frac{1}{(1+u^2)^2}du$ ce qui assure la convergence de tous les termes vers $0$.

		Ainsi $I_n$ converge vers $0$.

		On essaye d'obtenir une raltion de récurrence sur $I_n$ :

		$$
		\begin{align*}
		I_n+I_{n+2} &= \int_0^{\frac\pi4}(\tan x)^n(1+\tan^2(x))dx\\
		&=\left[\frac 1{n+1}(\tan x)^{n+1}\right]_0^{\frac\pi4}\\
		&=\frac 1{n+1}
		\end{align*}
		$$

		On doit maintenant utiliser la question précédente. On peut remarquer que $\frac 1{2n+1}=I_{2n}+I_{2n+2}$ et donc $\sum_{n=0}^N \frac{(-1)^n}{2n+1}=\sum_{n=0}^N(-1)^n(I_{2n}+I_{2n+2})=I_0+I_2-I_2-I_4+I_4+I_6-\dots +(-1)^NI_2N+(-1)^NI_{2N+2}=I_0+(-1)^NI_{2N+2}$. Par ailleurs $I_{2N+2}\to 0$ donc $\sum_{n=0}^{+\infty}\frac{(-1)^n}{2n+1}=I_0=\frac\pi4$.

		Enfin, $I_n$ est positive, décroissante et converge vers $0$. D'après le CSSA, $\sum(-1)^nI_n$ converge.


!!! exercice "RMS2022-1391"  
	=== "Enoncé"  
		 **IMT**On lance une pièce donnant pile avec probabilité $\frac{2}{3}$. On note $X$ le nombre de lancers
		nécessaires pour obtenir deux piles consécutifs. On pose $a_n=\P(X=n)$.

		- a) Calculer $a_1$ et $a_2$.


		- b) À l'aide de la formule des probabilités totales, montrer : $a_n=\frac{1}{3}a_{n-1}+\frac{2}{9}a_{n-2}$.

		- c) Montrer que le jeu se termine presque sûrement.

		- d) L'espérance de $X$ est-elle finie ? Si oui, la calculer.

	=== "Corrige"  

		$a_1=P(X=1)=0$ car au coup $1$ on ne peut pas avoir 2 piles consécutifs.

		$a_2=P(X=2)=P(P_1\cap P_2)=P(P_1)P(P_2)=\frac 23\times\frac 23=\frac 49$. en notant $P_i$ l'évènement \og Avoir pile au lancer $i$\fg.

		On va appliquer la formule des probas total en distinguant les 2 premiers coups. L'ensemble d'évèments $\{P_1\cap P_2,P_1\cap \bar P_2,\bar P_1\}$ est une système complet d'évènements. Par ailleurs, en prenant $n> 2$ :

		- $P(X=n|P_1\cap P_2)=0$ car le jeu s'est arrêté au rang $2$
		- $P(X=n|\bar P_1)=P(X=n-1)$ car le fait de ne pas tirer pile au premier lancer revient à relancer une partie avec un coup en moins
		- $P(X=n|P_1\cap \bar P_2)=P(X=n-2)$ puisque le fait d'avoir obtenu face au rang $2$ relancer une partie avec $2$ coups en moins.

		On a donc $P(X=n)=P(X_n|P_1\cap P_2)P(P_1\cap P_2)+P(X=n|P_1\cap\bar P_2)P(P_1\cap\bar P_2)+P(X=n|\bar P_1)P(\bar P_1)=0+P(X=n-2)\times \frac 29+P(X=n-1)\times\frac 13$. On obtient donc pour $n> 2$ : $a_n=\frac 13 a_{n-1}+\frac 29 a_{n-2}$.

		On a alors une relation de récurrence d'ordre deux, de polynôme caractéristique $X^2-\frac 13X-\frac 29$ dont le discriminant est $\Delta = 1$. Ses racines sont alors $r_{1}=\frac{\frac 13+ 1}{2}=\frac 23$ et $r_2=\frac{\frac 13-1}{2}=-\frac 13$.

		Précisons $\alpha$ et $\beta$ : $a_1=0$ donc $\frac 23\alpha-\frac 13\beta=0$ et $a_2=\frac 49$ donc $\frac 49=\frac 49\alpha+\frac 19\beta$. On obtient $\beta =\frac 43$ et $\alpha = \frac 23$.

		On a alors $a_n=(\frac 23)^{n+1}+\frac 43(-\frac 13)^n$. On peut calculer avec des séries géométriques que $\sum_{k\ge 1}a_n=1$ donc la probabilité que le jeu soit fini est $1$ : le jeu termine presque sûrement.

		L'espérance de $X$ serait égale à $\sum_{k=2}^{+\infty}ka_k$. Or $ka_k=k\left(\frac 23\right)^{k+1}-4k\left(-\frac 13\right)^{k+1}$ et $k\left(\frac 23\right)^{k+1}$ et $k\left(-\frac 13\right)^{k+1}$ sont les termes généraux de deux séries convergentes : l'espérance existe.

		De manière générale pour $\alpha<1$ :

		$$
		\sum_{k=2}^{+\infty}kr^{k+1}=r^2\sum_{k=2}^{+\infty}\sum_{p=0}^{k-1}r^pr^{k-1-p}=r^2\sum_{k=1}^{+\infty}\sum_{p=0}^{k}r^pr^{k-p}=r^2\sum_{k=0}^{+\infty}\sum_{p=0}^{k}r^pr^{k-p}-r^2
		$$

		On reconnaît un produit de Cauchy :
		$$
		\sum_{k=2}^{+\infty}kr^{k+1}=r^2\left(\sum_{k=0}^{+\infty}r^k\right)^2-r^2=r^2\times \frac{1}{(1-r)^2}-r^2=\frac{r^2(2r-r^2)}{(1-r)^2}
		$$

		En particulier pour $r=\frac 23$ : $\sum_{k=2}^{+\infty} kr^{k+1}=\frac {32}9$ et pour $r=-\frac 13$, $\sum_{k=2}^{+\infty} kr^{k+1}=-\frac 7{144}$. On peut alors donner l'espérance de $X$ :

		$$
		E(X)=\frac {32}9+4\times \frac7{144}=\frac{128}{36}+\frac 7{36}=\frac{135}{36}
		$$

		résultat vérifié numériquement :
		```Python
		def partie() :
		    coup1 = (rd.rand()<2/3)
		    coup2 = rd.rand()<2/3
		    n=2
		    while not(coup1 and coup2) :
		        coup1=coup2
		        coup2 = rd.rand()<2/3
		        n=n+1
		    return n

		def moyenne(n) :
		    l=[]
		    for i in range(n) :
		        l.append(partie())
		    return sum(l)/n

		>>> moyenne(1000000)
		3.751457
		```



!!! exercice "RMS2022-1393"  
	=== "Enoncé"  
		 **CCINP**

		- a) Résoudre, dans $\C$, l'équation $z^{n}=\mathrm{e}^{i\pi/3}$.
		- b) Résoudre, dans $\C$, l'équation $\left(\dfrac{z-1}{z+1}\right)^{n}+\left(\dfrac{z+1}{z-1}\right)^{n}=1$.

	=== "Corrige"  

		pour l'équation $z^n=e^{i\pi/3}$ on doit se ramener à une équation de la forme $Z^n=1$. On pose donc ici $Z=\frac z{e^{i\pi/3n}}$ et on a donc $Z\in\mathbb U_n$. Les solutions sont donc $Z=e^{2ik\pi/n}$ et $z=e^{2ik\pi/n+i\pi/(3n)}=e^{i\pi\frac{1+6k}{3n}}$.

		Pour la seconde équation, on note que $z e1$ et $z \ne-1$, on pose $Z=(\frac{z-1}{z+1})^n$ et l'équation est équivalente à $Z^2-Z+1=0$ dont les racines sont $\frac{1\pm i\sqrt3}{2}=e^{\pm i\pi/3}$. On en déduit que $\frac{z-1}{z+1}=e^{i\pi\frac{\pm1+6k}{3n}}$.

		Or si $\frac{z-1}{z+1}=e^{i\theta}$, alors $z=\frac{e^{i\theta}+1}{1-e^{i\theta}}=i\frac{2\cos\frac\theta2}{2\sin\frac\theta2}=\frac i{\tan\frac\theta2}$.

		Dans le cas qui nous intéresse, on a donc les solutions : $\frac i{\tan\frac{\pm1+6k}{6n}\pi}$ pour $k\in\{0,\dots,n-1\}$.



!!! exercice "RMS2022-1394"  
	=== "Enoncé"  
		  **CCINP**  .
		On note $\mathbb{U}_n$ l'ensemble des complexes solution de l'équation $z^n = 1$,
		et $\mathbb{U}$ l'ensemble des complexes de module 1.


		Pour $n\in ^*$, on écrit $\left({3 + 4i}\right)^n =A_n+iB_n$ avec $A_n$ et $B_n$ dans $\R$.

		 - a)
		Montrer que, pour $n\in ^*$, $\U_n\subset \U$. Montrer que, pour tout $n\in ^*$, $\left(\frac{3 + 4i}{5}\right)^n$
		appartient à $\mathbb{U}$.

		 - b)
		Exprimer $A_{n + 1}$ et $B_{n + 1}$ en fonction de $A_n$ et $B_n$.

		 - c)
		Montrer que les suites $(A_n)$ et $(B_n)$ sont à valeurs dans $\mathbb{Z}$.

		 - d)
		Montrer que, pour tout $n$, le reste de la division euclidienne de $A_n$ par 5 est 3, et le reste de la division euclidienne
		de $B_n$ par 5 est 4.

		 - e)
		En déduire que $\bigcup_{n\in ^*}\U_n \ne\U$.



	=== "Corrige"  

		La première question est triviale.

		On a $A_{n+1}+iB_{n+1}=(3+4i)(A_n+iB_n)=(3A_n-4B_n)+i(3B_n+4A_n)$.

		On montre facilement par récurrence grâce à la relation précédente que $A_n$ et $B_n$ sont des entiers.

		On montre les histoires de restes par récurrence aussi.

		La dernière question est plus intéressante : le complexe $(\frac {3+4i}5)^n$ est dans $\mathbb U$. Montrons que pour tout entier $n$, $(\frac {3+4i}5)^n \ne1$. On s'interesse pour cela à la valeur de $B_n$ : $B_n$ est un entier dont le reste modulo $5$ est $4$ : $B_n$ ne peut donc pas être nul. Or $(\frac {3+4i}5)^n=\frac {A_n}{5^n}+i\frac{B_n}{5^n}$ donc $(\frac {3+4i}5)^n \ne1$.


!!! exercice "RMS2022-1395"  
	=== "Enoncé"  
		 **CCINP**.  
		 Soit $P=\left(X+1\right)^{n}-1$.

		- a) Déterminer les racines de $P$ et factoriser $P$ dans $\C[X]$.
		- b)   Calculer $ \prod_{k=1}^{n-1}\sin\left(\dfrac{k\pi}{n}\right)$.

	=== "Corrige"  

		Les racines de $P$ sont les $w-1$ avec $w\in\mathbb U_n$. Or $e^{2ik\pi/n}-1=2ie^{ik\pi/n}\sin(k\pi n)$.

		Or $P=(X+1)^n-1=\sum_{k=0}^n\binom nk X^k-1=X\sum_{k=1}^n\binom nk X^{k-1}$. Les racines de second membre sont donc les $2ie^{ik\pi/n}\sin(k\pi/n)$ pour $k\in\{1\dots n-1\}$ : le produit des racines se lit sur le coefficient de degré $0$ avec un coefficient multiplicatif de $(-1)^{n-1}$ : $(-1)^{n-1}n=\prod_{k=1}^{n-1}2ie^{ik\pi/n}\sin(k\pi/n)=(2i)^{n-1}\prod_{k=1}^{n-1}\sin(k\pi/n) e^{i\sum_{k=1}^{n-1}k\pi/n}=(2i)^{n-1}\prod_{k=1}^{n-1}\sin(k\pi/n) e^{in(n-1)\pi/2n}=(2i)^{n-1}\prod_{k=1}^{n-1}\sin(k\pi/n) e^{i(n-1)\pi/2}=(-2)^{n-1}\prod_{k=1}^{n-1}\sin(k\pi/n)$.

		On en déduit : $\prod_{k=1}^{n-1}\sin(k\pi/n)=\frac{n}{2^{n-1}}$.




!!! exercice "RMS2022-1397"  
	=== "Enoncé"  
		 **CCINP**.

		 - a) Soit $n\in Z$. Montrer que $\dfrac{n\left(n+1\right)}{2}\in\mathbb{Z}$.
		On pose $P=\dfrac{X\left(X+1\right)}{2}$. Montrer que $P\left(\mathbb{Z}\right)\subset\mathbb{Z}$.

		- b) On pose $H_{0}=1$ et $\forall k\in ^{*}$, $H_{k}=\frac{1}{k!}\prod_{i=1}^{k}\left(X-i+1\right)$.

			- i) Établir une relation entre $H_{k}\left(X+1\right)$, $H_{k}\left(X\right)$ et $H_{k-1}\left(X\right)$.

			- ii)  Montrer que la famille $\left(H_{i}\right)_{i\in[\![0,k]\!] }$ est une base de $\R_{k}\left[X\right]$.

		- c)
			- i) Pour $\left(k,n\right)\in ^{2}$, calculer $H_{k}\left(n\right)$.

			 ii) Pour $ k\in $, montrer que $H_{k}\left(\Z\right)\subset\Z$.

		- d) Montrer~: $\forall\left(k,n\right)\in ^{2}$, $\sum_{i=0}^{n}H_{k}\left(i\right)=H_{k+1}\left(n+1\right)$.
		- e) Soit $P\in\C\left[X\right]$ de degré $d$.
		Montrer que

		$P\left(\Z\right)\subset\Z$ $\Leftrightarrow$
		$\exists\left(m_{i}\right)_{i\in[\![ 0,d]\!] }\in\Z^{d+1}$
		tel que $P=\sum_{i=0}^{d}m_{i}H_{i}$.

	=== "Corrige"  




!!! exercice "RMS2022-1398"  
	=== "Enoncé"  
		  **IMT**   .
		Soient $P$ le plan vectoriel de $\mathbb{R}^3$ d'équation $x - 2y + 3z = 0$ et $D$ la droite vectorielle engendrée
		par $(2, 2, 1)$. Montrer que $P$ et $D$ sont supplémentaires et déterminer les matrices dans la base canonique
		du projecteur sur $P$ parallèlement à $D$ et de la symétrie par rapport à~$P$ parallèlement à $D$.

	=== "Corrige"  

		On a $\dim(P)+\dim(D)=3$. Soit $u\in D\cap P$. $u\in D$, donc $u=(2\lambda,2\lambda,1)$. $u\in P$ donc $2\lambda-4\lambda+3\lambda=0$ càd $\lambda=0$. Ainsi les deux espaces sont bien supplémentaires.

		Les deux vecteurs $u=(2,1,0)$ et $v=(3,0,-1)$ forment une base de $P$, et le vecteur $w=(2,2,1)$ forme une base de $D$. Notons $p$ le projecteur et $s$ la symétrie. La matrice de $p$ dans la base $(u,v,w)$ est $A=\begin{pmatrix} 1 & 0 & 0\\0&1&0\\0&0&0\end{pmatrix}$ et la matrice de $s$ est $\begin{pmatrix} 1 & 0 & 0\\0&1&0\\0&0&-1\end{pmatrix}$. La matrice de passage de la base canonique vers cette base est $P=\begin{pmatrix}2&3&2\\1&0&2\\0 & -1 &1\end{pmatrix}$. Pour avoir la matrice de $p$ dans la base canonique on fait $P A P^{-1}$. On laisse les calculs à la bonne volonté du lecteur.


!!! exercice "RMS2022-1399"  
	=== "Enoncé"  
		  **CCINP**  .
		Soit $(a_1, \ldots,a_n) \in \mathbb{C}^n$. On pose
		$A =\small
		\begin{pmatrix}
		a_1 & a_1 & \cdots & a_1 \\
		a_2 & a_2 & \cdots & a_2 \\
		\vdots & \vdots& & \vdots \\
		a_n & a_n & \cdots & a_n
		\end{pmatrix}$.


		 - a)
		Exprimer $A^2$ en fonction de $A$.

		 - b)
		On pose $B = 2A - \tr(A)\, I_n$. Trouver une condition nécessaire pour que $B$ soit inversible.


	=== "Corrige"  

		Posons $C=\begin{pmatrix}a_1\\a_2\\\vdots\\a_n\end{pmatrix}$ et $U=\begin{pmatrix}1\\1\\\vdots\\1\end{pmatrix}$. On remarque alors que $A=CU^\top$. On calcule alors $A^2=CU^\top CU^\top=C(U^\top C)U^\top=(U^\top C)CU^\top=\sum_{i=1}^na_iA=\mathrm{tr}(A)A$ (vu la suite de l'exercice, c'est une bonne chose de faire apparaître la trace).

		Posons $B=2A-\mathrm{tr}(A)I_n$. On a $B^2=4A^2-4\mathrm{tr}(A)A+\mathrm{tr}(A)^2I_n=\mathrm{tr}(A)^2I_n$. On a alors $\det(B^2)=\mathrm{tr}(A)^{2n}$ et donc $B$ est inversible ssi $\mathrm{tr}(A) \ne0$. C'est une CNS, pas juste nécessaire.


!!! exercice "RMS2022-1402"  
	=== "Enoncé"  
		 **CCINP** Soit $f$ un endomorphisme d'un espace vectoriel $E$ de dimension finie.  

		- a) On suppose que $f$ est un projecteur. Montrer que $\rg\left(f\right)=\tr\left(f\right)$.
		- b)  Réciproquement, on suppose que $\rg\left(f\right)=\tr\left(f\right)=1$.
		Montrer que $f$ est un projecteur.

	=== "Corrige"  

		Soit $f$ un projecteur. Alors dans une base adaptée à l'image et au noyau, la matrice de $f$ est diagonale avec des $1$ et des $0$ sur la diagonale. On a alors $\tr(f)=\dim(\im(f))=\rg(f)$.

		Réciproquement supposons que $\rg(f)=1$, alors comme vu dans un exercice précédent, il existe une base de $E$ dans laquelle la matrice de $f$ est de la forme $A=\begin{pmatrix}\lambda_1&\lambda_2&\dots&\lambda_n\\0&\dots&\dots&0\\
		\vdots&\vdots&\vdots&\vdots\\
		0&\dots&\dots&0\\
		\end{pmatrix}$. Puisque $\tr(f)=1$ on a alors $\lambda_1=1$ et directement $A^2=A$ : $f$ est un projecteur.



!!! exercice "RMS2022-1403"  
	=== "Enoncé"  
		 **IMT**   Soit $A$ la matrice de $\cm_n(\R)$ dont tous les coefficients valent $1$.

		On note $\mathcal{F}=\left\{ M\in\mathcal{M}_{n}\left(\R\right),\; AMA=0\right\}$.

		- a) Montrer que $\mathcal{F}$ est un espace vectoriel. Quelle est sa
		dimension ?


		On pose $\mathcal{E}=\left\{ M\in\mathcal{M}_{n}\left(\R\right),\;AMA=A\right\}$.

		- b) Pour quels réels $\lambda$ a-t-on $\lambda A\in\mathcal{E}$ ?
		- c) Déterminer les éléments de $\mathcal{E}$.

	=== "Corrige"  

		Que savons nous sur $A$ ? On sait que $A^p=n^{p-1}A$ (exercice classique).

		On peut aussi montrer que $AMA=\sum_{ij}m_{ij}A$. Ainsi $AMA=0$ ssi $\sum_{ij}a_{ij}=0$ : cela correspond au noyau d'une forme linéaire, sa dimension est donc $n^2-1$.

		Déterminer l'ensemble $\mathcal E$ correspond à résoudre l'équation linéaire $AMA=A$ d'inconnue $M$. On remarque que, d'après la remarque initiale, $\frac{1}{n^2}A$ est solution : en effet $A^3=n^2A$.

		Or l'ensemble des solutions d'une équation linéaire non homogène est de la forme $solution\ particuliere+solution\ homogene$. Donc les solutions sont de la forme $\frac 1{n^2}A+\ker f$ avec $f$ la forme linéaire : $M\mapsto \sum_{ij}m_{ij}$.


!!! exercice "RMS2022-1405"  
	=== "Enoncé"  
		  **IMT**   .
		Soient $n$ un entier $\geqslant 2$ et $\mathbb{K} = \mathbb{R}$ ou $\mathbb{C}$.

		On suppose que est  $H$ un hyperplan de $\mathcal{M}_n(\mathbb{K})$ ne contenant aucune matrice inversible.

		 - a) Donner un supplémentaire simple de $H$.

		 - b)
		Montrer que toutes les matrices nilpotentes sont dans $H$.

		 - c)
		Conclure qu'un tel  $H$ n'existe pas.

	=== "Corrige"  

		$I_n$ n'appartient pas à $H$. Donc $<I_n>$ est un supplémentaire de $H$.

		Soit $N$ une matrice nilpotente. Alors $I-N$ est inversible : en effet, si on note $p$ l'indice de nilpotence de $N$, alors pour tout $\lambda\in\mathbb R^*$, $(\lambda I)^p-N^p=(\lambda I-N)(....)$ c'est à dire $\lambda^p I_n=(\lambda I-N)(\dots)$. Ainsi $\lambda I-N$ est inversible. Notons $N=\mu I_n+A$ avec $A\in H$, en supposant $\mu \ne 0$, alors $N-\mu I$ est inversible d'après la remarque précédente, c'est à dire $A$ est inversible. Or $A\in H$ : ça n'est pas possible donc $\mu =0$ et $N\in H$.

		Or on sait que l'ensemble des matrices nilpotentes engendre l'ensemble des matrices de trace nulle (exercice classique). On en déduit que $H=\ker\tr$ mais $\ker\tr$ contient des matrices inversibles : un tel $H$ n'existe pas.


!!! exercice "RMS2022-1406"  
	=== "Enoncé"  
		   **CCINP**  .  
		Soit $f$ un endomorphisme d'un espace vectoriel $E$.


		Montrer que $\Ker(f)+\Im(f)=E$ si et seulement si $\Im(f)=\Im(f^2)$.


	=== "Corrige"  

		 Soit $f$ un endomorphisme de $E$. Supposons que $\Ker(f)+\Im(f)=E$. On sait déjà que $\Im(f^2)\subset \Im(f)$. Soit $y\in\Im(f)$, posons $y=f(x_0)$ et $x_0=x_K+x_I$ avec $x_K\in\Ker(f)$ et $x_I\in\Im(f)$. Posons alors $x_I=f(z)$. On a :$y=f(x_K+x_I)=f(x_K)+f(x_I)=0+f(f(z))\in\Im(f^2)$. Donc $\Im(f)\subset \Im(f^2)$ et ainsi $\Im(f)=\Im(f^2)$.

		 Réciproquement, si $\Im(f^2)=\Im(f)$, on sait que $\Ker(f)+\Im(f)\subset E$. C'est l'inclusion réciproque qui nous intéresse. Soit $x\in E$, on a $f(x)\in\Im(f)$ et donc $f(x)\in\Im f^2$. On en déduit qu'il existe $z\in E$ tel que $f(x)=f^2(z)$. Posons $y=x-f(z)$, alors $f(y)=f(x)-f^2(z)=0$. Donc $y\in\Ker f$. On en déduit $x=y+f(z)\in \Ker f+\Im f$ et donc $E=\Ker f+\Im f$.  


!!! exercice "RMS2022-1419"  
	=== "Enoncé"  
		  **CCINP**  .
		Soit un entier $n \geqslant 2$ et soit $A\in \mathcal{M}_n(\mathbb{K})$, où $\mathbb{K}$ vaut $\mathbb{R}$
		ou $\mathbb{C}$.


		 - a)
		On suppose $A$ diagonalisable. Montrer que $A$ est semblable à sa transposée.

		 - b)
		Montrer que la réciproque est fausse. On pourra considérer la matrice triangulaire supérieure
		$(a_{i, j})_{1 \leqslant i, j \leqslant n}$ définie par $a_{i, j} = 1$ si $i \leqslant j$.



	=== "Corrige"  

		Si $A$ est diagonalisable, alors elle est semblable à une matrice diagonale. En notant $A=P^{-1}DP$, on a $A=P^{-1}D^\top P=P^{-1}(PAP^{-1})^\top P=P^{-1}(P^{-1})^\top A^\top P^\top P=(P^\top P)^{-1} A^\top P^\top P$ : $A$ est semblable à sa transposée que $P^\top P$ est inversible.

		Notons $B$ la matrice proposée. Notons $f$ l'endomorphisme canoniquement associé à $B$. Notons $(e_1,\dots,e_n)$ la base canonique de $\mathbb R^n$. alors la matrice de $f$ dans $(e_n,\dots,e_1)$ est la transposée de $B$. Donc $B$ est semblable à sa transposée.

		Cependant $B$ n'est pas diagonalisable. En effet, si $B$ est diagonalisable, alors il existe une base $(b_1,\dots,b_n)$ et des scalaires $(\lambda_1,\dots,\lambda_n)$ tels que $f(b_i)=\lambda_i b_i$. Si $X$ est un vecteur de $M_{n,1}(\mathbb K)$ alors $BX=\begin{pmatrix}x_1+x_2+\dots+x_n\\x_2+x_3+\dots+x_n\\x_3+\dots+x_n\\ \vdots\\x_n\end{pmatrix}$ Et donc $BX=\lambda X$ donne $\begin{cases}
		x_1+x_2+\dots+x_n=\lambda x_1\\
		x_2+\dots+x_n=\lambda x_2\\
		\vdots\\
		x_n=\lambda x_n
		\end{cases}$
		Ce qui donne, si au moins l'un des $x_i$ est non nul : $\lambda=1$. Le système donne alors $x_n=x_{n-1}=\dots=x_2=0$ et aucune contrainte sur $x_1$. L'ensemble des vecteurs vérifiant $BX=X$ est donc limité à l'espace engendré par $\begin{pmatrix}1\\0\\\vdots\\0\end{pmatrix}$ : on ne peut pas trouver là dedans une base de $M_{n,1}$ et donc la matrice n'est pas diagonalisable.


!!! exercice "RMS2022-1421"  
	=== "Enoncé"  
		 **CCINP**
		Soient $E$ un espace vectoriel de dimension $n\geq 2$ et  $u\in {\cal L}(E)$. On suppose  que $u^2-2u+\id =0$ et $u \neq \id $.


		 - a) Montrer que $u\in {\rm GL}(E)$ et déterminer $u^{-1}$.

		 - b)
		  Montrer que ${\rm Im}(u-\id )\subset {\rm Ker}(u-\id )$.
		  Montrer que $1$ est la seule valeur propre de $u$. L'endomorphisme $u$ est-il diagonalisable ?



		 - c) Soient $f$ et $g$ deux projecteurs. Montrer que $f\circ g =g $ $\Leftrightarrow$ $ {\rm Im}(g)\subset {\rm Im} (f)$.

		 - d) Soit $v\in {\cal L}(E)$ non nul tel que $v^2=0$.

		 Soit $S$ un supplémentaire de ${\rm Im}(v)$ dans $E$. Soit $p_1$ la projection sur ${\rm Im}(v)$ parallèlement à~$S$. On pose $q_1=p_1-v$.
		Montrer que $q_1$ est un projecteur et que ${\rm Im}(q_1)={\rm Im}(p_1)$.

		 - e) Montrer qu'il existe $p$ et $q$ deux projecteurs tels que $u=p+q$ et ${\rm Im}(p)={\rm Ker}(q)$.

	=== "Corrige"  

		 On a $u(2id-u)=id$ donc $u$ est inversible, d'inverse $2id-u$.

		 Soit $x\in{\rm Im}(u-id)$, il existe $x_0\in E$ tel que $x=u(x_0)-x_0$. On a alors $(u-id)(x)=(u-id)^2(x_0)=0$ car $(u-id)^2=0$. On en déduit que ${\rm Im}(u-id)\subset{\rm ker}(u-id)$.

		 Supposons $f\circ g=g$. Soit $y\in{\rm Im}(g)$, il existe $y_0\in E$ tel que $y=g(y_0)$. Or $g=f\circ g$ donc $y=g(y_0)=f\circ g(y_0)\in{\rm Im}(f)$. On en déduit ${\rm Im}(g)\subset{\rm Im}(f)$.

		 Calculons $q_1^2=(p_1-v)^2=p_1^2-p_1\circ v-v\circ p_1+v^2=p_1-p_1\circ v-v\circ p_1$. Or $p_1$ est la projection sur ${\rm Im}(v)$ donc $p_1\circ v=v$. De plus, si $x\in E$ alors $p_1(x)\in {\rm Im}(v)$. On peut donc écrire $p_1(x)=v(x_0)$ et $v\circ p_1(x)=v\circ v(x_0)=0$. Donc $v\circ p_1=0$. On a donc $q_1^2=p_1-v=q_1$ donc $q_1$ est un projecteur.

		 On sait que ${\rm Im}(p_1)={\rm Im}(v)$. On cherche donc à montrer que ${\rm Im}(q_1)={\rm Im}(v)$. Soit $y\in{\rm Im}(q_1)$ alors $y=q_1(y_0)=p_1(y_0)-v(y_0)$. Or $p_1(y_0)\in {\rm Im}(v)$ ainsi que $v(y_0)$. Donc $y\in {\rm Im}(v)$.

		 Réciproquement, si $y\in{\rm Im}(v)$, alors en particulier $v(y)=0$ et $y=p(y)$. Donc $y=p(y)-v(y)=(p-v)(y)=q_1(y)$. On en déduit que $y\in{\rm Im}(q_1)$.

		 Faisons jouer maintenant à $(u-id)$ le rôle de $v$ : on a bien $(u-id)^2=0$ par hypothèses. Prenons un supplémentaire de ${\rm Im}(u-id)$ $S$ et $p_1$ la projection sur ${\rm Im}(u-id)$ parallèlement à $S$. Alors $q_1=p_1-(u-id)$ est aussi un projecteur. On trouve alors $u=p_1+(id-q_1)$. Or $q_1$ est un projecteur d'image ${\rm Im}(v)$ donc $id-q_1$ est aussi un projecteur dont le noyau est ${\rm Im}(v)$. En posant $p=id-q_1$ et $q=p_1$, on a bien $p$ et $q$ deux projecteurs et ${\rm Im}(p)={\rm ker}(q)$.


!!! exercice "RMS2022-1423"  
	=== "Enoncé"  
		  **ENSEA**
		On munit $\mathcal{M}_2(\mathbb{R})$ du produit scalaire $\langle M, N \rangle = \tr(M^T   N)$.


		 - a)
		Vérifier que $\langle \;, \; \rangle$ est bien un produit scalaire sur $\mathcal{M}_2(\mathbb{R})$.

		 - b)
		Montrer que $F = \left\{\small\begin{pmatrix}
		a & b \\
		-b & a
		\end{pmatrix}, \ (a, b) \in \mathbb{R}^2\right\}$
		est un sous-espace vectoriel de $\mathcal{M}_2(\mathbb{R})$ et trouver une base $(E_1, E_2)$ de $F$.

		 - c)
		Déterminer une base orthonormée de $F^{\perp}$.

		 - d)
		En déduire la projection orthogonale de $J$ sur $F^{\perp}$.

	=== "Corrige"  

		$F$ est clairement un sev. Une base est $E_1=\begin{pmatrix} 1 & 0 \\ 0 & 1\end{pmatrix}$ et $E_2=\begin{pmatrix} 0 & 1 \\ -1 & 0\end{pmatrix}$.

		$F^\bot$ est de dimension $2$. On a $E_3=\begin{pmatrix} 1 & 0 \\ 0 & -1\end{pmatrix}$ et $E_4=\begin{pmatrix} 0 & 1 \\ 1 & 0\end{pmatrix}$ sont toutes les deux orthogonales à $E_1$ et $E_2$ et elles ne sont pas liées donc $(E_3,E_4)$ est une base orthogonale de $F^\bot$.

		On pose $J=\begin{pmatrix} 1 & 1 \\ 1 & 1\end{pmatrix}$. La projection de $J$ sur $F^\bot$ est donnée par $\frac{<J|E_3>}{||E_3||^2}E_3+\frac{<J|E_4>}{||E_4||^2}E_4=0+\frac{2}{2}E_4=E_4$.


!!! exercice "RMS2022-1424"  
	=== "Enoncé"  
		  **CCINP**  .
		Dans $\mathbb{R}^3$, muni du produit scalaire canonique, on considère une droite de vecteur directeur unitaire $u$,
		et $p$ la projection orthogonale sur cette droite.


		 - a)
		Donner l'expression de $p(x)$ pour $x \in \mathbb{R}^3$.

		 - b)
		Soit $P $ le plan d'équation $x - 2y + z = 0$. Donner la matrice de la projection orthogonale $q$ sur ce plan dans la base canonique
		de $\mathbb{R}^3$.


	=== "Corrige"  

		D'après le cours, $p(x)=<x|u>u$.

		Un vecteur orthogonal à ce plan est $(1,-2,1)$ de norme $\sqrt 6$ : on pose $u=\frac 1{\sqrt 6}(1,-2,1)$.   Notons $p$ la projection orthogonale sur $u$. Alors $q=id-p$. Or : $p(e_1)=<e_1|u>u=\frac1{\sqrt 6}u$, $p(e_2)=<e_2|u>u=-\frac2{\sqrt 6}u$ et $p(e_3)=<e_3|u>u=\frac1{\sqrt 6}u$. On a donc $mat(p)=\frac 1{6}\begin{pmatrix}1&-2&1\\-2&4&-2\\1&-2&1\end{pmatrix}$. On en déduit $mat(q)=I_3-mat(p)=\frac 16\begin{pmatrix}
		5 & 2 & -1\\2 & 2 & 2 \\-1 & 2 & 5
		\end{pmatrix}$


!!! exercice "RMS2022-1433"  
	=== "Enoncé"  
		  **CCINP**  .
		Soit $E = \mathcal{C}^0([0, 1], \mathbb{R})$. Soit $p \in \mathbb{R}$ avec $ p \geqslant 2$.
		Pour toute fonction $f \in E$, on pose $\|f\|_p = \left(\int_0^1 |f(t)|^p \,\mathrm d  t\right)^{1/p}$.


		 - a)
		Soit $f_n \colon x \in [0, 1] \mapsto x^n$. Calculer $\|f_n\|_p$.




		Pour  $f$ et $g$ dans $E$, on pose $\langle f, g \rangle = \int_0^1 f(t) g(t) \,\mathrm d  t$.

		- b) Montrer qu'on définit ainsi un produit scalaire. Quelle est sa norme associée ?




		Soient $F$ un sous-espace vectoriel de $E$ non nul et $C>0$.

		 On suppose~:
		 $\forall f \in F, \ \| f\|_{\infty} \leqslant C \|f\|_p$.


		 - c) À l'aide de la question - a)\!, montrer que $F  \neq E$.

		 - d) En remarquant que $|f|^p =  |f|^2 |f|^{p - 2}$, montrer que $\forall f \in F, \ \|f\|_p^p \leqslant \|f\|_{\infty}^{p - 2} \|f\|_2^2$.
		En déduire que $\forall f \in F, \ \|f\|_{\infty} \leqslant C^{\frac{p}{2}}\|f\|_2$.


		 - e) Soient $(u_1, \ldots, u_n)$ une famille orthonormale de $F$, $(\lambda_1, \ldots, \lambda_n) \in \mathbb{R}^n$.
		Soit $x \in [0, 1]$. Montrer que
		$(\lambda_1 u_1(x) + \cdots + \lambda_n u_n(x))^2 \leqslant C^p(\lambda_1^2 + \cdots + \lambda_n^2)$.


		 - f)
		En prenant $\lambda_k = u_k(x)$ pour tout $k \in  [\![  1, n  ]\!] $, montrer que $\dim F \leqslant C^p$.



	=== "Corrige"  

		On a $||f_n||_p^p=\int_0^1 x^{np} dx = \frac{1}{np+1}$. On en déduit que $||f_n||_p=\left(\frac 1{np+1}\right)^{1/p}$.

		On a bien un produit scalaire avec la norme associée $||.||_2$.

		On sait que $F\subset E$. Montrons que l'inclusion réciproque n'est pas vraie : il s'agit d'exhiber une fonction continue qui ne soit pas dans $F$.

		Soit un entier $n$ naturel, on remarque que $||f_n||_\infty=1$ et $||f_n||_p=\sqrt{\frac1{np+1}}$. Il existe donc un $n$ tel que $||f_n||_p<C$. Pour un tel $n$ on obtient $1<1$ ce qui est absurde. Ainsi un tel $f_n$ n'appartient pas à $E$.

		Soit $f\in F$, $||f||_p^p = \int_0^1 |f|^{p-2}|f|^2\le ||f||_\infty^{p-2}\int_0^1 f^2dt=||f||_\infty^{p-2}||f||_2^2$.

		Or $f\in F$ donc $||f||_\infty^{p}\le C^{p}||f||_p^{p}$. On en déduit $||f||_\infty\le C^p||f||_p^p\le C^p||f||_\infty^{p-2}||f||_2^2$. On a alors $||f||_\infty^2\le C^p||f||_2^2$ c'est à dire, puisque tout est positif, $||f||_\infty\le C^{p/2}||f||_2$.

		Les $u_i$ formant une famille orthonormale de $F$, alors pour tout $n$-uplet $\lambda_1,\dots,\lambda_n$ d'éléments de $\mathbb R$, $u=\sum_{i=1}^n\lambda_iu_i$ est un élément de $F$. On peut donc affirmer que $||u||\infty\le C^{p/2}||u||_2$. Or $u(x)^2=(\sum_{i=1}^n \lambda_iu_i(x))^2\le ||u(x)||_\infty^2\le C^p||u||_2^2$. Or $||u||_2^2=<\sum\lambda_iu_i|\sum\lambda_iu_i>=\sum_{i=1}^n\lambda_i^2||u_i||_2^2=\sum_{i=1}^n\lambda_i^2$ par orthonormalité de la famille des $u_i$. On en déduit l'inégalité voulue,  puis en posant $\lambda_i=u_i(x)$ pour un $x$ donné, on obtient $(\sum u_i(x)^2)^2\le C^p(\sum u_i(x)^2)$ ou encore $\sum u_i(x)^2)\le C^p$. En intégrant entre $0$ et $1$, on obtient par normalité de la famille : $n\le C^p$. Prenons donc comme famille une base orthonormale (qui existe), on a $\dim(F)\le C^p$.


!!! exercice "RMS2022-1434"
	=== "Enoncé"  
		 **CCINP**On pose $P_{n}=\sum_{k=0}^{2n+1}\dfrac{\left(-X\right)^{k}}{k!}$.

		- a) Montrer que : $P_{n+1}'=-P_{n}-\dfrac{X^{2n+2}}{\left(2n+2\right)!}$
		et $P_{n+1}''=P_{n}$.

		- b)  
			- i) Par récurrence, montrer la stricte décroissance de la fonction polynomiale
		$p_{n}$ associée à  $P_{n}$. Montrer que $p_{n}$ admet une unique
		valeur d'annulation sur $\R$, notée $u_{n}$.

			- ii)  Montrer que $P_{n}=\sum_{k=0}^{n}\dfrac{X^{2k}}{\left(2k\right)!}\left(1-\dfrac{X}{2k+1}\right)$.

		- c)
			- i) Montrer que $u_{n}\in\left[1,2n+1\right]$.

			- ii) Montrer que la suite $u$ est monotone.


	=== "Corrige"  

		Calculons bêtement :

		$$
		\begin{align*}
		P_{n+1}' &= (\sum_{k=0}^{2n+3}\frac{(-X)^k}{k!})'\\
		&= -\sum_{k=1}^{2n+3}\frac{(-X)^{k-1}}{(k-1)!}\\
		&= -\sum_{k=0}^{2n+2}\frac{(-X)^{k}}{(k)!}\\
		&= -\sum_{k=0}^{2n+1}\frac{(-X)^{k}}{(k)!}-\frac{X^{2n+2}}{(2n+2)!}\\
		&= -P_n-\frac{X^{2n+2}}{(2n+2)!}
		\end{align*}
		$$

		On a alors :

		$$
		\begin{align*}
		 P_{n+1}'' &= -P_n'-\frac{X^{2n+1}}{(2n+1)!}\\
		 &=P_{n-1}+\frac{X^{2n}}{(2n)!}-\frac{X^{2n+1}}{(2n+1)!}\\
		 &=P_n
		\end{align*}
		$$

		Je n'arrive pas à traiter la stricte décroissance par récurrence. Cependant, on remarque que si on pose $R_n$ le reste de la série de terme général $\frac{(-x)^n}{n!}$, cette série étant alternée, le signe de $R_n$ est déterminé par la parité de $n$. En particulier, $R_{2n}$ est négatif.

		On remarque alors d'après la question précédente que $P_{n+1}'(x)=-e^{-x}+R_{2n+2}< 0$. On en déduit que $P_{n+1}$ est strictement décroissante pour tout $n$ (on remarque aussi que $P_0=1$ n'est pas strictement décroissante !)

		Enfin $P_n$ est un polynôme de degré impair et strictement décroissant, donc il admet une unique solution $u_n$ sur $\mathbb R$.

		On a alors en regroupant termes pairs et impairs :

		$$
		\begin{align*}
		 P_n&=\sum_{k=0}^{2n+1}\frac{(-X)^k}{k!}\\
		 &=\sum_{p=0}^n\frac{(-X)^{2p}}{(2p)!}+\frac{(-X)^{2p+1}}{(2p+1)!}\\
		 &=\sum_{p=0}^n \frac{X^{2p}}{(2p)!}(1-\frac{X}{2p+1})
		\end{align*}
		$$

		On calcule alors $P(1)>0$ et

		$$
		\begin{align*}
		P(2n+1) &= \sum_{p=0}^{n}\frac{(2n+1)^{2p}}{(2p)!}\underset{<0}{\underbrace{(1-\frac{2n+1}{2k+1})}}\\
		&<0
		\end{align*}
		$$

		D'après le TVi, un fonction polynômiale étant continue, $u_n\in[1,2n+1]$.

		Calculons enfin $P_{n+1}(u_n)=P_n(u_n)+\frac{u_n^{2n}}{(2n!)}-\frac{u_n^{2n+1}}{(2n+1)!}=\frac{u_n^{2n}}{(2n)!}\left(1-\frac{u_n}{2n+1}\right)$. On $u_n<2n+1$ donc $(1-\frac{u_n}{2n+1})>0$ et ainsi $P_{n+1}(u_n)>0$. On en déduit d'après les variations de $P_{n+1}$ que $u_n<u_{n+1}$ : la suite $u_n$ est monotone croissante (strictement).


!!! exercice "RMS2022-1435"  
	=== "Enoncé"  
		   **CCINP**  .  
		 Pour tout $n\in ^*$, on pose $S_n=\sum_{k=1}^n\dfrac{(-1)^{k-1}}{k}$.


		 - a) Quelle est la nature de la série $\sum\dfrac{(-1)^{n-1}}{n}$?

		 - b) Montrer que $\forall n\in ,\ S_{2n+2}=\sum_{k=0}^n\left(\dfrac{1}{2k+1}-\dfrac{1}{2k+2}\right)$.

		 - c) Montrer que la suite $(S_{2n})$ converge. Retrouver le résultat de la question a).



		 - d) Soit $t\in[0,1]$. Convergence et somme de la série $\sum_{k\geq 1} (t^{2k}-t^{2k+1})$.

		 - e)  En déduire la valeur de  $\sum_{n=1}^{+\infty}\dfrac{(-1)^{n-1}}{n}$.

	=== "Corrige"  

		D'après le CSSA, la série est convergente.

		On a $S_{2n+2}=\sum_{k=1}^{2n+2}\frac{(-1)^{k-1}}{k}=\sum_{k=1}^{n+1}\frac{(-1)^{2k-2}}{2k-1}+\frac{(-1)^{2k-1}}{2k}=\sum_{k=0}^{n}\frac{(-1)^{2k}}{2k+1}+\frac{(-1)^{2k+1}}{2k+2}=\sum_{k=0}^{n}\frac{1}{2k+1}-\frac{1}{2k+2}$.

		Avec cette remarque, on obtient que le terme général de $S_{2n}$ est équivalent à $\frac1{k^2}$ qui est le terme général d'une série convergente. Ainsi $S_{2n}$ est une suite convergente et $S_{2n+1}=S_{2n}+\frac{1}{2n+1}$ converge aussi vers la même limite. Les deux suites extraites de la somme partielle $S_{2n}$ et $S_{2n+1}$ convergent vers la même limite : la suite des sommes partielles converge donc.

		Soit $t\in[0,1]$. Si $t=1$ ou $t=0$ alors la série est à terme général nul et elle converge donc. Sinon, $t^{2k}-t^{2k+1}=t^{2k}(1-t)$ qui est le terme général d'une série géométrique à termes positifs convergente. Par théorème de comparaison des séries à termes positifs, $\sum t^{2k}-t^{2k+1}$ est convergente. La suite des sommes partielles étant convergente, pour déterminer la limite on peut se contenter d'étudier une suite extraite. On a $f_N(t)=\sum_{k=1}^Nt^{2k}-t^{2k+1}=\sum_{k=2}^{2N+1}(-t)^k=t^2\frac{1-(-t)^{2N-1}}{1+t}=t^2\frac{1+t^{2N-1}}{1+t}$ et à la limite :$\sum_{k\ge1}t^{2k}-t^{2k+1}=\frac{t^2}{1+t}$.

		Primitivons la somme partielle : Posons $F_N(t)$ une primitive de $f_N(t)$ qui s'annule en $0$ par exemple.
		On a $F_N(t)=\sum_{k=1}^N\frac{t^{2k+1}}{2k+1}-\frac{t^{2k+2}}{2k+2}$ et donc $F_N(1)=S_{2N+2}-\frac 12$. Par ailleurs, on a :

		$$
		\begin{align*}
		F_N(1) &= \int_0^1 t^2\frac{1+t^{2N-1}}{1+t}dt\\
		&=\int_0^1 \frac{t^2}{1+t}dt+\frac A{2N}&\text{par IPP sur le second membre de la fraction}\\
		&=\int_0^1 (t-1)+\frac 1{1+t}dt+o(\frac 1N)\\
		&=-\frac 12+\ln2+o(\frac 1N).
		\end{align*}
		$$

		Avec les deux précédents résultats on a donc $S_{2N+2}=\ln2+o(\frac 1N)$ et ainsi la somme de la série recherchée vaut $\ln 2$.


!!! exercice "RMS2022-1436"  
	=== "Enoncé"  
		  **CCINP**  .
		Pour tout $n \in \mathbb{N}^*$, on pose $u_n = 1 + \frac{1}{2} + \cdots + \frac{1}{n} - \ln(n)$.

		La série $\sum (u_n - u_{n - 1})$ est-elle convergente ? Que peut-on en déduire ?

	=== "Corrige"  

		On a $u_n-u_{n-1}=\frac 1n+\ln(1-\frac1n)=\frac 1n-\frac 1n-\frac 1{n^2}+o(\frac 1{n^2})\sim-\frac 1{n^2}$. C'est le terme général d'une série de Riemann convergente, donc pas théorèmes de comparaison, la série de terme général $u_n-u_{n-1}$ est convergente. Or c'est une série télescopique donc sa nature est équivalente à celle de la suite $(u_n)$ : la suite $(u_n)$ converge, et cela donne l'existence d'une constante $\gamma$ telle que $H_n=\ln(n)+\gamma+o(1)$.


!!! exercice "RMS2022-1437"  
	=== "Enoncé"  
		 **CCINP**Soit $\left(\theta,\varphi\right)\in\R^{2}$.
		Pour $n\geqslant2$, on pose : $u_{n}=\dfrac{\mathrm{e}^{\mathrm{i}n\theta}}{\sqrt{n}+\mathrm{e}^{\mathrm{i}n\varphi}}$.

		- a) Montrer que $u_{n}=\dfrac{\mathrm{e}^{\mathrm{i}n\theta}}{\sqrt{n}}-\dfrac{\mathrm{e}^{\mathrm{i}n\left(\theta+\varphi\right)}}{n}+\co\left(\dfrac{1}{n\sqrt{n}}\right)$.
		- b) On suppose que $\theta$ et $\varphi$ sont des multiples de $\pi$.
		Quelle est la nature de $\sum u_{n}$ ?


	=== "Corrige"  

		 Calculons :

		$$
		 \begin{align*}
		 u_n&=\frac{e^{in\theta}}{\sqrt n+e^{in\varphi}}\\
		 &=\frac{e^{in\theta}}{\sqrt n}\frac 1{1+\frac{e^{in\varphi}}{\sqrt n}}\\
		 &=\frac{e^{in\theta}}{\sqrt n}(1-\frac{e^{in\varphi}}{\sqrt n}+O(\frac1{n}))\\
		 &=\frac{e^{in\theta}}{\sqrt n}-\frac{e^{in(\theta+\varphi)}}{n}+O(\frac1{ n\sqrt n})\\
		 \end{align*}
		$$

		Posons $v_n=\frac{e^{in\theta}}{\sqrt n}$ et $w_n=\frac{e^{in(\theta+\varphi)}}{n}$. Le $O(\frac1{n\sqrt n})$ est le terme général d'une série absolument convergente, la convergence de $\sum u_n$ dépend de la convergence de $\sum v_n+w_n$.
		\begin{itemize}
		\item Si $\theta$ et $\varphi$ sont des multiples impairs de $\pi$, alors $v_n$ est le terme général d'une série alternée donc convergente mais $w_n$ est divergente. Le $O$ étant le terme général d'une série absolument convergente, on en déduit $\sum u_n$ diverge.
		\item Si $\theta$ est un multiple pair et $\varphi$ un multiple impair alors $\sum v_n$ diverge et $\sum w_n$ converge donc $\sum u_n$ diverge.
		\item Si $\theta$ est un multiple impair et $\varphi$ un multiple pair, alors $\sum v_n$ converge ainsi que $\sum w_n$ par CSSA. Donc $\sum u_n$ converge
		\item Si $\theta$ et $\varphi$ sont des multiples pairs, alors $v_n+w_n=\frac 1{\sqrt n}-\frac 1n=\frac{\sqrt n -1}{n}\sim\frac 1{\sqrt n}$ qui est le terme général d'un série divergente. Par théorème de comparaison des séries à termes positifs, $\sum u_n$ diverge.
		\end{itemize}


!!! exercice "RMS2022-1438"  
	=== "Enoncé"  
		 **CCINP** Soient $x\in\R$ et, pour $n\in $,  $u_{n}=3^{n}\sin^{3}\left(\dfrac{x}{3^{n}}\right)$.

		- a) Montrer que la série $\sum u_{n}$ converge.
		- b) Calculer $\sum_{n=0}^{+\infty}u_{n}$.

		*Ind*. Utiliser
		la formule : $\sin^{3}x=\dfrac{3}{4}\sin x-\dfrac{1}{4}\sin\left(3x\right)$.


	=== "Corrige"  

		Pour $x$ fixé, $u_n\sim 3^n\frac{x^3}{3^{3n}}=\frac{x^3}{3^{2n}}$ qui est le terme général d'une série convergente à termes de signe constant. Par théorème de comparaison, la série $\sum u_n$ converge.

		On a $u_n=3^n\sin^3\left(\frac x{3^n}\right)=3^n\left(\frac34\sin\frac x{3^n}-\frac {1}4\sin \frac x{3^{n-1}}\right)=\frac 34\left(3^{n}\sin\frac x{3^n}-3^{n-1}\sin \frac x{3^{n-1}}\right) $. On reconnait alors le terme général d'une série télescopique dont la limite est $\frac 34(x-\sin(x))$. Ainsi $\sum_{n=0}^{+\infty}u_n=\frac 34(x-\sin(x))$.  


!!! exercice "RMS2022-1439"  
	=== "Enoncé"  
		  **CCINP**  .
		Soit $(a, b) \in (]0, +\infty[)^2$ tel que $1 + a < b$. Soit $(u_n)_{n \in \mathbb{N}}$ une suite réelle  à termes
		strictement positifs telle que $\forall n \in \mathbb{N}, \ \frac{u_{n + 1}}{u_n} = \frac{n + a}{n + b}$.


		 - a)
		Trouver un équivalent de $\ln\left(\frac{u_{n + 1}}{u_n}\right)$ quand $n \to +\infty$.

		 - b)
		Montrer que $\lim \sum_{n = 0}^N \ln\left(\frac{u_{n + 1}}{u_n}\right) = -\infty$ quand $N \to +\infty$.
		En déduire que $\lim u_n = 0$.


		 - c)
		On pose $\alpha = b - a$ et $v_n = n^{\alpha} u_n$. Montrer que la série $ \sum \ln\left(\frac{v_{n + 1}}{v_n}\right)$ converge.

		 - d)
		Montrer qu'il existe $A \in \R^{+*}$ tel que $u_n \sim \frac{A}{n^{\alpha}}$ quand $n \to +\infty$ et en déduire
		que $\sum u_n$ converge.

		 - e)
		Montrer que $\sum_{n = 0}^{+\infty} u_n = u_0\, \frac{1 - b}{a - b + 1}$.


	=== "Corrige"  

		On a $\ln(\frac{u_{n+1}}{u_n})=\ln(\frac{n+a}{n+b})=\ln(1+\frac{a-b}{n+b})\sim\frac{a-b}{n}$ car $a-b>-1$ donc $a-b \ne0$.

		Or $\frac{a-b}{n}$ est le terme général d'une série divergente. On en déduit par théorème de comparaison des séries à termes positifs (ici négatifs) que $\sum\ln(\frac{u_{n+1}}{u_n})$ est divergente. A ce titre, on peut comparer les sommmes partielles et puisque $\sum_{k\ge 1}\frac{a-b}{k}\to -\infty$, alors $\sum_{n=0}^N\ln(\frac{u_{n+1}}{u_n})\to-\infty$.

		Cette série étant télescopique, on en déduit que $\ln(u_{N+1})\to -\infty$ et donc $u_N\to 0$.

		Si maintenant $v_n=n^\alpha u_n$ alors $\frac{v_{n+1}}{v_n}=(1+\frac{1}{n})^\alpha\frac{n+a}{n+b}$. On peut calculer :

		$$
		\begin{align*}
		\ln(\frac{v_{n+1}}{v_n})&=\alpha\ln(1+\frac 1n)+\ln(1+\frac{a-b}n)\\
		&=\alpha(1+\frac 1n-\frac{1}{2n^2}+\frac {a-b}n-\frac{(a-b)^2}{2n^2}+o(\frac 1{n^2})\\
		&=-\frac{1+(a-b)^2}{2n^2}+o(\frac 1{n^2})\\
		&\sim \frac K{n^2}
		\end{align*}
		$$

		Par théorèmes de comparaison des séries à termes positifs (ici négatifs), la série de terme général $\ln(\frac {v_{n+1}}{v_n})$ est convergente.

		C'est encore une fois une série télescopique donc $\ln(v_n)$ est une suite convergente. Notons $\ell$ sa limite réelle, alors $v_n\to e^\ell=A>0$ et $v_n=n^\alpha u_n$ donc $u_n\sim\frac A{n^\alpha}$.  Or $\alpha >1$ donc par théorème de comparaison $\sum u_n$ converge.

		La relation $\frac{u_{n+1}}{u_n}=\frac{n+a}{n+b}$ donne $n(u_{n+1}-u_n)=au_n-bu_{n+1}$. Or :

		$$
		\begin{align*}
		\sum_{n=O}^N n(u_{n+1}-u_n)&=\sum_{n=0}^N nu_{n+1}-\sum_{n=0}^N nu_n\\
		&=\sum_{n=1}^{N+1}(n-1)u_n -\sum_{n=0}^N nu_n\\
		&=\sum_{n=1}^N -u_n +Nu_{N+1}\\
		&=-\sum_{n=0}^Nu_n +Nu_{N+1}+u_0
		\end{align*}
		$$

		et

		$$
		\begin{align*}
		\sum_{n=0}^N au_n-bu_{n+1}&=a\sum_{n=0}^N u_n -b\sum_{n=0}^N u_{n+1}\\
		&=a\sum_{n=0}^N u_n -b\sum_{n=1}^{N+1} u_n\\
		&=(a-b)\sum_{n=1}^N u_n +au_0-bu_{N+1}\\
		&=(a-b)\sum_{n=0}^N u_n +bu_0-bu_{N+1}
		\end{align*}
		$$

		On en déduit $(a-b+1)\sum_{n=0}^Nu_n=(1-b)u_0+Nu_{N+1}+bu_{N+1}$. Or $u_{N+1}\to 0$ et $Nu_{N+1}\sim \frac A{N^{\alpha-1}}\to 0$. Donc

		$$
		\sum_{n=0}^{+\infty} u_n = \frac{1-b}{a-b+1}u_0.
		$$



!!! exercice "RMS2022-1440"  
	=== "Enoncé"  
		  **CCINP**  .
		Soit $(u_n)$ une suite réelle telle que $u_0 = a > 0$ et $\forall n \in \mathbb{N}, \ u_{n + 1} = u_n\,  e^{-u_n}$.


		 - a)
		Montrer que, pour tout entier $n$, $u_n > 0$.

		 - b)
		Montrer que la suite $(u_n)$ converge et calculer sa limite.

		 - c)
		Étudier $\sum u_n$. On pourra s'intéresser à $\ln(u_n)$.


	=== "Corrige"  

		On montre par récurrence très simple que $u_n>0$ pour tout $n$.

		On a $\frac{u_{n+1}}{u_n}=e^{-u_n}<1$ et donc $(u_n)$ est décroissante. On en déduit que $u$ est décroissante et minorée donc convergente. La limite est un point fixe de la fonction continue $x\mapsto xe^{-x}$ : $u_n\to 0$.

		On a enfin $\ln(u_{n+1})=\ln(u_n)-u_n$, donc $u_n=\ln(u_n)-\ln(u_{n+1})$. On en déduit par télescopage que la série $\sum u_n$ est de même nature que la suite $\ln(u_n)$. Or $u_n\to 0^+$ donc $\ln(u_n)\to -\infty$ : il y a divergence.


!!! exercice "RMS2022-1441"  
	=== "Enoncé"  
		   **CCINP**  

		 - a) Calculer $\sum_{k=1}^n\cos(2k)$.
		 - b) En déduire que $\sum_{k=1}^n|\sin(k)|\geq \dfrac{n(n+1)}{4}-\dfrac{\sin(n)\cos(n+1)}{2\sin(1)}$

	=== "Corrige"  

		  A priori le résultat demandé est faux, cela se voit numériquement. Ce que l'on peut dire, c'est :

		$$
		\begin{align*}
		\sum_{k=1}^n\cos(2k)&=\mathrm{Re}(\sum_{k=1}^ne^{2ik})\\
		&=\mathrm{Re}(\sum_{k=1}^n(e^{2i})^k)\\
		&=\mathrm{Re}(e^{2i}\frac{1-e^{2in}}{1-e^{2i}})\\
		&=\mathrm{Re}(e^{(1+n)i})\frac{\sin(n)}{\sin(1)}\\
		&=\cos(n+1)\frac{\sin(n)}{\sin(1)}
		\end{align*}
		$$

		puis $\cos(2k)=1-2\sin(k)^2$ donc $\sin(k)^2=\frac12{1-\cos(2k)}$. De plus $|\sin(k)|\ge \sin^2(k)$ donc :

		$$
		\sum_{k=1}^n|\sin(k)|\ge\sum_{k=1}^n\frac 12(1-\cos(2k))=\frac n2 -\frac{\sin(n)\cos(n+1)}{2\sin(1)}.
		$$

		Je ne crois pas qu'on puisse faire mieux.


!!! exercice "RMS2022-1443"  
	=== "Enoncé"  
		  **CCINP**  .
		Soit $\omega \in \R^{+*}$. On considère l'équation fonctionnelle $(*)$ suivante, portant sur des fonctions
		$f \in \mathcal{C}^2(\mathbb{R}, \mathbb{R})$ :
		$
		\forall (x, y) \in \mathbb{R}^2, \quad f(x + y) + f(x - y) = 2f(x)\,  f(y).$

		 - a)
		Résoudre le problème de Cauchy $(E_1) \colon y'' = -\omega^2 y$, $y(0) = 1$ et $y'(0) = 0$.

		 - b)
		Résoudre le problème de Cauchy $(E_2) \colon y'' = \omega^2 y$, $y(0) = 1$ et $y'(0) = 0$.

		 - c)
		Montrer que $\forall(x, y) \in \mathbb{R}^2, \ \ch(x + y) = \ch(x) \ch(y) + \sh(x) \sh(y)$.
		En déduire que la fonction $\ch$ est solution de $(*)$.

		 - d)
		Soit $f$ une solution de $(*)$.
		Montrer que $f(0) \in \{0, 1\}$, et que si $f(0) = 0$, alors $f$ est la fonction nulle.
		Montrer que si $f(0) = 1$, alors $f'(0) = 0$.


		 - e)
		Trouver toutes les solutions $f \in \mathcal{C}^2(\mathbb{R}, \mathbb{R})$ de $(*)$.


	=== "Corrige"  

		 Première problème de Cauchy : $y(x)=\cos(wx)$.

		 Deuxième problème de Cauchy : $y(x)=\cosh(wx)$.

		 La relation demandé est une relation de trigonométrie hyperbolique classique. En sommant $\cosh(x+y)$ et $\cosh(x-y)$ on montre que $\cosh$ est solution de $(*)$. Il en va de même d'ailleurs pour $\cos$.

		 Soit $f$ une solution de $(*)$. en prenant $x=y=0$ on a $2f(0)=2f(0)^2$ et donc $f(0)\in\{0,1\}$. Si $f(0)=0$ alors en prenant $y=0$ dans $(*)$ on a $2f(x)=0$ pour tout $x\in\mathbb R$ donc $f$ est la fonction nulle. Concentrons nous sur $f(0)=1$. On dérive par rapport à $y$ l'équation $(*)$ et on prend $y=0$ : $f(x)-f(x)=2f(x)f'(0)$ donc $2f(x)f'(0)=0$. En particulier pour $x=0$ : $2f'(0)=0$ c'est-à-dire $f'(0)=0$.

		 Soit donc $f$ une solution $\mathcal C^2$ de $(*)$. En dérivant par rapport à $y$ deux fois, on a $f''(x+y)+f''(x-y)=2f(x)f''(y)$. Puis en prenant $y=0$ : $f''(x)=f(x)f''(0)$. On se ramène aux cas des deux premières questions avec $w^2=\pm f''(0)$ en fonction du signe de $f''(0)$. On en déduit que les solutions sont de la forme $x\mapsto\cos(wx)$ ou $x\mapsto \cosh(wx)$, dont on a vérifié avant qu'elles étaient effectivement solutions.  


!!! exercice "RMS2022-1448"  
	=== "Enoncé"  
		  **CCINP**  .

		  - a) Pour tout réel $\theta$, calculer $\sum_{k = 1}^n e^{i k \theta}$.

		 - b)
		Soient deux suites complexes $(a_n)_{n \geqslant 1}$ et $(b_n)_{n \geqslant 1}$. Pour  $n \in \mathbb{N}^*$,
		on pose $S_n = \sum_{k = 1}^n b_k$. Montrer que
		$\sum_{k = 1}^n a_k b_k = \sum_{k = 1}^{n - 1}(a_k - a_{k + 1})S_k + a_n S_n$.

		 - c)
		Soit $\theta  \not \equiv 0\,  [2\pi]$.
		Montrer que la suite de terme général $\sum_{k = 1}^{n - 1} \left|\left(\frac{1}{k} - \frac{1}{k + 1}\right)\sum_{m = 1}^k e^{i m \theta}\right|$
		est majorée. En déduire  que la série $\sum\frac{e^{i k \theta}}{k}$ converge.


		 - d)
		Quelle est la nature de la série précédente lorsque $\theta \equiv 0\,  [2\pi]$ ?

		 - e)
		Montrer que $f \colon x \mapsto \sum_{k = 1}^{+\infty} \frac{\sin(k x)}{k}$ est définie et continue sur $]0, 2\pi[$.

	=== "Corrige"  

		On a :$\sum_{k=1}^ne^{ik\theta}=e^{i\theta}\frac{1-e^{in\theta}}{1-e^{i\theta}}=e^{i\frac{n+1}{2}\theta}\frac{\sin\frac{n\theta}{2}}{\sin\frac\theta2}$.

		Pour la deuxième question on a :

		$$
		\begin{align*}
		\sum_{k=1}^n a_kb_k&=\sum_{k=1}^na_k(S_k-S_{k-1})\\
		&=\sum_{k=1}^n a_kS_k -\sum_{k=1}^na_kS_{k-1}\\
		&=\sum_{k=1}^n a_kS_k-\sum_{k=0}^{n-1}a_{k+1}S_k\\
		&=\sum_{k=1}^{n-1}(a_k-a_{k+1})S_k +a_nS_n-a_1S_0\\
		&=\sum_{k=1}^{n-1}(a_k-a_{k+1})S_k +a_nS_n\\
		\end{align*}
		$$

		Soit $\theta \not\equiv0[2\pi]$,

		$$
		\begin{align*}
		\sum_{k = 1}^{n - 1} \left|\left(\frac{1}{k} - \frac{1}{k + 1}\right)\sum_{m = 1}^k e^{i m \theta}\right|&\le \sum_{k=1}^{n-1}|\frac 1k-\frac 1{k+1}\frac{\sin\frac {k\theta}2}{\sin\frac\theta2}|\\
		&\le \sum_{k=1}^{n-1}(\frac1k-\frac 1{k+1})\frac 1{|\sin\frac\theta2|}\\
		&\le\frac1{|\sin\frac\theta2|}&\text{par télescopage}
		\end{align*}
		$$

		On en déduit que cette suite est bien bornée.

		On sait par ailleurs d'après les questions précédentes que :

		$$
		\sum_{k=1}^{n}\frac{e^{ik\theta}}{k}=\sum_{k=1}^{n-1}(\frac 1k-\frac1{k+1})\sum_{m=1}^ke^{im\theta}
		$$

		Or cette deuxième série est absolument convergente d'après la question précédente donc la série $\sum\frac{e^{ik\theta}}{k}$ est convergente.

		Si $\theta=0[2\pi]$, alors on a affaire à la série harmonique, divergente.

		La fonction demandée est la partie imaginaire de la série précédente (avec $\theta=x$ : elle est donc évidemment convergente pour $x \not\equiv 0[2\pi]$. La fonction $f$ est donc bien définie sur $]0,2\pi[$.


!!! exercice "RMS2022-1470"  
	=== "Enoncé"  
		  **CCINP**  .
		On étudie sur $]0, 1]$ l'équation différentielle $(E): x^2 y'' + 4 x y' + 2 y = \frac{1}{x\sqrt{x}}$.


		 - a)
		Donner les solutions de l'équation homogène de la forme $x \mapsto x^{\alpha}$.

		 - b)
		Chercher les solutions de $(E)$ sous la forme $x \mapsto \frac{z(x)}{x^2}$.

	=== "Corrige"  

		Soit $f(x)=x^\alpha$ une solution de $(E_0)$. Alors $\alpha(\alpha-1)x^\alpha+4\alpha x^\alpha+2x^\alpha = (\alpha^2+3\alpha+2)x^\alpha= 0$. On en déduit que $\alpha = -1$ ou $\alpha=-2$. On obtient alors deux solutions libres : c'est une base de l'ensemble des solutions de l'équation homogène.

		Cherchons maintenant une solution de $(E)$ $y$ sous la forme $y(x)=\frac{z(x)}{x^2}$ c'est à dire $z(x)=x^2y(x)$. On a alors $z'(x)=2xy(x)+x^2y'(x)$ et $z''(x)=2y(x)+4xy'(x)+x^2y''(x)$. L'équation $(E)$ devient $z''(x) = -\frac1{x\sqrt x}$. Une solution particulière est donc $z(x)=-4\sqrt x$ et donc $y(x)=\frac {-4}{x\sqrt x}$.

		On en déduit que l'ensemble des solutions de $(E)$ est $y(x)=\frac {-4}{x\sqrt x}+\frac ax+\frac b{x^2}$.


!!! exercice "RMS2022-1472"  
	=== "Enoncé"  
		 **CCINP**. Soit $f: [0,2]\times [-1,0] \to \R$ la fonction $(x,y) \mapsto x^{2}-2x+xy+y^{2}$. Trouver les extrémums globaux de $f$.

	=== "Corrige"  

		On commence par calculer $\frac{\partial f}{\partial x}(x,y)=2x-2+y$ et $\frac{\partial f}{\partial y}(x,y)=x+2y$. Les points critiques sont donc les points vérifiant :

		$$
		\begin{cases}
		2x+y=2\\
		x+2y=0
		\end{cases}
		\Leftrightarrow
		\begin{cases}
		x=\frac 43\\
		y=-\frac 23
		\end{cases}
		$$
		On calcule $f(\frac 43,-\frac 23)=\frac{16}{9}-\frac{8}{3}-\frac{8}{9}+\frac{4}{9}=-\frac 43.$

		Ce point est le seul candidat pour être extremum global dans l'intérieur de l'ensemble de définition. Etudions $f(x,y)+\frac 43$ pour savoir si l'on a ici un minimum ou maximum global. On pose dans la suite $x'=x-\frac 43$ et $y'=y+\frac 23$.

		$$
		\begin{align*}
		f(x,y)+\frac 43 &=x^2-2x+xy+y^2+\frac 43\\
		&=(x'+\frac 43)^2-2(x'+\frac 43)+(x'+\frac 43)(y'-\frac 23)+(y'-\frac 23)^2+\frac 43\\
		&={x'}^2+\frac 83 x'+\frac{16}{9}-2x'-\frac 83+x'y'-\frac 23 x'+\frac 43y'-\frac89+{y'}^2-\frac 43y'+\frac 49+\frac 43\\
		&={x'}^2+x'y'+{y'}^2\\
		&=\frac 12 ({x'}^2+{y'}^2)+\frac 12(x'+y')^2\\
		&\ge0
		\end{align*}
		$$

		Le point critique est donc un minimum global.

		Cherchons à voir s'il existe un maximum global. On a $f(x,y)=x^2-2x+xy+y^2=(x-1)^2-1+xy+y^2=(x-1)^2+y^2+xy-1\le 1+1+0-1=1$. Or $f(-1,0)=1$ donc c'est un majorant atteint : c'est un maximum. On peut ensuite vérifier que ce maximum global n'est atteint qu'en ce point.


!!! exercice "RMS2022-1473"  
	=== "Enoncé"  
		 **CCINP**. Pour tous réels $x,y$, on pose $f\left(x,y\right)=x^{3}+y^{3}-3xy$.

		- a) Quels sont les points critiques de $f$ ?
		- b) Est-ce que $f$ possède un extrémum global ?
		- c) Quels sont les extrémums locaux de $f$ ?

	=== "Corrige"  

		Calculons les dérivées partielles qui existent par théorèmes généraux.
		$\frac{\partial f}{\partial x}(x,y)=3x^2-3y$ et $\frac{\partial f}{\partial y}(x,y)=3y^2-3x$. Les points critiques sont alors définis par les équations $x^2=y$ et $y^2=x$ ce qui donne $x(x-1)(x^2+x+1)=0$, donc $x=0$ ou $x=1$ et  les couples $(x,y)=(0,0)$ ou $(x,y)=(1,1)$.

		Or $f(1,1)=-1$, $f(0,0)=0$, $f(-1,-1)=1$ (donc $(0,0)$ n'est pas un extremum global) et $f(-2,-2)=-28$. DONC $(1,1)$ n'est pas un extremum global.

		On a $f(t,t)=2t^3-3t^2=t^2(2t-3)$. Le point $(0,0)$ est un maximum local dans cette direction. Par ailleurs $f(t,-t)=t^3-t^3+3t^2=3t^2$. Le point $(0,0)$ est un minimum local dans cette direction. Donc $(0,0)$ n'est pas un extremum local.

		Considérons maintenant le point $(1,1)$. Posons $x'=x-1$ et $y'=y-1$.

		$$
		\begin{align*}
		f(x,y)+1&=(x'+1)^3+(y'+1)^3-3(x'+1)(y'+1)+1\\
		&=x'^3+3x'^2+3x'+1+y'^3+3y'^2+3y'+1-3x'y'-3x'-3y'-3+1\\
		&=x'^3+3x'^2+y'^3+3y'^2-3x'y'\\
		&\ge x'^3+3x'^2+y'^3+3y'^2-\frac 32(x'^2+y'^2)\\
		&\ge x'^3+y'^3+\frac32 x'^2+\frac 32 y'^2\\
		&\ge x'^2(x'+\frac 32)+y'^2(y'+\frac 32)
		\end{align*}
		$$

		La dernière quantité est positive  pour $x'>-\frac 32$ et $y'>-\frac 32$ : $(1,1)$ est donc un point de minimum local.



!!! exercice "RMS2022-1475"  
	=== "Enoncé"  
		  **CCINP**  .
		Un $n$-lancer est  un $n$-uplet $(k_1,\dots ,k_n)$ où $k_i$ correspond au $i$-ième lancer d'un  dé équilibré.
		Un $n$-lancer
		 est dit $q$-chanceux s'il n'y a jamais $q$ faces identiques à la suite.
		On note $u_n$ le nombre de $n$-lancers qui sont $q$-chanceux et $p_n$ la probabilité qu'un $n$-lancers
		soit $q$-chanceux.

		Exemple : $(1, 2, 3, 3, 3, 1, 6, 5)$ est un 8-lancers 4-chanceux mais pas 3-chanceux.


		 - a)
		Quel est le nombre de $n$-lancers ? En déduire une expression de $p_n$ en fonction de $u_n$.

		 - b)
		Pour $1 \leqslant n < q$, donner $u_n$ et $p_n$. Donner ensuite $u_q$ et $p_q$.


		 - c)
		On suppose que $q = 2$. Donner $u_n$ et $p_n$ pour $n \geqslant 2$.

		 - d)
		Pour $n \geqslant 1$, on admet que $5u_n \leqslant u_{n + 1} \leqslant 6u_n$.


		 Montrer que $(p_n)$ est décroissante et converge.
		Pour $n \geqslant 1$, montrer~: $\left(\frac{5}{6}\right)^{n - 1} \leqslant p_n \leqslant 1$.


		 - e)
		On suppose que $q = 3$. Pour $n \geqslant 3$, montrer que $u_n = 5(u_{n - 1} + u_{n - 2})$.
		Comment trouver une expression de $p_n$ en fonction de $n$~?
		 - f)
		Montrer l'encadrement de la question - c)\!.

	=== "Corrige"  

		 Le nombre de $n$ lancers est $6^n$.

		 Si $n<q$, il n'y a jamais $q$ faces identiques à la suite (car au plus, il y a $n<q$ faces identiques. Donc tous les $n$-lancers sont $q$-chanceux : $u_n=6^n$ et $p_n=1$.

		 Si $n=q$, un $n$-lancers non $q$-chanceux est un $n$ lancers dans lequel toutes les valeurs sont identiques. Le nombre de $n$-lancers non $q$-chanceux est donc $6$ (un $n$-lancer pour chaque valeur de face possible) et ainsi $u_q=6^n-6$ et $p_q=1-\frac{1}{6^{n-1}}$.

		 Posons maintenant $q=2$ et $n\ge q$. Un $n$-lancer $2$-chanceux est un $n$-lancer dans lequel il n'y a jamais deux valeurs identiques consécutives. Pour construire un tel lancer, on a $6$ choix pour le premier lancer, mais seulement $5$ pour les suivants (qui ne peuvent pas avoir la même valeur que le précédent). Cela fait $u_n=6\times 5^{n-1}$ lancers possibles et $p_n=\left(\frac{5}{6}\right)^{n-1}$.

		 Admettons que $5u_n\le u_{n+1}\le 6u_n$. On a alors en divisant par $6^{n+1}$ : $\frac 56p_n\le p_{n+1}\le p_n$. Ainsi $p_n$ est décroissante. Puisque $p_n$ est positive elle converge vers une valeur $\ell$. On peut montrer par récurrence la relation demandée : en effet, $p_1=1$ et $\left(\frac{5}{6}\right)^{1-1}\le 1\le 1$ donc l'inégalité est vraie au rang $1$. Pour l'hérédité, on suppose que $\left(\frac 56\right)^{n-1}\le p_n\le 1$, on a alors $\frac 56 p_n\le p_{n+1}\le p_n$ et par conséquent :
		 $$ \left(\frac 56\right)^n\le \frac 56p_n\le p_{n+1}\le p_n\le 1.$$
		 Ce qui termine la récurrence.

		 Posons maintenant $q=3$. Soit un $n$-lancer $3$-chanceux $(k_1,\dots,k_n)$. Deux situations peuvent se produire : soit $k_{n-1}=k_{n}$, soit $k_{n-1} \ne k_n$. Dans le premier cas, $(k_1,\dots,k_{n-2})$ est un $(n-2)$-lancer $3$-chanceux ($u_{n-2}$ choix) puis on a $5$ choix pour $k_{n-1}$ ($k_{n-1} \ne k_{n-2}$) et $1$ choix pour $k_n$ : au total $u_{n-2}\times 5$ choix). Dans le second cas, $(k_1,\dots,k_{n-1})$ est un $(n-1)$-lancer $3$-chanceux ($u_{n-1}$ choix) et $k_n \ne k_{n-1}$ ($5$ choix). Les deux cas étant disjoints, on a  bien $u_{n}=5(u_{n-2}+u_{n-1})$. On reconnaît une relation de récurrence d'ordre $2$ linéaire. Le polynôme caractéristique que $X^2-5X-5$ de discriminant $\Delta=45=9\times5$. La racines sont $r_1=\frac{5+3\sqrt 5}{2}$ et $r_2=\frac{5-3\sqrt 5}{2}$. Les solutions sont de la forme $u_n=\alpha r_1^n+\beta r_2^n$ avec $u_1=6$ et $u_2=36$.


		Considérons l'ensemble des $n$-lancers $q$-chanceux auxquels on ajoute un lancers quelconque : on a $6u_n$ tels lancers. Or un $(n+1)$-lancer $q$-chanceux est un cas particulier de tel lancer : on a donc $u_{n+1}\le 6u_n$. Considérons maintenant les $n$-lancers $q$ chanceux auxquels on ajoute un lancer différent du dernier lancer : il y a $5u_n$ tels lancers, et chacun de ces lancers est par construction un $(n+1)$-lancer $q$-chanceux. Donc $5u_n\le u_{n+1}$. On en déduit $5u_n\le u_{n+1}\le 6u_n$.


!!! exercice "RMS2022-1477"  
	=== "Enoncé"  
		  **CCINP**  .
		Soit $(X_n)_{n \in \mathbb{N}}$ une suite i.i.d. de variables aléatoires  suivant  la loi
		de Bernoulli de paramètre $p$. On pose $Y_n = X_n X_{n + 1}$.
		Donner la loi de $Y_n$, son espérance et sa variance.
		Que vaut la covariance de $Y_n$ et de $Y_{n + k}$ avec $k \in \mathbb{N}^*$ ?

	=== "Corrige"  

		Les valeurs possibles pour $Y_n$ sont $0$ ou $1$ ($Y_n$ suit aussi une loi de Brenoulli). $\mathbb P(Y_n=1)=\mathbb P(X_n=1\cap X_{n+1}=1)=\mathbb P(X_n=1)\mathbb P(X_{n+1}=1)=p^2$ et donc $\mathbb P(Y_n=0)=1-p^2$.

		On a alors $E(Y_n)=p^2$ et $V(Y_n)=p^2(1-p^2)$.

		Pour la covariance de $Y_n$ et $Y_{n+k}$, on a deux cas possibles : si $k=1$ et si $k>1$. En effet, si $k>1$ alors par lemme des coalitions, $Y_n$ et $Y_{n+k}$ sont indépendantes et $Cov(Y_n,Y_{n+k})=0$. Si maitenant $k=1$, on s'intéresse à la covariance de $Y_n$ et $Y_{n+1}$.

		$$
		\begin{align*}
		Cov(Y_n,Y_{n+1})&=E((Y_n-p^2)(Y_{n+1}-p^2))\\
		&=E(Y_nY_{n+1})-p^4\\
		&=E(X_nX_{n+1}^2X_{n+2})-p^4\\
		&=E(X_nX_{n+1}X_{n+2})-p^4\\
		&=p^3-p^4\\
		&=p^3(1-p)
		\end{align*}
		$$



!!! exercice "RMS2022-1478"  
	=== "Enoncé"  
		  **CCINP**  .
		On considère une urne remplie de $n \geqslant 2$ boules numérotées de 1 à $n$, dans laquelle on tire 2 boules
		successivement sans remise. On note $X$ (resp. $Y$) la variable aléatoire discrète donnant le numéro de la première
		(resp. deuxième) boule tirée. Donner la loi conjointe de $X$ et $Y$.

	=== "Corrige"  

		On cherche à calculer pour tout couple $i \ne j$ : $\mathbb P(X=i\cap Y=j)$.

		$$
		\begin{align*}
		P(X=i\cap Y=j)&=P(Y=j|X=i)\times P(X=i)\\
		&=\frac 1{n-1}\times \frac 1n
		\end{align*}
		$$

		et c'est tout\dots



!!! exercice "RMS2022-1479"  
	=== "Enoncé"  
		  **IMT**   .
		Soient $n \in \mathbb{N}$ et $X, Y$ deux variables aléatoires à valeurs dans $ [\![  1, n + 1  ]\!] $, de loi conjointe
		donnée par $\forall (i, j) \in  [\![  1, n + 1  ]\!] ^2, \ \P(X = i, Y = j) = \frac{1}{2^{2n}} \binom{n}{i - 1} \binom{n}{j - 1}$.


		 - a)
		Vérifier que $\forall i \in  [\![  1, n + 1  ]\!] , \ \P(X = i) = \frac{1}{2^n}\binom{n}{i - 1}$.
		Les variables $X$ et $Y$ sont-elles indépendantes ?

		 - b)
		Quelle loi suit $X - 1$ ? Calculer l'espérance et la variance de $X$.

		 - c)
		Calculer $\E(e^X)$.


	=== "Corrige"  

		 On a :

		$$
		\begin{align*}
		P(X=i)&=\sum_{j=1}^{n+1}P(X=i\cap Y=j)\\
		&=\frac 1{2^{2n}}\binom{n}{i-1}\sum_{j=1}^{n+1} \binom n{j-1}\\
		&=\frac 1{2^{2n}}\binom{n}{i-1}\sum_{j=0}^{n} \binom n{}\\
		&=\frac 1{2^{2n}}\binom{n}{i-1}2^n\\
		&=\frac 1{2^{n}}\binom{n}{i-1}\\
		\end{align*}
		$$

		ce qui est bien la valeur cherchée.

		La variable aléatoire $Z=X-1$ est à valeurs dans $\{0,\dots,n\}$ avec $P(Z=i)=P(X=i+1)=\frac 1{2^n}\binom n i$. On a alors $E(Z)=\frac1{2^n}\sum_{i=0}^n i\binom ni=\frac1{2^n}\sum_{i=1}^nn\binom{n-1}{i-1}=\frac n{2^n}\sum_{i=0}^{n-1}\binom {n-1}i=\frac n2$. On calcul de même la variance.

		Pour le calcul de $\mathbb E(e^X)$, on a :

		$$
		\begin{align*}
		\mathbb E(e^X) &= \sum_{i=1}^{n+1} e^i \mathbb P(X=i)\\
		&=\frac 1{2^n}\sum_{i=1}^{n+1}\binom n{i-1} e^i\\
		&=\frac e{2^n}\sum_{i=0}^{n}\binom ni e^i\\
		&=\frac e{2^n}(1+e)^n
		\end{align*}
		$$
