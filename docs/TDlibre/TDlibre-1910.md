# TD libre du 19/10/23

## Proposition d'Axel

**Produit de convolution.**

Soient $f, g: \mathbb{R} \rightarrow \mathbb{R}$ continues avec $g$ nulle en dehors d'un segment $[a, b]$.

1. Montrer que, pour tout $x \in \mathbb{R}, \int_{-\infty}^{+\infty} f(t) g(x-t) \mathrm{d} t=\int_{-\infty}^{+\infty} f(x-t) g(t) \mathrm{d} t$.

On pose $\varphi(x)=\int_{-\infty}^{+\infty} f(t) g(x-t) \mathrm{d} t$

2. Montrer que $\varphi$ est continue sur $\mathbb{R}$. $\varphi$ est le produit de convolution de $f$ et $g$. On le note $f * g$.

3. Montrer que si $g$ est de classe $\mathcal{C}^k$ alors $\varphi$ l'est aussi.

4. Montrer que si $f$ est intégrable alors $\varphi$ est bornée.

5. On suppose que $\int_{\mathbb{R}} g=1$ et on considère, pour tout entier $n$, la fonction $g_n: t \mapsto n g(n t)$. On pose enfin, $f_n=f * g_n$.
Montrer que, pour tout $x \in \mathbb{R}, f_n(x) \underset{n \rightarrow+\infty}{\longrightarrow} f(x)$.

## Proposition de Victor

Soient $E$ et $F$ deux espaces vectoriels de dimension fini et $u \in \mathscr{L}(E, F)$.

1. Montrer que $u$ est injective si et seulement s'il existe $v \in \mathscr{L}(F, E)$ telle que $v \circ u=\mathrm{Id}_E$.

2. Montrer que $u$ est surjective si et seulement s'il existe $v \in \mathscr{L}(F, E)$ telle que $u \circ v=\mathrm{Id}_F$.




## Proposition de Merlin 

Soir $A\in\mathcal{M}_n(\mathbb R)$ et $B\in\mathcal M_n(\mathbb R)$ le produit tensoriel de $A$ et $B$ : $A\otimes B = \begin{pmatrix} a_{11}B & \dots & a_{1n}B\\\vdots&\ddots &\vdots \\ a_{n1}B&\dots&a_{nn}B\end{pmatrix}$. 

1. Quelle est la taille de $A$ ?

2. Montrer que $\tr(A\otimes B)=\mathrm{tr}(A)\times\mathrm{tr}(B)$. 

## Deuxième proposition de Merlin 

Soit $M$ définie par blocs par $M=\begin{pmatrix} A & B \\ C & D \end{pmatrix}$ avec $A\in\mathcal G\ell_p(\mathbb R)$ et $D\in\mathcal M_q(\mathbb R)$. 

1. Déterminer $N$ une matrice triangulaire supérieure par blocs tq $MN=\begin{pmatrix} A & 0 \\C & S\end{pmatrix}$ avec $S=D-CA^{-1}B$. 

2. Montrer que $\det(M)=\det(A)\det(D-CA^{-1}B)$.

3. On prend $B\in\mathcal M_{qp}(\mathbb R)$, $C\in\mathcal M_q(\mathbb R)$, $M\in\mathcal M_{p+q}(\mathbb R)$ et $A\in\mathcal G\ell_p(\mathbb R)$, on pose $M=\begin{pmatrix} A & 0 \\ B & C \end{pmatrix}$. Montrer que $\mathrm{rg}(M)=\mathrm{rg}(A)+\mathrm{rg}(C)$. 


## Proposition d'Antoine

Calculer $\ds \sum_{k=0}^n{\binom nk}^2$ grâce à la fonction $f_n:t\mapsto t^n(1+t)^n$. 


## Proposition de Hugo

Soit $A\in\mathcal M_{np}(\mathbb K)$. 

1. Montrer que : $\mathrm{rg}(A)=1$ ou $0$ ssi  $\exists C\in\mathcal M_{n1}(\mathbb K)$ et $L\in\mathcal M_{1,p}(\mathbb K)$ tels que $A=CL$.

2. Condition nécessaire et suffisante pour que $\mathrm{rg}(A)=1$. 

## Exercice de la récolte 2023 - 1

Soit $E$ un $\mathbb{R}$-espace vectoriel de dimension $n, f$ une application dont sa matrice est $A$ dans toutes les bases de $E$.

1. Montrer que : $\forall P \in \mathrm{GL}_n(\mathbb{R}), A P=P A$.

2. Soit $B \in \mathfrak{M}_n(\mathbb{R})$.

a) Montrer qu'il existe $\lambda \in \mathbb{R}^*$ tel que $B-\lambda I_n \in \mathrm{GL}_n(\mathbb{R})$.

b) En déduire que $A B=B A$.

3. Montrer que $f$ est une homothétie

## Exercice de la récolte 2023 - 2

Soit $f: x \in \mathbb{R} \longmapsto \int_0^{+\infty} \mathrm{e}^{-t^2} \cos (2 x t) \mathrm{d} t$

1. Montrer que $f$ est définie et de classe $\mathcal{C}^1$ sur $\mathbb{R}$.

2. Trouver une relation entre $f$ et $f^{\prime}$.

3. Sachant que $f(0)=\frac{\sqrt{\pi}}{2}$, déterminer $f$ et donner sa limite en $+\infty$.

## Exercice de la récolte 2023 - 3

Soit $E=\mathbb{R}_n[X]$.

Pour $P, Q \in E$, on note : $\Phi(P, Q)=\int_0^{+\infty} P(t) Q(t) \mathrm{e}^{-t} \mathrm{~d} t$.

1. Montrer que $\Phi$ définit un produit scalaire sur $E$.

2. On note $\left(P_0, \ldots, P_n\right)$ l'orthonormalisée de Gram-Schmidt pour la base canonique de $E$. Calculer $P_i(0)$ pour $i \in \llbracket 0, n \rrbracket$ (on pourra s'intéresser à $\Phi\left(P_i, P_i^{\prime}\right)$ ).

3. On note $F=\{P \in E / P(0)=0\}$. Calculer la distance de 1 à $F$.

## Exercice de la récolte 2023 - 4

Soit $\mathrm{E}$ un espace préhilbertien réel. Soit $(u, v) \in E^2$.

Soit $\phi: E \longrightarrow E, x \longmapsto\langle u, x\rangle v-\langle x, v\rangle u$.

Soit $F=\operatorname{Vect}(u, v)$.

1. Pourquoi peut-on dire que $E=F \oplus F^{\perp}$ ?

2. Ecrire la matrice de $\phi$ dans une base adaptée à cette décomposition.

3. Montrer que $\phi$ est diagonalisable.


## Exercice de la récolte 2023 - 5

Pour tout $n \in \mathbb{N}$, on pose $I_n=\int_0^1 \ln (1+x)^n d x$.

1. Montrer que, pour tout $n, I_n$ est bien définie et $\quad 0 \leqslant I_n \leqslant(\ln 2)^n$.

2. Montrer que, pour tout $n, \quad I_{n+1}=2(\ln 2)^{n+1}-(n+1) I_n$.

3. Etudier la convergence de la série $\sum \frac{I_n}{n !}$.

4. Déterminer le rayon de convergence de la série entière $\sum \frac{I_n}{n !} x^n$.