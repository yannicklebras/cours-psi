<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD S5 (Intégrale sur un intervalle)

!!! ExerciceTD  "1 (BEOS 5847)"
    === "Énoncé"
        Pour $n \in \mathbb{N}$, on pose $I_n=\int_0^{\frac{\pi}{4}} \tan ^n x \mathrm{~d} x$.  
        1. Déterminer $\lim I_n$.  
        2. Calculer $I_n+I_{n+2}$ en utilisant $t=\tan x$.  
        3. En déduire la valeur de $\sum_{n=0}^{+\infty} \frac{(-1)^n}{2 n+1}$.  
        4. Montrer la convergence de $\sum_{n \geqslant 0}(-1)^n I_n$ et calculer sa somme.  



!!! ExerciceTD "2 (BEOS 5649)"
    === "Énoncé"
        1. Rappeler le théorème de convergence dominée.  
        2. Pour $n \geqslant 2$, on pose $I_n=n \int_0^{+\infty} \sin (x^n) \mathrm{~d} x$; montrer l'existence de $I_n$.  
        3. On donne $\int_0^{+\infty} \frac{\sin u}{u} \mathrm{~d} u=\frac{\pi}{2}$; déterminer $\lim _{n \rightarrow+\infty} I_n$.

    === "Corrigé"
        On doit montrer la convergence de l'intégrale, qui n'est certainement pas une conergence par intégrabilité. Il n'y a pas de problème en $0$ évidemment. On écrit : 



        $$  
        \begin{aligned}
        \int_1^{+\infty}\sin(x^n)dx&=\int_1^{+\infty} \frac{nx^{n-1}}{x^{n-1}}\sin(x^n)dx\\
        &=\left[\frac {-\cos(x^n)}{x^{n-1}}\right]_1^{+\infty}-(n-1)\int_1^{+\infty}\frac{\cos(x^n)}{x^{n}}dx
        \end{aligned}
        $$

        Le théorème d'intégration par partie indique ici que les deux intégrales ont même nature. Or la seconde est convergente car $n\ge 2$ donc la première l'est aussi. 

        On calcule ensuite par théorème d'IPP: 

        $$
        \begin{aligned}
        \int_1^{+\infty}n\sin(x^n)dx &\underset{u=x^n}=\int_1^{+\infty}\sin(u)u^{\frac 1n-1}du\\
        &= \cos(1)+(\frac 1n-1)\int_1^{+\infty}\cos(u)u^{\frac 1n-2}du\\
        \end{aligned}
        $$

        Posons $g_n(u)=\cos(u)u^{\frac 1n-2}$. comme ici $u\ge 1$, on a $|g_n(u)|\le \frac 1{u^{\frac 12-2}}=\frac 1{u^{3/2}}$ qui est intégrable sur $[1,+\infty[$. Par théorème de convergence dominée, $\int_1^{+\infty} \cos(u)u^{\frac 1n-2}du\to \int_1^{+\infty}\frac{\cos(u)}{u^2}du$. On en déduit que $\int_1^{+\infty}\sin(u)u^{\frac 1n-1}$ converge vers $\cos(1)-\int_1^{+\infty}\frac{\cos(u)}{u^2}du$. Or $\cos(1)-\int_1^{+\infty}\frac{\cos(u)}{u^2}du=\cos(1)-\left(\cos(1)-\int_1^{+\infty}\frac{\sin(u)}{u}du\right)=\int_1^{+\infty}\frac{\sin(u)}{u}du$. 

        Par ailleurs, le théorème de convergence dominée s'applique naturellement aussi sur $[0,1]$, avec la majoration par la constante $1$ de l'intégrande après changement de variable. On a donc $\lim_{n\to+\infty}I_n=\int_0^{+\infty}\frac{\sin(u)}{u}du$.


!!! ExerciceTD "3 (BEOS 7079)"
    === "Énoncé"
        Pour tout $x \in \mathbb{R}$, on pose $F(x)=\int_0^1 \frac{\exp \left(-x^2\left(1+t^2\right)\right)}{1+t^2} \mathrm{~d} t$.  
        1) Montrer que $F$ est de classe $\mathcal{C}^1$ sur un intervalle $I$ à déterminer.  
        2) En déduire que $\int_0^{+\infty} \exp \left(-t^2\right) \mathrm{d} t=\frac{\sqrt{\pi}}{2}$  



!!! ExerciceTD "4 (BEOS 4668)"
    === "Énoncé"
        Soit $F: x \mapsto \int_0^{+\infty} \mathrm{e}^{-x t} \frac{1-\cos (t)}{t^2} \mathrm{~d} t$.  
        1) Montrer que $F$ est définie et continue sur $\mathbb{R}_{+}$, de classe $\mathcal{C}^2$ sur $\mathbb{R}_{+}^*$.  
        2) Déterminer la limite de $F, F^{\prime}$ et $F^{\prime \prime}$ en $+\infty$.  
        3) Calculer $F(x)$ pour $x \in \mathbb{R}_{+}^*$.  
        4) En déduire la valeur de $\int_0^{+\infty} \frac{\sin t}{t} \mathrm{~d} t$.  


!!! ExerciceTD "5 (BEOS 8202)"
    === "Énoncé"
        Soit $b>0$. On pose : $I(x)=\int_0^{+\infty} \frac{\mathrm{e}^{-\frac{x}{t}}}{\sqrt{t}} \mathrm{e}^{-b t} \mathrm{~d} t$ pour $x>0$.  
        1. $I$ est-elle bien définie ? Continue ? Que dire de $I(0)$ ?  
        2. Montrer que $I$ est de classe $\mathcal{C}^1$ sur $\mathbb{R}_{+}^*$.  
        3. Montrer que pour $x>0$ :  

        $$
        I(x)=2 \int_0^{+\infty} \mathrm{e}^{-\frac{x}{u^2}} \mathrm{e}^{-b u^2} \mathrm{~d} u \quad, \quad I^{\prime}(x)=\int_0^{+\infty}-2 \frac{\mathrm{e}^{-\frac{x}{u^2}} \mathrm{e}^{-b u^2}}{u^2} \mathrm{~d} u
        $$

        4. Montrer à l'aide d'un changement de variable judicieux que :  

        $$
        \forall x>0, I^{\prime}(x)=-\sqrt{\frac{b}{x}} I(x)
        $$

        5. En déduire l'expression de $I$.  


!!! ExerciceTD "6 (BEOS 5859)"
    === "Énoncé"
        Pour tout $x$ réel, on pose : $S(x)=\int_0^{+\infty} \sin (x t) \mathrm{e}^{-t^2} \mathrm{~d} t$ et $C(x)=\int_0^{+\infty} t \cos (x t) \mathrm{e}^{-t^2} \mathrm{~d} t$.  
        a) Montrer que $C$ et $S$ sont bien définies sur $\mathbb{R}$. Sont-elles continues ?  
        b) Montrer que $S$ est dérivable. Exprimer $S^{\prime}(x)$ au moyen de $C(x)$.  
        c) Montrer que: $\forall x \in \mathbb{R}, C(x)=\frac{1}{2}-\frac{x}{2} S(x)$.  
        d) En déduire $S(x)$ et $C(x)$, exprimées au moyen d'une intégrale.  


!!! ExerciceTD "7 (BEOS 5711)"
    === "Énoncé"
        Soit $f: t \mapsto \int_0^{+\infty} \frac{\sin (t u)}{e^u-1} \mathrm{~d} u$  
        1) Montrer $f$ est de classe $C^0$ sur $\mathbb{R}$  
        2)Montrer $f$ est de classe $C^1$ sur $\mathbb{R}$  
        3) Montrer que
        $\forall t \in \mathbb{R} \quad f(t)=\sum_{n=1}^{+\infty} \frac{t}{n^2+t^2}$  
        4) Montrer $f$ est de classe $C^{\infty}$ sur $\mathbb{R}$  


!!! ExerciceTD "8 (RMS 973)"
    === "Énoncé"
        On pose $I=\int_0^{+\infty} \frac{e^{-t}}{\sqrt{t}} \mathrm{~d} t$. Soit $x \geqslant 0$. On pose $F(x)=\int_0^{+\infty} \frac{e^{-x t}}{\sqrt{t}(t+1)} \mathrm{d} t$.  
        a) Montrer que $F$ est bien définie.  
        b) Déterminer une équation différentielle linéaire d'ordre 1 vérifiée par $F$ sur $] 0,+\infty[$.  
        c) Calculer $F(0)$ et $\lim _{x \rightarrow+\infty} F(x)$.  
        d) En déduire la valeur de $I$.  






!!! ExerciceTD "9 (RMS 1159)"
    === "Énoncé"

        Soit $f: x \mapsto \int_0^{+\infty} \frac{t^{x-1}}{1+t^2} \mathrm{~d} t$.  
        a) Déterminer le domaine de définition de $f$.   
        b) La fonction $f$ est-elle continue, de classe $\mathcal{C}^1$, de classe $\mathcal{C}^{\infty}$ ?  
        c) Déterminer les limites de $f$ aux bornes du domaine de définition.  


!!! ExerciceTD "10 (RMS 1306)"
    === "Énoncé"
        On pose $F: x \mapsto \int_0^{+\infty} \frac{\arctan (x t)}{t+t^3} \mathrm{~d} t$.  
        a) Montrer que $F$ est de classe $\mathcal{C}^1$ sur $\mathbb{R}^{+}$  
        b) Pour tout $x$ réel positif, calculer $F(x)$.  
        c) Justifier la définition de $J=\int_0^{\frac{\pi}{2}} \ln (\sin \theta) \mathrm{d} \theta$ et calculer sa valeur.  



