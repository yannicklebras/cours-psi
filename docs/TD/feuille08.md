<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD S08 Espaces Euclidiens


!!! ExerciceTD "RMS2023 942"
    === "Énoncé"
        Soit $E=\mathbb{R}_3[X]$. On pose, pour $i \in \llbracket 0,3 \rrbracket: L_i=\prod_{k \in \llbracket 0,3 \rrbracket \backslash\{i\}} \frac{X-k}{i-k}$.  
        a) Calculer $L_i(j)$ pour $j \in \llbracket 0,3 \rrbracket$. Montrer que $\left(L_0, L_1, L_2, L_3\right)$ est une base de $E$.  
        b) Soit $\phi:(P, Q) \in E^2 \mapsto \sum_{k=0}^3(P(k)+P(k+1))(Q(k)+Q(k+1))$. Montrer qu'il s'agit d'un produit scalaire.  
        c) Trouver une base orthonormale pour ce produit scalaire.  


!!! ExerciceTD "RMS2023 945"
    === "Énoncé"
        Calculer $\inf _{(a, b, c) \in \mathbb{R}^3} \int_{-1}^1\left(t^4-a t^2-b t-c\right)^2 \mathrm{~d} t$.
 
!!! ExerciceTD "RMS2023 1327"
    === "Énoncé"
        Soient $(E,\langle\rangle)$ un espace euclidien et $p \in \mathcal{L}(E)$ un projecteur. Montrer que $\forall x \in E$, $\|p(x)\| \leqslant\|x\|$ si et seulement si $p$ est un projecteur orthogonal.
        

!!! ExerciceTD "BEOS6090"
    === "Énoncé"
        1. Montrer que $(\cdot \mid \cdot): \quad \mathcal{C}^0([0,1], \mathbb{R}) \times \mathcal{C}^0([0,1], \mathbb{R}) \quad \longrightarrow \quad \mathbb{R}$

        $$
        (f, g) \longmapsto \int_0^1 x^2 f(x) g(x) \mathrm{d} x
        $$

        est un produit scalaire sur $\mathcal{C}^0([0,1], \mathbb{R})$.  
        2. Pour tout $n \in \mathbb{N}$, montrer que $\int_0^1 x^n \ln x \mathrm{~d} x$ est définie et la calculer.  
        3. Soit $F$ le sous-espace des fonctions affines sur $[0,1]$. Déterminer la projection orthogonale de $x \mapsto x \ln x$ sur $F$.

!!! ExerciceTD "BEOS2520"
    === "Énoncé"
        Soit $a_0, \ldots, a_n$ des réels deux à deux distincts. On pose :

        $$
        \forall(P, Q) \in \mathbb{R}_n[X]^2,(P \mid Q)=\sum_{k=0}^n P\left(a_k\right) Q\left(a_k\right)
        $$

        1. Montrer qu'il s'agit d'une produit scalaire.  
        2. On pose : $F=\left\{P \in \mathbb{R}_n[X] / \sum_{k=0}^n P\left(a_k\right)=0\right\}$.  
        a) Justifier rapidement que $F$ est un sous-espace vectoriel de $\mathbb{R}_n[X]$, calculer sa dimension ainsi que son orthogonal.  
        b) Calculer la distance de $X^n$ à $F$.  



!!! ExerciceTD "BEOS8404"
    === "Énoncé"
        Soit $(E, \langle \cdot, \cdot \rangle)$ un espace préhilbertien de dimension $n$, $a$ un réel différent de $1$ et $(u_1, \dots, u_{n+1})$ une famille de vecteurs unitaires de $E$ telle que :
        
        $$
        \forall (i, j) \in \{1, \dots, n+1\}^2, \; i \neq j \Rightarrow \langle u_i, u_j \rangle = a
        $$


        1. Calculer $\forall j \in \{1, \dots, n\}$ le produit scalaire $\left\langle \sum_{i=1}^{n+1} \lambda_i u_i , u_j - u_{n+1} \right\rangle$.

        2. Montrer que $(u_1, \dots, u_n)$ est une base de $E$.

        3. Que se passe-t-il si $a=1$ ?
        

        
        

!!! ExerciceTD "BEOS1618"
    === "Énoncé"
        $$
        \begin{array}{cccc}
        \phi: & M_n(\mathbb{R}) & \rightarrow & \mathbb{R} \\
        \text { Soit } & (M, N) & \mapsto & \sum_{1 \leqslant i, j \leqslant n} m_{i, j} n_{i, j}
        \end{array}
        $$

        1. Montrer que $\phi$ est un produit scalaire  
        2. Soit $H=\left\{M \in M_n(\mathbb{R}), \sum_{1 \leqslant i, j \leqslant n} m_{i, j}=0\right\}$ et $A \in M_n(\mathbb{R})$.

        Trouver $d=\inf _{M \in H} \sum_{1 \leqslant i, j \leqslant n}\left(a_{i, j}-m_{i, j}\right)^2$.
        
!!! ExerciceTD "BEOS4479"
    === "Énoncé"
        Soit $E$ un espace euclidien de dimension $n$.
        Soit $f$ un endomorphisme de $E$ tel que : $\forall(x, y) \in E^2:(x \mid y)=0 \Rightarrow(f(x) \mid f(y))=0$.  
        Soit $\left(e_1, \cdots, e_n\right)$ une base orthonormée de $E$.  
        1. $\forall(i, j) \in\{1, \cdots, n\}$ Calculer $\left(f\left(e_i+e_j\right) \mid f\left(e_i-e_j\right)\right)$  
        2. Montrer qu'il existe $\alpha \in \mathbb{R}_{+}$tel que $\forall i \in\{1, \cdots, n\},\left\|f\left(e_i\right)\right\|=\alpha$.


        
       


!!! ExerciceTD "RMS2023 1564"
    === "Énoncé"
        Soit $E=\mathcal{C}^0([-1,1], \mathbb{R})$. Pour $(f, g) \in E^2$, on pose $\langle f, g\rangle=\int_{-1}^1 f(t) g(t) \mathrm{d} t$.

        Soient $F=\{f \in E ; \forall x \in[0,1], f(x)=0\}$ et $G=\{g \in E ; \forall x \in[-1,0], g(x)=0\}$.  
        a) Montrer que $\langle,\rangle$ est un produit scalaire sur E.  
        b) Montrer que $F$ et $G$ sont en somme directe orthogonale.  
        c) Les sous-espaces $F$ et $G$ sont-ils supplémentaires?  
        d) Montrer que $G \subset F^{\perp}$ puis que $G=F^{\perp}$.  
        
!!! ExerciceTD "RMS2023 1435"
    === "Énoncé"
        On pose $E=\mathcal{C}^1([0,1], \mathbb{R})$. Pour $(f, g) \in E^2$, on pose $\langle f, g\rangle=\int_0^1\left(f g+f^{\prime} g^{\prime}\right)$.  
        a) Montrer que $\langle,\rangle$ est un produit scalaire.  

        Soient $V=\{f \in E, f(0)=f(1)=0\}$ et $W=\left\{f \in E, f^{\prime \prime}=f\right\}$.  
        b) Montrer que $V$ et $W$ sont des sous-espaces vectoriels puis que $\left\{t \mapsto e^t, t \mapsto e^{-t}\right\}$ est une base de $W$.  
        c) Montrer que $V$ et $W$ sont orthogonaux.  
        d) Calculer $p_W(f)$ le projeté orthogonal de $f \in E$ sur $W$.   
        e) Montrer que $V$ et $W$ sont supplémentaires.  
        
