<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>


!!! ExerciceTD "1 (BEOS 8472)"

    === "Énoncé"
        1) Montrer que $\displaystyle \mathcal{M}_n(\mathbb{R})=\mathcal{S}_n(\mathbb{R}) \oplus \mathcal{A}_n(\mathbb{R})$.

        2) Montrer que $\displaystyle \mathcal{S}_n(\mathbb{R})=\left(\mathcal{A}_n(\mathbb{R})\right)^{\perp}$.

        3) Soit $\displaystyle M=\left(\begin{array}{lll}1 & 2 & 3 \\ 4 & 5 & 6 \\ 7 & 8 & 9\end{array}\right)$. Quelle est la distance de $\displaystyle M$ à $\displaystyle \mathcal{S}_n(\mathbb{R})$ ?


!!! ExerciceTD "2 (BEOS 8461)"

    === "Énoncé"
        Soit $\displaystyle \varphi:\left\{\begin{array}{lll}\mathcal{M}_n(\mathbb{R}) & \rightarrow & \mathcal{M}_n(\mathbb{R}) \\ A & \mapsto & -A+\operatorname{tr}(A) I_n\end{array}\right.$.

        1) Montrer que $\displaystyle \varphi$ est un endomorphisme de $\displaystyle \mathcal{M}_n(\mathbb{R})$.

        2) Montrer que $\displaystyle \operatorname{ker}(\operatorname{tr})$ est un hyperplan de $\displaystyle \mathcal{M}_n(\mathbb{R})$.

        3) Existe-t'il une base dans laquelle la matrice de $\displaystyle \varphi$ est diagonale ?


!!! ExerciceTD "3 (BEOS 8461)"

    === "Énoncé"
        Soit $\displaystyle n$ un entier naturel, on dispose initialement d'une urne constituée d'une boule blanche, et d'une pièce de monnaie équilibrée. On effectue des lancers de la pièce selon la règle suivante : 

        - si on obtient "Face" on ajoute une boule noire dans l'urne ;
        
        - si on obtient "Pile" ou si on a atteint le $\displaystyle n$-ème tour, on tire une boule dans l'urne et on arrête l'expérience.


        Soit $\displaystyle X$ la variable aléatoire donnant le nombre de lancers de la pièce.  
        1) Quelle est la loi de $\displaystyle X$ ?  
        2) Quelle est la probabilité d'obtenir une boule blanche à la fin de l'expérience (on donnera une forme avec un symbole de somme) ?






!!! ExerciceTD "4 (BEOS 8445)"

    === "Énoncé"
        Soit $\displaystyle n \in \mathbb{N}^*$. Une urne contient $\displaystyle n$ boules blanches numérotées de 1 à $\displaystyle n$ et deux boules noires numérotées 1 et 2 . On effectue le tirage une à une, sans remise, de toutes les boules de l'urne.
        On suppose que tous les tirages sont équiprobables.
        On note $\displaystyle X$ la variable aléatoire égale au rang d'apparition de la première boule blanche.
        On note $\displaystyle Y$ la variable aléatoire égale au rang d'apparition de la première boule numérotée 1.  
        1) Déterminer la loi de $\displaystyle X$.  
        2) DéTerminer la loi de $\displaystyle Y$.  


!!! ExerciceTD "5 (BEOS 8444)"
    
    === "Énoncé"
        Une urne contient des boules numérotées de 1 à $\displaystyle n$. On effectue des tirages avec remise.
        Soit $\displaystyle X$ le numéro de la première boule et $\displaystyle Y$ le numéro de la deuxième boule. On note $\displaystyle U=\min (X, Y)$ et $\displaystyle V=\max (X, Y)$.  
        1) Déterminer les lois de $\displaystyle U$ et $\displaystyle V$.  
        2) Déterminer $\displaystyle E(U)$ et $\displaystyle E(V)$.  
        3) Donner une relation entre $\displaystyle X, Y, U+V$.  
        4) Calculer $\displaystyle P(X+Y=k)$.  


!!! ExerciceTD "6 (BEOS 8455)"

    === "Énoncé"
        Soit $\displaystyle P \in \mathbb{C}[X]$.  
        1) Déterminer le reste de la division euclidienne de $\displaystyle P \operatorname{par}(X-a)(X-b)$ (on pourra traiter séparément les cas $\displaystyle a=b$ et $\displaystyle a \neq b$ ).  
        2) Déterminer le reste de la division euclidienne de $\displaystyle (X+1)^{2 n+1}-X^{2 n}$ par $\displaystyle X^2+X+1$.


!!! ExerciceTD "7 (BEOS 8325)"

    === "Énoncé"
        Déterminer les solutions sur $\displaystyle ] 0,1[$ et $\displaystyle ] 1,+\infty[$ de l'équation différentielle :

        $$
        y^{\prime} x \ln x=y(3 \ln x+1)
        $$

        puis par recollement les solutions sur $\displaystyle ] 0,+\infty[$.

!!! ExerciceTD "8 (BEOS 8285)"

    === "Énoncé"
        Soit $\displaystyle F=\left\{(x, y, z, t) \in \mathbb{R}^4 \mid x+y+z+t=x-y-z+t=0\right\}$.  
        1. Montrer que $\displaystyle F$ est un plan de $\displaystyle \mathbb{R}^4$ et en déterminer une base.  
        2. Donner la matrice dans la base canonique de $\displaystyle \mathbb{R}^4$, de la projection orthogonale sur $\displaystyle F$ (pour le produit scalaire usuel).  
        3. Soit $\displaystyle u=(1,1,1,1)$. Déterminer $\displaystyle d\left(u, F^{\perp}\right)$.

!!! ExerciceTD "9 (BEOS 8203)"

    === "Énoncé"
        Soit $\displaystyle p \in \mathbb{N}$ fixé.

        1. Montrer que $\displaystyle \sum_{n \geqslant 0} \frac{n^p}{2^n}$ converge.

        2. On pose $\displaystyle S_p=\sum_{n=0}^{+\infty} \frac{n^p}{2^n}$

        a) Exprimer $\displaystyle S_p$ en fonction de $\displaystyle S_0, \ldots, S_{p-1}$ en faisant apparaitre le développement de $\displaystyle (n+1)^p$.

        b) Montrer que $\displaystyle \forall p \in \mathbb{N}, S_p \in \mathbb{N}$.


!!! ExerciceTD "10 (BEOS 8089)"
    === "Énoncé"

        Soit $\displaystyle n>1$ entier. On considère le polynôme $\displaystyle P$ tel que :

        $$
        \forall z \in \mathbb{C}, P(z)=\sum_{k=0}^{n-1} z^k
        $$

        1. Déterminer les racines de $\displaystyle P$.

        2. Montrer que : $\displaystyle \prod_{k=1}^{n-1} 1-\mathrm{e}^{2 i \frac{k \pi}{n}}=n$.

        3. En déduire que : $\displaystyle \prod_{k=1}^{n-1} \sin \frac{k \pi}{n}=\frac{n}{2^{n-1}}$.