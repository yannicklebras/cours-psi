<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>



# TD S12 Probabilités de sup
# TD12 Probabilités

!!! ExerciceTD "BEOS6524"
    === "Énoncé"
        Soient $A, B$ deux variables aléatoires suivant une loi uniforme sur $\{0,1,2\}$.  
        Soit $M=\left(\begin{array}{ccc}1 & A-B & 0 \\ 0 & A & A-1 \\ 0 & 0 & B\end{array}\right)$.  
        Trouver la probabilité que $M$ soit diagonalisable.




!!! ExerciceTD "BEOS7550"
    === "Énoncé"
        Soit $n \in \mathbb{N}^*$ puis $X$ et $Y$ deux variables aléatoires définies sur un même espace probabilisé et à valeurs dans $[\![ 1, n+1 ]\!]$ dont la loi de couple est donnée par :

        $$
        \forall(i, j) \in \llbracket 1, n+1 \rrbracket^2, \mathrm{P}(X=i, Y=j)=\lambda\binom{n}{i-1}\binom{n}{j-1} .
        $$
 
        1. Montrer que $\lambda=\frac{1}{4^n}$.  
        2. Déterminer les lois marginales de $X$ et $Y$. Les variables aléatoires $X$ et $Y$ sont-elles indépendantes ?  
        3. Déterminer l'espérance et la variance de $X$.  
        4. Soit $B=\left(b_{i, j}\right)_{1 \leqslant i, j \leqslant n+1} \in \mathscr{M}_{n+1}(\mathbb{R})$ telle que $b_{i, j}=\mathrm{P}(X=i, Y=j)$ pour tout $(i, j) \in \llbracket 1, n+1 \rrbracket^2$.  
        a. Justifier que $B$ est diagonalisable.  
        b. En calculant $B^2$, déterminer les valeurs propres de $B$ et donner la dimension des sous-espace propres associés.  





!!! ExerciceTD "BEOS8167"
    === "Énoncé"
        On considère une urne composée de $n$ boules :  
        - une est rouge,  
        - $b$ sont blanches,  
        - $n-b-1$ sont noires.  

        On note $p=\frac{b}{n-1}$.  
        L'expérience consiste à tirer des boules avec remise et de s'arrêter lorsque la boule rouge est tirée.
        On note :  
        - $T$ la variable aléatoire comptant le nombre de tirages effectués en incluant celui de la boule rouge,  
        - $X$ la variable aléatoire comptant le nombre de boules blanches tirées.  
        1. Soit $r \in \mathbb{N}$. Déterminer le rayon de convergence de la série entière $\sum_{n \geqslant r}\binom{n}{r} x^{n-r}$ dont on calculera la somme.  
        2. Donner la loi de probabilité de $T$.  
        3. Pour $k \in \mathbb{N}^*, i \in \mathbb{N}$, calculer $P_{(T=k)}(X=i)$.  
        4. En déduire la loi de $X$, calculer son espérance et sa variance.






!!! ExerciceTD "BEOS8256"
    === "Énoncé"
        Soit $\left(X_i\right)_{i \in \mathbb{N}}$ une suite de variables aléatoires réeelles indépendantes suivant la loi de Bernoulli de paramètre $p$.
        On pose $Y_i=X_i X_{i+1}$ pour tout $i \in \mathbb{N}$.  
        1) Donner la loi de $Y_i$  
        2) Calculer $E\left(Y_i\right), V\left(Y_i\right)$ et $\operatorname{Cov}\left(Y_i, Y_j\right)$.  
        3) On pose $F_n=\frac{S_n}{n}$ où $S_n=\sum_{i=1}^n Y_i$. Pour tout $\varepsilon>0$, montrer que $P\left(F_n-p^2 \geqslant \varepsilon\right) \xrightarrow[n \rightarrow+\infty]{\longrightarrow} 0$.




!!! ExerciceTD "BEOS7178"
    === "Énoncé"
        On considère une urne contenant $n$ boules numérotées de 1 à $n$.  
        On effectue $n$ tirages successifs avec remise.  
        On note $X_k$ la variable aléatoire correspondant au nombre de boules distinctes tirées au bout de $k$ tirages.  
        1. Déterminer $P\left(X_{k+1}=i\right), i \in [\![ 1, n]\!]$.  
        2. Calculer $E\left(X_k\right)$.












!!! ExerciceTD "RMS2023-1314"
    === "Énoncé"
        Soient $a$ et $b$ deux entiers naturels non nuls et $N$ leur somme. Une urne contient initialement $a$ boules vertes et $b$ boules rouges. On effectue une suite de tirages selon le protocole suivant : si la boule tirée est rouge, on la remet dans l'urne; si elle est verte, elle est remplacée par une boule rouge prise dans une réserve annexe.  
        On définit deux variables aléatoires: $T_k$ vaut 1 si l'on pioche une boule verte au $k$-ième tirage et 0 sinon; $X_k$ est le nombre de boules vertes piochées lors des $k$ premiers tirages.  
        a) Déterminer la loi de $T_1$ et celle de $T_2$.  
        b) Montrer que $\mathbf{P}\left(T_{n+1}=1\right)=\frac{a-\mathbf{E}\left(X_n\right)}{N}$. En déduire que $\mathbf{P}\left(T_n=1\right)=a \frac{(N-1)^{n-1}}{N^n}$.  
        c) Calculer $\mathbf{E}\left(X_n\right)$ puis déterminer sa limite quand $n$ tend vers l'infini.





!!! ExerciceTD "RMS2023-1601"
    === "Énoncé"
        Soient $X_1$ et $X_2$ deux variables aléatoires indépendantes suivant la loi $\mathcal{B}((n, 1 / 2)$.
        
        Soit $M=\left(\begin{array}{cc}X_1 & 1 \\ 0 & X_2\end{array}\right)$.  
        a) En développant le polynôme $(1+X)^{2 n}$ de deux manières, montrer $\sum_{k=1}^n\binom{n}{k}^2=\binom{2 n}{n}$.  
        b) Calculer la probabilité de l'événement $\left(X_1=X_2\right)$.  
        c) Calculer la probabilité que $M$ soit diagonalisable.





!!! ExerciceTD "RMS2023-1604"
    === "Énoncé"
        On considère une urne avec deux boules rouges et deux boules blanches. À chaque instant, si on pioche une boule rouge, on la remet dans l'urne; si on pioche une boule blanche, on ne la remet pas. On note $U_n=\left(\begin{array}{l}\mathbf{P}\left(X_n=0\right) \\ \mathbf{P}\left(X_n=1\right) \\ \mathbf{P}\left(X_n=2\right)\end{array}\right)$ avec $X_n$ le nombre de boules blanches dans l'urne. On suppose qu'à l'instant $t=0$, le contenu de l'urne est celui donné initialement  
        a) i) Soit $M=\left(\begin{array}{lll}6 & 2 & 0 \\ 0 & 4 & 3 \\ 0 & 0 & 3\end{array}\right)$. Diagonaliser $M$ et donner ses sous-espaces propres.  
        ii) Exprimer $M^n$ pour $n \in \mathbb{N}$.  
        b) Déterminer $A$ telle que $\forall n \in \mathbb{N}$, $U_{n+1}=A U_n$.  
        c) On note $T_1$ la variable aléatoire qui donne le numéro du tirage où l'on pioche une boule blanche pour la première fois. Donner la loi de $T_1$.  
        d) On note $T_2$ la variable aléatoire qui donne le numéro du tirage où l'on pioche la deuxième boule blanche. Donner la loi de $T_2$.





!!! ExerciceTD "RMS2023-1697"
    === "Énoncé"
        On considère deux dés, un blanc et un noir. Le dé noir est pipé au sens où la probabilité de tirer un 6 vaut $1 / 3$ et les autres faces ont la même probabilité d'être tirées. Le dé blanc n'est pas pipé. Deux joueurs s'affrontent, chacun choisit un dé et le lance une fois. Un joueur gagne s'il a obtenu le plus grand chiffre ou, en cas d'égalité, s'il a le dé blanc. Quel dé a-t-on intérêt à choisir?






!!! ExerciceTD "RMS2023-1703"
    === "Énoncé"
        Soit $M \in \mathcal{M}_3(\mathbb{R})$. On suppose que les coefficients $M_{i, j}$ sont des variables aléatoires i.i.d. de loi de Bernoulli de paramètre $p$.  
        a) Donner la loi de $\operatorname{tr}(M)$.  
        b) Calculer $\mathbf{E}(\operatorname{det}(M))$.  
        c) On suppose maintenant que les variables aléatoires $M_{1,3}, M_{2,3}, M_{3,1}, M_{3,2}$ sont nulles.  
        Donner la probabilité que $M$ soit diagonalisable.  
        d) Sous les mêmes hypothèses que ci-dessus, donner la probabilité que $M$ soit inversible.  


