<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD S6 (Compléments d'algèbre linéaire)




!!! ExerciceTD "1 BEOS 1724"  
    === "Énoncé"
        Soit $A, B \in \mathcal{M}_n(\mathbb{K})$. On définit $M \in \mathcal{M}_{3 n}(\mathbb{K})$ :  

        $$
        M=\left[\begin{array}{lll}
        A & A & A \\
        A & B & A \\
        A & B & B
        \end{array}\right]
        $$

        1) Calculer le rang de $M$    
        2) Calculer $M^{-1}$ lorsqu'elle existe, en fonction de $A^{-1}$ et de $(B-A)^{-1}$  


!!! ExerciceTD "2 BEOS 8321"  
    === "Énoncé"
        Soit $A \in \mathcal{M}_n(\mathbb{C})$. On définit :  

        $$
        u: \left\lvert\, \begin{array}{ccc}
        \mathcal{M}_n(\mathbb{C}) & \longrightarrow & \mathcal{M}_n(\mathbb{C}) \\
        X & \longmapsto & -X+\operatorname{tr}(X) A
        \end{array}\right.
        $$

        1. Montrer que $u$ est linéaire.  
        2. Montrer que $u$ est injective si et seulement si $\operatorname{tr}(A) \neq 1$.  
        3. En déduire une condition pour que $u$ ne soit pas bijective.  
        4. Discuter selon $A$ et $B \in \mathcal{M}_n(\mathbb{C})$ de l'existence de solutions de l'équation $u(X)=B$.  

!!! ExerciceTD "3 BEOS-6901"  
    === "Énoncé"
        Soient $A, B \in \mathbb{M}_n(\mathbb{R})$ telles que $A B-B A=A$.  
        Soit $f$ telle que : $\forall X \in \mathbb{M}_n(\mathbb{R}), f(X)=X B-B X$.  
        1) Montrer que $f$ est un endomorphisme.  
        2) Quelle est la trace de $A$ ? Celle de $A^k$ pour tout $k \in \mathbb{N}$ ?  
        3) Montrer que : $\forall k \in \mathbb{N}, f\left(A^k\right)=k A^k$.  
        4) En déduire que $A$ est nilpotente, c'est-à-dire qu'il existe $k_0 \in \mathbb{N}$ tel que $A^{k_0}=0$.  



!!! ExerciceTD "4 BEOS 6927"
    === "Énoncé"

        Soient $E$ et $F$ deux espaces de dimension finie et $u, v \in \mathcal{L}(E, F)$.  
        Montrer que $\operatorname{dim}(\operatorname{ker}(u+v) \leq \operatorname{dim}(\operatorname{ker}(u) \cap \operatorname{ker}(v))+\operatorname{dim}(\operatorname{Im}(u) \cap \operatorname{Im}(v))$.  


!!! ExerciceTD "5 BEOS 5479"
    === "Énoncé"

        Soient $E$ et $F$ deux espaces vectoriels de dimension finie. Soit $W$ un sous-espace vectoriel de $E$. On pose $\mathcal{A}=\{u \in \mathcal{L}(E, F) / W \subset \operatorname{Ker} u\}$.  
        1. Montrer que $\mathcal{A}$ est un sous-espace vectoriel de $\mathcal{L}(E, F)$.  
        2. Exprimer la dimension de $\mathcal{A}$ en fonction des dimensions de $E, F, W$.  


!!! ExerciceTD "6 BEOS 2509"
    === "Énoncé"
        Soient $E$ un espace vectoriel sur $\mathbb{K}=\mathbb{R}$ ou $\mathbb{C}$, et $u$ et $v$ deux endomorphismes de $E$ qui commutent. On suppose que $\operatorname{Ker} u \cap \operatorname{Ker} v=\{0\}$.  
        1. Pour $(a, b) \in \mathbb{K}^2$ tel que $a \neq b$, montrer que $\operatorname{Ker}(u-a v) \cap \operatorname{Ker}(u-b v)=\{0\}$.  
        2. Plus généralement, si $a_1, \ldots, a_n$ sont des scalaires distincts, montrer que la somme des sous-espaces vectoriels $\operatorname{Ker}\left(u-a_k v\right)$ pour $k \in\{1, \ldots, n\}$ est directe.  


!!! ExerciceTD "7 RMS2023 928"
    === "Énoncé"

        Soit $M=\left(\begin{array}{l|l}A & A \\ \hline A & B\end{array}\right)$ avec $A, B \in \mathcal{M}_n(\mathbb{R})$. Trouver une condition nécessaire et suffisante sur $A$ et $B$ pour que $M$ soit inversible. Calculer alors $M^{-1}$.  

!!! ExerciceTD "8 RMS2023 1003"  
    === "Énoncé"
        Soient $A, B \in \mathcal{M}_n(\mathbb{R})$. Quelle est la trace de l'endomorphisme de $\mathcal{M}_n(\mathbb{R})$ défini par $\varphi: M \mapsto A M B$ ?  

!!! ExerciceTD "9 RMS2023 1619"  
    === "Énoncé"
        Soit $U$ le vecteur de $\mathcal{M}_{n, 1}(\mathbb{R})$ dont toutes les composantes valent 1. On pose $\varphi:(X, Y) \in \mathcal{M}_{n, 1}(\mathbb{R})^2 \mapsto X U^T+U Y^T$.  

        a) Donner les dimensions de $\mathcal{M}_n(\mathbb{R})$ et de $\mathcal{M}_{n, 1}(\mathbb{R})^2$ et montrer que $\varphi$ est linéaire.  

        b) Donner le rang de $\varphi$.  


!!! ExerciceTD "10 RMS2023 1319" 
    === "Énoncé"

        Soit $M \in \mathcal{M}_n(\mathbb{C})$.  
        a) Pour $k \in \mathbb{N}$, montrer que $\operatorname{Ker}\left(M^k\right) \subset \operatorname{Ker}\left(M^{k+1}\right)$ puis montrer qu'il existe $k_0 \in \mathbb{N}^*$ tel que $\operatorname{Ker}\left(M^{k_0}\right) \neq \operatorname{Ker}\left(M^{k_0-1}\right)$ et, pour tout $p \geqslant k_0, \operatorname{Ker}\left(M^p\right)=\operatorname{Ker}\left(M^{k_0}\right)$.  
        b) On suppose qu'il existe $p \in \mathbb{N}^*$ tel que $M^{p-1} \neq 0$ et $M^p=0$. Montrer que $M$ est semblable à une matrice triangulaire supérieure à diagonale nulle.  
        c) Montrer que le noyau de la trace est égal au sous-espace vectoriel engendré par les matrices $M$ pour lesquelles il existe $p \in \mathbb{N}^*$ tel que $M^p=0$.  







