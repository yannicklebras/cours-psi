<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD S2



!!! ExerciceTD "1 (BEOS 8441)"
    === "Énoncé"
        Soit $r \in]-1,1$. On définit $f_n: x \mapsto r^n \cos n x$ pour tout $n \in \mathbb{N}$.  
        1) Montrer que $\sum_{n \geqslant 0} f_n$ converge normalement sur $\mathbb{R}$.  
        2) Soit $x \in \mathbb{R}$. Déterminer la fonction $S$ définie par

        $$
        S: x \mapsto \sum_{n=0}^{+\infty} f_n(x)
        $$

        3) En déduire que :

        $$
        \forall x \in \mathbb{R}, \frac{1-r^2}{1+r^2-2 r \cos x}=1+2 \sum_{n=1}^{+\infty} f_n(x)
        $$

        4) Soit $(k, n) \in \mathbb{N}^2$. Calculer $\int_0^{2 \pi} \cos k x \cos n x \mathrm{~d} x$.  
        5) En déduire, pour $k$ dans $\mathbb{N}$, l'expression de

        $$
        I_k=\int_0^{2 \pi} \frac{\cos k x}{1+r^2-2 r \cos x} \mathrm{~d} x
        $$



!!! ExerciceTD "2 (BEOS 8124)" 
    === "Énoncé"
        On pose $f(x)=\sum_{n=1}^{+\infty} \frac{1}{n} \cos ^n x \sin n x$.  
        1. Montrer que $f$ est de classe $\mathcal{C}^1$ sur $] 0, \frac{\pi}{2}[$.  
        2. Expliciter $f(x)$ sans utiliser le symbole $\sum$.


!!! ExerciceTD "3 (BEOS 8113)"
    === "Énoncé"
        On définit, pour tout $n \in \mathbb{N}^*, f_n: x \mapsto \frac{2 x}{x^2+n^2}$. On pose $S=\sum_{n=1}^{+\infty} f_n$.  
        1. Étudier la converge simple de la série de fonctions $\sum f_n$.  
        2. Étudier la continuité de $S$ sur son ensemble de définition.   
        3. Déterminer les limites de $S$ en $+\infty$ et $-\infty$.


!!! ExerciceTD "4 (BEOS 7849)"
    === "Énoncé"
        Pour $n \in \mathbb{N}$ et $x \in \mathbb{R}$, on pose $v_n(x)=n^x \mathrm{e}^{-n x}$.
        Soit $S: x \longmapsto \sum_{n=0}^{+\infty} v_n(x)$.  
        1) Donner l'ensemble de définition de $S$.  
        2) Montrer que $S$ est continue sur son ensemble de définition.  
        3) Donner la limite de $S$ en $+\infty$ par le théorème de la double limite.  
        4) $\sum_{n \geqslant 0} v_n$ converge-t-elle uniformément sur $] 0,+\infty[$ ?  
        5) $S$ est-elle dérivable sur $] 0,+\infty[$ ?


!!! ExerciceTD "5 (BEOS 7006)"
    === "Énoncé"
        Pour $x \in \mathbb{R}$ et $n \in \mathbb{N}^{\star}$, on pose $u_n(x)=(-1)^n \frac{\mathrm{e}^{-n x}}{n}$. On note $f$ la somme de cette série de fonctions.  
        1. Montrer que $f$ est définie sur $\mathbb{R}_{+}$.  
        2. Montrer la convergence uniforme de $\sum u_n$ sur $\mathbb{R}_{+}$et en déduire que $f$ est continue sur $\mathbb{R}_{+}$.  
        3. Y a-t-il convergence normale sur $\mathbb{R}_{+}$ ?  
        4. Montrer que $f$ est de classe $\mathcal{C}^1$ sur $\mathbb{R}_{+}^{\star}$ et en déduire, pour $x>0$, une expression explicite de $f^{\prime}(x)$.  
        5. Déterminer $f$ puis en déduire la valeur de $\sum_{n=1}^{+\infty} \frac{(-1)^n}{n}$.  


!!! ExerciceTD "6 (BEOS 6777)"
    === "Énoncé"
        Soit $f : x \longmapsto \sum_{n=1}^{+\infty} \frac{\cos (n x)}{x^2+n^2}$  
        1) Montrer que $f$ est bien définie sur $\mathbb{R}$  
        2) Montrer que $f$ est $\mathcal{C}^1$ sur $\mathbb{R}$  




!!! ExerciceTD "7 (BEOS 7806)"
    === "Énoncé"
        Pour $n \in \mathbb{N}^*$, on définit de $\mathbb R^+$ dans $\mathbb R^+$ par

        $$
        u_n(x)=\frac{x}{\sqrt{n}\left(1+n x^2\right)}
        $$

        1. Montrer que $\sum u_n$ converge simplement sur $\mathbb{R}_{+}$.  
        - On pose $S=\sum_{n=1}^{+\infty} u_n$.  
        2. a) La convergence est-elle normale sur $\mathbb{R}_{+}$ ?  
        b) Montrer que, pour tout $a>0$, la série converge normalement sur $[a,+\infty[$. En déduire que $S$ est continue sur $\mathbb{R}_{+}^*$.  
        3. a) Montrer que : $\forall n \in \mathbb{N}^*, R_n(x)-R_{2 n}(x) \geqslant \frac{\sqrt{n} x}{\sqrt{2}\left(1+2 n x^2\right)}$.  
        b) En déduire que $\sum u_n$ ne converge pas uniformément sur $\mathbb{R}_{+}$.  
        4. On admet $(\varepsilon)$ :
        $$
        \forall x>0 \quad 2 \arctan \frac{1}{x} \leqslant S(x) \leqslant 2 \arctan \frac{1}{x}+\frac{x}{1+x^2}
        $$
        Montrer que $S$ n'est pas continue en 0.  
        5. Démontrer $(\varepsilon)$.


!!! ExerciceTD "8 (BEOS 7633)" 
    === "Énoncé"
        On définit la suite $\left(u_n\right)_{n \in \mathbb{N}}$ par $u_0=\frac{\pi}{2}$ et $\forall n \in \mathbb{N}, u_{n+1}=\sin \left(u_n\right)$.  
        1. Montrer que la suite $\left(u_n\right)$ converge vers 0.  
        2. Montrer que $\sum \ln \left(\frac{u_{n+1}}{u_n}\right)$ et $\sum u_n^2$ sont de même nature. En déduire la nature de la série $\sum u_n^2$.  
        3. Montrer que $\sum\left(u_{n+1}-u_n\right)$ et $\sum u_n^3$ sont de même nature. En déduire la nature de la série $\sum u_n^3$.  
        4. En déduire la nature de la série $\sum u_n^k$ pour tout $k \in \mathbb{N}^*$.


!!! ExerciceTD "9 (RMS23 1139)" 
    === "Énoncé"
        Soit $f: x \mapsto \sum_{n=1}^{+\infty} \frac{1}{n+n^2 x^2}$.  
        a) Déterminer le domaine de définition et de continuité de $f$.  
        b) Déterminer la limite de $f$ et un équivalent en $+\infty$.  
        c) Déterminer la limite de $f$ et un équivalent en 0 .  



!!! ExerciceTD "10 (RMS23 1142)" 
    === "Énoncé"
        Soit $a>0$.  
        a) Déterminer l'ensemble de définition de la fonction $f: x \mapsto \sum_{n=1}^{+\infty} \ln \left(1+\frac{a^2}{n^2 x^2}\right)$.  
        b) Étudier la dérivabilité de $f$.  
        c) Donner la limite puis un équivalent de $f(x)$ quand $x \rightarrow+\infty$.  
        d) Donner la limite puis un équivalent de $f(x)$ quand $x \rightarrow 0^{+}$.
