<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD S10 Espaces vectoriel normés



!!! ExerciceTD "1 BEOS-6984"
    === "Énoncé"
        Pour tout polynôme réel $P$ et tout entier $n$, on pose $a_n(P)=\int_0^1 P(t) t^n \mathrm{~d} t$.
        Pour tout polynôme réel $P$, on pose

        $$
        \|P\|_{\infty}=\sup _{t \in[0,1]}|P(t)|, \quad N_{\infty}(P)=\sup _{n \in \mathbb{N}}\left|a_n(P)\right|, \quad N_2(P)=\sqrt{\sum_{n=0}^{+\infty} a_n(P)^2} .
        $$

        1. Justifier que ces quantités sont bien définies.  
        2. Montrer que $\|\cdot\|_{\infty}, N_{\infty}$ et $N_2$ sont des normes sur $\mathbb{R}[X]$.  
        3. Trouver des constantes $\alpha$ et $\beta$ strictement positives telles que

        $$
        \forall P \in \mathbb{R}[X], \quad N_1(P) \leqslant \alpha N_2(P) \quad \text { et } \quad N_2(P) \leqslant \beta\|P\|_{\infty} .
        $$

        4. Montrer que ces normes ne sont pas équivalentes entre elles.

!!! ExerciceTD "2 BEOS-4899"
    === "Énoncé"
        Soit $A \in \mathfrak{M}_n(\mathbb{C})$.
        On note $\|A\|=\max _{i \in[1, n]}\left(\sum_{j=1}^n\left|a_{i, j}\right|\right)$ et $\rho(A)=\max \{|\lambda|, \lambda$ valeur propre de $A\}$.  
        1. Déterminer la norme de $A$ et $\rho(A)$ lorsque $A=\left(\begin{array}{cc}1 & 1+i \\ 0 & \mathrm{e}^{i \theta}\end{array}\right)$  
        2. Montrer que $\|A B\| \leqslant\|A\| \cdot\|B\|$ pour $A, B \in \mathfrak{M}_n(\mathbb{C})$.  
        3. a) Soit $x$ un vecteur propre associé à la valeur propre $\lambda$. Montrer que  

        $$
        \left|\lambda x_i\right| \leqslant \sum_{j=1}^n\left|a_{i, j} x_j\right|
        $$

        pour tout $i \in \llbracket 1, n \rrbracket$.  
        b) En déduire que $\rho(A) \leqslant\|A\|$.  
        4. Montrer que la suite $\left(A^k\right)$ converge vers la matrice nulle dans $\mathfrak{M}_n(\mathbb{C})$ si et seulement si $\rho(A)<1$.   
        5. Montrer que $\rho(A)^k=\rho\left(A^k\right)$ pour tout $k \geqslant 1$.  

!!! ExerciceTD "3 RMS23-1076"
    === "Énoncé"
        Soient $p, q \in] 1,+\infty\left[\right.$ tels que $\frac{1}{p}+\frac{1}{q}=1$. Pour $x=\left(x_1, \ldots, x_n\right)$ et $y=\left(y_1, \ldots, y_n\right)$ dans $\mathbb{R}^n$ on pose $\|x\|_p=\left(\sum_{k=1}^n\left|x_k\right|^p\right)^{1 / p},\|y\|_p=\left(\sum_{k=1}^n\left|y_k\right|^q\right)^{1 / q}$ et $\langle x, y\rangle=\sum_{k=1}^p x_k y_k$.

        a) On admet que $\forall(x, y) \in\left(\mathbb{R}^n\right)^2,|\langle x, y\rangle| \leqslant\|x\|_p\|y\|_q$.

        Montrer que $\|x\|_p=\sup \left\{\langle x, y\rangle ; y \in \mathbb{R}^n,\|y\|_q=1\right\}$.  
        b) Montrer que $\left\|\|_p\right.$ est une norme.  
        c) Si \|\| est une norme euclidienne sur un $\mathbb{R}$-espace vectoriel $E$, montrer que, pour tout $(x, y) \in E^2,\|x+y\|^2+\|x-y\|^2=2\left(\|x\|^2+\|y\|^2\right)$.  
        d) Trouver une inclusion entre les boules unités de $\left(\mathbb{R}^n,\| \|_p\right)$ et $\left(\mathbb{R}^n,\| \|_{p^{\prime}}\right)$ avec $1 \leqslant p<p^{\prime}$.


!!! ExerciceTD "4 RMS23-1077"
    === "Énoncé" 

        Pour $A, B \in \mathcal{M}_n(\mathbb{R})$, on pose $\langle A, B\rangle=\operatorname{Tr}\left(A^T B\right)$. Soit $N$ la norme euclidienne associée à ce produit scalaire.  
        a) Pour $A \in \mathcal{M}_n(\mathbb{R})$, calculer $N(A)$ avec les coefficients de $A$.  
        b) Montrer que $\forall A \in \mathcal{M}_n(\mathbb{R}),|\operatorname{tr}(A)| \leqslant \sqrt{n} N(A)$ et déterminer les cas d'égalité.   
        c) Pour $A \in \mathcal{M}_n(\mathbb{R})$ et $U \in \mathcal{O}_n(\mathbb{R})$, montrer que $N(A U)=N(U A)=N(A)$.  
        d) Pour $A, B \in \mathcal{M}_n(\mathbb{R})$, montrer que $N(A B) \leqslant N(A) N(B)$.  
        e) Soit $M \in \mathcal{M}_n(\mathbb{R})$. Déterminer $m=\inf \left\{\sum_{i, j}\left(m_{i, j}-s_{i, j}\right)^2, S \in S_n(\mathbb{R})\right\}$.  
        f) On pose, pour $A \in \mathcal{M}_n(\mathbb{R}),\|A\|_1=\sum_{i, j}\left|a_{i j}\right|$. Trouver les meilleures constantes $a$ et $b$ telles que $\forall A \in \mathcal{M}_n(\mathbb{R}), a N(A) \leqslant\|A\|_1 \leqslant b N(A)$.

!!! ExerciceTD "5 BEOS-7377"
    === "Énoncé"
        Soit $E=\mathcal{C}^0([0,1], \mathbb{R})$. Si $f \in E$, on note $\varphi(f): x \mapsto \int_0^x t f(t) \mathrm{d} t$.  
        1. Montrer que $\varphi$ est un endomorphisme de $E$.  
        2. Trouver le plus petit $k>0$ tel que : $\forall f \in E,\|\varphi(f)\|_{\infty} \leqslant k\|f\|_{\infty}$  
        3. Trouver le plus petit $k>0$ tel que : $\forall f \in E,\|\varphi(f)\|_{\infty} \leqslant k\|f\|_1$.



!!! ExerciceTD "6 BEOS-6708"
    === "Énoncé"
        Pour toute matrice $M$ de $\mathcal{M}_d(\mathbb{C})$, on pose $\|M\|_{\infty}=\max \left\{\left|m_{i, j}\right| ; 1 \leqslant i, j \leqslant d\right\}$.  
        1. On pose $A=\left(\begin{array}{ccc}1 & 2 & 3 \\ 0 & 1 & -1 \\ 0 & 0 & 1\end{array}\right)$. Cette matrice est-elle diagonalisable?  
        2. On pose $N=A-I_3$. Calculer $N^2$, puis les autres puissances de $N$.  
        3. Déterminer la limite de $\left\|A^n\right\|_{\infty}$ quand $n$ tend vers $+\infty$.  
        4. Vérifier que $\|\cdot\|_{\infty}$ est une norme sur $\mathcal{M}_d(\mathbb{C})$.  
        5. Pour tout couple $(M, N)$ de matrices de $\mathcal{M}_d(\mathbb{C})$, prouver la majoration

        $$
        \|M N\|_{\infty} \leqslant d \times\|M\|_{\infty} \times\|N\|_{\infty}
        $$

        6. On suppose que $M$ est diagonalisable et possède au moins une valeur propre de module strictement supérieur à 1 .  

        Déterminer la limite de $\left\|M^n\right\|_{\infty}$ quand $n$ tend vers $+\infty$.




!!! ExerciceTD "7 BEOS-7124"
    === "Énoncé"
        Soit $E=\mathcal{C}^1([0,1], \mathbb{R})$
        On pose pour tout $f \in E:\|f\|_1=\int_0^1|f(x)| \mathrm{d} x$ et:

        $$
        N_1(f)=\|f\|_1+\left\|f^{\prime}\right\|_1 \text { et } N_2(f)=|f(0)|+\left\|f^{\prime}\right\|_1 \text {. }
        $$

        1. Montrer que $N_1$ et $N_2$ sont des normes.  
        2. $N_1$ et $N_2$ sont-elles équivalentes ?


!!! ExerciceTD "8 RMS23-1045"
    === "Énoncé"

        Soit $E$ l'espace des suites complexes bornées.

        Pour $u=\left(u_n\right)_{n \in \mathbb{N}} \in E$ on pose $N_1(u)=\sum_{n=0}^{+\infty} \frac{\left|u_n\right|}{2^n}$ et $N_2(u)=\sum_{n=0}^{+\infty} \frac{\left|u_n\right|}{n!}$.  
        a) Montrer que $N_1$ et $N_2$ sont bien définis.  
        b) Montrer que $N_1$ et $N_2$ sont des normes sur $E$.  
        c) Montrer qu'il existe une constante $C$ telle que $N_2 \leqslant C N_1$.  
        d) Montrer qu'il n'existe pas de constante positive $D$ telle que $N_1 \leqslant D N_2$.




!!! ExerciceTD "9 RMS23-232"
    === "Énoncé"
        Soit $E$ l'espace des fonctions $f:[0 ; 1] \rightarrow \mathbb{R}$ de classe $\mathcal{C}^1$ telles que $f(0)=0$. Pour $f \in E$, on pose $\|f\|=\left\|f+f^{\prime}\right\|_{\infty}$.  
        a) Montrer que $\|\|$ est une norme sur $E$.  
        b) Montrer qu'il existe $a>0$ tel que, pour tout $f \in E$, on ait $\|f\|_{\infty} \leqslant a\|f\|$.  
        c) Les normes $\|\|$ et $\| \|_{\infty}$ sont-elles équivalentes sur $E$ ?


!!! ExerciceTD "10 RMS23-1458"
    === "Énoncé" 
        On note $E$ l'ensemble des fonctions $f \in \mathcal{C}^1([0,1], \mathbb{R})$ telles que $f(0)=0$. Pour $f \in E$, on pose $N(f)=\left\|f+f^{\prime}\right\|_{\infty}$ et $N^{\prime}(f)=\|f\|_{\infty}+\left\|f^{\prime}\right\|_{\infty}$.  
        a) Montrer que $N$ et $N^{\prime}$ sont des normes sur $E$.  
        b) Montrer que $N$ et $N^{\prime}$ sont équivalentes. Ind. Exprimer $f$ en fonction de $g=f+f^{\prime}$.


