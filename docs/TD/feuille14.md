<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>


# TD 14 Probas de spé 2


!!! ExerciceTD "1 - BEOS4773"
    === "Énoncé"
        $X$ est une variable aléatoire qui suit une loi de Bernoulli de paramètre $p, Y$ suit une loi de Poisson de paramètre $\lambda$. $X$ et $Y$ sont indépendantes. On pose $Z=X Y$.  
        a) Déterminer la loi de $Z$.  
        b) Déterminer $G_Z(t)$.  
        c) Déterminer espérance et variance de $Z$.  
        

!!! ExerciceTD "2 - BEOS4776"
    === "Énoncé"
        $X$ et $Y$ sont deux variables aléatoires indépendantes, de même loi.
        $Z=X+Y+1$. On suppose que $Z \hookrightarrow \mathscr{G}(p)$ où $p \in] 0,1[$.  
        a) Montrer que $X$ admet une espérance et une variance et les calculer.  
        b) Trouver $G_X: t \mapsto \mathbb{E}\left(t^X\right)$.  
        c) En déduire la loi de $X$.  
        
!!! ExerciceTD "3 - BEOS8322"
    === "Énoncé"
        On effectue des lancers successifs d'une pièce équilibrée. On convient qu'on gagne 1 point à lissue d'un lancer si celui-ci est différent du précédent, 0 point, sinon. Soit $n \in \mathbb{N}^{\star}$. On note $X_n$ la variable aléatoire correspondant au gain total au bout de $n$ lancers.
        
        1. Déterminer $E\left(X_2\right)$ et $E\left(X_3\right)$.  
        2. Pour $n \geq 2$, déterminer $\mathbb{P}\left(X_n=0\right)$ et $\mathbb{P}\left(X_n=n-1\right)$.  
        3. Pour $k \in \mathbb{N}$ convenable, exprimer $\mathbb{P}\left(X_{n+1}=k\right)$ en fonctions de $\mathbb{P}\left(X_n=k\right)$ et $\mathbb{P}\left(X_n=k-1\right)$.  
        4. Pour $x \in \mathbb{R}$, on note $Q_n(x)=\sum_{k=0}^{n-1} \mathbb{P}\left(X_n=k\right) x^k$. Montrer que $Q_{n+1}(x)=\left(\frac{1+x}{2}\right) Q_n(x)$.  
        5. En déduire une expression de $Q_n(x)$, puis la loi de $X_n$.  
        
        
!!! ExerciceTD "4 - BEOS8505"
    === "Énoncé"       
        On dispose d'une urne contenant $n$ boules blanches et $n$ boules rouges. On effectue des tirages selon la règle suivante :
        - si l'on pioche une boule rouge, on la remet dans l'urne ;
        - si l'on pioche une boule blanche, on la met de côté et on rajoute une boule rouge dans l'urne.

        On note la variable aléatoire $X_p$ qui désigne le nombre de boules blanches dans l'urne au $p$-ième tirage, pour tout $p$ entier naturel non nul.  
        1. Donner la loi de $X_1$ et $X_2$.  
        2. Pour tout $k$ entier naturel, calculer $P\left(X_p=k\right)$.  
        3. Donner une relation entre $P\left(X_{p+1}=k\right), P\left(X_p=k+1\right), P\left(X_p=k\right)$.  
        4. Justifier que la fonction génératrice $G_p$ est polynomiale.  
        5. On admet la relation $G_{p+1}(t)=G_p(t)+\frac{1-t}{2 n} G_p^{\prime}(t)$.

        Donner une relation entre $E\left(X_{p+1}\right)$ et $E\left(X_p\right)$. Calculer $E\left(X_p\right)$.
        Calculer sa limite lorsque $t$ tend vers $+\infty$. Interpréter.
        
!!! ExerciceTD "5 - BEOS7517"
    === "Énoncé"       
        Soit $n \in \mathbb{N}^*, X$ suit une loi géométrique de paramètre $1 / n$.
        Montrer que :

        $$
        P\left(X \geqslant n^2\right) \leqslant 1 / n ; P(|X-n| \geqslant n) \leqslant 1-1 / n ; P(X \geqslant 2 n) \leqslant 1-1 / n
        $$




!!! ExerciceTD "6 - RMS23-984"
    === "Énoncé"       
        Soit $X$ une variable aléatoire de loi $\mathcal{P}(\lambda)$.  
        a) Montrer que, pour tout $n \in \mathbb{N}, \mathbf{P}(X \leqslant n)=\frac{1}{n!} \int_\lambda^{+\infty} t^n e^{-t} \mathrm{~d} t$.  
        b) Donner un équivalent de $\int_\lambda^{+\infty} t^n e^{-t} \mathrm{~d} t$ quand $n \rightarrow \infty$.  
        c) Grâce à la fonction génératrice, calculer la probabilité que $X$ soit pair.  
        d) Soit $Y$ une variable aléatoire suivant la loi uniforme sur $\{1,2\}$, indépendante de $X$. Calculer la probabilité que $X Y$ soit paire.  


!!! ExerciceTD "7 - RMS23-1208"
    === "Énoncé"    
        Soit $\alpha>0$.
        a) Montrer l'existence d'une variable aléatoire $X$ valeurs dans $\mathbb{N}$ de fonction génératrice

        $$
        G_X(t)=\frac{1}{(2-t)^\alpha}
        $$

        b) Donner un équivalent de $\mathbf{P}(X=n)$ quand $n \rightarrow+\infty$.
        c) Pour $\lambda>0$, montrer que $\mathbf{P}(X \geqslant \lambda+\alpha) \leqslant \frac{2 \alpha}{\lambda^2}$.
        
        
!!! ExerciceTD "8 - RMS23-1607"
    === "Énoncé"    
        Soit $X$ suivant une loi de Poisson de paramètre $\lambda$.  
        a) Expliciter $G_X(t)$.  
        b) Montrer que, pour $t \geqslant 1, \mathbf{P}(X \geqslant 2 \lambda) \leqslant \frac{G_X(t)}{t^{2 \lambda}}$.  
        c) Montrer que $\mathbf{P}(X \geqslant 2 \lambda) \leqslant\left(\frac{e}{4}\right)^\lambda$.  
        d) Comparer avec l'inégalité de Bienaymé-Tchebychev.  
        
!!! ExerciceTD "9 - RMS23-886"
    === "Énoncé"    
        886. On considère une urne qui contient une proportion $p \in] 0,1[$ de boules blanches. On effectue un tirage avec remise des boules. Soit $X_n$ la variable donnant le nombre de tirages successifs nécessaires pour obtenir $n$ boules blanches. Donner la loi de $X_1$ ainsi que sa fonction génératrice $\mathcal{G}_{X_1}$. En déduire $\mathcal{G}_{X_n}$. Loi et espérance de $X_n$ ?

!!! ExerciceTD "10 - RMS23-1603"
    === "Énoncé"    
        Soit $n \in \mathbb{N}^*$. On considère un graphe aléatoire non orienté à $n$ sommets notés $A_1, \ldots, A_n$. La probabilité que les sommets $A_i$ et $A_j$, pour $i \neq j$, soient reliés est égale à $\left.p_n \in\right] 0,1\left[\right.$, et cela de façon indépendante. On note $X_i$ la variable aléatoire égale à 1 si le sommet $A_i$ est isolé, c'est-à-dire s'il n'est relié à aucun autre sommet. On pose $S_n=$ $X_1+\cdots+X_n$.  
        a) Donner la loi de $X_1$. En déduire $\mathbf{E}\left(S_n\right)$.  
        b) Donner une majoration de la probabilité d'avoir au moins un sommet isolé.  
        c) On suppose que, pour $n \geqslant 2, p_n=C \frac{\ln (n)}{n}$ avec $C>1$. Montrer : $\mathbf{P}\left(S_n=0\right) \underset{n \rightarrow+\infty}{\longrightarrow} 0$.