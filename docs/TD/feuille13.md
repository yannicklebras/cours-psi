<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>


# TD 13 Probas de spé

!!! ExerciceTD "BEOS8461"
    === "Énoncé"
        On dispose initialement d'une urne constituée d'une boule blanche, et d'une pièce de monnaie équilibrée. On effectue des lancers de la pièce selon la règle suivante :  
        - si on obtient "Face" on ajoute une boule noire dans l'urne ;  
        - si on obtient "Pile" on tire une boule dans l'urne et on arrête l'expérience.

        Soit $X$ la variable aléatoire donnant le nombre de lancers de la pièce.  
        1) Quelle est la loi de $X$ ?  
        2) Quelle est la probabilité d'obtenir une boule blanche à la fin de l'expérience?  



!!! ExerciceTD "BEOS7641"
    === "Énoncé"

        Soit une urne avec 3 jetons numérotés. On tire avec remise des jetons de l'urne.  
        On note $Y$ la variable aléatoire qui compte le nombre de tirages nécessaire pour obtenir 2 jetons différents pour la première fois.  
        On note $Z$ la variable aléatoire qui compte le nombre de tirages nécessaires pour obtenir les 3 jetons pour la première fois  
        1. Donner la loi de $Y$.  
        2. Reconnaitre la loi de $Y-1$. En déduire la variance et l'espérance de $Y$.  
        3. Déterminer la loi de $(Y, Z)$.   
        4. Donner enfin la loi de $Z$.


!!! ExerciceTD "BEOS6419"
    === "Énoncé"
        Soit $X$ une variable aléatoire à valeurs dans $\mathbb{N}^*$, de loi donnée par: $\forall k \in \mathbb{N}^*, P(X=k)=p(1-p)^{k-1}$, où $\left.p \in\right] 0,1[$. On pose $Y=(-1)^X$.  
        1. Déterminer la loi de $Y$.  
        2. Calculer $E(Y)$ et $E(X Y)$.



!!! ExerciceTD "BEOS5894"
    === "Énoncé"
        $X$ et $Y$ sont deux variables aléatoires indépendantes qui suivent une loi de Poisson de même paramètre $\lambda>0$.  
        a) Rappeler l'expression de la loi de $X$, donner son espérance et sa variance.  
        b) Les variables aléatoires $U=\operatorname{Min}(X, Y)$ et $V=\operatorname{Max}(X, Y)$ sont-elles indépendantes?  
        c) Soit $Z$ variable aléatoire. On suppose que pour tout $k \in \mathbb{N}$ la loi de probabilité de $Z$ sachant $(X=k)$ suit une loi binomiale de paramètre $(k, p)$ où $p \in] 0,1[$. Préciser la loi de $Z$.



!!! ExerciceTD "BEOS7332"
    === "Énoncé"
        Soit une variable aléatoire $X \hookrightarrow P(\lambda)$.  
        Soit la variable aléatoire $Y$ définie par $Y=0$ si $X$ est impair et $Y=\frac{X}{2}$ si $X$ est pair.  
        1. Rappeler les développements en série entière de cosinus et sinus hyperbolique.  
        2. Déterminer la loi de $Y$.  
        3. Calculer l'espérance de $Y^2$.




!!! ExerciceTD "RMS1193"
    === "Énoncé"
        On allume une ampoule. On note $X$ le temps de vie de cette ampoule (en jours). On suppose que pour tout $n \in \mathbb{N}^*, \mathbf{P}(X=n)=\frac{1}{2^n}$. Soit $n \in \mathbb{N}^*$. Sachant que l'ampoule est allumée le jour $n$, calculer le nombre moyen de jours où cette ampoule restera allumée.




!!! ExerciceTD "RMS1195"
    === "Énoncé"
        Soient $X$ et $Y$ deux variables aléatoires indépendantes suivant des lois géométriques de paramètres respectifs $p$ et $q$. Probabilité pour que la matrice $\left(\begin{array}{cc}X & 1 \\ 0 & Y\end{array}\right)$ soit diagonalisable?



!!! ExerciceTD "RMS1200"
    === "Énoncé"
        Soit $M=\left(X_{i, j}\right)_{1 \leqslant i \leqslant n, 1 \leqslant j \leqslant n}$ une matrice aléatoire réelle où les $\left(1+X_{i j}\right)$ sont i.i.d. de loi $\mathcal{G}(p)$, où $p \in[0,1[$.  
        a) Calculer la probabilité pour que $M$ soit symétrique.  
        b) Calculer la probabilité pour que $M$ soit orthogonale.  



!!! ExerciceTD "RMS1377"
    === "Énoncé"
        Soient $X$ et $Y$ deux variables aléatoires indépendantes suivant la loi de Poisson de paramètre $\lambda$. On pose $M=\left(\begin{array}{ll}(-1)^X & 1 \\ (-1)^Y & 1\end{array}\right)$.  
        a) Calculer la probabilité que la matrice $M$ soit inversible.  
        b) Calculer la probabilité que la matrice $M$ soit diagonalisable sur $\mathbb{R}$, puis sur $\mathbb{C}$.

!!! ExerciceTD "RMS1602"
    === "Énoncé"
        Soient $X$ et $Y$ deux variables aléatoires indépendantes suivant la loi géométrique $\mathcal{G}(p)$ où $p \in] 0,1\left[\right.$. On pose $M=\left(\begin{array}{ll}X & Y \\ Y & X\end{array}\right)$. On note $I$ et $S$ les valeurs propres de $M$ avec $I \leqslant S$.  
        a) Donner les expressions de $I$ et $S$.  
        b) Quel est la probabilité que $M$ soit inversible?  
        c) Calculer la covariance de $I$ et de $S$. Les variables $I$ et $S$ sont elles indépendantes?"