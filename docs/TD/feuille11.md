<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>



# TD S11 Espaces normés et applications

!!! ExerciceTD "1 BEOS ?"
    === "Énoncé"

        Soit $E=\mathbb{C}[X], P=\sum_{k \in \mathbb{N}} a_k X^k \in E$. On définit la norme ॥. ॥ par: $\forall P \in E,\|P\|=\sup _{k \in \mathbb{N}}\left|a_k\right|$. Soit $b \in \mathbb{N}$, on définit l'application $f: \left\lvert\, \begin{array}{lll}E & \longrightarrow & \mathbb{C} \\ P & \longmapsto & P(b)\end{array}\right.$     
        1. Montrer que $f$ est linéaire.     
        2. Etudier la continuité de f.  


!!! ExerciceTD "2 BEOS ?"
    === "Énoncé"
        Soit $a \in \mathbb{R}$ et $A(a)=\left(\begin{array}{cccc}0 & -1 & \ldots & -1 \\ a & \ddots & \ddots & \vdots \\ \vdots & \ddots & \ddots & -1 \\ a & \ldots & a & 0\end{array}\right)$ et $U=((1))$ (la matrice constituée de 1 ), deux éléments de $\mathcal{M}_n(\mathbb{R})$.  

        1. Calculer $\operatorname{det}(A(-1))$.  
        3. On note $P(x)=\operatorname{det}(A(a)+x U)$. Montrer que $P$ est polynomial de degré inférieur ou égal à 1 .  
        5. Calculer $P(-a)$ et $P(1)$. En déduire $\operatorname{det}(A(a))$.  
        7. Question supplémentaire : Étudier la continuité de $a \mapsto \operatorname{det}(A(a))$ et retrouver la valeur $\operatorname{de} \operatorname{det}(A(-1))$.


!!! ExerciceTD "3 RMS1079"
    === "Énoncé"
        a) Soit $A \in \mathcal{M}_2(\mathbb{C})$. Montrer que $\{Q(A), Q \in \mathbb{C}[X]\}$ est un fermé.  
        b) Soient $Q \in \mathbb{C}[X]$ non constant et $B \in \mathcal{M}_2(\mathbb{C})$ diagonalisable. Montrer qu'il existe $A \in \mathcal{M}_2(\mathbb{C})$ tel que $B=Q(A)$.  
        c) Soit $Q \in \mathbb{C}[X]$ non constant. Montrer que $\left\{Q(A), A \in \mathcal{M}_2(\mathbb{C})\right\}$ est dense dans $\mathcal{M}_2(\mathbb{C})$.



!!! ExerciceTD "4 RMS1081"
    === "Énoncé" 
        Soit $f$ définie sur $E=\mathcal{S}_2(\mathbb{R})$ par $f: M \in E \mapsto\left(\lambda_1, \lambda_2\right)$ où $\lambda_1$ et $\lambda_2$ sont les valeurs propres de $M$ avec $\lambda_1 \geqslant \lambda_2$. Étudier la continuité de $f$.



!!! ExerciceTD "5 BEOS8343"
    === "Énoncé"
        Soit $\Omega=\left\{A \in \mathfrak{M}_n(\mathbb{C}):\left(I_n, A, \ldots, A^{n-1}\right)\right.$ soit libre $\}$.  
        1. Montrer que $\Omega$ est un ouvert de $\in \mathfrak{M}_n(\mathbb{C})$.  
        2. Montrer que $\Omega$ est convexe.

!!! ExerciceTD "6 RMS462"
    === "Énoncé"
        On pose : $E=\mathcal{M}_n(\mathbb{R})$ et $F=\left\{P \in E, P=P^T=P^2\right\}$. Soit $(P, Q) \in F^2$. Donner une condition nécessaire et suffisante pour qu'il existe $f:[0,1] \rightarrow F$ continue telle que $f(0)=P$ et $f(1)=Q$.


!!! ExerciceTD "7 RMS1291"
    === "Énoncé"
        Soit $n \in \mathbb{N}^*$. On munit $\mathbb{R}^n$ de sa structure euclidienne canonique. Soit $K \in \mathbb{R}^n$ un convexe fermé non vide. Soit $x \notin K$. On pose $f: z \in K \mapsto\|z-x\|$.  
        a) Montrer que, si $a, b \in K,\left\|x-\frac{a+b}{2}\right\|^2+\frac{1}{4}\|a-b\|^2=\frac{1}{2}\left(\|x-a\|^2+\|x-b\|^2\right)$  
        b) Montrer que $f$ est continue.  
        c) Soient $z_0 \in K$ et $B_0$ la boule fermée de centre $z_0$ et de rayon $\left\|z_0-x\right\|, K_0=K \cap B_0$.   
        Montrer que $f$ admet un minimum sur $K_0$. On note $\widetilde{z}$ tel que $f(\widetilde{z})=\min _{z \in K_0} f(z)$   
        d) Montrer que $\forall z \in K,\|z-x\| \geqslant\|\widetilde{z}-x\|$   
        e) En déduire que $f$ admet un minimum sur $K$ atteint en un unique point.





!!! ExerciceTD "8 BEOS1331"
    === "Énoncé"
        Soit E l'espace des suites à valeurs réelles convergeant vers 0 , on définit $\| .| |$ une application qui à $u \in E$ associe sup ${ }_{n \in \mathbb{N}|u n|}$  
        1) a)Montrer que E est un sous-espace vectoriel de l'espace des suites à valeurs réelles  
        b) Montrer que II.|| est bien une norme sur E  
        2) Soit $\mathrm{u} \in E$, montrer que $\sum \frac{u_n}{2^{n+1}}$ converge  
        3) Soit f , qui à $\mathrm{u} \in E$ associe $\sum_{n=0}^{\infty} \frac{u_n}{2^{n+1}}$, montrer que f est continue



!!! ExerciceTD "9 BEOS8079"
    === "Énoncé"
        Soit $n \in \mathbb{N}^*$.  
        1.a) Rappeler la définition de $\left\|\left|\|| |\right.\right.$ sur $\mathcal{M}_n(\mathbb{C})$.  
        b) On note $\rho(A)=\max _{\lambda \in \operatorname{Sp}(A)}|\lambda|$. L'application $A \mapsto \rho(A)$ est-elle une norme ?  
        2. Montrer que $\forall A \in \mathcal{M}_n(\mathbb{C}), \forall k \in \mathbb{N}^*, \rho(A) \leqslant\left\|A^k\right\|^{1 / k}$.  
        3. Soit $N$ une norme quelconque sur $\mathcal{M}_n(\mathbb{C})$.  
        Montrer que $\rho(A)=\lim _{k \rightarrow+\infty}\left[N\left(A^k\right)\right]^{\frac{1}{k}}$.




!!! ExerciceTD "10 BEOS6838"
    === "Énoncé"
        1. Donner la norme euclidienne habituelle sur $\mathbb{R}^n$. Existe-t-il d'autres normes euclidiennes sur $\mathbb{R}^n$ ?
        Dans la suite, la norme euclidienne habituelle sur $\mathbb{R}^n$ est notée $\|\|$. La sphère unité correspondante est notée $S$.  
        2. Soit $f \in \mathcal{L}\left(\mathbb{R}^n\right)$. Montrer que la fonction $x \mapsto\|f(x)\|$ admet un maximum sur $S$. Ce maximum est noté $\|f\|$.  
        3. Pour tout couple $(f, g)$ d'éléments de $\mathcal{L}\left(\mathbb{R}^n\right)$, prouver l'inégalité $\|f \circ g\| \leqslant\|f\| \times\|g\|$.  
        4. Soit $f \in \mathcal{L}\left(\mathbb{R}^n\right)$ tel que $\|f\|<1$. Montrer que la série $\sum_{k \geqslant 0} f^k$ est convergente et que sa somme est un automorphisme de $\mathbb{R}^n$.

!!! ExerciceTD "11 BEOS2084 "
    === "Énoncé"
        On considère une matrice $A$ symétrique réelle.
        On note $p(A)=\max _{\lambda \in \operatorname{Sp}(A)}|\lambda|$.  
        1) Prouver que $\forall k \in \mathbb{N}^*, p\left(A^k\right)=p(A)^k$.  
        2) Montrer que $A \mapsto p(A)$ définit une norme sur $\mathcal{S}_n(\mathbb{R})$.  
        3) Soient $A, B \in \mathcal{S}_n(\mathbb{R})$ telles que $A B=B A$,  
        a) Montrer que $A B \in \mathcal{S}_n(\mathbb{R})$.  
        b) Montrer que $p(A B) \leqslant p(A) \times p(B)$.  
        4) Soit ||. || une norme vérifiant:

        $$
        \forall A, B \in \mathcal{S}_n(\mathbb{R}), A B=B A \Longrightarrow\|A B\| \leqslant\|A\| \times\|B\|
        $$


        Montrer que $\forall A \in \mathcal{S}_n(\mathbb{R}),\|A\| \geqslant p(A)$.
        (autrement dit, $p$ est la plus petite norme vérifiant la propriété (1)).