<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD S07 Réduction des endomorphismes

!!! ExerciceTD "1 BEOS 8381"
    === "Énoncé"
        Soit $u \in \mathcal{L}\left(\mathbb{C}^n\right)$ où $n \in \mathbb{N}^*$.  
        1. Montrer que si $u$ est diagonalisable dans $\mathbb{C}$, alors $u^2$ est diagonalisable dans $\mathbb{C}$.  
        2. Montrer par un contre-exemple que la réciproque est fausse.  
        3. Montrer que si $\lambda \in \mathbb{C}^*$, alors :  

        $$
        \operatorname{Ker}\left(u^2-\lambda^2 \mathrm{Id}\right)=\operatorname{Ker}(u-\lambda \mathrm{Id}) \oplus \operatorname{Ker}(u+\lambda \mathrm{Id})
        $$

        4. Montrer que si $u$ est bijective, la réciproque de 1 est vraie.  
        

!!! ExerciceTD "2 BEOS 7534"
    === "Énoncé"
        Soit $A \in \mathcal{M}_n(\mathbb{R})$ une matrice de rang 1 . Montrer que $A$ est diagonalisable si et seulement si $\operatorname{tr}(A) \neq 0$.
        
        
!!! ExerciceTD "3 BEOS 7509"
    === "Énoncé"
        Soit $E$ un $\mathbb{R}$-ev de dimension $n \in \mathbb{N}^*$.  
        Soit $u$ un endomorphisme de $E$.  
        1. On suppose qu'il existe $x \in E$ tel que la famille $\left(x, u(x), \ldots, u^{n-1}(x)\right)$ soit libre. Montrer que la famille $\left(i d_E, u, \ldots, u^{n-1}\right)$ est libre.  
        Que peut-on dire de la famille $\left(i d_E, u, \ldots u^n\right)$ ?  
        2. On suppose désormais que $u$ est diagonalisable et ( $i d_E, \ldots, u^{n-1}$ ) libre. Montrer qu'il existe $x \in E$ tel que : $\left(x, u(x), \ldots, u^{n-1}(x)\right)$ soit libre.  
        
!!! ExerciceTD "4 RMS23 1038"
    === "Énoncé"
        Montrer que toute matrice $M \in \mathcal{M}_n(\mathbb{R})$ est somme de deux matrices diagonalisables.
    

    
!!! ExerciceTD "5 RMS23 1041"
    === "Énoncé"
        Soient $A \in \mathcal{M}_n(\mathbb{K})$ et $\varphi: M \in \mathcal{M}_n(\mathbb{K}) \mapsto A M-M A$.  
        a) Montrer que $\varphi$ est un endomorphisme de $\mathcal{M}_n(\mathbb{K})$.  
        b) On suppose que $A$ est nilpotente. Montrer que $\varphi$ est nilpotente.  
        c) On suppose que $A$ est diagonalisable. Montrer que $\varphi$ est diagonalisable.  


!!! ExerciceTD "6 BEOS 7079"
    === "Énoncé"
        Soient $\left(a_0, a_1, \ldots, a_{n-1}\right) \in \mathbb{C}^n$.  
        On pose $A=\left(\begin{array}{ccccc}0 & \cdots & \cdots & 0 & -a_0 \\ 1 & \ddots & & \vdots & -a_1 \\ 0 & \ddots & \ddots & \vdots & \vdots \\ \vdots & \ddots & \ddots & 0 & -a_{n-2} \\ 0 & \cdots & 0 & 1 & -a_{n-1}\end{array}\right)$.  
        1) Déterminer $\chi_A$  
        2) Montrer que: $A$ est diagonalisable $\Longleftrightarrow \chi_A$ est scindé à racines simples.  
        Pour le sens direct on montrera l'implication par deux méthodes différentes.

!!! ExerciceTD "7 BEOS 7451"
    === "Énoncé"
        Soit $z \in \mathbb{C}$ et $M(z)=\left(\begin{array}{lll}0 & z & z \\ 1 & 0 & z \\ 1 & 1 & 0\end{array}\right)$. Donner le polynôme caractéristique de $M(z)$. Pour quelles valeurs de $z, M(z)$ est-elle diagonalisable?
        
!!! ExerciceTD "8 RMS23 937"
    === "Énoncé"
        Soit $A \in \mathcal{M}_n(\mathbb{R})$ telle que $A^3-A-I_n=0$.  
        a) Montrer que si $\lambda$ est une valeur propre de $A$ alors $\lambda^3-\lambda-1=0$.  
        b) Montrer que $\operatorname{det}(A)>0$.    
        

!!! ExerciceTD "9 RMS23 1036"
    === "Énoncé"
        Soit $M$ la matrice de $\mathcal{M}_n(\mathbb{R})$ définie par $M=\left(\begin{array}{cccc}0 & \cdots & 0 & 1 \\ \vdots & & \vdots & \vdots \\ 0 & \cdots & 0 & \vdots \\ 1 & \cdots & \cdots & 1\end{array}\right)$.  
        a) Montrer que $M$ est diagonalisable.  
        b) Caractériser $\operatorname{Ker}(M)$ en donnant une équation, sa dimension et une base.  
        c) Montrer que $M$ admet deux valeurs propres non nulles.  
        d) Déterminer ces valeurs propres ainsi que leurs espaces propres respectifs.   
        e) Donner le polynôme caractéristique de $M$.  
    
!!! ExerciceTD "10 RMS23 933"
    === "Énoncé"
        Soit $A=\left(\begin{array}{ccc}1 & 0 & 0 \\ 1 & 2 & 1 \\ 2 & -2 & -1\end{array}\right)$.  
        a) Donner le spectre de $A$ et ses espaces propres. La matrice $A$ est-elle diagonalisable.  
        b) Montrer qu'il existe $P \in \mathrm{GL}_3(\mathbb{R})$ tel que $A=P T P^{-1}$ avec $T=\left(\begin{array}{ccc}0 & 0 & -3 \\ 0 & 1 & 4 \\ 0 & 0 & 1\end{array}\right)$.  
        c) Trouver l'ensemble des matrices $M \in \mathcal{M}_3(\mathbb{R})$ telles que $M T=T M$.  
        d) Soit $N \in \mathcal{M}_3(\mathbb{R})$ telle que $N^2=T$. Montrer que $N T=T N$.  
        Trouver l'ensemble des matrices $N$ telles que $N^2=T$.  
        e) En déduire l'ensemble des matrices $M \in \mathcal{M}_3(\mathbb{R})$ telles que $A=M^2$.
        
