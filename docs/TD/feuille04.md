<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD S4 (Intégrale sur un intervalle)

!!! ExerciceTD "1 (BEOS 7755)"
    === "Énoncé"
        On définit pour tout $t>0, f(t)=\frac{\ln t}{(1+t)^2}$.  
        1) Montrer que $f$ est intégrable sur $] 0,1]$, puis sur $[1,+\infty[$.   
        2) Calculer $\int_0^1 f(t) \mathrm{d} t$ et $\int_1^{+\infty} f(t) \mathrm{d} t$.
        


!!! ExerciceTD "2 (BEOS 6686)" 
    === "Énoncé"
        1) Soit $\alpha \in \mathbb{R}_{+}^*$. Montrer que $\int_1^{+\infty} \frac{\mathrm{e}^{i t}}{t^\alpha} \mathrm{d} t$ converge.  
        2) En déduire la nature de $\int_1^{+\infty} \sin \left(t^2\right) \mathrm{d} t$.  
        3) Montrer que $\int_1^{+\infty} \frac{\sqrt{t} \sin t}{t+\cos t} \mathrm{~d} t$ converge.



!!! ExerciceTD "3 (BEOS 4779)" 
    === "Énoncé"
        a) Justifier que la fonction $t \mapsto \frac{1}{t^2}-\frac{1}{[\operatorname{Arctan}(t)]^2}$ est intégrable sur $[0,1[$.  
        b) En déduire un équivalent simple de $\Phi(\alpha)=\int_\alpha^1 \frac{1}{[\operatorname{Arctan}(t)]^2} \mathrm{~d} t$ quand $\alpha \rightarrow 0^{+}$.


!!! ExerciceTD "4 (BEOS 8036)"
    === "Énoncé"
        Soit $f: \mathbb{R}_{+} \longrightarrow \mathbb{R}$ une fonction de classe $\mathcal{C}^1$ telle que $\left(f^{\prime}\right)^2$ soit intégrable.  
        Montrer que $t \longmapsto \frac{f^2(t)}{t^2}$ est intégrable sur $[1,+\infty[$.     


!!! ExerciceTD "5 (BEOS 6911)"
    === "Énoncé"
        1. Soit $M>0$ et $u:\left[1,+\infty\left[\rightarrow \mathbb{R}\right.\right.$ de classe $\mathcal{C}^1$ tel que $\forall x \in[1,+\infty[,|u(x)| \leqslant M$.

        Montrer que $\int_1^{\infty} \frac{u^{\prime}(t)}{t} \mathrm{~d} t$ converge.  
        2. Montrer que $\int_1^{\infty} \frac{\sin t}{t} \mathrm{~d} t$ et $\int_1^{\infty} \sin \left(t^2\right) \mathrm{d} t$ convergent.  
        3. Montrer que $\int_1^{\infty} \sin \left(t^3\right) \mathrm{d} t$ converge.


!!! ExerciceTD "6 (BEOS 7576)"
    === "Énoncé"
        Justifier la convergence de l'intégrale $\int_0^{+\infty}\left(1-t \arctan \left(\frac{1}{t}\right)\right) \mathrm{d} t$ puis calculer sa valeur.



!!! ExerciceTD "7 (BEOS 4721)"
    === "Énoncé"
        1. Démontrer la convergence de l'intégrale $I=\int_0^{\frac{\pi}{2}} \ln (\sin t) d t$.  
        2. Calculer $J=\int_0^{\frac{\pi}{2}} \ln (\cos t) d t$ et $K=\int_0^\pi \ln (\sin t) d t$ en fonction de $I$.   
        3. Déterminer $L=\int_0^{\frac{\pi}{2}} \ln (\sin t \cos t) d t$ en fonction de $I, J, K$.

        En déduire les valeurs de $I, J, K, L$.


!!! ExerciceTD "8 (RMS23 962)" 
    === "Énoncé"
        Existence et calcul de $\int_0^{+\infty} \frac{\sin (x)^3}{x^2} \mathrm{~d} x$.       


!!! ExerciceTD "9 (RMS23 1348)"
    === "Énoncé"
        1348. On pose $F(x)=\int_x^{+\infty} \frac{\sin t}{t^2} \mathrm{~d} t$.  
        a) Montrer que $F$ est définie sur $] 0,+\infty[$.  
        b) Montrer que $F$ est de classe $\mathcal{C}^{\infty}$.  
        c) Montrer qu'en $+\infty$, on a $F(x)=\frac{\cos x}{x^2}+\mathrm{O}\left(\frac{1}{x^3}\right)$.  
        d) Montrer qu'en zéro, on a $F(x) \sim-\ln x$.  
 


!!! ExerciceTD "10 (BEOS 1579)"
    === "Énoncé"
        Convergence puis calcul de $\int_0^1 \frac{\ln x}{\sqrt{x}(1-x)^{3 / 2}} \mathrm{~d} x$.
        