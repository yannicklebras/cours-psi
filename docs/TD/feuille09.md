<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD S09 Endomorphismes des espaces euclidiens


!!! ExerciceTD "1 RMS457"
    === "Énoncé"
        Donner une condition sur $A=\left(a_{i, j}\right)_{1 \leqslant i, j \leqslant n} \in \mathcal{S}_n(\mathbb{R})$ pour que l'application qui à $U=\left(u_{i, j}\right)_{1 \leqslant i, j \leqslant n} \in \mathcal{O}_n(\mathbb{R})$ associe $\sum_{1 \leqslant i, j \leqslant n} a_{i, j} u_{i, j}$ atteigne son maximum en un unique $U$.


!!! ExerciceTD "2 RMS657"
    === "Énoncé"
        Soit $A \in \mathcal{S}_n(\mathbb{R})$. Montrer que $\left(\sum_{i=1}^n a_{i, i}\right)^2 \leqslant \operatorname{rg}(A) \sum_{i=1}^n \sum_{j=1}^n a_{i, j}^2$.



!!! ExerciceTD "3 RMS947"
    === "Énoncé"
        a) Soient $A, B \in \mathcal{S}_n^{++}(\mathbb{R})$.  
        i) Montrer qu'il existe une matrice $C \in \mathcal{M}_n(\mathbb{R})$ et une seule telle que $A C+C A=B$.  
        ii) Montrer que $C$ est symétrique et à spectre inclus dans $\mathbb{R}^{+*}$.  
        b) On prend $n=2$. Montrer qu'il existe $C, A \in \mathcal{S}_n^{++}(\mathbb{R})$ avec $A C+C A$ à spectre non inclus dans $\mathbb{R}^{+*}$.  


!!! ExerciceTD "4 BEOS6817"
    === "Énoncé"
        Soit $a_1, \ldots, a_n n$ réels non tous nuls. On pose :

        $$
        A=\left(\begin{array}{cccc}
        0 & a_1 & \cdots & a_n \\
        a_1 & 0 & \cdots & 0 \\
        \vdots & \vdots & \ddots & \vdots \\
        a_n & 0 & \cdots & 0
        \end{array}\right)
        $$

        1. Montrer que $A$ est diagonalisable.  
        2. Quel est le rang de $A$ ? Que peut-on en déduire sur son spectre ?  
        3. Calculer $A^2$. En déduire le spectre et le polynôme caractéristique de $A$.  


!!! ExerciceTD "5 BEOS8318"
    === "Énoncé"
        Soit $M \in \mathcal{M}_2(\mathbb{R})$ telle que $M M^{\top}=M^{\top} M$ et $M^2+2 I_2=0$.  
        1. Montrer que $M^{\top} M$ est diagonalisable.  
        2. Montrer que $\operatorname{Sp}\left(M^{\top} M\right) \subset\{-2,2\}$.  
        3. Montrer que $\forall \lambda \in \operatorname{Sp}\left(M^{\top} M\right), \lambda \geqslant 0$. En déduire $\operatorname{Sp}\left(M^{\top} M\right)$.  
        4. Montrer que $\frac{1}{\sqrt{ } 2} M$ est orthogonale.  
        5. Montrer que $\frac{1}{\sqrt{ } 2} M$ est la matrice d'une rotation d'angle $\theta$ à déterminer.  
        6. Déterminer toutes les matrices $M$ possibles.  

!!! ExerciceTD "6 BEOS5251"
    === "Énoncé"
        Soit $a \in \mathbb{R}$ et $A=\frac{1}{7}\left(\begin{array}{ccc}-a & 4+a & -(1+a) \\ 4+a & 1+a & a \\ 1+a & -a & -(4+a)\end{array}\right)$.   
        1) Condition nécessaire et suffisante sur $a$ pour que $A$ appartienne à $\mathrm{SO}_3(\mathbb{R})$.  
        2) Quel est l'endomorphisme associé à $A$ dans ce cas ?  


!!! ExerciceTD "7 BEOS5034"
    === "Énoncé"
        Soit $M \in \mathfrak{M}_n(\mathbb{R})$ telle que ${ }^t M M=M^t M, M^3=I_n$ et $M \neq I_n$.  
        1) Montrez que $M$ est orthogonale.  
        2) Soit $n=3$. Donnez toutes les valeurs possibles de $M$.  


!!! ExerciceTD "8 BEOS6063"
    === "Énoncé"
        Soit $H=\left(\begin{array}{lll}0 & 1 & 1 \\ 1 & 0 & 1 \\ 1 & 1 & 0\end{array}\right)$.

        1. $H$ est-elle diagonalisable?  
        2. Si $(a, b) \in \mathbb{R}^2$, on pose : $R(a, b)=\left(\begin{array}{lll}a & b & b \\ b & a & b \\ b & b & a\end{array}\right)$, notée plus simplement $R$.  

        Exprimer $R$ en fonction de $H$ et $I_3$. La matrice $R$ est-elle diagonalisable ?  
        3. On pose $u_n=\operatorname{Tr} H^n$ pour tout $n \in \mathbb{N}$. Montrer que la suite $\left(u_n\right)_{n \in \mathbb{N}}$ est à valeurs entières et diverge.  
        4. On pose $v_n=\operatorname{Tr} R^n$ pour tout $n \in \mathbb{N}$. Peut-on trouver $a$ et $b$ tels que la suite $\left(v_n\right)_{n \in \mathbb{N}}$ converge ?  




!!! ExerciceTD "9 RMS946"
    === "Énoncé"
        Soit $A \in \mathcal{M}_2(\mathbb{R})$ telle que $A^T=A^2$.  
        a) Montrer que $A^T A$ est semblable à une matrice diagonale $D$. Quel est le nombre de possibilités pour $D$ ?  
        b) Montrer que $A$ est orthogonalement semblable à une des matrices suivantes :

        $$
        0_2, I_2, \quad\left(\begin{array}{ll}
        1 & 0 \\
        0 & 0
        \end{array}\right),\left(\begin{array}{lr}
        -\frac{1}{2} & -\frac{\sqrt{3}}{2} \\
        \frac{\sqrt{3}}{2} & -\frac{1}{2}
        \end{array}\right) .
        $$



!!! ExerciceTD "10 RMS1069"
    === "Énoncé"
        Soient $X \in \mathbb{R}^n$ non nul et $A$ une matrice symétrique définie positive. Montrer que la suite de terme général $\frac{\left\langle A^{k+1} X, X\right\rangle}{\left\langle A^k X, X\right\rangle}$ converge vers une valeur propre de $A$.


