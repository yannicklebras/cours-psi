<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD S1



!!! ExerciceTD "1 (BEOS 5763)"

    === "Énoncé"
        Soit $\ds A=\left(\begin{array}{lll}0 & 3 & 0 \\ 1 & 0 & 1 \\ 1 & 0 & 0\end{array}\right)$ et, pour tout $\ds n, u_n=\operatorname{Tr}\left(A^n\right)$.

        1. Trouver une relation vérifiée par la suite $\ds \left(u_n\right)$.

        2. Étudier la série $\ds \Sigma\left(1 / u_n\right)$.





!!! ExerciceTD "2 (BEOS 2487)"

    === "Énoncé"
        On considère la suite définie par $\ds u_0 \in \mathbb{R}$ et $\ds u_n=(-1)^n \frac{\cos u_{n-1}}{n}$ pour tout $\ds n \geqslant 1$. Déterminer la nature de la série de terme général $\ds u_n$.

!!! ExerciceTD "3 (BEOS 1286)"

    === "Énoncé"
        Soit $\ds a, b \in \mathbb{R}$. On définit, au moins à partir d'un certain rang, $\ds u_n=\tan \left(\pi \sqrt{n^2+a n+b}\right)$. Quelle est la nature de la série de terme général $\ds u_n$ en fonction des valeurs de $\ds a$ et $\ds b$ ?


!!! ExerciceTD "4 (BEOS 7538)"

    === "Énoncé"
        Pour $\ds n \in \mathbb{N}^*$, on pose $\ds u_n=\sum_{k=1}^n(-1)^k \sqrt{k}$.  
        1. Montrer que : $\ds u_{2 n}=\sum_{\ell=1}^n \frac{1}{\sqrt{2 \ell}+\sqrt{2 \ell-1}}$.  
        2. En déduire que $\ds u_{2 n} \underset{n \rightarrow+\infty}{\sim} \frac{\sqrt{2 n}}{2}$.  
        3. Déterminer un équivalent simple de $\ds u_n$ quand $\ds n$ tend vers $\ds +\infty$.  
        4. Pour $\ds n \in \mathbb{N}^*$, on pose $\ds v_n=u_n+u_{n+1}$. Justifier que la série $\ds \sum_{n \geqslant 1}\left(v_{n+1}-v_n\right)$ est convergente de somme strictement négative.  
        5. Trouver la nature de $\ds \sum_{n \geqslant 1} \frac{1}{u_n}$.  


!!! ExerciceTD "5 (BEOS 4360)"

    === "Énoncé"
        Étudier la convergence de la série de terme général : $\ds u_n=\frac{(-1)^{\frac{n(n+1)}{2}}}{n^{1 / 4}}$.


!!! ExerciceTD "6 (RMS23 1340)"

    === "Énoncé"
        Pour $\ds n \in \mathbb{N}^*$, on pose $\ds a_n=\sum_{k=1}^n(\ln k)^2$. Déterminer un équivalent de $\ds a_n$ en $\ds +\infty$, et la nature de la série $\ds \sum \frac{1}{a_n}$.


!!! ExerciceTD "7 (RMS23 1293)"

    === "Énoncé"
        Nature de la série de terme général $\ds z_n=\sqrt{(n-1)!} \prod_{k=1}^{n-1} \sin \left(\frac{1}{\sqrt{k}}\right)$ ?

!!! ExerciceTD "8 (RMS23 1572)"

    === "Énoncé"
        Nature de la série de terme général $\ds u_n=\frac{(-1)^n}{(-1)^n+\ln (n) \sqrt{n}}, n \geqslant 1$.

!!! ExerciceTD "9 (RMS23 1653)"

    === "Énoncé"
        Pour tout $\ds n \in \mathbb{N}^*$, on pose $\ds H_n=\sum_{k=1}^n \frac{1}{k}$ et $\ds u_n=\frac{1}{\sum_{k=1}^n k^2}$.  
        a) Trouver $\ds a, b, c \in \mathbb{R}$ tels que $\ds \forall n \in \mathbb{N}^*, \frac{1}{n(n+1)(2 n+1)}=\frac{a}{n}+\frac{b}{n+1}+\frac{c}{2 n+1}$  
        b) Montrer que $\ds \forall n \in \mathbb{N}^*, \sum_{k=1}^n k^2=\frac{n(n+1)(2 n+1)}{6}$. En déduire la nature de $\ds \sum u_n$.  
        c) À l'aide d'une comparaison somme-intégrale, montrer que $\ds H_{2 n}-H_n \underset{n \rightarrow+\infty}{\longrightarrow} \ln (2)$.  
        d) Exprimer $\ds \sum_{j=1}^n \frac{1}{2 j+1}$ à l'aide de $\ds H_n, H_{2 n}$ et $\ds n$.  
        e) Calculer $\ds \sum_{n=1}^{+\infty} u_n$.


!!! ExerciceTD "10 (RMS23 1656)"

    === "Énoncé"
        Soit $\ds (a, b) \in \mathbb{R}^2$. Pour tout $\ds n \in \mathbb{N}$, on pose $\ds u_n=\sqrt{n}+a \sqrt{n+1}+b \sqrt{n+2}$.  
        a) Trouver $\ds \alpha$ tel que $\ds u_n=(1+a+b) \sqrt{n}+\frac{\alpha}{\sqrt{n}}+\mathrm{O}\left(\frac{1}{n \sqrt{n}}\right)$.  
        b) Trouver les couples $\ds (a, b)$ tels que la série $\ds \sum u_n$ converge.

!!! ExerciceTD "11 (RMS 1657)"

    === "Énoncé"
        Pour tout $\ds n \geqslant 2$, on pose $\ds a_n=\frac{(-1)^n}{\sqrt{n}}$.  
        a) Quelle est la nature de la série $\ds \sum \ln \left(1+a_n\right)$ ?  
        b) Calculer $\ds \lim _{n \rightarrow+\infty} \prod_{k=2}^n\left(1+a_k\right)$.

