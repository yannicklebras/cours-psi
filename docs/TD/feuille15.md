<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD calcul différentiel


!!! ExerciceTD "1-BEOS7770"
    === "Énoncé"

        Soit $f: \quad \mathbb{R}^2 \quad \longrightarrow \mathbb{R}$

        $$
        (x, y) \longmapsto\left\{\begin{array}{ccc}
        x y \frac{x^2-y^2}{x^2+y^2} & \text { si } & (x, y) \neq(0,0) \\
        0 & \text { si } & (x, y)=(0,0)
        \end{array}\right.
        $$

        1. Étudier la continuité de $f$ sur $\mathbb{R}^2$.  
        2. Déterminer les dérivées partielles premières de $f$; est-elle de classe $\mathcal{C}^1$ sur $\mathbb{R}^2$ ?  
        3. Calculer $\frac{\partial^2 f}{\partial x \partial y}(0,0)$ et $\frac{\partial^2 f}{\partial y \partial x}(0,0)$. Commenter.


!!! ExerciceTD "2-BEOS6656"
    === "Énoncé"
        Étudier la continuité en $(0,0)$ de chacune des fonctions suivantes, toutes supposées nulles en $(0,0)$. Elles sont définies pour tout $(x, y) \in \mathbb{R}^2 \backslash\{(0,0)\}$ respectivement par :

        $$
        f(x, y)=\frac{x^3 y}{x^2+y^2}, g(x, y)=\frac{x y}{x^2+y^2}, h(x, y)=\frac{x^3 y}{x^2+y^2+x y}
        $$


!!! ExerciceTD "3-BEOS7059"
    === "Énoncé"
        1. Soit $h: \mathbb{R} \longrightarrow \mathbb{R}$ continue. A l'aide du changement de variables $\left\{\begin{array}{l}x=u+v \\ y=u-v^{\prime}\end{array}\right.$, déterminer les fonctions $f: \mathbb{R}^2 \longrightarrow \mathbb{R}$ de classe $\mathcal{C}^1$ telles que $: \frac{\partial f}{\partial x}-\frac{\partial f}{\partial y}=h(3 x+y)$.  
        2. En adaptant le changement de variables précédent, déterminer les fonctions $f: \mathbb{R}^2 \longrightarrow \mathbb{R}$ de classe $\mathcal{C}^1$ telles que $: \frac{\partial f}{\partial x}=3 \frac{\partial f}{\partial y}$.  
        3. En déduire les solutions de : $\frac{\partial^2 f}{\partial x^2}-4 \frac{\partial^2 f}{\partial x \partial y}+3 \frac{\partial^2 f}{\partial y^2}=0$, avec $f: \mathbb{R}^2 \longrightarrow \mathbb{R}$ de classe $\mathcal{C}^2$.  
        


!!! ExerciceTD "4-BEOS4479"
    === "Énoncé"
        Soit $a \in \mathbb{R}$. Soit $E=C^1\left(\mathbb{R}^2, \mathbb{R}\right)$ et $F=C^0\left(\mathbb{R}^2, \mathbb{R}\right)$.
        Soit $\phi: f \rightarrow \frac{\partial f}{\partial x}-a f$.  
        1. Montrer que $\phi$ est une application linéaire de $E$ dans $F.  
        2. Soit $\forall(x, y) \in \mathbb{R}^2, f(x, y)=\sin (y) \exp (a x)$. Calculer $\phi(f)$.  
        3. Soit $G=\left\{\alpha(y) \exp (a x)\right.$ avec $\left.\alpha \in C^1(\mathbb{R}, \mathbb{R})\right\}$. Montrer que $G \subset \operatorname{Ker}(\phi)$. $\phi$ est elle injective?  
        4. Soit $A \in F$. Soit $\forall(x, y) \in \mathbb{R}^2, f(x, y)=\exp (a x) \int_0^x A(t, y) \exp (-a t) d t$. Montrer que $f$ admet des dérivées partielles sur $\mathbb{R}^2$ et les calculer. Montrer que $\phi(f)=A$.  
        5. Montrer que $G=\operatorname{Ker}(\phi)$.  
        6. Trouver toutes les fonctions $f \in E$ telles que:

        $$
        \forall(x, y) \in \mathbb{R}^2, \frac{\partial f}{\partial x}(x, y)-a f(x, y)=2 x-3 y
        $$


!!! ExerciceTD "5-BEOS8390"
    === "Énoncé"
        Soit 
        
        $$
        f(x, y)=3 x y-x^3-y^3
        $$

        Etudier les extremas de la fonction $f$.
        

!!! ExerciceTD "6-RMS980"
    === "Énoncé"
        Soient $u \in \mathcal{C}^0\left(\mathbb{R}^{+}, \mathbb{R}\right)$ intégrable sur $\mathbb{R}^{+}$et $f \in \mathcal{C}^2\left(\mathbb{R}^{+}, \mathbb{R}\right)$ telle que $f^{\prime \prime}+(1+u) f=0$ Soit $g: x \in \mathbb{R}^{+} \mapsto f(x)+\int_0^x \sin (x-t) f(t) u(t) \mathrm{d} t$.  
        a) Trouver une équation différentielle linéaire vérifiée par $g$.  
        b) En déduire l'existence de $c$ positif tel que : $\forall x \in \mathbb{R}^{+},|f(x)| \leqslant c+\int_0^x|f(t) u(t)| \mathrm{d} t$.
        c) Montrer que $f$ est bornée.
        
!!! ExerciceTD "7-RMS1184"
    === "Énoncé"
        a) Trouver les fonctions $f \in \mathcal{C}^1(] 1,+\infty[\times] 0,+\infty[, \mathbb{R})$ telles que

        $$
        x(x-1) \frac{\partial f}{\partial x}+y(x-1) \frac{\partial f}{\partial y}-x^2 f=0
        $$


        Ind. Effectuer le changement de variables $x=u$ et $y=u v$.  
        b) Soient $a_1, \ldots, a_n$ des réels distincts, $f_i: x \mapsto \exp \left(a_i x\right)$ pour $1 \leqslant i \leqslant n$. Montrer que $\left(f_i\right)_{1 \leqslant i \leqslant n}$ est libre.  
        c) Le sous-espace vectoriel des solutions obtenu dans la première question est-il de dimension finie?



!!! ExerciceTD "8-RMS1313"
    === "Énoncé"
        Soient $n \in \mathbb{N}^*$ et $f$ une fonction de classe $\mathcal{C}^2$ de $\mathbb{R}^n$ dans $\mathbb{R}$. On suppose que, pour tout $x \in \mathbb{R}^n$, la matrice Hessienne $H_f(x)$ a toutes ses valeurs propres dans $[1,+\infty[$.  
        a) Pour $x$ fixé dans $\mathbb{R}$ on note $\varphi: t \in \mathbb{R} \mapsto f(t x)$. Montrer que $\varphi$ est de classe $\mathcal{C}^2$ et exprimer $\varphi^{\prime \prime}$ en fonction de la matrice Hessienne de $f$.  
        b) En considérant la fonction $\psi: t \mapsto f(t x)-\langle\nabla f(0), t x\rangle-\frac{t^2}{2} x^T x$, montrer l'inégalité $f(x) \geqslant f(0)+\langle\nabla f(0), t x\rangle+\frac{1}{2} x^T x$.  
        c) En déduire que $\lim _{\|x\| \rightarrow+\infty} f(x)=+\infty$. Montrer que $f$ admet un minimum.  



!!! ExerciceTD "9-RMS1599"
    === "Énoncé"
        [IMT] Soit $A=\left(\begin{array}{cc}-5 & 3 \\ 6 & -2\end{array}\right)$.  
        a) Montrer que $A$ est diagonalisable et donner ses valeurs propres.  
        b) Montrer qu'il existe $B$ telle que $B^3=A$.  
        c) Résoudre $X^{\prime}(t)=A X(t)$.  
        
!!! ExerciceTD "10-RMS1600"
    === "Énoncé"
        [CCINP] Soient $F=\left\{(x, y) \in \mathbb{R}^2 \mid x+y=0\right\}$ et $f: \mathbb{R}^2 \rightarrow \mathbb{R}$ telle que $f(x, y)=$ $\frac{x^2 y^2}{x+y}$ si $(x, y) \notin F$, et $f(x, y)=0$ si $(x, y) \in F$.  
        a) Montrer que $f$ est de classe $\mathcal{C}^1$ sur $\mathbb{R}^2 \backslash F$ et que $x \frac{\partial f}{\partial x}+y \frac{\partial f}{\partial y}=3 f$.  
        b) Montrer que $f$ admet des dérivées partielles en $(0,0)$ et les calculer.  
        c) La fonction $f$ est-elle continue en $(0,0)$ ?  

