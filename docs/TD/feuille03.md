<style>
@media print {
  body {
  column-count: 2;
     -webkit-column-count: 2;
     -moz-column-count: 2;
  }
  .md-typeset .admonition.exercicetd,
    .md-typeset details.exercicetd {
      border:none;
      page-break-inside:auto;
      font-size:0.5rem;
    }
    .md-typeset .exercicetd .tabbed-block {
        margin-left:-10px;
        margin-right:-10px
    }
    .md-typeset .exercicetd > .admonition-title,
    .md-typeset .exercicetd > summary {
      padding-left:0rem;
      background-color: light-gray;
    }
    .md-typeset .exercicetd .tabbed-labels {display:none};
    .md-typeset .exercicetd > .admonition-title::before,
    .md-typeset .exercicetd > summary::before {
    background: none;
    background-color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
    -webkit-mask-image: none;
              mask-image: none;
    width:1rem
    }
}
</style>

# TD S3 (séries entières)

!!! ExerciceTD "1 (BEOS 7755)"
    === "Énoncé"
        1. Définir le rayon de convergence d'une série entière à coefficients complexes.  
        2. Soit $\left(a_n\right)$ une suite bornée telle que $\sum a_n$ diverge. Déterminer le rayon de convergence de $\sum a_n z^n$.  
        3. Déterminer le rayon de convergence de :  

        $$
        \sum(\sqrt{n})^{(-1)^n} \ln \left(1+\frac{1}{\sqrt{n}}\right) z^n
        $$


!!! ExerciceTD "2 (BEOS 7634)" 
    === "Énoncé"
        Soit $\left(a_n\right)_{n \in \mathbb{N}}$ une suite complexe telle que la série entière $\sum a_n z^n$ soit de rayon infini. On note $f$ sa somme.  
        1. Soit $r>0$ et $p \in \mathbb{N}$. Montrer que :  

        $$
        \int_0^{2 \pi} f\left(r \mathrm{e}^{i t}\right) \mathrm{e}^{-i p t} \mathrm{~d} t=2 \pi a_p r^p
        $$

        2. On suppose $f$ bornée sur $\mathbb{C}$.  
        a) Montrer qu'il existe $M \geqslant 0$ tel que :   

        $$
        \forall p \in \mathbb{N},\left|a_p\right| \leqslant \frac{M}{r^p}
        $$

        b) Montrer que $a_p=0$ pour tout $p \in \mathbb{N}^*$.  

        En déduire que $f$ est constante.  
        3. On suppose maintenant qu'il existe $q \in \mathbb{N}^*$ et $(\alpha, \beta) \in\left(\mathbb{R}_{+}^*\right)^2$ tels que :  

        $$
        \forall z \in \mathbb{C},|f(z)| \leqslant \alpha|z|^q+\beta
        $$


        Montrer que $f$ est une fonction polynôme.  


!!! ExerciceTD "3 (BEOS 6860)" 
    === "Énoncé"
        Soit la fonction $f(x)=\arcsin x \cdot \sqrt{1-x^2}$  
        1. Montrer que cette fonction est $\mathcal{C}^1$ sur un intervalle que l'on précisera et donner sa dérivée.  
        2. Trouver des polynômes non nuls $a, b, c$ tels que $f$ soit solution de l'équation différentielle du premier ordre : $a(x) y^{\prime}+b(x) y=c(x)$.  
        3. Montrer que l'unique solution de cette équation qui s'annule en 0 , est une fonction impaire développable en série entière au voisinage de 0.  
        4. En déduire que $f$ est développable en série entière au voisinage de 0.  
        5. Donner ce développement en série entière.  


!!! ExerciceTD "4 (BEOS 6944)"
    === "Énoncé"
        Pour tout $n \in \mathbb{N}$, on pose $a_n=\frac{n^2-3 n+1}{n!}$.  
        Déterminer le rayon de convergence de la série entière $\sum_{n \geqslant 0} a_n x^n$ et calculer sa somme.



!!! ExerciceTD "5 (BEOS 6911)"
    === "Énoncé"
        On considère le problème de Cauchy suivant, noté $(S)$  

        $$
        \left\{\begin{array}{l}
        y^{\prime \prime}(x)+x y^{\prime}(x)+y(x)=1 \\
        y(0)=0, y^{\prime}(0)=0
        \end{array}\right.
        $$


        Soit $\left(a_n\right)_{n \in \mathbb{N}}$ une suite réelle. On suppose que le rayon de convergence de la série entière $\sum_{n \geqslant 0} a_n x^n$, noté $R$, est strictement positif.  
        On définit alors la fonction $f: x \mapsto \sum_{n=0}^{+\infty} a_n x^n$ de $]-R, R[$ dans $\mathbb{R}$.  
        1. On suppose que $f$ est solution du problème $(S)$ sur $]-R, R[$.  

        Trouver alors une relation de récurrence sur les coefficients $a_n$.  
        2. Exprimer $f(0)$ et $f^{\prime}(0)$ à l'aide de certains coefficients $a_k$.  
        3. Sous la même hypothèse qu'à la question 1 , trouver une expression des coefficients $a_n$ et en déduire une expression de $f$ à l'aide des fonctions usuelles.  
        4. (Question extrapolée par le transcripteur) Effectuer la réciproque.



!!! ExerciceTD "6 (RMS23 965)"
    === "Énoncé"
        Soit $n \in \mathbb{N}^*$. Calculer $w_n=\sum_{k=0}^n(-1)^k\binom{2 k}{k}\binom{2 n-2 k}{n-k}$. Ind. Utiliser une série entière.)




!!! ExerciceTD "7 (RMS23 966)"
    === "Énoncé"
        a) Déterminer le rayon de convergence et calculer la somme de la série entière  

        $$
        \sum_{n \geqslant 1} \frac{x^{2 n+2}}{\sum_{k=0}^n k^2}
        $$

        b) Calculer $\sum_{n=1}^{+\infty} \frac{1}{\sum_{k=0}^n k^2}$.  



!!! ExerciceTD "8 (RMS23 967)" 
    === "Énoncé"
        On définit la suite $\left(u_n\right)$ par : $u_0=1$ et, pour $n \in \mathbb{N}^*, u_n=\sqrt{n+u_{n-1}}$.  
        a) Montrer que, pour tout $n \in \mathbb{N}$, on a : $\sqrt{n} \leqslant u_n \leqslant 2 \sqrt{n+1}$.  
        b) Montrer que $u_n \sim \sqrt{n}$ et déterminer la limite de $\left(u_n-\sqrt{n}\right)$.  
        c) Donner le rayon de convergence $R$ de la série entière $\sum u_n x^n$.  
        d) Calculer $\lim _{x \rightarrow R^{-}} \sum_{n \geqslant 0} u_n x^n$.  


!!! ExerciceTD "9 (RMS23 1304)"
    === "Énoncé"
        Soit $f: x \mapsto \sum_{n=0}^{+\infty} \cos \left(n^2 x\right) e^{-n}$.  
        a) Montrer que $f$ est de classe $C^{\infty}$. Exprimer $f^{(4 p)}(0)$ sous forme de somme.  
        b) Montrer : $\forall p \in \mathbb{N}^*, \int_0^{8 p} t^{8 p} e^{-t} \mathrm{~d} t \leqslant \sum_{n=0}^{8 p} n^{8 p} e^{-n}$ et $\int_{8 p}^{+\infty} t^{8 p} e^{-t} \mathrm{~d} t \leqslant \sum_{n=8 p}^{+\infty} n^{8 p} e^{-n}$.  
        c) Montrer que, pour tout $r>0$, la série $\sum \frac{f^{(4 p)(0)}}{(4 p)!} r^{4 p}$ diverge.  
        d) Montrer que $f$ n'est pas développable en série entière en 0.  

!!! ExerciceTD "10 (BEOS 7741)"
    === "Énoncé"
        On pose, pour $p \in \mathbb{N}^*, f(x)=\sum_{n=0}^{+\infty}\binom{n+p}{p} x^n$.  
        1. Préciser le rayon de convergence de $f$.  
        2. Montrer que $f$ est solution de

        $$
        (E):(1-x) y^{\prime}(x)=(p+1) y(x)
        $$

        sur son intervalle ouvert de convergence.  
        3. Donner une expression de $f$.  
 