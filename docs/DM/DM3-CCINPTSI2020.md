<style>
h2 {text-align:center}
</style>


# Devoir Maison 3

Sujet etoilé : Vous pouvez faire, presque en entier [Centrale Maths 2 2023](https://www.concours-centrale-supelec.fr/CentraleSupelec/2023/PSI/M014.pdf).

## Partie I - Préliminaires

Q1. Justifier que pour tout $\ds n$ appartenant à $\ds \mathbb{N}, t^n e^{-t^2}=\underset{t\to+\infty}{o}\left(\frac{1}{t^2}\right)$.

Q2. Montrer que pour tout $\ds n$ élément de $\ds \mathbb{N}$, l'intégrale $\ds \int_0^{+\infty} t^n e^{-t^2} d t$ est convergente.

Q3. En déduire que pour tout $\ds n$ élément de $\ds \mathbb{N}$, l'intégrale $\ds \int_{-\infty}^{+\infty} t^n e^{-t^2} d t$ est convergente.

Q4. En déduire que pour tout polynôme $\ds P$ appartenant à $\ds \mathbb{R}[X]$, l'intégrale $\ds \int_{-\infty}^{+\infty} P(t) e^{-t^2} d t$ est convergente.

Pour la suite, on admet que $\ds \int_{-\infty}^{+\infty} e^{-t^2} d t=\sqrt{\pi}$ et on note $\ds I_n=\int_{-\infty}^{+\infty} t^n e^{-t^2} d t$.

Q5. Établir à l'aide d'une intégration par parties que pour tout $\ds n$ élément de $\ds \mathbb{N}, I_{n+2}=\frac{n+1}{2} I_n$.

Q6. Montrer que pour tout $\ds p$ élément de $\ds \mathbb{N}, I_{2 p+1}=0$.

Q7. Montrer que pour tout $\ds p$ appartenant à $\ds \mathbb{N}, I_{2 p}=\frac{(2 p) !}{2^{2 p} p !} \sqrt{\pi}$.

## Partie II - Recherche des extrema

Soit $\ds F$ une fonction définie sur $\ds \mathbb{R}^2$ par : $\ds F(x, y)=\frac{1}{\sqrt{\pi}} \int_{-\infty}^{+\infty}(t-x)^2(t-y)^2 e^{-t^2} d t$.

Q8. Montrer que pour tout $\ds (x, y)$ appartenant à $\ds \mathbb{R}^2: F(x, y)=\frac{3}{4}+\frac{1}{2}\left(x^2+4 x y+y^2\right)+x^2 y^2$.

Q9. Calculer les dérivées partielles premières de $\ds F$ et en déduire les trois points critiques de $\ds F$.

Q10. Calculer pour tout $\ds x$ appartenant à $\ds \mathbb{R}, F(x, x)-F(0,0)$ et $\ds F(x,-x)-F(0,0)$.

Q11. Le point $\ds (0,0)$ est-il un extremum local?


## Partie III - Intégrale dépendant d'un paramètre

Q12. Pour tout $\ds x$ élément de $\ds \mathbb{R}$, montrer que les intégrales $\ds \int_0^{+\infty} \sin (x t) e^{-t^2} d t$ et $\ds \int_0^{+\infty} t \cos (x t) e^{-t^2} d t$ convergent.

Pour la suite, on note $\ds S(x)=\int_0^{+\infty} \sin (x t) e^{-t^2} d t$ et $\ds C(x)=\int_0^{+\infty} t \cos (x t) e^{-t^2} d t$.

Q13. Rappeler la formule de Taylor avec reste intégral sans oublier les hypothèses.

Q14. En appliquant la formule précédente à la fonction sin, montrer que pour tout $\ds (\lambda, a)$ éléments de $\ds \mathbb{R}^2,|\sin (\lambda+a)-\sin (a)-\lambda \cos (a)| \leq \frac{\lambda^2}{2}$.

Q15. Montrer que pour tout $\ds x$ appartenant à $\ds \mathbb{R}, \lim _{h \rightarrow 0} \frac{S(x+h)-S(x)}{h}-C(x)=0$.

Q16. En déduire que la fonction $\ds S$ est dérivable sur $\ds \mathbb{R}$ et donner sa dérivée.

Q17. Démontrer que pour tout $\ds x$ élément de $\ds \mathbb{R}, C(x)=\frac{1}{2}-\frac{x}{2} S(x)$ (on pourra effectuer une intégration par partie).

Q18. Donner une équation différentielle dont $\ds S$ est solution sur $\ds \mathbb{R}$.

Q19. Montrer que pour tout $\ds x$ appartenant à $\ds \mathbb{R}, S(x)=\frac{1}{2} e^{-\frac{x^2}{4}} \int_0^x e^{\frac{t^2}{4}} d t$.