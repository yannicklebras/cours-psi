<style>
h2 {text-align:center}
</style>
# Devoir no 5

Sujet etoilé : Vous pouvez faire, presque en entier [Centrale Maths PC1 2023](https://www.concours-centrale-supelec.fr/CentraleSupelec/2023/PC/M061.pdf).

Pour tout $(a, b) \in \mathbb{R}^2$, on pose

$$
M(a, b)=\left(\begin{array}{ccc}
a & -a & b \\
-a & a & b \\
b & b & 0
\end{array}\right) .
$$

L'ensemble des matrices de la forme $M(a, b)$ avec $(a, b) \in \mathbb{R}^2$ est noté $E$.
Dans les parties A et B, l'espace $\mathbb{R}^3$ est muni de son produit scalaire usuel.


## A. Généralités

1) Justifier que $E$ est un sous-espace vectoriel de $M_3(\mathbb{R})$. En donner une base et la dimension.

2) Pourquoi peut-on assurer sans calculs que $M(a, b)$ est diagonalisable ? Que peut-on dire de plus?

3) Déterminer le polynôme caractéristique de $M(a, b)$, puis, pour chacune des valeurs propres obtenues (non nécessairement distinctes), déterminer un vecteur propre associé, que l'on choisira unitaire, et de première composante positive.

4) En déduire que $M(a, b)=P DP^\top$, avec

$$
D=\left(\begin{array}{ccc}
2 a & 0 & 0 \\
0 & b \sqrt{2} & 0 \\
0 & 0 & -b \sqrt{2}
\end{array}\right) \text { et } P=\frac{1}{2}\left(\begin{array}{ccc}
\sqrt{2} & 1 & 1 \\
-\sqrt{2} & 1 & 1 \\
0 & \sqrt{2} & -\sqrt{2}
\end{array}\right) .
$$

## B. Matrices orthogonales de E

1) Déterminer, parmi les matrices $M(a, b)$ de $E$, celles qui sont orthogonales.

2) On note $A=\frac{1}{2}\left(\begin{array}{ccc}-1 & 1 & \sqrt{2} \\ 1 & -1 & \sqrt{2} \\ \sqrt{2} & \sqrt{2} & 0\end{array}\right)$.
Justifier que l'endomorphisme $\psi$ de $\mathbb{R}^3$, admettant $A$ pour matrice dans la base canonique, est une isométrie vectorielle.

En préciser la nature et les éléments caractéristiques.



## C. Construction de nouveaux produits scalaires sur $\mathbb{R}^3$

Etant donnés trois réels $\lambda, a$ et $b$, on pose $N=\lambda I_3+M(a, b)$.

Pour tous vecteurs $U=\left(\begin{array}{l}x \\ y \\ z\end{array}\right)$ et $V=\left(\begin{array}{l}x^{\prime} \\ y^{\prime} \\ z^{\prime}\end{array}\right)$ de $\mathbb{R}^3$, on pose alors $\phi(U, V)=U^\top N V$.

On souhaite déterminer une condition nécessaire et suffisante, portant sur $\lambda, a$ et $b$, pour que $\phi$ soit un produit scalaire sur $\mathbb{R}^3$.
Selon l'usage, on convient d'identifier une matrice carrée d'ordre 1 et son unique coefficient.

1) Sans déterminer explicitement $\phi(U, V)$, montrer que $\phi$ est une application à valeurs dans $\mathbb{R}$, bilinéaire et symétrique.

2) On pose $Z=P^\top U$ ( $P$ a été définie dans la partie A ), et on note $z_1, z_2$ et $z_3$ les composantes de $Z$, de sorte que $Z=\left(\begin{array}{c}z_1 \\ z_2 \\ z_3\end{array}\right)$.
Montrer que $\phi(U, U)=Z^\top\left(\lambda I_3+D\right) Z=(\lambda+2 a) z_1{ }^2+(\lambda+b \sqrt{2}) z_2{ }^2+(\lambda-b \sqrt{2}) z_3{ }^2$.

3) En déduire que si $\lambda>\max (-2 a,|b| \sqrt{2})$, alors $\phi$ est un produit scalaire sur $\mathbb{R}^3$.

4) Etudier la réciproque.

## D. Etude des points critiques d'une fonction de deux variables

Dans cette question, $a=-1, b=1$ et $\lambda=2$, de sorte que $N=2 I_3+M(-1,1)=\left(\begin{array}{lll}1 & 1 & 1 \\ 1 & 1 & 1 \\ 1 & 1 & 2\end{array}\right)$.

Pour tout couple de réels $x$ et $y$, on pose $U=\left(\begin{array}{c}x \\ x y \\ y\end{array}\right)$, puis $f(x, y)=\phi(U, U)=U^\top N U$.

1) Calculer explicitement $f(x, y)$, puis vérifiẹ que $f$ est de classe $C^1$ et admet exactement deux points critiques, que l'on précisera. (On pourra remarquer que $\frac{\partial f}{\partial x}(x, y)=2(1+y)(x+x y+y)$.)

2) A l'aide du résultat de C.2), montrer que l'un de ces points critiques - à préciser - correspond à un minimum global de $f$.

3) Vérifier que $f(-2+t,-1+t)-f(-2,-1)\sim_0-2 t^3$. Qu'en déduit-on pour l'autre point critique ?