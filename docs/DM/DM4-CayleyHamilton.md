<style>
h2 {text-align:center}
</style>


# Devoir Maison 4

Sujet etoilé : Vous pouvez faire, presque en entier [Centrale Maths 2 2023](https://www.concours-centrale-supelec.fr/CentraleSupelec/2023/PSI/M014.pdf).

## Exercice 1: Première démonstration de Cayley-Hamilton

$E$ est un espace vectoriel de dimension finie $n$ . $u$ est un endomorphisme de $E$. On note $\chi_u$ le polynôme caractéristique de $u$. On veut prouver que $\chi_u(u)=0$. Il suffit pour cela de prouver que pour tout $x$ non nul $\chi_u(u)(x)=0$. On supposera $n \geq 1$; pour $n=0$ le résultat est vrai (mais sans grand intérêt).

On fixe un élément $x$ non nul de $E$. On note $F$ le plus petit sous-espace stable par $u$ contenant $x$.

1) Montrer que $F$ existe bien.

2) Montrer qu'il existe un entier $p \geq 1$ tel que $\left(x, \ldots, u^{p-1}(x)\right)$ soit libre et $\left(x, \ldots, u^p(x)\right)$ liée.

3) Montrer que $F=\operatorname{Vect}\left\{x, \ldots, u^{p-1}(x)\right\}$.

4) Montrer que $\mathcal{B}=\left(x, \ldots, u^{p-1}(x)\right)$ est une base de $F$.

On note $\tilde{u}$ l'endomorphisme induit par $u$ sur $F$.

5) Quelle est la matrice de $\tilde{u}$ dans la base $\mathcal{B}$ si

$$
u^p(x)=a_0 x+a_1 u(x)+\cdots+a_{p-1} u^{p-1}(x) ?
$$

6) Calculer le polynôme caractéristique de $\tilde{u}$ noté $\chi_{\bar{u}}$.

7) Montrer que $\chi_{\bar{u}}(\tilde{u})(x)=0=\chi_{\bar{u}}(u)(x)$.

8) Montrer que $\chi_{\bar{u}}$ divise $\chi_u$.

9) Conclure.

## Exercice 2: Deuxième démonstration de Cayley-Hamilton
On se donne un espace vectoriel $E$ de dimension finie $n(\geq 1)$ et $u$ un endomorphisme de $E$, $\chi_u$ son polynôme caractéristique.

On suppose d'abord que le corps de base est $\mathbb{C}$. On peut alors démontrer que $u$ est trigonalisable sans utiliser le théorème de Cayley-Hamilton. En effet $\chi_u$ est scindé, il peut s'écrire

$$
\chi_u=\prod_{i=1}^n\left(X-\lambda_i\right) .
$$

Soit $\left(e_1, \ldots, e_n\right)$ une base dans laquelle la matrice $\left(m_{i, j}\right)$ de $u$ est triangulaire supérieure, avec pour tout $i$, $m_{i, i}=\lambda_i$.

On définit le drapeau $\left(F_0, \ldots, F_n\right)$ en posant $F_i=\operatorname{Vect}\left\{e_1, \ldots, e_i\right\}$.

10) Montrer que $\left(u-\lambda_i \operatorname{Id}_E\right)\left(F_i\right) \subset F_{i-1}$.

11) En déduire le théorème de Cayley-Hamilton pour les endomorphismes de $\mathbb C$-espaces vectoriels.

12) En déduire le théorème de Cayley-Hamilton dans $M_n(\mathbb{C})$ puis $M_n(\mathbb{R})$.

13) En déduire le théorème de Cayley-Hamilton pour les endomorphismes de $\mathbb{R}$-espaces vectoriels.