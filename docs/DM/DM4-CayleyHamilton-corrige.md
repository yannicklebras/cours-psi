<style>
h2 {text-align:center}
</style>


# Devoir Maison 4

Sujet etoilé : Vous pouvez faire, presque en entier [Centrale Maths 2 2023](https://www.concours-centrale-supelec.fr/CentraleSupelec/2023/PSI/M014.pdf).

## Exercice 1: Première démonstration de Cayley-Hamilton

$E$ est un espace vectoriel de dimension finie $n$ . $u$ est un endomorphisme de $E$. On note $\chi_u$ le polynôme caractéristique de $u$. On veut prouver que $\chi_u(u)=0$. Il suffit pour cela de prouver que pour tout $x$ non nul $\chi_u(u)(x)=0$. On supposera $n \geq 1$; pour $n=0$ le résultat est vrai (mais sans grand intérêt).

On fixe un élément $x$ non nul de $E$. On note $F$ le plus petit sous-espace stable par $u$ contenant $x$.

1) Montrer que $F$ existe bien.

!!! corrigé
    On remarque que l'espace $vect(x)$ est bien un sev contenant $x$. $u$ étant un endomorphisme de $E$, $E$ est un sev de $E$ contenant $x$ et stable par $u$. Ainsi, il existe bien des sev de $E$ stables par $u$ et contenant $x$ : il en existe donc un plus petit au sens de la dimension.

2) Montrer qu'il existe un entier $p \geq 1$ tel que $\left(x, \ldots, u^{p-1}(x)\right)$ soit libre et $\left(x, \ldots, u^p(x)\right)$ liée.

!!! corrigé
    $x$ étant non nul, la famille $x$ est libre. Ainsi, l'ensemble $\{k\in\mathbb N|(x,u(x),\dots,u^k(x))\text{ est libre}\}$ est un ensemble d'entiers non vide et majoré par la dimension de $E$. On en déduit qu'il admet un plus grand élément qu'on note $p-1$. Pour ce $p-1$ donné, on a bien $(x,u(x),\dots,u^{p-1}(x))$ qui est libre mais par maximalité, $(x,u(x),\dots,u^p(x))$ est liée. 

3) Montrer que $F=\operatorname{Vect}\left\{x, \ldots, u^{p-1}(x)\right\}$.

!!! corrigé
    Notons $G=vect(x,u(x),\dots,u^{p-1}(x))$, alors $G$ est bien un sev de $E$ stable par $u$ qui contient $x$. Soit $H$ un sev de $E$ stable par $u$ et contenant $x$, alors $H$ contient aussi $u^k(x)$ pour tout $k$ et donc en particulier la famille $(x,u(x),\dots,u^{p-1}(x))$ et par linéarité, $H$ contient $G$. Ainsi $G$ est un sev de $E$ stable par $u$ et contenant $x$, inclu dans tous les sev de $E$ stables par $u$ et contenant $x$. On en déduit que $G=F$. 

4) Montrer que $\mathcal{B}=\left(x, \ldots, u^{p-1}(x)\right)$ est une base de $F$.
!!! corrigé
    Cette famille est par définition donc génératrice de $F$. On montre qu'elle est libre. Soit $(\lambda_0,\dots,\lambda_{p-1})$ des scalaires tels que $\lambda_0x+\lambda_1u(x)+\dots+\lambda_{p-1} u^{p-1}(x)=0$. On compose $p-1$ fois par $u$ pour avoir $\lambda_0u^{p-1}(x)=0$. Or $u^{p-1}(x)\ne 0$ donc $\lambda_0=0$. On montre de même que les autres $\lambda_i$ sont nuls et donc que la famille est libre.

    Elle est libre maximale donc c'est une base.

On note $\tilde{u}$ l'endomorphisme induit par $u$ sur $F$.

5) Quelle est la matrice de $\tilde{u}$ dans la base $\mathcal{B}$ si

$$
u^p(x)=a_0 x+a_1 u(x)+\cdots+a_{p-1} u^{p-1}(x) ?
$$

!!! Corrigé
    La matrice de $u$ a alors la forme suivante : 
    
    $$
    \begin{pmatrix} 
    0 & 0 & \dots & 0     & a_0 \\
    1 & 0 & \dots & \dots & a_1 \\
    0 & 1 & 0     & \dots & a_2 \\
    \vdots & & \ddots & \ddots & \vdots\\
    0 & \cdots & \cdots & 1  & a_{p-1}
    \end{pmatrix}
    $$

6) Calculer le polynôme caractéristique de $\tilde{u}$ noté $\chi_{\tilde{u}}$.

!!! corrigé
    par un développement sucessif par rapport aux premières lignes, on trouve que $\chi_{\tilde u}=a_0+a_1X+\dots +a_{p-1}X^{p-1}$. 

7) Montrer que $\chi_{\tilde{u}}(\tilde{u})(x)=0=\chi_{\tilde{u}}(u)(x)$.

!!! corrigé
    On a alors $\chi_{\tilde u}(\tilde u)(x)=a_0 x+a_1 \tilde u(x)+\cdots+a_{p-1} {\tilde u}^{p-1}(x) ={\tilde u}^p(x)=0$. Et puisque $\tilde u(x)=u(x)$ quand $x\in F$, on a l'égalité avec $u$ aussi. 


8) Montrer que $\chi_{\tilde{u}}$ divise $\chi_u$.

!!! corrigé
    C'est une propriété de cours. 

9) Conclure.

!!! corrigé
    On en déduit que pour tout $x$, $\chi_u(x)=(P\chi_{\tilde u})(u)(x)=P(\chi_{\tilde u}(u)(x))=P(0)=0$ donc $u$ annule son polynôme caractéristique.

## Exercice 2: Deuxième démonstration de Cayley-Hamilton
On se donne un espace vectoriel $E$ de dimension finie $n(\geq 1)$ et $u$ un endomorphisme de $E$, $\chi_u$ son polynôme caractéristique.

On suppose d'abord que le corps de base est $\mathbb{C}$. On peut alors démontrer que $u$ est trigonalisable sans utiliser le théorème de Cayley-Hamilton. En effet $\chi_u$ est scindé, il peut s'écrire

$$
\chi_u=\prod_{i=1}^n\left(X-\lambda_i\right) .
$$

Soit $\left(e_1, \ldots, e_n\right)$ une base dans laquelle la matrice $\left(m_{i, j}\right)$ de $u$ est triangulaire supérieure, avec pour tout $i$, $m_{i, i}=\lambda_i$.

On définit le drapeau $\left(F_0, \ldots, F_n\right)$ en posant $F_i=\operatorname{Vect}\left\{e_1, \ldots, e_i\right\}$.

10) Montrer que $\left(u-\lambda_i \operatorname{Id}_E\right)\left(F_i\right) \subset F_{i-1}$.

!!! corrigé
    On sait que si $j<i$ alors $e_j\in F_{i-1}$. De plus $(u-\lambda_iId_E)(e_i)=u(e_i)-\lambda_ie_i=\sum_{k=1}^{i-1}\lambda_ke_k+\lambda_ie_i-\lambda_ie_i= \sum_{k=1}^{i-1}\lambda_ke_k\in F_{i-1}$. Donc $u(F_i)\subset F_{i-1}$.   

11) En déduire le théorème de Cayley-Hamilton pour les endomorphismes de $\mathbb C$-espaces vectoriels.

!!! corrigé
    D'après la question précédente, on a $\prod_{i=1}^n(u-\lambda_iId_E)(E)\subset \prod_{i=1}^{n-1}(u-\lambda_iId_E)(F_{n-1})=\dots =(u-\lambda_1)(F_1)\subset F_0=\{0\}$. Donc $u$ annule son polynôme caractéristique. 

12) En déduire le théorème de Cayley-Hamilton dans $M_n(\mathbb{C})$ puis $M_n(\mathbb{R})$.

!!! corrigé
    On en déduit directement le théorème de Cayley-Hamilton dans $M_n(\mathbb C)$. Pour $M_n(\mathbb R)$, On considère une matrice réelle $M$. Son polynôme caractéristique est à coefficients réels. En tant que matrice de $M_n(\mathbb C)$, elle annule ce polynôme caractéristique. Mais les calculs restent dans $\mathbb R$ donc $M$ annule son polynôme caractéristique en tant que matrice de $M_n(\mathbb R)$.  

13) En déduire le théorème de Cayley-Hamilton pour les endomorphismes de $\mathbb{R}$-espaces vectoriels.

!!!corrigé
    Soit $u$ un $\mathbb R$ espace vectoriel, soit $A$ sa matrice dans la base canonique, réelle. $A$ annule son polynôme caractéristique, réel, et donc par propriété des représentations matricielles, $u$ annule son polynôme caractéristique. 