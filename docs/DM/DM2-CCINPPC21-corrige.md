<style>
h2 {text-align:center}
</style>


# Devoir Maison 1

Les DM sont à rendre le vendredi de la semaine où vous n'avez pas colle de maths. Merci de les rendre en groupes de 2 ou trois.


# Résolution d'une équation fonctionnelle


Dans cet exercice, on souhaite déterminer les fonctions $f:] 0,+\infty[\rightarrow \mathbb{R}$ vérifiant les relations :
$$
\left.\lim _{x \rightarrow+\infty} f(x)=0 \quad \text { et } \quad \forall x \in\right] 0,+\infty\left[, \quad f(x+1)+f(x)=\frac{1}{x^2} .\right.
$$

## Partie I - Existence et unicité de la solution du problème (P)


Dans cette partie, on démontre que le problème $(\mathrm{P})$ admet une unique solution et on détermine une expression de celle-ci sous la forme d'une série de fonctions.


### Existence de la solution

Pour tout $k \in \mathbb{N}$, on définit la fonction $\left.\varphi_k:\right] 0,+\infty[\rightarrow \mathbb{R}$ par :

$$
\forall x \in] 0,+\infty[, \quad \varphi_k(x)=\frac{(-1)^k}{(x+k)^2} .
$$

Q1. Montrer que la série de fonctions $\sum_{k \ge 0} \varphi_k$ converge simplement sur $] 0,+\infty[$.

!!! corrigé
    Soit $x\in]0,+\infty[$, on a pour tout entier $k$, $|\varphi_k(x)|\le\frac 1{k^2}$ qui est le terme général d'une série convergente. Alors par comparaison des séries à termes positifs, $la série $\sum\varphi_k$ est absolument convergente donc convergente. Il y a donc convergence simple de la série de fonctions sur $]0,+\infty[$. 

Dans tout le reste de cet exercice, on note $\varphi:] 0,+\infty[\rightarrow \mathbb{R}$ la somme de la série $\sum_{k \geqslant 0} \varphi_k$.

Q2. Montrer que pour tout $x \in] 0,+\infty[$, on a $\varphi(x+1)+\varphi(x)=\frac{1}{x^2}$.

!!! corrigé
    Soit $x\in]0,+\infty[$, on peut passer par les sommes partielles en remarquant que pour tout entier naturel $k$, $\varphi_k(x+1)=\frac{(-1)^k}{(x+1+k)^2}=-\frac{(-1)^{k+1}}{(x+(k+1))^2}=-\varphi_{k+1}(x)$. On a donc $\sum_{k=0}^{N}\varphi_k(x+1)=-\sum_{k=0}^{N}\varphi_{k+1}(x)=-\sum_{k=1}^{N+1}\varphi_k(x)=-\sum_{k=0}^{N+1}\varphi_k(x)+\varphi_0(x)$ et en faisant tendre $N$ vers $+\infty$, ce que l'on peut faire grâce à la convergence simple, on obtient $\varphi(x+1)=-\varphi(x)+\frac 1{x^2}$. 

Q3. En utilisant le théorème spécial des séries alternées, montrer que :

$$
\forall x \in] 0,+\infty[, \quad \forall n \in \mathbb{N}, \quad\left|\sum_{k=n+1}^{+\infty} \varphi_k(x)\right| \leqslant \frac{1}{(x+n+1)^2}.
$$

!!! corrigé
    Pour $x\in]0,+\infty[$, la série $\sum\frac {(-1)^k}{(x+k)^2}$ est alternée (à vérifier) et donc d'après le CSSA en notant $R_n$ le reste d'ordre $n$, $|R_n(x)| \le \frac{1}{(x+n+1)^2}$.

Q4. Montrer que la fonction $\varphi$ est une solution de (P).

!!! corrigé 
    On applique la majoration précédente avec $n=0$ : $|R_0(x)|\le \frac 1{(x+1)^2}$. Or $R_0(x)=\varphi(x)-\varphi_0(x)=\varphi(x)-\frac 1{x^2}$ Par encadrement on a donc $\R_0(x)\xrightarrow[x\to+\infty]{}0$. Or $\frac 1{x^2}\xrightarrow[x\to+\infty]{}0$ donc $\varphi(x)\xrightarrow[x\to+\infty]{}0$. 

### Unicité de la solution

Q5. Montrer que si $f:] 0,+\infty[\rightarrow \mathbb{R}$ est une solution de (P), alors pour tout $n \in \mathbb{N}$, on a :

$$
\forall x \in] 0,+\infty[, \quad f(x)=(-1)^{n+1} f(x+n+1)+\sum_{k=0}^n \frac{(-1)^k}{(x+k)^2} .
$$

!!! corrigé
    Soit $f$ une solution de $(P)$ sur $]0,+\infty[$. On a alors pour $x\in]0,+\infty[$ et pour $k\in\mathbb N$, $f(x+k+1)+f(x+k)=\frac 1{(x+k)^2}$. Posons $u_k=(-1)^kf(x+k)$, alors la relation précédente donne $-u_{k+1}+u_k=\frac{(-1)^k}{(x+k)^2}$. On somme alors ces égalités pour $k$ entre $0$ et $n$ et on obtient par télescopage : $u_0-u_{n+1}=\sum_{k=0}^{n}\frac{(-1)^k}{(x+k)^2}$, c'est à dire $f(x)-(-1)^{n+1}f(x+n+1)=\sum_{k=0}^n\frac{(-1)^k}{(x+k)^2}$. 

Q6. En déduire que la fonction $\varphi$ est l'unique solution de (P).

!!! corrigé
    Or la limite de $f$ en $+\infty$ vaut $0$. Donc en faisant tendre $n$ vers $+\infty$, on obtient $f(x)=0+\sum_{k=0}^{+\infty}\frac{(-1)^k}{(x+k)^2}=\varphi(x)$. Donc $\varphi$ est la seule solution au problème $(P)$. 

## Partie II - Étude de la solution du problème (P)

Dans cette partie, on étudie quelques propriétés de l'unique solution $\varphi:] 0,+\infty[\rightarrow \mathbb{R}$ du problème (P).

Q7. Soit $\varepsilon>0$. Montrer que la série de fonctions $\sum_{k \ge 0} \varphi_k$ converge uniformément sur $[\varepsilon,+\infty[$.

!!! corrigé
    Soit $x\in[\varepsilon,+\infty[$ et $k$ un entier naturel, alors $\varphi_k(x)\le \varphi_k(\varepsilon)$. On en déduit que $R_n(x)\le \sum_{k=n+1}^{+\infty}\varphi_k(\varepsilon)$ et donc $\sup_{[\varepsilon,+\infty[}|R_n|\le |R_n(\varepsilon)|\to 0$. On en déduit que la suite des restes converge uniformément vers $0$ et donc que la série de fonctions $\sum varphi_k$ converge uniformément vers $\varphi$ sur $[\varepsilon,+\infty[$.  

Q8. Montrer que la fonction $\varphi$ est continue sur $] 0,+\infty[$. En utilisant le fait que $\varphi$ est une solution du problème (P), en déduire un équivalent simple de $\varphi$ au voisinage de $0^{+}$.

!!! corrigé
    D'après le théorème de continuité des séries de fonctions, $\varphi$ est donc continue sur tout $[\varepsilon,+\infty[$ et donc sur $]0,+\infty[$. On a alors $\varphi(x)=\frac 1{x^2}-\varphi(x+1)=\frac1{x^2}(1+x^2\varphi(x+1))$. Or $1+x^2\varphi(x+1)\to 1$ donc $\varphi(x)\sim_{x\to 0^+}\frac 1{x^2}$. 

Q9. Justifier que la fonction $\varphi$ est dérivable sur $] 0,+\infty[$ et que l'on a :

$$
\forall x \in] 0,+\infty[, \quad \varphi^{\prime}(x)=\sum_{k=0}^{+\infty} \frac{2(-1)^{k+1}}{(x+k)^3} .
$$

!!! corrigé
    La série de fonctions $\sum\varphi_k$ converge uniformément vers $\varphi$ sur tout $[\varepsilon,+\infty[$. Or les $\varphi_k$ sont dérivables sur $[\varepsilon,+\infty[$. Par théorème de dérivation, $\varphi$ est dérivable sur tout $[\varepsilon,+\infty[$ et donc sur $]0,+\infty[$ et $\varphi'(x)=\sum_{k=0}^{+\infty}\varphi_k'(x)=\sum_{k=0}^{+\infty}\frac{2\times(-1)^{k+1}}{(x+k)^3}$. 

Q10. En déduire que la fonction $\varphi$ est décroissante sur $] 0,+\infty[$.

!!! corrigé
    D'après la majoration du reste $R_0$ qui découle du CSSA, on a $\varphi'(x)=\frac {-1}{x^3}+R_0(x)\le\frac{-1}{x^3}+\frac{1}{(x+1)^3}\le 0$. Ainsi $\varphi'$ est négative et $\varphi$ est dérivable sur $]0,+\infty[$ : $\varphi$ est donc décroissante sur $]0,+\infty[$. 

Q11. En utilisant le résultat de la question précédente et la relation $(\mathrm{P})$, montrer que :

$$
\forall x \in] 1,+\infty[, \quad \frac{1}{x^2} \leqslant 2 \varphi(x) \leqslant \frac{1}{(x-1)^2} .
$$

En déduire un équivalent de $\varphi$ en $+\infty$.

!!! corrigé
    On a alors pour $x\in]1,+\infty[$, $\varphi(x+1)\le\varphi(x)$ et donc $\varphi(x)+\varphi(x+1)\le 2\varphi(x)$ et $\frac 1{x^2}\le 2\varphi(x)$. De même, $\varphi(x-1)+\varphi(x)\ge2\varphi(x)$ c'est à dire $\frac1{(x-1)^2}\ge 2\varphi(x)$. On en l'inégalité voulue.

    Par conservation de l'équivalent dans une inégalité, on a donc $2\varphi(x)\sim_{+\infty}\frac 1{x^2}$ et donc $\varphi(x)\sim_{+\infty}\frac 1{2x^2}$.

## Partie III - Expression intégrale de la solution du problème (P) (hors programme pour le moment)

Dans cette partie, on détermine une expression de $\varphi$ sous la forme d'une intégrale. On considère un élément $x \in] 0,+\infty[$.

Q12. Pour tout $k \in \mathbb{N}$, montrer que la fonction $t \mapsto t^{x+k-1} \ln (t)$ est intégrable sur ]0,1] et que l'on a :

$$
\int_0^1 t^{x+k-1} \ln (t) \mathrm{d} t=-\frac{1}{(x+k)^2} .
$$

!!! corrigé
    La fonction est prolongeable par continuité en $0$ pour $k\ge 1$. Et pour $k=0$, $t^{x-1}\ln(t)=o(\frac1{t^{1-\frac x2}})$ et $t\mapsto \frac{1}{t^{1-\frac x2}}$ est intégrable en $0$ donc la fonction $t\mapsto t^{x-1}\ln(t)$ est intégrable en $0$. On a alors par intégration par parties :

    $$
    \int_0^1 t^{x+k-1} \ln (t) \mathrm{d} t=-\frac{1}{(x+k)^2} .
    $$

Q13. En déduire que la fonction $t \mapsto \frac{t^{x-1} \ln (t)}{1+t}$ est intégrable sur $] 0,1]$ et que :

$$
\varphi(x)=-\int_0^1 \frac{t^{x-1} \ln (t)}{1+t} \mathrm{d} t .
$$

!!! corrigé
    Soit $x\in]0,1]$, posons $f_k(t)=t^{x+k-1}\ln(t)$. Si $k>1$, alors on peut prolonger $f_k$ par continuité et considérer $f_k$ comme continue. La série de fonction $\sum_{k\ge 1} f_k(t)$ converge uniformément vers $\frac{t^x}{1-x}\ln(t)$. On peut donc appliquer le théorème d'intervation série intégrale : 
    $\int_0^1\frac{t^x}{1-x}\ln(t) dt=\sum_{k=1}^{+\infty}\frac1{(x+k)^2}=\varphi(x)-\frac 1{x^2}$. Or $\frac 1{x^2}=\int_{0}^1t^{x-1}\ln(t) dt$. Donc $\varphi(x)=\int_0^1\frac{t^x}{1-x}\ln(t)+t^{x-1}\ln(t) dt=\int_0^1\frac{t^{x-1}}{1-x}\ln(t) dt$.
