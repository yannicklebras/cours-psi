<style>
h2 {text-align:center}
</style>

# Devoir Maison 1
Les DM sont à rendre le vendredi de la semaine où vous n'avez pas colle de maths. Merci de les rendre en groupes de 2 ou trois.

Dans tout le problème, on considère les suites $\left(H_n\right)_{n \geqslant 1}$ et $\left(u_n\right)_{n \geqslant 1}$ définies pour tout entier naturel $n$ non nul, par :

$$
H_n=\sum_{k=1}^n \frac{1}{k} \text { et } u_n=H_n-\ln (n)
$$


## Partie 1

1. Établir pour tout entier naturel $k$ non nul, l'encadrement suivant :

    $$
    \frac{1}{k+1} \leqslant \ln (k+1)-\ln (k) \leqslant \frac{1}{k}
    $$

    !!! corrigé
        on utilise les relations classiques sur le logarithme, notamment : pour tout $x>-1$, $\ln(1+x)\le x$. On a alors ici $\ln(k+1)-\ln(k)=\ln(1+\frac 1k)\le \frac 1k$. De plus, $\ln(k+1)-\ln(k)=-\ln(1-\frac 1{k+1})\ge \frac 1{k+1}$ en faisant attention aux signes. On a alors l'inégalité voulue. 

2. a. Quelle est la limite de la suite $\left(H_n\right)$ ?
    
    !!! corrigé
        On somme alors l'inégalité de droite de $1$ à $n$ : $\ln(n+1)-\ln(1)\le\sum_{k=1}^{n}\frac 1k$. On en déduit que $H_n$ est minoré par $\ln(n+1)$ et donc que $H_n$ diverge vers $+\infty$. 
    
    b. En utilisant le résultat de la question 1, montrer pour tout entier naturel $n$ non nul, l'encadrement suivant :
    
    $$
    \ln (n)+\frac{1}{n} \leqslant H_n \leqslant \ln (n)+1
    $$

    !!! corrigé
        Comme précédemment, on somme de $1$ à $n-1$ à gauche : $\sum_{k=1}^{n-1}\frac 1{k+1}\le \ln(n)-\ln(1)$ ce qui donne $H_n-1\le \ln(n)$, c'est à dire $H_n\le \ln(n)+1$. On somme ensuite aussi de $1$ à $n-1$ à droite : $\ln(n)-\ln(1)\le \sum_{k=1}^{n-1}\frac 1k=H_n-\frac 1n$. On en déduit l'encadrement demandé.

    c. En déduire un équivalent simple de $H_n$ quand $n$ tend vers $+\infty$.
    
    !!! corrigé
        Par conservation de l'équivalent par encadrement, on a donc $H_n\sim\ln(n)$. 

3. a. En utilisant à nouveau l'encadrement obtenu à la question 1 , montrer que la suite $\left(u_n\right)_{n \geqslant 1}$ est décroissante.
    
    !!! corrigé
        Calculons $u_{n+1}-u_n = \frac H_{n+1}-H_n-\ln(n+1)+\ln(n)=\frac 1{n+1}-\ln(n+1)+\ln(n)\le 0$ d'après l'encadrement de la question 1.
    
    b. En déduire que cette suite est convergente; on note $\gamma$ sa limite. Montrer que $\gamma$ appartient à $[0,1]$.
    
    !!! corrigé
        La suite $u_n$ est donc décroissante et minorée par $\frac 1n$ donc positive. Ainsi par théorème de convergence monotone, cette suite est convergente, vers une limite qu'on note $\gamma$ (constante d'Euler). De plus, $\frac 1n\le u_n\le 1$ donc par passage à la limite dans l'inégalité, $\gamma\in[0,1]$.  

4. Soit $f$ une fonction définie et deux fois dérivable sur $\mathbb{R}_{+}^*$, dont les dérivées première et seconde sont notées respectivement $f^{\prime}$ et $f^{\prime \prime}$. On suppose que $f^{\prime \prime}$ est continue sur $\mathbb{R}_{+}^*$.  
On pose pour tout entier naturel $k$ non nul :  

    $$
    J_k=\frac{1}{2} \int_k^{k+1}\left(t-k-\frac{1}{2}\right)^2 f^{\prime \prime}(t) \d t
    $$

    a. Établir pour tout entier naturel $k$ non nul, l'égalité suivante :
    
    $$
    J_k=\frac{f^{\prime}(k+1)-f^{\prime}(k)}{8}-\frac{f(k+1)+f(k)}{2}+\int_k^{k+1} f(t) d t
    $$

    !!! corrigé
        Il s'agit de faire des intégrations par parties successives : 

        $$
        \begin{aligned}
        J_k &= \frac 12\int_{k}^{k+1}\left(t-k-\frac{1}{2}\right)^2 f''(t) \d t\\
        &= \frac 12\left(\left[(t-k-\frac{1}{2})^2f'(t)\right]_k^{k+1}-2\int_{k}^{k+1}(t-k-\frac{1}{2})f'(t)\d t\right)\\
        &= \frac{f'(k+1)-f'(k)}{8}-\int_{k}^{k+1}(t-k-\frac{1}{2})f'(t)\d t\\
        &= \frac{f'(k+1)-f'(k)}{8}-\left[(t-k-\frac{1}{2})f(t)\right]_k^{k+1}+\int_{k}^{k+1}f(t)\d t\\
        &= \frac{f'(k+1)-f'(k)}{8}-\frac{f(k+1)+f(k)}{2}+\int_{k}^{k+1}f(t)\d t\\
        \end{aligned}
        $$
    
        D'où le résultat demandé.

    b. En déduire pour tout entier naturel $n$ non nul, la relation suivante :
    
    $$
    \sum_{k=1}^n f(k)=\frac{f(1)+f(n)}{2}+\frac{f^{\prime}(n)-f^{\prime}(1)}{8}+\int_1^n f(t) d t-\sum_{k=1}^{n-1} J_k
    $$
    
    !!! corrigé
        Pour obtenir la relation demandée, il suffit de sommer l'égalité précédente entre $1$ et $n-1$. 

5. On suppose dans cette question que la fonction $f$ est définie sur $\mathbb{R}_{+}^*$ par : $f(x)=\frac{1}{x}$.

    a. Établir pour tout entier naturel $k$ non nul, la double inégalité suivante $: 0 \leqslant J_k \leqslant \int_k^{k+1} \frac{1}{4 t^3} d t$.

    !!! corrigé
        On revient à l'expression de $J_k$. On a ici $f''(t)=\frac2{x^3}$ et pour $t\in[k,k+1]$, $|t-k-\frac 12|\le \frac 12$. Donc $0\le (t-k-\frac 12)^2f(t)\le \frac 1{2t^3}$. On en déduit en intégrant de $k$ à $k+1$ que $0\le J_k\le \int_{k}^{k+1}\frac 1{4t^3}\d t$.   


    b. En déduire que la série de terme général $J_k$ est convergente.

    !!! corrigé
        La fonction $t\mapsto 1{t^3}$ est décroissante, donc $0\le J_k\le \frac 1{4k^3}$ et donc par comparaison des séries à termes positifs à une série de Riemann convegente, $\sum J_k$ est convergente.  

    c. En déduire également, pour tout entier naturel $n$ non nul, l'encadrement suivant : $0 \leqslant \sum_{k=n}^{+\infty} J_k \leqslant \frac{1}{8 n^2}$.
    
    !!! corrigé
        Sommons l'inégalité entre $n$ et $N$ : $0\le \sum_{k=n}^N J_k \le \int_n^{N+1}\frac 1{4t^3}\d t=\frac 18(\frac{1}{n^2}-\frac 1{(N+1)^2}$. La série des $J_k$ étant convergente, on peut faire tendre $N$ vers $+\infty$ et obtenir un encadrement du reste : $0\le \sum_{k=n}^{+\infty} J_k\le\frac 1{8n^2}$.


    d. Prouver l'existence d'une suite convergente $\left(\varepsilon_n\right)_{n \geqslant 1}$, de limite nulle, telle que l'on ait pour tout entier naturel $n$ non nul :

    $$
    H_n=\ln (n)+\gamma+\frac{1}{2 n}+\frac{\varepsilon_n}{n}
    $$

    !!! corrigé
        Si on reprend alors l'égalité de la question 4b) dans le cas $f(t)=\frac 1t$, on peut écrire : 

        $$
        \begin{aligned}
        H_n&=\frac 12+\frac 1{2n}-\frac 1{8n^2}+\frac 18+\ln(n)-\sum_{k=1}^{n-1}J_k\\
        &=\ln(n)+\gamma +\frac 1{2n}+O\left(\frac 1{n^2}\right)\\
        &=\ln(n)+\gamma+\frac 1{2n}+\frac {\varepsilon_n}{n}
        \end{aligned}
        $$

        avec $\varepsilon_n\to 0$ et $\gamma = \frac 12+\frac 18+\sum_{k=1}^{+\infty}J_k$. 

    ## Partie 2

6. Soit $\left(a_n\right)_{n \in \mathbb{N}}$ et $\left(b_n\right)_{n \in \mathbb{N}}$ deux suites de réels strictement positifs vérifiant les deux conditions suivantes :  
    (i) la série de terme général $a_n$ est convergente;  
    (ii) $a_n \underset{n \rightarrow+\infty}{\sim} b_n$ ($a_n$ est équivalent à $b_n$ lorsque $n$ tend vers $+\infty$).  
    
    a. Soit $\varepsilon$ un réel strictement positif. Justifier l'existence d'un entier naturel $n_0$ tel que :

    $$
    \forall n \geqslant n_0, \quad\left|a_n-b_n\right| \leqslant \varepsilon b_n .
    $$

    !!! corrigé
        Le rapport $\frac {a_n}{b_n}$ converge vers $1$. Par définition de la limite, il existe donc un rang $n_0$ tel que si $n\ge n_0$, alors $|\frac {a_n}{b_n}-1|\le \varepsilon$. On a alors, puisque $b_n$ est strictement positif : $|a_n-b_n|\le \varepsilon b_n$. 



    b. En déduire pour tout entier naturel $n$ supérieur ou égal à $n_0$, l'inégalité suivante :

    $$
    \left|\sum_{k=n+1}^{+\infty} a_k-\sum_{k=n+1}^{+\infty} b_k\right| \leqslant \varepsilon \sum_{k=n+1}^{+\infty} b_k
    $$

    !!! corrigé
        On applique simplement l'inégalité triangulaire (on remarque que par théorème de comparaison des séries à termes positifs, $\sum b_n$ est convergente) : 

        $$
        \begin{aligned}
        |\sum_{k=n+1}^{+\infty}a_k-\sum_{k=n+1}^{+\infty}b_k|&=|\sum_{k=n+1}^{+\infty}a_k-b_k|\\
        &\le\sum_{k=n+1}^{+\infty}|a_k-b_k|\\
        &\le\varepsilon\sum_{k=n+1}^{+\infty}b_k
        \end{aligned}
        $$

    c. Établir l'équivalence suivante :

    $$
    \sum_{k=n+1}^{+\infty} a_k \underset{n \rightarrow+\infty}{\sim} \sum_{k=n+1}^{+\infty} b_k
    $$

    !!! corrigé
        Par définition de l'équivalence, on a bien $\sum_{k=n+1}^{+\infty}a_k\sim \sum_{k=n+1}^{+\infty}b_k$.


7. Soit $\alpha$ un réel strictement supérieur à 1 .

    a. Montrer que pour tout entier $k$ supérieur ou égal à 2 , on a :
    
    $$
    \int_k^{k+1} \frac{1}{t^\alpha} d t \leqslant \frac{1}{k^\alpha} \leqslant \int_{k-1}^k \frac{1}{t^\alpha} d t
    $$

    !!! corrigé
        La fonction $t\mapsto 1{t^\alpha}$ est décroissante si $\alpha>1$. Ainsi pour tout $t\in[k,k+1]$, on a $\frac 1{t^\alpha}\le 1{k^\alpha}$ et en intégrant entre $k$ et $k+1$, il vient $\int_{k}^{k+1}\frac 1{t^\alpha}\d t\le\frac1{k^\alpha}$. De la même manière, pour $t\in[k-1,k]$, $\frac 1{k^\alpha}\le \frac 1{t^\alpha}$ et donc en intégrant entre $k-1$ et $k$, $\frac 1{k^\alpha}\le \in_{k-1}^k\frac 1{t^\alpha}\d t$. On a ainsi l'encadrement souhaité.

    b. En déduire pour tout entier naturel $n$ non nul et pour tout entier $N$ strictement supérieur à $n$, la double inégalité suivante :

    $$
    \int_{n+1}^{N+1} \frac{1}{t^\alpha} d t \leqslant \sum_{k=n+1}^N \frac{1}{k^\alpha} \leqslant \int_n^N \frac{1}{t^\alpha} d t
    $$

    !!! corrigé
        En sommant pour $k$ allant de $n+1$ jusqu'à $N$, on obtient cette inégalité.

    c. Établir l'équivalence suivante :
    
    $$
    \sum_{k=n+1}^{+\infty} \frac{1}{k^\alpha} \underset{n \rightarrow+\infty}{\sim} \frac{1}{\alpha-1} \times \frac{1}{n^{\alpha-1}}
    $$

    !!! corrigé
        On calcule les intégrales : $ \int_n^N\frac 1{t^\alpha} \d t=\frac 1{\alpha-1}\left(\frac 1{n^{\alpha -1}}-\frac 1{N^{\alpha -1}}\right)\xrightarrow[N\to+\infty]{}\frac 1{\alpha-1}\times\frac 1{n^{\alpha -1}}$. Pour l'intégrale de gauche, on obtient $\frac 1{\alpha-1}\times\frac 1{(n+1)^{\alpha -1}}$. On en déduit ainsi par passage à la limité dans l'inégalité (en $N\to+\infty$) que :

        $$
        \frac 1{\alpha-1}\times\frac 1{(n+1)^{\alpha -1}}\le \sum_{k=n+1}^{+\infty} \frac 1{k^{\alpha}}\le \frac 1{\alpha-1}\times\frac 1{(n)^{\alpha -1}}
        $$

        et par conservation de l'équivalent par encadrement, $\sum_{k=n+1}^{+\infty}\frac 1{k^{\alpha}}\sim\frac 1{\alpha -1}\times\frac 1{n^{\alpha -1}}$. 


    ## Partie 3

    On considère les suites $\left(x_n\right)_{n \geqslant 1}$ et $\left(y_n\right)_{n \geqslant 2}$ définies par :

    $$
    \forall n \geqslant 1, x_n=u_n-\frac{1}{2 n} \text { et } \forall n \geqslant 2, y_n=x_n-x_{n-1} .
    $$

8. a. Quelle est la limite de la suite $\left(x_n\right)_{n \geqslant 1}$ ?

    !!! corrigé
        D'après le résultat de la question 5d), $x_n$ converge vers $\gamma$. 


    b. Justifier pour tout entier naturel $n$ non nul, l'égalité suivante :
    
    $$
    \gamma-x_n=\sum_{k=n+1}^{+\infty} y_k .
    $$

    !!! corrigé 
        La somme des $y_k$ est une somme télescopique : on a $\sum_{k=n+1}^N y_k=x_N-x_n\xrightarrow[N\to+\infty]{}\gamma-x_n$. 

    c. En déduire pour tout entier naturel $n$ non nul, l'égalité suivante :
    
    $$
    \gamma-x_n=\frac{1}{2} \sum_{k=n+1}^{+\infty}\left(\frac{1}{k}+\frac{1}{k-1}+2 \ln \left(1-\frac{1}{k}\right)\right)
    $$

    !!! corrigé
        On a $x_n-x_{n-1}=H_n-\ln(n)- \frac 1{2n}-H_{n-1}+\ln(n-1)+\frac 1{2n-2}=\frac1{2n}+\ln(1-\frac 1n)+\frac 1{2(n-1)}=\frac 12\left(\frac 1n+\frac 1{n-1}-2\ln(1-\frac 1n\right)$ et on remplace dans la somme.


9. a. Montrer qu'il existe une suite $\left(\varepsilon_k^{\prime}\right)_{k \geqslant 1}$ convergente de limite nulle vérifiant :
    
    $$
    \frac{1}{k-1}=\frac{1}{k}+\frac{1}{k^2}+\frac{1}{k^3}+\frac{\varepsilon_k^{\prime}}{k^3}
    $$

    !!! corrigé
        On peut par exemple passer par un DL : $\frac 1{k-1}=\frac 1k\left(1+\frac 1k+\frac 1{k^2}+o(\frac 1{k^2})\right)=\frac 1k+\frac 1{k^2}+\frac 1{k^3}+o(\frac 1{k^3})$. Et la définition du $o()$, c'est exactement l'existence d'une d'une suite $\varepsilon'_k$ de limite nulle telle que $\frac 1{k-1}=\frac 1k+\frac 1{k^2}+\frac 1{k^3}+\frac{\varepsilon'_k}{k^3}$.

    b. Établir à l'aide d'un développement limité à l'ordre 3, l'équivalence suivante :

    $$
    \frac{1}{k}+\frac{1}{k-1}+2 \ln \left(1-\frac{1}{k}\right) \underset{k \rightarrow+\infty}{\sim} \frac{1}{3 k^3}
    $$

    !!! corrigé
        On a $\frac 1k+\frac 1{k-1}+2\ln(1-\frac 1k)=\frac 1k+\frac 1{k-1}+2\left(-\frac 1k-\frac 1{2k^2}-\frac 1{k^3}+o(\frac 1{k^3})\right)=-\frac 1k-\frac 1{k^2}-\frac 2{3k^3}+\frac 1{k-1}+o(\frac 1{k^3})=\frac 1{k^3}-\frac 2{3k^3}+o(\frac 1{k^3})=\frac 1{3k^3}+o(\frac 1{k^3})\sim\frac 1{3k^3}$. 

10. En utilisant les résultats précédents, en déduire l'existence d'une suite convergente $\left(\varepsilon_n^{\prime \prime}\right)_{n \geqslant 1}$, de limite nulle, vérifiant pour tout entier naturel $n$ non nul :
    
    $$
    H_n=\gamma+\ln (n)+\frac{1}{2 n}-\frac{1}{12 n^2}+\frac{\varepsilon_n^{\prime \prime}}{n^2}
    $$

    !!! corrigé
        D'après les questions $6$ et $7$, on peut alors affirmer que $\sum_{k=n+1}^{+\infty} \frac 1k+\frac 1{k-1}+2\ln(1-\frac 1k)\sim\sum_{k=n+1}^{+\infty}\frac 1{3k^3}\sim\frac 1{3-1}\times\frac 1{3n^2}\sim1{6n^2}$. On en déduit donc que $\gamma-x_n\sim \frac 1{12n^2}$ et par conséquent $H_n=\ln(n)+\gamma+\frac 1{2n}-\frac 1{12n^2}+o(\frac 1{n^2})$. 