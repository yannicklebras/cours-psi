# e3a 2017 - PSI2 

Dans tout le problème, on se donne $n \geq 2$ un entier et on note

- $\mathcal{E}=\mathcal{M}_{n}(\mathbb{R})$ l'ensemble des matrices carrées de taille $n$ à coefficients réels

- $O_{n}$ la matrice nulle de $\mathcal{E}$ et $I_{n}$ la matrice identité

- ${ }^{t} A$ la transposée d'un élément de $\mathcal{E}$

- $E_{i, j} \in \mathcal{E}$ la matrice dont tous les coefficients sont nuls sauf celui de la ligne $i$ et de la colonne $j$

- $\mathcal{S}_{n}(\mathbb{R})$ l'ensemble des matrices symétriques de $\mathcal{E}$

- $\mathcal{N}$ l'ensemble des matrices nilpotentes de $\mathcal{E}$, c'est à dire des $A \in \mathcal{E}$ telles qu'il existe un entier $p$ avec $A^{p}=O_{n}$.


## Questions de cours

1) Quelle est la dimension de $\mathcal{E}$ ? En donner sans justification une base.

2) Soient $i, j, k, \ell \in[|1, n|]$. calculer le produit des matrices $E_{i, j}$ et $E_{k, \ell}$. On montrera en particulier que ce produit est nul lorsque $j \neq k$.

3) Enoncer le théorème de Cayley-Hamilton.

4) Donner une condition nécessaire et suffisante pour que $M \in \mathcal{M}_{n}(\mathbb{K})(\mathbb{K}=\mathbb{R}$ ou $\mathbb{C}$ ) soit trigonalisable dans $\mathcal{M}_{n}(\mathbb{K})$.

## 1 Propriétés élémentaires

Soit $A$ un élément de $\mathcal{N}$


5) La matrice $A$ peut-elle être inversible? Justifier votre réponse.

6) On note $\operatorname{Sp}(A)$ le spectre de $A$, c'est à dire l'ensemble des valeurs propres complexes de la matrice $A$. Déterminer $\operatorname{Sp}(A)$ et donner le polynôme caractéristique de $A$.

7) Donner une condition nécessaire et suffisante pour que $A$ soit diagonalisable.

8) Montrer que le sous-espace vectoriel de $\mathcal{E}$ engendré par $A$, noté $\operatorname{Vect}(A)$, est inclus dans $\mathcal{N}$.

9) Vérifier que ${ }^{t} A \in \mathcal{N}$.

10) Montrer que si $M$ est semblable à $A$, alors $M \in \mathcal{A}$.

11) Montrer que $A^{n}=O_{n}$.

12) En déduire qu'une condition nécessaire et suffisante pour que $M \in \mathcal{E}$ soit nilpotente est que $M^{n}=O_{n}$.

On pourra admettre ce résultat et l'utiliser dans la suite du problème.

13) Montrer que $A$ est trigonalisable dans $\mathcal{M}_{n}(\mathbb{R})$. Quel est le rang maximal de $A$ ?

14) Soient $B, C \in \mathcal{E}$.

(a) On suppose que $B C \in \mathcal{N}$. Prouver alors que $C B \in \mathcal{N}$.

(b) Ici, on suppose de plus que $B \in \mathcal{N}$ et $A B=B A$. Montrer que $A B \in \mathcal{N}$ et que $A+B \in \mathcal{N}$.

15) Déterminer l'ensemble de toutes les matrices symétriques réelles appartenant à $\mathcal{N}$.

16) Dans cette question on suppose que la matrice nilpotente $A$ est antisymétrique.

(a) Prouver que $A^{2}=O_{n}$.

(b) En déduire l'ensemble de toutes les matrices antisymétriques appartenant à $\mathcal{N}$ (on pourra utiliser la trace).


## 2 Exemples

Dans cette partie, $M$ est une matrice de $\mathcal{E}$.

17) Dans cette question, on prend $M=\left(m_{i, j}\right) \in \mathcal{E}$ définie par : $\forall(i, j) \in[|1, n|]^{2}, m_{i, j}=$ $\left\{\begin{array}{ll}0 & \text { si } i \geq j \\ 1 & \text { sinon }\end{array}\right.$, c'est à dire

$$
M=\left(\begin{array}{ccccc}
0 & 1 & 1 & \ldots & 1 \\
0 & 0 & 1 & \ldots & 1 \\
\vdots & \vdots & & \ddots & \vdots \\
0 & 0 & 0 & \ldots & 1 \\
0 & 0 & 0 & \ldots & 0
\end{array}\right)
$$

(a) Déterminer les éléments propres (valeurs propres et sous-espaces propres) de la matrice $M$.

(b) On pose $S=M+{ }^{t} M$. A-t-on $S \in \mathcal{N}$ ?

Montrer que $S^{2} \in \operatorname{Vect}\left(I_{n}, S\right)$. Déterminer alors les éléments propres de la matrice $S$.

(c) $\mathcal{N}$ est-il un sous-espace vectoriel de $\mathcal{N}$ ?

18) Dans cette question on prend $n=2$.

(a) On suppose que $M$ est de rang 1.

Montrer que $M^{2}=\operatorname{tr}(M) M$. En déduire que $M$ est diagonalisable ou nilpotente.

(b) Déterminer une matrice nilpotente de $\mathcal{M}_{2}(\mathbb{R})$ dont la diagonale n'est pas identiquement nulle.

(c) En déduire l'ensemble de toutes les matrices nilpotentes de $\mathcal{M}_{2}(\mathbb{R})$.

## 3 Sous-espace engendré par $\mathcal{N}$

Soient

- $T_{0}$ le sous-espace vectoriel de $\mathcal{E}$ constitué des matrices de trace nulle

- $V$ le sous-espace de $\mathcal{E}$ engendré par $\mathcal{N}: V=\operatorname{Vect}(\mathcal{N})$, c'est à dire l'ensembme de toutes les combinaisons linéaires (finies) d'éléments de $\mathcal{N}$.

19) Déterminer la dimension de $T_{0}$.

20) Prouver que $\mathcal{N}$ et $V$ sont inclus dans $T_{0}$.

21) Pour tout $j \in[|2, n|]$, on note

$$
F_{j}=E_{1,1}+E_{1, j}-E_{j, 1}-E_{j, j} \text { et } G_{j}=F_{j}-E_{1, j}+E_{j, 1}
$$

(a) Calculer $F_{j}^{2}$.

(b) Montrer que $G_{j} \in V$

(c) Soit $\mathcal{F}$ la famille de $\mathcal{E}$ constituée des $E_{i, j}$ avec $i \neq j$ et $i, j \in[|1, n|]$ et de toutes les matrices $G_{k}$ pour $k \in[|2, n|]$.

Montrer que la famille $\mathcal{F}$ est libre dans $V$.

(d) En déduire que $V=T_{0}$.

## 4 Sous-espaces de dimension maximale contenus dans $\mathcal{N}$

On note $\mathcal{T}_{1}$ le sous-espace vectoriel de $\mathcal{E}$ constitué des matrices triangulaires supérieures dont la diagonale est composée uniquement de 0 .

22) Déterminer la dimension de $\mathcal{T}_{1}$.

23) Montrer que toute matrice nilpotente est semblable à une matrice de $\mathcal{T}_{1}$. On pourra utiliser les résultats de la partie 1 .

24) Démontrer que $\mathcal{E}=\mathcal{S}_{n}(\mathbb{R}) \oplus \mathcal{T}_{1}$.

25) Soit $F$ un sous-espace vectoriel de $\mathcal{E}$ contenu dans $\mathcal{N}$ dont la dimension est notée $d$.


(a) On suppose que $d>\frac{n(n-1)}{2}$. Démontrer que $\operatorname{dim}\left(\mathcal{S}_{n}(\mathbb{R}) \cap F\right)>0$. Conclure.

(b) Quelle est la dimension maximale d'un sous-espace de $\mathcal{E}$ contenu dans $\mathcal{N}$ ? Donner un exemple de tel sous-espace.

## 5 Un peu de topologie

$\mathcal{E}$ est muni de sa structure d'espace vectoriel normé de dimension finie.

26) Montrer que $\mathcal{N}$ est une partie fermée de $\mathcal{E}$.

27) Soient $A \in \mathcal{N}, \alpha \in \mathbb{R}^{*}$ et $M=I_{n}+\alpha A$.

Montrer que $\operatorname{det}(M)=1$. En déduire que toute boule ouverte de centre $A$ contient au moins une matrice de rang $n$ puis que l'intérieur de $\mathcal{N}$ est vide.

28) Soit $F$ un sous-espace de $\mathcal{E}$. Montrer que si l'intérieur de $F$ est non vide, alors $F=\mathcal{E}$.

Retrouver alors le résultat de la question précédente.

## 6 Deux autres résultats

Soient $A \in \mathcal{N}, \alpha \in \mathbb{R}^{*}$ et $M=I_{n}+\alpha A$.


29) On sait que $M$ est inversible. Calculer son inverse à l'aide des puissances de la matrice $A$. On pourra utiliser une suite géométrique.

30) Donner sans démonstration le développement en série entière de la fonction $x \mapsto(1+x)^{1 / 2}$.

31) Montrer qu'il existe une matrice $B \in \mathcal{E}$ telle que $B^{2}=M$. On exprimera $B$ comme un polynôme de la matrice $A$.
