<style>
h2 {text-align:center}
</style>


# Devoir Maison 3

Sujet etoilé : Vous pouvez faire, presque en entier [Centrale Maths 2 2023](https://www.concours-centrale-supelec.fr/CentraleSupelec/2023/PSI/M014.pdf).

## Partie I - Préliminaires

Q1. Justifier que pour tout $\ds n$ appartenant à $\ds \mathbb{N}, t^n e^{-t^2}=\underset{t\to+\infty}{o}\left(\frac{1}{t^2}\right)$.

!!! corrigé
    On a $t^2\times t^ne^{-t^2}=t^{n+2}e^{-t^2}\xrightarrow[t\to +\infty]{}0$ par croissances comparées donc $t^ne^{-t^2}=o(\frac 1{t^2})$. 

Q2. Montrer que pour tout $\ds n$ élément de $\ds \mathbb{N}$, l'intégrale $\ds \int_0^{+\infty} t^n e^{-t^2} d t$ est convergente.

!!! corrigé
    Pour $n$ entier naturel, $t\mapsto t^ne^{-t^2}$ est définie en $0$, continue sur $[0,+\infty[$ et un $o(\frac 1{t^2})$ au voisinage de $+\infty$. Par théorème de comparaison, l'intégrale est convergente. 

Q3. En déduire que pour tout $\ds n$ élément de $\ds \mathbb{N}$, l'intégrale $\ds \int_{-\infty}^{+\infty} t^n e^{-t^2} d t$ est convergente.

!!! corrigé
    Par un changement de variable $u=-t$ $\mathcal C^1$ et strictement décroissant, on trouve que l'intégrale $\int_{-\infty}^0(-1)^nu^ne^{-u^2} dt$ est de même nature que $\int_0^{+\infty} t^ne^{-t^2}dt$ et est donc convergente. On en déduit que les deux intégrales $\int_{-\infty}^0 t^ne^{-t^2}dt$ et $\int_{0}^{+\infty}t^ne^{-t^2}dt$ sont convergentes et donc que $\int_{-\infty}^{+\infty}t^ne^{-t^2} dt$ est convergente.

Q4. En déduire que pour tout polynôme $\ds P$ appartenant à $\ds \mathbb{R}[X]$, l'intégrale $\ds \int_{-\infty}^{+\infty} P(t) e^{-t^2} d t$ est convergente.

!!! corrigé
    Soit $P=p_0+p_1X+\cdots+p_nX^n$ un polynôme de degré $n$, alors pour tout $k\in\{0,\dots,n\}$, $\int_{-\infty}^{+\infty} t^ke^{-t^2}d t$ est convergente et par linéarité, $\int_{-\infty}^{+\infty} P(t)e^{-t^2}d t$ est convergente.


Pour la suite, on admet que $\ds \int_{-\infty}^{+\infty} e^{-t^2} d t=\sqrt{\pi}$ et on note $\ds I_n=\int_{-\infty}^{+\infty} t^n e^{-t^2} d t$.

Q5. Établir à l'aide d'une intégration par parties que pour tout $\ds n$ élément de $\ds \mathbb{N}, I_{n+2}=\frac{n+1}{2} I_n$.

!!! corrigé
    On pose $g(t)=\frac 1{n+1}t^{n+1}$ et $h(t)=e^{-t^2}$. On a alors $I_{n}=\int_{-\infty}^{+\infty}g'(t)h(t)dt$. Par ailleurs $\lim_{-\infty}g(t)h(t)=0$ et $\lim_{+\infty}g(t)h(t)=0$. Donc par théorème d'IPP, $\int_{-\infty}^{+\infty}g'(t)h(t)dt$ et $\int_{-\infty}^{+\infty}g(t)h'(t)dt$ ont même nature (ici convergente car la première est convergente) et on a $\int_{-\infty}^{+\infty}g'(t)h(t)dt=0-\int_{-\infty}^{+\infty}g(t)h'(t)dt$ c'est à dire $\int_{0}^{+\infty} t^ne^{-t^2}dt=-\int_{-\infty}^{+\infty}\frac{t^{n+1}}{n+1}(-2te^{-t^2})dt=\frac{2}{n+1}\int_{-\infty}^{+\infty}t^{n+2}e^{-t^2}dt$ et donc $I_{n+2}=\frac{n+1}{2}I_n$.

Q6. Montrer que pour tout $\ds p$ élément de $\ds \mathbb{N}, I_{2 p+1}=0$.

!!! corrigé
    On a donc une relation de récurrence d'ordre $2$ entre les termes de la suite. Or $I_1=\int_{-\infty}^{+\infty}te^{-t^2}dt=\left[\frac {-1}{2}e^{-t^2}\right]=0$. On peut aussi invoquer le caractère impair de l'intégrande. On a alors $I_{2p+1}=pI_{2p-1}=\dots=p!I_1=0$.

Q7. Montrer que pour tout $\ds p$ appartenant à $\ds \mathbb{N}, I_{2 p}=\frac{(2 p) !}{2^{2 p} p !} \sqrt{\pi}$.

!!! corrigé
    On montre cela par récurrence par exemple. Pour $p$ entier naturel, on pose $H_p$ la propriété «$I_{2 p}=\frac{(2 p) !}{2^{2 p} p !}\sqrt\pi$». Pour $p=0$, on a $I_0=\int_{-\infty}^{+\infty}e^{-t^2}dt=\sqrt \pi$ d'après l'énoncé. Par ailleurs $\frac{(2 p) !}{2^{2 p} p !}\sqrt\pi=\frac{0!}{2^00!}\sqrt\pi=\sqrt\pi$. Ainsi, $H_0$ est vraie.

    Soit maintenant $p$ un entier naturel, supposons que $H_p$ est vraie. Montrons que $H_{p+1}$ est vraie. D'après la relation précédente, on a $I_{2(p+1)}=\frac{2p+1}{2}I_{2p}\underset{H_p}{=}\frac {2p+1}2\times \frac{(2 p) !}{2^{2 p} p !} \sqrt{\pi}=\frac{(2p+1)!}{2^{2p+1}p!}\sqrt \pi=\frac{(2p+2)!}{2(p+1)2^{2p+1}p!}\sqrt \pi=\frac{(2(p+1))!}{2^{2(p+1)}(p+1)!}\sqrt \pi$. Ainsi $H_{p+1}$ est vraie. 

    On a donc montré par récurrence que pour tout $p$ entier naturel, $I_{2p}=\frac{(2 p) !}{2^{2 p} p !} \sqrt{\pi}$. 


## Partie II - Recherche des extrema

Soit $\ds F$ une fonction définie sur $\ds \mathbb{R}^2$ par : $\ds F(x, y)=\frac{1}{\sqrt{\pi}} \int_{-\infty}^{+\infty}(t-x)^2(t-y)^2 e^{-t^2} d t$.

Q8. Montrer que pour tout $\ds (x, y)$ appartenant à $\ds \mathbb{R}^2: F(x, y)=\frac{3}{4}+\frac{1}{2}\left(x^2+4 x y+y^2\right)+x^2 y^2$.

!!! corrigé
    On remarque que $(t-x)^2(t-y)^2 e^{-t^2}=(t^2-2tx+x^2)(t^2-2ty+y^2)e^{-t^2}=(t^4-2t^3(x+y)+t^2(x^2+4xy+y^2)-2txy(x+y)+x^2y^2)e^{-t^2}$ et donc en intégrant on obtient : $\frac{1}{\sqrt{\pi}} \int_{-\infty}^{+\infty}(t-x)^2(t-y)^2 e^{-t^2} d t=\frac{1}{\sqrt{\pi}}\left(I_4-2(x+y)I_3+(x^2+4xy+y^2)I_2-xy(x+y)I_1+x^2y^2I_0\right)=\frac 1{\sqrt \pi}\left(I_4+(x^2+4xy+y^2)I_2+x^2y^2I_0\right)$. Or $I_2=\frac{2!}{2^21!}\sqrt\pi=\frac{1}{2}\sqrt\pi$ et $I_4=\frac{3}{2}I_2=\frac 34\sqrt\pi$. On en déduit l'égalité voulue.


Q9. Calculer les dérivées partielles premières de $\ds F$ et en déduire les trois points critiques de $\ds F$.

!!! corrigé
    On calcule $\frac{\partial F}{\partial x}(x,y)=x+2y+2xy^2$ et $\frac{\partial F}{\partial y}(x,y)=2x+y+2x^2y$.
    Les points critiques sont les points qui annulent les deux dérivées partielles. On a donc $x+2y+2xy^2=0\quad (1)$ et $2x+y+2x^2y=0\quad(2)$. En faisant $x(1)-y(2)$ on a $x^2-y^2=0$ et donc $x=y$ ou $x=-y$. Dans le premier cas, on a avec $(1)$ : $3x+2x^3=0$ c'est à dire $x=0$ et donc $y=0$ : le point $(0,0)$ est un point critique. Dans le deuxième cas, on a avec $(1)$ : $-x+2x^3=0$ c'est à dire $x=\pm\frac 1{\sqrt 2}$ et $y$ en découle. Les trois points critiques sont donc $(0,0)$, $(\frac 1{\sqrt 2},-\frac 1{\sqrt 2})$ et $(-\frac 1{\sqrt 2},\frac 1{\sqrt 2})$. 


Q10. Calculer pour tout $\ds x$ appartenant à $\ds \mathbb{R}, F(x, x)-F(0,0)$ et $\ds F(x,-x)-F(0,0)$.

!!! corrigé
    On a $F(x,x)-F(0,0)=3x^2+x^4$ et $F(x,-x)-F(0,0)=\frac{1}{2}\left(x^2-4 x^2+x^2\right)+x^4=x^4-2x^2$. 

Q11. Le point $\ds (0,0)$ est-il un extremum local?

!!! corrigé
    Ainsi au voisinage de $0$ (pour $x\in]-1,1|$ par exemple), on a $F(x,x)-F(0,0)\ge 0$ et $F(x,-x)-F(0,0)\le 0$. On en déduit donc que $(0,0)$ est un maximum dans la direction $(1,1)$ mais un minimum dans la direction $(1,-1)$. On en déduit que $(0,0)$ n'est pas un extremum local.


## Partie III - Intégrale dépendant d'un paramètre

Q12. Pour tout $\ds x$ élément de $\ds \mathbb{R}$, montrer que les intégrales $\ds \int_0^{+\infty} \sin (x t) e^{-t^2} d t$ et $\ds \int_0^{+\infty} t \cos (x t) e^{-t^2} d t$ convergent.

!!! corrigé
    $t\mapsto \sin(xt)e^{-t^2}$ est définie sur $[0,+\infty[$ et $t^2\sin(xt)e^{-t^2}\xrightarrow[t\to +\infty]{}0$ donc $\sin(xt)e^{-t^2}=o(\frac 1{t^2})$ donc est intégrable en $+\infty$. On en déduit la convergence de la première intégrale. Et on fait de même pour la deuxième sans difficulté. 

Pour la suite, on note $\ds S(x)=\int_0^{+\infty} \sin (x t) e^{-t^2} d t$ et $\ds C(x)=\int_0^{+\infty} t \cos (x t) e^{-t^2} d t$.

Q13. Rappeler la formule de Taylor avec reste intégral sans oublier les hypothèses.

!!! corrigé
    Si $f$ est de classe $\mathcal C^{n+1}$ sur $[a,b]$ alors $f(b)=\sum_{k=0}^{n}\frac{f^{(k)}(a)}{k!}(b-a)^k \quad +\int_{a}^{b}f^{(n+1)}(t)\frac{(b-t)^n}{n!}dt$.  

Q14. En appliquant la formule précédente à la fonction $sin$, montrer que pour tout $\ds (\lambda, a)$ éléments de $\ds \mathbb{R}^2,|\sin (\lambda+a)-\sin (a)-\lambda \cos (a)| \leq \frac{\lambda^2}{2}$.

!!! corrigé
    D'après la formule de Taylor reste intégral avec $n=1$ et $b=\lambda+a$, $\sin(\lambda+a)-\sin(a)-\lambda\cos(a)=\int_{a}^{\lambda+a}\sin(t)\frac{(\lambda+a-t)^2}{2}dt$ et par positivité de l'intégrale, si $\lambda>0$, $|\sin(\lambda+a)-\sin(a)-\lambda\cos(a)|\le\int_{a}^{\lambda+a}\frac{(\lambda+a-t)}{1}dt=\frac{\lambda^2}{2}$. On a la même chose si $\lambda<0$. 

Q15. Montrer que pour tout $\ds x$ appartenant à $\ds \mathbb{R}, \lim _{h \rightarrow 0} \frac{S(x+h)-S(x)}{h}-C(x)=0$.

!!! corrigé
    On écrit : 

    $$
    \begin{aligned}
    \left|\frac{S(x+h)-S(x)}{h}-C(x)\right|&=\left|\int_{0}^{+\infty}\left(\frac{\sin(xt+ht)-\sin(xt)}{h}-t\cos(xt)\right)e^{-t^2}dt\right|\\
    &\le\int_{0}^{+\infty}\frac 1h\left|\sin(xt+ht)-\sin(xt)-th\cos(xt)\right|e^{-t^2}dt\\
    &\underset{\lambda=ht,a=xt}{\le}\int_{0}^{+\infty} \frac 1h\frac{t^2h^2}{2}e^{-t^2}dt\\
    &\le \frac h2\int_{0}^{+\infty}t^2e^{-t^2}dt\\
    &\le \frac h4I_2\\
    &\xrightarrow[h\to 0]{}0.  
    \end{aligned}
    $$

    car l'intégrande de $I_2$ est paire.

Q16. En déduire que la fonction $\ds S$ est dérivable sur $\ds \mathbb{R}$ et donner sa dérivée.

!!! corrigé
    On en déduit que pour tout réel $x$, $\frac{S(x+h)-S(x)}{h}\xrightarrow[h\to 0]{}C(x)$ et donc $S$ est dérivable en $x$, de dérivée $C(x)$. 

Q17. Démontrer que pour tout $\ds x$ élément de $\ds \mathbb{R}, C(x)=\frac{1}{2}-\frac{x}{2} S(x)$ (on pourra effectuer une intégration par partie).

!!! corrigé
    On a  :

    $$
    \begin{aligned}
    C(x)&=\int_0^{+\infty} t\cos(tx)e^{-t^2}dt\\
    &=[-\frac 12e^{-t^2}\cos(tx)]_0^{+\infty}-\frac x2\int_0^{+\infty}\sin(tx)e^{-t^2}dt\\
    &=\frac 12-\frac x2S(x).
    \end{aligned}
    $$

Q18. Donner une équation différentielle dont $\ds S$ est solution sur $\ds \mathbb{R}$.

!!! corrigé
    On a donc d'après les questions précédentes, $S'(x)=\frac 12-\frac x2S(x)$ et ainsi $S$ est solution de l'équation différentielle $f'+\frac x2f=\frac 12$. 


Q19. Montrer que pour tout $\ds x$ appartenant à $\ds \mathbb{R}, S(x)=\frac{1}{2} e^{-\frac{x^2}{4}} \int_0^x e^{\frac{t^2}{4}} d t$.

!!! corrigé
    On résoud cette équation différentielle : les solutions de l'ESSM sont de la forme $\lambda e^{-\frac {x^2}4}$. Une solution particulière est alors donnée par la méthode de la variation de la constante sous la forme $y_p(x)=\frac 12\int_0^xe^{\frac{t^2}4}dte^{-\frac{x^2}{4}}$. On a alors $S(x)=\lambda e^{-\frac{x^2}{4}}+\frac 12\int_0^xe^{\frac{t^2}4}dt$ et puisque $S(0)=0$, $S(x)=\frac 12\int_0^xe^{\frac{t^2}4}dte^{-\frac{x^2}{4}}$. 