<style>
h2 {text-align:center}
</style>


# Devoir Maison 1

Les DM sont à rendre le vendredi de la semaine où vous n'avez pas colle de maths. Merci de les rendre en groupes de 2 ou trois.


# Résolution d'une équation fonctionnelle


Dans cet exercice, on souhaite déterminer les fonctions $f:] 0,+\infty[\rightarrow \mathbb{R}$ vérifiant les relations :
$$
\left.\lim _{x \rightarrow+\infty} f(x)=0 \quad \text { et } \quad \forall x \in\right] 0,+\infty\left[, \quad f(x+1)+f(x)=\frac{1}{x^2} .\right.
$$

## Partie I - Existence et unicité de la solution du problème (P)


Dans cette partie, on démontre que le problème $(\mathrm{P})$ admet une unique solution et on détermine une expression de celle-ci sous la forme d'une série de fonctions.


### Existence de la solution

Pour tout $k \in \mathbb{N}$, on définit la fonction $\left.\varphi_k:\right] 0,+\infty[\rightarrow \mathbb{R}$ par :

$$
\forall x \in] 0,+\infty[, \quad \varphi_k(x)=\frac{(-1)^k}{(x+k)^2} .
$$

Q1. Montrer que la série de fonctions $\sum_{k \gg 0} \varphi_k$ converge simplement sur $] 0,+\infty[$.


Dans tout le reste de cet exercice, on note $\varphi:] 0,+\infty[\rightarrow \mathbb{R}$ la somme de la série $\sum_{k \geqslant 0} \varphi_k$.

Q2. Montrer que pour tout $x \in] 0,+\infty[$, on a $\varphi(x+1)+\varphi(x)=\frac{1}{x^2}$.

Q3. En utilisant le théorème spécial des séries alternées, montrer que :

$$
\forall x \in] 0,+\infty[, \quad \forall n \in \mathbb{N}, \quad\left|\sum_{k=n+1}^{+\infty} \varphi_k(x)\right| \leqslant \frac{1}{(x+n+1)^2}.
$$


Q4. Montrer que la fonction $\varphi$ est une solution de (P).

### Unicité de la solution

Q5. Montrer que si $f:] 0,+\infty[\rightarrow \mathbb{R}$ est une solution de (P), alors pour tout $n \in \mathbb{N}$, on a :

$$
\forall x \in] 0,+\infty[, \quad f(x)=(-1)^{n+1} f(x+n+1)+\sum_{k=0}^n \frac{(-1)^k}{(x+k)^2} .
$$

Q6. En déduire que la fonction $\varphi$ est l'unique solution de (P).


## Partie II - Étude de la solution du problème (P)

Dans cette partie, on étudie quelques propriétés de l'unique solution $\varphi:] 0,+\infty[\rightarrow \mathbb{R}$ du problème (P).

Q7. Soit $\varepsilon>0$. Montrer que la série de fonctions $\sum_{k \gtrless 0} \varphi_k$ converge uniformément sur $[\varepsilon,+\infty[$.

Q8. Montrer que la fonction $\varphi$ est continue sur $] 0,+\infty[$. En utilisant le fait que $\varphi$ est une solution du problème (P), en déduire un équivalent simple de $\varphi$ au voisinage de $0^{+}$.

Q9. Justifier que la fonction $\varphi$ est dérivable sur $] 0,+\infty[$ et que l'on a :

$$
\forall x \in] 0,+\infty[, \quad \varphi^{\prime}(x)=\sum_{k=0}^{+\infty} \frac{2(-1)^{k+1}}{(x+k)^3} .
$$

Q10. En déduire que la fonction $\varphi$ est décroissante sur $] 0,+\infty[$.

Q11. En utilisant le résultat de la question précédente et la relation $(\mathrm{P})$, montrer que :

$$
\forall x \in] 1,+\infty[, \quad \frac{1}{x^2} \leqslant 2 \varphi(x) \leqslant \frac{1}{(x-1)^2} .
$$

En déduire un équivalent de $\varphi$ en $+\infty$.


## Partie III - Expression intégrale de la solution du problème (P)

Dans cette partie, on détermine une expression de $\varphi$ sous la forme d'une intégrale. On considère un élément $x \in] 0,+\infty[$.

Q12. Pour tout $k \in \mathbb{N}$, montrer que la fonction $t \mapsto t^{x+k-1} \ln (t)$ est intégrable sur ]0,1] et que l'on a :

$$
\int_0^1 t^{x+k-1} \ln (t) \mathrm{d} t=-\frac{1}{(x+k)^2} .
$$

Q13. En déduire que la fonction $t \mapsto \frac{t^{x-1} \ln (t)}{1+t}$ est intégrable sur $\left.] 0,1\right]$ et que :

$$
\varphi(x)=-\int_0^1 \frac{t^{x-1} \ln (t)}{1+t} \mathrm{~d} t .
$$