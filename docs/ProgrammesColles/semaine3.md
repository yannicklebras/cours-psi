# Suites et séries de fonctions

## A - Compléments sur les séries numériques


*Cette section a pour objectif de consolider et d'élargir les acquis de
première année sur les séries, notamment la convergence absolue, en
vue de l'étude des probabilités discrètes et des séries de fonctions.
L'étude de la semi-convergence n'est pas un objectif du programme.*


<table>
<tr><td>a) Compléments sur les séries numériques</td></tr>

  
<tr>
<td>Technique de comparaison série-intégrale.
</td>
<td>
Les étudiants doivent savoir utiliser la comparaison série-intégrale
pour établir des convergences et des divergences
de séries, estimer des sommes partielles de séries
divergentes ou des restes de séries convergentes 
dans le cas d'une fonction monotone.</td></tr>

<tr><td>Formule de Stirling : équivalent de $n!$.
</td><td>
La démonstration n'est pas exigible.
</td></tr>

<tr><td>Règle de d'Alembert.</td><td></td>

<tr><td>Théorème spécial des séries alternées, majoration et signe du reste.
</td><td>La transformation d'Abel est hors programme.</td></tr>


<tr><td>Produit de Cauchy de deux séries absolument convergentes.</td><td>
La démonstration n'est pas exigible.</td></tr>
</table>

## B - Suites et séries de fonctions

*Cette section a pour objectif de définir différents modes de  convergence d'une suite, d'une série de fonctions et d'étudier le transfert à la limite, à la somme des propriétés des fonctions. Les fonctions sont définies sur un intervalle $I$ de $\R$ et à valeurs dans $\R$ ou $\C$.*



<table>
<tr><td>a) Modes de convergence d'une suite ou d'une série de fonctions}</td></tr>

<tr><td>Convergence simple d'une suite de fonctions. Convergence uniforme.
La convergence uniforme entraîne la convergence simple.</td><td></td></tr>

<tr><td>Norme de la convergence uniforme sur l'espace des fonctions bornées à
valeurs dans $\R$ ou $\C$.</td><td></td></tr> 



<tr><td>Convergence simple, convergence uniforme, convergence normale d'une
série de fonctions.</td><td>
Utilisation d'une majoration uniforme de $|f_n(x)|$ pour établir la
convergence normale de $\sum f_n$.</td></tr>  


<tr><td>La convergence normale entraîne la convergence uniforme.
</td><td> La convergence normale entraîne la convergence absolue en tout point.</td></tr>


<tr><td>b) Régularité de la limite d'une suite de fonctions</td></tr>

<tr><td>Continuité de la limite d'une suite de fonctions :

si une suite $(f_n)$ de fonctions continues sur $I$ converge
uniformément vers $f$ sur $I$, alors $f$ est continue sur $I$.</td><td>


En pratique, on vérifie la convergence uniforme sur tout segment, ou
sur d'autres intervalles adaptés à la situation.
</td></tr>


<tr><td>Intégration sur un segment de la limite d'une suite de fonctions :

si une suite $(f_n)$ de fonctions continues converge uniformément vers
$f$ sur $[a,b]$ alors : 

$\displaystyle
\int_a^b f_n(t) \,\mathrm{d}\!t \xrightarrow[n \to +\infty]{} 
\int_a^b f(t) \,\mathrm{d}\!t
$.

</td><td></td></tr>

<tr><td>Dérivabilité de la limite d'une suite de fonctions :

si une suite $(f_n)$ de fonctions de classe $\mathcal{C}^1$ sur un intervalle 
$I$ converge simplement sur $I$ vers une fonction $f$, et si la suite
$(f'_n)$ converge uniformément sur $I$ vers une fonction $g$, alors $f$ est de
classe $\mathcal{C}^1$ sur $I$ et $f'=g$.</td><td>



En pratique, on vérifie la convergence uniforme sur tout segment, ou
sur d'autres intervalles adaptés à la situation.
</td></tr>

<tr><td>Extension aux suites de fonctions de classe $\mathcal{C}^k$, sous l'hypothèse
de convergence uniforme de $(f^{(k)}_n)$ et de convergence simple de
$(f^{(j)}_n)$ pour $0\leqslant j<k$.
</td><td>
</td></tr>

<tr><td>c) Régularité de la somme d'une série de fonctions</td><td></td></tr>


<tr><td>Continuité de la somme d'une série de fonctions :

si une série $\sum f_n$ de fonctions continues sur $I$ converge uniformément
sur $I$, alors 
sa somme est continue sur $I$.</td><td>


En pratique, on vérifie la convergence uniforme sur tout segment, ou
sur d'autres intervalles adaptés à la situation.
</td></tr>
<tr><td>
Théorème de la double limite :

si une série $\sum f_n$ de fonctions définies sur $I$ converge uniformément
sur $I$ et si,
pour tout $n$, $f_n$ admet une limite $\ell_n$ en 
$a$ borne de $I$ (éventuellement infinie),
alors la série $\displaystyle \sum \ell_n$ converge, la somme de
la série admet une limite en $a$ et :  
\[
\sum_{n=0}^{+\infty} f_n(x) \xrightarrow[x\to a]{} \sum_{n=0}^{+\infty} \ell_n.
\]
</td><td>
La démonstration est hors programme.
</td></tr>
<tr><td>

Intégration de la somme d'une série de fonctions sur un segment :

si une série $\displaystyle \sum f_n$ de fonctions continues converge uniformément sur $[a,b]$
alors la série des intégrales est convergente et : 


$$
\displaystyle
\int_a^b \sum_{n=0}^{+\infty} f_n (t) \,\mathrm{d}\!t =
\sum_{n=0}^{+\infty} \int_a^b f_n(t) \,\mathrm{d}\!t. 
$$

</td><td>
</td></tr>
<tr><td>

Dérivation de la somme d'une série de fonctions :

si une série $\displaystyle \sum f_n$ de classe $\mathcal{C}^1$ converge simplement sur un intervalle $I$
et si la série $\displaystyle \sum f'_n$ converge uniformément sur $I$, alors
la somme $\displaystyle \sum_{n=0}^{+\infty} f_n$ est de classe
$\mathcal{C}^1$ sur $I$ et sa dérivée 
est $\displaystyle \sum_{n=0}^{+\infty} f'_n$.
</td><td>
En pratique, on vérifie la convergence uniforme sur tout segment ou
sur d'autres intervalles adaptés à la situation.

Extension à la classe $\mathcal{C}^k$ sous hypothèse similaire à celle
décrite dans le cas des suites de fonctions.

</td></tr>
<tr><td>
</table>


## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.aeif.fr/cours-psi/Banque%20dexercices/)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 3, les exercices de la banque au programme sont les exercices de 1 à 33.

## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 