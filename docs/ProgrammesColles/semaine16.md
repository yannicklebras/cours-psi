


## Calcul différentiel

### A - Équations différentielles linéaires scalaires
<table>
<tr><td>
Équation différentielle scalaire d'ordre $2$ à coefficients continus
$y''+a(t)y'+b(t)y = c(t)$.</td><td></td></tr> 


<tr><td>Forme des solutions : somme d'une solution particulière et de la
solution générale de l'équation homogène.</td><td>
La résolution explicite de l'équation différentielle doit comporter des indications.
</td></tr>
<tr><td>
Théorème de Cauchy linéaire : existence et unicité de la solution d'un
problème de Cauchy. 
</td><td>
</td></tr>
<tr><td> Espace vectoriel des solutions de l'équation homogène,
dimension.</td><td>Exemples  d'utilisation de développements en série entière
pour la recherche de solutions.

</td></tr>
<tr><td> 

</table>

### B - Dérivabilité des fonctions vectorielles



L'objectif de cette section est de
généraliser aux fonctions à valeurs dans $\:\R^n$ la notion de dérivée
d'une fonction numérique.

Toutes les fonctions sont définies sur un intervalle $I$ de $\R$ et
à valeurs dans $\R^n$.

<table>
<tr><td>
Interprétation d'une
fonction à valeurs dans $\R^n$ comme courbe paramétrée.
</td><td>
L'étude
et le tracé d'arcs paramétrés sont hors programme.


</td></tr>
<tr><td>

Dérivabilité en un point.


Dérivabilité sur un intervalle.
</td><td>Définition par le taux d'accroissement, caractérisation par le
développement limité d'ordre un. 

Traduction par les coordonnées dans la base canonique.

Interprétation cinématique.</td></tr>
<tr><td>

</td><td>
</td></tr>
<tr><td>

Combinaison linéaire de fonctions dérivables.
</td><td></td></tr>
<tr><td>

Dérivée de $L(f)$, où $L$ est linéaire et $f$ à valeurs
dans $\R^n$.</td><td></td></tr>
<tr><td> 

Dérivée de $B(f,g)$, où $B$ est bilinéaire, de $M(f_1,\dots,f_p)$, où
$M$ est $p$-linéaire, et $f$, $g$, $f_1,\dots,f_p$ à valeurs
vectorielles.</td><td> 
La démonstration n'est pas exigible.

Application au produit scalaire et au déterminant.
</td></tr>
<tr><td>

Dérivée de $f \circ \varphi$ où $\varphi$ est à valeurs réelles et $f$
à valeurs vectorielles. </td><td></td></tr>
<tr><td> 

Fonction de classe $\mathcal{C}^k$, de classe $\mathcal{C}^{\infty}$
sur un intervalle. 
</td><td>
</td></tr>
<tr><td>

</table>


### C - Fonctions de plusieurs variables

Les  dérivées partielles d'une fonction numérique définie sur un ouvert
  de $\R^2$ ont été introduites en première année. 
L'objectif de cette section est d'approfondir et de généraliser cette
étude aux fonctions de $p\geqslant 2$ variables.

L'étude d'une fonction de $\R^p$ dans $\R^n$ se 
ramenant à celle de ses coordonnées, cette section se consacre à
l'étude des fonctions de $\R^p$ dans $\R$.
Elle est axée sur la mise en place d'outils permettant de traiter
des applications du calcul différentiel à
l'analyse et la géométrie. On se limite en pratique au cas
$p=2$ ou $p=3$.



<table>
<tr><td>a) Fonctions de classe $\mathcal{C}^1$</td><tr>

<tr><td>Dérivée en un point selon un vecteur.
</td><td> Notation $D_vf(a)$.
</td></tr>
<tr><td>

Dérivées partielles d'ordre $1$ en un point d'une fonction définie sur
un ouvert $\Omega$ de $\R^p$ à valeurs dans  $\R$.
</td><td>
Notation $\dfrac{\partial f}{\partial x_i}(a)$.
On peut aussi utiliser $\partial_if(a)$. </td></tr>
<tr><td> 

Une fonction est dite de classe $\mathcal{C}^{1}$ sur $\Omega$ si ses
dérivées partielles d'ordre $1$ existent et sont continues sur $\Omega$.</td></tr>
<tr><td> 

Opérations sur les fonctions de classe $\mathcal{C}^{1}$. </td><td> </td></tr>
<tr><td>

Une fonction de classe $\mathcal{C}^1$ sur $\Omega$ admet en tout point $a$
de $\Omega$ un développement limité d'ordre $1$.</td><td>
La démonstration n'est pas exigible.

Une fonction de classe $\mathcal{C}^1$ sur $\Omega$ est continue sur $\Omega$.</td></tr>
<tr><td>
Différentielle de $f$ en $a$.
</td><td>Elle est définie comme la forme linéaire sur $\R^p$ :

$$
\mathrm{d} f(a)\,:\; (h_1,\ldots,h_p)\mapsto \displaystyle\sum_{i=1}^p
\frac{\partial f}{\partial x_i}(a)\, h_i
$$ 

Notation $\mathrm{d} f(a)\cdot h$.</td></tr>
<tr><td>
</td><td></td></tr>
<tr><td>

b) Règle de la chaîne</td></tr>

<tr><td>Dérivée de 
$t\mapsto f\bigl(x_1(t),\ldots,x_p(t)\bigr).$</td><td>
Interprétation géométrique.
</td></tr>
<tr><td> 
Application au calcul des dérivées partielles de :  


$$
(u_1,\dots,u_n)\mapsto
f\bigl(x_1(u_1,\dots,u_n),\dots,x_p(u_1,\dots,u_n)\bigr)
$$

</td><td>
En pratique, on se limite à $n\leqslant 3$ et $p\leqslant 3$.

Les étudiants doivent connaître le cas
particulier des coordonnées polaires. </td></tr>
<tr><td> 

Caractérisation des fonctions constantes sur un ouvert convexe. </td><td> </td></tr>
<tr><td> 

c) Gradient</td></tr>

<tr><td>
Dans $\R^p$ muni de sa structure euclidienne canonique, gradient d'une
fonction de classe $\mathcal{C}^1$.

Coordonnées du gradient.
</td><td>
Le gradient est défini par la relation $\mathrm{d} f(a) \cdot h=\langle \nabla
f(a), h\rangle$ pour $h\in\R^p$.

Notation ${\nabla}f(a).$
</td></tr>
<tr><td>
d) Applications géométriques</td></tr>

<tr><td>Courbe du plan définie par une équation $f(x,y)=0$ où $f$ est de
classe $\mathcal{C}^1$. 
</td><td> Lignes de niveau de $f$.</td></tr>
<tr><td>
Point régulier. Le gradient est normal à la tangente en un point
régulier.</td><td>
On admet que la courbe admet un paramétrage local de
classe $\mathcal{C}^1$.

Détermination d'une équation de la tangente en un point régulier.

Lorsqu'il est non nul, le gradient de $f$ est orthogonal aux lignes de
niveau et orienté dans le sens des valeurs croissantes de $f$.
</td></tr>
<tr><td>


Surface définie par une équation $f(x,y,z)=0$ où $f$ est de
classe $\mathcal{C}^1$. 
</td><td> 
</td></tr>
<tr><td>
Point régulier. Le plan tangent en un point régulier est défini
comme orthogonal au gradient.</td><td>
</td></tr>
<tr><td>
Courbe tracée sur une surface.</td><td>
Dans le cas d'une courbe régulière, la tangente à la courbe est
incluse dans le plan tangent à la surface.
</td></tr>
<tr><td> 

e) Fonctions de classe $\mathcal{C}^2$</td></tr>

<tr><td>Dérivées partielles d'ordre 2 d'une fonction définie sur un
  ouvert de $\R^p$ à valeurs dans $\R$.</td><td> 
Notations 
$\dfrac{\partial^2f}{\partial x_i\partial x_j}$. </td></tr>
<tr><td> 

Fonction de classe
$\mathcal{C}^{2}$ sur un ouvert de $\R^p$.</td><td>
</td></tr>
<tr><td> 

Théorème de Schwarz.</td><td>
La démonstration est hors programme.</td></tr>
<tr><td>


Matrice hessienne en un point $a$ d'une fonction de
  classe $\mathcal{C}^2$ sur un ouvert de $\R^p$ à valeurs
  dans $\R$.</td><td>Notation $H_f(a)$.</td></tr>
<tr><td> 
Formule de Taylor-Young à l'ordre $2$ :


$$
f(a+h) \underset{h \to 0}{=} f(a) + {\nabla f(a)}^\top h +
\frac{1}{2} {h}^\top H_f(a) h + \mathrm{o}(\|h\|^2).
$$

</td><td>
La démonstration est hors programme.

Expression en termes de produit scalaire.
</td></tr>
<tr><td>

f) Extremums 
  d'une fonction de $\R^p$ dans $\R$</td></tr>

<tr><td>Extremum local, global.</td><td>
</td></tr>
<tr><td>

Point critique d'une application de classe $\mathcal{C}^1$.</td><td></td></tr>
<tr><td>

Si une fonction de classe $\mathcal{C}^1$ sur un ouvert de $\R^p$
admet un extremum local en un point $a$, alors $a$ est un point
critique. </td><td></td></tr>
<tr><td>

Si $f$ est une fonction de classe $\mathcal{C}^2$ sur un ouvert de
$\R^p$ et $a$ un point critique de $f$ :

-  si $H_f(a) \in\mathcal{S}_p^{++}(\R)$, alors $f$ atteint un
  minimum local strict en $a$ ;

-  si $H_f(a) \notin \mathcal{S}_p^{+}(\R)$,
  alors $f$ n'a pas de minimum en $a$.

</td><td>
Adaptation à l'étude d'un maximum local.

Explicitation pour $p=2$ (trace et déterminant).

</td></tr>
<tr><td>

</td><td>Exemples de recherche d'extremums globaux sur une partie de
$\R^p$.</td></tr>
</table>




## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.apps.education.fr/cours-psi/Banque%20dexercices)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 15, les exercices de la banque au programme sont les exercices de 1 à 40 puis 43, 44, 47, 48, 50, 53 plus les exercices 54, 56 et 57, 59 ,61, 62,63, 64,65,67,70,73,75,77 (j'ai oublié d'inclure les exercices de calcul diff...)



## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 