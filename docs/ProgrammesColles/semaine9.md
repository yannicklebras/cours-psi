## B - Réduction des endomorphismes et des matrices carrées 

*La réduction des endomorphismes et des matrices carrées permet d'approfondir les notions étudiées en première année. <br> Il est attendu des étudiants qu'ils maîtrisent les deux points de vue suivants :*

-  *l'aspect géométrique (sous-espaces stables, éléments propres)* ;  
-  *l'aspect algébrique (utilisation de polynômes annulateurs).*

*L'étude des classes de similitude est hors programme ainsi que la notion de polynôme minimal.*


<table>

<tr><td>a) Éléments propres </td><td></td></tr>
<tr><td>Droite stable par un endomorphisme.</td><td></td></tr><tr><td>

Valeur propre, vecteur propre (non nul), sous-espace propre d'un endomorphisme.</td><td> 
Équation aux éléments propres $u(x)=\lambda x$.

Si $u$ et $v$ commutent, les sous-espaces 
propres de $u$ sont stables par $v$. </td></tr><tr><td>


Spectre d'un endomorphisme en dimension finie.
</td><td> Notation $\mathrm{Sp}(u)$. 

La notion de valeur spectrale est hors programme. 
</td></tr><tr><td>

La somme d'une famille finie de sous-espaces propres
d'un endomorphisme est directe.
</td><td>Toute famille finie de vecteurs propres associés à des
valeurs propres distinctes est libre.</td></tr><tr><td>  


Si un polynôme $P$ annule $u$, toute valeur propre
  de $u$ est racine de $P$.
</td><td>Si $u(x)=\lambda x$, alors $P(u)(x)=P(\lambda)x$.</td></tr><tr><td>

Valeur propre, vecteur propre, sous-espace propre
et spectre d'une matrice carrée.
</td><td>
Équation aux éléments propres $AX=\lambda X$.
</td></tr><tr><td>

b) Polynôme caractéristique</td><td></td></tr>

<tr><td>Polynôme caractéristique d'une matrice carrée, d'un
endomorphisme d'un espace vectoriel de dimension finie. 
</td><td>Par convention le polynôme caractéristique est unitaire.

Notations $\chi_A$, $\chi_u$.

Coefficients de degrés $0$ et $n-1$.</td></tr><tr><td>

Les valeurs propres d'un endomorphisme 
sont les racines de son polynôme caractéristique.</td><td>
Spectre complexe d'une matrice carrée réelle.</td></tr><tr><td>

Multiplicité d'une valeur propre.
Majoration de la dimension d'un
sous-espace propre par la multiplicité.</td><td>
Deux matrices semblables ont le même polynôme
caractéristique, donc les mêmes valeurs propres avec mêmes
multiplicités. </td></tr><tr><td>
Théorème de Cayley-Hamilton.
</td><td> La démonstration n'est pas exigible.</td></tr><tr><td>

c) Diagonalisation en dimension finie</td><td></td></tr>

<tr><td>Un endomorphisme d'un espace vectoriel de dimension finie
est dit diagonalisable s'il existe une base dans 
laquelle sa matrice est diagonale.</td><td>
Une telle base est constituée de vecteurs propres. 
</td></tr><tr><td>

Une matrice carrée est dite diagonalisable si elle est
semblable à une matrice diagonale.</td><td>
Interprétation en termes d'endomorphisme.

Application au calcul des puissances d'une matrice diagonalisable, à des exemples
de systèmes différentiels à coefficients 
constants.

Dans la pratique des cas numériques, on se limite à $n=2$ ou $n=3$.
</td></tr><tr><td>

Un endomorphisme d'un espace vectoriel $E$ est diagonalisable
  si et seulement si la somme de ses sous-espaces propres est égale à $E$. 
</td><td>
Exemple des projecteurs et des symétries. 
</td></tr><tr><td>

Un endomorphisme est diagonalisable si et seulement si  la somme des
dimensions de ses sous-espaces propres est égale à la dimension de
l'espace.</td><td>
Traduction matricielle.
</td></tr><tr><td>

Un endomorphisme est diagonalisable si et seulement si son polynôme
caractéristique est scindé sur $\K$ et si, pour toute
valeur propre, la dimension du sous-espace propre associé est égale à
sa multiplicité.
</td><td>
Traduction matricielle.

</td></tr><tr><td>

Un endomorphisme d'un espace vectoriel de dimension $n$ admettant $n$
valeurs propres distinctes est diagonalisable. 
</td><td>Polynôme caractéristique scindé à racines simples.

Traduction  matricielle.</td></tr><tr><td>


d) Diagonalisabilité et polynômes annulateurs</td><td></td></tr>

<tr><td> Un endomorphisme est diagonalisable
si et seulement s'il admet un polynôme annulateur scindé à racines simples.
</td><td>
La démonstration n'est pas exigible.

Traduction matricielle.

Le lemme de décomposition des noyaux est hors programme.</td></tr><tr><td>

L'endomorphisme induit par un endomorphisme diagonalisable
sur un sous-espace vectoriel stable est diagonalisable.</td><td>
</td></tr><tr><td>

Un endomorphisme $u$ est diagonalisable
si et seulement s'il admet
$\prod\limits_{\lambda \in \mathrm{Sp}(u)} (X-\lambda)$
pour polynôme annulateur.
</td><td></td></tr><tr><td>

e) Trigonalisation en dimension finie</td><td></td></tr>

<tr><td>Un endomorphisme d'un espace vectoriel de dimension finie est dit
trigonalisable s'il existe une base dans 
laquelle sa matrice est triangulaire.</td><td>

Expression de la trace et du déterminant d'un endomorphisme
trigonalisable, d'une matrice trigonalisable à l'aide des valeurs
propres. 
</td></tr><tr><td>
Une matrice carrée est dite trigonalisable si elle est
semblable à une matrice triangulaire.
</td><td>
Interprétation en termes d'endomorphisme.
</td></tr><tr><td>


Un endomorphisme est trigonalisable si et seulement si son polynôme
caractéristique est scindé sur $\K$. 
</td><td>
La démonstration n'est pas exigible.

Traduction matricielle.</td></tr><tr><td>

Toute matrice de $\mathcal{M}_n(\C)$ est
trigonalisable. 
</td><td>La technique générale de trigonalisation est hors programme. On
se limite dans la pratique à des exemples simples en petite dimension
et tout exercice de trigonalisation effective doit comporter une
indication.</td></tr>
</table>

# Espaces préhilbertiens réels, espaces euclidiens

## A -  Espaces préhilbertiens réels

L'objectif majeur est le théorème de projection orthogonale et
l'existence de la meilleure approximation quadratique. On s'appuie sur
des exemples de géométrie du plan et de l'espace pour illustrer les
différentes notions. 


<table> 
<tr><td>a) Produit scalaire et norme associée </td><td></td></tr>

<tr><td>Produit scalaire.

Espace préhilbertien réel, espace euclidien.
</td><td>Notations $\langle x,y\rangle$, $(x|y)$,  $x\cdot y$.

</td></tr><tr><td>

Exemples de référence : 

produit scalaire euclidien canonique sur
$\R^n$, produit scalaire canonique sur $\mathcal{M}_n(\R)$, 
produit scalaire défini par une intégrale sur
$\mathcal{C}^0([a,b],\R)$. 
</td><td>

Expression $X^\top Y$.

Expression $tr(A^\top B)$.
</td></tr><tr><td>

Inégalité de Cauchy-Schwarz, cas d'égalité.
</td><td></td></tr><tr><td>

Norme associée au produit scalaire.</td><td>Cas d'égalité dans l'inégalité
triangulaire.

Les étudiants doivent savoir manipuler les identités remarquables sur les normes
(développement de $\lVert u\pm v \rVert^2$, identité de polarisation).
</td></tr><tr><td>

b) Orthogonalité </td><td></td></tr><tr><td>

Vecteurs orthogonaux, sous-espaces orthogonaux, orthogonal d'un
sous-espace vectoriel $F$, d'une partie $X$.
</td><td> Notation $F^\bot$.

L'orthogonal d'une partie est un sous-espace vectoriel.</td></tr><tr><td>

Famille orthogonale, orthonormée (ou orthonormale).

Toute famille orthogonale de vecteurs non nuls est libre.

Théorème de Pythagore.
</td><td></td></tr><tr><td>

Algorithme d'orthonormalisation de Gram-Schmidt.
</td><td>

</td></tr><tr><td>
c) Bases orthonormées d'un espace euclidien</td><td></td></tr><tr><td>

Existence de bases orthonormées dans un espace euclidien. Théorème de
la base orthonormée incomplète.
</td><td></td></tr><tr><td>

Expression des coordonnées, du produit scalaire et de la norme dans
une base orthonormée. 
</td><td>

</td></tr><tr><td>
d) Projection orthogonale sur un sous-espace de dimension finie</td><td></td></tr><tr><td>

Supplémentaire orthogonal d'un sous-espace de dimension finie.
</td><td></td></tr><tr><td>
Dimension de $F^{\perp}$ en dimension finie.</td></tr><tr><td>


Projection orthogonale $p_F$ sur un sous-espace vectoriel $F$ de
dimension finie.
</td><td>Les étudiants doivent savoir déterminer $p_F(x)$ en calculant
son expression dans une base orthonormée de $F$
ou en résolvant un système linéaire traduisant l'orthogonalité de
$x-p_F(x)$ aux vecteurs d'une famille génératrice de $F$. 
</td></tr><tr><td>

Distance d'un vecteur à un sous-espace. Le projeté orthogonal de $x$
sur $F$ est l'unique élément de $F$ qui réalise la distance de $x$ à $F$.
</td><td>
Notation $d(x, F)$. </td></tr><tr><td>
Projeté orthogonal d'un vecteur sur l'hyperplan $\mathrm{Vect}(u)^\bot$ ;
distance entre $x$ et $\mathrm{Vect}(u)^\bot$.
</td><td>
Application géométrique à des calculs de distances.

</td></tr><tr><td>
 e) Formes linéaires sur un espace euclidien</td><td></td></tr><tr><td>

Représentation d'une forme linéaire à l'aide d'un produit scalaire.
</td><td>
Vecteur normal à un hyperplan.
</td></tr></table>

## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.apps.education.fr/cours-psi/Banque%20dexercices)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 8, les exercices de la banque au programme sont les exercices de 1 à 40 puis 43, 44, 47, 48, 50, 53 plus les exercices 54, 56, 57, 59 ,61.

## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 