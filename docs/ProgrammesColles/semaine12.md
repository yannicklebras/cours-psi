# Révisions de probas de première année.


Cette section, qui a vocation à interagir avec l'ensemble du
programme, a pour objectif de donner aux étudiants une bonne pratique des variables aléatoires dans le cadre fini.

Pour enrichir la pratique de la modélisation probabiliste
développée au lycée, on met en évidence qu'une situation
probabiliste finie peut être décrite par un $n$-uplet de variables
aléatoires, l'univers étant vu dans cette optique comme une source suffisante d'aléa.
L'objectif de cette présentation est de pouvoir travailler le plus tôt possible avec des événements construits en termes de variables aléatoires. La construction d'un univers fini susceptible de porter un $n$-uplet de variables aléatoires peut être présentée, mais ne constitue pas un objectif du programme.

Les exemples et activités proposés sont de nature plus
conceptuelle qu'au lycée. On pourra faire travailler les étudiants
sur des marches aléatoires ou des chaînes de Markov en temps fini,
des graphes aléatoires, des inégalités de concentration\ldots

Le programme de probabilités de première année s'achève sur une approche non asymptotique de la loi faible des grands nombres qui justifie l'approche fréquentiste des probabilités.

## A - Probabilités sur un univers fini, variables aléatoires et lois

<table>
<tr><td>a) Univers, événements, variables aléatoires</td><td></td></tr>

<tr><td>Lien entre vocabulaire ensembliste et vocabulaire des probabilités.</td><td>
On se limite au cas d'un univers fini.

Événement élémentaire (singleton), système complet d'événements,
événements disjoints (ou incompatibles).</td></tr>
<tr><td> 
Une variable aléatoire $X$ est une application définie sur l'univers
$\Omega$ à valeurs dans un ensemble $E$.</td><td> Notations $\{ X \in A \}$
et $(X \in A)$.</td></tr>
<tr><td> 
b) Espaces probabilisés finis</td><td></td></tr>
<tr><td>Probabilité sur un univers fini.</td><td>
Espace probabilisé fini $(\Omega , P)$. 

Notations $P(X \in A)$, $P(X = x)$ et $P(X \leqslant x)$.</td></tr>
<tr><td>
Une distribution de probabilités sur un ensemble $E$ 
est une famille d'éléments de $\R^+$ indexée par $E$ 
et de somme~1.</td><td> 
Une probabilité $P$ sur $\Omega$ est déterminée par la distribution de
probabilités $(P (\{ \omega \}))_{\omega \in \Omega}$.</td></tr>
<tr><td>

Probabilité uniforme.</td><td></td></tr>
<tr><td>

Probabilité de la réunion ou de la différence de deux événements, de
l'événement contraire. Croissance.</td><td>
La formule du crible est hors programme.</td></tr>
<tr><td> 
c) Probabilités conditionnelles</td><td></td></tr>
<tr><td>
Si $P (B) > 0$, la probabilité conditionnelle de $A$ sachant $B$ est
définie par la relation 
$P (A|B) = P_B (A) = \dfrac{P (A \cap B)}{P(B)}.$ 
  
L'application $P_B$ est une probabilité.</td><td></td></tr>
<tr><td> 

Formules des probabilités composées, des probabilités totales, de
Bayes.</td><td>
Par convention, $P (A|B) P (B) = 0$ lorsque $P (B) = 0$.</td></tr>
<tr><td> 
d) Loi d'une variable aléatoire</td><td></td></tr>
<tr><td>Loi $P_X$ d'une variable aléatoire $X$ à valeurs dans $E$.</td><td>
La probabilité $P_X$ est déterminée par la distribution de probabilités
$(P (X = x))_{x \in E}$. 

On note $X \sim Y$ la relation $P_X=P_Y$.</td></tr>
<tr><td>

Variable aléatoire $f (X)$.</td><td>
Si $X \sim Y$ alors $f (X) \sim f (Y)$.</td></tr>
<tr><td>

Variable uniforme sur un ensemble fini non vide $E$.</td><td>
Notation $X\sim \mathcal{U} (E)$.</td></tr>
<tr><td>
Variable de Bernoulli de paramètre $p \in [ 0 , 1 ]$.</td><td>
Notation $ X\sim \mathcal{B} (p)$.

Interprétation comme succès d'une expérience.
</td></tr>
<tr><td>
Variable binomiale de paramètres $n \in \N^*$ et $p\in [ 0 , 1 ]$.</td><td>
Notation $X\sim \mathcal{B} (n , p)$.</td></tr>
<tr><td>

Loi conditionnelle d'une variable aléatoire $X$ sachant un événement
$A$.</td></tr>
<tr><td>
Couple de variables aléatoires. Loi conjointe, lois marginales.</td><td>
Un couple de variables aléatoires
est une variable aléatoire à valeurs dans un produit.

Notation $P (X = x, Y = y)$.

Extension aux $n$-uplets de variables aléatoires.</td></tr>
<tr><td>e) Événements indépendants</td><td></td></tr>
<tr><td>Les événements $A$ et $B$ sont indépendants si
$P (A \cap B) = P (A) P(B)$.</td><td>
Si $P (B) > 0$, l'indépendance de $A$ et $B$ s'écrit $P (A|B)= P (A)$.</td></tr>
<tr><td>

Famille finie d'événements indépendants.</td><td>
L'indépendance deux à deux n'implique pas l'indépendance.</td></tr>
<tr><td>

Si $A$ et $B$ sont indépendants, $A$ et $\overline{B}$ le sont aussi.</td><td>
Extension au cas de $n$ événements.</td></tr>
<tr><td>f) Variables aléatoires indépendantes</td><td></td></tr>
<tr><td>Les variables aléatoires $X$ et $Y$ définies sur l'univers $\Omega$
sont indépendantes si pour tout $A \in \mathcal{P} (X(\Omega))$ et
tout $B \in \mathcal{P} (Y (\Omega))$, les événements $(X \in A)$ et
$(Y \in B)$ sont indépendants.</td><td>
Notation $X \independent Y$.
Cette condition équivaut au fait que
la distribution de probabilités de $(X , Y)$ est
donnée par $P\bigl((X,Y)=(x,y)\bigr)=P(X=x)P(Y=y)$.</td></tr>
<tr><td>
	
Extension aux $n$-uplets de variables aléatoires.</td><td>
Modélisation de $n$ expériences aléatoires indépendantes
par une suite finie $(X_i)_{1\leqslant i \leqslant n}$
de variables aléatoires indépendantes.</td></tr>
<tr><td>

Si $X_1, \ldots, X_n$ sont indépendantes de loi $\mathcal{B} (p)$,
alors $X_1 + \cdots + X_n$ suit la loi $\mathcal{B}(n , p)$.</td><td>
Interprétation : nombre de succès lors de la répétition de
$n$ expériences indépendantes ayant chacune 
la probabilité $p$ de succès.</td></tr>
<tr><td>

Si les variables aléatoires $X$ et $Y$ sont indépendantes, alors $f(X)$ et $g(Y)$ sont indépendantes.</td><td></td></tr>
<tr><td>

Lemme des coalitions : si les variables aléatoires 
$X_1, \ldots, X_n$ sont indépendantes, alors
$f(X_1, \ldots, X_m)$ et $g(X_{m+1},\ldots, X_n)$ le sont aussi.</td><td>
La démonstration est hors programme.

Extension au cas de plus de deux coalitions.

</td></tr>
</table>



## B - Espérance et variance

<table>
<tr><td>a) Espérance d'une variable aléatoire réelle ou complexe</td><td></td></tr>
<tr><td>
Espérance
$\ds \textrm{E} (X) = \sum_{x \in X (\Omega)} x P (X= x)$
d'une variable aléatoire $X$.</td><td>
L'espérance est un indicateur de position. 

Formule $\ds \textrm{E} (X) = 
\sum_{\omega \in \Omega}  X (\omega) P (\{\omega \})$. 

Variable aléatoire centrée.</td></tr>
<tr><td>
Linéarité, positivité, croissance, inégalité triangulaire.</td></tr>
<tr><td>
Espérance d'une variable constante, de Bernoulli, binomiale.</td><td>
Exemple : $\textrm{E} (\mathbb{1}_A) = P (A)$.</td></tr>
<tr><td> 

Formule de transfert : $\ds \textrm{E} \big( f (X)\big) = \sum_{x \in X (\Omega)} f (x)  P (X = x)$.</td><td>
On souligne que la formule de transfert s'applique en particulier aux couples et aux $n$-uplets.</td></tr>
<tr><td>
  
Si $X$ et $Y$ sont indépendantes, alors $\textrm{E} (XY) = \textrm{E}(X) \textrm{E} (Y)$.</td><td>
Extension au cas de $n$ variables aléatoires indépendantes.</td></tr>
<tr><td> b) Variance d'une variable aléatoire réelle, écart type et
  covariance</td><td></td></tr>

<tr><td>Variance et écart type d'une variable aléatoire réelle.</td><td>
Variance et écart type sont des indicateurs de dispersion. 

Variable aléatoire réduite.</td></tr>
<tr><td>
Relation $\textrm{V} (aX+b) = a^2 \textrm{V} (X)$.</td><td> Si $\sigma (X) > 0$, la
variable $\dfrac{X - \textrm{E} (X)}{\sigma (X)}$ est centrée
réduite.</td></tr>
<tr><td> 
Relation $\textrm{V} (X) = \textrm{E} (X^2) - \textrm{E} (X)^2$.</td></tr>
<tr><td>
Variance d'une variable de Bernoulli, d'une variable binomiale.</td></tr>
<tr><td>
Covariance de deux variables aléatoires.</td><td>
Deux variables aléatoires
dont la covariance est nulle sont dites décorrélées.</td></tr>
<tr><td> 
Relation $\mathrm{Cov} (X , Y) = \textrm{E} (XY) - \textrm{E} (X)
\textrm{E} (Y)$, cas de deux variables indépendantes.</td></tr>
<tr><td> 
Variance d'une somme, cas de variables décorrélées.</td><td>
On retrouve la variance d'une variable binomiale.  </td></tr>
<tr><td>c) Inégalités probabilistes</td><td></td></tr>
<tr><td>Inégalité de Markov.</td><td>
Application à l'obtention d'inégalités de concentration.</td></tr>
<tr><td> 
Inégalité de Bienaymé-Tchebychev.</td><td>
Application à une moyenne de variables indépendantes de même loi, interprétation fréquentiste.</td></tr>
</table>


## Exercice de la banque

Pas d'exercice de banque cette semaine. Les exercices peuvent mêler programme de proba et programme de spé d'algèbre ou d'analyse. On se limite aux probas finies mais on peut faire tendre $n$ vers $+\infty$...


## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 