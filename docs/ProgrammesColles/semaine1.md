# Suites et séries de fonctions

Tout le programme de 1A sur les séries numériques.

## A - Compléments sur les séries numériques


*Cette section a pour objectif de consolider et d'élargir les acquis de
première année sur les séries, notamment la convergence absolue, en
vue de l'étude des probabilités discrètes et des séries de fonctions.
L'étude de la semi-convergence n'est pas un objectif du programme.*


<table>
<tr><td>a) Compléments sur les séries numériques</td></tr>

  
<tr>
<td>Technique de comparaison série-intégrale.
</td>
<td>
Les étudiants doivent savoir utiliser la comparaison série-intégrale
pour établir des convergences et des divergences
de séries, estimer des sommes partielles de séries
divergentes ou des restes de séries convergentes 
dans le cas d'une fonction monotone.</td></tr>

<tr><td>Formule de Stirling : équivalent de $n!$.
</td><td>
La démonstration n'est pas exigible.
</td></tr>

<tr><td>Règle de d'Alembert.</td><td></td>

<tr><td>Théorème spécial des séries alternées, majoration et signe du reste.
</td><td>La transformation d'Abel est hors programme.</td></tr>


<tr><td>Produit de Cauchy de deux séries absolument convergentes.</td><td>
La démonstration n'est pas exigible.</td></tr>
</table>



## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.aeif.fr/cours-psi/Banque%20dexercices/)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 1, les exercices de la banque au programme sont les exercices de 1 à 28 (les élèves les ont eus à préparer pendant les vacances).

## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 