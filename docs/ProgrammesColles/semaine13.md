# Révisions de probas de première année.


Cette section, qui a vocation à interagir avec l'ensemble du
programme, a pour objectif de donner aux étudiants une bonne pratique des variables aléatoires dans le cadre fini.

Pour enrichir la pratique de la modélisation probabiliste
développée au lycée, on met en évidence qu'une situation
probabiliste finie peut être décrite par un $n$-uplet de variables
aléatoires, l'univers étant vu dans cette optique comme une source suffisante d'aléa.
L'objectif de cette présentation est de pouvoir travailler le plus tôt possible avec des événements construits en termes de variables aléatoires. La construction d'un univers fini susceptible de porter un $n$-uplet de variables aléatoires peut être présentée, mais ne constitue pas un objectif du programme.

Les exemples et activités proposés sont de nature plus
conceptuelle qu'au lycée. On pourra faire travailler les étudiants
sur des marches aléatoires ou des chaînes de Markov en temps fini,
des graphes aléatoires, des inégalités de concentration...

Le programme de probabilités de première année s'achève sur une approche non asymptotique de la loi faible des grands nombres qui justifie l'approche fréquentiste des probabilités.

## A - Probabilités sur un univers fini, variables aléatoires et lois

<table>
<tr><td>a) Univers, événements, variables aléatoires</td><td></td></tr>

<tr><td>Lien entre vocabulaire ensembliste et vocabulaire des probabilités.</td><td>
On se limite au cas d'un univers fini.

Événement élémentaire (singleton), système complet d'événements,
événements disjoints (ou incompatibles).</td></tr>
<tr><td> 
Une variable aléatoire $X$ est une application définie sur l'univers
$\Omega$ à valeurs dans un ensemble $E$.</td><td> Notations $\{ X \in A \}$
et $(X \in A)$.</td></tr>
<tr><td> 
b) Espaces probabilisés finis</td><td></td></tr>
<tr><td>Probabilité sur un univers fini.</td><td>
Espace probabilisé fini $(\Omega , P)$. 

Notations $P(X \in A)$, $P(X = x)$ et $P(X \leqslant x)$.</td></tr>
<tr><td>
Une distribution de probabilités sur un ensemble $E$ 
est une famille d'éléments de $\R^+$ indexée par $E$ 
et de somme 1.</td><td> 
Une probabilité $P$ sur $\Omega$ est déterminée par la distribution de
probabilités $(P (\{ \omega \}))_{\omega \in \Omega}$.</td></tr>
<tr><td>

Probabilité uniforme.</td><td></td></tr>
<tr><td>

Probabilité de la réunion ou de la différence de deux événements, de
l'événement contraire. Croissance.</td><td>
La formule du crible est hors programme.</td></tr>
<tr><td> 
c) Probabilités conditionnelles</td><td></td></tr>
<tr><td>
Si $P (B) > 0$, la probabilité conditionnelle de $A$ sachant $B$ est
définie par la relation 
$P (A|B) = P_B (A) = \dfrac{P (A \cap B)}{P(B)}.$ 
  
L'application $P_B$ est une probabilité.</td><td></td></tr>
<tr><td> 

Formules des probabilités composées, des probabilités totales, de
Bayes.</td><td>
Par convention, $P (A|B) P (B) = 0$ lorsque $P (B) = 0$.</td></tr>
<tr><td> 
d) Loi d'une variable aléatoire</td><td></td></tr>
<tr><td>Loi $P_X$ d'une variable aléatoire $X$ à valeurs dans $E$.</td><td>
La probabilité $P_X$ est déterminée par la distribution de probabilités
$(P (X = x))_{x \in E}$. 

On note $X \sim Y$ la relation $P_X=P_Y$.</td></tr>
<tr><td>

Variable aléatoire $f (X)$.</td><td>
Si $X \sim Y$ alors $f (X) \sim f (Y)$.</td></tr>
<tr><td>

Variable uniforme sur un ensemble fini non vide $E$.</td><td>
Notation $X\sim \mathcal{U} (E)$.</td></tr>
<tr><td>
Variable de Bernoulli de paramètre $p \in [ 0 , 1 ]$.</td><td>
Notation $ X\sim \mathcal{B} (p)$.

Interprétation comme succès d'une expérience.
</td></tr>
<tr><td>
Variable binomiale de paramètres $n \in \N^*$ et $p\in [ 0 , 1 ]$.</td><td>
Notation $X\sim \mathcal{B} (n , p)$.</td></tr>
<tr><td>

Loi conditionnelle d'une variable aléatoire $X$ sachant un événement
$A$.</td></tr>
<tr><td>
Couple de variables aléatoires. Loi conjointe, lois marginales.</td><td>
Un couple de variables aléatoires
est une variable aléatoire à valeurs dans un produit.

Notation $P (X = x, Y = y)$.

Extension aux $n$-uplets de variables aléatoires.</td></tr>
<tr><td>e) Événements indépendants</td><td></td></tr>
<tr><td>Les événements $A$ et $B$ sont indépendants si
$P (A \cap B) = P (A) P(B)$.</td><td>
Si $P (B) > 0$, l'indépendance de $A$ et $B$ s'écrit $P (A|B)= P (A)$.</td></tr>
<tr><td>

Famille finie d'événements indépendants.</td><td>
L'indépendance deux à deux n'implique pas l'indépendance.</td></tr>
<tr><td>

Si $A$ et $B$ sont indépendants, $A$ et $\overline{B}$ le sont aussi.</td><td>
Extension au cas de $n$ événements.</td></tr>
<tr><td>f) Variables aléatoires indépendantes</td><td></td></tr>
<tr><td>Les variables aléatoires $X$ et $Y$ définies sur l'univers $\Omega$
sont indépendantes si pour tout $A \in \mathcal{P} (X(\Omega))$ et
tout $B \in \mathcal{P} (Y (\Omega))$, les événements $(X \in A)$ et
$(Y \in B)$ sont indépendants.</td><td>
Notation $X   \bot\!\bot Y$.
Cette condition équivaut au fait que
la distribution de probabilités de $(X , Y)$ est
donnée par $P\bigl((X,Y)=(x,y)\bigr)=P(X=x)P(Y=y)$.</td></tr>
<tr><td>
	
Extension aux $n$-uplets de variables aléatoires.</td><td>
Modélisation de $n$ expériences aléatoires indépendantes
par une suite finie $(X_i)_{1\leqslant i \leqslant n}$
de variables aléatoires indépendantes.</td></tr>
<tr><td>

Si $X_1, \ldots, X_n$ sont indépendantes de loi $\mathcal{B} (p)$,
alors $X_1 + \cdots + X_n$ suit la loi $\mathcal{B}(n , p)$.</td><td>
Interprétation : nombre de succès lors de la répétition de
$n$ expériences indépendantes ayant chacune 
la probabilité $p$ de succès.</td></tr>
<tr><td>

Si les variables aléatoires $X$ et $Y$ sont indépendantes, alors $f(X)$ et $g(Y)$ sont indépendantes.</td><td></td></tr>
<tr><td>

Lemme des coalitions : si les variables aléatoires 
$X_1, \ldots, X_n$ sont indépendantes, alors
$f(X_1, \ldots, X_m)$ et $g(X_{m+1},\ldots, X_n)$ le sont aussi.</td><td>
La démonstration est hors programme.

Extension au cas de plus de deux coalitions.

</td></tr>
</table>



## B - Espérance et variance

<table>
<tr><td>a) Espérance d'une variable aléatoire réelle ou complexe</td><td></td></tr>
<tr><td>
Espérance
$\ds \textrm{E} (X) = \sum_{x \in X (\Omega)} x P (X= x)$
d'une variable aléatoire $X$.</td><td>
L'espérance est un indicateur de position. 

Formule $\ds \textrm{E} (X) = 
\sum_{\omega \in \Omega}  X (\omega) P (\{\omega \})$. 

Variable aléatoire centrée.</td></tr>
<tr><td>
Linéarité, positivité, croissance, inégalité triangulaire.</td></tr>
<tr><td>
Espérance d'une variable constante, de Bernoulli, binomiale.</td><td>
Exemple : $\textrm{E} (\mathbb{1}_A) = P (A)$.</td></tr>
<tr><td> 

Formule de transfert : $\ds \textrm{E} \big( f (X)\big) = \sum_{x \in X (\Omega)} f (x)  P (X = x)$.</td><td>
On souligne que la formule de transfert s'applique en particulier aux couples et aux $n$-uplets.</td></tr>
<tr><td>
  
Si $X$ et $Y$ sont indépendantes, alors $\textrm{E} (XY) = \textrm{E}(X) \textrm{E} (Y)$.</td><td>
Extension au cas de $n$ variables aléatoires indépendantes.</td></tr>
<tr><td> b) Variance d'une variable aléatoire réelle, écart type et
  covariance</td><td></td></tr>

<tr><td>Variance et écart type d'une variable aléatoire réelle.</td><td>
Variance et écart type sont des indicateurs de dispersion. 

Variable aléatoire réduite.</td></tr>
<tr><td>
Relation $\textrm{V} (aX+b) = a^2 \textrm{V} (X)$.</td><td> Si $\sigma (X) > 0$, la
variable $\dfrac{X - \textrm{E} (X)}{\sigma (X)}$ est centrée
réduite.</td></tr>
<tr><td> 
Relation $\textrm{V} (X) = \textrm{E} (X^2) - \textrm{E} (X)^2$.</td></tr>
<tr><td>
Variance d'une variable de Bernoulli, d'une variable binomiale.</td></tr>
<tr><td>
Covariance de deux variables aléatoires.</td><td>
Deux variables aléatoires
dont la covariance est nulle sont dites décorrélées.</td></tr>
<tr><td> 
Relation $\mathrm{Cov} (X , Y) = \textrm{E} (XY) - \textrm{E} (X)
\textrm{E} (Y)$, cas de deux variables indépendantes.</td></tr>
<tr><td> 
Variance d'une somme, cas de variables décorrélées.</td><td>
On retrouve la variance d'une variable binomiale.  </td></tr>
<tr><td>c) Inégalités probabilistes</td><td></td></tr>
<tr><td>Inégalité de Markov.</td><td>
Application à l'obtention d'inégalités de concentration.</td></tr>
<tr><td> 
Inégalité de Bienaymé-Tchebychev.</td><td>
Application à une moyenne de variables indépendantes de même loi, interprétation fréquentiste.</td></tr>
</table>





## Probabilités de deuxième année



### Variables aléatoires discrètes


On généralise l'étude des variables aléatoires à valeurs dans un
ensemble fini menée en première année aux variables aléatoires
discrètes. Ces outils permettent d'aborder, sur des exemples
simples, l'étude de procédés stochastiques à temps discret. La mise
en place du cadre de cette étude se veut à la fois minimale,
pratique et rigoureuse :

-  la notion de tribu n'appelle aucun autre développement que sa définition ;

-  l'étude de la dénombrabilité d'un ensemble et la construction d'espaces probabilisés sont hors programme ;

-  les diverses notions de convergences (presque sûre, en probabilité, en loi, etc.) sont hors programme. 


Toutes les variables aléatoires mentionnées dans le programme sont
implicitement supposées discrètes.


La notion de variable à densité est hors programme.


La notion d'espérance conditionnelle est hors programme. 


### A - Ensembles dénombrables, familles sommables

Ce préambule propose une introduction a minima de la dénombrabilité et des familles sommables, afin de poser les bases 
de vocabulaire, méthodes et résultats qui seront admis, et directement
utilisés. Chaque professeur est libre d'en adapter le contenu au
niveau de formalisme qu'il juge préférable pour ses étudiants.

Ces notions ne feront l'objet d'aucune évaluation spécifique, et leur usage est strictement réservé au contexte probabiliste.

- Un ensemble est dit (au plus) dénombrable s'il est en bijection avec (une partie de) $\N$, c'est-à-dire s'il peut être décrit en extension sous la forme $\{ x_i, i \in I\}$ où $I = \N$ ($I \subset \N$) avec des $x_i$ distincts. 


Sont dénombrables : $\Z$, un produit cartésien d'un nombre fini d'ensembles dénombrables, une union au plus dénombrable d'ensembles dénombrables. Une partie d'un ensemble dénombrable est au plus dénombrable. 


- En vue de généraliser les sommes finies et les sommes de séries de réels positifs, on admet sans soulever de difficulté qu'on sait associer à toute famille au plus dénombrable $(x_i)_{i \in I}$ d'éléments de $[ 0 , + \infty ]$ sa somme $\displaystyle \sum_{i \in I} x_i \in [ 0 , + \infty ]$, et que pour tout découpage en paquets $I=\displaystyle\bigcup_{n\in\N}I_n$ de $I$, $\displaystyle\sum_{i\in I}x_i=\sum_{n=0}^{+\infty}\Bigl(\sum_{i\in I_n} x_i\Bigr)$. 


La famille $(x_i)_{i\in I}$ d'éléments de $[0,+\infty]$
est dite sommable si $\displaystyle\sum_{i\in  I}x_i<\infty$. En pratique, dans le cas positif, les étudiants
peuvent découper, calculer et majorer leurs sommes directement, la
finitude de la somme valant preuve de sommabilité. 


- Une famille $(x_i)_{i\in I}$ au plus dénombrable de nombres complexes est dite sommable si $(|x_i|)_{i\in I}$ l'est. Pour $I =\N$, la sommabilité d'une suite équivaut à la convergence absolue de la série associée. Si $|x_i| \leqslant y_i$ pour tout $i\in I$,  la sommabilité de $(y_i)_{i\in I}$ implique celle de $(x_i)_{i\in I}$.

En cas de sommabilité, les sommes se manipulent naturellement grâce
aux propriétés suivantes : croissance, linéarité, sommation par paquets, théorème de Fubini, produit de deux sommes. 


### B - Probabilités, variables aléatoires discrètes et lois usuelles}

<table>
<tr><td>a) Univers, événements, variables aléatoires discrètes</td></tr>

<tr><td>Univers $\Omega$, tribu $\mathcal{A}$. 
Espace probabilisable $(\Omega, \mathcal{A})$. </td><td>

On se limite à la définition et à la stabilité par les opérations
ensemblistes finies ou dénombrables.
  
Traduction de la réalisation des événements 
	
  $\displaystyle\bigcap_{n=0}^{+\infty} A_n$ à l'aide des
  quantificateurs $\exists$ et $\forall$. 
  </td></tr>
<tr><td>
  Événements.
  </td><td> Généralisation du vocabulaire relatif aux événements introduit en
  première année.

  </td></tr>
<tr><td>

  Une variable aléatoire discrète $X$ est une application définie sur
  $\Omega$, telle que
  $X(\Omega)$ est au plus dénombrable et,
  pour tout $x\in X(\Omega)$, $X^{-1}(\{x\})$ est un événement.
  </td><td>
  L'univers $\Omega$ n'est en général pas explicité.

  

  Notations $(X=x)$, $\{X=x\}$, $(X\in A)$. 

  Notation $(X\geqslant x)$ (et analogues) lorsque $X$ est à valeurs réelles. 
  </td></tr>
<tr><td>

  
b) Probabilité</td></tr>
<tr><td>  Probabilité sur $(\Omega,\mathcal{A})$, $\sigma$-additivité. 

  Espace
  probabilisé $(\Omega, \mathcal{A}, P)$. 
  </td><td>
  Notation $P(A)$.</td></tr>
<tr><td>

  Probabilité de la réunion ou de la différence de deux événements, de
  l'événement contraire. 
  </td><td> </td></tr>
<tr><td>
  Croissance de la probabilité.
  </td><td> </td></tr>
<tr><td>
  Continuité croissante, continuité décroissante.
  </td><td>
  Application : pour une suite $(A_n)_{n\in\N}$ d'événements (non nécessairement monotone), 
limites  quand $n$ tend vers l'infini de 

$$
P\left(\bigcup_{k=0}^n A_k\right) 
\quad \text{et}\quad 
P\left(\bigcap_{k=0}^n A_k\right).
$$

  </td></tr>
<tr><td>
   Sous-additivité : 
  $\displaystyle P\Bigl(\bigcup_{n=0}^{+\infty} A_n\Bigr) \leqslant
  \sum_{n=0}^{+\infty}P(A_n)$.
</td><td> 
  En cas de divergence de la série  à termes
  positifs $\sum P(A_n)$, on rappelle que 

  $\displaystyle
  \sum_{n=0}^{+\infty} P(A_n)=+\infty$. </td></tr>
<tr><td> 
   Événement presque sûr, événement
  négligeable.   
  </td><td> Système quasi-complet d'événements.  </td></tr>
<tr><td>

c) Probabilités conditionnelles</td></tr>
<tr><td>  Si $P(B)>0$, la probabilité conditionnelle de $A$ sachant $B$ est
  définie par la relation $P(A|B)=P_B(A)=\dfrac{P(A\cap B)}{P(B)}$.
  </td><td>
  </td></tr>
<tr><td>
  L'application $P_B$ définit une probabilité.
  </td><td> </td></tr>
<tr><td>
  Formule des probabilités composées.
  </td><td> </td></tr>
<tr><td>
  Formule des probabilités totales.
  </td><td> Si $(A_n)_{n\geqslant 0}$ est un système complet ou quasi-complet d'événements, alors



    $P(B) 
    = \displaystyle \sum_{n=0}^{+\infty} P(B \cap A_n) 
    = \displaystyle \sum_{n=0}^{+\infty} P(B|A_n) P(A_n)
    $



  On rappelle la convention $P(B|A_n)P(A_n)=0$ lorsque $P(A_n)=0$. 
  </td></tr>
<tr><td>
  Formule de Bayes.
  </td><td> </td></tr>
<tr><td>

d) Loi d'une variable aléatoire discrète</td></tr>
<tr><td>Loi $P_X$ d'une variable aléatoire discrète.</td><td>
La probabilité $P_X$ est déterminée par la distribution de
probabilités $(P(X=x))_{x\in X(\Omega)}$.
  
On note $X\sim Y$ lorsque les variables $X$ et $Y$ suivent la même
loi, sans soulever de difficulté sur cette notation. </td></tr>
<tr><td>

Variable aléatoire $f(X)$.

Si $X\sim Y$ alors $f(X)\sim f(Y)$.</td><td>
On ne soulève aucune difficulté sur le fait que $f(X)$ est
une variable aléatoire.
 </td></tr>
<tr><td>[1ex]

Variable géométrique de paramètre $p\in\mathopen]0,1\mathclose[$ :
$
    \forall k\in \N^*,\; P(X=k) = p (1-p)^{k-1}.
    $
  </td><td>
  Notation $X\sim\mathcal{G}(p)$.
  
  Relation $P(X>k) = (1-p)^k$. 

Interprétation comme rang du premier succès dans une suite illimitée 
d'épreuves de Bernoulli indépendantes et de même paramètre $p$.
 </td></tr>
<tr><td>

Variable de Poisson de paramètre $\lambda > 0$ :

  
$
    \forall k\in \N,\; P(X=k) =
    \mathrm{e}^{-\lambda} \dfrac{\lambda^k}{k!}.
    $
</td><td> Notation $X\sim\mathcal{P}(\lambda)$.

Interprétation en termes d'événements rares.</td></tr>
<tr><td>

Couple de variables aléatoires discrètes.</td><td>
Un couple de variables aléatoires est une variable aléatoire à
valeurs dans un produit.

Notation $P(X=x, Y=y)$.</td></tr>
<tr><td>
  
Loi conjointe, lois marginales.</td><td>
Extension aux $n$-uplets de variables aléatoires.</td></tr>
<tr><td>

Loi conditionnelle de $Y$ sachant un événement $A$.
</td><td></td></tr>
<tr><td>
  

 

e) Événements indépendants</td></tr>
<tr><td>Indépendance de deux événements.</td><td>
 Si $P(B)>0$, l'indépendance de $A$ et $B$ équivaut à
  

    $P(A|B)=P(A)$.</td></tr>
<tr><td>
  
Indépendance d'une famille finie d'événements.</td><td>
L'indépendance deux à deux n'entraîne pas l'indépendance. </td></tr>
<tr><td>
Si $A$ et $B$ sont indépendants, $A$ et $\overline{B}$ le sont
 aussi.</td><td>
 Extension au cas de $n$ événements. </td></tr>
<tr><td>

f) Variables aléatoires indépendantes</td></tr>

<tr><td>Deux variables aléatoires discrètes $X$ et $Y$ définies sur $\Omega$
sont indépendantes si, pour tout $A\subset X(\Omega)$ et $B\subset
 Y(\Omega)$, les événements $(X \in A)$ et $(Y\in B)$ sont
 indépendants. 
 </td><td>Notation $X   \bot\!\bot Y$. 
  
De façon équivalente, la distribution 
de probabilités de $(X,Y)$ est donnée par 
$ P(X=x,Y=y) = P(X=x) P(Y=y) $.
 
Extension au cas de $n$ variables aléatoires.
</td></tr>
<tr><td>
Suites de variables aléatoires indépendantes, suites i.i.d.</td><td> 
On ne soulève aucune difficulté quant à l'existence d'un espace
probabilisé portant une suite i.i.d.

Modélisation du jeu de pile ou face infini : suite i.i.d. de variables
de Bernoulli.</td></tr>
<tr><td>
Fonctions de variables indépendantes :

si $X   \bot\!\bot Y$, alors $f(X)   \bot\!\bot g(Y)$.</td><td> 
Extension au cas de plus de deux variables aléatoires.</td></tr>
<tr><td>
Lemme des coalitions :

si les variables aléatoires $X_1,\dots, X_n$
sont indépendantes, alors 
$f(X_1,\dots, X_m)$ et $g(X_{m+1},\dots,X_n)$
le sont aussi.</td><td>
  
Extension au cas de plus de deux coalitions.</td></tr>
<tr><td>
</table>


### C - Espérance et variance

<table>
a) Espérance d'une variable aléatoire discrète réelle ou complexe</td></tr>
<tr><td>Espérance d'une variable aléatoire à valeurs dans $[0,+\infty]$, définie par

$$
E(X) = 
\sum_{x\in X(\Omega)}
x P(X=x). 
$$


</td><td>

On adopte la convention $xP(X=x)=0$ lorsque $x=+\infty$ et
$P(X=+\infty)=0$.</td></tr>
<tr><td>
Variable aléatoire $X$ à valeurs réelles ou complexes d'espérance finie,
espérance de $X$.</td><td>
$X$ est d'espérance finie si la famille $\big(x P(X=x)\big)_{x\in X(\Omega)}$ est sommable.
Dans ce cas, la somme de cette famille est l'espérance de $X$.

Variable centrée.</td></tr>
<tr><td>

Pour $X$ variable aléatoire à valeurs dans $\N\cup\{+\infty\}$, relation : 

    $ E(X)
    = \displaystyle \sum_{n=1}^{+\infty} P(X \geqslant n).
    $

  </td><td>
  </td></tr>
<tr><td>
  
Espérance d'une variable géométrique, de Poisson.

</td><td></td></tr>
<tr><td>
Formule de transfert :
  
$f(X)$ est d'espérance
finie si et seulement si la famille $\big(f(x)P(X=x)\big)_{x\in X(\Omega)}$ est
sommable. Dans ce cas :

    $ E\Big(f(X)\Big) =
    \displaystyle\sum_{x \in
      X(\Omega)} f(x) P(X=x)$.</td><td>
On remarque que la formule
s'applique aux couples, aux $n$-uplets de variables aléatoires. 
</td></tr>
<tr><td>

Linéarité de l'espérance.</td><td> </td></tr>
<tr><td>

Si $|X|\leqslant Y$ et $ E(Y)<+\infty$,
alors $X$ est d'espérance finie.</td><td> </td></tr>
<tr><td>

Positivité, croissance de l'espérance.</td><td></td></tr>
<tr><td>
Si $X$ est positive et d'espérance nulle, 
alors $(X=0)$ est presque sûr.
</td><td></td></tr>
<tr><td>
 
 Pour $X$ et $Y$ deux variables aléatoires indépendantes d'espérance
finie, alors $XY$ est d'espérance finie et :  

    $ E(XY) =  E(X)  E(Y)$.


  </td><td>  
  Extension au cas de $n$ variables aléatoires.
  </td></tr>
<tr><td>  
  
b) Variance d'une variable aléatoire discrète réelle, écart type et covariance</td></tr>
<tr><td>Si $X^2$ est d'espérance finie, $X$ est d'espérance finie.</td><td></td></tr>
<tr><td>
Inégalité de Cauchy-Schwarz :

si $X^2$ et $Y^2$ sont d'espérance
finie, alors $XY$ l'est aussi et :
  

    $ E(XY)^2 \leqslant  E(X^2)  E(Y^2)$
</td><td>
   Cas d'égalité.</td></tr>
<tr><td>
  Variance, écart type.
  </td><td>
  Notations $  V(X)$, $\sigma(X)$.
  
  Variable réduite.
  </td></tr>
<tr><td>
  Relation $  V(X) =
   E(X^2)- E(X)^2$.
  </td><td> </td></tr>
<tr><td>

  Relation
  $  V(aX+b)=a^2   V(X)$.

  </td><td>
  Si $\sigma(X)>0$, la variable $\dfrac{X -  E(X)}{\sigma(X)}$
  est centrée réduite.
  </td></tr>
<tr><td>
  
  Variance d'une variable géométrique, de Poisson.

  </td><td> </td></tr>
<tr><td>
  
  Covariance de deux variables aléatoires.</td><td></td></tr>
<tr><td>
  
  Relation $Cov(X,Y) =  E(XY) -
   E(X) E(Y)$, cas de deux variables indépendantes.
  </td><td> </td></tr>
<tr><td>
  Variance d'une somme finie, cas de variables deux à deux
  indépendantes.
  </td><td>
  </td></tr>
<tr><td>
  
c) Fonctions génératrices</td></tr>
<tr><td>Fonction génératrice de la variable aléatoire $X$ à valeurs
dans $\N$ :


    $G_X(t) = \displaystyle  E\left(t^X\right) = \sum_{n=0}^{+\infty} P(X=n) t^n.$
  </td><td>
  La série entière définissant $G_X$ est de rayon $\geqslant 1$ et
  converge normalement sur $[-1,1]$. Continuité de $G_X$.

  Les étudiants doivent savoir calculer rapidement la fonction génératrice
  d'une variable aléatoire de Bernoulli, binomiale, géométrique, de
  Poisson. 
  </td></tr>
<tr><td>
La loi d'une variable aléatoire $X$ à valeurs dans $\N$ est
caractérisée par sa fonction génératrice $G_X$.</td><td></td></tr>
<tr><td>
  
La variable aléatoire $X$ est d'espérance finie si et seulement si
$G_X$ est dérivable en $1$ ; dans ce cas $\mathrm{E}(X)={G_X}'(1)$.
</td><td>La démonstration de la  réciproque n'est pas exigible.

Utilisation de $G_X$ pour calculer $ E(X)$ et $  V(X)$.
  </td></tr>
<tr><td>

Fonction génératrice d'une somme de deux variables aléatoires indépendantes à valeurs dans $\N$.
</td><td>

  Extension au cas d'une somme finie de variables aléatoires
  indépendantes. </td></tr>
<tr><td>









  
d) Inégalités probabilistes</td></tr>
<tr><td>
  Inégalité de Markov.
  </td><td> </td></tr>
<tr><td>
  Inégalité de Bienaymé-Tchebychev.
  </td><td> </td></tr>
<tr><td>
  
  Loi faible des grands nombres :

  si $(X_n)_{n\geqslant 1}$ est une
  suite i.i.d. de variables aléatoires de variance finie, alors en notant
  $S_n = 
  \displaystyle\sum_{k=1}^{n}X_k$ et $m =  E(X_1)$, pour tout
  $\varepsilon>0$ :

    $
    \displaystyle
    P\left(
      \left|
        \frac{S_n}{n} - m
      \right|
      \geqslant \varepsilon
    \right)
    \xrightarrow[n \to +\infty]{} 0
    $.
  
  </td><td>
  Loi faible des grands nombres :
  
si $(X_n)_{n\geqslant 1}$ est une suite i.i.d. de variables aléatoires de variance finie,
alors, pour tout $\varepsilon>0$, 

$$
P\left(\left|\frac{S_n}{n}-m\right|\geqslant \varepsilon\right)\underset{n\to\infty}\longrightarrow 0,
$$

où $\ds S_n=\sum_{k=1}^n X_k$ et $m=\mathrm{E}(X_1)$.

  Les étudiants doivent savoir retrouver, avec $\sigma =\sigma(X_1)$ : 


    $
    \displaystyle
    P\left(
      \left|
        \frac{S_n}{n} - m
      \right|
      \geqslant \varepsilon
    \right)
    \leqslant
    \frac{\sigma^2}{n \varepsilon^2}
    $.

  </td></tr>
<tr><td>
</table>



## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.apps.education.fr/cours-psi/Banque%20dexercices)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 11, les exercices de la banque au programme sont les exercices de 1 à 40 puis 43, 44, 47, 48, 50, 53 plus les exercices 54, 56 et 57, 59 ,61, 62,63, 64,65,67,70,73,75,77



## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 