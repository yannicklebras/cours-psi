# Suites et séries de fonctions


## B - Suites et séries de fonctions

*Cette section a pour objectif de définir différents modes de  convergence d'une suite, d'une série de fonctions et d'étudier le transfert à la limite, à la somme des propriétés des fonctions. Les fonctions sont définies sur un intervalle $I$ de $\R$ et à valeurs dans $\R$ ou $\C$.*



<table>
<tr><td>a) Modes de convergence d'une suite ou d'une série de fonctions}</td></tr>

<tr><td>Convergence simple d'une suite de fonctions. Convergence uniforme.
La convergence uniforme entraîne la convergence simple.</td><td></td></tr>

<tr><td>Norme de la convergence uniforme sur l'espace des fonctions bornées à
valeurs dans $\R$ ou $\C$.</td><td></td></tr> 



<tr><td>Convergence simple, convergence uniforme, convergence normale d'une
série de fonctions.</td><td>
Utilisation d'une majoration uniforme de $|f_n(x)|$ pour établir la
convergence normale de $\sum f_n$.</td></tr>  


<tr><td>La convergence normale entraîne la convergence uniforme.
</td><td> La convergence normale entraîne la convergence absolue en tout point.</td></tr>


<tr><td>b) Régularité de la limite d'une suite de fonctions</td></tr>

<tr><td>Continuité de la limite d'une suite de fonctions :

si une suite $(f_n)$ de fonctions continues sur $I$ converge
uniformément vers $f$ sur $I$, alors $f$ est continue sur $I$.</td><td>


En pratique, on vérifie la convergence uniforme sur tout segment, ou
sur d'autres intervalles adaptés à la situation.
</td></tr>


<tr><td>Intégration sur un segment de la limite d'une suite de fonctions :

si une suite $(f_n)$ de fonctions continues converge uniformément vers
$f$ sur $[a,b]$ alors : 

$\displaystyle
\int_a^b f_n(t) \,\mathrm{d}\!t \xrightarrow[n \to +\infty]{} 
\int_a^b f(t) \,\mathrm{d}\!t
$.

</td><td></td></tr>

<tr><td>Dérivabilité de la limite d'une suite de fonctions :

si une suite $(f_n)$ de fonctions de classe $\mathcal{C}^1$ sur un intervalle 
$I$ converge simplement sur $I$ vers une fonction $f$, et si la suite
$(f'_n)$ converge uniformément sur $I$ vers une fonction $g$, alors $f$ est de
classe $\mathcal{C}^1$ sur $I$ et $f'=g$.</td><td>



En pratique, on vérifie la convergence uniforme sur tout segment, ou
sur d'autres intervalles adaptés à la situation.
</td></tr>

<tr><td>Extension aux suites de fonctions de classe $\mathcal{C}^k$, sous l'hypothèse
de convergence uniforme de $(f^{(k)}_n)$ et de convergence simple de
$(f^{(j)}_n)$ pour $0\leqslant j<k$.
</td><td>
</td></tr>

<tr><td>c) Régularité de la somme d'une série de fonctions</td><td></td></tr>


<tr><td>Continuité de la somme d'une série de fonctions :

si une série $\sum f_n$ de fonctions continues sur $I$ converge uniformément
sur $I$, alors 
sa somme est continue sur $I$.</td><td>


En pratique, on vérifie la convergence uniforme sur tout segment, ou
sur d'autres intervalles adaptés à la situation.
</td></tr>
<tr><td>
Théorème de la double limite :

si une série $\sum f_n$ de fonctions définies sur $I$ converge uniformément
sur $I$ et si,
pour tout $n$, $f_n$ admet une limite $\ell_n$ en 
$a$ borne de $I$ (éventuellement infinie),
alors la série $\displaystyle \sum \ell_n$ converge, la somme de
la série admet une limite en $a$ et :  
\[
\sum_{n=0}^{+\infty} f_n(x) \xrightarrow[x\to a]{} \sum_{n=0}^{+\infty} \ell_n.
\]
</td><td>
La démonstration est hors programme.
</td></tr>
<tr><td>

Intégration de la somme d'une série de fonctions sur un segment :

si une série $\displaystyle \sum f_n$ de fonctions continues converge uniformément sur $[a,b]$
alors la série des intégrales est convergente et : 


$$
\displaystyle
\int_a^b \sum_{n=0}^{+\infty} f_n (t) \,\mathrm{d}\!t =
\sum_{n=0}^{+\infty} \int_a^b f_n(t) \,\mathrm{d}\!t. 
$$

</td><td>
</td></tr>
<tr><td>

Dérivation de la somme d'une série de fonctions :

si une série $\displaystyle \sum f_n$ de classe $\mathcal{C}^1$ converge simplement sur un intervalle $I$
et si la série $\displaystyle \sum f'_n$ converge uniformément sur $I$, alors
la somme $\displaystyle \sum_{n=0}^{+\infty} f_n$ est de classe
$\mathcal{C}^1$ sur $I$ et sa dérivée 
est $\displaystyle \sum_{n=0}^{+\infty} f'_n$.
</td><td>
En pratique, on vérifie la convergence uniforme sur tout segment ou
sur d'autres intervalles adaptés à la situation.

Extension à la classe $\mathcal{C}^k$ sous hypothèse similaire à celle
décrite dans le cas des suites de fonctions.

</td></tr>
</table>

## C - Séries entières


Les objectifs de cette section sont les suivants :

-  étudier la convergence d'une série entière et mettre en évidence la notion de rayon de convergence ;

- étudier les propriétés de sa somme en se limitant à la continuité dans le cas d'une variable complexe ;

- établir les développements en série entière des fonctions usuelles.

Les séries entières trouveront un cadre d'application dans la notion de fonction génératrice en probabilités et au détour d'exemples de résolution d'équations différentielles linéaires.






<table>
<tr><td>a) Rayon de convergence</td><td></td></tr>
<tr><td>
Série entière de la variable réelle, de la variable complexe.

Lemme d'Abel :

si  la suite $\left(a_n z_0^n\right)$ est bornée
alors, pour tout nombre complexe $z$ tel que
$\vert z \vert <\vert z_0\vert$, la série $\displaystyle \sum a_n z^n$ est absolument convergente.
</td><td></td></tr>
<tr><td>
Rayon de convergence $R$ défini comme borne supérieure dans
$[0,+\infty]$ de l'ensemble des réels positifs $r$ tels que la
suite $(a_n r^n)$ est bornée.</td><td> 
La série $\ds\sum a_n z^n$ converge absolument
si $|z|< R$, et elle diverge grossièrement si $|z|>R$.</td></tr>

<tr><td>
Intervalle ouvert de convergence.

Disque ouvert de convergence.</td><td></td></tr>
<tr><td>
Avec $R_a$ (resp. $R_b$) le rayon de convergence de $\displaystyle \sum a_n z^n$,
(resp. $\displaystyle \sum b_n z^n$) :

- si $a_n = O (b_n)$, alors $R_a \geqslant R_b$  ;

- si $a_n \sim b_n$, alors $R_a = R_b$.
</td><td>
Pour $\alpha\in\R$, $R\left(\displaystyle\sum n^\alpha x^n\right)=1$.


Le résultat s'applique en particulier lorsque $a_n = o(b_n)$.</td></tr>

<tr><td>
Application de la règle de d'Alembert pour les séries numériques au calcul du rayon.</td><td>
La limite du rapport $\dfrac{|a_{n+1}|}{|a_n|}$ peut être directement utilisée.</td></tr>

<tr><td>Rayon de convergence de la somme et du produit de Cauchy de deux
séries entières.</td><td></td></tr> 

<tr><td>b) Régularité de la somme d'une série entière de la variable réelle</td><td></td></tr>
<tr><td>Convergence normale d'une série entière d'une variable réelle sur tout
segment inclus dans l'intervalle ouvert de 
convergence.</td><td></td></tr>
Continuité de la somme sur l'intervalle ouvert de convergence.
</td><td>
L'étude des propriétés de la somme au bord de l'intervalle ouvert de
convergence n'est pas un objectif du programme.  
</td></tr>
<tr><td>
Primitivation d'une série entière d'une variable réelle sur
l'intervalle ouvert de convergence. 
</td><td> Relation $R\left(\displaystyle\sum a_n x^n\right)=R\left(\displaystyle\sum na_n x^n\right)$.</td></tr>
<tr><td>Caractère $\mathcal{C}^{\infty}$ de la somme d'une série entière d'une
variable réelle sur l'intervalle ouvert de convergence et obtention
des dérivées par dérivation terme à terme. 
</td><td></td></tr>
<tr><td>
Expression des coefficients d'une série entière de rayon de
  convergence strictement positif au moyen des dérivées 
successives en $0$ de sa somme. 
</td><td></td></tr>

<tr><td>c) Développement en série entière au voisinage de $0$ d'une fonction d'une variable réelle</td><td></td></tr>

<tr><td>
Fonction développable en série entière sur un intervalle $\mathopen]-r,r\mathclose[$.</td><td></td></tr>

<tr><td>Série de Taylor d'une fonction de classe $\mathcal{C}^{\infty}$.</td><td>
Formule de Taylor avec reste intégral.</td></tr>

<tr><td>Unicité du développement en série entière.</td><td></td></tr>


<tr><td>Développements des fonctions usuelles.</td><td> Les étudiants doivent
connaître les développements en série entière des fonctions : 
exponentielle, cosinus, sinus, cosinus et sinus hyperboliques,
$\arctan$, $x\mapsto \ln(1+x)$ et $x\mapsto (1+x)^\alpha$. 

Les étudiants doivent savoir développer une fonction en série entière
à l'aide d'une équation différentielle linéaire.
</td></tr>

<tr><td>d) Séries géométrique et exponentielle d'une variable complexe</td><td></td></tr>
<tr><td>Continuité de la somme d'une série entière de la variable
complexe sur le disque ouvert de convergence.
</td><td>
La démonstration est hors programme.</td></tr>
<tr><td>Développement de $\dfrac{1}{1-z}$ sur le disque unité ouvert.</td><td></td></tr>
<tr><td>Développement de $\exp(z)$ sur $\C$. </td><td> </td></tr>
</table>

## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.aeif.fr/cours-psi/Banque%20dexercices/)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 3, les exercices de la banque au programme sont les exercices de 1 à 38.

## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 