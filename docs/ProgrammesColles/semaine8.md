# Algèbre Linéaire

*Dans toute cette partie, $\K$ désigne $\:\R$ ou $\:\C$.*

## A - Compléments sur les espaces vectoriels, les endomorphismes et les matrices 

Le programme est organisé autour de trois objectifs :

-  consolider les acquis de la classe de première année ;
   
-  introduire de nouveaux concepts préliminaires à la réduction des endomorphismes : somme deplusieurs sous-espaces vectoriels, somme directe,sous-espaces stables, matrices par blocs,trace, polynômes d'endomorphismeset de matrices carrées, polynômes interpolateurs deLagrange ; 
  
- passer du point de vue vectoriel au  point de vue matriciel et inversement.  

Le programme valorise les interprétations géométriques et préconise
l'illustration des notions et résultats par de nombreuses figures.

<table>
<tr><td>a) Produit d'espaces vectoriels, somme de sous-espaces vectoriels</td></tr>

<tr><td>Produit d'un nombre fini d'espaces vectoriels ; dimension dans le cas
où ces espaces sont de dimension finie.
</td><td>
</td></tr><tr><td>

Somme, somme directe d'une famille finie de sous-espaces vectoriels.</td><td></td></tr><tr><td>

En dimension finie, base adaptée à un sous-espace vectoriel,
à une décomposition $E=\bigoplus E_i$.
</td><td>
Décomposition en somme directe obtenue par partition d'une base.</td></tr><tr><td>

Si $F_1, \ldots, F_p$ sont des sous-espaces de dimension finie,
\vspace{-3mm}
\[\dim \biggl(\sum_{i=1}^p F_i\biggr) \leqslant \sum_{i=1}^p \dim(F_i)\]

\vspace{-3mm}
avec égalité si et seulement si la somme est directe.
</td><td></td></tr><tr><td>


b) Matrices par blocs et sous-espaces stables</td></tr>

Matrices définies par blocs, opérations par blocs de tailles
  compatibles (combinaison linéaire, produit, transposition).</td><td></td></tr><tr><td> 

Déterminant d'une matrice triangulaire par blocs.</td><td>
</td></tr><tr><td>

Sous-espace vectoriel stable par un endomorphisme, endomorphisme
induit. 

Si $u$ et $v$ commutent alors le noyau de $u$
est stable par $v$.
</td><td> Traduction matricielle de la stabilité 
d'un sous-espace vectoriel par un endomorphisme et interprétation en
termes d'endomorphismes d'une matrice triangulaire ou diagonale par
blocs. 
</td></tr><tr><td>


c) Trace</td></tr>
Trace d'une matrice carrée.

Linéarité, trace d'une transposée.

Relation $\tr(AB)=\tr(BA)$. 

Invariance de la trace par similitude. Trace d'un endomorphisme d'un
espace de dimension finie. 
</td><td>Notation $\tr(A)$. </td></tr><tr><td>
d) Polynômes d'endomorphismes et de matrices carrées</td></tr>

Polynôme d'un endomorphisme, d'une matrice carrée.
</td><td> Relation $(PQ)(u)=P(u)\circ Q(u)$.</td></tr><tr><td>

Polynôme annulateur.
</td><td>Application au calcul de l'inverse et des puissances.
</td></tr><tr><td>

Deux polynômes de l'endomorphisme $u$ commutent.
</td><td>Le noyau de $P(u)$ est stable par $u$.
</td></tr><tr><td>


Adaptation de ces résultats aux matrices carrées.</td><td>
</td></tr><tr><td>
e) Interpolation de Lagrange</td></tr>
Base de $\K_n[X]$ constituée des polynômes interpolateurs
de Lagrange en $n+1$ points distincts de $\K$.</td><td>
Expression d'un polynôme $P\in\K_n[X]$ dans cette base.

La somme des polynômes interpolateurs de Lagrange en $n+1$
  points est le polynôme constant égal à 1.
</td></tr><tr><td>

Déterminant de Vandermonde.</td><td>Lien avec le problème
  d'interpolation de Lagrange. </td></tr><tr><td>
</td></tr></table>



## B - Réduction des endomorphismes et des matrices carrées 

*La réduction des endomorphismes et des matrices carrées permet d'approfondir les notions étudiées en première année. <br> Il est attendu des étudiants qu'ils maîtrisent les deux points de vue suivants :*

-  *l'aspect géométrique (sous-espaces stables, éléments propres)* ;  
-  *l'aspect algébrique (utilisation de polynômes annulateurs).*

*L'étude des classes de similitude est hors programme ainsi que la notion de polynôme minimal.*


<table>

<tr><td>a) Éléments propres </td><td></td></tr>
<tr><td>Droite stable par un endomorphisme.</td><td></td></tr><tr><td>

Valeur propre, vecteur propre (non nul), sous-espace propre d'un endomorphisme.</td><td> 
Équation aux éléments propres $u(x)=\lambda x$.

Si $u$ et $v$ commutent, les sous-espaces 
propres de $u$ sont stables par $v$. </td></tr><tr><td>


Spectre d'un endomorphisme en dimension finie.
</td><td> Notation $\mathrm{Sp}(u)$. 

La notion de valeur spectrale est hors programme. 
</td></tr><tr><td>

La somme d'une famille finie de sous-espaces propres
d'un endomorphisme est directe.
</td><td>Toute famille finie de vecteurs propres associés à des
valeurs propres distinctes est libre.</td></tr><tr><td>  


Si un polynôme $P$ annule $u$, toute valeur propre
  de $u$ est racine de $P$.
</td><td>Si $u(x)=\lambda x$, alors $P(u)(x)=P(\lambda)x$.</td></tr><tr><td>

Valeur propre, vecteur propre, sous-espace propre
et spectre d'une matrice carrée.
</td><td>
Équation aux éléments propres $AX=\lambda X$.
</td></tr><tr><td>

b) Polynôme caractéristique</td><td></td></tr>

<tr><td>Polynôme caractéristique d'une matrice carrée, d'un
endomorphisme d'un espace vectoriel de dimension finie. 
</td><td>Par convention le polynôme caractéristique est unitaire.

Notations $\chi_A$, $\chi_u$.

Coefficients de degrés $0$ et $n-1$.</td></tr><tr><td>

Les valeurs propres d'un endomorphisme 
sont les racines de son polynôme caractéristique.</td><td>
Spectre complexe d'une matrice carrée réelle.</td></tr><tr><td>

Multiplicité d'une valeur propre.
Majoration de la dimension d'un
sous-espace propre par la multiplicité.</td><td>
Deux matrices semblables ont le même polynôme
caractéristique, donc les mêmes valeurs propres avec mêmes
multiplicités. </td></tr><tr><td>
Théorème de Cayley-Hamilton.
</td><td> La démonstration n'est pas exigible.</td></tr><tr><td>

c) Diagonalisation en dimension finie</td><td></td></tr>

<tr><td>Un endomorphisme d'un espace vectoriel de dimension finie
est dit diagonalisable s'il existe une base dans 
laquelle sa matrice est diagonale.</td><td>
Une telle base est constituée de vecteurs propres. 
</td></tr><tr><td>

Une matrice carrée est dite diagonalisable si elle est
semblable à une matrice diagonale.</td><td>
Interprétation en termes d'endomorphisme.

Application au calcul des puissances d'une matrice diagonalisable, à des exemples
de systèmes différentiels à coefficients 
constants.

Dans la pratique des cas numériques, on se limite à $n=2$ ou $n=3$.
</td></tr><tr><td>

Un endomorphisme d'un espace vectoriel $E$ est diagonalisable
  si et seulement si la somme de ses sous-espaces propres est égale à $E$. 
</td><td>
Exemple des projecteurs et des symétries. 
</td></tr><tr><td>

Un endomorphisme est diagonalisable si et seulement si  la somme des
dimensions de ses sous-espaces propres est égale à la dimension de
l'espace.</td><td>
Traduction matricielle.
</td></tr><tr><td>

Un endomorphisme est diagonalisable si et seulement si son polynôme
caractéristique est scindé sur $\K$ et si, pour toute
valeur propre, la dimension du sous-espace propre associé est égale à
sa multiplicité.
</td><td>
Traduction matricielle.

</td></tr><tr><td>

Un endomorphisme d'un espace vectoriel de dimension $n$ admettant $n$
valeurs propres distinctes est diagonalisable. 
</td><td>Polynôme caractéristique scindé à racines simples.

Traduction  matricielle.</td></tr><tr><td>


d) Diagonalisabilité et polynômes annulateurs</td><td></td></tr>

<tr><td> Un endomorphisme est diagonalisable
si et seulement s'il admet un polynôme annulateur scindé à racines simples.
</td><td>
La démonstration n'est pas exigible.

Traduction matricielle.

Le lemme de décomposition des noyaux est hors programme.</td></tr><tr><td>

L'endomorphisme induit par un endomorphisme diagonalisable
sur un sous-espace vectoriel stable est diagonalisable.</td><td>
</td></tr><tr><td>

Un endomorphisme $u$ est diagonalisable
si et seulement s'il admet
$\prod\limits_{\lambda \in \mathrm{Sp}(u)} (X-\lambda)$
pour polynôme annulateur.
</td><td></td></tr><tr><td>

e) Trigonalisation en dimension finie</td><td></td></tr>

<tr><td>Un endomorphisme d'un espace vectoriel de dimension finie est dit
trigonalisable s'il existe une base dans 
laquelle sa matrice est triangulaire.</td><td>

Expression de la trace et du déterminant d'un endomorphisme
trigonalisable, d'une matrice trigonalisable à l'aide des valeurs
propres. 
</td></tr><tr><td>
Une matrice carrée est dite trigonalisable si elle est
semblable à une matrice triangulaire.
</td><td>
Interprétation en termes d'endomorphisme.
</td></tr><tr><td>


Un endomorphisme est trigonalisable si et seulement si son polynôme
caractéristique est scindé sur $\K$. 
</td><td>
La démonstration n'est pas exigible.

Traduction matricielle.</td></tr><tr><td>

Toute matrice de $\mathcal{M}_n(\C)$ est
trigonalisable. 
</td><td>La technique générale de trigonalisation est hors programme. On
se limite dans la pratique à des exemples simples en petite dimension
et tout exercice de trigonalisation effective doit comporter une
indication.</td></tr>
</table>

## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.apps.education.fr/cours-psi/Banque%20dexercices)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.

Pour cette semaine 8, les exercices de la banque au programme sont les exercices de 1 à 40 puis 43, 44, 47, 48, 50, 53 plus les exercices 54, 56 et 57.

## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 