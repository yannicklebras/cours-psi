
## C - Séries entières


Les objectifs de cette section sont les suivants :

-  étudier la convergence d'une série entière et mettre en évidence la notion de rayon de convergence ;

- étudier les propriétés de sa somme en se limitant à la continuité dans le cas d'une variable complexe ;

- établir les développements en série entière des fonctions usuelles.

Les séries entières trouveront un cadre d'application dans la notion de fonction génératrice en probabilités et au détour d'exemples de résolution d'équations différentielles linéaires.






<table>
<tr><td>a) Rayon de convergence</td><td></td></tr>
<tr><td>
Série entière de la variable réelle, de la variable complexe.

Lemme d'Abel :

si  la suite $\left(a_n z_0^n\right)$ est bornée
alors, pour tout nombre complexe $z$ tel que
$\vert z \vert <\vert z_0\vert$, la série $\displaystyle \sum a_n z^n$ est absolument convergente.
</td><td></td></tr>
<tr><td>
Rayon de convergence $R$ défini comme borne supérieure dans
$[0,+\infty]$ de l'ensemble des réels positifs $r$ tels que la
suite $(a_n r^n)$ est bornée.</td><td> 
La série $\ds\sum a_n z^n$ converge absolument
si $|z|< R$, et elle diverge grossièrement si $|z|>R$.</td></tr>

<tr><td>
Intervalle ouvert de convergence.

Disque ouvert de convergence.</td><td></td></tr>
<tr><td>
Avec $R_a$ (resp. $R_b$) le rayon de convergence de $\displaystyle \sum a_n z^n$,
(resp. $\displaystyle \sum b_n z^n$) :

- si $a_n = O (b_n)$, alors $R_a \geqslant R_b$  ;

- si $a_n \sim b_n$, alors $R_a = R_b$.
</td><td>
Pour $\alpha\in\R$, $R\left(\displaystyle\sum n^\alpha x^n\right)=1$.


Le résultat s'applique en particulier lorsque $a_n = o(b_n)$.</td></tr>

<tr><td>
Application de la règle de d'Alembert pour les séries numériques au calcul du rayon.</td><td>
La limite du rapport $\dfrac{|a_{n+1}|}{|a_n|}$ peut être directement utilisée.</td></tr>

<tr><td>Rayon de convergence de la somme et du produit de Cauchy de deux
séries entières.</td><td></td></tr> 

<tr><td>b) Régularité de la somme d'une série entière de la variable réelle</td><td></td></tr>
<tr><td>Convergence normale d'une série entière d'une variable réelle sur tout
segment inclus dans l'intervalle ouvert de 
convergence.</td><td></td></tr>
Continuité de la somme sur l'intervalle ouvert de convergence.
</td><td>
L'étude des propriétés de la somme au bord de l'intervalle ouvert de
convergence n'est pas un objectif du programme.  
</td></tr>
<tr><td>
Primitivation d'une série entière d'une variable réelle sur
l'intervalle ouvert de convergence. 
</td><td> Relation $R\left(\displaystyle\sum a_n x^n\right)=R\left(\displaystyle\sum na_n x^n\right)$.</td></tr>
<tr><td>Caractère $\mathcal{C}^{\infty}$ de la somme d'une série entière d'une
variable réelle sur l'intervalle ouvert de convergence et obtention
des dérivées par dérivation terme à terme. 
</td><td></td></tr>
<tr><td>
Expression des coefficients d'une série entière de rayon de
  convergence strictement positif au moyen des dérivées 
successives en $0$ de sa somme. 
</td><td></td></tr>

<tr><td>c) Développement en série entière au voisinage de $0$ d'une fonction d'une variable réelle</td><td></td></tr>

<tr><td>
Fonction développable en série entière sur un intervalle $\mathopen]-r,r\mathclose[$.</td><td></td></tr>

<tr><td>Série de Taylor d'une fonction de classe $\mathcal{C}^{\infty}$.</td><td>
Formule de Taylor avec reste intégral.</td></tr>

<tr><td>Unicité du développement en série entière.</td><td></td></tr>


<tr><td>Développements des fonctions usuelles.</td><td> Les étudiants doivent
connaître les développements en série entière des fonctions : 
exponentielle, cosinus, sinus, cosinus et sinus hyperboliques,
$\arctan$, $x\mapsto \ln(1+x)$ et $x\mapsto (1+x)^\alpha$. 

Les étudiants doivent savoir développer une fonction en série entière
à l'aide d'une équation différentielle linéaire.
</td></tr>

<tr><td>d) Séries géométrique et exponentielle d'une variable complexe</td><td></td></tr>
<tr><td>Continuité de la somme d'une série entière de la variable
complexe sur le disque ouvert de convergence.
</td><td>
La démonstration est hors programme.</td></tr>
<tr><td>Développement de $\dfrac{1}{1-z}$ sur le disque unité ouvert.</td><td></td></tr>
<tr><td>Développement de $\exp(z)$ sur $\C$. </td><td> </td></tr>
</table>



# Intégration sur un intervalle quelconque
<i>Cette section vise les objectifs suivants :
-  étendre la notion d'intégrale étudiée en première année à des fonctions continues par morceaux sur un intervalle quelconque par lebiais des intégrales généralisées ;

-  définir, dans le cadre des fonctions continues par morceaux, la notion de fonction intégrable ;

- compléter la section dédiée aux suites et aux séries de fonctions par les théorèmes de convergence dominée et d'intégration terme à terme ;

- étudier les fonctions définies par des intégrales dépendant d'un paramètre.

On évite tout excès de rigueur dans la rédaction. Ainsi, dans
  les calculs concrets mettant en jeu l'intégration par parties ou le 
changement de variable, on n'impose pas de rappeler les hypothèses de régularité des
résultats utilisés. De même, dans l'application des 
théorèmes de passage à la limite sous l'intégrale ou de régularité des
intégrales à paramètre, on se limite à la vérification 
des hypothèses cruciales, sans insister sur la continuité par morceaux
en la variable d'intégration.

Les fonctions considérées sont définies sur un intervalle de $\:\R$ et
à valeurs dans $\:\K$, ensemble des nombres réels ou des nombres complexes.
</i>


<table>
<tr><td>a) Fonctions continues par morceaux</td></tr>

<tr><td>Fonctions continues par morceaux sur un segment, sur un intervalle de $\R$.</td><td></td></tr>
<tr><td>
Intégrale sur un segment d'une fonction continue par morceaux.</td><td>
Brève extension des propriétés de l'intégrale d'une fonction continue sur un segment étudiées en première année.
Aucune construction n'est exigible.</td></tr>
<tr><td>
b) Intégrales généralisées sur un intervalle de la forme $[a,+\infty\mathclose[$</td></tr>
<tr><td>
Pour $f$ continue par morceaux sur $[a, +\infty\mathclose[$, l'intégrale 
$\ds\int_a^{+\infty} f(t) \,\mathrm{d}\!t$ est dite convergente si 
$\ds\int_a^x f(t) \,\mathrm{d}\!t$ a une limite finie
lorsque $x$ tend vers $+\infty$.</td><td>
Notations $\ds \int_a^{+\infty} f$, $\ds\int_a^{+\infty}f(t) \,\mathrm{d}\!t.$

Intégrale convergente (resp. divergente) en $+\infty$.</td></tr>
<tr><td>

Si $f$ est continue par morceaux sur $[a,+\infty\mathclose[$ et à
valeurs positives, alors  $\ds\int_a^{+\infty}f(t)\,\mathrm{d}\!t$
converge si et seulement si 
$\ds x \mapsto \int_a^xf(t)\,\mathrm{d}\!t$ est majorée.</td><td></td></tr>
<tr><td>

Si $f$ et $g$ sont deux fonctions continues par morceaux sur
$[a,+\infty[$ telles que $0\leqslant f\leqslant g$,  
la convergence de $\ds \int_a^{+\infty}g$ implique celle
de $\displaystyle \int_a^{+\infty}f$.</td><td></td></tr>
<tr><td> c) Intégrales généralisées sur un intervalle quelconque</td></tr>
<tr><td>
Adaptation du paragraphe précédent aux fonctions
continues par morceaux définies sur un intervalle semi-ouvert ou ouvert de $\R$.</td><td>
Notations $\ds \int_a^b f$, 
$\ds\int_a^bf(t) \,\mathrm{d}\!t$.

Intégrale convergente (resp. divergente) en $b$, en $a$.</td></tr>
<tr><td>
Propriétés des intégrales généralisées : 

linéarité, positivité, croissance, relation de Chasles.</td><td></td></tr>
<tr><td>
Intégration par parties sur un intervalle quelconque :


$$\ds\int_a^b f(t)g'(t)\,\mathrm{d}\!t=
\left[fg\right]_a^b- \ds\int_a^b f'(t)g(t)\,\mathrm{d}\!t.$$</td><td>


La démonstration n'est pas exigible.

L'existence des limites finies du produit $fg$ aux bornes de l'intervalle
assure que les intégrales de $fg'$ et $f'g$ sont de même
nature.

Pour les applications pratiques, on ne demande pas de
rappeler les hypothèses de régularité.</td></tr>
<tr><td>

Changement de variable :

si $\varphi : \mathopen]\alpha,\beta\mathclose[ \to
\mathopen]a,b\mathclose[$ est une bijection strictement croissante de
classe $\mathcal{C}^1$, 
et si $f$ est continue sur $\mathopen]a,b\mathclose[$,
alors $\displaystyle \int_{a}^b f(t) \,\mathrm{d}\!t$ et 
$\displaystyle \int_\alpha^{\beta} (f \circ \varphi)(u)\,\varphi'(u) \,\mathrm{d}\!u$ sont 
de même nature, et égales en cas de convergence.
</td><td>
La démonstration n'est pas exigible.

Adaptation au cas où $ \varphi$ est strictement décroissante.

On applique ce résultat sans justification dans
les cas de changements de variable usuels.</td></tr>
<tr><td>

d) Intégrales absolument convergentes et fonctions intégrables</td></tr>


<tr><td>Intégrale absolument convergente.</td><td></td></tr>
<tr><td>

La convergence absolue implique la convergence.

Inégalité triangulaire.</td><td>
L'étude des intégrales semi-convergentes n'est pas un objectif du programme.</td></tr>
<tr><td> 

Une fonction est dite intégrable sur un intervalle $I$ si elle est
continue par morceaux sur $I$ et son intégrale sur $I$ est absolument 
convergente.</td><td>
Notations $\ds\int_I f$, $\ds\int_I f(t) \,\mathrm{d}\!t$.
  
Pour $I=[a,b\mathclose[$, (respectivement $\mathopen]a,b]$),
fonction intégrable en $b$ (resp. en $a$).</td></tr>
<tr><td> 
  
Espace vectoriel $L^1(I,\K)$ des fonctions
intégrables sur $I$ à valeurs dans $\K$.</td><td></td></tr>
<tr><td>
  
Si $f$ est continue, intégrable et positive sur $I$, et si 
$\ds\int_I f(t) \,\mathrm{d}\!t=0$, alors $f$ est identiquement nulle.</td><td></td></tr>
<tr><td> 

Théorème de comparaison : 

pour $f$ et $g$ deux fonctions continues par morceaux
sur $[a,+\infty\mathclose[$ :
<ul>
<li> si $f(t)\underset{t \to +\infty}{=}O\left(g(t)\right)$, alors l'intégrabilité de $g$  en $+\infty$ implique celle de $f$. </li>
<li> si $f(t)\underset{t \to +\infty}{\sim} g(t)$, alors l'intégrabilité de $f$  en $+\infty$ est équivalente à celle de $g$. </li>
</ul>
</td><td>
 Adaptation au cas d'un intervalle quelconque.

Le résultat s'applique en particulier si
$f(t)\underset{t \to +\infty}{=}o(g(t))$.</td></tr>
<tr><td>
  
Fonctions de référence :

pour $\alpha\in\R$,

- intégrales de Riemann : étude de l'intégrabilité de $t \mapsto \dfrac{1}{t^\alpha}$ en $+\infty$, en $0^+$ ;

- étude de l'intégrabilité de $t \mapsto e^{-\alpha t}$ en $+\infty$.

</td><td>
 L'intégrabilité de $t\mapsto \ln t$ en $0$ peut être directement
 utilisée.
    
Les résultats relatifs à l'intégrabilité de $x\mapsto
\dfrac{1}{|x-a|^\alpha}$ en $a$ peuvent être directement utilisés.

Plus généralement, les étudiants doivent savoir que
la fonction $x\mapsto f(x)$ est intégrable en $a^+$
(resp. en $b^-$) si $t\mapsto f(a+t)$ (resp. $t\mapsto f(b-t)$) l'est en $0^+$.</td></tr>
</table>




## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.aeif.fr/cours-psi/Banque%20dexercices/)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 5, les exercices de la banque au programme sont les exercices de 1 à 40.

## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 