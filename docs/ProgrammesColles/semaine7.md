# Intégration sur un intervalle quelconque


<table><tr><td>
e) Suites et séries de fonctions intégrables 
</td><td></td></tr>
<tr><td>

Pour l'application pratique des énoncés de ce paragraphe,
on vérifie les hypothèses de convergence simple et de domination 
(resp. convergence de la série des intégrales), 
sans expliciter celles relatives à la continuité par morceaux.
<br>
Théorème de convergence dominée :
<br>
si une suite $(f_n)$ de fonctions continues par morceaux
sur $I$ 
converge simplement vers une fonction $f$ continue par
morceaux sur $I$ et s'il existe 
une fonction $\varphi$ intégrable sur $I$
vérifiant $\vert f_n \vert \leqslant \varphi$ pour tout $n$, alors
les fonctions $f_n$ et $f$ sont intégrables sur $I$ et : 

$$
\displaystyle
\int_I f_n(t) \,\mathrm{d}\!t \,\,\xrightarrow[n \to +\infty]{}\,\,
\int_I f(t) \,\mathrm{d}\!t. 
$$

</td><td>

La démonstration est hors programme.</td></tr><tr><td>
 
Théorème d'intégration terme à terme :
<br>
si une série $\sum f_n$ de fonctions intégrables sur $I$
converge simplement, si sa somme est continue 
par morceaux sur $I$, et si la série 
$\ds\sum \int_I \vert f_n(t) \vert \,\mathrm{d}\!t$
converge, alors
$\ds \sum_{n=0}^{+\infty} f_n$ est intégrable sur $I$ et : 


$$
\ds
\int_I \sum_{n=0}^{+\infty} f_n (t) \,\mathrm{d}\!t =
\sum_{n=0}^{+\infty} \int_I f_n(t) \,\mathrm{d}\!t. 
$$

</td><td>
La démonstration est hors programme.
<br>

On présente des exemples sur lesquels cet énoncé
ne s'applique pas, mais dans lesquels l'intégration terme à terme 
peut être justifiée par le théorème de
convergence dominée pour les sommes partielles.</td></tr><tr><td> 
 
f) Régularité d'une fonction définie par une intégrale à paramètre
<br>
Pour l'application pratique des énoncés de ce paragraphe,
on vérifie les hypothèses de régularité par rapport à $x$ et de
domination, sans expliciter celles relatives à la continuité par morceaux par rapport à $t$.
<br>
Théorème de continuité :
<br>
si $A$ et $I$ sont deux intervalles de $\R$ et $f$ une fonction
définie sur $A\times I$, telle que :

<ul>
<li>pour tout $t\in I$, $x\mapsto f(x,t)$ est continue sur $A$ ;
<li>pour tout $x\in A$, $t\mapsto f(x,t)$ est continue par morceaux sur $I$ ;
<li>il existe une fonction $\varphi$ intégrable sur $I$, telle que pour tout $(x,t)\in A \times I$, on ait $\vert f(x,t) \vert \leqslant  \varphi(t)$ ;
</ul>

alors la fonction $\displaystyle x \mapsto \int_I f(x,t) \,\mathrm{d}\!t$ est
définie et continue sur $A$. </td><td>

  
En pratique, on vérifie l'hypothèse de domination sur tout segment
de $A$, ou sur d'autres intervalles adaptés à la situation. </td></tr><tr><td>

Théorème de convergence dominée à paramètre continu :
<br>  
si $A$ et $I$ sont deux intervalles de $\R$, $a$ une borne
de $A$ et $f$ une fonction définie sur $A\times I$
telle que :

<ul>
<li>pour tout $t\in I$, $f(x,t)\xrightarrow[x\to a]{} \ell(t)$ ;    
<li>pour tout $x\in A$, $t\mapsto f(x,t)$ et $t\mapsto \ell(t)$ sont continues par morceaux sur $I$ ;    
<li>il existe une fonction $\varphi$ intégrable sur $I$, telle que pour tout $(x,t)\in A \times I$,  on ait $\vert f(x,t) \vert \leqslant  \varphi(t)$ ;
</ul>

alors $\ell$ est intégrable sur $I$ et : 

$$
\ds
\int_I f(x,t)\,\mathrm{d}\!t \xrightarrow[x\to a]{}
\int_I \ell(t)\,\mathrm{d}\!t.
$$

</td><td>
On remarque qu'il s'agit d'une simple extension du théorème
relatif aux suites de fonctions. 
</td></tr><tr><td>
  
Théorème de dérivation :
<br>
si $A$ et $I$ sont deux intervalles de $\R$ et $f$ une fonction
définie sur $A\times I$, 
telle que :
<ul>
<li>pour tout $t\in I$, $x\mapsto f(x,t)$ est de classe $\mathcal{C}^1$ sur $A$ ;
<li>pour tout $x\in A$, $t\mapsto f(x,t)$ est intégrable sur $I$ ;  
<li>pour tout $x\in A$, $\displaystyle t\mapsto \frac{\partial f}{\partial x}(x,t)$ est continue par morceaux sur $I$ ;
<li>il existe une fonction $\varphi$ intégrable sur $I$, telle que pour tout $(x,t)\in A \times I$, on ait $\displaystyle \left\vert \frac{\partial f}{\partial x}(x,t) \right\vert \leqslant \varphi(t)$ ;
</ul>
alors la fonction $\displaystyle g:x \mapsto \int_I f(x,t) \,\mathrm{d}\!t$ est de classe $\mathcal{C}^1$ sur $A$ et vérifie : 
  
$$
\forall x\in A,\quad g'(x) = \int_I \frac{\partial f}{\partial x}(x,t) \,\mathrm{d}\!t.
$$

</td><td>
La démonstration n'est pas exigible.
<br>
En pratique, on vérifie l'hypothèse de domination sur tout segment
de $A$, ou sur d'autres intervalles adaptés à la situation.</td></tr><tr><td>

Extension à la classe $\mathcal{C}^k$ d'une intégrale à paramètre, sous hypothèse
de domination de $t\mapsto\dfrac{\partial^kf}{\partial x^k}(x,t)$ et
d'intégrabilité des $t\mapsto\dfrac{\partial^jf}{\partial x^j}(x,t)$
pour $0\le j\textless k$. 

</td><td>Exemples d'études de fonctions définies comme intégrales
  à paramètre : régularité, étude asymptotique, exploitation
  d'une équation différentielle élémentaire.</td></tr>
</table>


# Algèbre Linéaire

*Dans toute cette partie, $\K$ désigne $\:\R$ ou $\:\C$.*

## A - Compléments sur les espaces vectoriels, les endomorphismes et les matrices 

Le programme est organisé autour de trois objectifs :

-  consolider les acquis de la classe de première année ;
  
-  introduire de nouveaux concepts préliminaires à la réduction des endomorphismes : somme deplusieurs sous-espaces vectoriels, somme directe,sous-espaces stables, matrices par blocs,trace, polynômes d'endomorphismeset de matrices carrées, polynômes interpolateurs deLagrange ; 
  
- passer du point de vue vectoriel au  point de vue matriciel et inversement.  

Le programme valorise les interprétations géométriques et préconise
l'illustration des notions et résultats par de nombreuses figures.

<table>
<tr><td>a) Produit d'espaces vectoriels, somme de sous-espaces vectoriels</td><td></td></tr>

<tr><td>Produit d'un nombre fini d'espaces vectoriels ; dimension dans le cas
où ces espaces sont de dimension finie.
</td><td>
</td></tr><tr><td>

Somme, somme directe d'une famille finie de sous-espaces vectoriels.</td><td></td></tr><tr><td>

En dimension finie, base adaptée à un sous-espace vectoriel,
à une décomposition $E=\bigoplus E_i$.
</td><td>
Décomposition en somme directe obtenue par partition d'une base.</td></tr><tr><td>

Si $F_1, \ldots, F_p$ sont des sous-espaces de dimension finie,

$$\dim \biggl(\sum_{i=1}^p F_i\biggr) \leqslant \sum_{i=1}^p \dim(F_i)$$

avec égalité si et seulement si la somme est directe.
</td><td></td></tr><tr><td>


b) Matrices par blocs et sous-espaces stables</td><td></td></tr><tr><td>

Matrices définies par blocs, opérations par blocs de tailles
  compatibles (combinaison linéaire, produit, transposition).</td><td></td></tr><tr><td> 

Déterminant d'une matrice triangulaire par blocs.</td><td>
</td></tr><tr><td>

Sous-espace vectoriel stable par un endomorphisme, endomorphisme
induit. 

Si $u$ et $v$ commutent alors le noyau de $u$
est stable par $v$.
</td><td> Traduction matricielle de la stabilité 
d'un sous-espace vectoriel par un endomorphisme et interprétation en
termes d'endomorphismes d'une matrice triangulaire ou diagonale par
blocs. 
</td></tr><tr><td>


c) Trace</td><td></td></tr><tr><td>
Trace d'une matrice carrée.

Linéarité, trace d'une transposée.

Relation $\tr(AB)=\tr(BA)$. 

Invariance de la trace par similitude. Trace d'un endomorphisme d'un
espace de dimension finie. 
</td><td>Notation $\tr(A)$. </td></tr><tr><td>
d) Polynômes d'endomorphismes et de matrices carrées</td><td></td></tr><tr><td>

Polynôme d'un endomorphisme, d'une matrice carrée.
</td><td> Relation $(PQ)(u)=P(u)\circ Q(u)$.</td></tr><tr><td>

Polynôme annulateur.
</td><td>Application au calcul de l'inverse et des puissances.
</td></tr><tr><td>

Deux polynômes de l'endomorphisme $u$ commutent.
</td><td>Le noyau de $P(u)$ est stable par $u$.
</td></tr><tr><td>


Adaptation de ces résultats aux matrices carrées.</td><td>
</td></tr><tr><td>
e) Interpolation de Lagrange</td><td></td></tr><tr><td>
Base de $\K_n[X]$ constituée des polynômes interpolateurs
de Lagrange en $n+1$ points distincts de $\K$.</td><td>
Expression d'un polynôme $P\in\K_n[X]$ dans cette base.

La somme des polynômes interpolateurs de Lagrange en $n+1$
  points est le polynôme constant égal à 1.
</td></tr><tr><td>

Déterminant de Vandermonde.</td><td>Lien avec le problème
  d'interpolation de Lagrange. </td></tr>
</table>



## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.apps.education.fr/cours-psi/Banque%20dexercices)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 7, les exercices de la banque au programme sont les exercices 1 à 40 puis 43, 44, 47, 48, 50, 53.

## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 