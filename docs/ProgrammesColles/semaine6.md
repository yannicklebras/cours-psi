# Intégration sur un intervalle quelconque
<i>Cette section vise les objectifs suivants :
-  étendre la notion d'intégrale étudiée en première année à des fonctions continues par morceaux sur un intervalle quelconque par lebiais des intégrales généralisées ;

-  définir, dans le cadre des fonctions continues par morceaux, la notion de fonction intégrable ;

- compléter la section dédiée aux suites et aux séries de fonctions par les théorèmes de convergence dominée et d'intégration terme à terme ;

- étudier les fonctions définies par des intégrales dépendant d'un paramètre.

On évite tout excès de rigueur dans la rédaction. Ainsi, dans
  les calculs concrets mettant en jeu l'intégration par parties ou le 
changement de variable, on n'impose pas de rappeler les hypothèses de régularité des
résultats utilisés. De même, dans l'application des 
théorèmes de passage à la limite sous l'intégrale ou de régularité des
intégrales à paramètre, on se limite à la vérification 
des hypothèses cruciales, sans insister sur la continuité par morceaux
en la variable d'intégration.

Les fonctions considérées sont définies sur un intervalle de $\:\R$ et
à valeurs dans $\:\K$, ensemble des nombres réels ou des nombres complexes.
</i>


<table>
<tr><td>a) Fonctions continues par morceaux</td></tr>

<tr><td>Fonctions continues par morceaux sur un segment, sur un intervalle de $\R$.</td><td></td></tr>
<tr><td>
Intégrale sur un segment d'une fonction continue par morceaux.</td><td>
Brève extension des propriétés de l'intégrale d'une fonction continue sur un segment étudiées en première année.
Aucune construction n'est exigible.</td></tr>
<tr><td>
b) Intégrales généralisées sur un intervalle de la forme $[a,+\infty\mathclose[$</td></tr>
<tr><td>
Pour $f$ continue par morceaux sur $[a, +\infty\mathclose[$, l'intégrale 
$\ds\int_a^{+\infty} f(t) \,\mathrm{d}\!t$ est dite convergente si 
$\ds\int_a^x f(t) \,\mathrm{d}\!t$ a une limite finie
lorsque $x$ tend vers $+\infty$.</td><td>
Notations $\ds \int_a^{+\infty} f$, $\ds\int_a^{+\infty}f(t) \,\mathrm{d}\!t.$

Intégrale convergente (resp. divergente) en $+\infty$.</td></tr>
<tr><td>

Si $f$ est continue par morceaux sur $[a,+\infty\mathclose[$ et à
valeurs positives, alors  $\ds\int_a^{+\infty}f(t)\,\mathrm{d}\!t$
converge si et seulement si 
$\ds x \mapsto \int_a^xf(t)\,\mathrm{d}\!t$ est majorée.</td><td></td></tr>
<tr><td>

Si $f$ et $g$ sont deux fonctions continues par morceaux sur
$[a,+\infty[$ telles que $0\leqslant f\leqslant g$,  
la convergence de $\ds \int_a^{+\infty}g$ implique celle
de $\displaystyle \int_a^{+\infty}f$.</td><td></td></tr>
<tr><td> c) Intégrales généralisées sur un intervalle quelconque</td></tr>
<tr><td>
Adaptation du paragraphe précédent aux fonctions
continues par morceaux définies sur un intervalle semi-ouvert ou ouvert de $\R$.</td><td>
Notations $\ds \int_a^b f$, 
$\ds\int_a^bf(t) \,\mathrm{d}\!t$.

Intégrale convergente (resp. divergente) en $b$, en $a$.</td></tr>
<tr><td>
Propriétés des intégrales généralisées : 

linéarité, positivité, croissance, relation de Chasles.</td><td></td></tr>
<tr><td>
Intégration par parties sur un intervalle quelconque :


$$\ds\int_a^b f(t)g'(t)\,\mathrm{d}\!t=
\left[fg\right]_a^b- \ds\int_a^b f'(t)g(t)\,\mathrm{d}\!t.$$</td><td>


La démonstration n'est pas exigible.

L'existence des limites finies du produit $fg$ aux bornes de l'intervalle
assure que les intégrales de $fg'$ et $f'g$ sont de même
nature.

Pour les applications pratiques, on ne demande pas de
rappeler les hypothèses de régularité.</td></tr>
<tr><td>

Changement de variable :

si $\varphi : \mathopen]\alpha,\beta\mathclose[ \to
\mathopen]a,b\mathclose[$ est une bijection strictement croissante de
classe $\mathcal{C}^1$, 
et si $f$ est continue sur $\mathopen]a,b\mathclose[$,
alors $\displaystyle \int_{a}^b f(t) \,\mathrm{d}\!t$ et 
$\displaystyle \int_\alpha^{\beta} (f \circ \varphi)(u)\,\varphi'(u) \,\mathrm{d}\!u$ sont 
de même nature, et égales en cas de convergence.
</td><td>
La démonstration n'est pas exigible.

Adaptation au cas où $ \varphi$ est strictement décroissante.

On applique ce résultat sans justification dans
les cas de changements de variable usuels.</td></tr>
<tr><td>

d) Intégrales absolument convergentes et fonctions intégrables</td></tr>


<tr><td>Intégrale absolument convergente.</td><td></td></tr>
<tr><td>

La convergence absolue implique la convergence.

Inégalité triangulaire.</td><td>
L'étude des intégrales semi-convergentes n'est pas un objectif du programme.</td></tr>
<tr><td> 

Une fonction est dite intégrable sur un intervalle $I$ si elle est
continue par morceaux sur $I$ et son intégrale sur $I$ est absolument 
convergente.</td><td>
Notations $\ds\int_I f$, $\ds\int_I f(t) \,\mathrm{d}\!t$.
  
Pour $I=[a,b\mathclose[$, (respectivement $\mathopen]a,b]$),
fonction intégrable en $b$ (resp. en $a$).</td></tr>
<tr><td> 
  
Espace vectoriel $L^1(I,\K)$ des fonctions
intégrables sur $I$ à valeurs dans $\K$.</td><td></td></tr>
<tr><td>
  
Si $f$ est continue, intégrable et positive sur $I$, et si 
$\ds\int_I f(t) \,\mathrm{d}\!t=0$, alors $f$ est identiquement nulle.</td><td></td></tr>
<tr><td> 

Théorème de comparaison : 

pour $f$ et $g$ deux fonctions continues par morceaux
sur $[a,+\infty\mathclose[$ :
<ul>
<li> si $f(t)\underset{t \to +\infty}{=}O\left(g(t)\right)$, alors l'intégrabilité de $g$  en $+\infty$ implique celle de $f$. </li>
<li> si $f(t)\underset{t \to +\infty}{\sim} g(t)$, alors l'intégrabilité de $f$  en $+\infty$ est équivalente à celle de $g$. </li>
</ul>
</td><td>
 Adaptation au cas d'un intervalle quelconque.

Le résultat s'applique en particulier si
$f(t)\underset{t \to +\infty}{=}o(g(t))$.</td></tr>
<tr><td>
  
Fonctions de référence :

pour $\alpha\in\R$,

- intégrales de Riemann : étude de l'intégrabilité de $t \mapsto \dfrac{1}{t^\alpha}$ en $+\infty$, en $0^+$ ;

- étude de l'intégrabilité de $t \mapsto e^{-\alpha t}$ en $+\infty$.

</td><td>
 L'intégrabilité de $t\mapsto \ln t$ en $0$ peut être directement
 utilisée.
    
Les résultats relatifs à l'intégrabilité de $x\mapsto
\dfrac{1}{|x-a|^\alpha}$ en $a$ peuvent être directement utilisés.

Plus généralement, les étudiants doivent savoir que
la fonction $x\mapsto f(x)$ est intégrable en $a^+$
(resp. en $b^-$) si $t\mapsto f(a+t)$ (resp. $t\mapsto f(b-t)$) l'est en $0^+$.</td></tr>


<tr><td>
e) Suites et séries de fonctions intégrables 
</td><td></td></tr>
<tr><td>

Pour l'application pratique des énoncés de ce paragraphe,
on vérifie les hypothèses de convergence simple et de domination 
(resp. convergence de la série des intégrales), 
sans expliciter celles relatives à la continuité par morceaux.
<br>
Théorème de convergence dominée :
<br>
si une suite $(f_n)$ de fonctions continues par morceaux
sur $I$ 
converge simplement vers une fonction $f$ continue par
morceaux sur $I$ et s'il existe 
une fonction $\varphi$ intégrable sur $I$
vérifiant $\vert f_n \vert \leqslant \varphi$ pour tout $n$, alors
les fonctions $f_n$ et $f$ sont intégrables sur $I$ et : 

$$
\displaystyle
\int_I f_n(t) \,\mathrm{d}\!t \,\,\xrightarrow[n \to +\infty]{}\,\,
\int_I f(t) \,\mathrm{d}\!t. 
$$

</td><td>

La démonstration est hors programme.</td></tr><tr><td>
 
Théorème d'intégration terme à terme :
<br>
si une série $\sum f_n$ de fonctions intégrables sur $I$
converge simplement, si sa somme est continue 
par morceaux sur $I$, et si la série 
$\ds\sum \int_I \vert f_n(t) \vert \,\mathrm{d}\!t$
converge, alors
$\ds \sum_{n=0}^{+\infty} f_n$ est intégrable sur $I$ et : 


$$
\ds
\int_I \sum_{n=0}^{+\infty} f_n (t) \,\mathrm{d}\!t =
\sum_{n=0}^{+\infty} \int_I f_n(t) \,\mathrm{d}\!t. 
$$

</td><td>
La démonstration est hors programme.
<br>

On présente des exemples sur lesquels cet énoncé
ne s'applique pas, mais dans lesquels l'intégration terme à terme 
peut être justifiée par le théorème de
convergence dominée pour les sommes partielles.</td></tr><tr><td> 
 
f) Régularité d'une fonction définie par une intégrale à paramètre
<br>
Pour l'application pratique des énoncés de ce paragraphe,
on vérifie les hypothèses de régularité par rapport à $x$ et de
domination, sans expliciter celles relatives à la continuité par morceaux par rapport à $t$.
<br>
Théorème de continuité :
<br>
si $A$ et $I$ sont deux intervalles de $\R$ et $f$ une fonction
définie sur $A\times I$, telle que :

<ul>
<li>pour tout $t\in I$, $x\mapsto f(x,t)$ est continue sur $A$ ;
<li>pour tout $x\in A$, $t\mapsto f(x,t)$ est continue par morceaux sur $I$ ;
<li>il existe une fonction $\varphi$ intégrable sur $I$, telle que pour tout $(x,t)\in A \times I$, on ait $\vert f(x,t) \vert \leqslant  \varphi(t)$ ;
</ul>

alors la fonction $\displaystyle x \mapsto \int_I f(x,t) \,\mathrm{d}\!t$ est
définie et continue sur $A$. </td><td>

  
En pratique, on vérifie l'hypothèse de domination sur tout segment
de $A$, ou sur d'autres intervalles adaptés à la situation. </td></tr><tr><td>

Théorème de convergence dominée à paramètre continu :
<br>  
si $A$ et $I$ sont deux intervalles de $\R$, $a$ une borne
de $A$ et $f$ une fonction définie sur $A\times I$
telle que :

<ul>
<li>pour tout $t\in I$, $f(x,t)\xrightarrow[x\to a]{} \ell(t)$ ;    
<li>pour tout $x\in A$, $t\mapsto f(x,t)$ et $t\mapsto \ell(t)$ sont continues par morceaux sur $I$ ;    
<li>il existe une fonction $\varphi$ intégrable sur $I$, telle que pour tout $(x,t)\in A \times I$,  on ait $\vert f(x,t) \vert \leqslant  \varphi(t)$ ;
</ul>

alors $\ell$ est intégrable sur $I$ et : 

$$
\ds
\int_I f(x,t)\,\mathrm{d}\!t \xrightarrow[x\to a]{}
\int_I \ell(t)\,\mathrm{d}\!t.
$$

</td><td>
On remarque qu'il s'agit d'une simple extension du théorème
relatif aux suites de fonctions. 
</td></tr><tr><td>
  
Théorème de dérivation :
<br>
si $A$ et $I$ sont deux intervalles de $\R$ et $f$ une fonction
définie sur $A\times I$, 
telle que :
<ul>
<li>pour tout $t\in I$, $x\mapsto f(x,t)$ est de classe $\mathcal{C}^1$ sur $A$ ;
<li>pour tout $x\in A$, $t\mapsto f(x,t)$ est intégrable sur $I$ ;  
<li>pour tout $x\in A$, $\displaystyle t\mapsto \frac{\partial f}{\partial x}(x,t)$ est continue par morceaux sur $I$ ;
<li>il existe une fonction $\varphi$ intégrable sur $I$, telle que pour tout $(x,t)\in A \times I$, on ait $\displaystyle \left\vert \frac{\partial f}{\partial x}(x,t) \right\vert \leqslant \varphi(t)$ ;
</ul>
alors la fonction $\displaystyle g:x \mapsto \int_I f(x,t) \,\mathrm{d}\!t$ est de classe $\mathcal{C}^1$ sur $A$ et vérifie : 
  
$$
\forall x\in A,\quad g'(x) = \int_I \frac{\partial f}{\partial x}(x,t) \,\mathrm{d}\!t.
$$

</td><td>
La démonstration n'est pas exigible.
<br>
En pratique, on vérifie l'hypothèse de domination sur tout segment
de $A$, ou sur d'autres intervalles adaptés à la situation.</td></tr><tr><td>

Extension à la classe $\mathcal{C}^k$ d'une intégrale à paramètre, sous hypothèse
de domination de $t\mapsto\dfrac{\partial^kf}{\partial x^k}(x,t)$ et
d'intégrabilité des $t\mapsto\dfrac{\partial^jf}{\partial x^j}(x,t)$
pour $0\leqslant j<k$. </td><td></td></tr><tr><td>
  
</td><td>Exemples d'études de fonctions définies comme intégrales
  à paramètre : régularité, étude asymptotique, exploitation
  d'une équation différentielle élémentaire.</td></tr>
</table>



## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.aeif.fr/cours-psi/Banque%20dexercices/)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 5, les exercices de la banque au programme sont les exercices de 1 à 40 puis 43, 44, 47.

## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 