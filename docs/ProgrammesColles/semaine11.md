# Espaces préhilbertiens réels, espaces euclidiens

## B - Endomorphismes d'un espace euclidien



*Cette section vise les objectifs suivants :*

-  *étudier les isométries vectorielles et matrices orthogonales, et les décrire en dimension deux et trois en insistant sur les représentations géométriques ;*
-  *approfondir la thématique de réduction des endomorphismes dans le cadre euclidien en énonçant les formes géométrique et matricielle du théorème spectral ;*
-  *introduire la notion d'endomorphisme autoadjoint positif, qui trouvera notamment son application au calcul différentiel d'ordre 2.*
  
*La notion d'adjoint est hors programme.*


<table>
<tr><td>a) Isométries vectorielles d'un espace euclidien </td><td></td></tr>

<tr><td>Un endomorphisme d'un espace euclidien est une isométrie
vectorielle s'il conserve la norme. </td><td>

Exemple : symétries orthogonales, cas particulier des
réflexions.
 </td></tr><tr><td> 

Caractérisations par la conservation du produit scalaire, par l'image
d'une base orthonormée. </td></tr><tr><td> 
Groupe orthogonal. </td><td>
Notation $\mathrm{O}(E)$.

  On vérifie les propriétés lui conférant une structure de groupe,
  mais la définition axiomatique des groupes est hors programme.  </td></tr><tr><td> 

Stabilité de l'orthogonal d'un sous-espace stable.  </td><td>  </td></tr>

<tr><td> b) Matrices orthogonales</td><td></td></tr><tr><td>

Une matrice $A$ de $\mathcal{M}_n(\R)$ 
est orthogonale si 
$A^{\!\!\top}\!\! A = I_n$.
 </td><td>Interprétation en termes de colonnes et de lignes.

Caractérisation comme matrice de changement de
base orthonormée. </td></tr><tr><td> 

Caractérisation d'une isométrie vectorielle à l'aide de sa matrice
dans une base orthonormée. </td><td>
On mentionne la terminologie «
  automorphisme orthogonal », 
tout en lui préférant celle d'«isométrie vectorielle». 
 </td></tr><tr><td> 
Groupe orthogonal. </td><td>
Notations $\mathrm{O}_n(\R)$, $\mathrm{O}(n)$.
 </td></tr><tr><td>

Déterminant d'une matrice orthogonale.
Groupe spécial orthogonal. </td><td> Notations $\mathrm{SO}_n(\R)$, $\mathrm{SO}(n)$. </td></tr><tr><td>

Orientation. Bases orthonormées directes. </td><td> </td></tr><tr><td>



c) Espace euclidien orienté de dimension 2 ou 3 </td><td></td></tr><tr><td>

Déterminant d'une famille de vecteurs dans une base orthonormée
directe: produit mixte. 
 </td><td> Notations $[u,v]$, $[u, v, w]$.

Interprétation géométrique comme aire ou volume. </td></tr><tr><td>

Produit vectoriel. Calcul dans une base orthonormée directe. </td><td>
 </td></tr><tr><td>

Orientation d'un plan ou d'une droite dans un espace euclidien orienté
de dimension 3. 
 </td><td>


 </td></tr><tr><td>
d) Isométries vectorielles d'un plan euclidien </td><td></td></tr><tr><td>

Description des matrices
de $\mathrm{O}_2(\R)$, de $\mathrm{SO}_2(\R)$.
 </td><td>
Commutativité de $\mathrm{SO}_2(\R)$.
 </td></tr><tr><td>

Rotation vectorielle d'un plan euclidien orienté. </td><td> On introduit à
cette occasion, sans soulever de difficulté,
la notion de mesure d'un angle orienté de
vecteurs non nuls. </td></tr><tr><td> 

Classification des isométries vectorielles d'un plan euclidien.
 </td><td> </td></tr><tr><td>
e) Isométries d'un espace euclidien de dimension 3</td><td></td></tr><tr><td>

Description des matrices
de $\mathrm{SO}_3(\R)$.
 </td><td>
 </td></tr><tr><td>
Rotation vectorielle d'un espace euclidien orienté de dimension 3. </td><td>
Axe et mesure de
l'angle d'une rotation. 

 </td></tr><tr><td>
f) Réduction des endomorphismes autoadjoints et des matrices symétriques réelles</td><td></td></tr><tr><td>

Endomorphisme autoadjoint d'un espace euclidien.
 </td><td>Notation $\mathcal{S}(E)$.

Caractérisation des projecteurs orthogonaux.
 </td></tr><tr><td>

Caractérisation d'un endomorphisme autoadjoint à l'aide de sa
matrice dans une base orthonormée. 

 </td><td>On mentionne la terminologie «
  endomorphisme symétrique », 
tout en lui préférant celle d'«endomorphisme autoadjoint».
 </td></tr><tr><td>

Théorème spectral :

tout endomorphisme autoadjoint d'un espace
euclidien admet une base orthonormée de vecteurs
propres. </td><td>
La démonstration n'est pas exigible.

Forme matricielle du théorème spectral. </td></tr><tr><td>


Endomorphisme autoadjoint positif, défini positif.
 </td><td>
Caractérisation spectrale. Notations $\mathcal{S}^+(E)$,
$\mathcal{S}^{++}(E)$. </td></tr><tr><td>
Matrice symétrique positive, définie positive.
 </td><td>
Caractérisation spectrale. Notations
  $\mathcal{S}_n^+(\R)$, 
$\mathcal{S}_n^{++}(\R)$.
 </td></tr><tr><td>

</table>


# Espaces vectoriels normés

*Cette section vise les objectifs suivants :*

- *généraliser au cas des espaces vectoriels sur $\:\K=\R$ ou $\:\C$ certaines notions (convergence de suites, limite et continuité de fonctions) étudiées en première année dans le cadre de l'analyse réelle, indispensables pour aborder l'étude des suites de matrices, des fonctions à valeurs vectorielles et du calcul différentiel* ;

-  *fournir un cadre topologique à la convergence des suites et séries de fonctions.*

*Les notions seront illustrées par des exemples concrets et variés.*

*Il convient de souligner l'aspect géométrique des concepts topologiques à l'aide de nombreuses figures.*


<table>
<tr><td>a) Normes</td><td></td></tr>

<tr><td>Norme sur un espace vectoriel réel ou complexe.</td><td> Normes usuelles $\|\;\|_1$, $\|\;\|_2$ et $\|\;\|_\infty$ sur
$\K^n$.
</td></tr>
<tr><td>

Espace vectoriel normé.

Norme associée à un produit scalaire sur un espace préhilbertien réel. </td><td>

Norme $\|\;\|_\infty$ sur un espace de fonctions
  bornées à valeurs dans $\K$.

L'égalité $\sup (kA) = k\sup(A)$
  pour $A$ partie non vide de $\R$ et $k\in\R^+$ peut être directement utilisée. 
</td></tr>
<tr><td>
Distance associée à une norme.
</td><td></td></tr>
<tr><td>

Boule ouverte, boule fermée, sphère.
</td><td></td></tr>
<tr><td>

Partie convexe.</td><td>
Convexité des boules.
</td></tr>
<tr><td>

Partie bornée, suite bornée, fonction bornée.
</td><td></td></tr>
<tr><td>

<tr><td>b) Suites d'éléments d'un espace vectoriel normé</td><td></td></tr>
<tr><td>
Convergence et divergence d'une suite. </td><td>
Exemples dans des espaces de matrices, dans des espaces de fonctions.</td></tr>
<tr><td>

Unicité de la limite. Opérations sur les limites.
</td><td></td></tr>
<tr><td>

Une suite convergente est bornée.
</td><td></td></tr>
<tr><td>

Toute suite extraite d'une suite convergente est convergente.
</td><td></td></tr>
<tr><td>

<tr><td>c) Comparaison des normes</td><td></td></tr>
<tr><td>
Normes équivalentes.</td><td>
Invariance du caractère borné, de la convergence d'une suite. 

Utilisation de suites pour montrer que deux normes ne sont pas équivalentes.

La comparaison effective de deux normes n'est pas un objectif du programme. On se limite en pratique à des exemples élémentaires.
</td></tr>
<tr><td>

<tr><td>d) Topologie d'un espace vectoriel normé</td><td></td></tr>
<tr><td>
Point intérieur à une partie.</td><td>
</td></tr>
<tr><td>

Ouvert d'un espace normé. </td><td> Une boule ouverte est un ouvert.
</td></tr>
<tr><td>

Stabilité par réunion quelconque, par intersection finie.</td><td>
</td></tr>
<tr><td>

Fermé d'un espace normé. </td><td> Caractérisation séquentielle. 
</td></tr>
<tr><td>

</td><td> Une boule fermée, une sphère, sont des fermés.
</td></tr>
<tr><td>

Stabilité par réunion finie, par intersection quelconque.</td><td>
</td></tr>
<tr><td>

Point adhérent à une partie, adhérence. </td><td>
L'adhérence est l'ensemble des points adhérents.

Caractérisation séquentielle. Toute autre propriété de l'adhérence est hors programme.
</td></tr>
<tr><td>

Partie dense.
</td><td>
</td></tr>
<tr><td>

Invariance des notions topologiques par passage à une norme équivalente.</td><td>
</td></tr>
<tr><td>

<tr><td>e) Limite et continuité en un point</td><td></td></tr>
<tr><td>
Limite d'une fonction en un point adhérent à son domaine de
  définition.</td><td> Caractérisation séquentielle.
</td></tr>
<tr><td>

Opérations algébriques sur les limites, composition.</td><td></td></tr>
<tr><td>

Continuité en un point.</td><td>
Caractérisation séquentielle.
</td></tr>
<tr><td>



<tr><td>f) Continuité sur une partie </td><td></td></tr>
<tr><td>
Opérations algébriques, composition.</td><td>
</td></tr>
<tr><td>

Image réciproque d'un ouvert, d'un fermé par une application continue.</td><td>
Si $f$ est une application
continue de $E$ dans $\R$ alors l'ensemble défini 
par $f(x)>0$ est un ouvert et les ensembles définis par $f(x)=0$ ou
$f(x) \geqslant 0$ 
sont des fermés.</td></tr>
<tr><td>

Fonction lipschitzienne.
Toute fonction lipschitzienne est continue.</td><td></td></tr>
<tr><td>

<tr><td>g) Espaces vectoriels normés de dimension finie}
<tr><td>
Équivalence des normes en dimension finie.</td><td>
La démonstration est hors programme.

La convergence d'une suite (ou l'existence de la limite
d'une fonction) à valeurs dans un espace vectoriel normé
de dimension finie équivaut à celle de chacune de ses
coordonnées dans une base.
</td></tr>
<tr><td>

Théorème des bornes atteintes :

toute fonction réelle
continue sur une partie non vide fermée bornée d'un espace vectoriel
normé de dimension finie est bornée et atteint ses bornes.
</td><td>
La démonstration est hors programme.
</td></tr>
<tr><td> 

Continuité des applications linéaires, multilinéaires et polynomiales.
</td><td> 
La notion de norme subordonnée est hors programme.

Exemples du déterminant, du produit matriciel.</td></tr>
</table>


## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.apps.education.fr/cours-psi/Banque%20dexercices)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 11, les exercices de la banque au programme sont les exercices de 1 à 40 puis 43, 44, 47, 48, 50, 53 plus les exercices 54, 56 et 57, 59 ,61, 62,63, 64,65,67.

## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 