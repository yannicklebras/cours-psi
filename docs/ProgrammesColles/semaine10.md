# Espaces préhilbertiens réels, espaces euclidiens

## A -  Espaces préhilbertiens réels

L'objectif majeur est le théorème de projection orthogonale et
l'existence de la meilleure approximation quadratique. On s'appuie sur
des exemples de géométrie du plan et de l'espace pour illustrer les
différentes notions. 


<table> 
<tr><td>a) Produit scalaire et norme associée </td><td></td></tr>

<tr><td>Produit scalaire.

Espace préhilbertien réel, espace euclidien.
</td><td>Notations $\langle x,y\rangle$, $(x|y)$,  $x\cdot y$.

</td></tr><tr><td>

Exemples de référence : 

produit scalaire euclidien canonique sur
$\R^n$, produit scalaire canonique sur $\mathcal{M}_n(\R)$, 
produit scalaire défini par une intégrale sur
$\mathcal{C}^0([a,b],\R)$. 
</td><td>

Expression $X^\top Y$.

Expression $tr(A^\top B)$.
</td></tr><tr><td>

Inégalité de Cauchy-Schwarz, cas d'égalité.
</td><td></td></tr><tr><td>

Norme associée au produit scalaire.</td><td>Cas d'égalité dans l'inégalité
triangulaire.

Les étudiants doivent savoir manipuler les identités remarquables sur les normes
(développement de $\lVert u\pm v \rVert^2$, identité de polarisation).
</td></tr><tr><td>

b) Orthogonalité </td><td></td></tr><tr><td>

Vecteurs orthogonaux, sous-espaces orthogonaux, orthogonal d'un
sous-espace vectoriel $F$, d'une partie $X$.
</td><td> Notation $F^\bot$.

L'orthogonal d'une partie est un sous-espace vectoriel.</td></tr><tr><td>

Famille orthogonale, orthonormée (ou orthonormale).

Toute famille orthogonale de vecteurs non nuls est libre.

Théorème de Pythagore.
</td><td></td></tr><tr><td>

Algorithme d'orthonormalisation de Gram-Schmidt.
</td><td>

</td></tr><tr><td>
c) Bases orthonormées d'un espace euclidien</td><td></td></tr><tr><td>

Existence de bases orthonormées dans un espace euclidien. Théorème de
la base orthonormée incomplète.
</td><td></td></tr><tr><td>

Expression des coordonnées, du produit scalaire et de la norme dans
une base orthonormée. 
</td><td>

</td></tr><tr><td>
d) Projection orthogonale sur un sous-espace de dimension finie</td><td></td></tr><tr><td>

Supplémentaire orthogonal d'un sous-espace de dimension finie.
</td><td></td></tr><tr><td>
Dimension de $F^{\perp}$ en dimension finie.</td></tr><tr><td>


Projection orthogonale $p_F$ sur un sous-espace vectoriel $F$ de
dimension finie.
</td><td>Les étudiants doivent savoir déterminer $p_F(x)$ en calculant
son expression dans une base orthonormée de $F$
ou en résolvant un système linéaire traduisant l'orthogonalité de
$x-p_F(x)$ aux vecteurs d'une famille génératrice de $F$. 
</td></tr><tr><td>

Distance d'un vecteur à un sous-espace. Le projeté orthogonal de $x$
sur $F$ est l'unique élément de $F$ qui réalise la distance de $x$ à $F$.
</td><td>
Notation $d(x, F)$. </td></tr><tr><td>
Projeté orthogonal d'un vecteur sur l'hyperplan $\mathrm{Vect}(u)^\bot$ ;
distance entre $x$ et $\mathrm{Vect}(u)^\bot$.
</td><td>
Application géométrique à des calculs de distances.

</td></tr><tr><td>
 e) Formes linéaires sur un espace euclidien</td><td></td></tr><tr><td>

Représentation d'une forme linéaire à l'aide d'un produit scalaire.
</td><td>
Vecteur normal à un hyperplan.
</td></tr></table>




## B - Endomorphismes d'un espace euclidien



*Cette section vise les objectifs suivants :*

-  *étudier les isométries vectorielles et matrices orthogonales, et les décrire en dimension deux et trois en insistant sur les représentations géométriques ;*
-  *approfondir la thématique de réduction des endomorphismes dans le cadre euclidien en énonçant les formes géométrique et matricielle du théorème spectral ;*
-  *introduire la notion d'endomorphisme autoadjoint positif, qui trouvera notamment son application au calcul différentiel d'ordre 2.*
  
*La notion d'adjoint est hors programme.*


<table>
<tr><td>a) Isométries vectorielles d'un espace euclidien </td><td></td></tr>

<tr><td>Un endomorphisme d'un espace euclidien est une isométrie
vectorielle s'il conserve la norme. </td><td>

Exemple : symétries orthogonales, cas particulier des
réflexions.
 </td></tr><tr><td> 

Caractérisations par la conservation du produit scalaire, par l'image
d'une base orthonormée. </td></tr><tr><td> 
Groupe orthogonal. </td><td>
Notation $\mathrm{O}(E)$.

  On vérifie les propriétés lui conférant une structure de groupe,
  mais la définition axiomatique des groupes est hors programme.  </td></tr><tr><td> 

Stabilité de l'orthogonal d'un sous-espace stable.  </td><td>  </td></tr>

<tr><td> b) Matrices orthogonales</td><td></td></tr><tr><td>

Une matrice $A$ de $\mathcal{M}_n(\R)$ 
est orthogonale si 
$A^{\!\!\top}\!\! A = I_n$.
 </td><td>Interprétation en termes de colonnes et de lignes.

Caractérisation comme matrice de changement de
base orthonormée. </td></tr><tr><td> 

Caractérisation d'une isométrie vectorielle à l'aide de sa matrice
dans une base orthonormée. </td><td>
On mentionne la terminologie \og
  automorphisme orthogonal \fg, 
tout en lui préférant celle d'\og isométrie vectorielle\fg. 
 </td></tr><tr><td> 
Groupe orthogonal. </td><td>
Notations $\mathrm{O}_n(\R)$, $\mathrm{O}(n)$.
 </td></tr><tr><td>

Déterminant d'une matrice orthogonale.
Groupe spécial orthogonal. </td><td> Notations $\mathrm{SO}_n(\R)$, $\mathrm{SO}(n)$. </td></tr><tr><td>

Orientation. Bases orthonormées directes. </td><td> </td></tr><tr><td>



c) Espace euclidien orienté de dimension 2 ou 3 </td><td></td></tr><tr><td>

Déterminant d'une famille de vecteurs dans une base orthonormée
directe: produit mixte. 
 </td><td> Notations $[u,v]$, $[u, v, w]$.

Interprétation géométrique comme aire ou volume. </td></tr><tr><td>

Produit vectoriel. Calcul dans une base orthonormée directe. </td><td>
 </td></tr><tr><td>

Orientation d'un plan ou d'une droite dans un espace euclidien orienté
de dimension 3. 
 </td><td>


 </td></tr><tr><td>
d) Isométries vectorielles d'un plan euclidien </td><td></td></tr><tr><td>

Description des matrices
de $\mathrm{O}_2(\R)$, de $\mathrm{SO}_2(\R)$.
 </td><td>
Commutativité de $\mathrm{SO}_2(\R)$.
 </td></tr><tr><td>

Rotation vectorielle d'un plan euclidien orienté. </td><td> On introduit à
cette occasion, sans soulever de difficulté,
la notion de mesure d'un angle orienté de
vecteurs non nuls. </td></tr><tr><td> 

Classification des isométries vectorielles d'un plan euclidien.
 </td><td> </td></tr><tr><td>
e) Isométries d'un espace euclidien de dimension 3</td><td></td></tr><tr><td>

Description des matrices
de $\mathrm{SO}_3(\R)$.
 </td><td>
 </td></tr><tr><td>
Rotation vectorielle d'un espace euclidien orienté de dimension~3. </td><td>
Axe et mesure de
l'angle d'une rotation. 

 </td></tr><tr><td>
f) Réduction des endomorphismes autoadjoints et des matrices symétriques réelles</td><td></td></tr><tr><td>

Endomorphisme autoadjoint d'un espace euclidien.
 </td><td>Notation $\mathcal{S}(E)$.

Caractérisation des projecteurs orthogonaux.
 </td></tr><tr><td>

Caractérisation d'un endomorphisme autoadjoint à l'aide de sa
matrice dans une base orthonormée. 

 </td><td>On mentionne la terminologie \og
  endomorphisme symétrique \fg{}, 
tout en lui préférant celle d'\og endomorphisme autoadjoint\fg.
 </td></tr><tr><td>

Théorème spectral :

tout endomorphisme autoadjoint d'un espace
euclidien admet une base orthonormée de vecteurs
propres. </td><td>
La démonstration n'est pas exigible.

Forme matricielle du théorème spectral. </td></tr><tr><td>


Endomorphisme autoadjoint positif, défini positif.
 </td><td>
Caractérisation spectrale. Notations $\mathcal{S}^+(E)$,
$\mathcal{S}^{++}(E)$. </td></tr><tr><td>
Matrice symétrique positive, définie positive.
 </td><td>
Caractérisation spectrale. Notations
  $\mathcal{S}_n^+(\R)$, 
$\mathcal{S}_n^{++}(\R)$.
 </td></tr><tr><td>

</table>


## Exercice de la banque

[Lien vers la banque d'exercices](https://yannicklebras.forge.apps.education.fr/cours-psi/Banque%20dexercices)

Les exercices de la banque sont sur le lien ci-dessus. La banque peut évoluer un peu au cours de l'année. Merci de me signaler d'éventuelles erreurs, notamment dans les corrigés.


Pour cette semaine 10, les exercices de la banque au programme sont les exercices de 1 à 40 puis 43, 44, 47, 48, 50, 53 plus les exercices 54, 56 et 57, 59 ,61, 62,63.

## Déroulement d'une colle 

Pendant les 55 minutes de la colle, les élèves se voient proposer un exercice de la banque (à traiter en premier) et un exercice libre. L'exercice de la banque ne porte pas obligatoirement sur le programme de la semaine. L'exercice libre doit porter principalement sur le programme de la semaine. Chaque exercice est noté sur 10 points et doit être traité en 20/25 minutes. Il n'y a pas de question de cours : la connaissance du cours est évaluée dans les exercices et les exercices peuvent faire apparaître des démonstrations de cours. 