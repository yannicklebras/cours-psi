


## Nombres complexes


!!! Exercice "Exercice 1 : RMS24-416"  
	Calculer $\sum_{z \in \mathbb{U}_{n}} \frac{1}{2-z}$.


!!! Exercice "Exercice 2 : RMS24-417"  
	=== "Énoncé"  
		Soit $n \in \mathbb{N}$ avec $n \geqslant 2$. Soient $u_{1}, \ldots, u_{n}$ des nombres complexes de module 1 . Montrer que $\prod_{i \neq j}\left|u_{i}-u_{j}\right|^{\frac{1}{n(n-1)}} \leqslant n^{\frac{1}{n-1}}$.

	=== "Corrigé"  
		Soit $n \in \mathbb{N}$ avec $n \geqslant 2$. Soient $u_1, \ldots, u_n$ des nombres complexes de module 1 . Montrer que $\prod_{i \neq j}\left|u_i-u_j\right|^{\frac{1}{n(n-1)}} \leqslant n^{\frac{1}{n-1}}$.


		Tout d'abord, on reconnaît un truc qui ressemble à du Vandermonde au carré et donc si deux $u_i$ sont égaux, le déterminant est nul et l'inégalité bien respectée.

		Sinon, on sait que ce déterminant est non nul. Concentrons-nous sur cette idée. Nous allons prendre quelques raccourcis en admettant que l'on puisse faire une orthonormalisation de Schmidt sur des vecteurs à coefficients complexes : la démonstration est identique au cas réel. Donc comme ce  n'est pas trop au programme on va faire comme si de rien n'était.

		On se place donc dans le cas où les $u_i$ sont deux à deux distincts.

		Soit $M$ la matrice $\begin{pmatrix}1 & \cdots & \cdots & 1\\ u_1 & u_2 & \cdots & u_n \\ \vdots & & & \vdots \\ u_1^{n-1} & \cdots & \cdots & u_n^{n-1}\end{pmatrix}$. On note $C_1,\dots,C_n$ ses colonnes qui forment une base. Le procédé d'orthonormalisation de Schmidt sur les colonnes permet d'obtenir une matrice «orthogonale» au sens complexe $M'$ dont on note les colonnes $E_1,\dots,E_n$. 

		Notons $P$ la matrice de passage de la base $(C_1,\dots,C_n)$ à la base $(E_1,\dots,E_n)$ : par construction algorithmique, la matrice $P$ est triangulaire supérieure et $P_{ii}=<C_i,E_i>$. On a alors $M=PM'$ et 

		$$
		|\det(M)|=|\det(P)|\times|\det(M')|=\prod_{i=1}^n<C_i|E_i>.
		$$

		Or 

		$$
		|<C_i|E_i>|\overset{C.S.}{\le}||C_i||||E_i||=||C_i||.
		$$

		On en déduit que $\det(M)\le \prod_{i=1}^n||C_i||$. Or ici 

		$$
		\prod_{i\ne j}|u_i-u_j|=|\det(M)|^2\le\prod_{i=1}^n||C_i||^2=n^n.
		$$ 

		On en déduit que $\prod_{i\ne j}|u_i-u_j|^{\frac 1{n(n-1)}}\le {(n^n)}^{\frac1{n(n-1)}}=n^{\frac1{n-1}}$. 


!!! Exercice "Exercice 3 : RMS24-418"  
	=== "Énoncé"  
		Pour $n \in \mathbb{N}^{*}$, calculer le module de $\sum_{k=0}^{n-1} \exp \left(2 i \pi \frac{k^{2}}{n}\right)$.
	=== "Corrigé" 
		Pour $n \in \mathbb{N}^*$, calculer le module de $\sum_{k=0}^{n-1} \exp \left(2 i \pi \frac{k^2}{n}\right)$.


		Notons $G= \sum_{k=0}^{n-1}\exp\left(\frac{2i\pi}{n}k^2\right)$. On a alors $|G|^2=G\bar G=\sum_{0\le k,l\le n-1}\exp\left(\frac{2i\pi}n(k^2-l^2)\right)$. Remarquons que le terme général est $n$-périodique par rapport à $k$. En effet : 

		$$
		\begin{aligned}
		\exp(\frac {2i\pi}n((k+n)^2-l^2))=\exp(\frac{2i\pi}{n}(k^2-l^2))
		\end{aligned}
		$$

		On a donc 

		$$
		\begin{aligned}
		\sum_{k=0}^{n-1}\exp\left(\frac{2i\pi}n(k^2-l^2)\right)&=\sum_{k'=-l}^{n-1-l}\exp\left(\frac{2i\pi}n((k'+l)^2-l^2)\right)\\
		&=\sum_{k'=-l}^{-1}\exp\left(\frac{2i\pi}n((k'+l)^2-l^2)\right)+\sum_{k'=0}^{n-1-l}\exp\left(\frac{2i\pi}n((k'+l)^2-l^2)\right)\\
		&=\sum_{k'=-l}^{-1}\exp\left(\frac{2i\pi}n((n+k'+l)^2-l^2)\right)+\sum_{k'=0}^{n-1-l}\exp\left(\frac{2i\pi}n((k'+l)^2-l^2)\right)\\
		&=\sum_{k'=n-l}^{n-1}\exp\left(\frac{2i\pi}n((k'+l)^2-l^2)\right)+\sum_{k'=0}^{n-1-l}\exp\left(\frac{2i\pi}n((k'+l)^2-l^2)\right)\\
		&=\sum_{k'=0}^{n-1}\exp\left(\frac{2i\pi}n((k'+l)^2-l^2)\right)\\
		&=\sum_{k'=0}^{n-1}\exp\left(\frac{2i\pi}n({k'}^2+2lk')\right)\\
		\end{aligned}
		$$

		Et on peut écrire ($1_{n pair}$ vaut 1 si $n$ est pair, $0$ sinon): 

		$$
		\begin{aligned}
		|G|^2&=\sum_{l=0}^{n-1}\sum_{k=0}^{n-1}\exp\left(\frac{2i\pi}n({k}^2+2lk)\right)\\
		&=\sum_{k=0}^{n-1}\exp\left(\frac{2i\pi}n{k}^2\right)\sum_{l=0}^{n-1}\exp\left(\frac{4i\pi}nkl\right)&\text{La 2è somme est une somme de racines de l'unité}\\
		&=\sum_{k=0}^{n-1}\exp\left(\frac{2i\pi}n{k}^2\right)\begin{cases}n&\text{si $2k\equiv 0[n]$}\\0 & \text{sinon}\end{cases}\\
		&=\sum_{k=0}^{n-1}\exp\left(\frac{2i\pi}n{k}^2\right)\begin{cases}n&\text{si $k=0$ ou $k=\frac n2$ }\\0 &\text{sinon}\end{cases}\\
		&=n + 1_{n\text{ pair}}n\times\exp\left(\frac{2i\pi}n{(\frac n2)}^2\right)\\
		&=n + 1_{n\text{ pair}}n\times\exp\left(\frac{i\pi n}2\right)\\
		&=n + 1_{n\text{ pair}}n\times(-1)^{n/2}\\
		&=\begin{cases}n & \text{si $n$ impair}\\ 2n & \text{si }n\equiv 0[4]\\ 0 & \text{si }n\equiv 2[4]\\\end{cases}
		\end{aligned}
		$$

		On en déduit : $|G| = \begin{cases}\sqrt n & \text{si }n\text{ impair}\\ \sqrt{2n} & \text{si }n\equiv 0[4]\\ 0 & \text{si }n\equiv 2[4]\\\end{cases}$. 

!!! Exercice "Exercice 4 : RMS24-484"  
	Soit $n \in \mathbb{N} \backslash\{0,1\}$. Calculer $S_{n}=\sum_{k=0}^{\left\lfloor\frac{n}{2}\right\rfloor}\binom{n}{2 k}(-3)^{k}$ et $T_{n}=\sum_{k=0}^{\left\lfloor\frac{n}{3}\right\rfloor}\binom{n}{3 k}$.


!!! Exercice "Exercice 5 : RMS24-1306"  
	Soit $z \in \mathbb{C}^{*}$ tel que $|z|<1$. Soient $I=[0,2 \pi]$ et $f: t \in I \mapsto \frac{1-|z|^{2}}{\left|z-e^{i t}\right|^{2}}$.  
	a) Montrer que $t \mapsto\left|z-e^{i t}\right|$ ne s'annule pas sur $I$ puis que $f$ est continue sur $I$.  
	b) Montrer que $u: t \mapsto 1, v: t \mapsto e^{i t}$ et $w: t \mapsto e^{-i t}$ sont $\mathbb{C}$-linéairement indépendantes.  
	c) Montrer qu'il existe un unique couple $(\alpha, \beta) \in \mathbb{C}^{2}$, que l'on déterminera, tel que  
	$\forall t \in I, f(t)=-1+\frac{\alpha}{1-z e^{i t}}+\frac{\beta}{1-\bar{z} e^{-i t}}$.  
	d) Montrer que $\frac{1}{2 \pi} \int_{0}^{2 \pi} f(t) \mathrm{d} t=1$.


!!! Exercice "Exercice 6 : RMS24-1344"  
	Soient $n \geqslant 2, \omega=e^{\frac{2 i \pi}{n}}$ et $\Omega_{n}=\left(\omega^{(\ell-1)(m-1)}\right)_{1 \leqslant \ell, m \leqslant n}$.  
	a) Exprimer $\operatorname{det}\left(\Omega_{n}\right)$ à l'aide des $\omega^{k}$ sous forme de produit.  
	b) Calculer $\Omega_{n} \overline{\Omega_{n}}$. En déduire $\left|\operatorname{det}\left(\Omega_{n}\right)\right|$.  
	c) On rappelle que $\sum_{k=1}^{n} k^{2}=\frac{n(n+1)(2 n+1)}{6}$. Calculer $\sum_{1 \leqslant \ell<k \leqslant n}(k+\ell)$.  
	d) Calculer $\operatorname{det}\left(\Omega_{n}\right)$.



## Algèbre linéaire de sup


!!! Exercice "Exercice 7 : RMS24-854"  
	Soit $A=\frac{1}{5}\left(\begin{array}{ccc}7 & -4 & 0 \\ 6 & -7 & 0 \\ 0 & 0 & -5\end{array}\right)$.  
	a) Interpréter géométriquement $A$.  
	b) Donner l'image du plan $P$ d'équation $x-y-z=0$ par $A$.


!!! Exercice "Exercice 8 : RMS24-855"  
	Soit $P \in \mathcal{M}_{n}(\mathbb{R})$ représentant un projecteur $p$ de rang $r$ dans la base canonique de $\mathbb{R}^{n}$. Déterminer la trace de l'endomorphisme de $\mathcal{M}_{n}(\mathbb{R})$ défini par: $\Psi(X)=P X-X P$.


!!! Exercice "Exercice 9 : RMS24-860"  
	Soient $a_{1}, \ldots, a_{n}$ des nombres complexes distincts. Soit $A \in \mathcal{M}_{n}(\mathbb{C})$ la matrice de terme général $a_{i, j}=\left\{\begin{array}{c}0 \text { si } i=j \\ a_{j} \text { si } i \neq j\end{array}\right.$. Soit $P: x \mapsto \operatorname{det}\left(A+x I_{n}\right)$.  
	a) Montrer que $P$ est un polynôme unitaire de degré $n$.  
	b) Calculer $P\left(a_{i}\right)$.  
	c) Trouver l'expression de $P$.  
	d) Décomposer $\frac{P(X)}{\left(X-a_{1}\right) \cdots\left(X-a_{n}\right)}$ en éléments simples.  
	e) Calculer $\operatorname{det}\left(A+I_{n}\right)$.


!!! Exercice "Exercice 10 : RMS24-863"  
	Soient $A, B \in \mathcal{M}_{n}(\mathbb{C})$. On suppose qu'il existe des complexes deux à deux distincts $\lambda_{0}, \ldots, \lambda_{n}$ tels que $A+\lambda_{i} B$ est nilpotente pour tout $i$.  
	a) Montrer que l'indice de nilpotence d'une matrice nilpotente de taille $n$ est inférieur ou égal à $n$.  
	b) Montrer que : $\forall \lambda \in \mathbb{C},(A+\lambda B)^{n}=0$.  
	c) Montrer que $A$ et $B$ sont nilpotentes.


!!! Exercice "Exercice 11 : RMS24-864"  
	Soient $n \geqslant 2$ et $A \in \mathcal{M}_{n}(\mathbb{R})$ telle que $\forall M \in \mathcal{M}_{n}(\mathbb{R}), \operatorname{det}(A+M)=\operatorname{det}(A)+$ $\operatorname{det}(M)$.  
	a) Montrer que $A$ n'est pas inversible.  
	b) Montrer que $A=0$. Ind. Écrire $A=P J_{r} Q^{-1}$.


!!! Exercice "Exercice 12 : RMS24-1270"  
	Soient $A \in \mathcal{M}_{n}(\mathbb{R})$ et $\Phi_{A}: M \in \mathcal{M}_{n}(\mathbb{R}) \mapsto M A-A M$.  
	a) Montrer que $\Phi_{A}$ est un endomorphisme non injectif.  
	b) Trouver la dimension du noyau de $\Phi_{A}$ dans le cas où $A=\operatorname{Diag}(1,2, \ldots, n)$.  
	c) Soit $\Phi: A \in \mathcal{M}_{n}(\mathbb{R}) \mapsto \Phi_{A} \in \mathcal{L}\left(\mathcal{M}_{n}(\mathbb{R})\right)$. Montrer que $\Phi$ est linéaire et trouver son noyau.


!!! Exercice "Exercice 13 : RMS24-1272"  
	Soit $A_{1}, A_{2}, A_{3}, A_{4} \in \mathcal{M}_{n}(\mathbb{R})$.  
	  
	On pose $A=\left(\begin{array}{ll}A_{1} & A_{2} \\ A_{3} & A_{4}\end{array}\right), C_{1}=\binom{A_{1}}{A_{3}}, C_{2}=\binom{A_{2}}{A_{4}}$.  
	a) Montrer que $\operatorname{rg} A \leqslant \operatorname{rg} C_{1}+\operatorname{rg} C_{2}$.  
	b) Montrer que $\operatorname{rg} A \leqslant \sum_{k=1}^{4} \operatorname{rg} A_{k}$.  
	c) $\operatorname{Si} \operatorname{rg} A_{1}=\operatorname{rg} A_{4}=n$ et $A_{3}=0, A$ est-elle inversible? Si oui, donner $A^{-1}$.



## Polynômes


!!! Exercice "Exercice 14 : RMS24-876"  
	Soient $E=\mathbb{C}_{n}[X], \alpha \in \mathbb{C}$ et $f: P \in E \mapsto P-\alpha(X-\alpha) P^{\prime}$.  
	a) Montrer que $f \in \mathcal{L}(E)$ et donner sa matrice dans la base canonique.  
	b) i) Montrer que $f$ est diagonalisable.  
	ii) À quelle condition sur $\alpha$, l'endomorphisme $f$ est-il inversible?  
	c) Montrer, pour tout $k \in \mathbb{N}: E=\operatorname{Ker}\left(f^{k}\right) \oplus \operatorname{Im}\left(f^{k}\right)$.


!!! Exercice "Exercice 15 : RMS24-1269"  
	Soit $\left(P_{n}\right)$ la suite à valeurs dans $\mathbb{R}[X]$ définie par $P_{0}=2, P_{1}=X$ et, pour $n \geqslant 2$, $P_{n}=X P_{n-1}-P_{n-2}$.  
	a) Déterminer le degré de $P_{n}$. Étudier la parité de $P_{n}$.  
	b) Montrer que, pour tout $n \in \mathbb{N}, \forall z \in \mathbb{C}^{*}, P_{n}\left(z+\frac{1}{z}\right)=z^{n}+\frac{1}{z^{n}}$.  
	c) Montrer que $P_{n}$ est scindé sur $\mathbb{R}$ et à racines simples pour tout $n \in \mathbb{N}^{*}$.


!!! Exercice "Exercice 16 : RMS24-1342"  
	a) Trouver tous les $P \in \mathbb{R}[X]$ tels que $P\left(X^{2}\right)=\left(X^{3}+1\right) P(X)$.  
	b) Trouver tous les $P \in \mathbb{R}[X]$ tels que $P\left(X^{2}\right)=P(X) P(X-1)$.


!!! Exercice "Exercice 17 : RMS24-1500"  
	[CCINP] Soit $\Phi: P \in \mathbb{R}[X] \mapsto(X-b)\left(P^{\prime}+P^{\prime}(b)\right)-2(P-P(b))$ où $b \in \mathbb{R}$.  
	a) Pour $P \in \mathbb{R}[X]$, montrer que $b$ est racine de $\Phi(P)$. Déterminer sa multiplicité si $\Phi(P)$ est non nul.  
	b) Déterminer le noyau de $\Phi$.  
	c) Déterminer l'image de $\Phi$.



## Continuité/Dérivabilité


!!! Exercice "Exercice 18 : RMS24-919"  
	Soit $f: \mathbb{R}^{+} \rightarrow \mathbb{R}$ continue et surjective. Montrer que tout $y \in \mathbb{R}$ admet une infinité d'antécédents par $f$.


!!! Exercice "Exercice 19 : RMS24-920"  
	${ }^{\star}$ Soit $f$ une application continue de $\mathbb{R}$ dans $\mathbb{R}$ telle que $f \circ f=2 f-\mathrm{id}$.  
	a) Montrer que $f$ est une bijection strictement croissante de $\mathbb{R}$ dans $\mathbb{R}$.  
	b) On pose $f_{0}=f$ et, pour $n \in \mathbb{N}, f_{n+1}=f \circ f_{n}$. Montrer que $\left(\frac{1}{n} f_{n}\right)$ admet une limite, que l'on précisera.  
	c) Déterminer $f$.


!!! Exercice "Exercice 20 : RMS24-922"  
	Trouver les fonctions $f \in \mathcal{C}^{1}(\mathbb{R}, \mathbb{R})$ telles que $\forall x \in \mathbb{R}, f(x)+\int_{0}^{x}(x-t) f(t) \mathrm{d} t=1$.


!!! Exercice "Exercice 21 : RMS24-1086"  
	Soit $f \in \mathcal{C}^{0}(\mathbb{R}, \mathbb{R})$ telle que $(*): \forall(x, y) \in \mathbb{R}^{2}, f(x+y) f(x-y)=(f(x) f(y))^{2}$.  
	a) Donner toutes les valeurs que peut prendre $f(0)$.  
	b) Montrer que, pour tout $x_{0} \in \mathbb{R}$ tel que $f\left(x_{0}\right)=0$, on a $f\left(\frac{x_{0}}{2^{n}}\right)=0$. En déduire que si $f$ s'annule en un point, $f$ est identiquement nulle.  
	c) Trouver toutes les fonctions continues vérifiant (*).


!!! Exercice "Exercice 22 : RMS24-1089"  
	Montrer que $x \mapsto \cos (x)$ admet un unique point fixe. Montrer qu'il n'existe pas de fonction $f$ dérivable telle que $\cos =f \circ f$.


!!! Exercice "Exercice 23 : RMS24-1091"  
	Soit $f \in \mathcal{C}^{1}([a, b], \mathbb{R})$ telle que $f^{\prime}(a)=f^{\prime}(b)=0$. Montrer qu'il existe $\left.x \in\right] a, b[$ tel que $f^{\prime}(x)=\frac{f(x)-f(a)}{x-a}$.


!!! Exercice "Exercice 24 : RMS24-1618"  
	[IMT] Soit $f: x \mapsto x e^{x^{2}}$. Montrer que $f$ est une bijection de $\mathbb{R}$ sur $\mathbb{R}$ et déterminer un DL à l'ordre 5 de $f^{-1}$ en 0 .



## Suites


!!! Exercice "Exercice 25 : RMS24-913"  
	Soit $\left(x_{n}\right)_{n \in \mathbb{N}^{*}}$ une suite de réels positifs et, pour $n \geqslant 1, y_{n}=\sqrt{x_{1}+\sqrt{x_{2}+\cdots+\sqrt{x_{n}}}}$.  
	a) Étudier la convergence de la suite $\left(y_{n}\right)$ lorsque la suite $\left(x_{n}\right)$ est constante.  
	b) Étudier la convergence de la suite ( $y_{n}$ ) lorsque $x_{n}=a b^{2^{n}}$ avec $a>0$ et $b>0$.  
	c) Montrer que la suite $\left(y_{n}\right)$ converge si et seulement si la suite $\left(x_{n}^{1 / 2^{n}}\right)$ est bornée.


!!! Exercice "Exercice 26 : RMS24-914"  
	Pour $n \geqslant 2$, on s'intéresse à l'équation $e^{x}-x^{n}=0$.  
	a) Montrer que cette équation admet exactement deux solutions positives $u_{n}$ et $v_{n}$, avec $u_{n}<v_{n}$.  
	b) $i)$ Montrer que $\left(u_{n}\right)$ tend vers une limite $\ell$.  
	ii) Trouver un équivalent de $u_{n}-\ell$.  
	c) Montrer que la suite $\left(v_{n}\right)$ diverge.


!!! Exercice "Exercice 27 : RMS24-917"  
	Soit $\left(u_{n}\right)_{n \geqslant 1}$ une suite définie par $u_{1}>0$ et, pour tout $n \in \mathbb{N}^{*}, u_{n+1}=\frac{u_{n}}{n}+\frac{1}{n^{2}}$.  
	a) Étudier la convergence de la suite $\left(u_{n}\right)_{n \geqslant 1}$.  
	b) Étudier la convergence de la série $\sum u_{n}$.


!!! Exercice "Exercice 28 : RMS24-1073"  
	Soit $\left(x_{n}\right)_{n \geqslant 0}$ une suite réelle telle que $x_{0}>1$ et, pour tout $n \in \mathbb{N}, x_{n+1}=x_{n}+x_{n}^{-1}$. Montrer que $x_{n} \sim \sqrt{2 n}$.



## Séries numériques


!!! Exercice "Exercice 29 : RMS24-1075"  
	Soit $\alpha \in \mathbb{R}$. Nature de la série de terme général $u_{n}=n^{\alpha} \prod_{k=1}^{n}\left(1+\frac{(-1)^{k-1}}{\sqrt{k}}\right)$ ?


!!! Exercice "Exercice 30 : RMS24-1076"  
	Nature de la série de terme général $u_{n}=\frac{(-1)^{n}}{\sum_{k=1}^{n} \frac{1}{\sqrt{k}}+(-1)^{n}}$.


!!! Exercice "Exercice 31 : RMS24-1077"  
	Pour $n \in \mathbb{N}^{*}$, on pose $x_{n}=\sum_{k=1}^{n} \frac{(-1)^K}{k}, H_{n}=\sum_{k=1}^{n} \frac{1}{k}, u_{n}=\sum_{k=1}^{n} \frac{(-1)^{k} \ln k}{k}, v_{n}=\sum_{k=1}^{n} \frac{\ln k}{k}$ et $w_{n}=\sum_{k=1}^{n} \frac{\ln (2 k)}{k}$.  
	a) Montrer que $\left(x_{n}\right)$ converge vers un réel $\ell$ à déterminer. Montrer que $x_{n}=\ell+\mathcal{O}\left(\frac{1}{n}\right)$.  
	b) Exprimer $u_{2 n}$ en fonction de $v_{2 n}$ et $w_{n}$.  
	c) Montrer que $H_{n}=\ln (n)+\gamma+o(1)$.  
	d) Établir la convergence de $\left(u_{n}\right)$ et préciser sa limite.


!!! Exercice "Exercice 32 : RMS24-1078"  
	Soit $\left(a_{n}\right)_{n \in \mathbb{N}^{*}}$ la suite définie par $a_{1}=1$ et, pour tout $n \geqslant 2, a_{n}=2 a_{\lfloor n / 2\rfloor}$. Montrer que $\sum \frac{1}{a_{n}^{2}}$ converge.


!!! Exercice "Exercice 33 : RMS24-1300"  
	On étudie la série $\sum(-1)^{n} \ln \left(1+\frac{1}{n}\right)$.  
	a) Justifier la convergence de la série.  
	b) Exprimer $\sum_{n=1}^{2 N}(-1)^{n} \ln \left(1+\frac{1}{n}\right)$ à l'aide de factorielles.  
	c) En déduire la valeur de la somme $\sum_{n=1}^{+\infty}(-1)^{n} \ln \left(1+\frac{1}{n}\right)$.


!!! Exercice "Exercice 34 : RMS24-1301"  
	Soit, pour $n \in \mathbb{N}, u_{n}=\sum_{k=n+1}^{+\infty} \frac{(-1)^{k+1}}{\sqrt{k}}$.  
	a) Montrer que $\left(u_{n}\right)$ est définie et tend vers 0 .  
	b) Montrer que $\sum_{n=1}^{+\infty} \frac{(-1)^{n}}{n} u_{n}$ converge.


!!! Exercice "Exercice 35 : RMS24-1302"  
	Soit $\left(u_{n}\right)_{n \in \mathbb{N}}$ une suite réelle. On suppose que, pour tout $n \in \mathbb{N}, u_{n} \neq-1$.  
	  
	Pour $n \in \mathbb{N}$ on pose $v_{n}=\frac{u_{n}}{\left(1+u_{0}\right)\left(1+u_{1}\right) \cdots\left(1+u_{n}\right)}$.  
	a) Soit $n \in \mathbb{N}$. Montrer que $\sum_{k=0}^{n} v_{k}=1-\frac{1}{\left(1+u_{0}\right)\left(1+u_{1}\right) \cdots\left(1+u_{n}\right)}$.  
	b) Montrer que, si $\left(u_{n}\right)$ est à termes positifs, alors $\sum v_{n}$ converge.  
	c) Montrer que, si $\sum\left|u_{n}\right|$ converge, alors $\sum v_{n}$ converge.


!!! Exercice "Exercice 36 : RMS24-1540"  
	[CCINP] a) Citer le théorème spécial des séries alternées.  
	b) Montrer que la série $\sum \frac{(-1)^{n}}{n} \int_{n}^{+\infty} e^{-t^{2}} \mathrm{~d} t$ converge. La convergence est-elle absolue?  
	c) En déduire la nature de la série $\sum(-1)^{n} \int_{0}^{1} e^{-n^{2} t^{2}} \mathrm{~d} t$.



## Suites et séries de fonctions


!!! Exercice "Exercice 37 : RMS24-932"  
	Soit la suite de fonctions définies par $f_{n}: x \mapsto \frac{x^{n}}{n!} e^{-x}$.  
	a) Étudier la convergence simple de la suite $\left(f_{n}\right)$.  
	b) Étudier la convergence uniforme de la suite $\left(f_{n}\right)$.  
	c) Calculer $\int_{0}^{+\infty} f_{n}$ puis sa limite lorsque $n$ tend vers $+\infty$. Est-ce cohérent avec les théorèmes du cours?


!!! Exercice "Exercice 38 : RMS24-933"  
	Étudier la convergence simple et la convergence uniforme de la suite de fonctions $\left(f_{n}\right)_{n \geqslant 0}$ définie sur $\mathbb{R}^{+*}$ par $\forall x>0, f_{0}(x)=x$ et $\forall n \in \mathbb{N}, f_{n+1}(x)=\frac{1}{2}\left(f_{n}(x)+\frac{x}{f_{n}(x)}\right)$.


!!! Exercice "Exercice 39 : RMS24-934"  
	Soit $f: x \mapsto \sum_{n=2}^{+\infty} \frac{x e^{-n x}}{\ln (n)}$.  
	a) Trouver les domaines de définition/continuité/dérivabilité de $f$.  
	b) Trouver la limite de $f$ en $+\infty$ puis un équivalent.


!!! Exercice "Exercice 40 : RMS24-1309"  
	On pose, pour $n \in \mathbb{N}$ et $x \geqslant 0, f_{n}(x)=\frac{n x^{2}}{1+n x}$ et, pour $x<0, f_{n}(x)=\frac{n x^{3}}{1+n x^{2}}$.  
	a) Montrer que la suite $\left(f_{n}\right)$ converge simplement sur $\mathbb{R}$ vers une fonction $f$ à déterminer.  
	b) La convergence est elle uniforme sur $\mathbb{R}$ ?


!!! Exercice "Exercice 41 : RMS24-1310"  
	Pour $x>0$ et $n \in \mathbb{N}^{*}$, on note $f_{n}(x)=\frac{1+\sin (2 \pi n x)}{1+n^{2} x^{2}}$.  
	a) Montrer que la suite $\left(f_{n}\right)$ converge simplement sur $\mathbb{R}^{+*}$. La convergence est-elle uniforme $\operatorname{sur} \mathbb{R}^{+*}$ ?  
	Soient $\varepsilon>0$ fixé, $A_{n}=f_{n}^{-1}\left(\left[\varepsilon,+\infty[)\right.\right.$ et $u_{n}=\mathbf{1}_{A_{n}}$.  
	b) Tracer le graphe de $f_{5}$ et en déduire que $u_{n}$ est continue par morceaux.  
	c) Montrer que la suite $\left(u_{n}\right)$ converge simplement et donner sa limite.  
	d) Montrer que $\lim _{n \rightarrow+\infty} \int_{0}^{+\infty} u_{n}=0$.


!!! Exercice "Exercice 42 : RMS24-1311"  
	Pour $n \in \mathbb{N}^{*}$, soit $f_{n}: t \mapsto \frac{e^{-n t^{2}}}{n^{2}}$.  
	a) Prouver que $\sum f_{n}$ converge simplement sur $\mathbb{R}$.  
	b) Prouver que $f=\sum_{n=1}^{+\infty} f_{n}$ est de classe $\mathcal{C}^{1}$ sur $\mathbb{R}$.


!!! Exercice "Exercice 43 : RMS24-1312"  
	Pour $n \geqslant 1$, soit $f_{n}: x \mapsto x^{\ln (n)}$. Soit $f: x \mapsto \sum_{n=1}^{+\infty} f_{n}(x)$.  
	a) Déterminer le domaine de définition $\mathcal{D}_{f}$ de $f$. La série $\sum f_{n}$ converge-t-elle uniformément sur $\mathcal{D}_{f}$ ?  
	b) Montrer que $f$ est continue sur $\mathcal{D}_{f}$.  
	  
	Montrer que $f$ est de classe $\mathcal{C}^{\infty}$ sur $\mathcal{D}_{f}$.  
	c) Déterminer l'équation de la tangente à la courbe de $f_{n}$ au point d'abscisse $1 / e$.



## Séries entières


!!! Exercice "Exercice 44 : RMS24-936"  
	a) Déterminer le rayon de convergence $R$ de la série entière $\sum \frac{x^{n}}{n^{2}}$.  
	b) Montrer que pour tout $x \in\left[0, R\left[, \sum_{n \geqslant 1} \frac{x^{n}}{n^{2}}=x \int_{0}^{1} \frac{\ln (t)}{x t-1} \mathrm{~d} t\right.\right.$.  
	c) Que se passe-t-il pour $x=1$ ?


!!! Exercice "Exercice 45 : RMS24-937"  
	Soit $f: x \mapsto \sum_{n \geqslant 0}\binom{2 n}{n} x^{n}$.  
	a) Déterminer le rayon de convergence de $f$.  
	b) Quel est le domaine de définition de $f$ ? La fonction $f$ est-elle dérivable? Si oui, déterminer sa dérivée.  
	c) Déterminer une équation différentielle d'ordre 1 vérifiée par $f$.  
	d) Que vaut $\sum_{n \geqslant 0}\binom{2 n}{n} \frac{(-1)^{n}}{4^{n}}$ ?


!!! Exercice "Exercice 46 : RMS24-938"  
	Soient $f: x \mapsto \sum_{n=0}^{+\infty} \frac{x^{n}}{(n+1)!}$ et $F: x \mapsto \int_{0}^{x} e^{-t} f(t) \mathrm{d} t$.  
	a) Déterminer le rayon de convergence de $f$ et exprimer $f$ à l'aide de fonctions usuelles.  
	b) Montrer que $F$ est définie et dérivable sur $\mathbb{R}$. Que vaut $F^{\prime}$ ?  
	c) Montrer que $F$ est développable en série entière et déterminer ce développement.


!!! Exercice "Exercice 47 : RMS24-940"  
	Soit $g: x \mapsto \frac{1}{\cos x}$.  
	a) Montrer que $g$ est développable en série entière au voisinage de 0 .  
	b) Donner un encadrement du rayon de convergence.


!!! Exercice "Exercice 48 : RMS24-941"  
	Soit $f: x \mapsto \sum_{n=0}^{+\infty} \frac{(-1)^{n}}{\left(2^{n} n!\right)^{2}} x^{2 n}$.  
	a) Trouver l'ensemble de définition de $f$.  
	b) Trouver une équation différentielle vérifiée par $f$.  
	c) Calculer $\int_{0}^{+\infty} f(t) e^{-x t} \mathrm{~d} t$ pour $\left.x \in\right] 1,+\infty[$.


!!! Exercice "Exercice 49 : RMS24-961"  
	On s'intéresse aux solutions $f: x \mapsto \sum_{n \geqslant 0} a_{n} x^{n}$ de l'équation différentielle  
	(E) : $x^{2} y^{\prime \prime}+4 x y^{\prime}+\left(2-x^{2}\right) y=1$.  
	a) Montrer que $a_{0}=1 / 2, a_{1}=0$ et $\forall n \geqslant 2, a_{n}=\frac{a_{n-2}}{(n+1)(n+2)}$.  
	b) En déduire l'unicité de $f$.  
	c) Déterminer les $a_{n}$, le rayon de convergence de $f$ puis exprimer $f$ à l'aide de fonctions usuelles.


!!! Exercice "Exercice 50 : RMS24-1316"  
	$a$ ) Déterminer le rayon de convergence $R$ de $\sum \frac{z^{n}}{n^{2}}$.  
	b) Établir que, pour $x \in]-R, R\left[, \sum_{n=1}^{+\infty} \frac{x^{n}}{n^{2}}=\int_{1-x}^{1} \frac{\ln (t)}{t-1} \mathrm{~d} t\right.$.  
	c) Que se passe t-il en $x=1$ ?


!!! Exercice "Exercice 51 : RMS24-1317"  
	a) Pour $n \in \mathbb{N}$, on note $W_{n}=\int_{0}^{\pi / 2}(\sin x)^{n} \mathrm{~d} x$. Déterminer une relation entre $W_{2 n+2}$ et $W_{2 n}$. En déduire que $\forall n \in \mathbb{N}, W_{2 n+1}=\frac{2^{2 n}(n!)^{2}}{(2 n+1)!}$.  
	b) Montrer que $\forall x \in\left[0, \pi / 2\left[, x=\sum_{n=0}^{+\infty} \frac{(2 n)!}{2^{2 n}(n!)^{2}} \frac{(\sin x)^{2 n+1}}{(2 n+1)}\right.\right.$. On pourra commencer par déterminer le développement en série entière de arcsin.  
	c) En déduire la valeur de $\sum_{n=0}^{+\infty} \frac{1}{(2 n+1)^{2}}$.


!!! Exercice "Exercice 52 : RMS24-1321"  
	a) Soit $\alpha>0$. Étudier la convergence de $\sum_{n=2}^{+\infty} \frac{1}{n \ln (n)}$ puis celle de $\sum_{n \geqslant n_{0}} \frac{1}{n \ln (n)^{(1-\alpha)}}$.  
	  
	Soient $n_{0} \in \mathbb{N}, \Phi \in \mathcal{C}^{0}\left(\left[n_{0},+\infty[, \mathbb{R})\right.\right.$ décroissante telle que $\sum \Phi(n)$ diverge.  
	b) Déterminer le rayon de convergence de $\sum_{n=n_{0}}^{+\infty} \Phi(n) x^{n}$.  
	c) Montrer que $\sum_{n=n_{0}}^{+\infty} \Phi(n) x^{n} \underset{x \rightarrow 1^{-}}{\longrightarrow}+\infty$. En déduire $\sum_{n=n_{0}}^{+\infty} \Phi(n) x^{n} \underset{x \rightarrow 1^{-}}{\sim} \int_{n_{0}}^{+\infty} \Phi(t) x^{t} \mathrm{~d} t$.


!!! Exercice "Exercice 53 : RMS24-1323"  
	Soit $f:[0,1] \rightarrow \mathbb{R}$ une fonction continue. Pour $n \in \mathbb{N}$, on note $u_{n}=\int_{0}^{1} f(x) x^{n} \mathrm{~d} x$.  
	a) Montrer que si $f(1)>0$, alors il existe $c>0, a \in] 0,1[$ tels que $\forall t \in[a, 1], f(t) \geqslant c$.  
	b) On suppose que $\sum u_{n}$ est convergente. Montrer que $f(1)=0$.  
	c) On suppose que $f$ est de classe $\mathcal{C}^{1}$. Montrer que $\sum u_{n}$ converge $\Leftrightarrow f(1)=0$.


!!! Exercice "Exercice 54 : RMS24-1324"  
	Soit, pour $n \in \mathbb{N}, I_{n}=\int_{0}^{1} e^{-1 / t} t^{n} \mathrm{~d} t$.  
	a) Soit $n \in \mathbb{N}$. Montrer que $I_{n}$ est bien défini et donner son signe  
	a) Étudier les variations de $\left(I_{n}\right)$.  
	b) Montrer la convergence de la suite $\left(I_{n}\right)$ et déterminer sa limite.  
	c) Montrer que, pour $n \in \mathbb{N},(n+1) I_{n}+I_{n-1}=e^{-1}$.  
	d) Montrer que $I_{n} \sim \frac{1}{e n}$.  
	e) Déterminer la nature des séries de termes généraux $I_{n}$ et $(-1)^{n} I_{n}$.  
	$f$ ) Déterminer le rayon de convergence de la série $\sum I_{n} x^{n}$.



## Réduction


!!! Exercice "Exercice 55 : RMS24-871"  
	Soit $k \in \mathbb{C}$. Soit $A=\left(\begin{array}{llll}0 & 1 & 0 & 0 \\ 1 & k & 1 & 1 \\ 0 & 1 & 0 & 0 \\ 0 & 1 & 0 & 0\end{array}\right)$. Étudier la diagonalisabilité de $A$ en fonction de $k$.


!!! Exercice "Exercice 56 : RMS24-873"  
	a) Soit $A \in \mathcal{M}_{n}(\mathbb{R})$ telle que $A^{2}$ soit diagonalisable et $\left.\operatorname{Sp}\left(A^{2}\right) \subset\right] 0,+\infty[$. Montrer que $A$ est diagonalisable.  
	b) Diagonaliser $A=\left(\begin{array}{cccc}a & b & \ldots & b \\ b & a & \ddots & \\ & \ddots & \ddots & b \\ b & \ldots & b & a\end{array}\right)$ et $B=\left(\begin{array}{cccc}b & \cdots & b & a \\ \vdots & . & . & b \\ b & . & . & \vdots \\ a & b & \cdots & b\end{array}\right)$ avec $a \in \mathbb{R}$ et $b \in \mathbb{R}^{*}$.


!!! Exercice "Exercice 57 : RMS24-884"  
	Soit $A \in \mathcal{M}_{n}(\mathbb{R})$ telle que $A^{3}-A-I_{n}=0$. Montrer que $\operatorname{det} A>0$.


!!! Exercice "Exercice 58 : RMS24-885"  
	Soit $A \in \mathcal{M}_{n}(\mathbb{R})$ telle que la suite $\left(A^{k}\right)_{k \in \mathbb{N}}$ converge vers une matrice $B$.  
	a) Montrer que $B^{2}=B$.  
	b) On suppose désormais que $A$ est diagonalisable avec $p$ valeurs propres.  
	  
	En considérant une division euclidienne, montrer que : $\forall k \in \mathbb{N}, A^{k} \in \mathbb{R}_{p-1}[A]$.  
	c) Décrire $B$ à l'aide des éléments propres de $A$.


!!! Exercice "Exercice 59 : RMS24-1274"  
	Soit $M(a, b, c)=\left(\begin{array}{ccc}a & 0 & c \\ 0 & b & 0 \\ c & 0 & a\end{array}\right)$ avec $a, b, c \in \mathbb{R}$.  
	a) Déterminer les valeurs propres de $M(a, b, c)$ et son déterminant.  
	b) Déterminer le noyau et l'image de la matrice.  
	c) La matrice $M(a, b, c)$ est-elle diagonalisable?


!!! Exercice "Exercice 60 : RMS24-1275"  
	Soit la matrice $M=\left(m_{i, j}\right)_{1 \leqslant i, j \leqslant n}$ définie par $m_{i, j}=a$ si $i=j$ et $m_{i, j}=b$ si $i \neq j$.  
	a) Déterminer les puissances de $M$.  
	b) Déterminer les éléments propres de $M$.


!!! Exercice "Exercice 61 : RMS24-1278"  
	On définit l'application $\phi$ qui à un polynôme $P$ de $\mathbb{R}_{3}[X]$ associe le reste de la division euclidienne de $X^{2} P$ par $X^{4}-1$.  
	a) Montrer que $\phi$ est un endomorphisme et donner sa matrice $A$ dans la base canonique.  
	b) Montrer que $\phi$ est diagonalisable et donner ses valeurs propres ainsi que ses sous espaces propres.  
	c) L'endomorphisme $\phi$ est-il inversible? Si oui, donner son l'inverse.


!!! Exercice "Exercice 62 : RMS24-1281"  
	Soient $A \in \mathcal{M}_{n}(\mathbb{R})$ non nulle et $\Phi: M \in \mathcal{M}_{n}(\mathbb{R}) \mapsto \operatorname{tr}(A M) I_{n} \in \mathcal{M}_{n}(\mathbb{R})$.  
	a) Exprimer $\Phi^{2}$ en fonction de $\Phi$.  
	b) Donner une condition nécessaire et suffisante pour que $\Phi$ soit diagonalisable. Préciser ses espaces propres.


!!! Exercice "Exercice 63 : RMS24-1285"  
	Soit $A \in \mathcal{M}_{n}(\mathbb{C})$. Soit $f_{A} \in \mathcal{L}\left(\mathcal{M}_{n}(\mathbb{C})\right)$ définie par $\forall M \in \mathcal{M}_{n}(\mathbb{C}), f_{A}(M)=A M$.  
	a) Montrer que $A$ est diagonalisable si et seulement si $f_{A}$ est diagonalisable.  
	b) Soient $\left(X_{1}, \ldots, X_{n}\right)$ et $\left(Y_{1}, \ldots, Y_{n}\right)$ deux bases de $\mathbb{C}^{n}$.  
	  
	Montrer que $\left(X_{i} Y_{j}^{T}\right)_{1 \leqslant i, j \leqslant n}$ est une base de $\mathcal{M}_{n}(\mathbb{C})$.  
	c) On suppose que $A$ est diagonalisable. Construire une base de vecteurs propres de $f_{A}$ à partir d'une base de vecteurs propres de $A$.


!!! Exercice "Exercice 64 : RMS24-1518"  
	[CCINP] a) Soient $A, B \in \mathcal{M}_{n}(\mathbb{R})$ deux matrices semblables. Soit $P \in \mathbb{R}[X]$. Montrer que $P(A)$ et $P(B)$ sont semblables.  
	Soient $A \in \mathcal{M}_{n}(\mathbb{R})$ et $M=\left(\begin{array}{cc}A & A \\ 0 & A\end{array}\right) \in \mathcal{M}_{2 n}(\mathbb{R})$.  
	a) Expliciter $M^{k}$ pour $k \in \mathbb{N}$.  
	b) Soit $P \in \mathbb{R}[X]$. Exprimer $P(M)$ en fonction de $A, P^{\prime}(A)$ et $P(A)$.  
	c) Montrer que si $M$ est diagonalisable, alors $A$ l'est aussi.  
	d) Étudier la réciproque si $A$ est inversible.  
	e) Montrer que, si $A$ n'est pas inversible et si $M$ est diagonalisable, alors $A=0$.


!!! Exercice "Exercice 65 : RMS24-1543"  
	[IMT] Soit $E=\mathcal{C}^{0}(\mathbb{R}, \mathbb{R})$. Pour $f \in E$, on pose, pour $x \in \mathbb{R}^{*}, \phi(f)(x)=\frac{1}{x} \int_{0}^{x} f(t) \mathrm{d} t$ et $\phi(f)(0)=f(0)$.  
	a) Montrer que $\phi$ est un endomorphisme.  
	b) Montrer que $\phi$ est injectif.  
	c) Déterminer les valeurs propres et les vecteurs propres de $\phi$.



## Intégration de spé


!!! Exercice "Exercice 66 : RMS24-926"  
	a) Justifier que $I=\int_{0}^{+\infty}\left\lfloor\frac{1}{\sqrt{x}}\right\rfloor \mathrm{d} x$ converge.  
	b) Calculer explicitement $I$ en admettant que $\sum_{k=1}^{+\infty} \frac{1}{k^{2}}=\frac{\pi^{2}}{6}$.


!!! Exercice "Exercice 67 : RMS24-929"  
	Soit $f: \mathbb{R} \rightarrow \mathbb{R}$ continue et $T$-périodique. On se propose de prouver l'existence d'un unique $\lambda \in \mathbb{R}$ tel que l'intégrale $\int_{1}^{+\infty} \frac{\lambda-f(t)}{t} \mathrm{~d} t$ converge.  
	a) Étudier le cas particulier où $f=\sin$.  
	b) Traiter le cas général.


!!! Exercice "Exercice 68 : RMS24-1307"  
	Soit $I=\int_{0}^{+\infty} \frac{\sin ^{3} t}{t^{2}} \mathrm{~d} t$.  
	a) Justifier la convergence de $I$.  
	b) Montrer que $\forall t \in \mathbb{R}, \sin ^{3} t=-\frac{1}{4} \sin (3 t)+\frac{3}{4} \sin (t)$.  
	c) Montrer que $I=\frac{3}{4} \lim _{x \rightarrow 0} \int_{x}^{3 x} \frac{\sin (t)}{t^{2}} \mathrm{~d} t$.  
	d) Montrer que $g: t \mapsto \frac{\sin (t)-t}{t^{2}}$ est prolongeable par continuité en 0 .  
	e) En déduire la valeur de $I$.


!!! Exercice "Exercice 69 : RMS24-1308"  
	Soient $f \in \mathcal{C}^{0}\left(\mathbb{R}^{+}, \mathbb{R}\right)$ et $F: x \mapsto \int_{0}^{x} f(t)^{2} \mathrm{~d} t$.  
	  
	On suppose que $f(x) F(x) \underset{x \rightarrow+\infty}{\longrightarrow} \ell \in \mathbb{R}^{+*}$.  
	a) Montrer que $f$ admet une limite finie en $+\infty$ et préciser cette limite.  
	b) Soit $g \in \mathcal{C}^{0}\left(\mathbb{R}^{+}, \mathbb{R}\right)$ telle que $g(x) \underset{x \rightarrow+\infty}{\longrightarrow} \ell^{\cdot} \in \mathbb{R}$. Montrer que $\frac{1}{x} \int_{0}^{x} g(t) \mathrm{d} t \underset{x \rightarrow+\infty}{\longrightarrow} \ell^{\prime}$.  
	c) Trouver un équivalent de $F$ et de $f$ en $+\infty$.



## Théorèmes d'intégration


!!! Exercice "Exercice 70 : RMS24-947"  
	Soient $f \in \mathcal{C}^{0}([0,1], \mathbb{R})$ et, pour $n \in \mathbb{N}, I_{n}=\int_{0}^{1} f\left(t^{n}\right) \mathrm{d} t$. Limite de $\left(I_{n}\right)$ ?


!!! Exercice "Exercice 71 : RMS24-948"  
	a) Soient $a$ et $b$ deux réels $>0$. Montrer que $\int_{0}^{1} \frac{t^{a-1}}{1+t^{b}} \mathrm{~d} t=\sum_{n=0}^{+\infty} \frac{(-1)^{n}}{a+b n}$.  
	b) Calculer $\sum_{n=0}^{+\infty} \frac{(-1)^{n}}{1+3 n}$ et $\sum_{n=0}^{+\infty} \frac{(-1)^{n}}{1+4 n}$.


!!! Exercice "Exercice 72 : RMS24-949"  
	Soit $f: x \mapsto \int_{0}^{+\infty} \frac{\arctan (t x)}{t\left(1+t^{2}\right)} \mathrm{d} t$.  
	a) Montrer que : $\forall u \in \mathbb{R},|\arctan (u)| \leqslant|u|$.  
	b) Montrer que $f$ est de classe $\mathcal{C}^{1}$ sur $\mathbb{R}$.  
	c) Déterminer le développement en éléments simples de $t \mapsto \frac{1}{1+x^{2} t^{2}\left(1+t^{2}\right)}$ pour $|x| \neq 1$.  
	d) Montrer que $f(x)=\frac{\pi}{2(x+1)}$ pour $x>0$. En déduire la valeur de $f \operatorname{sur} \mathbb{R}$.  
	e) Déterminer $\int_{0}^{+\infty}\left(\frac{\arctan (t)}{t}\right)^{2} \mathrm{~d} t$.


!!! Exercice "Exercice 73 : RMS24-952"  
	Soit $f: x \mapsto \int_{0}^{+\infty} \frac{1}{x+e^{t}} \mathrm{~d} t$.  
	a) Montrer que $f$ est définie au moins sur un intervalle de la forme $]-\alpha, \alpha[\operatorname{avec} \alpha>0$.  
	b) Montrer que $f$ est développable en série entière au voisinage de 0 .  
	c) Calculer ce développement et en déduire une expression $f(x)$.


!!! Exercice "Exercice 74 : RMS24-956"  
	Soit $F: x \mapsto \int_{0}^{+\infty} \frac{t^{3}}{\sqrt{1+t^{4}}} e^{-x t} \mathrm{~d} t$.  
	a) Déterminer le domaine de définition $I$ de $F$.  
	  
	Montrer que $F$ est de classe $\mathcal{C}^{1}$ sur $I$ et donner son sens de variation.  
	b) Déterminer les limites de $F$ aux bornes de $I$.  
	c) Calculer $G(x)=\int_{0}^{+\infty} t^{3} e^{-x t} \mathrm{~d} t$ pour $x>0$.  
	d) Montrer que $F(x) \underset{x \rightarrow+\infty}{\sim} \frac{6}{x^{4}}$.  
	  
	Ind. On pourra étudier $|F-G|$ et utiliser la relation de Chasles.


!!! Exercice "Exercice 75 : RMS24-1325"  
	a) Énoncer les théorèmes de convergence dominée et d'intégration terme à terme.  
	b) On veut montrer la relation suivante : $\int_{0}^{+\infty} \frac{t}{\operatorname{ch}(t)} \mathrm{d} t=2 \sum_{n=0}^{+\infty} \frac{(-1)^{n}}{(2 n+1)^{2}}$.  
	i) Justifiez la convergence de l'intégrale.  
	ii) Montrer que: $\forall t \geqslant 0, \frac{t}{\operatorname{ch}(t)}=2 \sum_{n=0}^{+\infty}(-1)^{n} t e^{-(2 n+1) t}$.  
	iii) Appliquer le théorème d'intégration terme à terme et conclure.  
	c) On veut montrer la relation suivante : $\int_{0}^{+\infty} \frac{\cos (t)}{1+e^{t}} \mathrm{~d} t=\sum_{n=1}^{+\infty}(-1)^{n-1} \frac{n}{n^{2}+1}$.  
	i) Justifiez la convergence de l'intégrale.  
	ii) Montrer que : $\forall t \in \mathbb{R}^{+*}, \frac{\cos (t)}{1+e^{t}}=\sum_{n=1}^{+\infty}(-1)^{n-1} e^{-n t} \cos (t)$.  
	iii) Le théorème d'intégration terme à terme s'applique-t-il?  
	iv) Établir la relation attendue.


!!! Exercice "Exercice 76 : RMS24-1326"  
	Soit $\varphi: x \mapsto \int_{0}^{\infty} \frac{\sin (x t) e^{-t}}{t} \mathrm{~d} t$.  
	a) Montrer que $\varphi$ est définie sur $\mathbb{R}$ et qu'elle y est dérivable.  
	b) Déterminer $\varphi^{\prime}$ et en déduire $\varphi$ à l'aide des fonctions usuelles.



## Espaces euclidiens


!!! Exercice "Exercice 77 : RMS24-891"  
	Soient $E=\mathbb{R}_{4}[X], F$ le sous-espace vectoriel de $E$ formé des polynômes pairs, $G$ le sous-espace vectoriel de $E$ formé des polynômes impairs.  
	Pour $P, Q \in E$, on note $\Phi(P, Q)=\sum_{k=0}^{4}\left(P(k)+(-1)^{k} P(-k)\right)\left(Q(k)+(-1)^{k} Q(-k)\right)$.  
	a) Montrer que $\Phi$ est un produit scalaire sur $E$.  
	b) Montrer que $E=F \stackrel{\perp}{\oplus} G$.  
	c) Déterminer une base orthonormée de $E$ adaptée à $E=F \stackrel{\perp}{\oplus} G$.


!!! Exercice "Exercice 78 : RMS24-892"  
	Soit $E$ un espace euclidien de dimension 3 . On considère une isométrie indirecte $f$. Montrer que $f$ se décompose en une rotation d'axe $\Delta$ et une réflexion de plan $\Delta^{\perp}$. Cette décomposition est-elle unique ? La rotation et la réflexion commutent-elles?


!!! Exercice "Exercice 79 : RMS24-895"  
	Soit $(E,\langle\rangle$,$) un espace euclidien. Pour x_{1}, \ldots, x_{p}$ dans $E$, on note $G\left(x_{1}, \ldots, x_{p}\right)$ la matrice de coefficient $G_{i, j}=\left\langle x_{i}, x_{j}\right\rangle$.  
	a) Montrer que : $G$ est inversible si et seulement si $\left(x_{1}, \ldots, x_{p}\right)$ est libre.  
	b) Montrer que $\operatorname{rg}(G)=\operatorname{rg}\left(x_{1}, \ldots, x_{p}\right)$.


!!! Exercice "Exercice 80 : RMS24-897"  
	On munit $\mathbb{R}^{3}$ de sa structure euclidienne canonique. Soit $u$ l'endomorphisme de $\mathbb{R}^{3}$ dont la matrice dans la base canonique est $\frac{1}{3}\left(\begin{array}{ccc}2 & 2 & -1 \\ -1 & 2 & 2 \\ 2 & -1 & 2\end{array}\right)$. Déterminer sa nature et ses valeurs propres.


!!! Exercice "Exercice 81 : RMS24-901"  
	Soient $E$ un espace euclidien et $u \in \mathcal{L}(E)$. Montrer qu'il existe une base orthonormée $\left(e_{1}, \ldots, e_{n}\right)$ telle que la famille $\left(u\left(e_{1}\right), \ldots, u\left(e_{n}\right)\right)$ soit orthogonale.


!!! Exercice "Exercice 82 : RMS24-906"  
	Soit $A \in \mathcal{S}_{n}(\mathbb{R})$. On dit que $A \in \mathcal{S}_{n}^{++}(\mathbb{R})$ lorsque, pour toute matrice $X \in \mathcal{M}_{n, 1}(\mathbb{R})$ non nulle, $X^{T} A X>0$.  
	a) Déterminer une condition nécessaire et suffisante pour que $A \in \mathcal{S}_{n}^{++}(\mathbb{R})$.  
	b) Soit $A \in \mathcal{S}_{n}^{++}(\mathbb{R}): A=\left(\begin{array}{cc}B & C \\ C^{T} & D\end{array}\right)$. Montrer que $\operatorname{det}(B)>0$, puis montrer que $\operatorname{det}(A) \leqslant \operatorname{det}(B) \operatorname{det}(D)$.  



!!! Exercice "Exercice 83 : RMS24-1288"  
	On se place dans $E=\mathbb{R}_{3}[X]$ et on pose, pour $0 \leqslant i \leqslant 3, L_{i}(X)=\prod_{\substack{0 \leq k \leqslant 3 \\ k \neq i}} \frac{X-k}{i-k}$.  
	a) En calculant $L_{i}(k)$, montrer que les $L_{i}$ forment une base de $E$.  
	b) On pose $\phi:(P, Q) \in E^{2} \mapsto \sum_{k=0}^{3}(P(k)+P(1))(Q(k)+Q(1))$. Montrer que $\phi$ est un produit scalaire sur $E$.  
	c) Déterminer une base orthonormée de $E$ pour $\phi$.


!!! Exercice "Exercice 84 : RMS24-1290"  
	On munit $\mathbb{R}^{n}$ de sa structure euclidienne canonique.  
	a) Soient $M \in \mathcal{M}_{n}(\mathbb{R})$ et $F$ un sous-espace vectoriel de $\mathbb{R}^{n}$. Montrer que $F$ est stable par $M$ si et seulement si $F^{\perp}$ est stable par $M^{T}$.  
	b) Trouver les plans de $\mathbb{R}^{3}$ stable par $A=\left(\begin{array}{ccc}1 / 2 & 1 & 1 / 2 \\ 0 & 1 & 0 \\ -1 / 2 & 1 & 3 / 2\end{array}\right)$.



## Espaces normés


!!! Exercice "Exercice 85 : RMS24-869"  
	Soient $a, b, c \in \mathbb{R}$ et $A=\left(\begin{array}{ccc}0 & -a & b \\ a & 0 & -c \\ -b & c & 0\end{array}\right)$.  
	a) Montrer qu'il existe $d$ tel que $A^{3}+d A=0$.  
	b) Déterminer $d$. Soit $n \in \mathbb{N}^{*}$, déterminer $A^{2 n}$ en fonction de $d, n$ et $A^{2}$.  
	c) Déterminer $\alpha$ et $\beta$ tels que $\sum_{k=0}^{+\infty} \frac{A^{k}}{k!}=I_{3}+\alpha A+\beta A^{2}$.


!!! Exercice "Exercice 86 : RMS24-907"  
	Soient $E=\mathcal{C}^{0}([0,1], \mathbb{R})$ et $\varphi \in E$. On note, pour $f \in E, N_{\varphi}(f)=\|f \varphi\|_{\infty}$.  
	a) Montrer que $N_{\varphi}$ est une norme si et seulement si $\varphi^{-1}(\{0\})$ est d'intérieur vide.  
	b) Montrer que $N_{\varphi}$ et $\left\|\|_{\infty}\right.$ sont équivalentes si et seulement si $\varphi^{-1}(\{0\})$ est vide.


!!! Exercice "Exercice 87 : RMS24-908"  
	Soient $E$ un $\mathbb{R}$ espace vectoriel, $N_{1}$ et $N_{2}$ deux normes sur $E$.  
	a) Soit ( $u_{n}$ ) une suite qui converge dans $\left(E, N_{1}\right)$. On suppose que $N_{1}$ et $N_{2}$ sont équivalentes. Montrer que ( $u_{n}$ ) converge dans ( $E, N_{2}$ ).  
	b) On suppose qu'une suite $\left(u_{n}\right)$ converge dans $\left(E, N_{1}\right)$ si et seulement si $\left(u_{n}\right)$ converge dans $\left(E, N_{2}\right)$. Montrer que $N_{1}$ et $N_{2}$ sont équivalentes.  
	c) On prend $E=\mathbb{R}[X]$ et, pour $a \in \mathbb{R}, N_{a}(P)=|P(a)|+\int_{0}^{1}\left|P^{\prime}(t)\right| \mathrm{d} t$. Montrer que, si $a, b \in[0,1], N_{a}$ et $N_{b}$ sont équivalentes.  
	d) Soit, pour $n \in \mathbb{N}, P_{n}=\frac{X^{n}}{2^{n}}$. Trouver les valeurs de $a$ telles que $\left(P_{n}\right)$ converge pour $N_{a}$ et déterminer alors la limite.  
	e) En déduire que $N_{a}$ et $N_{b}$ ne sont pas équivalentes si $0 \leqslant a<b$ et $b>1$.


!!! Exercice "Exercice 88 : RMS24-1066"  
	Les parties $E=\left\{(x, y) \in \mathbb{R}^{2}, x^{2}(x-1)(x-3)+y^{2}\left(y^{2}-4\right)=0\right\}$ et $F=\left\{(x, y) \in \mathbb{R}^{2}, 2 x^{2}-y(y-1)=0\right\}$ sont elles fermées? bornées?


!!! Exercice "Exercice 89 : RMS24-1298"  
	Soit $A \in \mathcal{S}_{3}(\mathbb{R})$. Pour $X \in \mathbb{R}^{3} \backslash\{0\}$, on pose $q(X)=\frac{X^{T} A X}{X^{T} X}$.  
	a) Énoncer le théorème spectral pour les matrices symétriques réelles.  
	b) Soient $\lambda_{1} \leqslant \lambda_{2} \leqslant \lambda_{3}$ les valeurs propres de $A$, distinctes ou non.  
	  
	Montrer que $\lambda_{3}=\max \left\{q(X), X \in \mathcal{M}_{3,1}(\mathbb{R}) \backslash\{0\}\right\}$. Énoncer une propriété similaire pour $\lambda_{1}$.  
	c) Soit $\mathcal{P}$ l'ensemble des plans vectoriels de $\mathbb{R}^{3}$. Pour $P \in \mathcal{P}$, justifier l'existence de $\max \left\{X^{T} A X ; X \in P,\|X\|=1\right\}$, puis montrer que :  
	$\lambda_{2}=\min _{P \in \mathcal{P}}\left(\max \left\{X^{T} A X ; X \in P,\|X\|=1\right\}\right)$.  




!!! Exercice "Exercice 90 : RMS24-1299"  
	Soit $E=\mathbb{R}[X]$. Pour $a \in \mathbb{R}$ et $P \in E$, on pose $N_{a}(P)=|P(a)|+\int_{0}^{1}\left|P^{\prime}(t)\right| \mathrm{d} t$  
	a) Montrer que $N_{a}$ est une norme sur $E$.  
	b) $i)$ Montrer que $N_{0}$ et $N_{1}$ sont équivalentes.  
	ii) Montrer qu'une suite de polynômes $\left(P_{n}\right)$ converge dans $\left(E, N_{0}\right)$ si et seulement si elle converge dans $\left(E, N_{1}\right)$.  
	c) Soit $(a, b) \in[0,1]^{2}$. Montrer que $N_{a}$ et $N_{b}$ sont équivalentes.  
	d) Soit $(a, b) \in\left[1,+\infty\left[^{2}\right.\right.$ avec $a<b$. Montrer que $N_{a}$ et $N_{b}$ ne sont pas équivalentes.  
	  
	Ind. Poser $P_{n}=X^{n}$.  
	d) Que dire si $E=\mathbb{R}_{n}[X]$ ?



## Probabilités


!!! Exercice "Exercice 91 : RMS24-971"  
	On considère une classe de PSI constituée de $N$ élèves, dont $n$ provenant de PCSI et $N-n$ de MPSI. On envoie successivement au tableau des élèves choisis au hasard. Un élève peut passer plusieurs fois au tableau.  
	a) Quelle est la probabilité qu'au cours des $n$ premiers passages, il n'y ait que des élèves de PCSI?  
	b) Quelle est la probabilité qu'au cours des $n+5$ premiers passages, il y ait $n$ élèves de PCSI?  
	c) Soit $i \in \mathbb{N}^{*}$. On note $X_{i}$ la variable aléatoire qui compte le nombre de tirages nécessaires pour faire passer $i$ élèves de PCSI distincts au tableau. Déterminer la loi de $X_{i}$.


!!! Exercice "Exercice 92 : RMS24-972"  
	On considère initialement une urne contenant une boule blanche et une boule rouge. On tire une boule, on note sa couleur, on la remet dans l'urne et on rajoute deux boules de la même couleur que celle tirée. On répète indéfiniment le processus.  
	a) Calculer la probabilité de ne tirer que des boules rouges lors des $n$ premiers tirages?  
	b) Calculer la probabilité de tirer indéfiniment uniquement des boules rouges?  
	c) Calculer la probabilité de tirer une boule blanche au 42-ième tirage.  
	d) Le résultat de la question $\boldsymbol{b}$ ) reste-t-il vrai si on rajoute 3 boules (au lieu de 2)? 4 boules?


!!! Exercice "Exercice 93 : RMS24-973"  
	a) Calculer $\int_{0}^{1} x^{p}(1-x)^{q} \mathrm{~d} x$ avec $p, q \in \mathbb{N}$.  
	b) On dispose de $p$ urnes contenant chacune $p$ boules. Pour $i \in \llbracket 1, p \rrbracket$, l'urne $i$ contient $i$ boules noires et $p-i$ blanches. On choisit une des urnes aléatoirement et on en tire successi-  
	vement des boules avec remise. On note $A_{n, p}$ l'évènement : on tire $2 n$ boules et on a autant de boules noires que de boules blanches.  
	i) Exprimer $P\left(A_{n, p}\right)$ sous forme d'une somme.  
	ii) Déterminer la limite de $\mathbf{P}\left(A_{n, p}\right)$ quand $n$ tend vers $+\infty$.  
	iii) Déterminer la limite de $\mathbf{P}\left(A_{n, p}\right)$ quand $p$ tend vers $+\infty$.


!!! Exercice "Exercice 94 : RMS24-977"  
	a) Soit $S: t \mapsto \sum_{n=0}^{+\infty} \frac{n^{2}+n+1}{n!} t^{n}$. Déterminer le rayon de convergence et donner une expression de $S$.  
	b) Soit $X$ une variable aléatoire à valeurs dans $\mathbb{N}$ de fonction génératrice $G_{X}=\lambda S$. Déterminer $\lambda$ et la loi de $X$.  
	c) Calculer $\mathbf{E}(X)$ et $\mathbf{V}(X)$.


!!! Exercice "Exercice 95 : RMS24-980"  
	Soient $X$ et $Y$ deux variables aléatoires indépendantes de lois respectives $\mathcal{G}(p)$ et $\mathcal{G}(q)$, où $p$ et $q$ sont éléments de $] 0,1\left[\right.$. On pose $U=\frac{X}{Y}$.  
	a) Donner la loi de $U$.  
	b) Calculer l'espérance de $U$.  
	c) Si $p=q$, montrer que $\mathbf{E}(U)>1$.


!!! Exercice "Exercice 96 : RMS24-982"  
	Soient $a, b>0, X, Y, Z$ des variables aléatoires indépendantes telles que $X \sim \mathcal{P}(a)$, $Y \sim \mathcal{P}(b), \mathbf{P}(Z=1)=1-p$ et $\mathbf{P}(Z=-1)=p$.  
	Quelle est la probabilité que la matrice $A=\left(\begin{array}{cc}X & Y \\ Y Z & X\end{array}\right)$ soit diagonalisable


!!! Exercice "Exercice 97 : RMS24-1331"  
	Un robot appuie sur une diode verte ou rouge à tout instant $n \in \mathbb{N}$. Lorsqu'il appuie sur la diode rouge à l'instant $n$, il appuie sur la diode verte à l'instant $n+1$ avec une probabilité $p \in] 0,1[$, ou sur la diode rouge avec probabilité $1-p$. Lorsqu'il appuie sur la diode verte à l'instant $n$, il appuie sur la diode rouge à l'instant $n+1$ avec une probabilité $q \in] 0,1[$, ou sur la diode verte avec probabilité $1-q$. On note $r_{n}$ la probabilité que le robot appuie sur la diode rouge à l'instant $n$, $v_{n}$ la probabilité que le robot appuie sur la diode verte à l'instant $n$.  
	a) Montrer qu'il existe $A \in \mathcal{M}_{2}(\mathbb{R})$ telle que $\forall n \in \mathbb{N},\binom{r_{n+1}}{v_{n+1}}=A\binom{r_{n}}{v_{n}}$.  
	b) Déterminer $B, C \in \mathcal{M}_{2}(\mathbb{R})$ telles que $B+C=I_{2}$ et $A=B+(1-p-q) C$.  
	c) En déduire une expression de $A^{n}$.  
	d) Déterminer $\lim _{n \rightarrow+\infty} r_{n}$ et $\lim _{n \rightarrow+\infty} v_{n}$. Commenter.


!!! Exercice "Exercice 98 : RMS24-1332"  
	a) Soient $X_{0}$ et $Y_{0}$ deux variables aléatoires indépendantes suivant la loi uniforme sur $\llbracket 1,6 \rrbracket$. Déterminer la loi de $X_{0}+Y_{0}$.  
	Soient $X$ et $Y$ deux variables aléatoires indépendantes à valeurs dans [1,6].  
	b) Montrer que $X+Y$ ne peut pas suivre la loi uniforme sur $[2,12 \rrbracket$.  
	c) Montrer que si $X+Y \sim X_{0}+Y_{0}$, alors $X$ et $Y$ suivent la loi uniforme sur $\llbracket 1,6 \rrbracket$.


!!! Exercice "Exercice 99 : RMS24-1333"  
	On considère $n$ urnes numérotées de 1 à $n$ et $b$ boules numérotées de 1 à $b$. On place les boules au hasard dans les urnes. Soit $k \in\{0, \ldots, b\}$.  
	a) Justifier qu'on peut modéliser le problème par l'univers $\Omega$ des fonctions de $\{1, \ldots, b\}$ dans $\{1, \ldots, n\}$ muni de la probabilité uniforme. Calculer la probabilité $p_{n, b}$ de l'événement «l'urne 1 contient $k$ boules».  
	b) En modélisant le problème par une suite de variables aléatoires de Bernoulli indépendantes, retrouver le résultat précédent.  
	c) Soit une suite d'entiers $\left(b_{n}\right)_{n \geqslant 1}$ telle que $b_{n} \sim n c$. Montrer que $p_{n, b_{n}} \rightarrow e^{-c} \frac{c^{k}}{k!}$ lorsque $n \rightarrow+\infty$.



## Calcul diff


!!! Exercice "Exercice 100 : RMS24-911"  
	Soit $f:(x, y) \in\left(\mathbb{R}^{+*}\right)^{2} \mapsto x^{2}+y^{2}+\frac{3}{x y}$. La fonction $f$ est-elle prolongeable par continuité en $(0,0)$ ?


!!! Exercice "Exercice 101 : RMS24-959"  
	Soit $(E)$ l'équation différentielle : $x^{2} y^{\prime}(x)+y(x)=x^{2}$.  
	a) Montrer que $(E)$ n'admet pas de solution développable en série entière.  
	b) Résoudre l'équation différentielle sur $] 0,+\infty[$.  
	c) Montrer qu'il existe une unique solution tendant vers 0 en $0^{+}$.


!!! Exercice "Exercice 102 : RMS24-963"  
	On note $(E)$ l'équation différentielle $x^{2} y^{\prime \prime}-2 x y^{\prime}+2 y=2(1+x)$.  
	a) Trouver les solutions de l'équation homogène associée de la forme $x \mapsto x^{\alpha}$, où $\alpha \in \mathbb{R}$.  
	b) Trouver une solution particulière de $(E)$, d'abord sur $] 0,+\infty[$, puis sur $]-\infty, 0[$.  
	  
	Ind. On la cherchera sous la forme $x \alpha(x)+x^{2} \beta(x)$, où $\alpha$ et $\beta$ sont des fonctions de classe $\mathcal{C}^{1}$ telles que $x \alpha^{\prime}(x)+x^{2} \beta^{\prime}(x)=0$.  
	c) L'équation $(E)$ admet-elle des solutions sur $\mathbb{R}$ ?


!!! Exercice "Exercice 103 : RMS24-965"  
	a) Soit $\alpha \in \mathbb{R}$. À l'aide d'un changement de variables classique, résoudre l'équation $x \frac{\partial f}{\partial x}(x, y)+y \frac{\partial f}{\partial y}(x, y)=\alpha f(x, y)$ d'inconnue $f \in \mathcal{C}^{1}\left(\mathbb{R}^{+*} \times \mathbb{R}^{+*}, \mathbb{R}\right)$.  
	b) Résoudre $x \frac{\partial f}{\partial x}(x, y)+y \frac{\partial f}{\partial y}(x, y)=\sqrt{x^{2}+y^{2}} f(x, y)$ d'inconnue $f \in \mathcal{C}^{1}\left(\mathbb{R}^{+*} \times \mathbb{R}^{+*}, \mathbb{R}\right)$.


!!! Exercice "Exercice 104 : RMS24-967"  
	On pose $f(x, y)=\frac{1}{1-y^{2}} \ln \left(\frac{x+y}{1+x y}\right)$. On note $\Omega$ l'ensemble de définition de $f$.  
	a) Représenter $\Omega$ et montrer que c'est un ouvert.  
	b) Monter que $f$ est de classe $\mathcal{C}^{1}$ sur $\Omega$.  
	c) Comparer $f(1 / x, y)$ et $f(x, y)$. Donner une interprétation géométrique pour $x>0$ et $y \in] 0,1[$.  
	d) Montrer que $f$ vérifie : $2 y f+\left(1-x^{2}\right) \frac{\partial f}{\partial x}-\left(1-y^{2}\right) \frac{\partial f}{\partial y}=0$.


!!! Exercice "Exercice 105 : RMS24-1575"  
	[CCINP] On considère l'équation différentielle $(E): t^{2} y^{\prime \prime}+t y^{\prime}+y=\frac{1}{t}+t$ sur $\mathbb{R}^{+*}$.  
	a) Énoncer le théorème de Cauchy linéaire.  
	b) On pose $g: x \mapsto f\left(e^{x}\right)$. Montrer que $f$ est solution de $(E)$ sur $\mathbb{R}^{+*}$ si et seulement si $g$ est solution d'une équation différentielle du second ordre que l'on déterminera.  
	c) Déterminer l'ensemble des solutions de $(E)$.


!!! Exercice "Exercice 106 : RMS24-1578"  
	[CCINP] Pour $(x, y) \in \mathbb{R} \times \mathbb{R}^{+*}$, on note $f(x, y)=x^{2} y+y \ln ^{2}(y)$.  
	a) Déterminer le(s) point(s) critique(s) de $f$.  
	b) Nature globale de ce(s) point(s) critique(s).  

