Les séances de "fil rouge" auront lieu le jeudi après midi et le vendredi matin. Les élèves inscrits passent au tableau et on discute ensuite de l'exercice et de la prestation. J'essayerai de marquer au fur et à mesure les exercices traités. 

Les élèves doivent être attentifs à leur tour de passage, notamment pour les séances du jeudi après midi qui se font en demie classe.

!!! exercice "68"
    Soit $\omega=\exp (2 i \pi / 7)$. On pose $S=\omega+\omega^2+\omega^4$ et $T=\omega^3+\omega^5+\omega^6$. Calculer $S+T$ et $S T$. En déduire $S$ et $T$.
    
!!! exercice "51"
    Soit $f: z \mapsto \frac{z+1}{z-i}$. Trouver l'ensemble des $z$ tels que $f(z) \in \mathbb{R}$, puis tels que $|f(z)|=2$.
    
    
!!! exercice "98"
    Déterminer les racines de $P=(X-1)^{2 n+1}-1$. En déduire $\prod_{k=0}^{2 n} \cos \left(\frac{k \pi}{2 n+1}\right)$.
    
!!! exercice "110"
    Soient $\left(a_0, \ldots, a_{n-1}\right) \in \mathbb{C}^n$ et $P=a_0+a_1 X+\cdots+a_{n-1} X^{n-1}+X^n$. Montrer que les racines complexes de $P$ ont un module majoré $\operatorname{par} \max \left\{1,\left|a_0\right|+\left|a_1\right|+\cdots+\left|a_{n-1}\right|\right\}$.
    
!!! exercice "319"
    Soit $u \in \mathcal{L}\left(\mathbb{R}^n\right)$. On suppose que la matrice de $u$ dans toute base de $\mathbb{R}^n$ est diagonale. Que peut-on dire de $u$ ?
    
!!! exercice "266"
    Soient $A \in \mathscr{M}_n(\mathbb{C})$ et $f: X \in \mathscr{M}_n(\mathbb{C}) \mapsto A X+X A$. Montrer que $f$ est un endomorphisme et calculer sa trace.
    

!!! exercice "256"
    (a) Montrer : Vect $\left\{A B-B A,(A, B) \in \mathscr{M}_n(\mathbb{K})^2\right\}=\left\{M \in \mathscr{M}_n(\mathbb{K}), \operatorname{tr}(M)=0\right\}$.

    (b) Soit $\phi \in \mathscr{L}\left(\mathscr{M}_n(\mathbb{K}), \mathbb{K}\right)$ telle que : $\forall(A, B) \in \mathscr{M}_n(\mathbb{K})^2, \phi(A B)=\phi(B A)$. Montrer que $\phi$ est proportionnelle à la trace.  
    
!!! exercice "301"
    Soit $f$ un endomorphisme d'un espace vectoriel $E$ de dimension finie $E$.

    (a) Montrer : $\operatorname{Im} f=\operatorname{Im} f^2 \Longleftrightarrow \operatorname{Ker} f=\operatorname{Ker} f^2$.

    (b) Montrer : $\operatorname{Ker} f=\operatorname{Ker} f^2 \Longrightarrow E=\operatorname{Im} f+\operatorname{Ker} f$.

    (c) Montrer : $E=\operatorname{Im} f+\operatorname{Ker} f \Longrightarrow \operatorname{Im} f=\operatorname{Im} f^2$.
    
!!! exercice "307"
    Soient $E$ un espace vectoriel de dimension finie et $f \in \mathcal{L}(E)$. Montrer $\operatorname{dim}(\operatorname{Ker} f) \leqslant \operatorname{dim}\left(\operatorname{Ker} f^2\right) \leqslant 2 \operatorname{dim}(\operatorname{Ker} f)$.
    

!!! exercice "352"
    Soient $E$ un espace de dimension finie, $p$ et $q$ dans $\mathscr{L}(E)$ tels que $p+q=\operatorname{id}$ et $\operatorname{rg} p+\operatorname{rg} q \leqslant \operatorname{dim} E$. Montrer que $p$ et $q$ sont des projecteurs.
    

!!! exercice "107"
    Soit $P(X)=\sum_{k=0}^n a_k X^k \in \mathbb{R}[X]$ un polynôme de degré $n \geqslant 1$ ayant $n$ racines distinctes réelles. Montrer que $P$ n'a pas deux coefficients consécutifs nuls, autrement dit $\forall k \in \llbracket 0, n-1 \rrbracket,\left|a_k\right|+\left|a_{k+1}\right| \neq 0$.
    
!!! exercice "119"
    (a) Soit $P \in \mathbb{R}[X]$ scindé sur $\mathbb{R}$ et à racines simples. Montrer que $P^{\prime}$ l'est aussi.

    (b) Soit $P \in \mathbb{R}[X]$ scindé sur $\mathbb{R}$. Montrer que $P^{\prime}$ l'est aussi.


!!! exercice "1929"
    Soit $f \in \mathscr{C}^2([0,1], \mathbb{R})$ telle que $f(0)=0, f(1)=1, f^{\prime}(0)=0$. Montrer qu'il existe $c \in[0,1]$ tel que $f^{\prime \prime}(c)=2$.
    
!!! exercice "1930"
    Soient $(a, b) \in \mathbb{R}^2$ avec $a<b$ et $f \in \mathscr{C}^2([a, b], \mathbb{R})$. On suppose que $f(a)=f(b)=0$ et $f^{\prime}(a)=f^{\prime}(b)=0$. Montrer qu'il existe $c \in] a, b\left[\right.$ tel que $f^{\prime \prime}(c)=f(c)$.
    
!!! exercice "1859"
    Soit $f: \mathbb{R}_{+} \rightarrow \mathbb{R}$ continue et surjective. Soit $y \in \mathbb{R}$. Montrer que $y$ possède une infinité d'antécédents par $f$.
    
!!! exercice "1864"
    Soit $f:[a, b] \rightarrow[a, b]$ continue.
    
    (a) Montrer que $f$ possède un point fixe.
    
    (b) Soit $J$ un segment inclus dans $f([a, b])$. Montrer qu'il existe $(c, d) \in[a, b]^2$ tel que $f([c, d])=J$.
    

!!! exercice "2038"
    Soit $f:[a, b] \rightarrow[a, b]$ continue.

    (a) Montrer que $f$ possède un point fixe.

    (b) Soit $J$ un segment inclus dans $f([a, b])$. Montrer qu'il existe $(c, d) \in[a, b]^2$ tel que $f([c, d])=J$.


!!! exercice "2048"
    Soit $f \in \mathscr{C}^0([0,1], \mathbb{R})$. Montrer qu'il existe $c \in[0,1]$ tel que $f(c)=3 \int_0^1 f(t) t^2 \mathrm{~d} t$.
    
!!! exercice "1976"
    Montrer à l'aide de l'inégalité des accroissements finis que $\forall x \in \mathbb{R}_{+}, \frac{x}{1+x^2} \leqslant \arctan x \leqslant x$.
    
!!! exercice "1623"
    On pose $f(x)=1+\frac{\sin (1 / x)}{2}$ pour tout $x \in \mathbb{R}^*$.

    (a) Montrer que $f(x) \in\left[\frac{1}{2}, \frac{3}{2}\right]$ et que $(f \circ f)(x) \geqslant 1$ pour tout $x \in \mathbb{R}^*$.

    (b) Montrer que $f$ est $\frac{1}{2}$-lipschitzienne sur $[1,+\infty[$.

    (c) Montrer qu'il existe un unique $\ell \in[1,+\infty[$ tel que $f(\ell)=\ell$.

    (d) Soit $\left(u_n\right)_{n \geqslant 0}$ définie par $u_0=a \in \mathbb{R}^*$ et $\forall n \in \mathbb{N}$, $u_{n+1}=f\left(u_n\right)$. Montrer que $u_n \geqslant 1$ si $n \geqslant 2$. Montrer que $\left(u_n\right)_{n \geqslant 0}$ converge vers $\ell$.
    
    
    


!!! exercice "1997"
    Soit $f \in \mathcal{C}^2(\mathbb{R}, \mathbb{R})$ convexe et non affine. Si $a \in \mathbb{R}$, montrer que $f(x)+f(a-x) \rightarrow+\infty$ quand $x \rightarrow+\infty$.
    

!!! exercice "1998"
    Soit $I$ un intervalle non vide de $\mathbb{R}$.
    
    (a) Soit $f \in \mathcal{C}^0(I, \mathbb{R})$ convexe. Montrer que $f$ vérifie $(C)$ :

    $$
    \forall a \in I, \quad \forall r \in \mathbb{R}_{+}, \quad[a-r, a+r] \subset I \Rightarrow \int_{a-r}^{a+r} f(t) \mathrm{d} t \geqslant 2 r f(a) .   
    $$

    (b) Soit $f \in \mathcal{C}^2(I, \mathbb{R})$. On suppose qu'il existe $a \in I$ tel que $f^{\prime \prime}(a)<0$. Montrer que $f$ ne vérifie pas $(C)$.

    (c) Que peut-on déduire des deux questions précédentes?
    
    


!!! exercice "1521"
    Soit $\left(u_n\right)_{n \geqslant 0}$ une suite complexe.
    
    (a) On suppose que les suites $\left(u_{2 n}\right)_{n \geqslant 0}$ et $\left(u_{3 n+1}\right)_{n \geqslant 0}$ convergent. La suite $\left(u_n\right)_{n \geqslant 0}$ est-elle nécessairement convergente?
    
    (b) On suppose que les suites $\left(u_{2 n}\right)_{n \geqslant 0},\left(u_{2 n+1}\right)_{n \geqslant 0}$ et $\left(u_{3 n+1}\right)_{n \geqslant 0}$ convergent. La suite $\left(u_n\right)_{n \geqslant 0}$ est-elle nécessairement convergente?
    
    
!!! exercice "1536"
    Déterminer la limite de $S_n=\sum_{k=1}^n \sin \left(\frac{k}{n}\right) \sin \left(\frac{k}{n^2}\right)$.
    
!!! exercice "1599"
    Soit $\left(u_n\right)_{n \geqslant 0}$ définie par $u_0=0$ et $\forall n \in \mathbb{N}, u_{n+1}=\frac{u_n}{2}+\frac{1}{n+1}$. Déterminer un équivalent de $u_n$.
    

!!! exercice "1614"
    Soit $\left(u_n\right)_{n \geqslant 0}$ telle que $u_0=1$ et $\forall n \in \mathbb{N}, u_{n+1}=\frac{3+2 u_n}{2+u_n}$.

    (a) Montrer que $\left(u_n\right)$ converge vers une limite $\ell$ que l'on précisera.

    (b) Déterminer un équivalent de $\left(u_n-\ell\right)$.
    
!!! exercice "2006"
    Déterminer les applications $f: \mathbb{R} \rightarrow \mathbb{R}$ continues en 0 telles que $\forall x \in \mathbb{R}, f(2 x)=f(x) \cos x$.
    
!!! exercice "2016"
    (a) Déterminer les $f \in \mathscr{C}^0(\mathbb{R}, \mathbb{R})$ dérivables en 0 et telles que $\forall x \in \mathbb{R}, f(2 x)=2 f(x)$.

    (b) Exprimer $\operatorname{th}(2 x)$ en fonction de $\operatorname{th}(x)$ pour $x \in \mathbb{R}$.

    (c) Déterminer les $g \in \mathscr{C}^0(\mathbb{R}]-1,,1[)$ dérivables en 0 et telles que, pour tout $x \in \mathbb{R}, g(2 x)=\frac{2 g(x)}{1+g(x)^2}$.
    
    (d) Déterminer les $g \in \mathscr{C}^0(\mathbb{R}, \mathbb{R})$ dérivables en 0 et telles que, pour tout $x \in \mathbb{R}, g(2 x)=\frac{2 g(x)}{1+g(x)^2}$.
    
!!! exercice "2029"
    (a) Montrer que la fonction cos admet dans $\mathbb{R}$ un unique point fixe.
    
    (b) Montrer qu'il n'existe aucune fonction $f \in \mathscr{C}^1(\mathbb{R}, \mathbb{R})$ telle que $f \circ f=\cos$.



!!! exercice "2104"
    Pour $x>0$, on pose $F(x)=\int_0^{1 / x} \frac{\mathrm{d} t}{x+\sin ^2 t}$.

    (a) Montrer que $F$ est bien définie, étudier sa monotonie.

    (b) Déterminer la limite de $F$ en 0 et en $+\infty$.

    (c) Déterminer un équivalent de $F$ en 0 . Ind. Poser $\theta=\tan t$.
    
    
!!! exercice "2109"
    (a) Calculer $\prod_{k=1}^n\left(1-2 r \cos \left(\frac{k \pi}{n}\right)+r^2\right)$.

    (b) Pour $|r| \neq 1$, calculer $I(r)=\int_0^\pi \ln \left(1-2 r \cos t+r^2\right) \mathrm{d} t$.
    
    
    
!!! exercice "1728"
    Soit la suite définie par $a_0>0$ et $a_{n+1}=1-e^{-a_n}$.
    
    (a) Étudier la convergence de cette suite.

    (b) Déterminer la nature de la série de terme général $(-1)^n a_n$.

    (c) Déterminer la nature de la série de terme général $a_n^2$.

    (d) Étudier la série de terme général $\ln \left(\frac{a_{n+1}}{a_n}\right)$. En déduire la nature de la série de terme général $a_n$.
    

!!! exercice "1748"
    Soit, pour $n \geqslant 2, u_n=\left(\frac{\ln (n+1)}{\ln n}\right)^n$.

    (a) Déterminer la limite de $\left(u_n\right)$.

    (b) Déterminer la nature de la série de terme général $\frac{u_n-1}{n}$.
    
!!! exercice "1759"
    Existence et calcul de $\sum_{n=0}^{\infty} \frac{1}{(3 n) !}$.
    
!!! exercice "2426"
    On pose $f_n(x)=\frac{x}{n\left(1+n^2 x^2\right)}$ pour $n \in \mathbb{N}^*$.
    
    (a) Étudier la convergence simple de $\sum_{n \geqslant 1} f_n$
    
    (b) La somme $S$ est-elle continue?
    
    (c) Déterminer un équivalent de $S(x)$ quand $x$ tend vers $0^{+}$.
    
!!! exercice "2534"
    Soit $f \in \mathscr{C}^1\left(\mathbb{R}_{+}, \mathbb{R}\right)$ telle que $f(0)=0, f^{\prime}(0)=1$ et $\forall x \in \mathbb{R}_{+}, f(x) \geqslant x$. Soit $I_n=\int_0^{+\infty} \frac{n}{1+n^2 f^2(x)} \mathrm{d} x$ pour tout $n \in \mathbb{N}^*$ Déterminer la limite de $\left(I_n\right)$.
    

!!! exercice "2609"
    Soit $f: x \mapsto \sum_{n=1}^{+\infty} \frac{(-1)^n}{x^2+n^2}$.

    (a) Montrer que $f$ est définie et continue sur $\mathbb{R}$.

    (b) Montrer que $f$ est intégrable sur $\mathbb{R}_{+}$et calculer $\int_0^{+\infty} f(x) \mathrm{d} x$.
    
!!! exercice "2638"
    Domaine de définition et calcul de $F: s \mapsto \int_0^{+\infty} e^{-s t} \sum_{n=0}^{+\infty} \frac{t^n}{(n !)^2} \mathrm{~d} t$.
    
!!! exercice "2687"
    Soit $A \in \mathscr{M}_n(\mathbb{C})$. Déterminer le rayon de convergence de la série entière $\sum \operatorname{tr}\left(A^k\right) z^k$.
    
!!! exercice "2723"
    Rayon de convergence et somme de $f: x \mapsto \sum_{n \geqslant 1}^{+\infty} \frac{3 n}{n+2} x^n$.
    
!!! exercice "2746"
    Soit, pour $n \in \mathbb{N}, W_n=\int_0^{\pi / 2} \cos ^n t \mathrm{~d} t$.
    
    (a) Montrer, pour $n \geqslant 2$, que $n W_n=(n-1) W_{n-2}$.
    
    (b) Montrer, pour $n \in \mathbb{N}$, que $\frac{1}{n+1} \leqslant W_n \leqslant \frac{\pi}{2}$.
    
    (c) Déterminer le rayon de convergence de $\sum_{n \geqslant 0} W_n x^n$.
    
    (d) Calculer la somme de cette série entière en utilisant (a) puis en utilisant $\int_0^{\pi / 2} \frac{\mathrm{d} t}{1+x \cos t}$.
    
    
!!! exercice "638"
    Si $v=\left(a_1, \ldots, a_n\right) \in \mathbb{C}^n$, on pose $M(v)=\left(\begin{array}{cccc}a_1 & a_2 & \cdots & a_n \\ a_n & a_1 & \ddots & \vdots \\ \vdots & \ddots & \ddots & a_2 \\ a_2 & \cdots & a_n & a_1\end{array}\right)$. Soit $J=M(0,1,0, \ldots, 0)$.

    (a) Exprimer $M(v)$ comme un polynôme en $J$ de degré $\leqslant n-1$.

    (b) Déterminer le spectre de $J$.

    (c) Calculer le déterminant de $M(v)$.
    
!!! exercice "650"
    Pour $n \geqslant 3$, soit $A_n \in \mathcal{M}_n(\mathbb{R})$ telle que $a_{i, i+1}=a_{i+1, i}=a_{1, n}=a_{n, 1}=-1, a_{i, i}=2$, les autres coefficients étant nuls.
    
    (a) Écrire $A_3$, déterminer ses éléments propres.
    
    (b) Montrer que 0 est valeur propre de $A_n$.
    
    (c) Déterminer le spectre de $A_n$. Donner une condition nécessaire et suffisante pour que 4 soit valeur propre de $A_n$.
    
    
!!! exercice "683"
    Pour $X \in \mathscr{M}_n(\mathbb{R})$, on pose $\varphi(X)=X+2 X^{\top}$. Déterminer les éléments propres de $\varphi$ et évaluer sa trace.
    

!!! exercice "875"
    Soient $a \in \mathbb{C}^*$ et $M=\left(m_{i, j}\right)_{1 \leqslant i, j \leqslant n}$ où $\forall(i, j) \in\{1, \ldots, n\}^2, m_{i, j}=a^{i-j}$.

    (a) La matrice $M$ est-elle diagonalisable?

    (b) Déterminer les sous-espaces propres de $A$.
    

!!! exercice "892"
    Soient $\left(a_1, \ldots, a_n\right) \in \mathbb{R}^n$ et $M=\left(m_{i, j}\right)_{1 \leqslant i, j \leqslant n} \in \mathcal{M}_n(\mathbb{R})$ où $m_{i, n}=m_{n, i}=a_i$ pour $i \in\{1, \ldots, n\}$, les autres coefficients étant nuls. Étudier la diagonalisabilité de $M$.
    
    
!!! exercice "929"
    Soit

    $$
    A=\left(\begin{array}{ccc}
    0 & 1 & 2 \\
    1 & 0 & -2 \\
    2 & -2 & 0
    \end{array}\right) \in \mathscr{M}_3(\mathbb{R}) .
    $$

    (a) Montrer que $A$ est diagonalisable et que ses sous-espaces propres sont de dimension 1 .

    (b) Montrer qu'il existe $M \in \mathscr{M}_3(\mathbb{R})$ tel que $M^5+M^3+M=A$.
    

!!! exercice "956"
    Soit $A \in \mathcal{M}_n(\mathbb{R})$ telle que $\operatorname{tr} A \neq 0$ et $\Phi: M \in \mathcal{M}_n(\mathbb{R}) \mapsto(\operatorname{tr} A) M-(\operatorname{tr} M) A$.

    (a) Montrer que $\Phi$ est un endomorphisme de $\mathcal{M}_n(\mathbb{R})$ et déterminer son noyau.
    
    (b) Déterminer les valeurs propres et les sous-espaces propres de $\Phi$. L'application $\Phi$ est-elle diagonalisable?
    
!!! exercice "982"
    Soient $n \in \mathbb{N}^*$ et $u: P \in \mathbb{R}_n[X] \mapsto P(-4) X+P(6) \in \mathbb{R}_n[X]$. Déterminer le noyau, l'image, les valeurs propres et les sous-espaces propres de $u$. L'endomorphisme $u$ est-il diagonalisable?
    
    
    
!!! exercice "1019"
    Soient $\mathscr{D}_n$ l'espace des matrices diagonales de $\mathscr{M}_n(\mathbb{R})$ et $D=\operatorname{diag}\left(d_1, \ldots, d_n\right)$, où les $d_i$ sont des réels distincts.

    (a) Montrer que $\left(I_n, D, \ldots, D^{n-1}\right)$ est une base de $\mathscr{D}_n$.

    (b) Soit $M \in \mathscr{M}_n(\mathbb{R})$ telle que $M D=D M$. Montrer que $M$ est diagonale.
    
    
!!! exercice "1057"
    Soit
    
    $$
    A=\left(\begin{array}{ccc}
    2 & 3 & 1 \\
    0 & -4 & -2 \\
    4 & 12 & 5
    \end{array}\right)
    $$
    
    (a) Diagonaliser $A$.
    
    (b) Si $B \in \mathscr{M}_3(\mathbb{C})$ vérifie $B^2=A$, montrer que $B$ et $A$ commutent. Déterminer l'ensemble $\left\{B \in \mathscr{M}_3(\mathbb{C}), B^2=A\right\}$.


!!! exercice "2273"
    Soit $f: x \mapsto \int_0^{+\infty} \frac{e^{-x t}}{\sqrt{1+t}} \mathrm{~d} t$.
    
    (a) Déterminer le domaine de définition de $f$; étudier la continuité et la dérivabilité de $f$.

    (b) Trouver une équation différentielle dont $f$ est solution.

    (c) Étudier la limite de $f$ en $+\infty$ et en 0 . Donner un équivalent de $f$ en 0 .
    
    
!!! exercice "2281"
    Soit $f: x \mapsto \int_0^{+\infty} \frac{t e^{-t}}{t+x} \mathrm{~d} t$.

    (a) Montrer que $f$ est définie et continue sur $\mathbb{R}_{+}$, dérivable sur $\mathbb{R}_{+}^*$.

    (b) Montrer que $f$ n'est pas dérivable à droite en 0 .

    (c) Calculer $\int_0^{+\infty} t e^{-t} \mathrm{~d} t$.

    (d) Montrer que $\forall x \in \mathbb{R}_{+}^*, 0 \leqslant 1-x f(x) \leqslant \frac{2}{x}$. En déduire un équivalent de $f$ en $+\infty$.    



!!! exercice "2290"
    Soit $f: x \mapsto \int_0^{+\infty} e^{-t x} \frac{\sin t}{t} \mathrm{~d} t$.
    
    (a) Montrer que $f$ est de classe $\mathscr{C}^1$ sur $\mathbb{R}_{+}^*$ et calculer $f^{\prime}(x)$ pour $x \in \mathbb{R}_{+}^*$.
    
    (b) Déterminer la limite de $f$ en $+\infty$.
    
    (c) En déduire une expression de $f$.
    
    
!!! exercice "2300"
    Soit $s$ un nombre complexe de partie réelle $>0$.

    (a) Montrer que $\Gamma(s)=\int_0^{+\infty} e^{-t} t^{s-1} \mathrm{~d} t$ converge.

    (b) Montrer que $I_n(s)=\int_0^n t^{s-1}\left(1-\frac{t}{n}\right)^n \mathrm{~d} t$ converge. Quelle est la limite de $I_n(s)$ lorsque $n$ tend vers $+\infty$ ?

    (c) Montrer que $\lim _{n \rightarrow+\infty} \frac{n ! n^s}{s(s+1) \cdots(s+n)}=\Gamma(s)$.
    
    
!!! exercice "2305"
    Pour $x \in \mathbb{R}$, on pose $F(x)=\int_0^1 \frac{\mathrm{d} t}{1+t^x}$.

    (a) Montrer que $F$ est définie sur $\mathbb{R}$ et que $\forall x \in \mathbb{R}, F(x)+F(-x)=1$. Calculer $F(k)$ pour $k \in\{-2,-1,0,1,2\}$.

    (b) Déterminer les limites de $F$ en $-\infty$ et $+\infty$. Donner un équivalent de $F(x)-1$ quand $x$ tend vers $+\infty$.

    (c) Montrer que $F$ est convexe sur $\mathbb{R}_{-}$et concave sur $\mathbb{R}_{+}$.
    
    
!!! exercice "2321"
    Soient $f: x \mapsto \int_0^x e^{-t^2} \mathrm{~d} t$ et $g: x \mapsto \int_0^{\pi / 4} e^{-x^2 / \cos ^2 u} \mathrm{~d} u$.

    (a) Montrer que $f^2+g$ est constante.

    (b) En déduire la valeur de l'intégrale de Gauss.


!!! exercice "2329"
    Soit $F: x \mapsto \int_0^{+\infty} \frac{\arctan (t x)}{t\left(1+t^2\right)} \mathrm{d} t$.
    
    (a) Déterminer le domaine de définition $D$ de $F$.

    (b) Montrer que $F$ est $\mathscr{C}^1$ sur $\mathbb{R}$.

    (c) Exprimer $F$ sur $D$.

    (d) En déduire la valeur de $\int_0^{+\infty}\left(\frac{\arctan t}{t}\right)^2 \mathrm{~d} t$.

    
!!! exercice "2353"
    Soit $f: x \mapsto \int_0^{\pi / 2} \arctan (x \tan \theta) \mathrm{d} \theta$.

    (a) Montrer que, pour $x \in \mathbb{R}_{+}^*, \arctan (x)+\arctan \left(\frac{1}{x}\right)=\frac{\pi}{2}$.

    (b) Montrer que $f$ est définie sur $\mathbb{R}$ et de classe $\mathscr{C}^1$ sur $\mathbb{R}^*$. Déterminer son sens de variation.

    (c) Montrer que, pour $x \in \mathbb{R}_{+}^*, f(x)+f\left(\frac{1}{x}\right)=\frac{\pi^2}{4}$. En déduire la limite de $f$ en $+\infty$.
 
!!! exercice "2372"
    Soit $f: x \mapsto \int_0^\pi \ln \left(1-2 x \cos t+x^2\right) \mathrm{d} t$.

    (a) Montrer que $f$ est définie sur $\mathbb{R}$ et que $f$ est paire.

    (b) Vérifier que $\forall x \in \mathbb{R}, f\left(x^2\right)=2 f(x)$.

    (c) Calculer $f(x)$ en distinguant les trois cas : $|x|=1,|x|<1$ et $|x|>1$.
    

 
!!! exercice "2380"
    Soit $f(x)=\int_0^{+\infty} \frac{e^{-x^2\left(t^2-i\right)}}{t^2-i} \mathrm{~d} t$.

    (a) Trouver le domaine de définition de $f$.

    (b) Montrer que $f$ est de classe $\mathscr{C}^1$.

    (c) Montrer que $\int_0^X e^{i x^2} \mathrm{~d} x$ a une limite finie quand $X \rightarrow+\infty$ et la calculer.


!!! exercice "1105"
    Soient $(E,\langle,\rangle)$ un espace euclidien, $\left(x_1, \ldots, x_n\right)$ des vecteurs de $E$. Montrer que le rang de la matrice $\left(\left\langle x_i, x_j\right\rangle\right)_{1 \leqslant i, j \leqslant n}$ est égal au rang de la famille $\left(x_1, \ldots, x_n\right)$.
    
    
    
!!! exercice "1124"
    Pour $P, Q \in \mathbb{R}_n[X]$, on pose $\phi(P, Q)=\sum_{k=0}^n P(k) Q(k)$.

    (a) Montrer que $\phi$ est un produit scalaire.

    (b) Trouver une base orthonormale de $\mathbb{R}_3[X]$ pour ce produit scalaire.
    
    
!!! exercice "1145"
    Soit $E=\mathscr{C}^1([0,1])$. Pour $f, g \in E$, on pose $\langle f, g\rangle=\int_0^1\left(f g+f^{\prime} g^{\prime}\right)$. On considère les sous-espaces $V=\left\{f \in \mathscr{C}^2([0,1]), f^{\prime \prime}=\right.$ $f\}$ et $W=\{f \in E, f(0)=f(1)=0\}$.

    (a) Montrer que la famille (ch, sh) est une base de $V$.

    (b) Soient $f \in V$ et $g \in E$. Montrer que $\langle f, g\rangle=f^{\prime}(1) g(1)-f^{\prime}(0) g(0)$.

    (c) Soient $f \in V$ et $g \in W$. Montrer que $\langle f, g\rangle=0$.

    (d) Soit $f \in E$ tel que $f(0)=0, f(1)=\operatorname{ch} 1$. Calculer $\langle f, \operatorname{ch}\rangle,\langle f, \operatorname{sh}\rangle,\|\operatorname{ch}\|^2$ et $\|\operatorname{sh}\|^2$. En déduire le projeté orthogonal de $f$ sur $V$.
    
!!! exercice "1171"
    Soient $E=\mathbb{R}_n[X]$ et $a_0, a_1, \ldots, a_n$ des réels distincts. On pose, pour $(P, Q) \in E^2:\langle P, Q\rangle=\sum_{k=0}^n P\left(a_k\right) Q\left(a_k\right)$.

    (a) Montrer que $\langle,\rangle$ est un produit scalaire sur $E$.

    (b) Déterminer une base orthonormée de $E$.

    (c) Déterminer la distance de $Q \in E$ au sous-espace $H=\left\{P \in E, \sum_{k=0}^n P\left(a_k\right)=0\right\}$.
    

!!! exercice "1200"
    Calculer $m=\inf _{(a, b, c) \in \mathbb{R}^3} \int_0^1\left(x^3-a x^2-b x-c\right)^2 \mathrm{~d} x$.
    


!!! exercice "1242"
    Soient $(E,\langle,\rangle)$ un espace euclidien et $g$ un automorphisme orthogonal de $E$. On pose $f=g-\operatorname{id}_E$. Soit $y \in \operatorname{Im}(f)$. Montrer que $y$ est dans l'orthogonal de $\operatorname{Ker}(f)$. En déduire que $(\operatorname{Im}(f))^{\perp}=\operatorname{Ker}(f)$.
    

!!! exercice "1261"
    (a) Soit $A \in \mathscr{S}_n(\mathbb{R})$. Montrer qu'il existe une unique $B \in \mathscr{S}_n(\mathbb{R})$ telle que $B^3=A$.
    
    (b) Déterminer $B$ pour $A=\left(\begin{array}{lll}2 & 3 & 3 \\ 3 & 2 & 3 \\ 3 & 3 & 2\end{array}\right)$.
    
    
!!! exercice "1295"
    Soit $(E,\langle,\rangle)$ un espace euclidien de dimension n$. Soient $f \in \mathcal{L}(E)$ un endomorphisme auto-adjoint, $\lambda_1, \leqslant \cdots \leqslant \lambda_n$ ses valeurs propres. Soit $x \in E$ unitaire.

    (a) Montrer que $\lambda_1 \leqslant\langle f(x), x\rangle \leqslant \lambda_n$.

    (b) Montrer que $\langle f(x), x\rangle=\lambda_1 \Longleftrightarrow f(x)=\lambda_1 x$.
    
    
!!! exercice "1301"
    Soit $A \in \mathscr{S}_n(\mathbb{R})$ telle que $A^2=A$.

    (a) Montrer que $A$ définit un projecteur orthogonal.

    (b) Si $M \in \mathscr{M}_n(\mathbb{R})$, exprimer $\operatorname{tr}\left(M^{\top} M\right)$ en fonction des coefficients de $M$.

    (c) Montrer que $\sum_{1 \leqslant i, j \leqslant n}\left|a_{i, j}\right| \leqslant n \sqrt{\operatorname{rg}(A)}$.
    
    
    


!!! exercice "1407"
    Caractériser l'endomorphisme de $\mathbb{R}^3$ de matrice
    
    $$
    \frac{1}{7}\left(\begin{array}{ccc}
    -6 & 3 & 2 \\
    3 & 2 & 6 \\
    2 & 6 & -3
    \end{array}\right)
    $$
    

!!! exercice "1419"
    Soit $(a, b) \in\left(\mathbb{R}^3\right)^2$. On note $f_a: x \in \mathbb{R}^3 \mapsto a \wedge x \in \mathbb{R}^3$.

    (a) Trouver l'adjoint de $f_a$ et de $f_a \circ f_b$.

    (b) Donner une condition nécessaire et suffisante pour que $f_a \circ f_b$ soit autoadjoint.

    (c) Calculer $f_a \circ f_b(a)$ et $f_a \circ f_b(a \wedge b)$.

    (d) L'endomorphisme $f_a \circ f_b$ est-il diagonalisable?    
    



!!! exercice "1463"
    Si $P=\sum a_k X^k \in \mathbb{R}[X]$, on pose $N_1(P)=\sum\left|a_k\right|, N_2(P)=\max _{k \geqslant 0}\left|a_k\right|, N_3(P)=\max _{[0,1]}|P|$.

    (a) Montrer que $N_1, N_2, N_3$ sont des normes.

    (b) Soit $\Phi: P \in \mathbb{R}[X] \mapsto P(0)$. L'application $\Phi$ est-elle continue pour ces normes?

    (c) Les normes $N_1, N_2$ et $N_3$ sont-elles équivalentes?
    
!!! exercice "1468"
    Soit $E=\mathcal{C}^1([0,1], \mathbb{R})$. Si $f \in E$, on pose $\|f\|_2=\sqrt{\int_0^1 f^2}$ et $\|f\|_{\infty}=\sup _{t \in[0,1]}|f(t)|$.

    (a) Montrer que \|\|$_2$ est une norme sur $E$. Soit $\alpha \in[0,1]$ et $\Phi: f \in E \mapsto f(\alpha) \in \mathbb{R}$. L'application $\Phi$ est-elle continue pour la norme \|\|$_2$ ?

    (b) Existe-t-il $C>0$ tel que $\forall f \in E,\|f\|_{\infty} \leqslant C\|f\|_2$ ?

    (c) Soit $n \in \mathbb{N}$. Existe-t-il $C>0$ tel que $\forall P \in \mathbb{R}_n[X],\|P\|_{\infty} \leqslant C\|P\|_2$ ?
    
!!! exercice "1500"
    On munit $E=\mathcal{M}_p(\mathbb{C})$ de la norme $\|\quad\|$ définie par: $\forall M=\left(m_{i, j}\right)_{1 \leqslant i, j \leqslant n} \in E,\|M\|=\max _{1 \leqslant i, j \leqslant n}\left|m_{i, j}\right|$.

    (a) Soient $X \in \mathcal{M}_{n, 1}(\mathbb{C})$ et $P \in \mathrm{GL}_n(\mathbb{C})$. Montrer que les applications $M \mapsto M X$ et $M \mapsto P^{-1} M P$ sont continues.

    (b) Montrer que l'application $(M, N) \mapsto M N$ est continue.

    (c) Soit $A \in E$. On suppose que la suite $\left(\left\|A^n\right\|\right)_{n \geqslant 1}$ est bornée. Montrer que les valeurs propres de $A$ sont de module $\leqslant 1$.

    (d) Soit $B \in E$. On suppose que $\left(B^n\right)_{n \geqslant 0}$ converge vers $C \in E$. Montrer que $C^2=C$ et que le spectre de $C$ est inclus dans $\{0,1\}$. Montrer que les valeurs propres de $B$ sont de module $\leqslant 1$; si $\lambda$ est une valeur propre de $B$ de module 1 , montrer que $\lambda=1$.
    

!!! exercice "3262"
    Une urne contient $n$ boules blanches et $n$ boules noires. On tire les boules de l'urne deux par deux. Quelle est la probabilité d'avoir à chaque tirage une boule blanche et une boule noire?
    

!!! exercice "3272"
    On place aléatoirement $n \geqslant 3$ boules dans $n$ urnes. Calculer la probabilité qu'une seule urne soit vide.
    
    
!!! exercice "3312"
    On lance un dé à 6 faces. Les lancers sont indépendants et le dé n'est pas pipé. On note $X_k$ la variable aléatoire égale à la valeur obtenue au $k$-ième lancer.

    (a) Déterminer la loi de $X_k$ et la fonction de répartition $F$ associée à $X_k$.

    (b) On note $Z_n$ la valeur maximale obtenue au bout de $n$ lancers. Déterminer la fonction de répartition $F_n$ de $Z_n$ en fonction de $F$.

    (c) Déterminer la limite de $\left(F_n\right)$ lorsque $n$ tend vers l'infini. La convergence est elle uniforme?

    (d) On note $Y_n$ la valeur minimale obtenue au bout de $n$ lancers. Déterminer sa fonction de répartition.
    
    
!!! exercice "3342"
    (a) Soient $p, q \in \mathbb{N}$. Montrer que $\sum_{k=p}^q\left(\begin{array}{l}k \\ p\end{array}\right)=\left(\begin{array}{l}q+1 \\ p+1\end{array}\right)$.
    
    (b) Une urne contient $a$ boules blanches et $b$ boules noires. On retire une à une et sans remise les boules de l'urne. Soit $X$ la variable aléatoire indiquant le nombre de tirages effectués jusqu'au retrait de toutes les boules blanches. Déterminer la loi de $X$. Calculer $E(X)$ et $V(X)$.
    
    
!!! exercice "3346"
    On donne: $\forall p \leqslant n, \sum_{k=p}^n\left(\begin{array}{l}k \\ p\end{array}\right)=\left(\begin{array}{l}n+1 \\ p+1\end{array}\right)$. On dispose d'une urne contenant $n$ boules numérotées de 1 à $n$. On tire simultanément deux boules au hasard. On note $X$ (resp. $Y$ ) la variable aléatoire correspondant au numéro le plus petit (resp. le plus grand) des deux boules.

    (a) Déterminer la loi de $(X, Y)$. En déduire les lois de $X$ et de $Y$.

    (b) Calculer $E(Y), E(Y(Y-2))$ et $V(Y)$.

    (c) Montrer que $n+1-X$ suit la même loi que $Y$. Calculer $E(X)$ et $V(X)$.

    (d) Calculer $E(X(Y-2))$ et $\operatorname{Cov}(X, Y)$.
    
    
!!! exercice "3285"
    Soit $\left(A_n\right)_{n \geqslant 1}$ une suite d'événements incompatibles. Montrer que $P\left(A_n\right) \rightarrow 0$ lorsque $n \rightarrow+\infty$.
    
    
!!! exercice "3287"
    Soient $(\Omega, \mathscr{T}, P)$ un espace probabilisé, $A$ et $B$ deux événements. Montrer que $|P(A \cap B)-P(A) P(B)| \leqslant \frac{1}{4}$.
    
!!! exercice "3366"
    (a) Soit $X$ une variable aléatoire suivant une loi géométrique de paramètre $p$. Calculer l'espérance et la variance de $X$. Montrer que $X$ est une loi sans mémoire c'est-à-dire que, pour tous $m, n \in \mathbb{N}, P(X>m+n \mid X>m)=P(X>n)$.

    (b) Soit $X$ une variable aléatoire à valeurs dans $\mathbb{N}^*$. On suppose que, pour tous $m, n \in \mathbb{N}, P(X>m+n \mid X>m)=P(X>n)$. Montrer que $X$ suit une loi géométrique.

    (c) Soit $X$ une variable aléatoire à valeurs dans $\mathbb{N}^*$ et non (presque sûrement) bornée. On note $\lambda_n=P(X=n \mid X \geqslant n)$ pour $n \in \mathbb{N}^*$. Montrer, pour $n \geqslant 2, P(X \geqslant n)=\prod_{k=1}^{n-1}\left(1-\lambda_k\right)$. Quelle est la nature de la série de terme général $\lambda_n$ ?
    
    
!!! exercice "3371"
    On effectue des tirages avec remise dans une urne contenant $n$ boules numérotées de 1 à $n$. On note $X_n$ le rang du premier tirage où l'on obtient une boule différente de la première boule tirée.

    (a) Justifier que $X_n$ est bien une variable aléatoire discrète et donner sa loi.

    (b) Justifier l'existence de l'espérance de $X_n$ et la calculer.

    (c) On note $Y_n$ le rang du premier tirage à l'issue duquel toutes les boules ont été tirées au moins une fois. Donner la loi de $Y_2$ puis celle de $Y_3$.
    
!!! exercice "3378"
    Soient $X$ et $Y$ deux variables aléatoires indépendantes suivant une loi géométrique $\mathcal{G}(p)$. On pose $U=|X-Y|$ et $V=$ $\min \{X, Y\}$.

    (a) Déterminer la loi de $(U, V)$.

    (b) En déduire les lois de $U$ et de $V$.
    
    (c) Les variables aléatoires $U$ et $V$ sont-elles indépendantes?
    
    
!!! exercice "3386"
    Soient $X$ une variable aléatoire suivant une loi géométrique de paramètre $p \in] 0,1[$ et $Y$ une variable aléatoire suivant une loi de Poisson de paramètre $\lambda>0$. On suppose $X$ et $Y$ indépendantes. Calculer $P(X=Y)$ et $P(X<Y)$.
    

!!! exercice "3395"
    Soient $X \hookrightarrow \mathscr{P}(\lambda)$ et $Y \hookrightarrow \mathscr{P}(\mu)$, indépendantes. On pose $Z=X+Y$.

    (a) Déterminer la loi de $Z$.

    (b) Pour $k \in \mathbb{N}$ et $n \in \mathbb{N}^*$, calculer $P(X=k \mid Z=n)$.

    (c) En déduire la loi de $X$ sachant $(Z=n)$.
    
    
!!! exercice "3402"
    Une entreprise de dépannage à domicile intervient en 10 minutes lorsqu'un client appelle, mais avec une probabilité de retard $p=0,25$.

    (a) Un client appelle 4 fois. Soit $X$ la variable aléatoire égale au nombre de retards. Déterminer la loi, l'espérance et la variance de $X$.

    (b) Soit $Y$ la variable aléatoire égale au nombre d'appels dans la journée, $Y$ suit une loi de Poisson de paramètre $\lambda$. Soit $Z$ la variable associée au nombre de retards. Quelle est la loi de $Z$ ?

    (c) Soit $U$ la variable aléatoire associée au rang du premier appel qui mène à un retard. Déterminer la loi, l'espérance et la variance de $U$.
    

!!! exercice "3421"
    On lance une pièce de monnaie équilibrée jusqu'à ce qu'on obtienne la séquence pile-face. Soit $X$ la variable aléatoire comptant le nombre de lancés effectués. Calculer $E(X)$.
    

!!! exercice "3434"
    Soient $X$ et $Y$ deux variables aléatoires indépendantes suivant la même loi géométrique. Trouver la probabilité pour que

    $$
    \left(\begin{array}{cc}
    X & X \\
    -Y & -Y
    \end{array}\right)
    $$

    soit nilpotente.
    
!!! exercice "3440"
    Soient $(\Omega, \mathscr{T}, P)$ un espace probabilisé, $A$ et $B$ deux variables aléatoires indépendantes qui suivent une loi géométrique. Déterminer la probabilité que toutes les solutions de l'équation $\left(E_\omega\right): y^{\prime \prime}+(A(\omega)-1) y^{\prime}+B(\omega) y=0$ tendent vers 0 en $+\infty$.
    

!!! exercice "3445"
    On considère quatre spots lumineux $S_1, \ldots, S_4$. À $t=0, S_1$ est allumé. Si à un instant $n$ le spot $S_1$ est allumé alors à l'instant $n+1$ on allume un spot au hasard parmi les quatre, et on éteint $S_1$ si le spot choisi n'est pas $S_1$. Si un autre spot $S_k$ avec $k \in\{2,3,4\}$ est allumé alors on l'éteint et on allume $S_{k-1}$ à la place.

    (a) Calculer la probabilité que seul $S_1$ soit allumé jusqu'à l'instant $n$ inclus.

    (b) Trouver la loi de la variable aléatoire $T$ indiquant l'instant auquel $S_2$ s'allume pour la première fois.

    (c) Trouver l'espérance de $T$.


!!! exercice "1437"
    Pour $u=(x, y)$ on pose $N(u)=\sup \{|x+t y|, t \in[0,1]\}$.
    
    (a) Montrer que $N(u)=\max \{|x|,|x+y|\}$, puis que $N$ est une norme.
    
    (b) Soit $B$ la boule unité de $N$. Trouver le plus petit disque euclidien contenant $B$ et le plus grand disque euclidien contenu dans $B$.

!!! exercice "1440"
    Soit $A$ une partie non vide d'un espace vectoriel normé $E$ de dimension finie. Pour $x \in E$, on pose $d(x, A)=\inf _{a \in A}\|x-a\|$. Soient $R>0$ et $A(R)=\{x \in E, d(x, A) \leqslant R\}$. Montrer que si $A$ est convexe alors $A(R)$ est convexe et fermé.

!!! exercice "1456"
    Soient $E=\mathcal{C}^1([a, b], \mathbb{C})$ et $N: f \in E \mapsto|f(a)|+\int_a^b\left|f^{\prime}\right|$. Montrer que $N$ est une norme. Comparer $N$ et $\|\cdot\|_{\infty}$.

!!! exercice "1459"
    Soit $E=\mathscr{C}^0([0,1], \mathbb{R})$. Si $f \in E$, on pose $N(f)=\int_0^1|f(t)| e^t \mathrm{~d} t$. Montrer que $N$ est une norme sur $E$. Est-elle équivalente à \|\|$_{\infty}$ ?

!!! exercice "1466"
    Soit $E$ l'espace des suites bornées à valeurs complexes. Montrer que $N(u)=\sum_{n=0}^{+\infty} \frac{\left|u_n\right|}{2^n}$ et $N^{\prime}(u)=\sum_{n=0}^{+\infty} \frac{\left|u_n\right|}{n !}$ sont deux normes sur $E$. Sont-elles équivalentes?

!!! exercice "1471"
    Soit $E$ l'ensemble des $\left(x_n\right)_{n \geqslant 0} \in \mathbb{R}^{\mathbb{N}}$ telles que la série de terme général $x_n^2$ converge.
    
    (a) Montrer que $E$ est un sous-espace vectoriel de $\mathbb{R}^{\mathbb{N}}$.
    
    (b) Montrer que l'application qui à $\left(x_n\right) \in E$ associe $\left\|\left(x_n\right)\right\|=\left(\sum_{n=0}^{+\infty} x_n^2\right)^{1 / 2}$ définit une norme.
    
    (c) Soit $F:\left(x_n\right) \in E \mapsto x_0 \in \mathbb{R}$. L'application $F$ est-elle continue?
    
    (d) Si $\left(x_n\right) \in E$, montrer que la suite de terme général $x_n+x_{n+1}$ est dans $E$. L'application $G:\left(x_n\right)_{n \geqslant 0} \in E \mapsto\left(x_n+\right.$ $\left.x_{n+1}\right)_{n \geqslant 0} \in E$ est-elle continue?

!!! exercice "1474"
    Soit $\ell^{\infty}$ le sous-espace de $\mathbb{C}^{\mathbb{N}}$ formé des suites bornées. On munit $\ell^{\infty}$ de la norme infinie \|\|$_{\infty}$. Si $u=\left(u_n\right) \in \ell^{\infty}$, on note $\Delta(u)$ la suite de terme général $u_{n+1}-u_n$. Montrer que $\Delta$ est un endomorphisme continu de $\ell^{\infty}$. Calculer $\sup\{||\Delta(u)||\big|u\in\mathcal L(E), ||u||=1\}$.

!!! exercice "1488"
    Soit $T$ une matrice triangulaire supérieure de taille $n$ et de diagonale $\lambda_1, \ldots, \lambda_n$. On pose, pour $p \in \mathbb{N}^*$,
    
    $$
    T_p=T+\operatorname{diag}\left(\frac{1}{p}, \frac{2}{p}, \cdots, \frac{n}{p}\right) .
    $$

    (a) Montrer qu'à partir d'un certain rang, $T_p$ a $n$ valeurs propres distinctes.
    
    (b) En déduire que toute matrice de $\mathscr{M}_n(\mathbb{C})$ est limite d'une suite de matrices diagonalisables.

!!! exercice "1510"
    (a) Soit $P \in \mathrm{GL}_n(\mathbb{C})$. L'application $\varphi_P: M \in \mathcal{M}_n(\mathbb{C}) \mapsto P^{-1} M P \in \mathcal{M}_n(\mathbb{C})$ est-elle continue?
    
    (b) Soit $A \in \mathcal{M}_n(\mathbb{Z})$ telle que $4 A^3+2 A^2+A=0$. Montrer que la suite $\left(A^k\right)$ converge. En déduire que $A=0$.

!!! exercice "2948"
    Résoudre le système différentiel $\left\{\begin{array}{l}x^{\prime}=2 y+z \\ y^{\prime}=3 x+4 z \\ z^{\prime}=3 y+5 x .\end{array}\right.$

!!! exercice "2954"
    On considère l'équation $(E): x^{\prime \prime \prime}-5 x^{\prime \prime}+7 x^{\prime}-3 x=0$.
    
    (a) Montrer que $x$ est solution de $(E)$ si et seulement si $X={ }^t\left(x, x^{\prime}, x^{\prime \prime}\right)$ est solution d'une équation matricielle $X^{\prime}=A X$ avec $A$ une matrice à déterminer.
    
    (b) Trouver une matrice $P$ inversible telle que $P^{-1} A P$ soit triangulaire. Résoudre $(E)$.

!!! exercice "2962"
    Intégrer l'équation différentielle $x^2 f^{\prime}(x)+f(x)=1$. Trouver une solution sur $\mathbb{R}$.

!!! exercice "3000"
    Déterminer les solutions développables en série entière de l'équation différentielle $4 x y^{\prime \prime}-2 y^{\prime}+9 x^2 y=0$.


!!! exercice "RMS2022-1099"
    - a)  Ecrire une fonction Python qui prend un entier $n$ et renvoie une matrice de $\mathcal{S}_n(\mathbb R)$ à coefficients aléatoires dans $[0,1[$. Calculer les valeurs propres et des vecteurs propres de quelques-unes de ses matrices. Que peut-on conjecturer sur la valeur propre maximale? Sur un vecteur propre associé?

    On munit $\mathbb R^n$ de sa structure euclidienne canonique.
    Soient  $M\in\mathcal{S}_n(\mathbb R^+)$, $\lambda_1 \ldots, \lambda_n$ ses valeurs propres et $\alpha$ leur maximum. Soit $\Phi:X\mapsto\langle X,MX\rangle$.

    - b)   Justifier l'existence de $\alpha$. Montrer que, pour tout $X\in\mathcal{M}_{n,1}(\mathbb R)$, $ \Phi(X)\leq \alpha \lVert X\rVert^2$ et qu'il y a égalité si et seulement si  $X$ appartient au sous-espace propre de $M$ associé à $\alpha$.

    - c)   Soit $C=\left\{X\in\mathcal{M}_{n,1}(\mathbb R)\;;\;\lVert X\rVert=1\text{ et }\forall i\in[\![1,n]\!],\,X_i\geq0\right\}$. Montrer que $\Phi$ est bornée sur $C$ et admet un maximum $\mu\leq \alpha$.

    - d) Soient $X$ un vecteur propre unitaire de $M$  associé à $\alpha$ et $W\in\mathcal{M}_{n,1}(\mathbb R)$ dont les composantes sont les valeurs absolues
    des composantes de $X$.  Montrer que $W \in C$ et en déduire que $\mu\geq\lvert \alpha\rvert$.

    - e)   Conclure que $\alpha\geq0$ puis que $M$ admet un vecteur propre positif et unitaire associé à $\alpha$.

    - f)   Montrer que, pour tout $i\in[\![  1,n]\!]$, $\lvert \lambda_i\rvert\leq \alpha$.


!!! exercice "RMS2022-1103"
    Soit $(u_n)$ définie par $u_0\in[0,\pi]$ et, pour  $n\in\mathbb N$, $u_{n+1}=\sum_{k=0}^n\sin\left(\frac{u_k}{n+1}\right)$.

    a)   \'Ecrire un programme Python qui prend un entier $n$ et qui renvoie les $n$ premières valeurs de cette suite. Discuter sa complexité en temps et en mémoire.

    b)
                Montrer que, pour  $x\in[0,\pi]$, $x-\frac{x^3}{6}\leq \sin x\leq x$.

    c)  On considère $v_n=\frac1{n+1}\sum_{k=0}^nu_k$ pour tout $n\in\mathbb N$.

    i)   Montrer que $u_n\in[0,\pi]$ pour tout $n\in\mathbb N$.

    ii) Montrer que $v_n-\frac{\pi^3}{6(n+1)^2}\leq u_{n+1}\leq v_n$ pour tout $n\in\mathbb N$.

    En déduire que $-\frac{\pi^3}{6(n+1)^3}\leq v_{n+1}-v_n\leq 0$ pour tout $n\in\mathbb N$.

    iii)   En déduire la convergence de $(u_n)$.


!!! exercice "RMS2022-1105"
    Soient $a>0$ et $b\geq 0$. Soit $(u_n)_{n\geq 0}$ la suite réelle définie par $u_0>0$, $u_1>0$ et, pour $n\in\N$, $u_{n+2}=\frac{u_{n+1}+a}{u_n+b}$.

    a) Avec Python, tracer les premiers termes de la suite $(u_n)$ pour différentes valeurs de $u_0$, $u_1$, $a$ et $b$.
   
    b) On suppose dorénavant $b>0$. Exprimer $u_{n+3}$ en fonction de $a,\, b,\, u_{n+1}$ et $u_n$. Montrer que la suite $(u_n)_{n\geq 3}$ est bornée.
   
    c) On pose, pour  $n\in\N$, $\alpha_n=\sup\{u_k,\ k\geq n\}$ et $\beta_n=\inf\{u_k,\ k\geq n\}$. Justifier que les suites $(\alpha_n)$ et $(\beta_n)$ sont bien définies puis qu'elles sont convergentes. On note $\alpha_{\infty}$ et $\beta_{\infty}$ leurs limites.
   
    d) Montrer que $\alpha_{\infty}\leq\frac{\alpha_{\infty}+a}{\beta_{\infty}+b}$ et $\beta_{\infty}\geq\frac{\beta_{\infty}+a}{\alpha_{\infty}+b}$ puis que $\alpha_{\infty}=\beta_{\infty}.$
   
    e) Conclure.


!!! exercice "RMS2022-1109"
    Soit $f: x \mapsto \int_x^{x^2} \frac{\mathrm{d} t}{\ln (t)}$.
    
    a) Montrer que $f$ est définie sur $\mathbb{R}^{+*} \backslash\{1\}$.
    
    b) Avec Python, tracer la courbe représentative de $f$ sur $[0,3]$. Si $f$ admet une limite finie en 1 , la comparer avec $\ln 2$.
    
    c) Montrer que $x \ln 2 \leqslant f(x) \leqslant x^2 \ln 2$ pour $\left.\left.x \in\right] 1,2\right]$. On pourra écrire $\frac{1}{\ln (t)}=t \frac{1}{t \ln (t)}$.
    
    d) En déduire que $f$ admet une limite à droite en 1 . Que dire pour la limite à gauche?
    
    e) Soit $\tilde{f}$ le prolongement continue de $f$ sur $\mathbb{R}^{+*}$. Avec le tracé précédemment obtenu, $\tilde{f}$ semble-t-elle dérivable en 1 ?
    
    f) Tracer cette tangente avec Python.

!!! exercice "RMS2022-1110"
    On pose, pour $n \in \mathbb{N}, f_n: x \mapsto \frac{(-1)^n}{n !(n+x)}$. Soit $S=\sum_{n=0}^{+\infty} f_n$.

    a) Montrer que la série $\sum f_n$ converge simplement sur $\mathbb{R}^{+*}$.

    b) Écrire une fonction somme $(n, x)$ qui renvoie la $n$-ième somme partielle de cette série.

    c) Calculer à $10^{-7}$ près la valeur de $S(1)$. Comparer cette valeur à la valeur exacte.

    d) Établir une conjecture concernant la fonction $x \mapsto x S(x)-S(x+1)$.

    e) Vérifier cette conjecture puis montrer que $S(n+1)=o(n !)$.

    f) Étudier la continuité de la fonction $S$.

!!! exercice "RMS2022-1111"
    On pose, pour $n \in \mathbb{N}, f_n: x \mapsto \frac{(-1)^n}{n !(n+x)}$. Soit $S=\sum_{n=0}^{+\infty} f_n$.

    a) Montrer que la série $\sum f_n$ converge simplement sur $\mathbb{R}^{+*}$.

    b) Écrire une fonction somme $(n, x)$ qui renvoie la $n$-ième somme partielle de cette série.

    c) Calculer à $10^{-7}$ près la valeur de $S(1)$. Comparer cette valeur à la valeur exacte.

    d) Établir une conjecture concernant la fonction $x \mapsto x S(x)-S(x+1)$.

    e) Vérifier cette conjecture puis montrer que $S(n+1)=o(n !)$.

    f) Étudier la continuité de la fonction $S$.

!!! exercice "RMS2022-1113"
    Soit $n\in\N^*$. On s'intéresse aux sous-ensembles de segments $[A_iA_j]$ avec $i<j$, où  $A_0,\dots ,A_{n-1}$ sont les points d'affixes les racines $n$-ièmes de l'unité, et où les extrémités sont toutes distinctes. Soit  $M_n$ le cardinal de ces sous-ensembles.
    
    Par exemple, $M_3=4$ et les quatre sous-ensembles sont $\varnothing$, $\{[A_0,A_1]\}$, $\{[A_0,A_2]\}$, et $\{[A_1,A_2]\}$. Par convention, $M_0=1$.

    a)   Calculer $M_4$ et $M_5$. % 9 et 21
   
    b)   Montrer que, pour tout $n\in\N^*$, $M_{n+1}=M_n+\sum_{k=0}^{n-1}M_kM_{n-1-k}.$
   
    c)   Calculer avec Python la liste des $n$ premiers termes de la suite $(M_k)$. Vérifier expérimentalement que  $M_n\leq 3^n$.
   
    d)   Montrer que  $M:z\mapsto \sum_{n=0}^{+\infty}M_nz^n$ a un rayon de convergence $R\geq 1/3$.
   
    e)   Montrer que $M(z)=1+zM(z)+z^2M(z)^2$, et  en déduire une expression de  $M(z)$. Vérifier avec Python.



!!! exercice "RMS2022-1117"
    On munit l'espace $E$ des fonctions $f:\R\rightarrow\R$ continues et bornées de la norme $\|\;\|_{\infty}$. Pour  $f\in E$, on pose $\Phi(f):x\mapsto \int_0^{+\infty}\arctan(xt)\frac{f(t)}{1+t^2}\d    t$.

    a)   Montrer que $\Phi$ est un endomorphisme de $E$.
    
    b)   Soit $g$ l'image par $\Phi$ de la fonction constante égale  à $1$. Avec Python, tracer $g$  sur le segment $[0,5]$ et émettre une conjecture sur la limite de $g$.
    
    c)   Calculer la limite de $g$ en $+\infty$.
    
    d)   Etudier la dérivabilité de $g$ et calculer sa dérivée.
    
    e)   Calculer $g(x)+g(1/x)$. On pourra utiliser Python pour intuiter le résultat.




!!! exercice "RMS2022-1254"
    Soit $p \in] 0,1\left[\right.$. Soit $\left(Z_n\right)_{n \in \mathbb{N}^*}$ une suite i.i.d. de variables aléatoires telle que, pour tout $n \in \mathbb{N}^*, \mathbf{P}\left(Z_n=1\right)=p$ et $\mathbf{P}\left(Z_n=-1\right)=1-p$. Soit $a \in \mathbb{N}$. On définit la suite $\left(X_n\right)_{n \in \mathbb{N}}$ de variables aléatoires par $X_0=a$ et $\forall n \in \mathbb{N}, X_{n+1}=X_n+Z_n$.

    a) Pour $n \in \mathbb{N}$, donner l'espérance et la variance de $Z_n$.

    b) Pour $n \in \mathbb{N}$, montrer que la variable aléatoire $\frac{1}{2}\left(X_n-a+n\right)$ suit une loi binomiale et préciser ses paramètres. Donner son espérance et sa variance.

    d) Écrire une fonction simul $(\mathrm{a}, \mathrm{n}, \mathrm{p})$ renvoyant le vecteur $\left(X_0, X_1, \ldots, X_n\right)$.

    e) Représenter graphiquement $\left(i, X_i\right)_{0 \leqslant i \leqslant n}$ pour $p \in\{0,4 ; 0,5 ; 0,6\}$, $a=5$ et $n=100$.

    f) Écrire une fonction test $(\mathrm{a}, \mathrm{n})$ qui, en testant sur 10000 vecteurs $\left(X_0, \ldots, X_n\right)$, estime la probabilité que $X_n<0$.

    g) Tracer le graphe de $\mathbf{P}\left(X_n<0\right)$ en fonction de $n \in \llbracket 0,200 \rrbracket$ pour $p \in\{0,4 ; 0,5 ; 0,6\}$ et $a=5$.

    h) Montrer que $\mathbf{P}\left(X_n<0\right) \leqslant \mathbf{P}\left(\left|X_n-\mathbf{E}\left(X_n\right)\right| \geqslant \mathbf{E}\left(X_n\right)\right)$.