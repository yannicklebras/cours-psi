# semaine 6

!!! exercice "2327"
    Soit $f: x \mapsto \int_0^{+\infty} \frac{t-\lfloor t\rfloor}{t(t+x)} \mathrm{d} t$. Montrer que $f$ est définie et de classe $\mathscr{C}^1$ sur $\mathbb{R}_{+}^*$. Étudier les variations de $f$. Calculer $f(1)$.


!!! exercice "2330"
    Soit $F: x \mapsto \int_0^{+\infty} \frac{\arctan (x t)}{1+t^2} \mathrm{~d} t$
    
    (a) Déterminer le domaine de définition de $F$.
    
    (b) Étudier la dérivabilité de $F$. Donner une expression de $F^{\prime}$.
    
    (c) Donner une expression simple de $F$.

!!! exercice "2348"
    Soit $f: x \mapsto \int_0^{+\infty} \frac{\mathrm{d} t}{1+t^3+x^3}$
    
    (a) Montrer que $f$ est définie et continue sur $\mathbb{R}_{+}$.
    
    (b) Calculer $f(0)$.
    
    (c) Montrer que $f$ admet une limite en $+\infty$ et la déterminer.


!!! exercice "2356"
    Soit $f: t \mapsto \int_1^{+\infty} \frac{x^t}{\operatorname{ch} x} \mathrm{~d} x$

    (a) Montrer que $f$ est définie et de classe $\mathscr{C}^{\infty}$ sur $\mathbb{R}_{+}$.

    (b) Calculer $f(0)$.
    
    (c) Soit $n \in \mathbb{N}$ avec $n \geqslant 2$. Montrer que l'équation $f(t)=n$ admet une unique solution $t_n$.
    
    (d) Déterminer la limite de $\left(t_n\right)$ puis la limite de $\left(\frac{t_n}{\ln n}\right)$.


!!! exercice "2551"
    Pour $n \in \mathbb{N}$, on pose $I_n=\int_0^{+\infty} \frac{t^n}{t^{n+2}+1} \mathrm{~d} t$. Justifier l'existence de $I_n$ et déterminer la limite de $\left(I_n\right)$.

!!! exercice "2572"
    Soit $f \in \mathscr{C}^0\left(\mathbb{R}_{+}^*, \mathbb{R}\right)$ telle que $t \mapsto e^{-t} f(t)$ soit intégrable sur $\mathbb{R}_{+}^*$.

    (a) Montrer que la suite de terme général $u_n=\sum_{k=1}^n \frac{1}{k}-\ln (n)$ converge. On note $\gamma$ sa limite.

    (b) Montrer que $\int_0^n\left(1-\frac{t}{n}\right)^n f(t) \mathrm{d} t$ tend vers $\int_0^{+\infty} e^{-t} f(t) \mathrm{d} t$ quand $n$ tend vers l'infini.

    (c) En déduire que $\int_0^{+\infty} e^{-t} \ln (t) \mathrm{d} t=-\gamma$.

