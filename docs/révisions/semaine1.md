# semaine 1

!!! exercice "47"
    Calculer $\arctan \left(\frac{1}{2}\right)+\arctan \left(\frac{1}{5}\right)+\arctan \left(\frac{1}{8}\right)$.
    
!!! exercice "48"
    Déterminer les $z \in \mathbb{C}$ tels que les points d'affixe $1, z$ et $1+z^2$ sont alignés.
    
!!! exercice "64"
    Résoudre $\left(z^2+1\right)^n-(z+i)^{2 n}=0$ dans $\mathbb{C}$.


!!! exercice "94"
    Trouver les racines de $X^2-2 X+i$.
    
    
!!! exercice "39"
    Déterminer le nombre de diviseurs de 36000000000 .

!!! exercice "--"
    Résoudre dans $\mathbb N^2$ $\left\{\begin{aligned} x+y & =100 \\ \operatorname{pgcd}(x, y) & =10\end{aligned}\right.$
    
    
!!! exercice "système"
    Résoudre les systèmes linéaires suivants:

    $$
    \left\{\begin{array}{l}
    x+y+2 z=3 \\
    x+2 y+z=1 \\
    2 x+y+z=0
    \end{array}\right.
    $$
    
!!! exercice "système"
    Résoudre les systèmes suivants :

    $$
    \left\{\begin{array}{l}
    x+y+z-3 t=1 \\
    2 x+y-z+t=-1 \\
    3x+2y-2t=0\\
    x-2z+4t=-2
    \end{array}\right.
    $$
    
    
!!! exercice "inverse"
    Calculer l'inverse des matrices carrées suivantes:
    $A=\left(\begin{array}{ccc}1 & 0 & -1 \\ 2 & 1 & -3 \\ -1 & 0 & 2\end{array}\right)$


!!! exercice "1061"
    Soit $\varphi \in \mathscr{L}\left(\mathbb{R}^3\right)$ dont la matrice dans la base canonique est
    
    $$
    \left(\begin{array}{ccc}
    1 & 1 & -1 \\
    -1 & 3 & -3 \\
    -2 & 2 & -2
    \end{array}\right)
    $$

    (a) Montrer que $\mathbb{R}^3=\operatorname{Ker}\left(\varphi^2\right) \oplus \operatorname{Ker}(\varphi-2 \mathrm{id})$.
    (b) Déterminer une base dans laquelle la matrice de $\varphi$ est
    
    $$
    \left(\begin{array}{lll}
    0 & 1 & 0 \\
    0 & 0 & 0 \\
    0 & 0 & 2
    \end{array}\right) .
    $$
    
    (c) Soit $g \in \mathscr{L}\left(\mathbb{R}^3\right)$ tel que $g^2=\varphi$. Montrer que $\operatorname{Ker}\left(\varphi^2\right)$ est stable par $g$. En déduire qu'un tel $g$ n'existe pas.
    
    
!!! exercice "338"
    Pour tout $P \in E=\mathbb{R}_n[X]$, on note $\varphi(P)(X)=P(X+1)$.

    (a) Déterminer la matrice de $\varphi$ dans la base canonique.
    
    (b) Déterminer $\varphi^{-1}$ ainsi que la matrice de $\varphi^{-1}$ dans la base canonique.
    
    
!!! exercice "680"
    Soit $f: A \in \mathscr{M}_n(\mathbb{C}) \mapsto A^{\top} \in \mathscr{M}_n(\mathbb{C})$. Calculer la trace de $f$.
    
!!! exercice "689"
    Soient $A \in \mathscr{M}_n(\mathbb{C})$ et $\Phi: M \in \mathscr{M}_n(\mathbb{C}) \mapsto M A \in \mathscr{M}_n(\mathbb{C})$.

    (a) Déterminer le rang de $\Phi$, la trace de $\Phi$.

    (b) Déterminer les éléments propres de $\Phi$.
    
    
!!! exercice "282"
    Soient $E$ un espace vectoriel et $f \in \mathcal{L}(E)$. Pour $n \in \mathbb{N}$, on pose $K_n=\operatorname{Ker} f^n$ et $I_n=\operatorname{Im} f^n$. Soient $K=\cup_{n \in \mathbb{N}} K_n$ et $I=\cap_{n \in \mathbb{N}} I_n$.

    (a) Montrer que $K$ et $I$ sont des sous-espaces vectoriels de $E$.

    (b) On suppose qu'il existe $q \in \mathbb{N}$ tel que $I_q=I_{q+1}$. Montrer que $\forall n \geqslant q, I_n=I_q$. Montrer un résultat analogue pour les $K_n$.

    (c) On suppose que $E$ est de dimension finie. Montrer que $E=K \oplus I$.
    
    
!!! exercice "302"
    Soient $n \in \mathbb{N}^*$ et $f \in \mathscr{L}\left(\mathbb{R}^n\right)$.
    
    (a) Montrer que $\mathbb{R}^n=\operatorname{Im}(f) \oplus \operatorname{Ker}(f)$ si et seulement si $\operatorname{rg}(f)=\operatorname{rg}\left(f^2\right)$.

    (b) Donner une condition nécessaire et suffisante pour que $\operatorname{Im}(f)=\operatorname{Ker}(f)$.
    
    
!!! exercice "305"
    Soient $E$ un espace vectoriel de dimension finie et $f \in \mathcal{L}(E)$ tel que $f+f^4=0$. Montrer que $\operatorname{Im} f \oplus \operatorname{Ker} f=E$.
    
!!! exercice "311"
    Soient $E$ un espace vectoriel de dimension finie, $u$ et $v$ deux endomorphismes de $E$. Montrer que $|\operatorname{rg}(u)-\operatorname{rg}(v)| \leqslant \operatorname{rg}(u+v) \leqslant$ $\operatorname{rg}(u)+\operatorname{rg}(v)$.
    
    
!!! exercice "850"
    Soient $A=\left(\begin{array}{ll}2 & 1 \\ 1 & 2\end{array}\right)$ et $B=\left(\begin{array}{ll}A & 2 A \\ A & 2 A\end{array}\right)$.

    (a) La matrice $A$ est-elle diagonalisable?

    (b) Montrer que 0 est valeur propre de $B$. Déterminer la dimension de $\operatorname{Ker} B$.

    (c) La matrice $B$ est-elle diagonalisable?
    
!!! exercice "1027"
    Soit

    $$
    A=\left(\begin{array}{lll}
    1 & 2 & 3 \\
    0 & 4 & 5 \\
    0 & 0 & 6
    \end{array}\right)
    $$

    Déterminer la dimension de $\left\{B \in \mathscr{M}_3(\mathbb{R}), A B=B A\right\}$.
    
    
!!! exercice "86"
    Montrer que le polynôme $P_n=\sum_{k=0}^n \frac{X^k}{k !}$ n'a que des racines simples.
    
!!! exercice "97"
    Montrer que $\prod_{k=1}^{n-1} \sin \left(\frac{k \pi}{n}\right)=\frac{n}{2^{n-1}}$.