En mathématiques, certains schémas de preuve sont classiques et vous devez savoir les appliquer. La liste ci-après n'est pas exhaustive. Elle doit vous permettre, à partir d'une phrase mathématique, de préparer un squelette de preuve.

Dans la suite, $A$, $B$ et $C$ sont des ensembles, $P$, $Q$, $P(x)$ et $Q(x)$ désigneront des propriétés mathématiques.

|  Proposition | Squelette de preuve |
| :--- | :--- |
| $\forall x\in E,\ P(x)$ | Soit $x\in E$, (*démonstration de P(x)*) ainsi $P(x)$ est vérifiée |
| $\exists x \in E;\; P(x)$ | Il faut exhiber un élément $x$ de $E$ qui vérifie $P$ |
| $A\subset B$ | Soit $a\in A$, (montrer que $a\in B$), donc $a\in B$ |
| $A=B$ | Montrons que $A\subset B$ (....). Montrons que $B\subset A$ (....). Donc $A=B$ |
| $C\subset A\cap B$ | Soit $c\in C$. Montrons que $c\in A$. Montrons que $c\in B$. Ainsi, $c\in A\cap B$ et donc $C\in\subset A\cap B$. |
| $C\subset A\cup B$ | Soit $c\in C$, supposons que $c\notin A$, montrons que $c\in B$. |
| $P\Rightarrow Q$ | Supposons que $P$ est vérifiée (*démonstration de Q*) Donc $Q$ est vraie. |
|  | Démontrons la contraposée. Supposons que $\neg Q$ est vérifiée (*démonstration de $\neg P$*) donc $\neg P$ est vraie. Ainsi $P\Rightarrow Q$. |
| $P\Leftrightarrow Q$ | Démonstration par double implication. Montrons $P\Rightarrow Q$ (.....). Montrons que $Q\Rightarrow P$ (....). Ainsi $P\Leftrightarrow Q$. |
|  | Démonstration par équivalences. Etablir une suite de propositions $P_0=P, P_1,\dots,P_n=Q$ telles que pour tout $i<n$, $P_i\Leftrightarrow P_{i+1}$. Ces équivalences doivent être élémentaires et justifiées. |
| Soient $f$ et $g$ deux applications $E\rightarrow F$, montrer que $f=g$ | Soit $x\in E$, $f(x)=\dots=g(x)$. |
| Disjonction de cas $(P_1\cup P_2\cup\dots\cup P_n)\Rightarrow Q$ | Supposons $P_1$, alors (....) donc $Q$ est vraie. Supposons $P_2$, alors (....) donc $Q$ est vraie. (....) Dans tous les cas, $Q$ est vraie donc $(P_1\cup P_2\cup\dots P_n)\Rightarrow Q$. |
| Raisonnement par l'absurde. Montrer $P$.  $\danger$ *Utiliser en dernier recours.* | On suppose $\neg P$. (....) on arrive à une contradiction donc $P$ est vraie. |
| Raisonnement par récurrence. $\forall n \in \N,\, P(n)$. | Soit $n\in N$, on pose $H(n)=$\og\dots\fg. Initialisation : Soit $n=0$, montrons $H(0)$. Hérédité : Soit $n\in\N$, supposons $H(n)$ vraie, montrons que $H(n+1)$ est vraie. (....) Ainsi $H(n+1)$ est vraie. On a montré par récurrence que pour tout entier naturel $n$, $H(n)$ est vraie. |
|  | Récurrence à 2 pas. On initialise à $n=0$ et $n=1$ par exemple. Pour l'hérédité : Soit $n\in \N$, supposons $H(n)$ et $H(n+1)$ vraie, montrons que $H(n+2)$ est vraie. (....) Ainsi $H(n+2)$ est vraie. On a montré par récurrence à $2$ pas que pour tout entier naturel $n$,  $H(n)$ est vraie. |
|  | Récurrence forte. On initialise à $n=0$ par exemple. Pour l'hérédité : Soit $n\in \N$, supposons que pour tout entier naturel $k\le n$, $H(k)$ est vraie, montrons que $H(n+1)$ est vraie. (....) On a donc montré par récurrence forte que pour tout entier naturel $n$, $H(n)$ est vraie. |
| Inégalité sur $\R$ : Montrer que pour tout $x\in I$, $f(x)\le g(x)$. | On pose $h(x)=f(x)-g(x)$ et on étudie $h$ pour montrer qu'elle est négative sur $I$. |
| Montrer que $f:E\rightarrow F$ est injective. | Soit $(x,y)\in E^2$ tels que $f(x)=f(y)$ (....) alors $x=y$ : $f$ est injective. |
| Montrer que $f:E\rightarrow F$ est surjective. | Soit $y\in F$. (*construction de $x\in E$ tel que $f(x)=y$*). Ainsi $y$ admet pour antécédent $x$ par $f$ : $f$ est surjective. |
| Montrer que $f$ est bijective. | Montrons que $f$ est injective. (....) Montrons que $f$ est surjective (....) Ainsi $f$ est bijective. |
| Si $f:E\rightarrow F$ est bijective, montrer que $h=f^{-1}$. | Montrons que $h\circ f=id_E$ (....). Montrons que $f\circ h=id_F$ (....). Ainsi, $h=f^{-1}$. |
| Et encore d'autres à venir\dots |  |

