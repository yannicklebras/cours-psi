# semaine 5

!!! exercice "779"
    Soit $M \in \mathscr{M}_3(\mathbb{C})$ une matrice nilpotente et $p \in \mathbb{N}$ son indice de nilpotence.
    
    (a) Montrer que $p \leqslant 3$.
    
    (b) On suppose que $p=3$. Montrer que $M$ est semblable à la matrice
    
    $$
    \left(\begin{array}{lll}
    0 & 1 & 0 \\
    0 & 0 & 1 \\
    0 & 0 & 0
    \end{array}\right) .
    $$
    
    (c) On suppose que $p=2$. Montrer que $M$ est semblable à la matrice
    
    $$
    \left(\begin{array}{lll}
    0 & 0 & 1 \\
    0 & 0 & 0 \\
    0 & 0 & 0
    \end{array}\right) .
    $$
    
    (d) Soit $A \in \mathscr{M}_3(\mathbb{C})$. Montrer que $A$ est semblable à $-A$ si et seulement si $\operatorname{det} A=\operatorname{tr} A=0$.

!!! exercice "794"
    Soient $n \geqslant 2$ et $A \in \mathcal{M}_n(\mathbb{C})$ une matrice de rang 1 . Montrer qu'elle est diagonalisable si et seulement si sa trace est non nulle.

!!! exercice  "884"
    Soient $a_1, \ldots, a_n$ des réels non tous nuls et
    
    $$
    A=\left(\begin{array}{ccc}
    a_1 & \cdots & a_1 \\
    \vdots & & \vdots \\
    a_n & \cdots & a_n
    \end{array}\right) .
    $$
    
    (a) Déterminer les valeurs propres de $A$. À quelle condition la matrice $A$ est-elle diagonalisable?
    
    (b) On pose $s=\operatorname{tr} A$ et $B=2 A-s I_n$. À quelle condition la matrice $B$ est-elle diagonalisable? $\grave{A}$ quelle condition est-elle inversible?

!!! exercice "959"
    Soient $A$ et $B$ dans $\mathscr{M}_n(\mathbb{C})$ et $\Phi: M \in \mathscr{M}_n(\mathbb{C}) \mapsto M+\operatorname{tr}(A M) B$.

    (a) Déterminer les valeurs propres et les vecteurs propres de $\Phi$.

    (b) L'application $\Phi$ est-elle diagonalisable?


!!! exercice  "998"
    Soit $n \in \mathbb{N}$. Montrer que $\Phi: P \mapsto X^n P\left(\frac{1}{X}\right)$ est un endomorphisme diagonalisable de $\mathbb{R}_n[X]$. Trouver une base de vecteurs propres de $\Phi$.

!!! exercice "1018"
    Soient $B$ et $M$ dans $\mathcal{M}_n(\mathbb{C})$. On suppose que $M$ possède $n$ valeurs propres distinctes. Montrer que $B$ et $M$ commutent si et seulement s'il existe $P \in \mathbb{C}[X]$ tel que $B=P(M)$.

!!! exercice "1084"
    Soient $A \in \mathscr{M}_n(\mathbb{C})$ et $\Delta(A)=\left\{M \in \mathscr{M}_n(\mathbb{C}), M+M^{\top}=\operatorname{tr}(M) A\right\}$. Montrer que $\Delta(A)$ est un sous-espace vectoriel de $\mathscr{M}_n(\mathbb{C})$. Déterminer $\Delta(A)$.