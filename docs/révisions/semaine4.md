# semaine 4

## Suite et série de fonctions

!!! exercice "1724"
    Soit $\left(u_n\right)_{n \geqslant 0}$ définie par $\left.u_0 \in\right] 0,1\left[\right.$ et $\forall n \in \mathbb{N}, u_{n+1}=\frac{1}{2}\left(u_n+u_n^2\right)$.  
    (a) Déterminer un équivalent de $u_n$.  
    (b) Nature de la série de terme général $u_n$ ?

!!! exercice "2414"
    Soit $f_0: x \in \mathbb{R} \mapsto \sin (x)$ et, pour $n \in \mathbb{N}, f_{n+1}: x \in \mathbb{R} \mapsto \sin \left(f_n(x)\right)$. Étudier la convergence simple et uniforme de $\left(f_n\right)$.

## Régularité des séries de fonctions

!!! exercice "2433"
    Soit $u_n(x)=\frac{1}{n\left(n x^2+1\right)}$.  
    (a) Trouver le domaine de définition de $f(x)=\sum_{n=1}^{+\infty} u_n(x)$.  
    (b) Montrer que $f$ est continue sur $] 0,+\infty[$, et déterminer $\lim f$ en 0 et en $+\infty$.  
    (c) Montrer que $f$ est intégrable sur $] 0,+\infty[$.


!!! exercice "2522"
    Soient $\zeta: s \mapsto \sum_{n=1}^{+\infty} \frac{1}{n^s}$ et $\zeta_2: s \mapsto \sum_{n=1}^{+\infty} \frac{(-1)^{n-1}}{n^s}$.  
    (a) Déterminer le domaine de définition de $\zeta$ et celui de $\zeta_2$. Montrer que ces fonctions sont continues.  
    (b) Déterminer la valeur de $\zeta_2(1)$.  
    (c) Si $s>1$, exprimer $\zeta_2(s)$ en fonction de $\zeta(s)$.


## Intégration terme à terme
!!! exercice "2623"
    Soit, pour $(k, n) \in \mathbb{N}^2, u_{k, n}=\int_0^1|\ln t|^k t^{n-1 / 2} \mathrm{~d} t$.  
    (a) Justifier la définition de $u_{k, n}$.  
    (b) Déterminer la limite de la suite de terme général $u_{100, n}$.  
    (c) Montrer que la série de terme général $u_{100, n}$ est convergente et donner une expression de sa somme.

## Série entière
!!! exercice "2671"
    Soit $\sum_{n \geqslant 0} a_n z^n$ une série entière de rayon de convergence infini et de somme $f$.  
    (a) Montrer que pour $p \in \mathbb{N}$ et $r \in \mathbb{R}_{+}$, on a $\int_0^{2 \pi} f\left(r e^{i t}\right) e^{-i p t} \mathrm{~d} t=2 \pi a_p r^p$.  
    (b) On suppose $f$ bornée sur $\mathbb{C}$. Montrer qu'il existe $M>0$ tel que $\forall r \in \mathbb{R}_{+}^*\left|a_p\right| \leqslant \frac{M}{r^p}$. En déduire que $f$ est une fonction constante.  
    (c) On suppose qu'il existe des réels $a>0$ et $b>0$, et un entier naturel non nul $q$ tels que : $\forall z \in \mathbb{C},|f(z)| \leqslant a|z|^q+b$. Montrer que $f$ est une fonction polynomiale.  
    (d) On suppose que $\forall z \in \mathbb{C},|f(z)| \leqslant \exp (\operatorname{Re} z)$. Montrer qu'il existe $K \in \mathbb{C}$ tel que $\forall z \in \mathbb{C}, f(z)=K \exp (z)$.


!!! exercice "2799"
    Pour tout $n \in \mathbb{N}$, on pose $a_n=\int_0^1\left(\frac{1+t^2}{2}\right)^n \mathrm{~d} t$.  
    (a) Calculer $a_0$ et $a_1$. Déterminer la limite de $\left(a_n\right)$.  
    (b) i. Étudier la monotonie de la suite $\left(a_n\right)$. En déduire la nature de la série de terme général $(-1)^n a_n$.  
    ii. Montrer que $\sum_{n=0}^{+\infty}(-1)^n a_n=2 \int_0^1 \frac{\mathrm{d} t}{3+t^2}$. En déduire la valeur de cette somme.  
    (c) Soit $f: x \mapsto \sum_{n=0}^{+\infty} a_n x^n$.  
    i. Montrer que $\forall n \in \mathbb{N}, a_n \geqslant \frac{1}{2 n+1}$. En déduire le rayon de convergence $R$ de la série entière $\sum a_n x^n$.  
    ii. Montrer que $f$ est solution d'une équation différentielle que l'on déterminera.

!!! exercice "2838"  
    On pose $f: x \mapsto \int_0^\pi \sqrt{|1+x \cos t|} \mathrm{d} t$.  
    (a) Déterminer le domaine de définition de $f$.  
    (b) Montrer que $f$ est continue et paire sur ce domaine.  
    (c) Montrer que $f$ est développable en série entière.


