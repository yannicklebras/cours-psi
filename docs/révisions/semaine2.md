# semaine 2

## continuité/dérivabilité

!!! exercice "2002"
    Soit $f: \mathbb{R} \rightarrow \mathbb{R}$ telle que $\forall(x, y) \in \mathbb{R}^2, f(x+y)=f(x)+f(y)$.
    
    (a) Déterminer $f(0)$ puis, pour $x \in \mathbb{R}$ et $\lambda \in \mathbb{Q}$, exprimer $f(\lambda x)$ en fonction de $f(x)$.
    
    (b) Montrer que si $f$ est continue en 0 , alors $f$ est continue sur $\mathbb{R}$. Déterminer $f$ dans ce cas.
    
    (c) Même question en supposant cette fois $f$ bornée au voisinage de 0 .

!!! exercice "2004"
    Déterminer les $f: \mathbb{R} \rightarrow \mathbb{R}$ continues telles que $\forall x \in \mathbb{R}, f(2 x)-f(x)=x$.
    
    
!!! exercice "2020"
    Déterminer les $f: \mathbb{R} \rightarrow \mathbb{R}$ dérivables en zéro et telles que $\forall(x, y) \in \mathbb{R}^2, f(x+y)=e^x f(y)+e^y f(x)$.
    
!!! exercice "1912"
    Si $n \in \mathbb{N}^*$, soit $f_n: x \mapsto x+\frac{n}{2} \ln x-n$. Montrer que l'équation $f_n(x)=0$ admet une unique solution $a_n \in\left[1, e^2\right]$. Étudier la limite de $\left(a_n\right)$.
    
!!! exercice "110"
    Soient $\left(a_0, \ldots, a_{n-1}\right) \in \mathbb{C}^n$ et $P=a_0+a_1 X+\cdots+a_{n-1} X^{n-1}+X^n$. Montrer que les racines complexes de $P$ ont un module majoré par $\max \left\{1,\left|a_0\right|+\left|a_1\right|+\cdots+\left|a_{n-1}\right|\right\}$.
    
!!! exercice "121"
    Soit $P \in \mathbb{R}[X]$ scindé à racines simples. Montrer que $\forall x \in \mathbb{R}, P(x) P^{\prime \prime}(x) \leqslant P^{\prime}(x)^2$.
    
    
!!! exercice "1948"
    Soit $f: \mathbb{R} \rightarrow \mathbb{R}$ dérivable et telle que $f^{\prime}(x)$ tend vers $+\infty$ quand $x$ tend vers $+\infty$. Montrer que $f(x)$ tend vers $+\infty$ quand $x$ tend vers $+\infty$.


## Intégration

!!! exercice "2048"
    Soit $f \in \mathscr{C}^0([0,1], \mathbb{R})$. Montrer qu'il existe $c \in[0,1]$ tel que $f(c)=3 \int_0^1 f(t) t^2 \mathrm{~d} t$.
    

!!! exercice "2111"
    (a) Exprimer $\cos ^2 u$ et $\sin ^2 u$ en fonction de $\cos (2 u)$.

    (b) Montrer que $\forall x \in \mathbb{R}_{+}, x-\frac{x^2}{2} \leqslant \ln 
    (1+x) \leqslant x$.

    (c) Calculer $I=\int_0^1 \sqrt{x(1-x)} \mathrm{d} x$.

    Ind. Poser $x=(1-t) / 2$.

    (d) À l'aide de sommes de Riemann, calculer la limite de $u_n=\frac{1}{n^2} 
    \sum_{k=1}^n \sqrt{k(n-k)}$.

    (e) Déterminer la limite de $v_n=\prod_{k=1}^n\left(1+\frac{1}{n} \sqrt{k(n-k)}\right)^{1 / n}$.
    
## convexité


!!! exercice "1995"
    Soit $f:[a, b] \rightarrow \mathbb{R}$ continue sur $[a, b]$ et $\mathscr{C}^2$ sur $] a, b\left[\right.$, telle que $f^{\prime \prime} \geqslant 0$. Soit $\left(x_1, x_2, x_3\right)$ dans $\mathbb{R}^3$ tel que $a \leqslant x_1<x_2<x_3 \leqslant b$. Montrer: $\frac{f\left(x_2\right)-f\left(x_1\right)}{x_2-x_1} \leqslant \frac{f\left(x_3\right)-f\left(x_1\right)}{x_3-x_1}$.


