# semaine 7

!!! exercice "Reprise DS6"
    Soit $\mathcal Y_n=\{M\in\mathcal M_n(\mathbb R\big| \forall(i,j)\in\{1,\dots,n\}^2,\, M_{ij}\in[0,1]\}$. On note $||M||$ la norme euclidienne canonique sur $\mathcal M_n(\mathbb R)$. 
    
    a) Démontrer que pour tout $M \in \mathcal Y_n, \operatorname{det}(M) \leqslant n$ ! et qu'il n'y a pas égalité.
    
    b) Démontrer que $\mathcal Y_n$ est une partie convexe et compacte de $\mathcal{M}_n(\mathbb{R})$.

    c) On fixe $A \in \mathcal{M}_n(\mathbb{R})$, prouver qu'il existe une matrice $M \in y_n$ telle que :
    
    $$
    \forall N \in y_n \quad\|A-M\| \leqslant\|A-N\|
    $$

    Justifier l'unicité de la matrice $M$ ci-dessus et expliciter ses coefficients en fonction de ceux de $A$.

!!! exercice "1475"
    On munit $E=\mathscr{C}^0([0,1], \mathbb{R})$ de la norme infinie $\forall f \in E,\|f\|_{\infty}=\max _{[0,1]}|f|$. Soit $T \in \mathscr{L}(E, \mathbb{R})$. On suppose que $\forall f \in E, f \geqslant 0 \Rightarrow T(f) \geqslant 0$. Montrer que $T$ est lipschitzien.

!!! exercice "1460"
    Soit $E=\mathcal{C}^0([0,1], \mathbb{R})$. Pour $f \in E$, on pose $\|f\|_{\infty}=\sup _{x \in[0,1]}|f(x)|$ et $\|f\|_2=\sqrt{\int_0^1 f^2}$. Soient $n \in \mathbb{N}$ et $F$ un sous-espace de $E$ tel que (*) $\forall f \in F,\|f\|_{\infty} \leqslant n\|f\|_2$.
    
    (a) Montrer que $F \neq E$.
    
    (b) Montrer que $F$ est de dimension finie $\leqslant n^2$.
    
    (c) Donner un exemple de sous-espace $F$ de dimension $n$ vérifiant $(*)$.

!!! exercice "1461"
    Soient $E=\mathcal{C}^1([0,1], \mathbb{R})$ et, pour $f \in E, N(f)=\left(f(0)^2+\int_0^1 f^{\prime}(t)^2 \mathrm{~d} t\right)^{\frac{1}{2}}$.
    
    (a) Montrer que $N$ est une norme sur $E$.
    
    (b) La norme $N$ et la norme préhilbertienne canonique sont-elles équivalentes sur $E$ ?

!!! exercice "1468"
    Soit $E=\mathcal{C}^1([0,1], \mathbb{R})$. Si $f \in E$, on pose $\|f\|_2=\sqrt{\int_0^1 f^2}$ et $\|f\|_{\infty}=\sup _{t \in[0,1]}|f(t)|$.
    
    (a) Montrer que \|\|$_2$ est une norme sur $E$. Soit $\alpha \in[0,1]$ et $\Phi: f \in E \mapsto f(\alpha) \in \mathbb{R}$. L'application $\Phi$ est-elle continue pour la norme \|\|$_2$ ?
    
    (b) Existe-t-il $C>0$ tel que $\forall f \in E,\|f\|_{\infty} \leqslant C\|f\|_2$ ?
    
    (c) Soit $n \in \mathbb{N}$. Existe-t-il $C>0$ tel que $\forall P \in \mathbb{R}_n[X],\|P\|_{\infty} \leqslant C\|P\|_2$ ?

!!! exercice "1489"
    Soit $n \geqslant 2$. Montrer qu'il n'existe pas de norme sur $\mathscr{M}_n(\mathbb{R})$ invariante par similitude.

!!! exercice "1498"
    Soit $A \in \mathcal{M}_n(\mathbb{R})$ telle que la suite $\left(A^p\right)_{p \geqslant 1}$ converge vers $B$. Montrer que $B$ est diagonalisable.

!!! exercice "1509"
    Soit $A \in \mathscr{M}_n(\mathbb{Z})$ telle que $4 A^3+2 A^2+A=0$.
    
    (a) Montrer que pour tout $P \in \mathrm{GL}_n(\mathbb{C})$, l'application $M \in \mathscr{M}_n(\mathbb{C}) \mapsto P^{-1} M P$ est continue.
    
    (b) Montrer que les valeurs propres de $A$ sont de module $\leqslant \frac{1}{2}$. En déduire que $\lim _{k \rightarrow+\infty} A^k=0$.
    
    (c) Montrer que toute suite d'entiers relatifs qui converge est stationnaire.
    
    (d) Montrer que $A$ est nilpotente. Que peut-on alors dire de $A$ ?

!!! exercice "1515"
    (a) Soit $E$ un espace vectoriel de dimension finie et $F$ un sous-espace vectoriel de $E$. Montrer que $F$ est un fermé.
    
    (b) Soit $A \in \mathscr{M}_n(\mathbb{C})$. On note $\exp _p(A)=\sum_{k=0}^p \frac{A^k}{k !}$. Montrer que la suite $\left(\exp _p(A)\right)_{p \in \mathbb{N}}$ converge. On notera $\exp (A)$ sa limite.
    
    (c) Montrer que $\exp (A)$ est un polynôme en $A$.
    
    (d) Existe-t-il un polynôme $P \in \mathbb{C}[X]$ tel que $\forall A \in \mathscr{M}_n(\mathbb{C}), \exp (A)=P(A)$ ?