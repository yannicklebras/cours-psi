# dernière semaine

!!! exercice "3383"
    Soient $(\Omega, \mathscr{A}, P)$ un espace probabilisé et $N$ une variable aléatoire suivant une loi géométrique de paramètre $p \in] 0,1[$. On lance une pièce équilibrée $N$ fois. Soit $X$ la variable aléatoire comptant le nombre de pile obtenus.
    
    (a) Donner la loi conditionnelle de $X$ sachant que $\left(N=n_0\right)$.
    
    (b) Soit $f: x \mapsto \sum_{n=k}^{+\infty}\left(\begin{array}{l}n \\ k\end{array}\right) x^n$. Donner le rayon de convergence de cette série entière et calculer $f(x)$ pour $x$ appartenant à son intervalle ouvert de convergence.
    
    (c) Déterminer la loi de $X$.

!!! exercice "3348"
    Soient $p \in \mathbb{N}^*, U_0, \ldots, U_p$ des urnes. On suppose que l'urne $U_k$ contient $k$ boules blanches et $p-k$ boules noires. On choisit une urne au hasard, on tire dans cette urne $n$ boules avec remise; on note $N_p$ le nombre de boules blanches tirées. Déterminer la loi de $N_p$.

    (a) Déterminer la loi de $N_p$ et l'espérance de $N_p$.

    (b) Soit $k \in\{0, \ldots, n\}$. Montrer que $P\left(N_p=k\right) \rightarrow\left(\begin{array}{l}n \\ k\end{array}\right) \int_0^1 x^k(1-x)^{n-k} \mathrm{~d} x$ quand $\mathrm{p} \rightarrow+\infty$.
    
    (c) En déduire la limite en loi de $\left(N_p\right)_p \geqslant 1$. En d'autres termes, trouver une variable aléatoire $Y$ à valeurs dans $\{0, \ldots, n\}$ telle que $\forall k \in\{0, \ldots, n\}, \lim _{p \rightarrow+\infty} \mathbb{P}\left(N_p=k\right)=\mathbb{P}(Y=k)$.

!!! exercice "3314"
    Soient $X$ et $Y$ deux variables aléatoires indépendantes suivant une loi uniforme sur $\{1, \ldots, n\}$. Déterminer la loi de $X+Y$.

!!! exercice "3329"
    Un questionnaire comporte 20 questions. Pour chaque question, $k$ réponses sont possibles dont une seule est bonne. Chaque bonne réponse rapporte 1 point. Un candidat répond au hasard à toutes les questions.
    
    (a) Soit $X$ la variable aléatoire donnant le nombre de points obtenus par le candidat à ce questionnaire. Déterminer la loi de $X$.
    
    (b) Pour chaque question, si le candidat s'est trompé, il a droit à une seconde chance et peut choisir une autre réponse parmi celles qui restent. II gagne alors $\frac{1}{2}$ point en cas de bonne réponse. Soit $Y$ le nombre de $\frac{1}{2}$ points obtenus, déterminer la loi de $Y$.
    
    (c) Déterminer $k$ pour que le candidat obtienne en moyenne une note de 5 sur 20 .

!!! exercice "3361"
    Soient $X_1, \ldots, X_n$ des variables aléatoires mutuellement indépendantes suivant une loi de Bernoulli de paramètre $p$. On pose $U=\left(\begin{array}{c}X_1 \\ \vdots \\ X_n\end{array}\right)$ et $M=U^{\mathrm{t}} U$

    (a) Déterminer la loi de probabilité de $\operatorname{rg}(M)$ et de $\operatorname{tr}(M)$.

    (b) Quelle est la probabilité que $M$ soit une matrice de projection?

    (c) On suppose $n=2$. Soit $V=\left(\begin{array}{l}1 \\ 1\end{array}\right)$ et $S={ }^t V M V$. Déterminer $E(S)$ et $V(S)$.

!!! exercice "3374"
    Soient $X$ et $Y$ deux variables aléatoires indépendantes suivant une loi géométrique de paramètre $p \in] 0,1[$. Soit $Z=$ $\max \{X, Y\}$. Déterminer l'espérance de $Z$.

!!! exercice "3395"
    Soient $X \hookrightarrow \mathscr{P}(\lambda)$ et $Y \hookrightarrow \mathscr{P}(\mu)$, indépendantes. On pose $Z=X+Y$.
    
    (a) Déterminer la loi de $Z$.
    
    (b) Pour $k \in \mathbb{N}$ et $n \in \mathbb{N}^*$, calculer $P(X=k \mid Z=n)$.
    
    (c) En déduire la loi de $X$ sachant $(Z=n)$.

!!! exercice "3441"
    Soit $Z$ une variable aléatoire suivant une loi géométrique de paramètre $p \in] 0,1\left[\right.$. On définit $f_Z: x \mapsto x^Z \sin \left(\frac{Z}{x}\right)$. Déterminer la probabilité que $f$ soit de classe $\mathscr{C}^1$ sur $\mathbb{R}$.