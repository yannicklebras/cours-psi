!!! exercice "BEOS 7339a"  
	=== "Enoncé"
		Pour $(a, b, c) \in \mathbb{R}^3$, on définit $f_{a, b, c}: \mathbb{R} \longrightarrow \mathbb{R}^3, t \longmapsto\left(\begin{array}{c}b e^t+c e^{-t} \\ 2 a-b e^t \\ a+c e^{-t}\end{array}\right)$ Soit $F=\left\{f_{a, b, c} ;(a, b, c) \in \mathbb{R}^3\right\}$.
		
		- 1) Montrer que $F$ est un espace vectoriel, en donner une base et la dimension.
		- 2) Déterminer $B \in \mathcal{M}_3(\mathbb{R})$ vérifiant $\quad \forall f \in F \quad \forall t \in \mathbb{R} \quad f^{\prime}(t)=B f(t)$.

	=== "Corrigé"
		$F$ est clairement un sev de l'ensemble des fonctions de $\mathbb R$ dans $\mathbb R^3$. Une base de $F$ est clairement $\left(f_{1,0,0},f_{0,1,0},f_{0,0,1}\right)$. Sa dimension est $3$.
		
		Calculons $f_{a,b,c}'(t)=\begin{pmatrix}be^t-ce^{-t}\\-be^t\\-ce^t\end{pmatrix}$. Par les calculs on trouve :
		
		$$
		f_{a,b,c}'(t)=\begin{pmatrix} 3 & 2 & -4 \\ -2 & -1 & 2 \\1 & 1 & -2\end{pmatrix}f_{a,b,c}(t)
		$$

	<div class="admonition-foot">equation différentielle,dimension finie</div>
