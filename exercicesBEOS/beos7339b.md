!!! exercice "BEOS 7339b"  
	=== "Enoncé"
		Soit $f: x \in \mathbb{R} \longmapsto \int_0^{+\infty} \mathrm{e}^{-t^2} \cos (2 x t) \mathrm{d} t$

		- 1) Montrer que $f$ est définie et de classe $\mathcal{C}^1$ sur $\mathbb{R}$.
		- 2) Trouver une relation entre $f$ et $f^{\prime}$.
		- 3) Sachant que $f(0)=\frac{\sqrt{\pi}}{2}$, déterminer $f$ et donner sa limite en $+\infty$.
	=== "Corrigé"
		Posons $g(x,t)=e^{-t^2}\cos(2xt)$. Alors pour $(x,t)\in\mathbb R\time [0,+\infty[$, $|g(x,t)|\le e^{-t^2}$ qui est intégrable sur $[0,+\infty[$. Par comparaison, $f(x)$ est bien définie sur $\mathbb R$.
		Vérifions les hypothèses du théorème de dérivation :  
		- $g(.,t)$ est de classe $\mathcal C^1$ par théorèmes généraux ;  
		- $g(x,.)$ est intégrable sur $[0,+\infty[$ d'après la question précédente ;  
		- $\frac{\partial g}{\partial x}(x,t)=2te^{-t^2}\sin(2xt)$ et donc $\frac{\partial g}{\partial x}(x,.)$ est continue par morceaux ;  
		- Soit $(x,t)\in \mathbb R\times [0,+\infty[$, $\left|\frac{\partial g}{\partial x}(x,t)\right|\le 2te^{-t^2}$ qui est intégrable sur $[0,+\infty[$.

		Ainsi, d'après le théorème de dérivation, $f$ est de classe $\mathcal C^1$ et $f'(x)=-\int_{0}^{+\infty}2te^{-t^2}\sin(2xt)dt$.

		Effectuons une intégration par parties sur $f'$ :

		$$
		\begin{align*}
		f'(t)&=-\int_{0}^{+\infty}2te^{-t^2}\sin(2xt)dt\\
		&=\left[e^{-t^2}\sin(2xt)\right]_0^{+\infty}-2x\int_0^{+\infty}e^{-t^2}\cos(2xt)dt\\
		&=-2xf(t)
		\end{align*}
		$$

		On a donc $f'(x)=-2xf(x)$.

		$f$ est donc une solution de $y'+2xy=0$ dont les solutions sont de la forme $x\mapsto Ae^{-x^2}$. Or $f(0)=\frac{\sqrt{\pi}}{2}$ donc $f(x)=\frac{\sqrt{\pi}}{2}e^{-x^2}$. Sa limite en $+\infty$ vaut $0$.
	<div class="admonition-foot">équation différentielle,théorème de dérivation</div>
