!!! exercice "BEOS 7330"  
	=== "Enoncé"
		Soit $A=\left(\begin{array}{rrr}1 & 2 & -3 \\ 2 & 4 & -6 \\ 4 & 8 & -12\end{array}\right)$

		- 1) Déterminer le rang de $A$.  
		- 2) En déduire sans calcul le polynôme caractéristique.  
		- 3) Déterminer les éléments propres de $A$.  
		- 4) $A$ est-elle diagonalisable?
	=== "Corrigé"
		Notons $C_i$ la $i$-ème colonne de $A$. On remarque que $C_1\ne 0$, $C_2=2C_1$ et $C_3=-3C_1$. La matrice est donc de rang $1$.

		Le polynôme caractéristique de $A$ est donc $X^2(X-\tr A)=X^2(X+8)$.

		Les valeurs propres de $A$ sont donc $0$ de multiplicité $2$ et $-8$ de multiplicité $1$. Remarquons que $A=C_1L_1^\top$. Alors $AX=0$ devient $C_1(X^\top L_1)^\top=<X|L_1>C_1=0$. Or $C_1\ne 0$ donc $<X|L_1>=0$. On en déduit que les vecteurs propres associés à $0$ forment exactement l'espace $\{L_1\}^\top$ qui est de dimension 2.

		Résolvons maintenant $AX=-8X$ : cette équation devient

		$$
		\begin{align*}
		\begin{cases}
		 x_1+2x_2-3x_3&=-8x_1\\
		2x_1+4x_2-6x_3&=-8x_2\\
		4x_1+8x_2-12x_3&=-8x_3
		\end{cases}
		&\Leftrightarrow
		\begin{cases}
		9x_1+2x_2-3x_3&=0\\
		2x_1+12x_2-6x_3&=0\\
		4x_1+8x_2-4x_3&=0
		\end{cases}\\
		&\Leftrightarrow
		\begin{cases}
		9x_1+2x_2-3x_3&=0\\
		x_1+6x_2-3x_3&=0\\
		x_1+2x_2-x_3&=0
		\end{cases}\\
		&\Leftrightarrow
		\begin{cases}
		x_1+2x_2-x_3&=0\\
		x_1+6x_2-3x_3&=0\\
		9x_1+2x_2-3x_3&=0
		\end{cases}\\
		&\Leftrightarrow
		\begin{cases}
		x_1+2x_2-x_3&=0 \\
		4x_2-2x_3&=0\\
		-16x_2+6x_3&=0
		\end{cases}\\
		&\Leftrightarrow
		\begin{cases}
		x_1+2x_2&=x_3\\
		4x_2&=2x_3
		\end{cases}\\
		&\Leftrightarrow
		\begin{cases}
		x_1&=0\\
		x_2&=\frac 12x_3
		\end{cases}
		\end{align*}
		$$

		On en déduit que l'espace propre associé à $-8$ est $Vect(\begin{pmatrix}0\\1\\2\end{pmatrix})$.

		Les multiplicités algébriques sont égales aux multiplicités géométriques : $A$ est diagonalisable.
	<div class="admonition-foot">Réduction</div>
