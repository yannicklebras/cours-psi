!!! exercice "BEOS 3132"  
	=== "Enoncé"
        a) Résoudre l'équation différentielle $\left(1+t^2\right) y^{\prime}+2 t y=0$.

        b) Soit $f$ définie sur $\mathbb{R} \times \mathbb{R}_{+}^*$ par $f(x, y)=g\left(\frac{y}{x}\right)$ avec $g$ de classe $\mathcal{C}^2$ sur $\mathbb{R}_{+}^*$.

        i) Calculer les dérivées partielles d'ordre 2 de $f$.

        ii) Déterminer les fonctions $f$ telles que $\Delta f=\frac{\partial^2 f}{\partial x^2}+\frac{\partial^2 f}{\partial y^2}=0$.

	=== "Corrigé" 
        L'équation différentielle a pour solutions les fonctions de la forme $\frac \lambda{1+t^2}$.


        $\frac{\partial f}{\partial x}(x,y)=-\frac y{x^2}g'(\frac yx)$

        $\frac{\partial f}{\partial y}(x,y)=\frac 1xg'(\frac yx)$

        $\frac{\partial^2 f}{\partial x^2}(x,y)=\frac{2y}{x^3}g'(\frac yx)+\frac{y^2}{x^4}g''(\frac yx)$. 

        $\frac{\partial^2 f}{\partial y^2}(x,y)=\frac1{x^2}g''(\frac yx)$.


        On traduit alors $\Delta f=0$ par $\frac{2y}{x^3}g'(\frac yx)+\frac{y^2}{x^4}g''(\frac yx)+\frac1{x^2}g''(\frac yx)=0$. Or $\frac{2y}{x^3}g'(\frac yx)+\frac{y^2}{x^4}g''(\frac yx)+\frac1{x^2}g''(\frac yx)=\frac1{y^2}\left(\frac{2y^3}{x^3}g'(\frac yx)+\frac{y^4}{x^4}g''(\frac yx)+\frac{y^2}{x^2}g''(\frac yx)\right)$ et en posant $u=\frac yx$ on obtient $2u^3g'(u)+(u^4+u^2)g''(u)=0$, c'est à dire pour $u\ne 0$, $2ug'(u)+(1+u^2)g''(u)=0$. On reconnaît l'équation différentielle du début et alors $g'(u)=\frac \lambda{1+u^2}$ et donc $g(u)=\lambda\arctan(u)+\delta$. On en déduit $f(x,y)=\lambda \arctan(\frac yx)+\delta$. 

        On vérifie par le calcul que ces fonctions sont bien solutions.
