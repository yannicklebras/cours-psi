!!! exercice "BEOS7333b"
	=== "Enoncé"
		Soit $A \in \mathcal{M}_n(\mathbb{C})$ et $B=\left(\begin{array}{ll}A & A \\ 0 & 0\end{array}\right)$
		
		- 1. Diagonaliser $\left(\begin{array}{ll}1 & 1 \\ 0 & 0\end{array}\right)$.
		- 2. On suppose $A$ diagonalisable. $B$ est-elle diagonalisable? Trouver les valeurs propres de $B$ en fonction de celles de $A$.

	=== "Corrigé"
		Posons $B_0=\begin{pmatrix}1 & 1 \\ 0 & 0\end{pmatrix}$. 
		Le polynôme caractéristique de $B_0$ est $\det(XI_2-B_0)$. Or
		
		$$
		\begin{align*}
		\det(XI_2-B_0)&=\begin{vmatrix} X-1 & -1 \\ 0 & X\end{vmatrix}\\
		&=X(X-1)
		\end{align*}
		$$ 
		
		Le polynôme caractéristique de $B_0$ est donc scindé à racines 
		simples et $B_0$ est diagonalisable, de matrice diagonale semblable
		
		$$
		\begin{pmatrix} 1 & 0 \\ 0 & 0\end{pmatrix}
		$$
		
		Supposons maintenant $A$ une matrice diagonalisable et $B$ définie 
		comme dans l'énoncé. Notons $P_A$ un polynôme scindé à racines simples annulateur de $A$.
		On remarque que pour $n\ge1$, $B^n=\begin{pmatrix}A^n&A^n\\0&0\end{pmatrix}$.

		__Si $0$ est valeur propre de $A$__  
		On en déduit par linéarité que $P_A(B)=\begin{pmatrix}P_A(A)&P_A(A)\\0&0\end{pmatrix}=0$.
		Ainsi, $P_A$ annule $B$. Or $P_A$ est scindé à racines simples
		donc $B$ est diagonalisable avec les mêmes valeurs propres que $A$.

		__Si $0$ n'est pas valeur propre de $A$__
		Le raisonnement ne marche pas à cause du terme en $B^0$.
		A chaque vecteur propre $X$ de $A$ on peut compléter avec des $0$
		pour obtenir un vecteur propre de $B$ associé à la même valeur.
		Les sous-espaces propres ont alors la même dimension. De plus, 
		si $BX=0$ alors en notant $X=\begin{pmatrix}X_1\\X_2\end{pmatrix}$,
		on trouve $A(X_1+X_2)=0$. Or $A$ est inversible donc $X_1+X_2=0$.
		Une base de $\mathrm{Ker}(B)$ est donc de dimension $n$. On peut donc construire une base
		de vecteurs propres pour $B$ : $B$ est diagonalisable. 

	<div class="admonition-foot">diagonalisation</div>
