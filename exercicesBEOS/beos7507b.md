<!--
tags:
    - réduction
-->
!!! exercice "BEOS 7507b"
	=== "Enoncé"
		
		Soit $\phi$ l'application de $\mathbb{R}_3[X]$ dans $\mathbb{R}^4: P \mapsto\left(P(0), P^{\prime}(0), P(-1), P^{\prime}(-1)\right)$.    
		  
		- 1) Montrer que $\phi$ est linéaire.  
		- 2) Déterminer $\operatorname{Ker} \phi$. L'application est-elle bijective ?  
		- 3) Exprimer $M$, matrice de $\phi$ dans la base canonique.  
		- 4) a) Montrer que $M$ est diagonalisable.  
			- b) Donner un polynôme annulateur de $M$.  
			- c) $M$ est-elle inversible ? Si oui, donner son inverse.  
		- 5) a) Montrer qu'il existe un unique polynôme $Q$ tel que $\phi(Q)=(0,1,0,1)$.  
			- b) Déterminer $Q$.  
			- c) En déduire la valeur de la somme $\sum_{k=0}^n k^2$ (on pourra étudier $Q(n)-Q(n-1)$ pour un entier $n\ge1$).

	=== "Corrigé"
		
		$\phi$ est clairement linéaire.  
		Soit $P \in\ker \phi$, alors $\phi(P)=0$. On en déduit que $P(0)=P'(0)$ donc $0$ est une racine de multiplicité 2. De même $1$ est une racine de multiplicité 2.
		Cela fait trop de racines pour un polynôme de degré $3$ donc $P$ est nul. $\phi$ est une application linéaire injective entre deux espaces de même dimension : 
		$\phi$ est bijective.  
		On a $\phi(1)=(1,0,1,0)$, $\phi(X)=(0,1,-1,1)$, $\phi(X^2)=(0,0,1,-2)$ et $\phi(X^3)=(0,0,-1,3)$. La matrice de $\phi$ dans la base canonique est donc 
		$\begin{pmatrix} 1&0&0&0\\0&1&0&0\\1&-1&1&-1\\0&1&-2&3\end{pmatrix}$  
		
		Calculons le polynôme caractéristique de $M$ : 
		
		$$
		\chi_M(\lambda)=\det\left(\begin{pmatrix} \lambda-1&0&0&0\\0&\lambda-1&0&0\\_1&1&\lambda-1&+1\\0&-1&2&\lambda-3\end{pmatrix}\right)=(\lambda-1)^2\left((\lambda-1)(\lambda-3)-2\right)=(\lambda-1)^2(\lambda^2-4\lambda+1)
		$$			
		
		On en déduit que $\chi_M$ admet 3 racines réelles : $1$ de multiplicité $2$ et les deux racines distinctes de $X^2-4X+1$. Pour étudier la diagonalisabilité, on peut se contenter d'étudier le sep assicié à $1$.
		
		$$
		MX+X\Leftrightarrow \begin{cases}x_1&=x_1\\x_2&=x_2\\x_1-x_2+x_3-x_4&=x_3\\x_2-2x_3+3x_4=x_4\end{cases}\Leftrightarrow\begin{cases}x_1-x_2-x_4&=0\\x_2-2x_3+2x_4&=0\end{cases}
		$$ 

		qui est l'intersection de deux hyperplans non parallèle et est donc de dimension 2.  Ainsi la multiplicité géométrique et la multiplicité algébrique de $1$ en tant que valeur propre sont égales : cela suffit ici (les deux autres vp sont de multiplicité $1$) à assurer la diagonalisabilité de $M$.
		
		Dès lors, un polynôme annulateur de $M$ est $(X-1)(X^2-4X+1)=X^3-5X^2+5X-1$. On a ainsi $M^3-5M^2+5M-I_4=0$ ce qu'on peut réécrire en $M(M^2-5M+5I_4)=I_4$ : $M$ est inversible, d'inverse $M^2-5M+5I_4$.

		Par bijectivité de l'application $\phi$, $Q$ existe et est unique. 

		On a donc $Q(0)=0$, $Q(-1)=1$ et $Q'(0)=Q'(-1)=1$. On en déduit $Q=\lambda X(X+1)(X-a)$ (racines $-1$ et $0$). On a alors $Q'(0)=-\lambda a$ et $Q'(-1)=\lambda(1+a)$. On en déduit $\lambda=2$ et $a=-\frac 12$. Ainsi $Q=X(X+1)(2X+1)$.
		
		Calculons $Q(n)-Q(n-1)=n(n+1)(2n+1)-(n-1)n(2n-1)=n(2n^2+3n+1-2n^2+3n-1)=6n^2$. On a alors par télescopage $Q(n)-Q(0)=6\sum_{k=1}^n 6k^2$ c'est à dire $\frac{n(n+1)(2n+1)}6=\sum_{k=1}^n k^2$.   
	
	
	<div class="admonition-foot">application linéaire,polynôme</div> 

