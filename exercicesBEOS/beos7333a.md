!!! exercice "BEOS7333a"  
	=== "Enoncé"
		On définit $(E): 5 x^{\prime \prime}(t)+10 x^{\prime}(t)+6 x(t)=0$
			- 1) Résoudre l'équation différentielle dans $\mathbb{R}$.
			- 2) Soit $x$ une solution non nulle de $(E)$. Montrer qu'il existe $t \in \mathbb{R}$ tel que $|x(t)|=1$.
			- 3) Étudier les variations de $\phi(t)=\frac{t^2}{1+t^4}$ sur $\mathbb{R}$.
			- 4) Soit $x$ vérifiant $(E)$. Montrer que l'application qui à $t$ associe $\frac{x(t)^2}{1+x(t)^4}$ est bornée et atteint sa borne supérieure. Atteint-elle sa borne inférieure ?
	=== "Corrigé"
		Dans un premier temps on résoud l'équation différentielle.
		Le polynôme caractéristique de l'équation est $5X^2+10X+6$
		dont le discriminant vaut $100-120=-20=(2i\sqrt 5)^2$. Les
		racines sont donc complexes conjuguées :
		$\frac{-10\pm2i\sqrt5}{10}=-1\pm\frac i{\sqrt 5}$.
		On sait donc que les solutions sont de la forme
		$x(t)=Ae^{-t}\cos(\frac t{\sqrt 5}+\phi)$.

		Soit $x$ une solution non nulle. Posons $k\in\mathbb Z$,
		$t_k=\sqrt5(2k\pi-\phi)$.
		On a alors $\cos(\frac {t_k}{\sqrt 5}+\phi)=1$. On a alors
		$|x(t_k)|=|A|e^{-t_k}$. On a $\lim_{k\to+\infty}|x(t_k)|=0$
		et $\lim_{k\to -\infty}|x(t_k)|=+\infty$. Il existe donc $k_1$
		et $k_2$ des entier relatifs tels que $|x(t_{k_1})|>1$ et
		$|x(t_{k_2})|<1$. La fonction étant continue, il existe, d'après
		le TVI, un $t$ tel que $|x(t)|=1$.

		Calculons $\Phi'(t)=\frac{2t(1+t^4)-4t^5}{(1+t^4)^2}=\frac{2t(1-t^4)}{(1+t^4)^2}$.
		On en déduit les variations de $\Phi$ : croissante que $]-\infty;-1]$,
		décroissante sur $[-1,0]$, croissante sur $[0,1]$ et décroissante
		sur $[1,+\infty[$.

		La fonction $\Phi$ est bornée entre $0$ et $1$. Donc en
		particulier, $\Phi(x(t))$ est bornée entre $0$ et $1$.
		Or $\Phi$ atteint un maximum en $-1$ et en $1$, et on a montré
		que $x(t)$ atteint l'une de ces deux valeurs. Ainsi, $\Phi(x(t))$
		atteint sa borne supérieur.

		$\Phi$ n'atteint pas a borne inférieur, donc, $x$ étant
		continue, $\Phi\circ x$ non plus.

	<div class="admonition-foot">équation différentielle,1A</div>
