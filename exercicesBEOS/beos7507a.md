!!! exercice "BEOS 7507a"  
	=== "Enoncé"
		Soit $\sum a_n x^n$ une série entière dont le rayon de convergence est $R$.
		On pose $f_n(x)=a_n x^n$

		- 1) Montrer la convergence normale de la série $\sum f_n$ sur $[-r, r]$ pour $0<r<R$.  
		- 2) En déduire la continuité de la somme $x \mapsto \sum_{n=0}^{+\infty} a_n x^n$.  
		- 3) Rappeler le développement de arctan en 0 . Montrer qu'il reste valable en 1.  
	=== "Corrigé"
		Soit $x\in[-r,r]$, alors $|f_n(x)|=|a_n||x|^n\le |a_n|r^n$ qui est une majoration indépendante de $x$.
		De plus la série $\sum a_nr^n$ est absolument convergente car $0<r<R$ (on est à l'intérieur du disque de convergence).
		Ainsi la série des $f_n$ est normalement convergente.

		On a ainsi convergence normale de la série de fonctions sur tout $[-r,r]$, donc convergence uniforme : par théorème de continuité de la somme d'une série de fonctions, la fonction est continue sur tout $[-r,r]$ avec $r<R$ donc elle est continue sur $]-R,R[$.

		Le développement en $0$ de $\arctan$ est :

		$$
		\arctan(x)=x-\frac{x^3}{3}+\frac{x^5}{5}+\dots+\frac{(-1)^nx^{2n+1}}{2n+1}+o(x^{2n+1}).
		$$

		Soit $x\in\mathbb R$, on pose $u_n(x)=\frac{(-1)^nx^{2n+1}}{2n+1}$.  On a $\left|\frac{u_{n+1}(x)}{u_{n}(x)}\right|=\frac{x^2(2n+1)}{2n+3}\to x^2$. On en déduit que le rayon de convergence de la série entière est $1$. On cherche donc à savoir s'il y a aussi convergence en $1$ (qui est a priori exclu du disque de convergence).

		Soit donc $x\in[0,1]$, la série $\sum u_n(x)$ est une série alternée convergente. Notons $R_n(x)$ le reste d'ordre $n$, on a d'après le CSSA $|R_n(x)|\le \frac{x^{2n+1}}{2n+1}\le \frac 1{2n+1}\to 0$. On en déduit que $\sum u_n(x)$ converge uniformément sur $[0,1]$ : d'après le théorème de continuité, $\sum u_n(x)$ est continue en $1$ et par unicité de la limite, $\sum_{k=0}^{+\infty}u_n(1)=\arctan(1)=\frac \pi4$.    	
	<div class="admonition-foot">Série entière, disque de convergence</div>
