!!! exercice "RMS2019-1011"
	=== "Enoncé"
		Soit $Q:x\mapsto (x-1)(x^2-2)^2$, $f:x\mapsto x-\frac{Q(x)}{Q'(x)}$ et enfin $(u_n)_n$ la suite définie par $u_0=10$ et, pour tout $n\in\mathbb N$, $u_{n+1}=f(u_n)$.

		- a) Écrire une fonction Python qui prend un entier $n$ en argument et renvoie les $n$ premiers termes de cette suite. Que peut-on conjecturer sur la limite de cette suite?  
		- b) Montrer que $Q'$ est scindé à racines simples dans $[-\sqrt{2},\sqrt{2}]$. On admet qu'il en est de même pour $Q''$.  
		- c) Étudier les variations de $f$.  
		- d) Écrire une fonction Python qui prend un entier $n$ en argument et trace les points de coordonnées $(k,\ln(u_k-\sqrt{2}))$ pour tout $k\leq n$. Que peut-on conjecturer?  
		- e) Montrer que, pour tout $n\in\mathbb N$, il existe
		  $c_n>\sqrt{2}$ tel que $u_{n+1}-\sqrt{2}=f'(c_n)(u_n-\sqrt{2})$.  
		- f) Montrer que la limite de $f'$ en $\sqrt{2}$ par valeurs supérieures est $1/2$.  
		- g) En déduire la preuve de la conjecture précédente.  

	=== "Corrigé"
		```python
		from numpy.polynomial import Polynomial

		Q = Polynomial([-1,1])*Polynomial([-2,0,1])**2

		def f(x) :
		    global Q
		    return x-Q(x)/Q.deriv()(x)
		```


		```python
		f(3)
		```




		    2.5483870967741935




		```python
		def u(n) :
		    u=[10]
		    for i in range(n-1) :
		        u.append(f(u[-1]))
		    return u
		```


		```python
		u(10)
		```




		    [10,
		     8.074235807860262,
		     6.541527629026789,
		     5.3248556425345015,
		     4.362783830788522,
		     3.606278708437776,
		     3.0161124501029803,
		     2.560729998204561,
		     2.214504641077209,
		     1.9563488863912304]




		```python
		u(100)
		```




		    [10,
		     8.074235807860262,
		     6.541527629026789,
		     5.3248556425345015,
		     4.362783830788522,
		     3.606278708437776,
		     3.0161124501029803,
		     2.560729998204561,
		     2.214504641077209,
		     1.9563488863912304,
		     1.768665852013774,
		     1.6365973057160934,
		     1.547469779474154,
		     1.4903613736369952,
		     1.4558698005004111,
		     1.4362243881477756,
		     1.4255700433662408,
		     1.4199885948718396,
		     1.4171265872593901,
		     1.4156766295325953,
		     1.4149467577738428,
		     1.414580578483789,
		     1.4143971754037628,
		     1.414305395179342,
		     1.414259485352659,
		     1.414236525506241,
		     1.4142250443464226,
		     1.4142193034629025,
		     1.4142164329255331,
		     1.414214997664669,
		     1.4142142799203778,
		     1.4142139213246057,
		     1.4142137421152652,
		     1.4142136526433327,
		     1.414213606620553,
		     1.4142135884478826,
		     1.4142135678892096,
		     1.4142135921845287,
		     1.4142135786981842,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994]



		La suite semble converger vers $\sqrt 2$.

		On a $Q'=(x^2-2)^2+2(x-1)\times 2x\times(x^2-2)=(x^2-2)(x^2-2+4x^2-4x)=(x^2-2)(5x^2-4x-2)$. Le second membre (noté $R$) est un trinôme dont le sommet se trouve en $\frac 25\in[-\sqrt 2,\sqrt 2]$. De plus  $R(\sqrt 2)=10-4\sqrt 2-2=4(2-\sqrt 2)>0$ et $R(-\sqrt 2)=10+4\sqrt2-2>0$. On en déduit que les racines de $R$ sont bien dans l'intervalle $[-\sqrt2,\sqrt2]$.

		On suppose donc qu'il en est de même pour $Q''$.

		On a $f'(x)=1-\frac{Q'(x)^2-Q(x)Q''(x)}{Q'(x)^2}=\frac{Q(x)Q''(X)}{Q'(x)^2}$ dont le signe est déterminé par le signe de $Q(x)Q''(x)$. Avec les données de l'énoncé, on ne peut pas dire grand chose de plus, si ce n'est que sur $[\sqrt2,+\infty[$ la fonction $f$ est croissante.


		```python
		from numpy import log
		from matplotlib import pyplot as plt
		def graphique(n) :
		    termes_u = u(n)
		    y =[log(termes_u[k]-2**(1/2)) for k in range(n)]
		    x = list(range(n))
		    plt.figure()
		    plt.plot(x,y)
		    plt.show()
		```


		```python
		graphique(150)
		```



		![png](CentralePython/rms2019-1011_files/rms2019-1011_9_0.png)



		L'interprétation est délicate : on semble avoir une décroissance linéaire, mais à partir du rang $\sim 40$ le comportement change.

		Montrons que $u_n>\sqrt2$. C'est le cas pour $u_0$. Intéressons-nous à l'hérédité : $u_{n+1} = u_n-\frac{Q(u_n)}{Q'(u_n)}$.  

		Or $\frac Q{Q'}=\frac{(X-1)(X^2-2)^2}{(X^2-2)(5X^2-4X-2)}=\frac{(X-1)(X^2-2)}{5X^2-4X-2}$. Or $f$ est croissante sur $[\sqrt2,+\infty[$ et $f(\sqrt2)=\sqrt2$. De plus $u_n>\sqrt2$ par hypothèse de récurrence dont $f(u_n)>\sqrt2$. On en déduit ainsi que $u_{n+1}>\sqrt 2$.

		On applique ensuite simplement le théorème des accroissements finis à $f$ entre $\sqrt 2$ et $u_n$ : il existe $c_n>\sqrt2$ tel que $u_{n+1}-\sqrt 2=f(u_n)-f(\sqrt 2)=f'(c_n)(u_n-\sqrt2)$.

		On a

		$$
		\begin{align*}
		f(x)&=x-\frac{(x-1)(x^2-2)}{(5x^2-4x-2}\\
		&=x-\frac{(x-1)(x^2-2)}{4(2-\sqrt2)+(10\sqrt 2-4)(x-\sqrt 2)+5(x-\sqrt 2)^2}\\
		&=\sqrt 2+(x-\sqrt2)-\frac{2\sqrt 2(\sqrt 2-1)(x-\sqrt 2)+o(x-\sqrt 2)}{4(2-\sqrt2)+(10\sqrt 2-4)(x-\sqrt 2)+o(x-\sqrt 2)}\\
		&=\sqrt 2+(x-\sqrt2)-\frac{1}{4(2-\sqrt2)}\times (2\sqrt 2(\sqrt 2-1)(x-\sqrt 2)+o(x-\sqrt 2))\times(1-\frac{(10\sqrt 2-4)}{4(2-\sqrt2)}(x-\sqrt 2)+o(x-\sqrt 2))\\
		&=\sqrt 2+(x-\sqrt2)-\frac{1}{4(2-\sqrt2)}\times (2\sqrt 2(\sqrt 2-1)(x-\sqrt 2)+o(x-\sqrt 2))\\
		&=\sqrt 2+\frac{1}{2}(x-\sqrt 2)+o(x-\sqrt2)
		\end{align*}
		$$

		$f$ étant dérivable sur $[\sqrt 2,+\infty[$, on en déduit que la limite de $f'$ en $\sqrt 2$ par valeurs supérieures est $\frac 12$.


		On pose $v_n=u_n-\sqrt 2$ et $w_n=\ln(v_{n+1})-\ln(v_n)$. La suite $w_n$ converge vers $-\ln(2)$. On en déduit que la série de terme général $w_n$ diverge et on peut donc donner un équivalent de la somme partielle : $\sum_{k=0}^N w_k\sim -\ln(2) N$. On en déduit que $\ln(v_N)-ln(v_0)\sim-\ln(2)N$. On a donc la décroissance linéaire observée : le chagement de comportement autour de $n=40$ est très probablement dû aux apporximations numériques.
