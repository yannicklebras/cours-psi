!!! exercice "RMS2017-1028"
	=== "Enoncé"
		Soient $\left(a_{n}\right)_{n \geq 1} \in\left(\mathbb{R}^{+}\right)^{\mathbf{N}^{*}}$ et $A_{n}=\left(\begin{array}{ccccc}1 & -1 & 0 & \cdots & 0 \\ a_{1} & 1 & -1 & \ddots & \vdots \\ 0 & \ddots & \ddots & \ddots & 0 \\ \vdots & \ddots & \ddots & \ddots & -1 \\ 0 & \ldots & 0 & a_{n} & 1\end{array}\right) \in \mathcal{M}_{n+1}(\mathbb{R})$ pour $n \in \mathbb{N}^{*}$.  
		- Écrire une fonction python calculant et affichant le déterminant $\Delta_{n}$ de $A_{n}$ pour les petites valeurs de $n$.  
		- Exécuter le programme lorsque $a_{n}=1$ et lorsque $a_{n}=1 / n^{2}$. Que peut-on dire ?  
		- Montrer que, lorsque $a_{n}=1$, la suite $\left(\Delta_{n}\right)_{n}$ est divergente. Donner un équivalent de $\Delta_{n}$ quand $n$ tend vers l'infini.  
		- Montrer que $\Delta_{n} \leq \prod_{k=1}^{n}\left(1+a_{k}\right)$  
		- Montrer que la suite $\left(\Delta_{n}\right)$ converge si et seulement si $\sum a_{n}$ converge.

	=== "Corrigé"

		On suit les recommandations du concours Centrale pour le calcul matriciel :


		```python
		from IPython.display import display,Latex

		import numpy as np
		import numpy.linalg as alg
		```

		On écrit une fonction qui créée une matrice du type demander lorsqu'on lui donne en entrée les $n$ valeurs $\left(a_{1}, a_{2}, \ldots, a_{n}\right)$.


		```python
		def matrice(L) :
		    n = len(L)
		    M = np.eye(n+1)
		    for i in range(n) :
		        M[i,i+1] = -1
		        M[i+1,i] = L[i]
		    return alg.det(M)
		```


		```python
		matrice ([1,2,3,4])
		```




		    25.99999999999999



		On commence par le cas où $a_i=1$.


		```python
		for i in range(1,30) :
		    display(Latex(r"$n=%d : \Delta_{%d}=%f$"%(i,i,matrice([1]*i))))
		```


		$n=1 : \Delta_{1}=2.000000$



		$n=2 : \Delta_{2}=3.000000$



		$n=3 : \Delta_{3}=5.000000$



		$n=4 : \Delta_{4}=8.000000$



		$n=5 : \Delta_{5}=13.000000$



		$n=6 : \Delta_{6}=21.000000$



		$n=7 : \Delta_{7}=34.000000$



		$n=8 : \Delta_{8}=55.000000$



		$n=9 : \Delta_{9}=89.000000$



		$n=10 : \Delta_{10}=144.000000$



		$n=11 : \Delta_{11}=233.000000$



		$n=12 : \Delta_{12}=377.000000$



		$n=13 : \Delta_{13}=610.000000$



		$n=14 : \Delta_{14}=987.000000$



		$n=15 : \Delta_{15}=1597.000000$



		$n=16 : \Delta_{16}=2584.000000$



		$n=17 : \Delta_{17}=4181.000000$



		$n=18 : \Delta_{18}=6765.000000$



		$n=19 : \Delta_{19}=10946.000000$



		$n=20 : \Delta_{20}=17711.000000$



		$n=21 : \Delta_{21}=28657.000000$



		$n=22 : \Delta_{22}=46368.000000$



		$n=23 : \Delta_{23}=75025.000000$



		$n=24 : \Delta_{24}=121393.000000$



		$n=25 : \Delta_{25}=196418.000000$



		$n=26 : \Delta_{26}=317811.000000$



		$n=27 : \Delta_{27}=514229.000000$



		$n=28 : \Delta_{28}=832040.000000$



		$n=29 : \Delta_{29}=1346269.000000$


		On passe au cas où $a_i=\frac 1{i^2}$.


		```python
		for i in range(1,30) :
		    display(Latex(r"$n=%d : \Delta_{%d}=%f$"%(i,i,matrice([1/n**2 for n in range(1,i)]))))
		```


		$n=1 : \Delta_{1}=1.000000$



		$n=2 : \Delta_{2}=2.000000$



		$n=3 : \Delta_{3}=2.250000$



		$n=4 : \Delta_{4}=2.472222$



		$n=5 : \Delta_{5}=2.612847$



		$n=6 : \Delta_{6}=2.711736$



		$n=7 : \Delta_{7}=2.784315$



		$n=8 : \Delta_{8}=2.839657$



		$n=9 : \Delta_{9}=2.883162$



		$n=10 : \Delta_{10}=2.918219$



		$n=11 : \Delta_{11}=2.947051$



		$n=12 : \Delta_{12}=2.971168$



		$n=13 : \Delta_{13}=2.991634$



		$n=14 : \Delta_{14}=3.009215$



		$n=15 : \Delta_{15}=3.024478$



		$n=16 : \Delta_{16}=3.037853$



		$n=17 : \Delta_{17}=3.049667$



		$n=18 : \Delta_{18}=3.060179$



		$n=19 : \Delta_{19}=3.069591$



		$n=20 : \Delta_{20}=3.078068$



		$n=21 : \Delta_{21}=3.085742$



		$n=22 : \Delta_{22}=3.092722$



		$n=23 : \Delta_{23}=3.099097$



		$n=24 : \Delta_{24}=3.104944$



		$n=25 : \Delta_{25}=3.110324$



		$n=26 : \Delta_{26}=3.115292$



		$n=27 : \Delta_{27}=3.119893$



		$n=28 : \Delta_{28}=3.124166$



		$n=29 : \Delta_{29}=3.128146$


		Dans le cas où $a_{n}=1$, on peut essayer d'établir une relation de récurrence d'ordre 2 probablement. On a en développant par rapport à la première ligne : $\Delta_{n}=\Delta_{n-1}+$ $\left|\begin{array}{ccccc}1 & -1 & 0 & \ldots & 0 \\ 0 & 1 & -1 & \ddots & \vdots \\ 0 & 1 & \ddots & \ddots & 0 \\ \vdots & \ddots & \ddots & \ddots & -1 \\ 0 & \ldots & 0 & 1 & 1\end{array}\right|=\Delta_{n-1}+\Delta_{n-2}$.

		Cette relation de récurrence admet pour polynôme caractéristique $X^{2}-X-1$ dont les racines sont $\frac{1+\sqrt{5}}{2}$ et $\frac{1-\sqrt{5}}{2}$. Avec $\Delta_{0}=1$ et $\Delta_{1}=2$, on en déduit que $\Delta_{n}=$ $\frac{1}{10}\left((5+3 \sqrt{5})\left(\frac{1+\sqrt{5}}{2}\right)^{n}-(5-3 \sqrt{5})\left(\frac{1-\sqrt{5}}{2}\right)^{n}\right)$.

		Ainsi, $\Delta_{n} \sim \frac{1}{10}(5+3 \sqrt{5})\left(\frac{1+\sqrt{5}}{2}\right)^{n}$ qui est le terme général d'une suite divergente (car $\left.\frac{1+\sqrt{5}}{2}>1\right)$
		Effectuons maintenant un développement par rapport à la dernière ligne :
		$\Delta_{n}=\Delta_{n-1}+a_{n} * \Delta_{n-2}$ En supposant qu'on ai fait un raisonnement par récurrence, par exemple à deux pas :
		$$
		\Delta_{n} \leq \prod_{k=1}^{n-1}\left(1+a_{k}\right)+a_{n} \prod_{k=1}^{n-2}\left(1+a_{k}\right)=\left(1+a_{n-1}+a_{n}\right) \prod_{k=1}^{n-2}\left(1+a_{k}\right)
		$$
		Or $1+a_{n-1}+a_{n} \leq\left(1+a_{n-1}\right)\left(1+a_{n}\right)$ car les $a_{k}$ sont positifs. On en déduit : $\Delta_{n} \leq \prod_{k=1}^{n}\left(1+a_{k}\right)$.
		Enfin, on montre facilement par récurrence grâce à la relation $\Delta_{n}=\Delta_{n-1}+a_{n} * \Delta_{n-2}$ que la suite des $\Delta_{n}$ est croissante strictement et positive. Elle converge donc si et seulement si elle est majorée.

		Supposons que la suite des $\Delta_{n}$ converge, alors d'après la relation $\Delta_{n}-\Delta_{n-1}=a_{n} \Delta_{n-2}$ nous permet de dire que la suite $\left(\Delta_{n}\right)$ et la série de terme général $a_{n} \Delta_{n-2}$ ont même nature. Or la suite $\Delta_{n}$ étant croissante strictement et positive, si elle converge c'est vers une limite $\ell>0$. On a donc $a_{n} \Delta_{n-2} \sim \ell a_{n}$ et par comparaison des séries à termes positifs, $\sum a_{n} \Delta_{n-2}$ et $\sum a_{n}$ ont même nature. FInalement, la suite $\left(\Delta_{n}\right)$ et la série $\sum a_{n}$ ont même nature.
