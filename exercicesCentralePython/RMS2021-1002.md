!!! exercice "RMS2021-1002"
    === "Énoncé"
        Soient $x_0< \ldots<x_{n-1}$ des réels distincts, $L_0, \ldots, L_{n-1}$ les polynômes de Lagrange associés.

        Soient $k \in[\![0, n-1]\!], A$ $=L_0+\cdots+L_k, P=A-\sum_{i \neq k} \frac{A^{\prime}\left(x_i\right)}{\Lambda_i^{\prime}\left(x_i\right)} \Lambda_i$ où, pour $i \in[\![0, n-1]\!], \Lambda_i=\left(X-x_i\right)\left(X-x_k\right) \prod_{j \neq i, j \neq k}\left(X-x_j\right)^2$.

        a) Écrire une fonction Python `Lambda(i,k,X)` où $X=\left(x_0, \ldots, x_{n-1}\right)$ et qui renvoie $\Lambda_i$.

        b) Vérifier que $P$ est bien défini. Calculer $P\left(x_j\right)$ pour tout $j \in[\![0, n-1]\!]$ et $P^{\prime}\left(x_j\right)$ pour tout $j \in[\![0, n-1]\!]$ distinct de $k$. Montrer que $P$ est l'unique polynôme de degré $2 n-2$ vérifiant ces conditions.

        c) En étudiant les racines de $P^{\prime}$ et les variations de $P$, montrer que, pour $t \leq x_k$, on a $P(t) \geq 1$.

    === "Corrigé"
        ```python
        from numpy.polynomial import Polynomial

        def Lambda(i,k,X) :
            n = len(X)
            l=Polynomial([1])
            Pi = Polynomial([-X[i],1])
            Pk = Polynomial([-X[k],1])
            l = l*Pi*Pk
            for j in range(n) :
                if j != i and j != k :
                    Pj = Polynomial([-X[j],1])*Polynomial([-X[j],1])
                    l = l*Pj
            return l

        Lambda(1,4,[1,2,3,4,5,6,9,13])
        ```




        $x \mapsto \text{7.0963776e+08} - \text{(3.24735091e+09)}\,x + \text{(6.46164151e+09)}\,x^{2} - \text{(7.43449007e+09)}\,x^{3} + \text{(5.55158025e+09)}\,x^{4} - \text{(2.85891951e+09)}\,x^{5} + \text{(1.05101418e+09)}\,x^{6} - \text{(2.81057872e+08)}\,x^{7} + \text{55070836.0}\,x^{8} - \text{7882982.0}\,x^{9} + \text{812666.0}\,x^{10} - \text{58580.0}\,x^{11} + \text{2794.0}\,x^{12} - \text{79.0}\,x^{13} + \text{1.0}\,x^{14}$



        Pour que $P$ soit bien défini, il suffit de s'assurer que $\Lambda_i'(x_i)\ne 0$ pour tout $i$. Or par construction de $\Lambda_i$, $x_i$ est racine simple de $\Lambda_i$. Donc $\Lambda'_i(x_i)\ne 0$ et donc $P$ est bien défini. 



        Si $j\le k$, alors $A(x_j)=1$. Et $x_j$ est racine de tous mes $\Lambda_i$ donc $P(x_j)=1$. Si $j>k$, alors $A(x_j)=0$ et on a toujours $\Lambda(x_j)=0$. Donc $P(x_j)=0$. 

        Passons à la dérivée. Tout d'abord, remarquons que pour $j\ne i$ et $j\ne k$, $x_j$ est racine double de $\Lambda_i$. Donc $\Lambda_i'(x_j)=0$. On en déduit pour $j\ne k$ :

        $$
        P'(x_j)=A'(x_j)-\sum_{i\ne k}\frac{A'(x_i)}{\Lambda'_i(x_i)}\Lambda'_i(x_j)=A'(x_j)-\frac{A'(x_j)}{\Lambda'_j(x_j}\lambda'_j(x_j)=0
        $$

        Ainsi, $P$ est un polynôme de degré $2n-2$ qui vérifie la propriété suivante : 

        $$
        \forall j\in\{0,\dots k\},\,P(x_j)=1,\quad \text{et}\quad \forall j\in\{k+1,\dots,n-1\},\,P(x_j)=0\quad\text{et}\quad\forall j\in\{0,\dots,n-1\}\setminus\{k\},\,P'(x_j)=0
        $$

        Soit $Q$ un polynôme vérifiant la même propriété. Alors $P-Q$ est de degré inférieur ou égal à $2n-2$ et pour tout $j\in\{0,\dots,n-1\}\setminus\{k\}$, $x_j$ est racine double de $P-Q$, et $x_k$ est racine simple. Ce qui donne, comptée avec leur multiplicité, $2n-1$ racines. $P-Q$ est de degré $\le 2n-2$ avec $2n-1$ racines : $P-Q=0$. 

        On a ainsi trouvé $n-1$ racines de $P'$ : les $x_j$ pour $j\ne k$. Par ailleurs on a $P(x_0)=P(x_1)=\dots=P(x_k)$ donc on peut appliquer $k$ fois le théorème de Rolle entre $x_i$ et $x_{i+1}$ pour obtenir $k$ racines $y_0,y_1,y_{k-1}$ de $P'$ supplémentaires vérifiant $x_0<y_0<x_1<\dots<y_{k-1}<x_k$. 

        De même, $P(x_{k+1})=P(x_{k+2})=\dots=P(x_{n-1})$ donc on peut appliquer $n-1-(k+1)$ fois le théorème de Rolles pour obtenir $n-1-(k+1)=n-k-2$ nouvelles racines de $P'$, $y_{k+1},y_{k+2},\dots,y_{n-2}$ telles que, en résumé :

        $$
        x_0<y_0<x_1<y_1<\dots<y_{k-1}<(x_k)<x_{k+1}<y_{k+1}<\dots<x_{n-2}<y_{n-2}<x_{n-1}
        $$

        ($x_k$ n'est pas racine). On a en tout exhibé $n-1+k+(n-1-k-2)=2n-3$ racines de $P'$ distinctes. Or $P'$ est exactement de degré $2n-3$ : on a donc toutes les racines de $P'$ et on peut affirmer que $P'$ est scindé à racines simples. 

        Ainsi, chaque racine de $P'$ correspond à un changement de signe de $P'$ et donc un changement de variation de $P$. Or entre $x_k$ et $x_{k+1}$, $P$ est strictement décroissant. Donc $P$ est décroissant entre $y_{k-1}$ et $x_k$ ($x_k$ n'est pas racine de $P'$ donc ne marque pas de changement de variation) puis croissant entre $x_{k-1}$ et $y_{k-1}$, et plus généralement pour $i<k$, croissant entre $x_i$ et $y_i$ et décroissante entre $y_i$ et $x_{i+1}$. Or pour $i\le k$, $P(x_i)=1$ donc pour tout $t\le x_k$, $P(t)\ge 1$. 
