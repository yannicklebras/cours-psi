!!! exercice "RMS2022-1136"
    === "Énoncé"
        Pour $a,b$ dans $\mathbb C$, on pose $D_1(a,b)=a$ et, pour tout $n \geq 2$,


        $$
        D_n(a,b)=\small\begin{vmatrix}
        a & 2b & 0 & & (0) \\
        1 & a & b & & \\
        0 & \ddots & \ddots & \ddots & 0 \\
        & & & & b \\
        (0) & & & 1 & a
        \end{vmatrix}\quad \text{et} \quad
        A_n(b)=\small\begin{bmatrix}
        0 & -2b & 0 & & (0) \\
        -1 & 0 & -b & & \\
        0 & \ddots & \ddots & \ddots & 0 \\
        & & & & -b \\
        (0) & & & -1 & 0
        \end{bmatrix}.
        $$

        a) Calculer $D_n(a,b)$ pour $n \leq 3$.

        b) Donner une relation de récurrence linéaire reliant $D_n(a,b)$, $D_{n+1}(a,b)$
        et $D_{n+2}(a,b)$.

        c) Coder en Python une fonction `D1(n,a,b)` renvoyant $D_n(a,b)$.

        d) Soit $n \in \mathbb N^*$ et $b \in \mathbb R$. Montrer que $a \mapsto D_n(a,b)$
        est polynomiale à coefficients réels, et donner son degré.

        e) Pour $b \in [\![ 1,n]\!]$ et $n \in [\![ 3,5]\!]$,
        donner une représentation graphique de $a \mapsto D_n(a,b)$ sur $[-2\sqrt{b},2\sqrt{b}]$.
        Conjecturer le nombre et la localisation des zéros de $a \mapsto D_n(a,b)$.

        f) Supposant vraie la conjecture de la question précédente, que peut-on dire de la réduction de la matrice $A_n(b)$ ?

        g)  Soit  $(T_n)_{n \in \mathbb N}$  la suite de polynômes  définie par $T_0=1$, $T_1=X$
        et $ T_{n+2}=2X T_{n+1}-T_n$  pour $n\geq 0$.
        Pour $\theta \in \mathbb R$, donner une expression simple des termes de la suite $(T_n(\cos \theta))_n$.

        h) Calculer les racines du polynôme $T_n$ pour tout $n \in \mathbb N$.

        i) Déterminer alors les zéros de $a \mapsto D_n(a,b)$ en fonction du nombre complexe $b$.

    === "Corrigé"

        a) $D_3(a,b)=\begin{vmatrix}a & 2b & 0\\1 & a  & b\\0 & 1 & a\end{vmatrix}=a\begin{vmatrix} a & b \\ 1 & a\end{vmatrix}-\begin{vmatrix}2b & 0 \\ 1 & a\end{vmatrix}=a(a^2-b)-2ab=a^3-3ab.$

        On développe par rapport à la dernière colonne  puis par rapport à la dernière ligne :

        $$
        D_{n+2}(a,b)=aD_{n+1}(a,b)-b\begin{vmatrix}
        a & 2b & 0 & & 0 \\
        1 & a & b & & \\
        0 & \ddots & \ddots & \ddots & 0 \\
        & & & a & b \\
        (0) & & & 0 & 1 
        \end{vmatrix}=aD_{n+1}(a,b)-bD_n(a,b)
        $$


        ```python
        def D1(n,a,b) :
            L=[a,a*a-2*b]
            if n==1 :
                return a
            for i in range(2,n) :
                L.append(a*L[-1]-b*L[-2])
            return L[-1]
        ```


        ```python
        D1(3,4,5)
        ```




            4



        d) Montrons cela par récurrence : soit $k\in\mathbb N^*$, $H(k)=$«$a\mapsto D_n(a,b)$ est un polynôme en $a$ à coefficients réels de degré $n$»  

        On a $H(1)$ et $H(2)$ qui sont bien vérifiées.

        Soit $k\in\mathbb N^*$, supposons que $H(k)$ et $H(k+1)$ sont vérifiées. Alors $D_{n+2}(a,b)=aD_{n+1}(a,b)-bD_n(a,b)$. Or par hypothèse de récurrence $D_{n+1}(a,b)$ est un polynôme réel en $a$ de degré $n$ et $D_n(a,b)$ aussi. Donc $aD_{n+1}(a,b)-bD_n(a,b)$ est un polynôme réel en $a$ de degré $n+2$. 




        ```python
        from matplotlib import pyplot as plt
        import numpy as np

        for (b,n) in [(1,3),(2,3),(3,3),(1,4),(2,4),(3,4),(4,4),(1,5),(2,5),(3,5),(4,5),(5,5)] :
            xr = np.arange(-2*np.sqrt(b),2*np.sqrt(b),0.01)
            yr = [D1(n,a,b) for a in xr]
            plt.figure()
            plt.plot(xr,yr,label="b="+str(b)+",n="+str(n))
            plt.legend()
            plt.show()
        ```


            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_0.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_1.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_2.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_3.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_4.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_5.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_6.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_7.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_8.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_9.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_10.png)
            



            
        ![png](CentralePython/RMS2022-1136_files/RMS2022-1136_6_11.png)
            


        Il semblerait qu'il y ait $n$ racines à chaque fois. Les racines sont situées entre $-2\sqrt b$ et $2\sqrt b$. 

        Le polynôme caractéristique de $A_n(b)$ est exactement $a\mapsto D_n(a,b)$. Si la conjecture est OK alors le polynôme caractéristique est scindé à racinces simple et $A_n(b)$ est diagonalisable.

        On a $T_0(\cos(\theta))=1$, $T_1(\cos\theta)=\cos\theta)$, $T_2(\cos\theta)=2\cos\theta\cos\theta-1=\cos2\theta$. On montre facilement par récurrence que $T_n(\cos\theta)=\cos(n\theta)$. Ce sont des polynômes bien connus. 

        $T_n$ est de degré $n$. On cherche donc $n$ racines qui sont de la forme $\cos(\theta)$ avec $\theta$ tel que $\cos(n\theta)=0$. Les racines sont donc les $\cos\left(\frac{(2k+1)\pi}{2n}\right)$ avec $k\in\{0,n-1\}$.



        Posons $P_n=(\sqrt b)^n T_n(\frac X{2\sqrt b})$. On a alors :

        $$
        \begin{aligned}
        P_{n+2}&=(\sqrt b)^{n+2}T_{n+2}(\frac X{2\sqrt b})\\
        &=(\sqrt b)^{n+2}\left(\frac X{\sqrt b}T_{n+1}(\frac X{2\sqrt b})-T_n(\frac X{2\sqrt b})\right)\\
        &=X(\sqrt b)^{n+1}T_{n+1}(\frac X{2\sqrt b})-b(\sqrt b)^nT_n(\frac X{2\sqrt b})
        \end{aligned}
        $$

        On en déduit que $P_n$ vérifie la même relation de récurrence que $D_n$. On a de plus $P_1(a)=\sqrt b\frac a{2\sqrt b}\frac a2=\frac 12D_1(a,b)$ et $P_2(a)=b(2\frac{a^2}{4b}-1)=\frac {a^2}{2}-b=\frac 12D_2(a,b)$. On en déduit que $D_n(a,b)=2P_n(a)$. On en déduit que $a\mapsto D_n(a,b)$ a les mêmes racines que $P_n$ qui sont de la forme $2\sqrt b\cos\frac{(2k+1)\pi}{2n}$. 
