<h1>Cahier de vacances pour la PSI</h1>

Chers futurs élèves de PSI*,

Je me présente, Yannick Le Bras, je serai votre enseignant en
 mathématiques l'année prochaine. L'année de PSI sera intense, il y a peu
de temps et beaucoup de choses à apprendre. Le plus important est de vous
créer une culture. Cette culture se forge en travaillant les exercices,
classiques et moins classiques.

J'ai compilé dans ce document des exercices tombés au concours. Ces exercices
sont "accessibles" à la fin de la première année. J'ai tenté de regrouper
des exercices autour des thématiques qui seront utiles l'année prochaine.

Vous trouverez aussi des exercices avec utilisation du langage Python. Ces
exercices sont issus des oraux de Centrale Mathématiques/Informatique. Ils
vous permettront de travailler en même temps vos compétences en Python. Afin
de tavailler ces exercices, vous devrez vous appuyer sur les documents qui sont
 en ligne sur le site de Centrale :
[Documents Python](https://www.concours-centrale-supelec.fr/CentraleSupelec/SujetsOral/PSI)

Enfin, certains exercices sont identifiés (en rose) de façon particulière : ces exercices
feront partie de notre banque d'exercices de colles. À chaque colle de mathématiques,
c'est à dire toutes les deux semaines, vous vous verrez proposer un exercice
issu de notre banque et un exercice original proposé par le colleur. Les
exercices de la banque seront connus, fournis avec les corrigés et augmenteront
chaque semaine. C'est le principe de l'oral de CCP en filière MP (qui est plus
ou moins valable en filière PSI, mais ce n'est pas officiel). Cette banque
couvrira l'ensemble des notions du programme de PSI et aura pour but d'aborder
essentiellement les théorèmes principaux et les méthodes classiques. Les Corrigés
proposés n'ont pas la prétention d'être parfaits, sachez les adapter à votre compréhension.


Ainsi, dans ce cahier de vacances sont identifiés 25 exercices (en rose donc), dont fera partie
votre exercice imposé lors de votre première colle de mathématiques. Je vous
conseille évidemment fortement de travailler ces exercices pendant les vacances.
Ce sont des exercices que vous devrez savoir faire.

Enfin, vous trouverez en fin de documents 3 sujets d'écrits que vous pouvez traiter en DM pendant les vacances. Si vous me les transmettez avant la date indiquée (version scannée), je corrigerai et vous enverrai ma correction. J'impose des dates progressives afin de ne pas être submergé au dernier moment. Vous pourrez me transmettre vos copies par mail, ou bien par la plateforme ci dessous.

Afin de vous aider et de vous guider pendant tout cet été, vous pourrez échanger
entre vous, et avec moi sur la plateforme suivante : [https://prepapsi.ylb-maths.fr/signup_user_complete/?id=8empk3wnp7gkdpxubi4s6pifdw&sbr=fa](https://prepapsi.ylb-maths.fr/signup_user_complete/?id=8empk3wnp7gkdpxubi4s6pifdw&sbr=fa)

Rien de tout ce que contient ce cahier de vacances n'est obligatoire (à part les exercices roses qui seront au programme de colle). Tout est donné pour vous aider à vous préparer à cette année de PSI*.

Je suis impatient de vous retrouver à la rentrée, je vous souhaite de
belles vacances,

Yannick Le Bras
