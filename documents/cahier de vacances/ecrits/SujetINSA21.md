#### Exercice (10 points)

À l'exception de la question 1.c), on ne demande pas de justification ou détail de calcul dans cet exercice. Seules les conclusions seront prises en compte.

On étudie dans cet exercice la fonction $f$ définie par

$$
f(x)=\frac{-x^{3}-\frac{3}{2} x^{2}+\frac{3}{2} x+1}{2 x^{2}-2 x-12} .
$$

**1\. a)** Donner l'ensemble de définition $D_{f}$ de la fonction $f$.

**b)** Factoriser le numérateur et le dénominateur de $f$.

**c)** La fonction $f$ est-elle prolongeable par continuité en ses valeurs interdites?

Justifier brièvement la réponse pour chaque point.

**2\.** On note toujours la fonction prolongée $f$. On admettra si besoin que, pour tout $x \neq 3$,

$$
f(x)=\frac{-2 x^{2}+x+1}{4 x-12}
$$

**a)** Dériver la fonction $f$.

**b)** Factoriser $f^{\prime}(x)$.

**c)** Dresser le tableau de variations de $f$, sans afficher les valeurs aux points critiques.

**3\. a)** Effectuer la division euclidienne du polynôme $P(X)=-2 X^{2}+X+1$ par $Q(X)=4 X-12$.

**b)** En déduire l'équation de l'asymptote oblique à la courbe représentative de $f$ en l'infini.

**4\.** Tracer l'allure de la courbe dans un repère orthonormé.

La figure doit être soignée, de taille raisonnable et repassée au stylo.

Valeurs approchées utiles: $\sqrt{7} \approx 2,6$; la fonction $f$ possède un minimum local $\alpha$ avec $f(\alpha) \approx-0,1$, et un maximum local $\beta$ avec $f(\beta) \approx-5,4$.

*Fin de l'exercice.*

#### Problème (30 points)

On s'intéresse dans ce problème aux applications $f$ bijectives d'un ensemble $A$ dans lui-même telles que

$$
\forall X \in A, \quad(f \circ f)(X)=f(f(X))=-X .
$$

On résoudra ce problème en prenant, successivement,

- $A=\mathbb{R}$ dans la partie $\mathbf{A}$, et on cherchera $f$ linéaire puis continue,
- $A=\mathbb{C}$ dans la partie $\mathbf{B}$, et on cherchera $f$ linéaire,
- et $A=\mathbb{R}^{2}$ dans la partie $\mathbf{C}$, et on cherchera $f$ linéaire, ce qui nous ramènera à un problème de matrices $2 \times 2$.

Les parties $\boldsymbol{A}, \boldsymbol{B}$ et $\boldsymbol{C}$ sont indépendantes entre elles et peuvent être traitées dans n'importe quel ordre.

Enfin, la partie $\mathbf{D}$ reliera les parties $\mathbf{B}$ et $\mathbf{C}$.

##### Partie A. Fonctions de $\mathbb{R}$ dans $\mathbb{R}$

Dans cette partie, les questions 1 et 2 sont indépendantes.

L'objectif de cette partie est de montrer qu'il n'existe aucune fonction de $\mathbb{R}$ dans $\mathbb{R}$, bijective et continue qui vérifie

$$
\forall x \in \mathbb{R}, \quad(f \circ f)(x)=f(f(x))=-x .
$$

**1\.** Le cas des fonctions linéaires.

Soit $a \in \mathbb{R}$ fixé, et $f_{a}$ la fonction définie par $f_{a}(x)=a x$.

**a)** Soit $y \in \mathbb{R}$ fixé. Résoudre l'équation $y=f_{a}(x)$.

À quelle condition sur $a$ la fonction $f_{a}$ est-elle bijective?

Exprimer alors la fonction réciproque $f_{a}^{-1}$.

**b)** Calculer $\left(f_{a} \circ f_{a}\right)(x)=f_{a}\left(f_{a}(x)\right)$.

À quelle condition sur $a$ a-t-on $\left(f_{a} \circ f_{a}\right)(x)=-x$ ?

Conclure qu'il n'existe aucune fonction linéaire de $\mathbb{R}$ dans $\mathbb{R}$ qui satisfait (2).

**2\.** Le cas continu général.

Supposons, pour faire un raisonnement par l'absurde, qu'il existe une fonction $f: \mathbb{R} \rightarrow \mathbb{R}$ bijective et continue qui vérifie (2).

On note $f^{-1}$ la réciproque de $f:$ pour tout $y \in \mathbb{R}$, il existe un unique $x \in \mathbb{R}$ tel que

$$
y=f(x) \Longleftrightarrow x=f^{-1}(y) .
$$

La fonction réciproque vérifie:

$$
\forall x \in \mathbb{R},\left(f^{-1} \circ f\right)(x)=x \text { et } \forall y \in \mathbb{R},\left(f \circ f^{-1}\right)(y)=y .
$$

**a)** Montrer que, dans ces conditions, pour tout $y \in \mathbb{R}, f^{-1}(y)=-f(y)$.

Indication: injecter (3) dans l'égalité (2).

**b)** Justifier que la fonction $f$ est strictement monotone sur $\mathbb{R}$.

**c)** Déterminer le sens de variation des fonctions $f^{-1}$ et $-f$ par rapport à celui de $f$.

**d)** Conclure la partie.

##### Partie B. Fonctions linéaires complexes

Dans cette partie, nous allons montrer qu'il existe des applications $f: \mathbb{C} \rightarrow \mathbb{C}$ qui vérifient

$$
\forall z \in \mathbb{C}, \quad(f \circ f)(z)=f(f(z))=-z .
$$

Nous allons, en plus, chercher leur interprétation en termes de transformations géométriques du plan complexe (homothéties, symétries, rotations, translations).

**1\.** Si $g$ est une fonction complexe telle que, pour tout $z \in \mathbb{C}, g(z)=-z$, quel est l'effet géométrique de $g$ sur le plan complexe?

**2\.** Soit $f_{\alpha}: \mathbb{C} \rightarrow \mathbb{C}$ définie par $f_{\alpha}(z)=\alpha z$, avec $\alpha \in \mathbb{C}$.

**a)** Calculer $\left(f_{\alpha} \circ f_{\alpha}\right)(z)$.

**b)** En déduire les valeurs possibles de $\alpha$ pour que $f_{\alpha}$ soit solution du problème (4).

**c)** Écrire les valeurs possibles de $\alpha$ sous forme polaire (ou exponentielle).

**d)** Écrire $f_{\alpha}(z)$ avec cette notation pour identifier l'action géométrique des solutions $f_{\alpha}$ sur le plan complexe.

##### Partie C. Matrices $2 \times 2$

Une application linéaire $f$ de $\mathbb{R}^{2}$ dans $\mathbb{R}^{2}$ est représentée par une matrice $M \in \mathcal{M}_{2}(\mathbb{R})$. Si l'application $f$ vérifie (1), c'est-à-dire, pour tout $(x, y) \in \mathbb{R}^{2}$,

$$
(f \circ f)(x, y)=-(x, y)=(-x,-y)
$$

alors $f \circ f=-\mathrm{id}$, où id est l'application identité, et la matrice $M$ vérifie $M^{2}=-I_{2}$, où $I_{2}$ désigne la matrice identité, soit

$$
M^{2}=-I_{2}=\left(\begin{array}{cc}
-1 & 0 \\
0 & -1
\end{array}\right)
$$

L'objectif de la partie est donc de trouver toutes les matrices qui vérifient cette propriété.

**1\.** Calculer $A^{2}$ et $B^{2}$, où $A$ et $B$ sont les matrices

$$
A=\left(\begin{array}{cc}
0 & -1 \\
1 & 0
\end{array}\right) \quad \text { et } B=\left(\begin{array}{cc}
3 & 5 \\
-2 & -3
\end{array}\right) \text {. }
$$

Pour cette question seulement, on exige de montrer le détail des calculs des coefficients.

**2\.** Soient $(b, c) \in \mathbb{R}^{2}$ et $C$ une matrice de la forme

$$
C=\left(\begin{array}{ll}
0 & b \\
c & 0
\end{array}\right)
$$

**a)** Calculer $C^{2}$.

**b)** En déduire les matrices de la forme de $C$ qui vérifient $C^{2}=-I_{2}$.

**3\.** Soient $(a, d) \in \mathbb{R}^{2}$ et $D$ une matrice diagonale:

$$
D=\left(\begin{array}{ll}
a & 0 \\
0 & d
\end{array}\right)
$$

**a)** Calculer $D^{2}$.

**b)** Montrer qu'il n'existe pas de matrice diagonale à coefficients réels telle que $D^{2}=-I_{2}$.

**c)** En déduire que si $M=\left(\begin{array}{ll}a & b \\ c & d\end{array}\right)$ telle que $M^{2}=-I_{2}$, nécessairement $b \neq 0$ ou $c \neq 0$.

**4\.** Désormais, $M$ désigne un élément de $\mathcal{M}_{2}(\mathbb{R})$ générique: $M=\left(\begin{array}{ll}a & b \\ c & d\end{array}\right)$.

Nous allons résoudre l'équation $M^{2}=-I_{2}$ en utilisant l'inverse de la matrice $M$.

Rappels: une matrice $M$ est inversible s'il existe une matrice $M^{-1}$ telle que $M M^{-1}=I_{2}$.

De plus, une matrice est non inversible si et seulement s'il existe $X=\left(\begin{array}{l}x \\ y\end{array}\right) \neq\left(\begin{array}{l}0 \\ 0\end{array}\right)$ tel que $M X=\left(\begin{array}{l}0 \\ 0\end{array}\right)$

**a)** En utilisant les rappels, montrer que si $M$ est non inversible, alors $M^{2}$ est aussi non inversible.

Conclure que si $M^{2}=-I_{2}, M$ doit être inversible.

**b)** En déduire que la propriété (5) équivaut à

$$
M^{-1}=-M
$$

**c)** Montrer que l'inverse de la matrice $M$ est donnée par

$$
M^{-1}=\frac{1}{a d-b c}\left(\begin{array}{cc}
d & -b \\
-c & a
\end{array}\right) .
$$

Déduire de l'égalité (6) un système sur les coefficients $(a, b, c, d)$.

**d)** En s'aidant de la question 3, déterminer la valeur de $a d-b c$.

**e)** En déduire les deux relations entre les coefficients $(a, b, c, d)$, et conclure: quelle est la forme des matrices qui vérifient $M^{2}=-I_{2}$ ?

##### Partie D. $\mathbb{R}$-linéaire vs $\mathbb{C}$-linéaire

On peut identifier le plan complexe avec le plan $\mathbb{R}^{2}$ par la bijection suivante

$$
\varphi: z=a+i b \in \mathbb{C} \longmapsto(a, b) \in \mathbb{R}^{2} .
$$

Dans les parties $\mathbf{B}$ et $\mathbf{C}$, nous avons cherché des solutions linéaires au problème (1) sur $\mathbb{C}$ et sur $\mathbb{R}^{2}$, avec des résultats différents, malgré cette identification qui est possible. On se donne pour objectif final dans ce problème de comprendre cette différence.

**1\.** Soit $f: \mathbb{C} \rightarrow \mathbb{C}$ définie par $f_{i}(z)=i z$, une des solutions du problème de la partie $\mathbf{B}$.

On lui associe une fonction $g_{i}: \mathbb{R}^{2} \rightarrow \mathbb{R}^{2}$ définie par

$$
g_{i}(x, y)=\left(\varphi \circ f_{i}\right)(x+i y)
$$

**a)** Exprimer $g_{i}(x, y)$.

L'application $g_{i}$ est-elle linéaire?

**b)** Calculer $g_{i}(1,0)$ et $g_{i}(0,1)$.

En déduire la matrice associée à $g_{i}$ dans la base canonique de $\mathbb{R}^{2}$.

**c)** Reprendre cette question avec l'autre solution de la partie $\mathbf{B}, f_{-i}(z)=-i z$.

**2\.** Dans l'autre sens, considérons, pour $b \in \mathbb{R}^{*}$ avec $|b| \neq 1$, la matrice $C=\left(\begin{array}{cc}0 & b \\ \frac{-1}{b} & 0\end{array}\right)$, qui est solution du problème de la partie $\mathbf{C}$.

**a)** Soit $X=\left(\begin{array}{l}x \\ y\end{array}\right)$. Calculer $C X$.

En déduire l'expression de l'application linéaire $g_{C}(x, y)$ représentée par $C$.

**b)** L'application $\varphi^{-1}$ est la réciproque de $\varphi$, associant un vecteur de $\mathbb{R}^{2}$ à son affixe complexe. Exprimer $\varphi^{-1}(x, y)$.

**c)** Soit la fonction $f_{C}=\varphi^{-1} \circ g_{C} \circ \varphi: \mathbb{C} \rightarrow \mathbb{C}$.

Exprimer $f_{C}(x+i y)$.

**d)** L'application $f_{C}$ est-elle $\mathbb{R}$-linéaire?

Est-elle $\mathbb{C}$-linéaire?

Indication: comparer $f_{C}(x+i y)$ et $f_{C}(i(x+i y))$.

**3\.** Expliquer la différence entre le nombre de solutions du problème dans la partie $\mathbf{B}$ et celui de la partie $\mathbf{C}$.

**FIN DU SUJET**
