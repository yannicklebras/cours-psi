#### Exercice 1

Une particule se trouve au point d'abscisse $a$ ( $a$ entier) sur un segment gradué de 0 à $N$ (on suppose donc $0 \leqslant a \leqslant N)$. À chaque instant, elle fait un bond de +1 avec la probabilité $p(0<p<1, p \neq 1 / 2)$ ou un bond de -1 avec la probabilité $q=1-p$. Autrement dit, si $x_{n}$ est l'abscisse de la particule à l'instant $n$, on a :

$$
x_{n+1}= \begin{cases}x_{n}+1 & \text { avec probabilité } p \\ x_{n}-1 & \text { avec probabilité } 1-p\end{cases}
$$

Le processus se termine lorsque la particule atteint l'une des extrémités du segment (i.e. s'il existe $n$ avec $x_{n}=0$ ou $\left.x_{n}=N\right)$. On cherche à déterminer la probabilité que le processus soit sans fin.

**1\.** On note $u_{a}$ la probabilité pour que la particule partant de $a$, le processus s'arrête en 0 .

**(a)** Que valent $u_{0} ? u_{N}$ ?

**(b)** Montrer que, si $0<a<N$, alors $u_{a}=p u_{a+1}+q u_{a-1}$.

**(c)** En déduire l'expression exacte de $u_{a}$.

**2\.** On note $v_{a}$ la probabilité pour que la particule partant de $a$, le processus s'arrête en $N$. Reprendre les questions précédentes avec $v_{a}$ au lieu de $u_{a}$ (attention, ce n'est peut-être plus la même formule de récurrence).

**3\.** Calculer $u_{a}+v_{a}$. Qu'en déduisez-vous?

#### Exercice 2

Dans tout cet exercice, $E$ est un $\mathbb{R}$-espace vectoriel de dimension finie.

On note $I$ l'application identique de $E\left(I=I d_{E}\right)$. Pour tout endomorphisme $g$ de $E$ et tout entier naturel $n$, on définit $g^{n}$ par: $\left\{\begin{array}{l}g^{0}=I \\ \forall n \in \mathbb{N}, g^{n+1}=g \circ g^{n}\end{array}\right.$

Dans toute cette première partie, on note $f$ un endomorphisme de $E$ vérifiant la relation

$$
f^{2}=\frac{1}{2}(f+I)
$$

**1\.** Dans cette question, on suppose que $f$ est une homothétie (c'est-à-dire qu'il existe un réel $\alpha$ tel que $f=\alpha I$ ). Préciser les valeurs possibles de $\alpha$.

On suppose désormais que les endomorphismes $f$ et $I$ ne sont pas colinéaires.

**2\.** Exprimer $f^{3}$ et $f^{4}$ comme combinaisons linéaires de $f$ et $I$.

Montrer que, pour tout entier naturel $n$, il existe des réels $a_{n}$ et $b_{n}$ tels que $f^{n}=a_{n} f+b_{n} I$. Préciser $a_{0}, b_{0}, a_{1}, b_{1}$ et donner une relation permettant de calculer $a_{n+1}$ et $b_{n+1}$ connaissant $a_{n}$ et $b_{n}$.

Pour un $n$ donné, y a-t-il unicité du couple de réels $\left(a_{n}, b_{n}\right)$ tel que $f^{n}=a_{n} f+b_{n} I$ ?

**3\.** Donner une relation entre $a_{n}, a_{n+1}$ et $a_{n+2}$.

En déduire l'expression de $a_{n}$ puis de $b_{n}$ en fonction de $n$.

Déterminer les nombres $\alpha=\lim _{n \rightarrow+\infty} a_{n}$ et $\beta=\lim _{n \rightarrow+\infty} b_{n}$.

**4\.** Soit l'endomorphisme $p=\alpha f+\beta I$. Vérifier la relation $p^{2}=p$.

**5\.** Montrer que $f$ est un automorphisme de $E$ et exprimer sa bijection réciproque $f^{-1}$ comme combinaison linéaire de $f$ et $I$. 6. Soient les sous-espaces $F=\operatorname{Ker}(f-I)$ et $G=\operatorname{Ker}\left(f+\frac{1}{2} I\right)$. Le but de cette question est de montrer qu'ils sont supplémentaires dans $E$.

Soit $x \in E$. On suppose que $x=y+z$ avec $y \in F$ et $z \in G$.

**(a)** Exprimer $f(x)$ à l'aide de $y$ et $z$.

**(b)** En déduire $y$ et $z$ en fonction de $x$ et $f(x)$.

**(c)** En conclure que $E=F \oplus G$

#### Exercice 3

On étudie dans cet exercice la suite $\left(S_{n}\right)_{n \in \mathbb{N}^{*}}$ définie par: $\forall n \geqslant 1, S_{n}=\sum_{k=1}^{n} \frac{1}{k^{2}} ;$ on considère, pour tout nombre entier $p \geqslant 0$, les deux intégrales suivantes : $I_{p}=\int_{0}^{\frac{\pi}{2}} \cos ^{2 p}(t) \mathrm{d} t$ et $J_{p}=\int_{0}^{\frac{\pi}{2}} t^{2} \cos ^{2 p}(t) \mathrm{d} t$.

**1\.** Convergence de la suite $\left(\frac{I_{p}}{J_{p}}\right)_{p \geqslant 0}$.

**(a)** Établir l'inégalité suivante pour tout nombre réel $t \in\left[0, \frac{\pi}{2}\right], t \leqslant \frac{\pi}{2} \sin t$.

**(b)** Établir l'inégalité suivante pour tout entier $p \geqslant 0,0 \leqslant J_{p} \leqslant \frac{\pi^{2}}{4}\left(I_{p}-I_{p+1}\right)$.

**(c)** Exprimer $I_{p+1}$ en fonction de $I_{p}$ et de $p$ en intégrant par parties l'intégrale $I_{p+1}$ pour $p \geqslant 1$ (on pourra poser $u^{\prime}(t)=\cos (t)$ et $v(t)=\cos ^{2 p+1}(t)$ dans l'intégration par parties)0.

**(d)** Déduire des résultats précédents que $\frac{J_{p}}{I_{p}} \underset{p \rightarrow \infty}{\longrightarrow} 0$.

**2\.** Convergence et limite de la suite $\left(S_{n}\right)_{n \geqslant 1}$.

**(a)** Exprimer $I_{p}$ en fonction de $J_{p}$ et $J_{p-1}$ en intégrant deux fois par parties l'intégrale $I_{p}$ pour $p \geqslant 1$.

**(b)** En déduire la relation suivante pour $p \geqslant 1, \frac{J_{p-1}}{I_{p-1}}-\frac{J_{p}}{I_{p}}=\frac{1}{2 p^{2}}$.

**(c)** Calculer $J_{0}$ et $I_{0}$ puis déterminer la limite de la suite $\left(S_{n}\right)_{n \geqslant 1}$.

**3\.** Accélération de la convergence.

On pose, pour tout $n \geqslant 1, S_{n}^{\prime}=S_{n}+\frac{1}{n}$.

**(a)** Montrer que la suite $\left(S_{n}\right)_{n \geqslant 1}$ est croissante et que la suite $\left(S_{n}^{\prime}\right)_{n \geqslant 1}$ est décroissante: en déduire que: $\forall n \geqslant 1, S_{n} \leqslant \frac{\pi^{2}}{6} \leqslant S_{n}^{\prime}$

**(b)** Montrer que $\forall k \geqslant 1, \frac{1}{k^{2}} \geqslant \frac{1}{k}-\frac{1}{k+1}$. En déduire que:

$$
\forall n \geqslant 1, \forall m>n, \quad \sum_{k=n+1}^{m} \frac{1}{k^{2}} \geqslant \frac{1}{n+1}-\frac{1}{m+1}
$$

**(c)** Soit $n \in \mathbb{N}^{*}$ fixé; montrer que $\frac{1}{n+1} \leqslant \frac{\pi^{2}}{6}-S_{n} \leqslant \frac{1}{n}$ et que $0 \leqslant S_{n}^{\prime}-\frac{\pi^{2}}{6} \leqslant \frac{1}{n^{2}}$.

**(d)** Application : quelle valeur faut-il donner à $n$ pour que $S_{n}$ soit une approximation par défaut de $\frac{\pi^{2}}{6}$ à $10^{-3}$ près et quelle valeur suffit-il donner à $n$ pour que $S_{n}^{\prime}$ soit une approximation par excès de $\frac{\pi^{2}}{6}$ à la même précision?
