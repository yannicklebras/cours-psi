Dans tout le problème, $E$ désigne le $\mathbb{R}$-espace vectoriel $\mathbb{R}[X]$ des polynômes à coefficients réels.

Pour tout entier naturel $n$, on note $E_{n}$ le sous-espace de $E$ formé par les polynômes de degré au plus égal à $n$.

Selon l'usage, on convient d'identifier un polynôme et la fonction polynomiale associée.

L'espace $E_{n}$ est muni de sa base canonique $\mathscr{B}_{n}=\left(1, X, X^{2}, \ldots, X^{n}\right)$.

Les coefficients binomiaux sont notés $\left(\begin{array}{l}n \\ k\end{array}\right)=\frac{n !}{k !(n-k) !}(0 \leqslant k \leqslant n)$.

#### Partie A : Étude d'un endomorphisme

Étant donné un polynôme $P$ de $E$, on définit un polynôme $\phi(P)$ par :

$$
\phi(P)(X)=\left(X^{2}-1\right) P^{\prime \prime}(X)+2 X P^{\prime}(X) .
$$

**1)** Justifier qu'on a ainsi défini un endomorphisme $\phi$ de $E$.

**2)** Montrer que, pour tout entier naturel $n$, le sous-espace vectoriel $E_{n}$ est stable par $\phi$.

On notera désormais $\varphi_{n}$ l'endomorphisme de $E_{n}$ induit par $\phi$ sur $E_{n}$ :

$$
\forall P \in E_{n}, \varphi_{n}(P)=\phi(P)
$$

**3)** Dans cette question, on suppose que $n$ est égal à 3 .

**a)** Écrire la matrice $M_{3}$ de $\varphi_{3}$ dans la base canonique de $E_{3}$.

**b)** (hors programme) Justifier que $\varphi_{3}$ est diagonalisable.

**c)** (hors programme) Déterminer une base de $E_{3}$ diagonalisant $\varphi_{3}$, formée de polynômes de coefficients dominants égaux à 1.

**4)** On revient au cas général d'un entier naturel $n$ quelconque.

**a)** Montrer que la matrice $M_{n}$ de $\varphi_{n}$ dans la base canonique est triangulaire supérieure et préciser ses coefficients diagonaux.

**b)** (hors programme) En déduire que $\varphi_{n}$ est diagonalisable et préciser les dimensions de ses sous-espaces propres.

#### Partie B : Étude d'une famille de polynômes

Pour tout entier naturel $n$, on définit le polynôme $L_{n}$ par

$$
L_{n}(X)=\frac{1}{2^{n}} \sum_{k=0}^{n}\left(\begin{array}{l}
n \\
k
\end{array}\right)^{2}(X-1)^{n-k}(X+1)^{k} .
$$

**1)** Calculer sous forme simplifiée les polynômes $L_{0}, L_{1}, L_{2}$ et $L_{3}$.

**2)** Calculer $L_{n}(1)$ pour tout $n \in \mathbb{N}$.

**3)** Déterminer le degré de $L_{n}$ en fonction de $n(n \in \mathbb{N})$ et donner son coefficient dominant sous la forme d'une somme.

**4)** En utilisant un changement d'indice, montrer que $L_{n}$ a la même parité que $n$.

**5)** Vérifier, à l'aide de la formule de Leibniz, que :

$$
\forall n \in \mathbb{N}, L_{n}(X)=\frac{1}{2^{n} n !} \frac{\mathrm{d}^{n}}{\mathrm{~d} X^{n}}\left[\left(X^{2}-1\right)^{n}\right]
$$

**6)** En déduire explicitement le coefficient dominant de $L_{n}$, puis la relation

$$
\forall n \in \mathbb{N}, \sum_{k=0}^{n}\left(\begin{array}{l}
n \\
k
\end{array}\right)^{2}=\left(\begin{array}{c}
2 n \\
n
\end{array}\right)
$$

**7)** Montrer alors que

$$
\forall n \in \mathbb{N}, \forall x \in \mathbb{R},\left|L_{n}(x)\right| \leqslant\left(\frac{1+|x|}{2}\right)^{n}\left(\begin{array}{c}
2 n \\
n
\end{array}\right) .
$$

**8)** On définit, pour tout entier naturel $n$, le polynôme $U_{n}(X)=\left(X^{2}-1\right)^{n}$.

**a)** Vérifier que :

$$
\left(X^{2}-1\right) U_{n}^{\prime}(X)=2 n X U_{n}(X)
$$

**b)** En dérivant $n+1$ fois cette relation, montrer que

$$
\forall n \in \mathbb{N}, \phi\left(L_{n}\right)=n(n+1) L_{n}
$$

#### Partie C : Définition d'un produit scalaire

On pose

$$
\forall(P, Q) \in E^{2},\langle P, Q\rangle=\int_{-1}^{1} P(x) Q(x) \mathrm{d} x .
$$

**1)** Justifier que l'on a ainsi défini un produit scalaire sur $E$.

Dans toute la suite du problème, l'espace $E$ et ses sous-espaces $E_{n}(n \in \mathbb{N})$ seront systématiquement munis de ce produit scalaire.

**2)** **a)** Montrer que

$$
\forall(P, Q) \in E^{2},\langle\phi(P), Q\rangle=\int_{-1}^{1}\left(1-x^{2}\right) P^{\prime}(x) Q^{\prime}(x) \mathrm{d} x .
$$

**b)** Que peut-on dire déduire pour les endomorphismes $\varphi_{n}(n \in \mathbb{N})$ ?

**c)** En déduire, à l'aide d'un résultat de la partie $\mathbf{B}$, que les polynômes $L_{p}(p \in \mathbb{N})$ sont deux à deux orthogonaux.

**3)** Soit $n$ un entier naturel.

**a)** Établir par récurrence sur $k$ que

$$
\forall k \in[|0, n|],\left\langle Q, L_{n}\right\rangle=\frac{(-1)^{k}}{2^{n} n !} \int_{-1}^{1} \frac{\mathrm{d}^{k}}{\mathrm{~d} x^{k}}[Q(x)] \frac{\mathrm{d}^{n-k}}{\mathrm{~d} x^{n-k}}\left[\left(x^{2}-1\right)^{n}\right] \mathrm{d} x .
$$

**b)** En déduire que, pour tout $n \in \mathbb{N}^{*}, L_{n}$ est orthogonal à $E_{n-1}$.

**c)** Retrouver ainsi que les polynômes $L_{p}(p \in \mathbb{N})$ sont deux à deux orthogonaux.

**4)** **a)** À l'aide de C.**3)** **a)**, exprimer, pour tout entier naturel $n,\left\|L_{n}\right\|^{2}$ en fonction de

$$
I_{n}=\int_{-1}^{1}\left(x^{2}-1\right)^{n} \mathrm{~d} x
$$

**b)** À l'aide d'une intégration par parties, montrer que

$$
\forall n \in \mathbb{N}, I_{n+1}=-\frac{2 n+2}{2 n+3} I_{n}
$$

**c)** En déduire, pour tout $n \in \mathbb{N}$, une expression de $I_{n}$ faisant intervenir des factorielles.

**d)** En déduire que

$$
\forall n \in \mathbb{N},\left\|L_{n}\right\|=\sqrt{\frac{2}{2 n+1}}
$$

**5)** Donner, pour tout entier naturel $n$, une base orthonormée de $E_{n}$.

#### Partie D : Une relation de récurrence

Soit $n$ un entier naturel non nul.

**1)** Calculer le coefficient de $X^{n+1}$ dans $(n+1) L_{n+1}(X)-(2 n+1) X L_{n}(X)$.

**2)** En déduire l'existence et l'unicité de $n+1$ réels $\alpha_{k}$ tels que

$$
(n+1) L_{n+1}(X)-(2 n+1) X L_{n}(X)=\sum_{k=0}^{n} \alpha_{k} L_{k}(X) .
$$

**3)** Montrer que $\forall k \in[|0, n|], \alpha_{k}=-(2 n+1) \frac{\left\langle X L_{n}, L_{k}\right\rangle}{\left\|L_{k}\right\|^{2}}$.

**4)** Pour tout $k \in[|0, n-2|]$, vérifier que $\left\langle X L_{n}, L_{k}\right\rangle=\left\langle L_{n}, X L_{k}\right\rangle$ puis montrer que $\alpha_{k}=0$.

**5)** Par des considérations de parité, montrer que $\alpha_{n}=0$.

**6)** En utilisant la valeur des polynômes $L_{k}$ au point 1, déterminer alors $\alpha_{n-1}$.

**7)** En déduire que $\forall n \in \mathbb{N}^{*},(n+1) L_{n+1}(X)-(2 n+1) X L_{n}(X)+n L_{n-1}(X)=0$.

#### Partie E : Fonction génératrice (hors programme)

On fixe un réel $t$ et on considère la série entière $\sum_{n \in \mathbb{N}} L_{n}(t) x^{n}$, de la variable réelle $x$.

**1)** Déterminer le rayon de convergence de la série entière $\sum_{n \in \mathbb{N}}\left(\frac{1+|t|}{2}\right)^{n}\left(\begin{array}{c}2 n \\ n\end{array}\right) x^{n}$.

**2)** En déduire que le rayon de convergence $R_{t}$ de la série entière $\sum_{n \in \mathbb{N}} L_{n}(t) x^{n}$ est strictement positif.

On donnera une minoration de $R_{t}$, mais on ne cherchera pas à le calculer.

On note $S_{t}$ la somme de cette série entière : $\left.\forall x \in\right]-R_{t}, R_{t}\left[, S_{t}(x)=\sum_{n=0}^{+\infty} L_{n}(t) x^{n}\right.$

**3)** En utilisant le résultat de **D.7)**, montrer que $S_{t}$ est solution sur $]-R_{t}, R_{t}$ [ de l'équation différentielle suivante, d'inconnue $y$ fonction de $x$ :

$$
\left(\mathscr{E}_{t}\right)\left(1-2 t x+x^{2}\right) y^{\prime}(x)+(x-t) y(x)=0
$$

**4)** Pour $|t|<1$, en déduire l'expression de $S_{t}(x)$ en fonction de $x$.

#### Partie F : Projection orthogonale, calcul de distance

**1)** Calculer, pour tout entier naturel $k$, l'intégrale

$$
J_{k}=\int_{-1}^{1} x^{k} \mathrm{~d} x
$$

**2)** Étant donnés deux entiers naturels $n$ et $r$, tels que $0 \leqslant r \leqslant n$, on note $p_{r}$ la projection orthogonale de $E_{n}$ sur son sous-espace vectoriel $E_{r}$.

Donner une expression générale de $p_{r}(P)$ utilisant le produit scalaire, pour tout polynôme $P$ de $E_{n}$.

**3)** On suppose désormais $n=3$ et $P=X^{3}$.

**a)** Déterminer $p_{0}(P), p_{1}(P)$ et $p_{2}(P)$.

**b)** Calculer les distances $d\left(P, E_{k}\right)$ de $P$ aux sous-espaces vectoriels $E_{k}$ pour tout entier $k$ tel que $0 \leqslant k \leqslant 2$.

**4)** On note $G$ l'ensemble des polynômes de degré 3 et de coefficient dominant égal à 1 .

Montrer l'existence de

$$
m=\min _{Q \in G} \int_{-1}^{1}(Q(x))^{2} \mathrm{~d} x
$$

et préciser sa valeur, ainsi que les polynômes réalisant ce minimum.

**Fin de l'énoncé**
