

# Suites numériques
L=[]
L.append(["exercicesBanquesCCINP/ccinp2023-001.md*",\
    "exercicesBanquesCCINP/ccinp2023-043.md*",\
    "exercicesBanquesCCINP/ccinp2023-055.md",\
    "exercicesRMS/2022/RMS2022-1434.md",\
    "exercicesRMS/2022/RMS2022-711.md*"])

# fonctions
L.append(["exercicesBanquesCCINP/ccinp2023-003.md*",\
    "exercicesBanquesCCINP/ccinp2023-004.md"])

# Séries numériques
L.append(["exercicesBanquesCCINP/ccinp2023-006.md*",\
	"exercicesBanquesCCINP/ccinp2023-007.md",\
	"exercicesBanquesCCINP/ccinp2023-008.md",\
	"exercicesBanquesCCINP/ccinp2023-046.md*",\
	"exercicesRMS/2022/RMS2022-1307.md*",\
	"exercicesRMS/2022/RMS2022-1437.md"])
#13
# Fonctions géométriques
L.append(["exercicesBanquesCCINP/ccinp2023-033.md*",\
	"exercicesBanquesCCINP/ccinp2023-052.md",\
	"exercicesBanquesCCINP/ccinp2023-057.md",\
	"exercicesRMS/2022/RMS2022-1472.md",\
	"exercicesRMS/2022/RMS2022-1473.md*",\
	"exercicesRMS/2022/RMS2022-1443.md"])

#19
# Equations différentielles
L.append(["exercicesBanquesCCINP/ccinp2023-042.md",\
	"exercicesRMS/2022/RMS2022-1470.md*"])
#21
# Espaces vectoriels, endomorphismes
L.append(["exercicesBanquesCCINP/ccinp2023-059.md*",\
	"exercicesBanquesCCINP/ccinp2023-060.md*",\
	"exercicesBanquesCCINP/ccinp2023-064.md*",\
	"exercicesBanquesCCINP/ccinp2023-071.md",\
	"exercicesBanquesCCINP/ccinp2023-090.md",\
	"exercicesRMS/2022/RMS2022-1398.md*",\
	"exercicesRMS/2022/RMS2022-1406.md"])

#28
# Espaces Euclidiens
L.append(["exercicesBanquesCCINP/ccinp2023-076.md",\
	"exercicesBanquesCCINP/ccinp2023-077.md*",\
	"exercicesBanquesCCINP/ccinp2023-079.md",\
	"exercicesBanquesCCINP/ccinp2023-080.md",\
	"exercicesBanquesCCINP/ccinp2023-081.md*",\
	"exercicesBanquesCCINP/ccinp2023-082.md",\
	"exercicesBanquesCCINP/ccinp2023-092.md",\
	"exercicesRMS/2022/RMS2022-1424.md*",\
	"exercicesRMS/2022/RMS2022-694.md"])
#37
# Intégration
L.append(["exercicesRMS/2022/RMS2022-1371.md*",\
	"exercicesRMS/2022/RMS2022-1383.md*"])

#39
# Nombres Complexes
L.append(["exercicesBanquesCCINP/ccinp2023-084.md",\
	"exercicesBanquesCCINP/ccinp2023-089.md*",\
	"exercicesRMS/2022/RMS2022-1393.md*",\
	"exercicesRMS/2022/RMS2022-1394.md",\
	"exercicesRMS/2022/RMS2022-1346.md"])

#44
# Polynômes
L.append(["exercicesBanquesCCINP/ccinp2023-085.md",\
	"exercicesBanquesCCINP/ccinp2023-087.md",\
	"exercicesRMS/2022/RMS2022-1395.md*",\
	"exercicesRMS/2022/RMS2022-675.md"])

#48
# Probas
L.append(["exercicesBanquesCCINP/ccinp2023-105.md*",\
	"exercicesBanquesCCINP/ccinp2023-107.md",\
	"exercicesBanquesCCINP/ccinp2023-112.md"])

#51
# Variables Aléatoires
L.append(["exercicesBanquesCCINP/ccinp2023-095.md*",\
	"exercicesBanquesCCINP/ccinp2023-098.md*",\
	"exercicesBanquesCCINP/ccinp2023-102.md",\
	"exercicesBanquesCCINP/ccinp2023-104.md",\
	"exercicesBanquesCCINP/ccinp2023-109.md",\
	"exercicesRMS/2022/RMS2022-1479.md"])

#Centrale Python
L.append(["exercicesCentralePython/rms2016-1068.md",\
"exercicesCentralePython/rms2017-1028.md",\
"exercicesCentralePython/rms2019-1011.md",\
"exercicesCentralePython/rms2019-1022.md",\
"exercicesCentralePython/rms2021-899.md",\
"exercicesCentralePython/rms2021-900.md",\
"exercicesCentralePython/rms2021-1035.md",\
"exercicesCentralePython/rms2021-1063.md",\
"exercicesCentralePython/rms2021-1097.md",\
"exercicesCentralePython/rms2022-1094.md",\
"exercicesCentralePython/rms2022-1103.md",\
"exercicesCentralePython/rms2022-1109.md",\
"exercicesCentralePython/rms2022-1124.md"])

#fichier_bib = open("exercices.bib","w")

fichier_md = open("cdv.md","w")
enonces = ""
corriges = "" 

i = 1
for domaine in L :
	for nom_fichier in domaine:
		nom_court = nom_fichier[nom_fichier.find("/")+1:]
		if nom_court[-1]=="*" :
			banque="b"
			nom_court = nom_court[:-1]
			nom_fichier = nom_fichier[:-1]
		else :
			banque="" 
		fichier = open("../../"+nom_fichier,"r")
		fichier.readline()
		contenu = fichier.read()
		enocor = contenu.split("===")
		enonce = enocor[1]
		enonce = enonce[enonce.find("\n")+1:]
		enonces += "\n\n!!! exercice"+banque+" \"Exercice <a name=\"enonce-"+str(i)+"\"></a><a href=\"#corrige-"+str(i)+"\">"+str(i)+"</a>\"\n"+enonce+"\n\n"
		corrige = enocor[2]
		corrige = corrige[corrige.find("\n")+1:]
		corriges += "\n\n!!! exercice"+banque+" \"Corrigé <a name=\"corrige-"+str(i)+"\"></a><a href=\"#enonce-"+str(i)+"\">"+str(i)+"</a>\"\n"+corrige+"\n\n"
#		fichier_bib.write("@exercice{"+nom_court+",\nenonce={"+enonce+"},\ncorrige={"+corrige+"},source =    {},difficulte =    0,concours =    {},motsclef =    {}}\n\n\n")
		i+=1
		fichier.close()
fichier_md.write(open("entete.md","r").read()+"<div style='page-break-before:always'></div>\n## Énoncés"+enonces+"<br><br><div style='page-break-before:always'></div>\n## Corrigés"+corriges+open("pied.md","r").read())

fichier_md.close()
