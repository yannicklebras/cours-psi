<h1>Cahier de vacances pour la PSI</h1>

Chers futurs élèves de PSI*,

Je me présente, Yannick Le Bras, je serai votre enseignant en
 mathématiques l'année prochaine. L'année de PSI sera intense, il y a peu
de temps et beaucoup de choses à apprendre. Le plus important est de vous
créer une culture. Cette culture se forge en travaillant les exercices,
classiques et moins classiques.

J'ai compilé dans ce document des exercices tombés au concours. Ces exercices
sont "accessibles" à la fin de la première année. J'ai tenté de regrouper
des exercices autour des thématiques qui seront utiles l'année prochaine.

Vous trouverez aussi des exercices avec utilisation du langage Python. Ces
exercices sont issus des oraux de Centrale Mathématiques/Informatique. Ils
vous permettront de travailler en même temps vos compétences en Python. Afin
de tavailler ces exercices, vous devrez vous appuyer sur les documents qui sont
 en ligne sur le site de Centrale :
[Documents Python](https://www.concours-centrale-supelec.fr/CentraleSupelec/SujetsOral/PSI)

Enfin, certains exercices sont identifiés (en rose) de façon particulière : ces exercices
feront partie de notre banque d'exercices de colles. À chaque colle de mathématiques,
c'est à dire toutes les deux semaines, vous vous verrez proposer un exercice
issu de notre banque et un exercice original proposé par le colleur. Les
exercices de la banque seront connus, fournis avec les corrigés et augmenteront
chaque semaine. C'est le principe de l'oral de CCP en filière MP (qui est plus
ou moins valable en filière PSI, mais ce n'est pas officiel). Cette banque
couvrira l'ensemble des notions du programme de PSI et aura pour but d'aborder
essentiellement les théorèmes principaux et les méthodes classiques. Les Corrigés
proposés n'ont pas la prétention d'être parfaits, sachez les adapter à votre compréhension.


Ainsi, dans ce cahier de vacances sont identifiés 25 exercices (en rose donc), dont fera partie
votre exercice imposé lors de votre première colle de mathématiques. Je vous
conseille évidemment fortement de travailler ces exercices pendant les vacances.
Ce sont des exercices que vous devrez savoir faire.

Enfin, vous trouverez en fin de documents 3 sujets d'écrits que vous pouvez traiter en DM pendant les vacances. Si vous me les transmettez avant la date indiquée (version scannée), je corrigerai et vous enverrai ma correction. J'impose des dates progressives afin de ne pas être submergé au dernier moment. Vous pourrez me transmettre vos copies par mail, ou bien par la plateforme ci dessous.

Afin de vous aider et de vous guider pendant tout cet été, vous pourrez échanger
entre vous, et avec moi sur la plateforme suivante : [https://prepapsi.ylb-maths.fr/signup_user_complete/?id=8empk3wnp7gkdpxubi4s6pifdw&sbr=fa](https://prepapsi.ylb-maths.fr/signup_user_complete/?id=8empk3wnp7gkdpxubi4s6pifdw&sbr=fa)

Rien de tout ce que contient ce cahier de vacances n'est obligatoire (à part les exercices roses qui seront au programme de colle). Tout est donné pour vous aider à vous préparer à cette année de PSI*.

Je suis impatient de vous retrouver à la rentrée, je vous souhaite de
belles vacances,

Yannick Le Bras
<div style='page-break-before:always'></div>
## Énoncés

!!! exerciceb "Exercice <a name="enonce-1"></a><a href="#corrige-1">1</a>"


		1.  On considère deux suites numériques $\left( u_{n}\right)_{n\in \mathbb{N}}$ et $\left( v_{n}\right) _{n\in \mathbb{N}}$ telles que $(v_n)_{n\in \mathbb{N}}$ est non nulle à partir d’un certain rang et $u_{n}\underset{+\infty}\thicksim v_{n}$.
		    Démontrer que $u_{n}$ et $v_{n}$ sont de même signe à partir d’un certain rang.

		2.  Déterminer le signe, au voisinage de l’infini, de : $u_{n}=\text{sh}\left( \dfrac{1}{n}\right) -\tan \left( \dfrac{1}{n}\right)$.






!!! exerciceb "Exercice <a name="enonce-2"></a><a href="#corrige-2">2</a>"


		Soit $x_0 \in \mathbb{R}$.
		On définit la suite $(u_n)$ par $u_0=x_0$ et, $\forall n\in\mathbb{N}\:,\: u_{n+1}=\mathrm{Arctan}(u_n)$.

		1.  1.  Démontrer que la suite $(u_n)$ est monotone et déterminer, en fonction de la valeur de $x_0$, le sens de variation de $(u_n)$.

		    2.  Montrer que $(u_n)$ converge et déterminer sa limite.

		2.  Déterminer l’ensemble des fonctions $h$, continues sur $\mathbb{R}$, telles que : $\forall x \in \mathbb{R}$, $h(x)=h(\mathrm{Arctan}\: x)$.






!!! exercice "Exercice <a name="enonce-3"></a><a href="#corrige-3">3</a>"


		Soit $a$ un nombre complexe.
		On note $E$ l’ensemble des suites à valeurs complexes telles que :
		$\forall\:n\in\mathbb{N}$, $u_{n+2}=2au_{n+1}+4(\mathrm{i}a-1)u_n$ avec $(u_0,u_1)\in  \mathbb{C} ^2$.

		1.  1.  Prouver que $E$ est un sous-espace vectoriel de l’ensemble des suites à valeurs complexes.

		    2.  Déterminer, en le justifiant, la dimension de $E$.

		2.  Dans cette question, on considère la suite de $E$ définie par: $u_0=1$ et $u_1=1$.
		    Exprimer, pour tout entier naturel $n$, le nombre complexe $u_n$ en fonction de $n$.
		    **Indication**: discuter suivant les valeurs de $a$.






!!! exercice "Exercice <a name="enonce-4"></a><a href="#corrige-4">4</a>"
		 **CCINP**On pose $P_{n}=\sum_{k=0}^{2n+1}\dfrac{\left(-X\right)^{k}}{k!}$.

		- a) Montrer que : $P_{n+1}'=-P_{n}-\dfrac{X^{2n+2}}{\left(2n+2\right)!}$
		et $P_{n+1}''=P_{n}$.

		- b)  
			- i) Par récurrence, montrer la stricte décroissance de la fonction polynomiale
		$p_{n}$ associée à  $P_{n}$. Montrer que $p_{n}$ admet une unique
		valeur d'annulation sur $\R$, notée $u_{n}$.

			- ii)  Montrer que $P_{n}=\sum_{k=0}^{n}\dfrac{X^{2k}}{\left(2k\right)!}\left(1-\dfrac{X}{2k+1}\right)$.

		- c)
			- i) Montrer que $u_{n}\in\left[1,2n+1\right]$.

			- ii) Montrer que la suite $u$ est monotone.






!!! exerciceb "Exercice <a name="enonce-5"></a><a href="#corrige-5">5</a>"
		Soit, pour  $n\geq 4$, $f_n : x\mapsto x^{3n} - \sqrt{n}x +1$.

		a) Montrer qu'il existe un unique $x_n\in [1,2]$ tel que $f_n(x_n)=0$.

		b) Étudier la convergence de la suite de terme général $\epsilon_n=x_n-1$.

		c)  Trouver un équivalent de $\epsilon_n$.


		d)  Donner un développement asymptotique à trois termes de $x_n$.







!!! exerciceb "Exercice <a name="enonce-6"></a><a href="#corrige-6">6</a>"


		1.  On pose $g(x)=\mathrm{e}^{2x}$ et $h(x)=\dfrac{1}{1+x}$.
		    Calculer, pour tout entier naturel $k$, la dérivée d’ordre $k$ des fonctions $g$ et $h$ sur leurs ensembles de définitions respectifs.

		2.  On pose $f(x)=\dfrac{\mathrm{e}^{2x}}{1+x}$.
		    En utilisant la formule de Leibniz concernant la dérivée $n^{\text{ième}}$ d’un produit de fonctions, déterminer, pour tout entier naturel $n$ et pour tout $x\in\mathbb{R}\backslash\left\lbrace -1\right\rbrace$, la valeur de $f^{(n)}(x)$.

		3.  Démontrer, dans le cas général, la formule de Leibniz, utilisée dans la question précédente.






!!! exercice "Exercice <a name="enonce-7"></a><a href="#corrige-7">7</a>"


		1.  Énoncer le théorème des accroissements finis.

		2.  Soit $f:\left[ a,b\right]\longrightarrow \mathbb{R}$ et soit $x_0\in \left]a,b \right[$.
		    On suppose que $f$ est continue sur $[a,b]$ et que $f$ est dérivable sur $]a,x_0[$ et sur $]x_0,b[$.
		    Démontrer que, si $f'$ admet une limite finie en $x_0$, alors $f$ est dérivable en $x_0$ et $f'(x_0)=\lim\limits_{x\rightarrow x_0}f'(x)$.

		3.  Prouver que l’implication : ( $f$ est dérivable en $x_0$) $\Longrightarrow$ ($f'$ admet une limite finie en $x_0$) est fausse.

		    **Indication**: on pourra considérer la fonction $g$ définie par: $g(x)=x^2\sin\dfrac{1}{x}$ si $x\not=0$ et $g(0)=0$.






!!! exerciceb "Exercice <a name="enonce-8"></a><a href="#corrige-8">8</a>"


		Soit $\left( u_{n}\right) _{n\in \mathbb{N}}$ une suite de réels strictement positifs et $l$ un réel positif strictement inférieur à 1.

		1.  Démontrer que si $\underset{n\rightarrow +\infty }{\lim }\dfrac{u_{n+1}}{u_{n}}=l$, alors la série $\displaystyle\sum u_{n}$ converge.

		    **Indication**: écrire, judicieusement, la définition de $\underset{n\rightarrow +\infty }{\lim }\dfrac{u_{n+1}}{u_{n}}=l$, puis majorer, pour $n$ assez grand, $u_{n}$ par le terme général d’une suite géométrique.

		2.  Quelle est la nature de la série $\displaystyle\sum_{n\geqslant1}{} \dfrac{n!}{n^{n}}$?






!!! exercice "Exercice <a name="enonce-9"></a><a href="#corrige-9">9</a>"
		1.  Soient $\left( u_{n}\right) _{n\in \mathbb{N}}$ et $\left(v_{n}\right) _{n\in \mathbb{N}}$ deux suites de nombres réels positifs.
		On suppose que $(u_n)_{n\in \mathbb{N}}$ et $(v_n)_{n\in \mathbb{N}}$ sont non nulles à partir d’un certain rang.
		Montrer que:

		$$
		u_{n}\underset{+\infty}{\thicksim} v_{n} \  \Longrightarrow  \ \displaystyle\sum u_{n}\ \text{et }\displaystyle\sum v_{n}\ \text{sont de m\^{e}me nature}.
		$$

		2.  Étudier la convergence de la série $\displaystyle\sum\limits_{n\geqslant 2}^{} \dfrac{\left((-1)^n+\mathrm{i}\right)\ln n \sin \left( \dfrac{1}{n}\right) }{\left(\sqrt{n+3}-1\right)}$.

		**Remarque** : $\mathrm{i}$ désigne le nombre complexe de carré égal à $-1$.






!!! exercice "Exercice <a name="enonce-10"></a><a href="#corrige-10">10</a>"


		1.  Soit $\left( u_{n}\right) _{n\in \mathbb{N}}\ $une suite décroissante positive de limite nulle.

		    1.  Démontrer que la série $\displaystyle\sum \left( -1\right) ^{k}u_{k}$ est convergente.

		        **Indication**: on pourra considérer $\left( S_{2n}\right) _{n\in \mathbb{N}}$ et $\left( S_{2n+1}\right) _{n\in \mathbb{N}}$ avec $S_{n}=\displaystyle\sum_{k=0}^{n}\left( -1\right) ^{k}u_{k}$.

		    2.  Donner une majoration de la valeur absolue du reste de la série $\displaystyle\sum \left( -1\right) ^{k}u_{k}$.

		2.  On pose : $\forall \:n\in\mathbb{N}^*$, $\forall\:x\in\mathbb{R}$, $f_n(x)=\dfrac{\left(-1\right)^{n}e^{-nx}}{n}$.

		    1.  Étudier la convergence simple sur $\mathbb{R}$ de la série de fonctions $\displaystyle\sum_{n\geqslant1}^{}f_n$.

		    2.  Étudier la convergence uniforme sur $\left[ 0,+\infty\right[$ de la série de fonctions $\displaystyle\sum_{n\geqslant1}^{}f_n$.






!!! exerciceb "Exercice <a name="enonce-11"></a><a href="#corrige-11">11</a>"


		On considère la série:$\displaystyle\sum\limits_{n\geqslant1}{}\cos \left( \pi \sqrt{n^2+n+1}\right)$.

		1.  Prouver que, au voisinage de $+\infty$, $\pi\sqrt{n^2+n+1}=n\pi+\dfrac{\pi}{2}+\alpha\dfrac{\pi}{n}+O\left(\dfrac{1}{n^2} \right)$ où $\alpha$ est un réel que l’on déterminera.

		2.  En déduire que $\displaystyle\sum\limits_{n\geqslant 1}{}\cos \left( \pi \sqrt{n^2+n+1}\right)$ converge.

		3.  $\displaystyle\sum\limits_{n\geqslant 1}{}\cos \left( \pi \sqrt{n^2+n+1}\right)$ converge-t-elle absolument?






!!! exerciceb "Exercice <a name="enonce-12"></a><a href="#corrige-12">12</a>"
		**IMT**   .
		Déterminer la nature de la série $\sum u_n$, avec $u_n =  \frac{(-1)^n }{n^{3/4} + \sin n}$.





!!! exercice "Exercice <a name="enonce-13"></a><a href="#corrige-13">13</a>"
		 **CCINP**Soit $\left(\theta,\varphi\right)\in\R^{2}$.
		Pour $n\geqslant2$, on pose : $u_{n}=\dfrac{\mathrm{e}^{\mathrm{i}n\theta}}{\sqrt{n}+\mathrm{e}^{\mathrm{i}n\varphi}}$.

		- a) Montrer que $u_{n}=\dfrac{\mathrm{e}^{\mathrm{i}n\theta}}{\sqrt{n}}-\dfrac{\mathrm{e}^{\mathrm{i}n\left(\theta+\varphi\right)}}{n}+\co\left(\dfrac{1}{n\sqrt{n}}\right)$.
		- b) On suppose que $\theta$ et $\varphi$ sont des multiples de $\pi$.
		Quelle est la nature de $\sum u_{n}$ ?






!!! exerciceb "Exercice <a name="enonce-14"></a><a href="#corrige-14">14</a>"


		On pose : $\forall\:(x,y)\in\mathbb{R}^2\backslash \left\lbrace (0,0)\right\rbrace$, $f\left( x,y\right) =\dfrac{xy}{\sqrt{x^{2}+y^{2}}}$ et $f\left( 0,0\right) =0$.

		1.  Démontrer que $f$ est continue sur $\mathbb{R}^{2}$.

		2.  Démontrer que $f$ admet des dérivées partielles en tout point de $\mathbb{R}^{2}$.

		3.  $f$ est-elle de classe $C^{1}$ sur $\mathbb{R}^{2}$? Justifier.






!!! exercice "Exercice <a name="enonce-15"></a><a href="#corrige-15">15</a>"


		Soit $\alpha\in\mathbb R$.
		On considère l’application définie sur $\mathbb{R}^2$ par $f(x,y)=\begin{cases}
		\dfrac{y^{4}}{x^{2}+y^{2}-xy} & \mathrm{si }(x,y)\not=(0,0)\\
		\alpha & \mathrm{si}\  (x,y)=(0,0).
		\end{cases}$

		1.  Prouver que : $\forall(x,y)\in\mathbb R^{2},\:x^{2}+y^{2}-xy\geqslant\dfrac{1}{2}(x^{2}+y^{2})$.

		2.  1.  Justifier que le domaine de définition de $f$ est bien $\mathbb{R}^2$.

		    2.  Déterminer $\alpha$ pour que $f$ soit continue sur $\mathbb R^{2}$.

		3.  Dans cette question, on suppose que $\alpha=0$.

		    1.  Justifier l’existence de $\dfrac{\partial f}{\partial x}$ et $\dfrac{\partial f}{\partial y}$ sur $\mathbb R^{2}\setminus\big\{(0,0)\big\}$ et les calculer.

		    2.  Justifier l’existence de $\dfrac{\partial f}{\partial x}(0,0)$ et $\dfrac{\partial f}{\partial y}(0,0)$ et donner leur valeur.

		    3.  $f$ est-elle de classe ${\cal C}^{1}$ sur $\mathbb R^{2}$?






!!! exercice "Exercice <a name="enonce-16"></a><a href="#corrige-16">16</a>"


		1.  Soit $f$ une fonction de $\mathbb{R}^{2}$ dans $\mathbb{R}$.

		    1.  Donner, en utilisant des quantificateurs, la définition de la continuité de $f$ en $(0,0)$.

		    2.  Donner la définition de «$f$ différentiable en $(0,0)$».

		2.  On considère l’application définie sur $\mathbb{R}^2$ par $f(x,y)=\left\lbrace
		      \begin{array}{ll}
		     xy\dfrac{x^2-y^2}{x^2+y^2}& \text{si}\: (x,y)\neq (0,0)\\
		     0 &\text{si} \:(x,y)=(0,0)
		     \end{array}
		     \right.$

		    1.  Montrer que $f$ est continue sur $\mathbb{R}^2$.

		    2.  Montrer que $f$ est de classe $\mathcal {C}^1$ sur $\mathbb{R}^2$.






!!! exercice "Exercice <a name="enonce-17"></a><a href="#corrige-17">17</a>"
		 **CCINP**. Soit $f: [0,2]\times [-1,0] \to \R$ la fonction $(x,y) \mapsto x^{2}-2x+xy+y^{2}$. Trouver les extrémums globaux de $f$.





!!! exerciceb "Exercice <a name="enonce-18"></a><a href="#corrige-18">18</a>"
		 **CCINP**. Pour tous réels $x,y$, on pose $f\left(x,y\right)=x^{3}+y^{3}-3xy$.

		- a) Quels sont les points critiques de $f$ ?
		- b) Est-ce que $f$ possède un extrémum global ?
		- c) Quels sont les extrémums locaux de $f$ ?





!!! exercice "Exercice <a name="enonce-19"></a><a href="#corrige-19">19</a>"
		  **CCINP**  .
		Soit $\omega \in \R^{+*}$. On considère l'équation fonctionnelle $(*)$ suivante, portant sur des fonctions
		$f \in \mathcal{C}^2(\mathbb{R}, \mathbb{R})$ :
		$
		\forall (x, y) \in \mathbb{R}^2, \quad f(x + y) + f(x - y) = 2f(x)\,  f(y).$

		 - a)
		Résoudre le problème de Cauchy $(E_1) \colon y'' = -\omega^2 y$, $y(0) = 1$ et $y'(0) = 0$.

		 - b)
		Résoudre le problème de Cauchy $(E_2) \colon y'' = \omega^2 y$, $y(0) = 1$ et $y'(0) = 0$.

		 - c)
		Montrer que $\forall(x, y) \in \mathbb{R}^2, \ \ch(x + y) = \ch(x) \ch(y) + \sh(x) \sh(y)$.
		En déduire que la fonction $\ch$ est solution de $(*)$.

		 - d)
		Soit $f$ une solution de $(*)$.
		Montrer que $f(0) \in \{0, 1\}$, et que si $f(0) = 0$, alors $f$ est la fonction nulle.
		Montrer que si $f(0) = 1$, alors $f'(0) = 0$.


		 - e)
		Trouver toutes les solutions $f \in \mathcal{C}^2(\mathbb{R}, \mathbb{R})$ de $(*)$.






!!! exercice "Exercice <a name="enonce-20"></a><a href="#corrige-20">20</a>"


		On considère les deux équations différentielles suivantes:
		$2xy'-3y=0$ $(H)$
		$2xy'-3y=\sqrt{x}$ $(E)$

		1.  Résoudre l’équation $(H)$ sur l’intervalle $\left]  0,+\infty\right[$.

		2.  Résoudre l’équation $(E)$ sur l’intervalle $\left]  0,+\infty\right[$.

		3.  L’équation $(E)$ admet-elle des solutions sur l’intervalle $\left[ 0,+\infty\right[$?






!!! exerciceb "Exercice <a name="enonce-21"></a><a href="#corrige-21">21</a>"
		  **CCINP**  .
		On étudie sur $]0, 1]$ l'équation différentielle $(E): x^2 y'' + 4 x y' + 2 y = \frac{1}{x\sqrt{x}}$.


		 - a)
		Donner les solutions de l'équation homogène de la forme $x \mapsto x^{\alpha}$.

		 - b)
		Chercher les solutions de $(E)$ sous la forme $x \mapsto \frac{z(x)}{x^2}$.





!!! exerciceb "Exercice <a name="enonce-22"></a><a href="#corrige-22">22</a>"


		Soit $n$ un entier naturel tel que $n\geqslant 2$.
		Soit $E$ l’espace vectoriel des polynômes à coefficients dans $\mathbb{K}$ ($\mathbb{K}=\mathbb{R}$ ou $\mathbb{K}=\mathbb{C}$) de degré inférieur ou égal à $n$.
		On pose : $\forall\:P\in E$, $f\left( P\right)=P-P^{\prime }$.

		1.  Démontrer que $f$ est bijectif de deux manières:

		    1.  sans utiliser de matrice de $f$,

		    2.  en utilisant une matrice de $f$.

		2.  Soit $Q\in E.$ Trouver $P$ tel que $f\left( P\right) =Q$ .

		    **Indication** : si $P\in E$, quel est le polynôme $P^{\left(n+1\right) }$?

		3.  (hors programme en début d'année) $f$ est-il diagonalisable?






!!! exerciceb "Exercice <a name="enonce-23"></a><a href="#corrige-23">23</a>"


		Soit la matrice $A=
		\begin{pmatrix}
		1 & 2 \\
		2 & 4
		\end{pmatrix}$ et $f$ l’endomorphisme de $\mathcal{M}_{2}\left( \mathbb{R}%
		\right)$ défini par :   $f\left( M\right) =AM.$

		1.  Déterminer une base de $\mathrm{Ker}f$.

		2.  $f$ est-il surjectif ?

		3.  Déterminer une base de $\text{Im}f.$

		4.  A-t-on $\mathcal{M}_{2}\left( \mathbb{R}%
		    \right)=\mathrm{Ker}f\oplus \mathrm{Im}f$?






!!! exerciceb "Exercice <a name="enonce-24"></a><a href="#corrige-24">24</a>"


		Soit $f$ un endomorphisme d’un espace vectoriel $E$ de dimension finie $n$.

		1.  Démontrer que: $E=\text{Im} f \oplus \mathrm{Ker} f \Longrightarrow \text{Im} f = \text{Im} f^2$.

		2.  1.  Démontrer que: $\text{Im} f = \text{Im} f^2 \Longleftrightarrow \mathrm{Ker} f = \mathrm{Ker} f^2$.

		    2.  Démontrer que: $\text{Im} f = \text{Im} f^2 \Longrightarrow E=\text{Im} f \oplus \mathrm{Ker} f$.






!!! exercice "Exercice <a name="enonce-25"></a><a href="#corrige-25">25</a>"


		Soit $P$ le plan d’équation $x+y+z=0$ et $D$ la droite d’équation $x=\dfrac{y}{2}=\dfrac{z}{3}$.

		1.  Vérifier que $\mathbb{R}^3=P\oplus D$.

		2.  Soit $p$ la projection vectorielle de $\mathbb{R}^3$ sur $P$ parallèlement à $D$.
		    Soit $u=(x,y,z)\in\mathbb{R}^3$.
		    Déterminer $p(u)$ et donner la matrice de $p$ dans la base canonique de $\mathbb{R}^3$.

		3.  Déterminer une base de $\mathbb{R}^3$ dans laquelle la matrice de $p$ est diagonale.






!!! exercice "Exercice <a name="enonce-26"></a><a href="#corrige-26">26</a>"


		$\mathbb K$ désigne le corps des réels ou celui des complexes.
		Soient $a_{1}, a_{2}, a_{3}$ trois scalaires distincts donnés de $\mathbb K$.

		1.  Montrer que $\Phi:\begin{array}{ccc}
		    \mathbb K_{2}[X] & \longrightarrow & \mathbb K^{3}\\
		    P & \longmapsto & \big(P(a_{1}),P(a_{2}),P(a_{3})\big)\\
		    \end{array}$ est un isomorphisme d’espaces vectoriels.

		2.  On note $(e_{1}, e_{2}, e_{3})$ la base canonique de $\mathbb K^{3}$ et on pose $\forall k\in\{1,2,3\}, \:L_{k}=\Phi^{-1}(e_{k})$.

		    1.  Justifier que $(L_{1}, L_{2}, L_{3})$ est une base de $\mathbb K_{2}[X]$.

		    2.  Exprimer les polynômes $L_{1}, L_{2}$ et $L_{3}$ en fonction de $a_{1}, a_{2}$ et $a_{3}$.

		3.  Soit $P\in\mathbb K_{2}[X]$. Déterminer les coordonnées de $P$ dans la base $(L_{1}, L_{2}, L_{3})$.

		4.  **Application** : on se place dans $\mathbb R^{2}$ muni d’un repère orthonormé et on considère les trois points $A(0,1), B(1,3), C(2,1)$.

		    Déterminer une fonction polynomiale de degré 2 dont la courbe passe par les points $A$, $B$ et $C$.






!!! exerciceb "Exercice <a name="enonce-27"></a><a href="#corrige-27">27</a>"
		  **IMT**   .
		Soient $P$ le plan vectoriel de $\mathbb{R}^3$ d'équation $x - 2y + 3z = 0$ et $D$ la droite vectorielle engendrée
		par $(2, 2, 1)$. Montrer que $P$ et $D$ sont supplémentaires et déterminer les matrices dans la base canonique
		du projecteur sur $P$ parallèlement à $D$ et de la symétrie par rapport à~$P$ parallèlement à $D$.





!!! exercice "Exercice <a name="enonce-28"></a><a href="#corrige-28">28</a>"
		   **CCINP**  .  
		Soit $f$ un endomorphisme d'un espace vectoriel $E$.


		Montrer que $\Ker(f)+\Im(f)=E$ si et seulement si $\Im(f)=\Im(f^2)$.






!!! exercice "Exercice <a name="enonce-29"></a><a href="#corrige-29">29</a>"


		Soit $E$ un $\mathbb{R}$-espace vectoriel muni d’un produit scalaire noté $(\:|\:)$.
		On pose $\forall\:x\in E$, $||x||=\sqrt{(x|x)}$.

		1.  1.  Énoncer et démontrer l’inégalité de Cauchy-Schwarz.

		    2.  Dans quel cas a-t-on égalité? Le démontrer.

		2.  Soit $E=\left\lbrace f\in\mathcal{C}\left( \left[ a,b\right] ,\mathbb{R}\right) ,\:\forall\:x\in \left[ a,b\right]\:f(x)>0  \right\rbrace$.
		    Prouver que l’ensemble $\left\lbrace \displaystyle\int_{a}^{b}f(t)\mathrm{d}t\times \displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t\:,\:f\in E\right\rbrace$ admet une borne inférieure $m$ et déterminer la valeur de $m$.






!!! exerciceb "Exercice <a name="enonce-30"></a><a href="#corrige-30">30</a>"


		Soit $E$ un espace euclidien.

		1.  Soit $A$ un sous-espace vectoriel de $E$.
		    Démontrer que $\left( A^{\perp }\right) ^{\perp }=A$.

		2.  Soient $F$ et $G$ deux sous-espaces vectoriels de $E$.

		    1.  Démontrer que  $\left( F+G\right) ^{\perp }=F^{\perp }\cap G^{\perp }$.

		    2.  Démontrer que $\left( F\cap G\right) ^{\perp }=F^{\perp}+G^{\perp }$.






!!! exercice "Exercice <a name="enonce-31"></a><a href="#corrige-31">31</a>"


		Soit $a$ et $b$ deux réels tels que $a<b$.

		1.  Soit $h$ une fonction continue et positive de $[a,b]$ dans $\mathbb{R}$.

		    Démontrer que $\displaystyle\int_{a}^{b}h(x)\text{d}x=0\Longrightarrow h=0$ .

		2.  Soit $E$ le $\mathbb{R}$-espace vectoriel des fonctions continues de $[a,b]$ dans $\mathbb{R}$.
		    On pose : $\forall\:(f,g)\in E^2$, $\left( f|g\right) =\displaystyle\displaystyle\int_{a}^{b}f(x)g(x)\text{d}x$.
		    Démontrer que l’on définit ainsi un produit scalaire sur $E$.

		3.  Majorer $\displaystyle\int_{0}^{1}\sqrt{x}e^{-x}\text{d}x$ en utilisant l’inégalité de Cauchy-Schwarz.






!!! exercice "Exercice <a name="enonce-32"></a><a href="#corrige-32">32</a>"


		Soit $E\ $l’espace vectoriel des applications continues et $2\pi$-périodiques de $\mathbb{R}$ dans $\mathbb{R}$.

		1.  Démontrer que $\left( f\ |\ g\right) =\dfrac{1}{2\pi }\displaystyle\int_{0}^{2\pi }f\left( t\right) g\left( t\right) \text{d}t$ définit un produit scalaire sur $E$.

		2.  Soit $F$ le sous-espace vectoriel engendré par $f:x\mapsto \cos x$ et $g:x\mapsto \cos \left( 2x\right)$.

		    Déterminer le projeté orthogonal sur $F$ de la fonction $u:x\mapsto \sin ^{2}x$.






!!! exerciceb "Exercice <a name="enonce-33"></a><a href="#corrige-33">33</a>"


		On définit dans $\mathcal{M}_{2}\left( \mathbb{R}\right) \times \mathcal{M}_{2}\left( \mathbb{R}\right)$ l’application $\varphi$ par : $\varphi \left( A,A'\right)
		=\text{tr}\left( ^{t}AA'\right)$, où $\text{tr}\left( ^{t}AA'\right)$ désigne la trace du produit de la matrice $^tA$ par la matrice $A'$.
		On admet que $\varphi$ est un produit scalaire sur $\mathcal{M}_{2}\left(
		\mathbb{R}\right)\ .$
		On note $\mathcal{F}=\left\{ \left(
		\begin{array}{cc}
		a & b \\
		-b & a%
		\end{array}%
		\right),\ \left( a,b\right) \in \mathbb{R}^{2}\right\}$.

		1.  Démontrer que $\mathcal{F}$ est un sous-espace vectoriel de $\mathcal{M}_{2}\left( \mathbb{R}\right)$.

		2.  Déterminer une base de $\mathcal{F}^{\perp }$.

		3.  Déterminer le projeté orthogonal de $J=\left(
		    \begin{array}{cc}
		    1 & 1 \\
		    1 & 1%
		    \end{array}%
		    \right)$ sur $\mathcal{F}^{\perp }$ .

		4.  Calculer la distance de $J$ à $\mathcal{F}$.






!!! exercice "Exercice <a name="enonce-34"></a><a href="#corrige-34">34</a>"


		Soit $E$ un espace préhilbertien et $F$ un sous-espace vectoriel de $E$ de dimension finie $n>0$.

		On admet que, pour tout $x\in E$, il existe un élément unique $y_0$ de $F$ tel que $x-y_0$ soit orthogonal à $F$ et que la distance de $x$ à $F$ soit égale à $\left\Vert x-y_0\right\Vert$.

		Pour $A=\begin{pmatrix}
		a & b \\
		c & d%
		\end{pmatrix}$ et $A'=\begin{pmatrix}
		a^{\prime } & b^{\prime } \\
		c^{\prime } & d^{\prime }%
		\end{pmatrix}$, on pose $\left(  A\ |\ A'\right) =aa^{\prime}+bb^{\prime}+cc^{\prime}+dd^{\prime}$.

		1.  Démontrer que $\left( \,.\,|\,.\,\right)$ est un produit scalaire sur $\mathcal{M}_{2}\left( \mathbb{R}\right)$.

		2.  Calculer la distance de la matrice $A=\left(
		    \begin{array}{cc}
		    1 & 0 \\
		    -1 & 2%
		    \end{array}%
		    \right)$ au sous-espace vectoriel $F$ des matrices triangulaires supérieures.






!!! exercice "Exercice <a name="enonce-35"></a><a href="#corrige-35">35</a>"


		Soit $n\in\mathbb{N}^*$. On considère $E=\mathcal{M}_n(\mathbb{R})$ l’espace vectoriel des matrices carrées d’ordre $n$.
		On pose : $\forall(A,B)\in E^2$, $\left\langle A\:,B \right\rangle=\mathrm{tr}({}^t\! AB)$ où tr désigne la trace et ${}^t\! A$ désigne la transposée de la matrice $A$.

		1.  Prouver que $\left\langle\:,\right\rangle$ est un produit scalaire sur $E$.

		2.  On note $S_n(\mathbb{R})$ l’ensemble des matrices symétriques de $E$.
		    Une matrice $A$ de $E$ est dite antisymétrique lorsque ${}^t\!A=-A$.
		    On note $A_n(\mathbb{R})$ l’ensemble des matrices antisymétriques de $E$.
		    On admet que $S_n(\mathbb{R})$ et $A_n(\mathbb{R})$ sont des sous-espaces vectoriels de $E$.

		    1.  Prouver que $E=S_n(\mathbb{R})\oplus A_n(\mathbb{R})$.

		    2.  Prouver que $A_n(\mathbb{R})^\perp=S_n(\mathbb{R})$.

		3.  Soit $F$ l’ensemble des matrices diagonales de $E$.
		    Déterminer $F^\perp$.






!!! exerciceb "Exercice <a name="enonce-36"></a><a href="#corrige-36">36</a>"
		  **CCINP**  .
		Dans $\mathbb{R}^3$, muni du produit scalaire canonique, on considère une droite de vecteur directeur unitaire $u$,
		et $p$ la projection orthogonale sur cette droite.


		 - a)
		Donner l'expression de $p(x)$ pour $x \in \mathbb{R}^3$.

		 - b)
		Soit $P $ le plan d'équation $x - 2y + z = 0$. Donner la matrice de la projection orthogonale $q$ sur ce plan dans la base canonique
		de $\mathbb{R}^3$.






!!! exercice "Exercice <a name="enonce-37"></a><a href="#corrige-37">37</a>"
		Soit $E$ un espace euclidien muni d'une base orthonormée $(e_1,\ldots,e_n)$.
		On considère une famille $(u_1,\ldots,u_n)$ de vecteurs de $E$ telle que, pour tout $k\in[\![1,n]\!]$, $\lVert u_k\rVert=\frac1n$. Est-ce que la famille $(e_1+u_1,\ldots,e_n+u_n)$ est une base de $E$?








!!! exerciceb "Exercice <a name="enonce-38"></a><a href="#corrige-38">38</a>"
		 **IMT**Trouver un équivalent de $\sum_{k=n+1}^{2n} \frac{1}{\sqrt{k}}$ en utilisant :

		- i) une comparaison série-intégrale,

		- ii) les sommes de Riemann.






!!! exerciceb "Exercice <a name="enonce-39"></a><a href="#corrige-39">39</a>"
		 **CCINP**
		On pose pour $n\in $ : $I_n=\int_0^{\pi/4} (\tan x)^n \mathrm d x$.

		- a) Déterminer $\lim_{n\to +\infty}I_n$.

		- b) Calculer $I_n+I_{n+2}$.

		- c) En déduire $\sum_{n=0}^{+\infty} \frac{(-1)^n}{2n+1}$. Ind. Faire apparaître un télescopage.

		- d) Prouver la convergence de $\sum (-1)^n I_n$ et calculer sa somme.





!!! exercice "Exercice <a name="enonce-40"></a><a href="#corrige-40">40</a>"


		1.  Donner la définition d’un argument d’un nombre complexe non nul (on ne demande ni l’interprétation géométrique, ni la démonstration de l’existence d’un tel nombre).

		2.  Soit $n\in{\mathbb{N}^{*}}$. Donner, en justifiant, les solutions dans $\mathbb{C}$ de l’équation $z^{n}=1$ et préciser leur nombre.

		3.  En déduire, pour $n\in{\mathbb{N}^{*}}$, les solutions dans $\mathbb{C}$ de l’équation $\left( z+\mathrm{i}\right)^{n}=\left(z-\mathrm{i}\right)^{n}$ et démontrer que ce sont des nombres réels.






!!! exerciceb "Exercice <a name="enonce-41"></a><a href="#corrige-41">41</a>"


		Soit $n \in {\mathbb{N}}$ tel que $n\geqslant 2$. On pose $z = e^{\mathrm{i}\,\frac{2\pi}{n}}$.

		1.  On suppose $k\in {\llbracket 1,n-1\rrbracket}$.
		    Déterminer le module et un argument du complexe $\:z^k - 1$.

		2.  On pose $\;S = \displaystyle\sum\limits_{k=0}^{n-1} \;\left|z^k - 1\right|$. Montrer que $\:S = \dfrac{2}{\tan\frac{\pi}{2n}}$.






!!! exerciceb "Exercice <a name="enonce-42"></a><a href="#corrige-42">42</a>"
		 **CCINP**

		- a) Résoudre, dans $\C$, l'équation $z^{n}=\mathrm{e}^{i\pi/3}$.
		- b) Résoudre, dans $\C$, l'équation $\left(\dfrac{z-1}{z+1}\right)^{n}+\left(\dfrac{z+1}{z-1}\right)^{n}=1$.





!!! exercice "Exercice <a name="enonce-43"></a><a href="#corrige-43">43</a>"
		  **CCINP**  .
		On note $\mathbb{U}_n$ l'ensemble des complexes solution de l'équation $z^n = 1$,
		et $\mathbb{U}$ l'ensemble des complexes de module 1.


		Pour $n\in ^*$, on écrit $\left({3 + 4i}\right)^n =A_n+iB_n$ avec $A_n$ et $B_n$ dans $\R$.

		 - a)
		Montrer que, pour $n\in ^*$, $\U_n\subset \U$. Montrer que, pour tout $n\in ^*$, $\left(\frac{3 + 4i}{5}\right)^n$
		appartient à $\mathbb{U}$.

		 - b)
		Exprimer $A_{n + 1}$ et $B_{n + 1}$ en fonction de $A_n$ et $B_n$.

		 - c)
		Montrer que les suites $(A_n)$ et $(B_n)$ sont à valeurs dans $\mathbb{Z}$.

		 - d)
		Montrer que, pour tout $n$, le reste de la division euclidienne de $A_n$ par 5 est 3, et le reste de la division euclidienne
		de $B_n$ par 5 est 4.

		 - e)
		En déduire que $\bigcup_{n\in ^*}\U_n \ne\U$.







!!! exercice "Exercice <a name="enonce-44"></a><a href="#corrige-44">44</a>"
		 **IMT**Soit $x\in\R$.  Calculer $\sum_{k=0}^n {n\choose k}\cos(kx)$.





!!! exercice "Exercice <a name="enonce-45"></a><a href="#corrige-45">45</a>"


		1.  Soient $n \in{\mathbb{N}^{*}}$, $P\in{\mathbb{R}_{n}\left[ X\right] }$ et $a \in{\mathbb{R}}$.

		    1.  Donner sans démonstration, en utilisant la formule de Taylor, la décomposition de $P(X)$ dans la base $\left( 1, X-a, \left( X-a\right) ^{2}, \cdots, (X-a)^{n}\right)$.

		    2.  Soit $r\in{\mathbb{N}^{*}}$. En déduire que :
		        $a$ est une racine de $P$ d’ordre de multiplicité $r$ si et seulement si $P^{(r)}(a)\neq 0$ et $\forall k \in{\llbracket 0,r-1 \rrbracket}$ , $P^{(k)}(a)=0$.

		2.  Déterminer deux réels $a$ et $b$ pour que $1$ soit racine double du polynôme $P=X^{5}+aX^{2}+bX$ et factoriser alors ce polynôme dans $\mathbb{R}\left[ X\right]$.






!!! exercice "Exercice <a name="enonce-46"></a><a href="#corrige-46">46</a>"


		Soient $a_{0},a_{1},\cdots ,a_{n}$ , $n+1$ réels deux à deux distincts.

		1.  Montrer que si $b_{0},b_{1},\cdots ,b_{n}\;$sont$\;n+1\;$ réels quelconques, alors il existe un unique polynôme $P$ vérifiant

		$$
		\deg P\leqslant n \:\:\text{et}\:\:\forall i\in \llbracket 0,n\rrbracket,\:\:P\left(
		    a_{i}\right) =b_{i}.
		$$

		2.  Soit $k\in \llbracket 0,n \rrbracket$.
		Expliciter ce polynôme $P$, que l’on notera $L_{k}$, lorsque :

		$$
		\forall i\in \llbracket 0 ,n \rrbracket,  \ \ b_{i}=
		\left\{
		\begin{array}{lll}
		0&\text{si}&i\neq k \\
		1&\text{si}&i=k
		\end{array}
		\right.
		$$

		3.  Prouver que $\forall p\in \llbracket 0 ,n \rrbracket$ , $\displaystyle\sum\limits_{k=0}^{n}a_{k}^{p}L_{k}=X^{p}$.






!!! exerciceb "Exercice <a name="enonce-47"></a><a href="#corrige-47">47</a>"
		 **CCINP**.  
		 Soit $P=\left(X+1\right)^{n}-1$.

		- a) Déterminer les racines de $P$ et factoriser $P$ dans $\C[X]$.
		- b)   Calculer $ \prod_{k=1}^{n-1}\sin\left(\dfrac{k\pi}{n}\right)$.





!!! exercice "Exercice <a name="enonce-48"></a><a href="#corrige-48">48</a>"
		Soit $P\in\mathbb Q_n[X]$. Montrer l'équivalence entre les propriétés:

		(i)  pour tout $k\in\mathbb Z$, $P(k)\in\mathbb Z$,

		(ii)   pour tout $k\in[\![  0,n]\!]$, $P(k)\in\mathbb Z$,

		(iii)   il existe $m\in\mathbb Z$ tel que, pour tout $k\in[\![m, m+n]\!]$, $P(k)\in\mathbb Z$.


		***Ind.*** On pourra introduire les polynômes $H_k=\frac1{k!}X(X-1)\cdots(X-k+1)$.






!!! exerciceb "Exercice <a name="enonce-49"></a><a href="#corrige-49">49</a>"


		1.  Énoncer et démontrer la formule de Bayes pour un système complet d’événements.

		2.  On dispose de 100 dés dont 25 sont pipés (c’est-à-dire truqués).
		    Pour chaque dé pipé, la probabilité d’obtenir le chiffre 6 lors d’un lancer vaut $\dfrac{1}{2}$.

		    1.  On tire un dé au hasard parmi les 100 dés. On lance ce dé et on obtient le chiffre 6.
		        Quelle est la probabilité que ce dé soit pipé?

		    2.  Soit $n\in\mathbb{N}^*.$
		        On tire un dé au hasard parmi les 100 dés. On lance ce dé $n$ fois et on obtient $n$ fois le chiffre $6$.
		        Quelle est la probabilité $p_n$ que ce dé soit pipé?

		    3.  Déterminer $\lim\limits_{n\to+\infty}^{}p_n$. Interpréter ce résultat.






!!! exercice "Exercice <a name="enonce-50"></a><a href="#corrige-50">50</a>"


		On dispose de deux urnes $U_1$ et $U_2$.
		L’urne $U_1$ contient deux boules blanches et trois boules noires.
		L’urne $U_2$ contient quatre boules blanches et trois boules noires.
		On effectue des tirages successifs dans les conditions suivantes:
		on choisit une urne au hasard et on tire une boule dans l’urne choisie.
		On note sa couleur et on la remet dans l’urne d’où elle provient.
		Si la boule tirée était blanche, le tirage suivant se fait dans l’urne $U_1$.
		Sinon le tirage suivant se fait dans l’urne $U_2$.
		Pour tout $n\in\mathbb{N}^*$, on note $B_n$ l’événement « la boule tirée au $n^{\text{ième}}$ tirage est blanche » et on pose $p_n=P(B_n)$.

		1.  Calculer $p_1$.

		2.  Prouver que : $\forall\:n\in\mathbb{N}^*$, $p_{n+1}=-\dfrac{6}{35}p_n+\dfrac{4}{7}$.

		3.  En déduire, pour tout entier naturel $n$ non nul, la valeur de $p_n$.






!!! exercice "Exercice <a name="enonce-51"></a><a href="#corrige-51">51</a>"


		Soit $n\in\mathbb{N}^*$ et $E$ un ensemble possédant $n$ éléments.
		On désigne par $\mathcal{P}(E)$ l’ensemble des parties de $E$.

		1.  Déterminer le nombre $a$ de couples $(A,B)\in \left(\mathcal{P}(E) \right)^2$ tels que $A\subset B$.

		2.  Déterminer le nombre $b$ de couples $(A,B)\in \left(\mathcal{P}(E) \right)^2$ tels que $A\cap B=\emptyset$.

		3.  Déterminer le nombre $c$ de triplets $(A,B,C)\in \left(\mathcal{P}(E) \right)^3$ tels que $A$, $B$ et $C$ soient deux à deux disjoints et vérifient $A\cup B\cup C=E$.






!!! exerciceb "Exercice <a name="enonce-52"></a><a href="#corrige-52">52</a>"


		Une urne contient deux boules blanches et huit boules noires.

		1.  Un joueur tire successivement, avec remise, cinq boules dans cette urne.
		    Pour chaque boule blanche tirée, il gagne 2 points et pour chaque boule noire tirée, il perd 3 points.
		    On note $X$ la variable aléatoire représentant le nombre de boules blanches tirées.
		    On note $Y$ le nombre de points obtenus par le joueur sur une partie.

		    1.  Déterminer la loi de $X$, son espérance et sa variance.

		    2.  Déterminer la loi de $Y$, son espérance et sa variance.

		2.  Dans cette question, on suppose que les cinq tirages successifs se font sans remise.

		    1.  Déterminer la loi de $X$.

		    2.  Déterminer la loi de $Y$.






!!! exerciceb "Exercice <a name="enonce-53"></a><a href="#corrige-53">53</a>"


		Une secrétaire effectue, une première fois, un appel téléphonique vers $n$ correspondants distincts.
		On admet que les $n$ appels constituent $n$ expériences indépendantes et que, pour chaque appel, la probabilité d’obtenir le correspondant demandé est $p$ ($p\in{\left]  0,1\right[ }$).
		Soit $X$ la variable aléatoire représentant le nombre de correspondants obtenus.

		1.  Donner la loi de $X$. Justifier.

		2.  La secrétaire rappelle une seconde fois, dans les mêmes conditions, chacun des $n-X$ correspondants qu’elle n’a pas pu joindre au cours de la première série d’appels. On note $Y$ la variable aléatoire représentant le nombre de personnes jointes au cours de la seconde série d’appels.

		    1.  Soit $i\in \llbracket 0,n \rrbracket$. Déterminer, pour $k\in \mathbb{N},$ $P(Y=k|X=i)$.

		    2.  Prouver que $Z=X+Y$ suit une loi binomiale dont on déterminera le paramètre.

		        **Indication** : on pourra utiliser, sans la prouver, l’égalité suivante: $\dbinom{n-i}{k-i}\dbinom{n}{i}=\dbinom{k}{i}\dbinom{n}{k}$.

		    3.  Déterminer l’espérance et la variance de $Z$.






!!! exercice "Exercice <a name="enonce-54"></a><a href="#corrige-54">54</a>"

	  (erreur, non accessible en sup)
		Soit $N\in\mathbb{N}^*$.
		Soit $p\in\left] 0,1\right[$. On pose $q=1-p$.
		On considère $N$ variables aléatoires $X_1,X_2,\cdots,X_N$ définies sur un même espace probabilisé $\left(\Omega,\mathcal{A},P\right)$, mutuellement indépendantes et de même loi géométrique de paramètre $p$.

		1.  Soit $i\in\llbracket1,N\rrbracket$. Soit $n\in{\mathbb{N}^*}$.
		    Déterminer $P(X_i\leqslant n)$, puis $P(X_i> n)$.

		2.  On considère la variable aléatoire $Y$ définie par $Y=\underset{1\leqslant i\leqslant N}{\min}(X_i)$
		    c’est-à-dire $\forall \omega \in\Omega$, $Y(\omega)=\min\left(X_1(\omega),\cdots,X_N(\omega)\right)$, $\min$ désignant « le plus petit élément de ».

		    1.  Soit $n\in{\mathbb{N}^*}$. Calculer $P(Y>n)$.
		        En déduire $P(Y\leqslant n)$, puis $P(Y=n)$.

		    2.  Reconnaître la loi de $Y$. En déduire $E(Y)$.






!!! exercice "Exercice <a name="enonce-55"></a><a href="#corrige-55">55</a>"


		Soit $n$ un entier naturel supérieur ou égal à 3.
		On dispose de $n$ boules numérotées de $1$ à $n$ et d’une boîte formée de trois compartiments identiques également numérotés de 1 à 3.
		On lance simultanément les $n$ boules.
		Elles viennent toutes se ranger aléatoirement dans les 3 compartiments.
		Chaque compartiment peut éventuellement contenir les $n$ boules.
		On note $X$ la variable aléatoire qui à chaque expérience aléatoire fait correspondre le nombre de compartiments restés vides.

		1.  Préciser les valeurs prises par $X$.

		2.  1.  Déterminer la probabilité $P(X=2)$.

		    2.  Finir de déterminer la loi de probabilité de $X$.

		3.  1.  Calculer $E(X)$.

		    2.  Déterminer $\lim\limits_{n\to +\infty}^{}E(X)$. Interpréter ce résultat.






!!! exercice "Exercice <a name="enonce-56"></a><a href="#corrige-56">56</a>"


		Soit $n\in\mathbb{N}^*$. Une urne contient $n$ boules blanches numérotées de 1 à $n$ et deux boules noires numérotées 1 et 2.
		On effectue le tirage une à une, sans remise, de toutes les boules de l’urne.
		On note $X$ la variable aléatoire égale au rang d’apparition de la première boule blanche.
		On note $Y$ la variable aléatoire égale au rang d’apparition de la première boule numérotée 1.

		1.  Déterminer la loi de $X$.

		2.  Déterminer la loi de $Y$.






!!! exercice "Exercice <a name="enonce-57"></a><a href="#corrige-57">57</a>"
		  **IMT**   .
		Soient $n \in \mathbb{N}$ et $X, Y$ deux variables aléatoires à valeurs dans $ [\![  1, n + 1  ]\!] $, de loi conjointe
		donnée par $\forall (i, j) \in  [\![  1, n + 1  ]\!] ^2, \ \P(X = i, Y = j) = \frac{1}{2^{2n}} \binom{n}{i - 1} \binom{n}{j - 1}$.


		 - a)
		Vérifier que $\forall i \in  [\![  1, n + 1  ]\!] , \ \P(X = i) = \frac{1}{2^n}\binom{n}{i - 1}$.
		Les variables $X$ et $Y$ sont-elles indépendantes ?

		 - b)
		Quelle loi suit $X - 1$ ? Calculer l'espérance et la variance de $X$.

		 - c)
		Calculer $\E(e^X)$.






!!! exercice "Exercice <a name="enonce-58"></a><a href="#corrige-58">58</a>"
		Soit $X_{1}, \ldots, X_{n+1}$ des variables aléatoires indépendantes qui suivent la loi uniforme sur $[\![ 1, n ]\!]$. On pose $T=\min \left\{j \in [\![2, n+1 ]\!] ; X_{j} \in\left\{X_{1}, \ldots, X_{j-1}\right\}\right\}$.  
		- Vérifier que $T$ est bien défini.  
		- Ecrire une fonction qui prend $\left(X_{1}, \ldots, X_{n+1}\right)$ en arguments et qui renvoie $T$.  
		- On choisit $n=1000$. Ecrire une fonction qui prend $N$ en argument et qui renvoie la moyenne de $T$ au bout de $N$ essais.  
		- Quelles sont les valeurs que peut prendre $T$ ?  
		- Déterminer la loi de $T$. - Montrer que $E(T)=\sum_{k=0}^{n} P(T>k)$ et en déduire que $\mathrm{E}(T)=\frac{n!}{n^{n}} \sum_{k=0}^{n} \frac{n^{k}}{k !}$.





!!! exercice "Exercice <a name="enonce-59"></a><a href="#corrige-59">59</a>"
		Soient $\left(a_{n}\right)_{n \geq 1} \in\left(\mathbb{R}^{+}\right)^{\mathbf{N}^{*}}$ et $A_{n}=\left(\begin{array}{ccccc}1 & -1 & 0 & \cdots & 0 \\ a_{1} & 1 & -1 & \ddots & \vdots \\ 0 & \ddots & \ddots & \ddots & 0 \\ \vdots & \ddots & \ddots & \ddots & -1 \\ 0 & \ldots & 0 & a_{n} & 1\end{array}\right) \in \mathcal{M}_{n+1}(\mathbb{R})$ pour $n \in \mathbb{N}^{*}$.  
		- Écrire une fonction python calculant et affichant le déterminant $\Delta_{n}$ de $A_{n}$ pour les petites valeurs de $n$.  
		- Exécuter le programme lorsque $a_{n}=1$ et lorsque $a_{n}=1 / n^{2}$. Que peut-on dire ?  
		- Montrer que, lorsque $a_{n}=1$, la suite $\left(\Delta_{n}\right)_{n}$ est divergente. Donner un équivalent de $\Delta_{n}$ quand $n$ tend vers l'infini.  
		- Montrer que $\Delta_{n} \leq \prod_{k=1}^{n}\left(1+a_{k}\right)$  
		- Montrer que la suite $\left(\Delta_{n}\right)$ converge si et seulement si $\sum a_{n}$ converge.





!!! exercice "Exercice <a name="enonce-60"></a><a href="#corrige-60">60</a>"
		Soit $Q:x\mapsto (x-1)(x^2-2)^2$, $f:x\mapsto x-\frac{Q(x)}{Q'(x)}$ et enfin $(u_n)_n$ la suite définie par $u_0=10$ et, pour tout $n\in\mathbb N$, $u_{n+1}=f(u_n)$.

		- a) Écrire une fonction Python qui prend un entier $n$ en argument et renvoie les $n$ premiers termes de cette suite. Que peut-on conjecturer sur la limite de cette suite?  
		- b) Montrer que $Q'$ est scindé à racines simples dans $[-\sqrt{2},\sqrt{2}]$. On admet qu'il en est de même pour $Q''$.  
		- c) Étudier les variations de $f$.  
		- d) Écrire une fonction Python qui prend un entier $n$ en argument et trace les points de coordonnées $(k,\ln(u_k-\sqrt{2}))$ pour tout $k\leq n$. Que peut-on conjecturer?  
		- e) Montrer que, pour tout $n\in\mathbb N$, il existe
		  $c_n>\sqrt{2}$ tel que $u_{n+1}-\sqrt{2}=f'(c_n)(u_n-\sqrt{2})$.  
		- f) Montrer que la limite de $f'$ en $\sqrt{2}$ par valeurs supérieures est $1/2$.  
		- g) En déduire la preuve de la conjecture précédente.  





!!! exercice "Exercice <a name="enonce-61"></a><a href="#corrige-61">61</a>"

		Soit $P\in\mathbb C[X]$ un polynôme non constant, dont aucune racine n'est de module 1.

		- a)
		Justifier l'existence de $M(P)=\int_{0}^{2\pi}\ln\left|P\left(e^{i t}\right)\right|\,{\rm d} t$.

		- b)
		Montrer qu'il existe un réel $A$, un entier naturel $s$ supérieur ou égal à 1,
		des complexes $z_1,\ldots,z_s$ de modules différents de 1 et des entiers $m_k$ tels que:


		$$M(P)=A+\sum_{k=1}^{s}m_{k}M\left(X-z_{k}\right).$$

		- c)
		Écrire une fonction `malher(r,theta)` renvoyant la valeur de
		$M(X-re^{i\theta})$.
		Quelle est la valeur de $M(2,50)$?
		Pour $r\in\left\{0.5,1,100,2019\right\}$ représenter la fonction
		$\theta\mapsto\mathrm{malher}(r,\theta)$ avec un pas de $0.1$ pour $\theta$.
		Que peut-on conjecturer ?

		- d)
		Représenter $r\mapsto\mathrm{malher}(r,\theta)$ pour $r$ variant de 0 à 3 avec un pas de $0.1$;
		puis $r\mapsto\mathrm{malher}(r,\theta)$ pour $r$ variant de 1 à 20 avec un pas de $0.1$.
		Représenter sur un même graphique $r\mapsto 2\pi\ln(r)$.
		Que peut-on conjecturer ?

		- e)
		Démontrer la conjecture faite à la question c).

		- f)
		Montrer la conjecture de la question d).





!!! exercice "Exercice <a name="enonce-62"></a><a href="#corrige-62">62</a>"
		Soit $\phi: \mathbb{N} \rightarrow \mathbb{N}$ telle que $\phi(0)=0, \phi(1)=1$ et :
		$\forall n \in \mathbb{N}, \phi(2 n)=\phi(n), \phi(2 n+1)=\phi(n)+\phi(n+1)$
		- a) Coder en python la fonction pgcd.  
		- b) Coder la fonction $\phi$.  
		- c) Calculer à l'aide de python certaines valeurs de $\operatorname{pgcd}(\phi(n), \phi(n+1))$. Faire une conjecture et la démontrer.  

		Soient $P=\left\{(a, b) \in \mathbb{N}^{2}, \operatorname{pgcd}(a, b)=1\right\}$ et $\Phi: n \in \mathbb{N} \mapsto(\phi(n), \phi(n+1))$.  
		- d) Montrer que $\Phi(\mathbb{N})=P$.  
		- e) Montrer que $\Phi$ est injective. En déduire une bijection entre $\mathbb{Q}^{+}$et $\mathbb{N}$.  
		- f) Coder la fonction réciproque de $\phi$, en complexité raisonnable. Tester pour $3^{100}$.  





!!! exercice "Exercice <a name="enonce-63"></a><a href="#corrige-63">63</a>"
		Pour $n \in \mathbb{N}^{*}$, on pose $H_{n}=\sum_{k=1}^{n} \frac{1}{k}$ et l'on note $H_{n}=\frac{A_{n}}{B_{n}}$, avec $A_{n} \wedge B_{n}=1$.

		On souhaite étudier la propriété $(\mathcal{P})$ : «pour tout $p$ premier, on a $p^{2} \mid A_{p-1}$.»  
		- a) i) Écrire une fonction pgcd qui renvoie le pgcd de deux entiers.  
		  - ii) Déterminer, en fonction d'un pgcd et de $A_{k-1}, B_{k-1}$, les expressions de $A_{k}$ et $B_{k}$.  
		  - iii) Écrire une fonction qui calcule $A_{n}$ pour un $n$ donné. Calculer $A_{16}$.  
		  - iv) Vérifier la propriété $\mathcal{P}$ jusqu'à 500  

		On pourra utiliser sympy . primerange ou sympy .nextprime.

		- b) Montrer le théorème de Wilson : $p$ est premier si eSt seulement si $(p-1) ! \equiv-1[p]$.

		On considère désormais un nombre premier $p \geqslant 5$.

		- c) Pour $k \in [\![ 1, p-1 ]\!]$, on pose $a_{k}=\frac{(p-1) !}{k(p-k)} \cdot$ Exprimer $\sum_{k=1}^{p-1} a_{k}$ en fonction de $H_{p-1}$. En déduire que $p$ divise $A_{p-1}$.

		- d) Montrer que $k^{2} a_{k} \equiv 1[p]$ pour tout $k \in [\![ 1, p-1 ]\!]$. En déduire que $p$ divise $\sum_{k=1}^{p-1} a_{k}$ et conclure.





!!! exercice "Exercice <a name="enonce-64"></a><a href="#corrige-64">64</a>"
		On dispose de $n$ urnes $U_1,\dots , U_n$ et $n$ boules numérotées de $1$ à $n$, que l'on place  indépendamment dans les urnes, chaque boule ayant la probabilité $\tfrac1n$ d'être placée dans l'urne $U_i$ pour chaque $i\in[\![1,n]\!]$. On note $X_n$ le nombre d'urnes vides après le placement des $n$ boules.

		- a) Écrire une fonction Python `different` qui prend une liste $l$ et qui renvoie le nombre d'éléments distincts de $l$. Par exemple, `different([1,2,3,1,2])` renvoie $3$.  
		- b) Écrire une fonction Python `simulX` qui prend un entier $n$ et renvoie une simulation de $X_n$.  
		- c) Pour tout $i\in[\![1,n]\!]$, on note $Y_i$ la variable aléatoire valant $1$ si l'urne $U_i$ est vide, $0$ sinon. Déterminer la loi de $Y_i$, son espérance et sa variance.  
		- d) Montrer que $X_n=Y_1+\cdots + Y_n$. Est-ce que la loi de $X_n$ est binomiale?  
		- e) Calculer l'espérance de $X_n$.  
		- f) Écrire une fonction Python `esperanceX` qui prend en entrée un entier $n$ et renvoie une valeur approchée de $\mathbb E(X_n)$.  
		- g) Calculer la covariance de $Y_i$ et $Y_j$ pour  $i\neq j$.
		    En déduire la variance de $X_n$.





!!! exercice "Exercice <a name="enonce-65"></a><a href="#corrige-65">65</a>"
		Pour $n \in \mathbb{N}^{*}$, soit $\left(E_{n}\right)$ l'équation : $x^{n}+x^{n-1}+\cdots+x^{2}+x-1=0$.  
		- a) Rappeler la formule de Stirling.  
		- b) Montrer que l'équation $\left(E_{n}\right)$ admet une unique solution dans $\left[0,+\infty\right[$, que l'on note $u_{n}$.  
		- c) Montrer que, pour tout $n \in \mathbb{N}^{*}, u_{n} \in] 0,1[$. Étudier les variations de la suite $\left(u_{n}\right)$ et en déduire que la suite $\left(u_{n}\right)_{n \in \mathbb{N}}$ converge. On note $\ell$ sa limite.  
		- d) À l'aide de l'outil informatique, écrire une fonction $u(n)$ qui retourne $u_{n}$ pour tout $n \geqslant 1$. Conjecturer la valeur de $\ell$. On pose $v_{n}=2^{n}\left(u_{n}-\ell\right)$. Conjecturer le comportement asymptotique de la suite $\left(v_{n}\right)$.  
		- e) Montrer que, pour tout $n \in \mathbb{N}^{*}$, $u_{n}^{n+1}-2 u_{n}+1=0$. Démontrer la conjecture faite sur $\ell$.  
		- f) Montrer que $n\left(u_{n}-\ell\right) \longrightarrow 0$. En déduire la limite de la suite $\left(v_{n}\right)$.





!!! exercice "Exercice <a name="enonce-66"></a><a href="#corrige-66">66</a>"
		Soient $n \in \mathbb{N}^{*},\left(X_{i, j}\right)_{1 \leqslant i \leqslant j \leqslant n}$ des variables aléatoires mutuellement indépendantes, chacune de loi uniforme sur $\{-1,1\}$. Soit $A_{n}$ la matrice symétrique de taille $n$ dont le coefficient d'indice $(i, j)$ est $X_{i, j}$.  
		- a) Montrer que $\mathbf{E}\left(A_{n}^{2}\right)=n I_{n}$ où $\mathbf{E}\left(A_{n}^{2}\right)$ est la matrice des espérances des coefficients de $A_{n}^{2}$.  
		- b) Écrire une fonction PYTHON sym(n) qui retourne $A_{n}$.  
		- c) Écrire une fonction PYTHON esperanceDet $(\mathrm{n})$ qui retourne $\Delta_{n}=\mathbf{E}\left(\operatorname{det}\left(A_{n}\right)\right)$.  
		- d) Tester pour $n=1, \ldots, 6$. Établir une conjecture sur l'expression de $\Delta_{2 n+1}$ puis sur $\Delta_{2 n}$. Comparer avec $1 \times 3 \times \cdots \times(2 n-1)$.  
		- e) Démontrer cette conjecture pour $\Delta_{1}$ et $\Delta_{2}$.  
		- f) Démontrer cette conjecture pour $\Delta_{2 n+1}$.  
		- g) Montrer que $\Delta_{2 n}$ est le nombre de permutations de $[\![1,2 n ]\!]$ qui sont le produit de $n$ transpositions disjointes et calculer ce nombre.





!!! exercice "Exercice <a name="enonce-67"></a><a href="#corrige-67">67</a>"
		Soit $n\in\mathbb N^*$. On s'intéresse au problème $\mathrm{FL}(n)$ suivant : étant donnés $a_0,\dots,a_{n-1}, \lambda_0,\dots,\lambda_{n-1}\in\mathbb C$, existe-t-il une matrice dont les coefficients diagonaux sont $a_0,\dots,a_{n-1}$ et les valeurs propres sont $\lambda_0,\dots,\lambda_{n-1}$ ?

		- a)   Ecrire une fonction Python prenant en entrée des complexes $a_0,\dots,a_{n-1},\mu_0,\dots,\mu_{n-1},\alpha$ et renvoyant la matrice
		$\begin{pmatrix}
		a_0&1&0&\cdots&0 \\
		0&\ddots  &\ddots  &\ddots  &\vdots \\
		\vdots&\ddots  &\ddots  &\ddots  &0 \\
		0&\cdots&0&a_{n-1}&1 \\
		\mu_0&\cdots&\cdots&\mu_{n-1}&\alpha
		\end{pmatrix}.$

		- b)    Montrer que le problème $\mathrm{FL}(2)$ admet une solution si et seulement si $a_0 + a_1 = \lambda_0+\lambda_1$. Préciser une solution sous cette condition.

		- c)   Le problème $\mathrm{FL}(3)$ admet-il une solution pour $a_0 = 1, a_1 = -1, a_2 = 0, \lambda_0 = 1, \lambda_1 = 2$ et $\lambda_2 = -3$ ?

		Soient $a_0 \ldots,a_{n-1}\in\mathbb C$. On pose, pour $k\in[\![  0,n - 1]\!]    $, $P_k = \prod_{i=0}^{k-1}(X - a_i)$.  

		- d)   Justifier que la famille $(P_0,\dots,P_n)$ est une base de $\mathbb R_n[X]$, puis qu'il existe $\mu_0,\dots,\mu_{n-1}$ tels que $ \prod_{k=0}^{n-1}(X - \lambda_k) = P_n -\sum_{k=0}^{n-1}\mu_kP_k.$  
		- e)   Ecrire une fonction Python prenant en entrée  $a_0,\dots,a_{n-1}$ et renvoyant  $[P_0,\dots,P_n]$.  
		- f)   Ecrire une fonction Python prenant en entrée $a_0,\dots,a_{n-1}, \lambda_0,\dots,\lambda_{n-1}$ et renvoyant les $\mu_k$ .





!!! exercice "Exercice <a name="enonce-68"></a><a href="#corrige-68">68</a>"
		Soit $(u_n)$ définie par $u_0\in[0,\pi]$ et, pour  $n\in\mathbb N$, $u_{n+1}=\sum_{k=0}^n\sin\left(\frac{u_k}{n+1}\right)$.

		a)   \'Ecrire un programme Python qui prend un entier $n$ et qui renvoie les $n$ premières valeurs de cette suite. Discuter sa complexité en temps et en mémoire.

		b)
		           Montrer que, pour  $x\in[0,\pi]$, $x-\frac{x^3}{6}\leq \sin x\leq x$.

		c)  On considère $v_n=\frac1{n+1}\sum_{k=0}^nu_k$ pour tout $n\in\mathbb N$.

		i)   Montrer que $u_n\in[0,\pi]$ pour tout $n\in\mathbb N$.

		ii) Montrer que $v_n-\frac{\pi^3}{6(n+1)^2}\leq u_{n+1}\leq v_n$ pour tout $n\in\mathbb N$.

		En déduire que $-\frac{\pi^3}{6(n+1)^3}\leq v_{n+1}-v_n\leq 0$ pour tout $n\in\mathbb N$.

		iii)   En déduire la convergence de $(u_n)$.





!!! exercice "Exercice <a name="enonce-69"></a><a href="#corrige-69">69</a>"
		Soit $f:x\mapsto\int_x^{x^2}\frac{\mathrm d    t}{\ln (t)}.$

		- a)   Montrer que $f$ est définie sur $\mathbb R^{+*}\setminus\{1\}$.  
		- b)   Avec Python, tracer la courbe représentative de $f$ sur $[0,3]$. Si $f$ admet une limite finie en $1$, la comparer avec $\ln 2$.  
		- c)   Montrer que $x\ln 2\leq f(x)\leq x^2\ln 2$ pour $x\in]1,2]$. On pourra écrire $\frac1{\ln (t)}=t\frac1{t\ln (t)}$.  
		- d)   En déduire que $f$ admet une limite à droite en $1$. Que dire pour la limite à gauche?  
		- e)   Soit $\tilde{f}$ le prolongement continu de $f$ sur $\mathbb R^{+*}$. Avec le tracé précédemment obtenu, $\tilde{f}$ semble-t-elle dérivable en $1$? Montrer que $f$ est de classe $\mathcal C^1$ sur $\mathbb R^{+*}$.   
		- f)   Tracer cette tangente avec Python.





!!! exercice "Exercice <a name="enonce-70"></a><a href="#corrige-70">70</a>"
		Deux amis se sont donné rendez-vous à 18 heures. Ils arrivent en retard de $X$ et $Y$ minutes
		respectivement. On suppose que $X$ et $Y$ sont des variables aléatoires indépendantes suivant la loi uniforme
		sur $\{0,\ldots,59\}$.

		a) À quoi correspond la variable $T=|X-Y|$ ?

		b) Donner la loi de $T$.

		c) Écrire une fonction `rdv(n)` qui renvoie les résultats de $n$ simulations de $T$.

		d) i) Calculer la valeur exacte de l'espérance de $T$.

		ii) Donner une approximation de l'espérance avec Python. Commenter l'écart.

		e) On pose $n=10^5$. Donner une fonction qui renvoie approximativement la loi de $T$ à l'aide de la fonction `rdv`. Commenter les écarts.

		f) On découpe une heure en $N$ divisions de temps. Donner un équivalent de la probabilité que les amis arrivent en même temps lorsque $N$ tend vers $+\infty$.



<br><br><div style='page-break-before:always'></div>
## Corrigés

!!! exerciceb "Corrigé <a name="corrige-1"></a><a href="#enonce-1">1</a>"


		1.  Par hypothèse, $\exists\:N_0\in\mathbb{N}/\forall\:n\in\mathbb{N}, n\geqslant N_0\Longrightarrow v_n\neq 0$.
		    Ainsi la suite $\left( \dfrac{u_n}{v_n}\right)$ est définie à partir du rang $N_0$.
		    De plus, comme $u_{n}\underset{+\infty}\thicksim v_{n}$, on a $\lim\limits_{n\to +\infty}^{}\dfrac{u_n}{v_n}=1$.
		    Alors, $\forall\:\varepsilon>0$, $\exists N\in\mathbb{N}/N\geqslant N_0$ et $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow\left|\dfrac{u_n}{v_n}-1\right|\leqslant\varepsilon$.(1)
		    Prenons $\varepsilon=\dfrac{1}{2}$. Fixons un entier $N$ vérifiant $(1)$.
		    Ainsi, $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow\left|\dfrac{u_n}{v_n}-1\right|\leqslant\dfrac{1}{2}$.
		    C’est-à-dire, $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow -\dfrac{1}{2}\leqslant\dfrac{u_n}{v_n}-1\leqslant\dfrac{1}{2}$.
		    On en déduit que $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow \dfrac{u_n}{v_n}\geqslant \dfrac{1}{2}$.
		    Et donc, $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow \dfrac{u_n}{v_n}> 0$.
		    Ce qui implique que $u_n$ et $v_n$ sont de même signe à partir du rang $N$.

		2.  Au voisinage de $+ \infty$, $\textrm{sh} (\dfrac{1}{n}) = \dfrac{1}{n} + \dfrac{1}{{6n^3 }} + o\left( {\dfrac{1}{{n^3 }}} \right)\text{ et }\tan \dfrac{1}{n} = \dfrac{1}{n} + \dfrac{1}{{3n^3 }} + o\left( {\dfrac{1}{{n^3 }}} \right)$. Donc $u_n  \underset{+\infty}\thicksim - \dfrac{1}{{6n^3 }}$.
		    On en déduit, d’après 1., qu’à partir d’un certain rang, $u_n$ est négatif.





!!! exerciceb "Corrigé <a name="corrige-2"></a><a href="#enonce-2">2</a>"


		On pose $f(x)=\mathrm{Arctan}\: x$.

		1.  1.  **Premier cas**: Si $u_1<u_0$
		        Puisque la fonction $f:x \mapsto \mathrm{Arctan}\: x$ est strictement croissante sur $\mathbb{R}$ alors $\mathrm{Arctan}(u_1)< \mathrm{Arctan}(u_0)$ c’est-à-dire $u_2 < u_1$.
		        Par récurrence, on prouve que $\forall n \in{\mathbb{N}} \:,\: u_{n+1} < u_n$. Donc la suite $(u_n)$ est strictement décroissante.  
		        **Deuxième cas**: Si $u_1>u_0$
		        Par un raisonnement similaire, on prouve que la suite $(u_n)$ est strictement croissante.  
		        **Troisième cas** : Si $u_1=u_0$
		        La suite $(u_n)$ est constante.

		        Pour connaître les variations de la suite $(u_n)$, il faut donc déterminer le signe de $u_1-u_0$, c’est-à-dire le signe de $\mathrm{Arctan} (u_0)-u_0$.
		        On pose alors $g(x)=\mathrm{Arctan} x -x$ et on étudie le signe de la fonction $g$.
		        On a $\forall x\in{\mathbb{R} }$, $g'(x)=\dfrac{-x^2}{1+x^2}$ et donc $\forall\:x\in\mathbb{R}^*$, $g'(x)<0$.
		        Donc $g$ est strictement décroissante sur $\mathbb{R}$ et comme $g(0)=0$ alors :
		        $\forall x\in{ \left]  0,+\infty\right[ }$, $g(x)< 0$ et $\forall x\in{ \left]-\infty,0\right[ }$, $g(x)> 0$.
		        On a donc trois cas suivant le signe de $x_0$:
		        - Si $x_0>0$, la suite$(u_n)$ est strictement décroissante.
		        - Si $x_0=0$, la suite $(u_n)$ est constante.
		        - Si $x_0<0$, la suite$(u_n)$ est strictement croissante.

		    2.  La fonction $g$ étant strictement décroissante et continue sur $\mathbb{R}$, elle induit une bijection de $\mathbb{R}$ sur $g(\mathbb{R})=\mathbb{R}$.
		        $0$ admet donc un unique antécédent par $g$ et, comme $g(0)=0$, alors 0 est le seul point fixe de $f$.
		        Donc si la suite $(u_n)$ converge, elle converge vers 0, le seul point fixe de $f$.  
		        **Premier cas**: Si $u_0>0$
		        L’intervalle $\left]  0,+\infty\right[$ étant stable par $f$, on a par récurrence, $\forall n\in\mathbb{N}$, $u_n > 0$. Donc la suite $(u_n)$ est décroissante et minorée par $0$, donc elle converge et ce vers 0, unique point fixe de $f$.  
		        **Deuxième cas**: Si $u_0<0$
		        Par un raisonnement similaire, on prouve que $(u_n)$ est croissante et majorée par 0, donc elle converge vers 0.   
		        **Troisième cas**: Si $u_0=0$
		        La suite $(u_n)$ est constante.

		    Conclusion: $\forall\:u_0\in\mathbb{R}$, $(u_n)$ converge vers 0.

		2.  Soit $h$ une fonction continue sur $\mathbb{R}$ telle que, $\forall x \in \mathbb{R}$, $h(x)=h(\mathrm{Arctan}\: x)$.
		    Soit $x \in \mathbb{R}$.
		    Considérons la suite $(u_n)$ définie par $u_0=x$ et $\forall n\in\mathbb{N}\:,\: u_{n+1}=\mathrm{Arctan}(u_n)$.
		    On a alors $h(x)=h(u_0)=h(\mathrm{Arctan}(u_0))=h(u_1)=h(\mathrm{Arctan}(u_1))=h(u_2)=\dots$.
		    Par récurrence, on prouve que, $\forall n \in {\mathbb{N}}$, $h(x)=h(u_n)$.
		    De plus $\lim\limits_{n\rightarrow+\infty} h(u_n)=h(0)$ par convergence de la suite $(u_n)$ vers $0$ et par continuité de $h$.
		    On obtient ainsi: $h(x)=h(0)$ et donc $h$ est une fonction constante.
		    Réciproquement, toutes les fonctions constantes conviennent.
		    Conclusion: Seules les fonctions constantes répondent au problème.





!!! exercice "Corrigé <a name="corrige-3"></a><a href="#enonce-3">3</a>"


		1.  1.  Montrons que $E$ est un sous-espace-vectoriel de l’ensemble des suites à valeurs complexes.
		        La suite nulle appartient à $E$ ( obtenue pour ($u_0,u_1)=(0,0))$.
		        Soit $u=(u_n)_{n\in\mathbb{N}}$ et $v=(v_n)_{n\in\mathbb{N}}$ deux suites de $E$. Soit $\lambda\in\mathbb{C}$.
		        Montrons que $w=u+\lambda v\in E$.
		        On a $\forall\:n\in\mathbb{N}$, $w_n=u_n+\lambda v_n$.
		        Soit $n\in\mathbb{N}$.
		        $w_{n+2}=u_{n+2}+\lambda v_{n+2}$.
		        Or $(u,v)\in E^2$, donc $w_{n+2}=2au_{n+1}+4(ia-1)u_n+\lambda
		        \left( 2av_{n+1}+4(ia-1)v_n\right)$
		        c’est-à-dire $w_{n+2}=2a\left( u_{n+1}+\lambda v_{n+1}\right)+4(ia-1)\left( u_{n}+\lambda v_{n}\right)$
		        ou encore $w_{n+2}=2aw_{n+1}+4(ia-1)w_n$.
		        Donc $w\in E$.
		        Donc $E$ est un sous-espace vectoriel de l’ensemble des suites à valeurs complexes.

		    2.  On considère l’application $\varphi$ définie par:
		        $\varphi:\,
		        \begin{array}{lll}
		        E &\longrightarrow& \mathbb{C}^2\\
		        u=(u_n)_{n\in\mathbb{N}}&\longmapsto &(u_0,u_1)
		        \end{array}$
		        Par construction, $\varphi$ est linéaire et bijective.
		        Donc $\varphi$ est un isomorphisme d’espaces vectoriels.
		        On en déduit que $\dim E=\dim \mathbb{C}^2=2$.

		2.  Il s’agit d’une suite récurrente linéaire d’ordre 2 à coefficients constants.
		    On introduit l’équation caractéristique $(E)$ : $r^2-2ar-4(ia-1)=0.$
		    On a deux possibilités :

		    -   si $(E)$ admet deux racines distinctes $r_1$ et $r_2$, alors $\forall\:n\in\mathbb{N}$, $u_n= \alpha r_1^n+\beta r_2^n$
		        avec $(\alpha,\beta)$ que l’on détermine à partir des conditions initiales.

		    -   si $(E)$ a une unique racine double $r$, alors $\forall\:n\in\mathbb{N}$, $u_n=(\alpha n +\beta) r^n$
		        avec $(\alpha,\beta)$ que l’on détermine à partir des conditions initiales.

		    Le discriminant réduit de $(E)$ est $\Delta'=a^2+4ia-4=(a+2i)^2.$
		    **Premier cas** : $a=-2i$
		    $r=a=-2i$ est racine double de $(E)$.
		    Donc, $\forall\:n\in\mathbb{N}$, $u_n=(\alpha n+\beta )(-2i)^n$.
		    Or $u_0=1$ et $u_1=1$, donc $1=\beta$ et $1=(\alpha +\beta)(-2i)$.
		    On en déduit que $\alpha=\dfrac{i}{2}-1$ et $\beta=1$.

		    **Deuxième cas**: $a\neq -2i$
		    On a deux racines distinctes $r_1=2(a+i)$ et $r_2=-2i$.
		    Donc $\forall\:n\in\mathbb{N}$, $u_n=\alpha \left( 2(a+i)\right)^n +\beta\left(-2i \right) ^n$.
		    Or $u_0=1$ et $u_1=1$, donc $\alpha+\beta=1$ et $2(a+i)\alpha-2i\beta=1$.
		    On en déduit, après résolution, que $\alpha=\dfrac{1+2i}{2a+4i}$ et $\beta=\dfrac{2a+2i-1}{2a+4i}$.





!!! exercice "Corrigé <a name="corrige-4"></a><a href="#enonce-4">4</a>"

		Calculons bêtement :

		$$
		\begin{align*}
		P_{n+1}' &= (\sum_{k=0}^{2n+3}\frac{(-X)^k}{k!})'\\
		&= -\sum_{k=1}^{2n+3}\frac{(-X)^{k-1}}{(k-1)!}\\
		&= -\sum_{k=0}^{2n+2}\frac{(-X)^{k}}{(k)!}\\
		&= -\sum_{k=0}^{2n+1}\frac{(-X)^{k}}{(k)!}-\frac{X^{2n+2}}{(2n+2)!}\\
		&= -P_n-\frac{X^{2n+2}}{(2n+2)!}
		\end{align*}
		$$

		On a alors :

		$$
		\begin{align*}
		 P_{n+1}'' &= -P_n'-\frac{X^{2n+1}}{(2n+1)!}\\
		 &=P_{n-1}+\frac{X^{2n}}{(2n)!}-\frac{X^{2n+1}}{(2n+1)!}\\
		 &=P_n
		\end{align*}
		$$

		Par récurrence, l'héridité est triviale ($P_0=1-X$ est décroissante et s'annule une unique fois)

		Concentrons nous sur l'hérédité. Soit $n\in\mathbb N, on suppose $p_n$ strictement décroissante et qui s'annule en une unique valeur notée $u_n$. On a $p_{n+1}''=p_n$ donc $p_{n+1}'$ est croissante sur $[-\infty,u_n]$ et décroissante sur $[u_n,+\infty]$. Elle admet alors un maximum en $u_n$. Or $p_{n+1}'(u_n)=-p_n(u_n)-\frac{u_n^{2n+2}}{(2n+2)!}=0-\frac{u_n^{2n+2}}{(2n+2)!}=-\frac{u_n^{2n+2}}{(2n+2)!}<0$ (car il est évident que $u_n\ne 0$). On en déduit que le maximum de $P_{n+1}'$ est sctrictement négatif et donc que $p_{n+1}'$ est strictement négative : $p_{n+1}$ est strictement décroissante. Or c'est une fonction polynômiale donc elle s'annule une unique fois sur $\mathbb R$.

		Enfin $P_n$ est un polynôme de degré impair et strictement décroissant, donc il admet une unique solution $u_n$ sur $\mathbb R$.

		On a alors en regroupant termes pairs et impairs :

		$$
		\begin{align*}
		 P_n&=\sum_{k=0}^{2n+1}\frac{(-X)^k}{k!}\\
		 &=\sum_{p=0}^n\frac{(-X)^{2p}}{(2p)!}+\frac{(-X)^{2p+1}}{(2p+1)!}\\
		 &=\sum_{p=0}^n \frac{X^{2p}}{(2p)!}(1-\frac{X}{2p+1})
		\end{align*}
		$$

		On calcule alors $P(1)>0$ et

		$$
		\begin{align*}
		P(2n+1) &= \sum_{p=0}^{n}\frac{(2n+1)^{2p}}{(2p)!}\underset{<0}{\underbrace{(1-\frac{2n+1}{2k+1})}}\\
		&<0
		\end{align*}
		$$

		D'après le TVi, un fonction polynômiale étant continue, $u_n\in[1,2n+1]$.

		Calculons enfin $P_{n+1}(u_n)=P_n(u_n)+\frac{u_n^{2n}}{(2n!)}-\frac{u_n^{2n+1}}{(2n+1)!}=\frac{u_n^{2n}}{(2n)!}\left(1-\frac{u_n}{2n+1}\right)$. On $u_n<2n+1$ donc $(1-\frac{u_n}{2n+1})>0$ et ainsi $P_{n+1}(u_n)>0$. On en déduit d'après les variations de $P_{n+1}$ que $u_n<u_{n+1}$ : la suite $u_n$ est monotone croissante (strictement).




!!! exerciceb "Corrigé <a name="corrige-5"></a><a href="#enonce-5">5</a>"
		a) $f_n$ est dérivable de dérivée $fn'(x)=3nx^{3n-1}-\sqrt n=\sqrt n(3\sqrt n x^{3n-1}-1)$. Si $x\ge 1$, cette quantité est strictement positive, donc $f_n$ est strictement croissante sur $[1,2]$ en particulier. Or $f_n(1)=2-\sqrt n<0$ car $n\ge 4$ et $f_n(2)=2^{3n}-2\sqrt n+1>0$ On en déduit que $f_n(x)=0$ admet une unique solution entre $1$ et $2$.

		b)  Nous allons utiliser une inégalité classique : pour $u\ge 0$, $\ln(1+u)\ge u-\frac{u^2}{2}$ ; qu'on peut démontrer par une étude de fonction.


		$$
		\begin{aligned}
		f_n(1+\frac 1{\sqrt n})&=(1+\frac 1{\sqrt n})^{3n}-\sqrt n\left(1+\frac1{\sqrt n}\right)+1\\
		&=\exp(3n\ln\left(1+\frac1{\sqrt n}\right))-\sqrt n\\
		&\ge \exp(3\sqrt n-\frac 32)-\sqrt n\\
		&\ge \exp(3\sqrt n-\frac32)-\sqrt n\\
		&\ge 1+3\sqrt n-\frac 32-\sqrt n\\
		&\ge 2\sqrt n-\frac 12
		\end{aligned}
		$$

		car $\exp(u)\ge 1+u$. On en déduit donc que $f(1+\frac1{\sqrt n})\ge 0$ et donc $1\le x_n\le 1+\frac1{\sqrt n}$. Par encadrement, on a donc $\epsilon_n\to 0$.


		c) On a alors $x_n^{3n}=\sqrt nx_n-1$ donc $x_n^{3n-1}\sim \sqrt n$. c'est à dire $3n\ln(x_n)=\ln(\sqrt nx_n-1)=\ln(\sqrt nx_n)+\ln(1-\frac{1}{\sqrt nx_n})$. D'où $6n\ln(x_n)=\ln(n)+2\ln(x_n)+2\ln(1-\frac{1}{\sqrt nx_n})$ dont on déduit $6n\ln(x_n)\sim\\ln(n)$. Or $\ln(x_n)\sim x_n-1$ donc $x_n-1\sim\frac{\ln(n)}{6n}$. On en déduit $x_n=1+\frac{\ln(n)}{6n}+o(\frac{\ln(n)}{n})$.

		d) On cherche un terme en plus. On a

		$$
		\begin{aligned}
		x_n&=(\sqrt n x_n-1)^{1/3n}\\
		&=\exp(\frac 1{3n}\ln(\sqrt n x_n-1))\\
		&=\exp(\frac 1{3n}\ln(\sqrt n+\frac{\ln(n)}{6\sqrt n}-1+o(\frac{\ln n}{\sqrt n})))\\
		&=\exp(\frac{\ln(n)}{6n}+\frac 1{3n}\ln(1-\frac 1{\sqrt n}+\frac{\ln(n)}{6n}+o(\frac{\ln n}{n})))\\
		&=\exp(\frac{\ln(n)}{6n}+\frac 1{3n}\ln(1-\frac 1{\sqrt n}+o(\frac{1}{\sqrt n})))\\
		&=\exp(\frac{\ln(n)}{6n}-\frac 1{3n\sqrt n}+o(\frac 1{n\sqrt n}))\\
		&=1+\frac{\ln(n)}{6n}-\frac 1{3n\sqrt n}+o(\frac 1{n\sqrt n})
		\end{aligned}
		$$




!!! exerciceb "Corrigé <a name="corrige-6"></a><a href="#enonce-6">6</a>"


		1.  $g$ est de classe $C^{\infty}$ sur $\mathbb{R}$ et $h$ est de classe $C^{\infty}$ sur $\mathbb{R}\backslash\left\lbrace-1 \right\rbrace$.
		    On prouve, par récurrence, que :
		    $\forall\:x\in\mathbb{R}$, $g^{(k)}(x)=2^k\mathrm{e}^{2x}$ et $\forall\:x\in\mathbb{R}\backslash\left\lbrace-1 \right\rbrace$, $h^{(k)}(x)=\dfrac{(-1)^kk!}{(1+x)^{k+1}}$.

		2.  $g$ et $h$ sont de classe $C^{\infty}$ sur $\mathbb{R}\backslash\left\lbrace-1 \right\rbrace$ donc, d’après la formule de Leibniz, $f$ est de classe $C^{\infty}$ sur $\mathbb{R}\backslash\left\lbrace-1 \right\rbrace$ et $\forall\:x\in \mathbb{R}\backslash\left\lbrace-1 \right\rbrace$:
		    $f^{(n)}(x)=\displaystyle\sum\limits_{k=0}^{n}\dbinom{n}{k}g^{(n-k)}(x)h^{(k)}(x)=
		    \displaystyle\sum\limits_{k=0}^{n}\dbinom{n}{k}2^{n-k}\mathrm{e}^{2x}\dfrac{(-1)^k k!}{(1+x)^{k+1}}= n!\mathrm{e}^{2x}\sum\limits_{k = 0}^n \dfrac{{( - 1)^k 2^{n - k} }}{{(n - k)!}(1 + x)^{k + 1}}$.

		3.  Notons $(P_n)$ la propriété:
		    Si $f:I \to \mathbb{R}$ et $g:I \to \mathbb{R}$ sont $n$ fois dérivables sur $I$ alors, $fg$ est $n$ fois dérivable sur $I$ et :
		    $\forall \:x\in I$, $(fg)^{(n)}(x)=\displaystyle\sum\limits_{k=0}^{n} \dbinom{n}{k}f^{(n-k)}(x) g^{(k)}(x)$.
		    Prouvons que $(P_n)$ est vraie par récurrence sur $n$.
		    La propriété est vraie pour $n = 0$ et pour $n=1$ (dérivée d’un produit).
		    Supposons la propriété vraie au rang $n \geqslant 0$.
		    Soit $f:I \to \mathbb{R}$ et $g:I \to \mathbb{R}$ deux fonctions $n + 1$ fois dérivables sur $I$.
		    Les fonctions $f$ et $g$ sont, en particulier, $n$ fois dérivables sur $I$ et donc par hypothèse de récurrence la fonction $fg$ l’est aussi avec $\forall \:x\in I$, $(fg)^{(n)}(x)=\displaystyle\sum\limits_{k=0}^{n} \dbinom{n}{k}f^{(n-k)}(x) g^{(k)}(x)$.

		    Pour tout $k \in \left\{ {0, \ldots ,n} \right\}$, les fonctions $f^{(n - k)}$ et $g^{(k)}$ sont dérivables sur $I$ donc par opération sur les fonctions dérivables, la fonction $(fg)^{(n)}$ est encore dérivable sur $I$.
		    Ainsi la fonction $fg$ est $(n + 1)$ fois dérivable et: $\forall \:x\in I$,$(fg)^{(n+1)}(x)=\displaystyle\sum\limits_{k=0}^{n} \dbinom{n}{k}\left( f^{(n+1-k)}(x) g^{(k)}(x)+f^{(n-k)}(x) g^{(k+1)}(x)\right)$.
		    En décomposant la somme en deux et en procédant à un décalage d’indice sur la deuxième somme, on obtient: $\forall \:x\in I$, $(fg)^{(n+1)}(x)=\displaystyle\sum\limits_{k=0}^{n} \dbinom{n}{k} f^{(n+1-k)}(x) g^{(k)}(x)
		    + \displaystyle\sum\limits_{k=1}^{n+1} \dbinom{n}{k-1}f^{(n+1-k)}(x) g^{(k)}(x)$.
		    C’est-à-dire $(fg)^{(n+1)}(x)=\displaystyle\sum\limits_{k=1}^{n}\left( \dbinom{n}{k}+\dbinom{n}{k-1}\right)f^{(n+1-k)}(x) g^{(k)}(x)+\dbinom{n}{0}f^{(n+1)}(x)g^{(0)}(x)+
		     \dbinom{n}{n}f^{(0)}(x)g^{(n+1)}(x)$.
		    Or, en utilisant le triangle de Pascal, on a $\dbinom{n}{k}+\dbinom{n}{k-1}=\dbinom{n+1}{k}$.
		    On remarque également que $\dbinom{n}{0}=1=\dbinom{n+1}{0}$ et $\dbinom{n}{n}=1=\dbinom{n+1}{n+1}$.
		    On en déduit que $(fg)^{(n+1)}(x)=\displaystyle\sum\limits_{k=0}^{n+1} \dbinom{n+1}{k}f^{(n+1-k)}(x) g^{(k)}(x)$.
		    Donc $(P_{n+1})$ est vraie.





!!! exercice "Corrigé <a name="corrige-7"></a><a href="#enonce-7">7</a>"


		1.  Théorème des accroissements finis :
		    Soit $f:\left[ a,b\right] \longrightarrow \mathbb{R}$.
		    On suppose que $f$ est continue sur $\left[ a,b\right]$ et dérivable sur $\left]a,b \right[$.
		    Alors $\exists\:c\in \left]a,b \right[$ tel que $f(b)-f(a)=f'(c)(b-a)$.

		2.  On pose $l=\lim\limits_{x\to x_0}^{}\ f'(x )$.
		    Soit $h \ne 0$ tel que $x_0  + h \in \left[ {a,b} \right]$.
		    En appliquant le théorème des accroissements finis, à la fonction $f$, entre $x_0$ et $x_0  + h$, on peut affirmer qu’il existe $c_h$ strictement compris entre $x_0$ et $x_0  + h$ tel que $f(x_0  + h) - f(x_0 ) = f'(c_h )h$.
		    Quand $h \to 0$ (avec $h \ne 0$), on a, par encadrement, $c_h  \to x_0$.
		    Donc $\lim\limits_{h\to 0}^{}\dfrac{1}{h}\left( {f(x_0  + h) - f(x_0 )} \right) =\lim\limits_{h\to 0}^{}\ f'(c_h ) =\lim\limits_{x\to x_0}^{}\ f'(x ) =l$.
		    On en déduit que $f$ est dérivable en $x_0$ et $f'(x_0 ) = l$.

		3.  La fonction $g$ proposée dans l’indication est évidemment dérivable sur $\left] { - \infty ,0} \right[$ et $\left] {0, + \infty } \right[$.
		    $g$ est également dérivable en 0 car $\dfrac{1}{h}\left( {g(h) - g(0)} \right) = h\sin \left( \dfrac{1}{h} \right)$.
		    Or $\lim\limits_{\underset{h\neq 0}{h\to 0}}^{}h\sin \left( \dfrac{1}{h} \right)=0$ car $|h\sin \left( \dfrac{1}{h} \right)|\leqslant |h|$.
		    Donc, $g$ est dérivable en $0$ et $g'(0)=0$.
		    Cependant, $\forall \:x\in\mathbb{R}\backslash \left\lbrace 0\right\rbrace$, $g'(x) = 2x\sin \left( \dfrac{1}{x}\right)  - \cos \left( \dfrac{1}{x} \right)$.
		    $2x\sin \left( \dfrac{1}{x}\right) \xrightarrow[{x \to 0}]{}0$ (car $|2x\sin(\dfrac{1}{x})|\leqslant2|x|)$, mais $x\longmapsto\cos \left( \dfrac{1}{x}\right)$ n’admet pas de limite en 0.
		    Donc $g'$ n’a pas de limite en $0$.





!!! exerciceb "Corrigé <a name="corrige-8"></a><a href="#enonce-8">8</a>"


		1.  Par hypothèse: $\forall \:\varepsilon >0,\:\exists\:N\in\mathbb{N}/\:\forall n\geqslant N,\:|\dfrac{u_{n+1}}{u_n}-l|\leqslant \varepsilon$.(1)
		    Prenons $\varepsilon=\dfrac{1-l}{2}$.
		    Fixons un entier $N$ vérifiant (1).
		    Alors $\forall\:n\in\mathbb{N},\: n\geqslant N\Longrightarrow\:|\dfrac{u_{n+1}}{u_n}-l|\leqslant \dfrac{1-l}{2}$.
		    Et donc, $\forall n\geqslant N,\:\dfrac{u_{n+1}}{u_n}\leqslant \dfrac{1+l}{2}$.
		    On pose $q=\dfrac{1+l}{2}$. On a donc $q\in \left] 0,1\right[$.
		    On a alors $\forall n\geqslant N,\:u_{n+1}\leqslant q u_n$.
		    On en déduit, par récurrence, que $\forall n\geqslant N,\:u_{n}\leqslant q^{n-N} u_N.$
		    Or $\displaystyle\sum\limits_{n\geqslant N}^{}q^{n-N}u_N=u_Nq^{-N}\displaystyle\sum\limits_{n\geqslant N}^{}q^n$ et $\displaystyle\sum\limits_{n\geqslant N}^{}q^n$ converge car $q\in \left] 0,1\right[$.
		    Donc, par critère de majoration des séries à termes positifs, $\displaystyle\sum u_n$ converge.

		2.  On pose : $\forall \:n\in\mathbb{N}^*$, $u_n=\dfrac{n!}{n^n}$.
		    $\forall \:n\in\mathbb{N}^{*}$, $u_n> 0$ et $\forall \:n\in\mathbb{N}^*$, $\dfrac{u_{n+1}}{u_n}=\dfrac{n^n}{(n+1)^{n}}=\mathrm{e}^{-n\ln(1+\dfrac{1}{n})}$.
		    Or $-n\ln(1+\dfrac{1}{n})\underset{+\infty}{\thicksim}-1$ donc $\lim\limits_{n\to +\infty}^{}\dfrac{u_{n+1}}{u_n}=\mathrm{e}^{-1}<1$.
		    Donc $\displaystyle\sum u_n$ converge.





!!! exercice "Corrigé <a name="corrige-9"></a><a href="#enonce-9">9</a>"


		1.  Par hypothèse, $\exists\:N_0\in\mathbb{N}/\:\:\forall\:n\in\mathbb{N}, n\geqslant N_0\Longrightarrow v_n\neq 0$.
		    Ainsi la suite $\left( \dfrac{u_n}{v_n}\right)$ est définie à partir du rang $N_0$.
		    De plus, on suppose que $u_{n}\underset{+\infty}\thicksim v_{n}$.
		    On en déduit que $\lim\limits_{n\to +\infty}^{}\dfrac{u_n}{v_n}=1$.
		    Alors, $\forall\:\varepsilon>0$, $\exists N\in\mathbb{N}\:\text{tel que}\:N\geqslant N_0$ et $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow\left|\dfrac{u_n}{v_n}-1\right|\leqslant\varepsilon$.(1)
		    Prenons $\varepsilon=\dfrac{1}{2}$. Fixons un entier $N$ vérifiant $(1)$.
		    Ainsi, $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow\left|\dfrac{u_n}{v_n}-1\right|\leqslant\dfrac{1}{2}$.
		    C’est-à-dire, $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow -\dfrac{1}{2}\leqslant\dfrac{u_n}{v_n}-1\leqslant\dfrac{1}{2}$.
		    On en déduit que $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow \dfrac{1}{2}\leqslant\dfrac{u_n}{v_n}\leqslant\dfrac{3}{2}$.(\*)

		    **Premier cas**: Si $\displaystyle\sum v_n$ converge
		    D’après (\*), $\forall \:n\geqslant N$, $u_n\leqslant \dfrac{3}{2}v_n$.
		    Donc, par critère de majoration des séries à termes positifs, $\displaystyle\sum u_n$ converge.

		    **Deuxième cas**: Si $\displaystyle\sum v_n$ diverge
		    D’après (\*), $\forall \:n\geqslant N$, $\dfrac{1}{2}v_n\leqslant u_n$.
		    Donc, par critère de minoration des séries à termes positifs, $\sum u_n$ diverge.
		    Par symétrie de la relation d’équivalence, on obtient le résultat.

		2.  On pose $\forall \:n\geqslant 2$, $u_n=\dfrac{\left((-1)^n+\mathrm{i}\right)\ln n \sin \left( \dfrac{1}{n}\right) }{\left(\sqrt{n+3}-1\right)}$.
		    $|u_n|=\dfrac{\sqrt{2}\ln n\sin(\dfrac{1}{n})}{\left( \sqrt{n+3}-1\right) }$.
		    De plus $|u_n|\underset{+\infty}\thicksim\dfrac{\sqrt{2}\ln n}{n^{\frac{3}{2}}}=v_n$
		    On a $n^{\frac{5}{4}}v_n=\dfrac{\sqrt{2}\ln n}{n^{\frac{1}{4}}}$, donc $\lim\limits_{n\to +\infty}^{}n^{\frac{5}{4}}v_n=0$. On en déduit que $\displaystyle\sum v_n$ converge.
		    D’après 1., $\displaystyle\sum\limits_{n\geqslant 2}^{}|u_n|$ converge.
		    Donc $\displaystyle\sum\limits_{n\geqslant 2}^{}u_n$ converge absolument.
		    De plus, la suite $(u_n)_{n\geqslant2}$ est à valeurs dans $\mathbb{C}$, donc $\displaystyle\sum\limits_{n\geqslant 2}^{}u_n$ converge.





!!! exercice "Corrigé <a name="corrige-10"></a><a href="#enonce-10">10</a>"


		1.  1.  $S_{2n + 2}  - S_{2n }  = u_{2n + 2}  - u_{2n + 1}  \leqslant 0$, donc $(S_{2n})_{n\in\mathbb{N}}$ est décroissante.
		        De même $S_{2n + 3}  - S_{2n + 1}  \geqslant 0$, donc $(S_{2n+1})_{n\in\mathbb{N}}$ est croissante.
		        De plus $S_{2n}  - S_{2n + 1}  = u_{2n + 1}$ et $\lim\limits_{n\to +\infty}^{}u_{2n + 1}=0$, donc $\lim\limits_{n\to +\infty}^{}(S_{2n}  - S_{2n + 1}) =0$.
		        On en déduit que les suites $(S_{2n} )_{n \in \mathbb{N}}$ et $(S_{2n + 1} )_{n \in \mathbb{N}}$ sont adjacentes. Donc elles convergent et ce vers une même limite.
		        Comme $(S_{2n} )_{n \in \mathbb{N}}$ et $(S_{2n + 1} )_{n \in \mathbb{N}}$ recouvrent l’ensemble des termes de la suite $(S_n)_{n\in\mathbb{N}}$, on en déduit que la suite $(S_n )_{n \in \mathbb{N}}$ converge aussi vers cette limite.
		        Ce qui signifie que la série $\displaystyle\sum {( - 1)^k u_k }$ converge.

		    2.  Le reste $R_n  = \displaystyle\sum\limits_{k = n + 1}^{ + \infty } {( - 1)^k u_k }$ vérifie $\forall\:n\in\mathbb{N}$, $\left| {R_n } \right| \leqslant u_{n + 1}$.

		2.  On pose : $\forall \:x\in\mathbb{R}$, $\forall\:n\in\mathbb{N}^*$, $f_n(x)=\dfrac{\left(-1\right)^{n}e^{-nx}}{n}$.
		    On a alors $\forall\:n\in\mathbb{N}^*$, $f_n(x)=(-1)^nu_n(x)$ avec $u_n(x)=\dfrac{e^{-nx}}{n}$.

		    1.  Soit $x\in\mathbb{R}$.
		        Si $x<0$, alors $\lim\limits_{n\to +\infty}^{}|f_n(x)| =+\infty$, donc $\displaystyle\sum\limits_{n\geqslant1}^{}f_n(x)$ diverge grossièrement.
		        Si $x\geqslant 0$, alors $(u_n(x))_{n \in \mathbb{N}}$ est positive, décroissante et $\lim\limits_{n\to +\infty}^{}u_n(x)=0$.
		        Donc d’après 1.(a), $\sum\limits_{n\geqslant1}^{}f_n(x)$ converge.
		        Donc $\displaystyle\sum\limits_{n\geqslant1}^{}f_n$ converge simplement sur $\left[ 0,+\infty\right[$.

		        **Remarque**: pour $x> 0$, on a aussi convergence absolue de $\sum\limits_{n\geqslant1}^{}f_n(x)$.
		        En effet, pour tout réel $x> 0$, $n^2|f_n(x)|=n\mathrm{e}^{-nx}\underset{n\to +\infty}{\longrightarrow}0$ donc, au voisinage de $+\infty$, $|f_n(x)|=o\left( \dfrac{1}{n^2}\right)$.

		    2.  Comme $\displaystyle\sum\limits_{n\geqslant1}^{}f_n$ converge simplement sur $\left[ 0,+\infty\right[$, on peut poser $\forall \:x\in \left[ 0,+\infty\right[$, $R_n(x)=\displaystyle\sum\limits_{k=n+1}^{+\infty}f_k(x)$.
		        Alors, comme, $\forall \:x\in \left[ 0,+\infty\right[$, $(u_n(x))_{n \in \mathbb{N}}$ est positive, décroissante et $\lim\limits_{n\to +\infty}^{}u_n(x)=0$, on en déduit, d’après 1.(b), que:
		        $\forall \:x\in \left[ 0,+\infty\right[$, $|R_n(x)|\leqslant \dfrac{e^{-(n+1)x}}{n+1}$.
		        Et donc $\forall \:x\in \left[ 0,+\infty\right[$, $|R_n(x)|\leqslant \dfrac{1}{n+1}$. (majoration indépendante de $x$)
		        Et comme $\lim\limits_{n\to +\infty}^{} \dfrac{1}{n+1}=0$, alors $(R_n)$ converge uniformément vers $0$ sur $\left[ 0,+\infty\right[$.
		        C’est-à-dire $\displaystyle\sum\limits_{n\geqslant1}^{}f_n$ converge uniformément sur $\left[ 0,+\infty\right[$.





!!! exerciceb "Corrigé <a name="corrige-11"></a><a href="#enonce-11">11</a>"


		1.  $\pi \sqrt{n^2+n+1}=n\pi\sqrt{1+\dfrac{1}{n}+\dfrac{1}{n^2}}$.
		    Or, au voisinage de $+\infty$, $\sqrt{1+\dfrac{1}{n}+\dfrac{1}{n^2}}=1+\dfrac{1}{2}(\dfrac{1}{n}
		    +\dfrac{1}{n^2})-\dfrac{1}{8n^2}+O(\dfrac{1}{n^3})=
		    1+\dfrac{1}{2n}
		    +\dfrac{3}{8n^2}+O(\dfrac{1}{n^3})$.
		    Donc, au voisinage de $+\infty$, $\pi \sqrt{n^2+n+1}=n\pi+\dfrac{\pi}{2}+\dfrac{3}{8}\dfrac{\pi}{n}+O(\dfrac{1}{n^2})$.

		2.  On pose $\forall n \in{\mathbb{N}^*}$, $v_n=\cos \left( \pi \sqrt{n^2+n+1}\right)$.



		    D’après 1., $v_n=\cos\left( n\pi+\dfrac{\pi}{2}+\dfrac{3}{8}\dfrac{\pi}{n}+O(\dfrac{1}{n^2})\right)
		    =(-1)^{n+1}\sin\left(\dfrac{3}{8}\dfrac{\pi}{n}+O(\dfrac{1}{n^2})\right)$.
		    Donc $v_n=\dfrac{3\pi}{8}\dfrac{(-1)^{n+1}}{n}+O(\dfrac{1}{n^2})$.

		    Or $\displaystyle\sum\limits_{n\geqslant 1}{}\dfrac{(-1)^{n+1}}{n}$ converge (d’après le critère spécial des séries alternées) et $\displaystyle\sum\limits_{n\geqslant 1}{}O(\dfrac{1}{n^2})$ converge (par critère de domination), donc $\displaystyle\sum\limits_{n\geqslant 1}{v_n}$ converge.

		3.  D’après le développement asymptotique du 2., on a $|v_n| \underset{+\infty}{\thicksim}
		    \dfrac{3\pi}{8n}$.
		    Or $\displaystyle\sum\limits_{n\geqslant 1}{}\dfrac{1}{n}$ diverge (série harmonique), donc $\displaystyle\sum\limits_{n\geqslant 1}|{v_n}|$ diverge, c’est-à-dire $\sum\limits_{n\geqslant 1}{v_n}$ ne converge pas absolument.





!!! exerciceb "Corrigé <a name="corrige-12"></a><a href="#enonce-12">12</a>"

		 On a :

		$$
		\begin{align*}
		u_n=\frac{(-1)^n}{n^{3/4}(1+\sin(n)n^{-3/4}}\\
		&=\frac{(-1)^n}{n^{3/4}}(1-\frac{\sin(n)}{n^{3/4}}+o(\frac1{n^{3/4}})\\
		&=\underset{v_n}{\underbrace{\frac{(-1)^n}{n^{3/4}}}}+\underset{w_n}{\underbrace{\frac{(-1)^n\sin(n)}{n^{3/2}}}}+o(\frac 1{n^{3/2}}
		\end{align*}
		$$

		Or $v_n$ est le terme général d'une série convergente (par critère des séries alternées)  et $w_n$ est le terme général d'une série absolument convergente (par comparaison à une série de Riemann $\sum\frac 1{n^{3/2}}$ convergente). Donc $\sum u_n$ converge.






!!! exercice "Corrigé <a name="corrige-13"></a><a href="#enonce-13">13</a>"

		 Calculons :

		$$
		 \begin{align*}
		 u_n&=\frac{e^{in\theta}}{\sqrt n+e^{in\varphi}}\\
		 &=\frac{e^{in\theta}}{\sqrt n}\frac 1{1+\frac{e^{in\varphi}}{\sqrt n}}\\
		 &=\frac{e^{in\theta}}{\sqrt n}(1-\frac{e^{in\varphi}}{\sqrt n}+O(\frac1{n}))\\
		 &=\frac{e^{in\theta}}{\sqrt n}-\frac{e^{in(\theta+\varphi)}}{n}+O(\frac1{ n\sqrt n})\\
		 \end{align*}
		$$

		Posons $v_n=\frac{e^{in\theta}}{\sqrt n}$ et $w_n=\frac{e^{in(\theta+\varphi)}}{n}$. Le $O(\frac1{n\sqrt n})$ est le terme général d'une série absolument convergente, la convergence de $\sum u_n$ dépend de la convergence de $\sum v_n+w_n$.
		\begin{itemize}
		\item Si $\theta$ et $\varphi$ sont des multiples impairs de $\pi$, alors $v_n$ est le terme général d'une série alternée donc convergente mais $w_n$ est divergente. Le $O$ étant le terme général d'une série absolument convergente, on en déduit $\sum u_n$ diverge.
		\item Si $\theta$ est un multiple pair et $\varphi$ un multiple impair alors $\sum v_n$ diverge et $\sum w_n$ converge donc $\sum u_n$ diverge.
		\item Si $\theta$ est un multiple impair et $\varphi$ un multiple pair, alors $\sum v_n$ converge ainsi que $\sum w_n$ par CSSA. Donc $\sum u_n$ converge
		\item Si $\theta$ et $\varphi$ sont des multiples pairs, alors $v_n+w_n=\frac 1{\sqrt n}-\frac 1n=\frac{\sqrt n -1}{n}\sim\frac 1{\sqrt n}$ qui est le terme général d'un série divergente. Par théorème de comparaison des séries à termes positifs, $\sum u_n$ diverge.
		\end{itemize}






!!! exerciceb "Corrigé <a name="corrige-14"></a><a href="#enonce-14">14</a>"


		1.  Par opérations sur les fonctions continues, $f$ est continue sur l’ouvert $\mathbb{R}^2 \backslash \left\{ {(0,0)} \right\}$.
		    On considère la norme euclidienne sur $\mathbb{R}^2$ définie par $\forall\:(x,y)\in\mathbb{R}^2$, $||(x,y)||_2=\sqrt{x^2+y^2}$.
		    On a $\forall\:(x,y)\in\mathbb{R}^2$, $|x|\leqslant ||(x,y)||_2$ et $|y|\leqslant||(x,y)||_2$.
		    On en déduit que $\forall\:(x,y)\in\mathbb{R}^2\backslash\left\lbrace (0,0)\right\rbrace$, $|f(x,y)-f(0,0)|=\dfrac{|x||y|}{||(x,y)||_2}\leqslant\dfrac{\left( ||(x,y)||_2\right) ^2}{||(x,y)||_2}=||(x,y)||_2\underset{(x,y)\to (0,0)}{\longrightarrow} 0$.
		    On en déduit que $f$ est continue en $(0,0)$.
		    Ainsi $f$ est continue sur $\mathbb{R}^2$.

		2.  Par opérations sur les fonctions admettant des dérivées partielles, $f$ admet des dérivées partielles en tout point de l’ouvert $\mathbb{R}^2 \backslash \left\{ {(0,0)} \right\}$.
		    En $(0,0)$:
		    $\mathop {\lim }\limits_{t \to 0} \dfrac{1}{t}\left( {f(t,0) - f(0,0)} \right) = 0\text{ }$, donc $f$ admet une dérivée partielle en $(0,0)$ par rapport à sa première variable et $\dfrac{{\partial f}}{{\partial x}}(0,0) = 0$.
		    De même, $\mathop {\lim }\limits_{t \to 0} \dfrac{1}{t}\left( {f(0,t) - f(0,0)} \right) = 0\text{ }$. Donc $f$ admet une dérivée partielle en $(0,0)$ par rapport à sa seconde variable et $\dfrac{{\partial f}}{{\partial y}}(0,0) = 0$.

		3.  D’après le cours, $f$ est de classe $C^{1}$ sur $\mathbb{R}^{2}$ si et seulement si $\dfrac{\partial f}{ \partial x}$ et $\dfrac{\partial f}{ \partial y }$ existent et sont continues sur $\mathbb{R}^{2}$.
		    Or, $\forall (x,y)\in\mathbb{R}^2 \backslash \left\{ {(0,0)} \right\}$, $\dfrac{\partial f}{ \partial x}(x,y)=\dfrac{y^3}{\left( x^2+y^2\right)^{\frac{3}{2}} }$.
		    On remarque que $\forall\:x>0$, $\dfrac{\partial f}{ \partial x}(x,x)=\dfrac{1}{2\sqrt{2}}$.
		    Donc, $\lim\limits_{x\to 0^{+}}^{}\dfrac{\partial f}{ \partial x}(x,x)=\dfrac{1}{2\sqrt{2}}\neq \dfrac{\partial f}{ \partial x}(0,0)$.
		    On en déduit que $\dfrac{\partial f}{ \partial x}$ n’est pas continue en $(0,0)$.
		    Donc $f$ n’est pas de classe $C^{1}$ sur $\mathbb{R}^{2}$.





!!! exercice "Corrigé <a name="corrige-15"></a><a href="#enonce-15">15</a>"


		-5truemm

		1.  Soit $(x,y)\in\mathbb R^{2}$. $x^{2}+y^{2}-xy-\dfrac{1}{2}(x^{2}+y^{2})=\dfrac{1}{2}(x^{2}+y^{2}-2xy)=\dfrac{1}{2}\left( x-y\right)^2\geqslant 0$.
		    Donc $x^{2}+y^{2}-xy\geqslant\dfrac{1}{2}(x^{2}+y^{2})$.

		2.  1.  Soit $(x,y)\in\mathbb R^{2}$.
		        D’après 1., $x^{2}+y^{2}-xy=0\Longleftrightarrow x^{2}+y^{2}=0\Longleftrightarrow x=y=0$.
		        Ainsi, $f$ est définie sur $\mathbb R^{2}$.

		    2.  D’après les théorèmes généraux, $f$ est continue sur $\mathbb R^{2}\setminus\big\{(0,0)\big\}$.

		        D’après **1.**, pour $(x,y)\not=(0,0),\: 0\leqslant f(x,y)\leqslant\dfrac{2y^{4}}{x^{2}+y^{2}}\leqslant\dfrac{2(x^{2}+y^{2})^{2}}{x^{2}+y^{2}}$ .

		        Ainsi, $0\leqslant f(x,y)\leqslant 2(x^{2}+y^{2})\underset{(x,y)\to(0,0)}{\longrightarrow}0.$
		        Or : $f$ est continue en $(0,0)$ $\Longleftrightarrow$ $f(x,y)\underset{(x,y)\to(0,0)}{\longrightarrow}f(0,0)=\alpha.$
		        Donc : $f$ est continue en $(0,0)$ $\Longleftrightarrow$ $\alpha=0$.
		        Conclusion : $f$ est continue sur $\mathbb{R}^2$ $\Longleftrightarrow$ $\alpha=0$.

		3.  1.  D’après les théorèmes généraux, $f$ est de classe ${\cal C}^{1}$ sur $\mathbb R^{2}\setminus\big\{(0,0)\big\}$.
		        $\forall(x,y)\in\mathbb R^{2}\setminus\big\{(0,0)\big\}$, $\dfrac{\partial f}{\partial x}(x,y)=\dfrac{-y^{4}(2x-y)}{(x^{2}+y^{2}-xy)^{2}}$   et   $\dfrac{\partial f}{\partial y}(x,y)=\dfrac{2y^{5}-3xy^{4}+4x^{2}y^{3}}{(x^{2}+y^{2}-xy)^{2}}$.

		    2.  Pour tout $x\not=0,\:\dfrac{f(x,0)-f(0,0)}{x-0}=0\underset{x\to 0}{\longrightarrow}0$, donc $\dfrac{\partial f}{\partial x}(0,0)$ existe et $\dfrac{\partial f}{\partial x}(0,0)=0$.

		        Pour tout $y\not=0,\: \dfrac{f(0,y)-f(0,0)}{y-0}=y\underset{y\to 0}{\longrightarrow}0$, donc $\dfrac{\partial f}{\partial y}(0,0)$ existe et $\dfrac{\partial f}{\partial y}(0,0)=0$.

		    3.  Pour montrer que $f$ est de classe ${\cal C}^{1}$ sur $\mathbb R^{2}$, montrons que $\dfrac{\partial f}{\partial x}$ et $\dfrac{\partial f}{\partial y}$ sont continues sur $\mathbb R^{2}$.
		        Pour cela, il suffit de montrer qu’elles sont continues en $(0,0)$.

		        $\forall(x,y)\in\mathbb R^{2}\setminus\big\{(0,0)\big\}$, on note $r=\sqrt{x^{2}+y^{2}}$. On a alors $|x|\leqslant r$ et $|y|\leqslant r$.
		        De plus, $(x,y)\to (0,0) \Longleftrightarrow r\to 0$.
		        D’après **1.** et l’inégalité triangulaire,

		        $\left|\dfrac{\partial f}{\partial x}(x,y)-\dfrac{\partial f}{\partial x}(0,0)\right|\leqslant4\,\dfrac{\big|y^{4}(2x-y)\big|}{(x^{2}+y^{2})^{2}}\leqslant4\,\dfrac{r^{4}(2r+r)}{r^{4}}=12r\underset{r\to 0}{\longrightarrow}0$.

		        $\left|\dfrac{\partial f}{\partial y}(x,y)-\dfrac{\partial f}{\partial y}(0,0)\right|\leqslant4\,\dfrac{\big|2y^{5}-3xy^{4}+4x^{2}y^{3}\big|}{(x^{2}+y^{2})^{2}}\leqslant4\,\dfrac{2r^{5}+3r^{5}+4r^{5}}{r^{4}}=36r\underset{r\to 0}{\longrightarrow}0$.

		        Donc $\dfrac{\partial f}{\partial x}$ et $\dfrac{\partial f}{\partial y}$ sont continues en $(0,0)$ et par suite sur $\mathbb R^{2}$.
		        Ainsi, $f$ est de classe ${\cal C}^{1}$ sur $\mathbb R^{2}$.




!!! exercice "Corrigé <a name="corrige-16"></a><a href="#enonce-16">16</a>"


		1.  1.  $f$ est continue en $(0,0)$ $\Longleftrightarrow$ $\forall\:\varepsilon>0,\; \exists\alpha>0/\: \forall (x,y)\in\mathbb{R}^2,\; \|(x,y)\|<\alpha\Longrightarrow |f(x,y)-f(0,0)|<\varepsilon$.
		        $\|\cdot\|$ désigne une norme quelconque sur $\mathbb{R}^2$ puisque toutes les normes sont équivalentes sur $\mathbb{R}^2$ (espace de dimension finie) .

		    2.  $f$ est différentiable en $(0,0)$ $\Longleftrightarrow$ $\exists L\in\mathcal{L}_{\mathcal{C}}(\mathbb{R}^2,\mathbb{R})$/ au voisinage de $(0,0)$, $f(x,y)=f(0,0)+L(x,y)+o(\|(x,y)\|).$

		        **Remarque** : Comme $\mathbb{R}^2$ est de dimension finie, si $L\in\mathcal{L}(\mathbb{R}^2,\mathbb{R})$ alors $L\in\mathcal{L}_{\mathcal{C}}(\mathbb{R}^2,\mathbb{R})$.

		2.  On notera $\|.\|$ la norme euclidienne usuelle sur $\mathbb{R}^2$.
		    On remarque que $\forall\:(x,y)\in\mathbb{R}^2$, $|x|\leqslant \|(x,y)\|$ et $|y|\leqslant \|(x,y)\|$(\*).

		    1.  $(x,y)\mapsto x^2+y^2$ et $(x,y)\mapsto xy(x^2-y^2)$ sont continues sur $\mathbb{R}^2\backslash\{(0,0)\}$ et $(x,y)\mapsto x^2+y^2$ ne s’annule pas sur $\mathbb{R}^2\backslash\{(0,0)\}$ donc, $f$ est continue sur $\mathbb{R}^2\backslash\{(0,0)\}$.
		        Continuité en (0,0):
		        On a, en utilisant (\*) et l’inégalité triangulaire, $|f(x,y)-f(0,0)|=\left|xy\frac{x^2-y^2}{x^2+y^2}\right|\leqslant |x|.|y|\leqslant \|(x,y)\|^2$.
		        Donc $f$ est continue en $(0,0)$.

		    2.  $f$ est de classe ${\cal C}^1$ sur $\mathbb{R}^2$ si et seulement si $\frac{\partial f}{\partial x}$ et $\frac{\partial f}{\partial y}$ existent sur $\mathbb{R}^2$ et sont continues sur $\mathbb{R}^2$.
		        $f$ admet des dérivées partielles sur $\mathbb{R}^2\backslash\{(0,0)\}$ et elles sont continues sur $\mathbb{R}^2\backslash\{(0,0)\}$.
		        De plus, $\forall\:(x,y)\in\mathbb{R}^2-\left\lbrace (0,0)\right\rbrace$, $\dfrac{\partial f}{\partial x}(x,y)=\dfrac{x^4y+4x^2y^3-y^5}{(x^2+y^2)^{2}}$ et $\dfrac{\partial f}{\partial y}(x,y)=\dfrac{x^5-4x^3y^2-xy^4}{(x^2+y^2)^{2}}.$(\*\*)
		        Existence des dérivées partielles en $(0,0)$:
		        $\forall\:x\in\mathbb{R}^*$, $\frac{f(x,0)-f(0,0)}{x-0}=0$, donc $\lim\limits_{x\rightarrow 0}\frac{f(x,0)-f(0,0)}{x-0}=0$; donc $\frac{\partial f}{\partial x}(0,0)$ existe et $\frac{\partial f}{\partial x}(0,0)=0$.
		        De même, $\forall\:y\in\mathbb{R}^*$, $\frac{f(0,y)-f(0,0)}{y-0}=0$, donc $\lim\limits_{y\rightarrow 0}\frac{f(0,y)-f(0,0)}{y-0}=0$; donc $\frac{\partial f}{\partial y}(0,0)$ existe et $\frac{\partial f}{\partial y}(0,0)=0$.
		        Continuité des dérivées partielles en $(0,0)$:
		        D’après (\*) et (\*\*), $\forall \:(x,y)\in \mathbb{R}^2\backslash\{(0,0)\}$,
		        $\left|\dfrac{\partial f}{\partial x}(x,y)\right|\leqslant \dfrac{6\|(x,y)\|^5}{\|(x,y)\|^4}=6\|(x,y)\|$ et $\left|\dfrac{\partial f}{\partial y}(x,y)\right|\leqslant\dfrac{6\|(x,y)\|^5}{\|(x,y)\|^4}=6\|(x,y)\|$.
		        Donc $\lim\limits_{(x,y)\to (0,0)}\frac{\partial f}{\partial x}(x,y)=0=\frac{\partial f}{\partial x}(0,0)$ et $\lim\limits_{(x,y)\to (0,0)}\frac{\partial f}{\partial y}(x,y)=0=\frac{\partial f}{\partial y}(0,0)$.
		        Donc $\frac{\partial f}{\partial x}$ et $\frac{\partial f}{\partial y}$ sont continues en $(0,0)$.
		        Conclusion:$\frac{\partial f}{\partial x}$ et $\frac{\partial f}{\partial y}$ existent et sont continues sur $\mathbb{R}^2$, donc $f$ est de classe $C^1$ sur $\mathbb{R}^2$.





!!! exercice "Corrigé <a name="corrige-17"></a><a href="#enonce-17">17</a>"

		On commence par calculer $\frac{\partial f}{\partial x}(x,y)=2x-2+y$ et $\frac{\partial f}{\partial y}(x,y)=x+2y$. Les points critiques sont donc les points vérifiant :

		$$
		\begin{cases}
		2x+y=2\\
		x+2y=0
		\end{cases}
		\Leftrightarrow
		\begin{cases}
		x=\frac 43\\
		y=-\frac 23
		\end{cases}
		$$
		On calcule $f(\frac 43,-\frac 23)=\frac{16}{9}-\frac{8}{3}-\frac{8}{9}+\frac{4}{9}=-\frac 43.$

		Ce point est le seul candidat pour être extremum global dans l'intérieur de l'ensemble de définition. Etudions $f(x,y)+\frac 43$ pour savoir si l'on a ici un minimum ou maximum global. On pose dans la suite $x'=x-\frac 43$ et $y'=y+\frac 23$.

		$$
		\begin{align*}
		f(x,y)+\frac 43 &=x^2-2x+xy+y^2+\frac 43\\
		&=(x'+\frac 43)^2-2(x'+\frac 43)+(x'+\frac 43)(y'-\frac 23)+(y'-\frac 23)^2+\frac 43\\
		&={x'}^2+\frac 83 x'+\frac{16}{9}-2x'-\frac 83+x'y'-\frac 23 x'+\frac 43y'-\frac89+{y'}^2-\frac 43y'+\frac 49+\frac 43\\
		&={x'}^2+x'y'+{y'}^2\\
		&=\frac 12 ({x'}^2+{y'}^2)+\frac 12(x'+y')^2\\
		&\ge0
		\end{align*}
		$$

		Le point critique est donc un minimum global.

		Cherchons à voir s'il existe un maximum global. On a $f(x,y)=x^2-2x+xy+y^2=(x-1)^2-1+xy+y^2=(x-1)^2+y^2+xy-1\le 1+1+0-1=1$. Or $f(-1,0)=1$ donc c'est un majorant atteint : c'est un maximum. On peut ensuite vérifier que ce maximum global n'est atteint qu'en ce point.






!!! exerciceb "Corrigé <a name="corrige-18"></a><a href="#enonce-18">18</a>"

		Calculons les dérivées partielles qui existent par théorèmes généraux.
		$\frac{\partial f}{\partial x}(x,y)=3x^2-3y$ et $\frac{\partial f}{\partial y}(x,y)=3y^2-3x$. Les points critiques sont alors définis par les équations $x^2=y$ et $y^2=x$ ce qui donne $x(x-1)(x^2+x+1)=0$, donc $x=0$ ou $x=1$ et  les couples $(x,y)=(0,0)$ ou $(x,y)=(1,1)$.

		Or $f(1,1)=-1$, $f(0,0)=0$, $f(-1,-1)=1$ (donc $(0,0)$ n'est pas un extremum global) et $f(-2,-2)=-28$. DONC $(1,1)$ n'est pas un extremum global.

		On a $f(t,t)=2t^3-3t^2=t^2(2t-3)$. Le point $(0,0)$ est un maximum local dans cette direction. Par ailleurs $f(t,-t)=t^3-t^3+3t^2=3t^2$. Le point $(0,0)$ est un minimum local dans cette direction. Donc $(0,0)$ n'est pas un extremum local.

		Considérons maintenant le point $(1,1)$. Posons $x'=x-1$ et $y'=y-1$.

		$$
		\begin{align*}
		f(x,y)+1&=(x'+1)^3+(y'+1)^3-3(x'+1)(y'+1)+1\\
		&=x'^3+3x'^2+3x'+1+y'^3+3y'^2+3y'+1-3x'y'-3x'-3y'-3+1\\
		&=x'^3+3x'^2+y'^3+3y'^2-3x'y'\\
		&\ge x'^3+3x'^2+y'^3+3y'^2-\frac 32(x'^2+y'^2)\\
		&\ge x'^3+y'^3+\frac32 x'^2+\frac 32 y'^2\\
		&\ge x'^2(x'+\frac 32)+y'^2(y'+\frac 32)
		\end{align*}
		$$

		La dernière quantité est positive  pour $x'>-\frac 32$ et $y'>-\frac 32$ : $(1,1)$ est donc un point de minimum local.







!!! exercice "Corrigé <a name="corrige-19"></a><a href="#enonce-19">19</a>"

		 Première problème de Cauchy : $y(x)=\cos(wx)$.

		 Deuxième problème de Cauchy : $y(x)=\cosh(wx)$.

		 La relation demandé est une relation de trigonométrie hyperbolique classique. En sommant $\cosh(x+y)$ et $\cosh(x-y)$ on montre que $\cosh$ est solution de $(*)$. Il en va de même d'ailleurs pour $\cos$.

		 Soit $f$ une solution de $(*)$. en prenant $x=y=0$ on a $2f(0)=2f(0)^2$ et donc $f(0)\in\{0,1\}$. Si $f(0)=0$ alors en prenant $y=0$ dans $(*)$ on a $2f(x)=0$ pour tout $x\in\mathbb R$ donc $f$ est la fonction nulle. Concentrons nous sur $f(0)=1$. On dérive par rapport à $y$ l'équation $(*)$ et on prend $y=0$ : $f(x)-f(x)=2f(x)f'(0)$ donc $2f(x)f'(0)=0$. En particulier pour $x=0$ : $2f'(0)=0$ c'est-à-dire $f'(0)=0$.

		 Soit donc $f$ une solution $\mathcal C^2$ de $(*)$. En dérivant par rapport à $y$ deux fois, on a $f''(x+y)+f''(x-y)=2f(x)f''(y)$. Puis en prenant $y=0$ : $f''(x)=f(x)f''(0)$. On se ramène aux cas des deux premières questions avec $w^2=\pm f''(0)$ en fonction du signe de $f''(0)$. On en déduit que les solutions sont de la forme $x\mapsto\cos(wx)$ ou $x\mapsto \cosh(wx)$, dont on a vérifié avant qu'elles étaient effectivement solutions.  






!!! exercice "Corrigé <a name="corrige-20"></a><a href="#enonce-20">20</a>"


		1.  On trouve comme solution de l’équation homogène sur $\left]  0,+\infty\right[$ la droite vectorielle engendrée par $x\longmapsto x^{\frac{3}{2}}$.
		    En effet, une primitive de $x\longmapsto\dfrac{3}{2x}$ sur $\left] 0,+\infty\right[$ est $x\longmapsto\dfrac{3}{2}\ln x$.

		2.  On utilise la méthode de variation de la constante en cherchant une fonction $k$ telle que $x\longmapsto k(x)x^\frac{3}{2}$ soit une solution de l’équation complète $(E)$ sur $\left]  0,+\infty\right[$.
		    On arrive alors à $2k'(x)x^\frac{5}{2}=\sqrt{x}$ et on choisit $k(x)=-\dfrac{1}{2x}$.
		    Les solutions de $(E)$ sur $\left]  0,+\infty\right[$ sont donc les fonctions $x\longmapsto kx^\frac{3}{2}-\dfrac{1}{2}\sqrt{x}$ avec $k\in\mathbb{R}$.

		3.  Si on cherche à prolonger les solutions de $(E)$ sur $\left[  0,+\infty\right[$, alors le prolongement par continuité ne pose pas de problème en posant $f(0)=0$.
		    Par contre, aucun prolongement ne sera dérivable en 0 car $\dfrac{f(x)-f(0)}{x-0}=k\sqrt{x}-\dfrac{1}{2}\dfrac{1}{\sqrt{x}}\underset{x\to 0}{\longrightarrow}-\infty$.
		    Conclusion: l’ensemble des solutions de l’équation différentielle $2xy'-3y=\sqrt{x}$ sur $\left[  0,+\infty\right[$ est l’ensemble vide.





!!! exerciceb "Corrigé <a name="corrige-21"></a><a href="#enonce-21">21</a>"

		Soit $f(x)=x^\alpha$ une solution de $(E_0)$. Alors $\alpha(\alpha-1)x^\alpha+4\alpha x^\alpha+2x^\alpha = (\alpha^2+3\alpha+2)x^\alpha= 0$. On en déduit que $\alpha = -1$ ou $\alpha=-2$. On obtient alors deux solutions libres : c'est une base de l'ensemble des solutions de l'équation homogène.

		Cherchons maintenant une solution de $(E)$ $y$ sous la forme $y(x)=\frac{z(x)}{x^2}$ c'est à dire $z(x)=x^2y(x)$. On a alors $z'(x)=2xy(x)+x^2y'(x)$ et $z''(x)=2y(x)+4xy'(x)+x^2y''(x)$. L'équation $(E)$ devient $z''(x) = \frac1{x\sqrt x}$. Une solution particulière est donc $z(x)=-4\sqrt x$ et donc $y(x)=\frac {-4}{x\sqrt x}$.

		On en déduit que l'ensemble des solutions de $(E)$ est $y(x)=\frac {-4}{x\sqrt x}+\frac ax+\frac b{x^2}$.






!!! exerciceb "Corrigé <a name="corrige-22"></a><a href="#enonce-22">22</a>"


		1.  $f$ est clairement linéaire.(\*) De plus, $\forall \:P\in E\backslash\left\lbrace 0\right\rbrace$, $\deg P'< \deg P$ donc $\deg (P-P')=\deg P$.
		    Et, si $P=0$, alors $P-P'=0$ donc $\deg( P-P')=\deg P=-\infty$.
		    On en déduit que $\forall\:P\in E$, $\deg f(P)=\deg P$.
		    Donc $f(E)\subset E$.(\*\*)
		    D’après (\*) et (\*\*), $f$ est bien un endomorphisme de $E$.

		    1.  Déterminons $\mathrm{Ker} f$.
		        Soit $P\in\mathrm{Ker} f$.
		        $f(P)=0$ donc $P-P'=0$ donc $\deg (P-P')=-\infty$.
		        Or, d’après ce qui précéde, $\deg(P-P')=\deg P$ donc $\deg P=-\infty$.
		        Donc $P=0$.
		        On en déduit que $\mathrm{Ker} f=\left\lbrace0\right\rbrace$.
		        Donc $f$ est injectif.
		        Or, $f\in\mathcal{L}\left( E\right)$ et $E$ est de dimension finie ($\dim E=n+1$) donc $f$ est bijectif.

		    2.  Soit $e=(1,X,...,X^n)$ la base canonique de $E$. Soit $A$ la matrice de $f$ dans la base $e$.
		        $A=\left( {
		        \begin{array}{cccc}
		         1 & { - 1} & {} & {(0)}  \\
		         {} & 1 &  \ddots  & {}  \\
		         {} & {} &  \ddots  & { - n}  \\
		         {(0)} & {} & {} & 1  \\
		        \end{array}
		        } \right)\in\mathcal{M}_{n+1}\left( \mathbb{R}\right) .$
		        $\det A=1$ d’où $\det A\neq 0$.
		        Donc $f$ est bijectif.

		2.  Soit $Q\in E$.
		    D’après 1.: $\exists\:!P\in E$, tel que $f(P) = Q$.
		    $P - P' = Q\text{, }P' - P'' = Q'\text{,\ldots , }P^{(n)}  - P^{(n + 1)}  = Q^{(n)}$.
		    Or $P^{(n + 1)}  = 0$, donc, en sommant ces $n+1$ égalités, $P = Q + Q' +  \cdots  + Q^{(n)}$.

		3.  Reprenons les notations de 1.(b).
		    Tout revient à se demander si $A$ est diagonalisable.
		    Notons $P_A(X)$ le polynôme caractéristique de $A$.
		    D’après 1.(b), on a $P_A(X)=(X-1)^{n+1}$.
		    Donc $1$ est l’unique valeur propre de $A$.
		    Ainsi, si $A$ était diagonalisable, alors $A$ serait semblable à la matrice unité $\mathrm{I}_{n+1}$.
		    On aurait donc $A=\mathrm{I}_{n+1}$.
		    Ce qui est manifestement faux car $f\neq \mathrm{Id}$.
		    Donc $A$ n’est pas diagonalisable et par conséquent, $f$ n’est pas diagonalisable.




!!! exerciceb "Corrigé <a name="corrige-23"></a><a href="#enonce-23">23</a>"


		1.  Posons $M = \left( {
		    \begin{array}{cc}
		     a & b  \\
		     c & d  \\
		    \end{array}
		    } \right) \in {\mathcal{M}}_2 (\mathbb{R})$.
		    On a $f(M) = \left( {
		    \begin{array}{cc}
		     {a + 2c} & {b + 2d}  \\
		     {2a + 4c} & {2b + 4d}  \\
		    \end{array}
		    } \right)$.
		    Alors $M\in\mathrm{Ker} f\Longleftrightarrow$ $\exists\:(a,b,c,d)\in\mathbb{R}^4$ tel que $M=\begin{pmatrix}
		    a&b\\c&d
		    \end{pmatrix}$ avec $\left\lbrace
		    \begin{array}{lll}
		    a&=&-2c\\
		    b&=&-2d
		    \end{array}
		    \right.$.
		    C’est-à-dire, $M\in\mathrm{Ker} f$ $\Longleftrightarrow$ $\exists\:(c,d)\in\mathbb{R}^2$ tel que $M=\begin{pmatrix}
		    -2c&-2d\\
		    c&d
		    \end{pmatrix}$.
		    On en déduit que $\mathrm{Ker} f = \textrm{Vect} \left\{ {\left( {
		    \begin{array}{cc}
		     -2 & 0  \\
		     { 1} & 0  \\
		    \end{array}
		    } \right),\left( {
		    \begin{array}{cc}
		     0 & -2  \\
		     0 & { 1}  \\
		    \end{array}
		    } \right)} \right\}$.(\*)
		    On pose $M_1=\left(
		    \begin{array}{cc}
		     -2 & 0  \\
		     {  1} & 0  \\
		    \end{array}
		     \right)$ et $M_2=\left(
		    \begin{array}{cc}
		     0 & -2  \\
		     0 & {  1}  \\
		    \end{array}
		     \right)$.
		    D’après (\*), la famille $(M_1,M_2)$ est génératrice de $\mathrm{Ker} f$.
		    De plus, $M_1$ et $M_2$ sont non colinéaires; donc $(M_1,M_2)$ est libre.
		    Donc $(M_1,M_2)$ est une base de $\mathrm{Ker} f$.

		2.  $\mathrm{Ker} f\neq \left\lbrace 0\right\rbrace$, donc $f$ est non injectif.
		    Or $f$ est un endomorphisme de $\mathcal{M}_2(\mathbb{R})$ et $\mathcal{M}_2(\mathbb{R})$ est de dimension finie.
		    On en déduit que $f$ est non surjectif.

		3.  Par la formule du rang, $\textrm{rg} f = 2$.
		    On pose $M_3=
		    f(E_{1,1} ) = \left( {
		    \begin{array}{cc}
		     1 & 0  \\
		     2 & 0  \\
		    \end{array}
		    } \right)$ et $M_4=f(E_{2,2} ) = \left( {
		    \begin{array}{cc}
		     0 & 2  \\
		     0 & 4  \\
		    \end{array}
		    } \right)$.
		    $M_3$ et $M_4$ sont non colinéaires, donc $(M_3,M_4)$ est une famille libre de $\mathrm{Im}f$.
		    Comme $\textrm{rg} f = 2$, $(M_3,M_4)$ est une base de $\mathrm{Im}f$.

		4.  On a $\dim \mathcal{M}_2\left(\mathbb{R} \right)=\dim \mathrm{Ker} f+\dim\mathrm{Im}f$.(1)
		    Prouvons que $\mathrm{Ker}f\cap \mathrm{Im}f=\left\lbrace0 \right\rbrace$.
		    Soit $M\in \mathrm{Ker}f\cap \mathrm{Im}f$.
		    D’après 1. et 3., $\exists(a,b,c,d)\in\mathbb{R}^4$ tel que $M=aM_1+bM_2$ et $M=cM_3+dM_4$.
		    On a donc $\left\lbrace \begin{array}{lll}
		    -2a&=&c\\
		    -2b&=&2d\\
		    a&=&2c\\
		    b&=&4d
		    \end{array}\right.$.
		    On en déduit que $a=b=c=d=0$.
		    Donc $M=0$.
		    Donc $\mathrm{Ker}f\cap \mathrm{Im}f=\left\lbrace0 \right\rbrace$(2)
		    Donc, d’après (1) et (2), $\mathcal{M}_{2}\left( \mathbb{R}%
		    \right)=\mathrm{Ker}f\oplus \mathrm{Im}f$.





!!! exerciceb "Corrigé <a name="corrige-24"></a><a href="#enonce-24">24</a>"


		1.  Supposons $E = \textrm{Im} f \oplus \mathrm{Ker} f$.
		    Indépendamment de l’hypothèse, on peut affirmer que $\textrm{Im} f^2  \subset \textrm{Im} f$ (\*)
		    Montrons que $\mathrm{Im}f\subset\mathrm{Im}f^2$.
		    Soit $y \in \textrm{Im} f$.
		    Alors, $\exists\:x\in E$ tel que $y = f(x)$.
		    Or $E = \textrm{Im} f \oplus \mathrm{Ker} f$, donc $\exists\:(a,b)\in E\times\mathrm{Ker} f$ tel que $x = f(a) + b$.
		    On a alors $y = f^2 (a) \in \textrm{Im} f^2$.
		    Ainsi $\textrm{Im} f \subset \textrm{Im} f^2$(\*\*)
		    D’après (\*) et (\*\*), $\textrm{Im} f = \textrm{Im} f^2$.

		2.  1.  On a $\textrm{Im} f^2  \subset \textrm{Im} f$ et $\mathrm{Ker} f \subset \mathrm{Ker} f^2$.
		        On en déduit que $\textrm{Im} f^2 = \textrm{Im} f$ $\Longleftrightarrow$ $\mathrm{rg}f^2=\mathrm{rg}f$ et $\mathrm{Ker} f = \mathrm{Ker} f^2$ $\Longleftrightarrow$ $\dim \mathrm{Ker} f=\dim \mathrm{Ker} f^2$.
		        Alors, en utilisant le théorème du rang, $\textrm{Im} f = \textrm{Im} f^2  \Leftrightarrow \textrm{rg} f = \textrm{rg} f^2  \Leftrightarrow \dim \mathrm{Ker} f = \dim \mathrm{Ker} f^2  \Leftrightarrow \mathrm{Ker} f = \mathrm{Ker} f^2$.

		    2.  Supposons $\textrm{Im} f = \textrm{Im} f^2$.
		        Soit $x \in \textrm{Im} f \cap \mathrm{Ker} f$.
		        $\exists\:a\in E$ tel que $x = f(a)$ et $f(x) = 0_E$.
		        On en déduit que $f^2 (a) = 0_E$ c’est-à-dire $a \in \mathrm{Ker} f^2$.
		        Or, d’après l’hypothèse et 2.(a), $\mathrm{Ker} f^2  = \mathrm{Ker} f$ donc $a\in \mathrm{Ker} f$ c’est-à-dire $f(a) = 0_E$.
		        C’est-à-dire $x=0$.
		        Ainsi $\textrm{Im} f \cap \mathrm{Ker} f = \left\{ {0_E } \right\}$.(\*\*\*)
		        De plus, d’après le théorème du rang, $\dim \mathrm{Im}f + \dim \mathrm{Ker} f = \dim E$. (\*\*\*\*)

		        Donc, d’après (\*\*\*) et (\*\*\*\*), $E = \textrm{Im} f \oplus \mathrm{Ker} f$.





!!! exercice "Corrigé <a name="corrige-25"></a><a href="#enonce-25">25</a>"


		1.  $D=\mathrm{Vect}\left( (1,2,3)\right)$.
		    $(1,2,3)\not\in P$ car les coordonnées du vecteur $(1,2,3)$ ne vérifient pas l’équation de $P$.
		    Donc $D\cap P=\left\lbrace 0\right\rbrace$.(\*)
		    De plus, $\dim D+\dim P= 1+2=\dim\mathbb{R}^3$.(\*\*)
		    D’après (\*) et (\*\*), $\mathbb{R}^3=P\oplus D$.

		2.  Soit $u=(x,y,z)\in\mathbb{R}^3$.
		    Par définition d’une projection, $p(u)\in P$ et $u-p(u)\in D$.
		    $u-p(u)\in D$ signifie que $\exists\:\alpha \in\mathbb{R}$ tel que $u-p(u)=\alpha (1,2,3)$.
		    On en déduit que $p(u)=(x-\alpha, y-2\alpha, z-3\alpha)$.(\*\*\*)
		    Or $p(u)\in P$ donc $(x-\alpha)+( y-2\alpha)+(z-3\alpha)=0$, c’est-à-dire $\alpha=\dfrac{1}{6}(x+y+z)$.
		    Et donc, d’après (\*\*\*), $p(u)=\dfrac{1}{6}(5x-y-z,-2x+4y-2z,-3x-3y+3z)$.
		    Soit $e=(e_1,e_2,e_3)$ la base canonique de $\mathbb{R}^3$.
		    Soit $A$ la matrice de $p$ dans la base $e$. On a $A=\dfrac{1}{6}\begin{pmatrix}
		     5&-1&-1\\
		     -2&4&-2\\
		     -3&-3&3\\
		     \end{pmatrix}$.

		3.  On pose $e'_1=(1,2,3)$, $e'_2=(1,-1,0)$ et $e'_3=(0,1,-1)$.
		    $e'_1$ est une base de $D$ et $(e'_2,e'_3)$ est une base de $P$.
		    Or $\mathbb{R}^3=P\oplus D$ donc $e'=(e'_1,e'_2,e'_3)$ est une base de $\mathbb{R}^3$.
		    De plus $e'_1\in D$ donc $p(e'_1)=0$. $e'_2\in P$ et $e'_3\in P$ donc $p(e'_2)=e'_2$ et $p(e'_3)=e'_3$.
		    Ainsi, $M(p,e')=\begin{pmatrix}
		     0&0&0\\
		     0&1&0\\
		     0&0&1\\
		     \end{pmatrix}$.





!!! exercice "Corrigé <a name="corrige-26"></a><a href="#enonce-26">26</a>"


		1.  Par linéarité de l’évaluation $P\mapsto P(a)$ (où $a$ est un scalaire fixé), $\Phi$ est linéaire.

		    Soit $P\in\mathbb K_{2}[X]$ tel que $\Phi(P)=0$.
		    Alors $P(a_1)=P(a_2)=P(a_3)=0$, donc $P$ admet trois racines distinctes.
		    Or $P$ est de degré inférieur ou égal à 2; donc $P$ est nul.
		    Ainsi, $\mathrm{Ker}(\Phi)=\{0\}$ i.e. $\Phi$ est injective.

		    Enfin, $\dim\big(\mathbb K_{2}[X]\big)=\dim\big(\mathbb K^{3}\big)=3$ donc $\Phi$ est bijective.

		    Par conséquent, $\Phi$ est un isomorphisme d’espaces vectoriels de $\mathbb{K}_2\left[ X\right]$ dans $\mathbb{K} ^3$.

		2.  1.  $\Phi$ est un isomorphisme donc l’image réciproque d’une base est une base.

		        Ainsi, $(L_{1}, L_{2}, L_{3})$ est une base de $\mathbb K_{2}[X]$.

		    2.  $L_{1}\in\mathbb R_{2}[X]$ et vérifie $\Phi(L_{1})=(1,0,0)$ i.e. $\big(L_{1}(a_{1}),L_{1}(a_{2}),L_{1}(a_{3})\big)=(1,0,0)$.

		        Donc, comme $a_2$ et $a_3$ sont distincts, $(X-a_{2})(X-a_{3})\,|L_{1}$.
		        Or $\deg L_1\leqslant 2$, donc $\exists k\in{\mathbb{K}}$ tel que $L_{1}=k(X-a_{2})(X-a_{3})$.
		        La valeur $L_{1}(a_{1})=1$ donne $k=\dfrac{1}{(a_{1}-a_{2})(a_{1}-a_{3})}$.
		        Donc $L_{1}=\dfrac{(X-a_{2})(X-a_{3})}{(a_{1}-a_{2})(a_{1}-a_{3})}$.

		        Un raisonnement analogue donne $L_{2}=\dfrac{(X-a_{1})(X-a_{3})}{(a_{2}-a_{1})(a_{2}-a_{3})}$ et $L_{3}=\dfrac{(X-a_{1})(X-a_{2})}{(a_{3}-a_{1})(a_{3}-a_{2})}$.

		3.  $(L_1,L_2,L_3)$ base de $\mathbb{K}_2[X]$ donc $\exists(\lambda_1,\lambda_2,\lambda_3)\in{\mathbb{K}^3}$ tel que $P=\lambda_{1}L_{1}+\lambda_{2}L_{2}+\lambda_{3}L_{3}$.
		    Par construction, $\forall(i,j)\in\{1,2,3\}^{2}, L_{i}(a_{j})=\delta_{ij}$ donc $P(a_{j})=\lambda_{j}$.
		    Ainsi, $P=P(a_{1})L_{1}+P(a_{2})L_{2}+P(a_{3})L_{3}$.

		4.  On pose $a_{1}=0$, $a_{2}=1$ et $a_{3}=2$. Ces trois réels sont bien distincts.
		    On cherche $P\in\mathbb R_{2}[X]$ tel que $\big(P(a_{1}),P(a_{2}),P(a_{3})\big)=(1,3,1)$.

		    Par bijectivité de $\Phi$ et d’après 3. , l’unique solution est le polynôme $P=1.L_{1}+3.L_{2}+1.L_{3}$.
		    On a $L_{1}=\dfrac{(X-1)(X-2)}{2}$, $L_{2}=\dfrac{X(X-2)}{-1}$ et $L_{3}=\dfrac{X(X-1)}{2}$.
		    Donc $P=-2X^{2}+4X+1$.





!!! exerciceb "Corrigé <a name="corrige-27"></a><a href="#enonce-27">27</a>"

		On a $\dim(P)+\dim(D)=3$. Soit $u\in D\cap P$. $u\in D$, donc $u=(2\lambda,2\lambda,1)$. $u\in P$ donc $2\lambda-4\lambda+3\lambda=0$ càd $\lambda=0$. Ainsi les deux espaces sont bien supplémentaires.

		Les deux vecteurs $u=(2,1,0)$ et $v=(3,0,-1)$ forment une base de $P$, et le vecteur $w=(2,2,1)$ forme une base de $D$. Notons $p$ le projecteur et $s$ la symétrie. La matrice de $p$ dans la base $(u,v,w)$ est $A=\begin{pmatrix} 1 & 0 & 0\\0&1&0\\0&0&0\end{pmatrix}$ et la matrice de $s$ est $\begin{pmatrix} 1 & 0 & 0\\0&1&0\\0&0&-1\end{pmatrix}$. La matrice de passage de la base canonique vers cette base est $P=\begin{pmatrix}2&3&2\\1&0&2\\0 & -1 &1\end{pmatrix}$. Pour avoir la matrice de $p$ dans la base canonique on fait $P A P^{-1}$. On laisse les calculs à la bonne volonté du lecteur.






!!! exercice "Corrigé <a name="corrige-28"></a><a href="#enonce-28">28</a>"

		 Soit $f$ un endomorphisme de $E$. Supposons que $\Ker(f)+\Im(f)=E$. On sait déjà que $\Im(f^2)\subset \Im(f)$. Soit $y\in\Im(f)$, posons $y=f(x_0)$ et $x_0=x_K+x_I$ avec $x_K\in\Ker(f)$ et $x_I\in\Im(f)$. Posons alors $x_I=f(z)$. On a :$y=f(x_K+x_I)=f(x_K)+f(x_I)=0+f(f(z))\in\Im(f^2)$. Donc $\Im(f)\subset \Im(f^2)$ et ainsi $\Im(f)=\Im(f^2)$.

		 Réciproquement, si $\Im(f^2)=\Im(f)$, on sait que $\Ker(f)+\Im(f)\subset E$. C'est l'inclusion réciproque qui nous intéresse. Soit $x\in E$, on a $f(x)\in\Im(f)$ et donc $f(x)\in\Im f^2$. On en déduit qu'il existe $z\in E$ tel que $f(x)=f^2(z)$. Posons $y=x-f(z)$, alors $f(y)=f(x)-f^2(z)=0$. Donc $y\in\Ker f$. On en déduit $x=y+f(z)\in \Ker f+\Im f$ et donc $E=\Ker f+\Im f$.  






!!! exercice "Corrigé <a name="corrige-29"></a><a href="#enonce-29">29</a>"


		1.  1.  Soit $E$ un $\mathbb{R}$-espace vectoriel muni d’un produit scalaire noté $\left(\:|\:\right)$.
		        On pose $\forall\:x\in E$, $||x||=\sqrt{(x|x)}$.

		        Inégalité de Cauchy-Schwarz: $\forall (x,y)\in E^2$, $|\left(x|y\right)|\leqslant ||x||\,||y||$
		        Preuve:
		        Soit $(x,y)\in E^2$. Posons $\forall \lambda\in \mathbb{R}$, $P(\lambda)=||x+\lambda y||^2$.
		        On remarque que $\forall \lambda \in \mathbb{R}$, $P(\lambda )\geqslant 0$.
		        De plus, $P(\lambda)=\left(x+\lambda y|x+\lambda y\right)$.
		        Donc, par bilinéarité et symétrie de $\left(\:|\:\right)$, $P(\lambda )=||y||^2\lambda ^2+2\lambda \left(x|y\right)+||x||^2$.
		        On remarque que $P(\lambda)$ est un trinôme en $\lambda$ si et seulement si $||y||^2\neq 0$.
		        **Premier cas**: si $y=0$
		        Alors $|\left(x|y\right)|=0$ et $||x||\,||y||=0$ donc l’inégalité de Cauchy-Schwarz est vérifiée.
		        **Deuxième cas**: $y\neq 0$
		        Alors $||y||=\sqrt{(y|y)}\neq 0$ car $y\neq 0$ et $\left(\:|\:\right)$ est une forme bilinéaire symétrique définie positive.
		        Donc, $P$ est un trinôme du second degré en $\lambda$ qui est positif ou nul.
		        On en déduit que le discriminant réduit $\Delta$ est négatif ou nul.
		        Or $\Delta=\left(x|y\right)^2-||x||^2||y||^2$ donc $\left(x|y\right)^2\leqslant||x||^2||y||^2$.
		        Et donc, $|\left(x|y\right)|\leqslant ||x||\,||y||$.

		    2.  On reprend les notations de 1. .
		        Prouvons que $\forall (x,y)\in E^2$, $|\left(x|y\right)|=||x||\,||y||$ $\Longleftrightarrow$ $x$ et $y$ sont colinéaires.
		        Supposons que $|\left(x|y\right)|=||x||\,||y||$.
		        Premier cas: si $y=0$
		        Alors $x$ et $y$ sont colinéaires.
		        Deuxième cas: si $y\neq 0$
		        Alors le discriminant de $P$ est nul et donc $P$ admet une racine double $\lambda_0$.
		        C’est-à-dire $P(\lambda_0)=0$ et comme $\left(\:|\:\right)$ est définie positive, alors $x+\lambda_0y=0$.
		        Donc $x$ et $y$ sont colinéaires.
		        Supposons que $x$ et $y$ soient colinéaires.
		        Alors $\exists\:\alpha\in\mathbb{R}$ tel que $x=\alpha y$ ou $y=\alpha x$.
		        Supposons par exemple que $x=\alpha y$ (raisonnement similaire pour l’autre cas).
		        $|\left(x|y\right)|=|\alpha|.|\left(y|y\right)|=|\alpha|\,||y||^2$ et $||x||\,||y||=\sqrt{(x|x)}\,||y||=\sqrt{\alpha^2(y|y)}||y||=|\alpha|.||y||^2$.
		        Donc, on a bien l’égalité.

		2.  On considère le produit scalaire classique sur $\mathcal{C}\left( \left[ a,b\right] ,\mathbb{R}\right)$ défini par :
		    $\forall (f,g)\in \mathcal{C}\left( \left[ a,b\right] ,\mathbb{R}\right)$, $(f|g)=\displaystyle\int_{a}^{b}f(t)g(t)dt$.
		    On pose $A=\left\lbrace \displaystyle\int_{a}^{b}f(t)\mathrm{d}t\times \displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t\:,\:f\in E\right\rbrace$.
		    $A\subset \mathbb{R}$.
		    $A\neq \emptyset$ car $(b-a)^2\in A$ ( valeur obtenue pour la fonction $t\longmapsto 1$ de $E$).
		    De plus, $\forall\:f\in E$,$\displaystyle\int_{a}^{b}f(t)\mathrm{d}t\times \displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t\geqslant 0$ donc $A$ est minorée par 0.
		    On en déduit que $A$ admet une borne inférieure et on pose $m=\inf A$.
		    Soit $f\in E$.
		    On considère la quantité $\left( \displaystyle\int_{a}^{b}\sqrt{f(t)}\dfrac{1}{\sqrt{f(t)}}\mathrm{d}t\right) ^2$.
		    D’une part, $\left( \displaystyle\int_{a}^{b}\sqrt{f(t)}\dfrac{1}{\sqrt{f(t)}}\mathrm{d}t\right)  ^2=\left( \displaystyle\int_{a}^{b}1\mathrm{d}t\right) ^2=(b-a)^2.$
		    D’autre part, si on utilise l’inégalité de Cauchy-Schwarz pour le produit scalaire $(\:|\:)$ on obtient:
		    $\left( \displaystyle\int_{a}^{b}\sqrt{f(t)}\dfrac{1}{\sqrt{f(t)}}\mathrm{d}t\right) ^2\leqslant \displaystyle\int_{a}^{b} f(t)\mathrm{d}t\displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t$.
		    On en déduit que $\forall\:f\in E$, $\displaystyle\int_{a}^{b} f(t)\mathrm{d}t\displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t\geqslant (b-a)^2$.
		    Donc $m\geqslant (b-a)^2$.
		    Et, si on considère la fonction $f:t\longmapsto 1$ de $E$, alors $\displaystyle\int_{a}^{b} f(t)\mathrm{d}t\displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t= (b-a)^2$.
		    Donc $m=(b-a)^2$.





!!! exerciceb "Corrigé <a name="corrige-30"></a><a href="#enonce-30">30</a>"


		1.  On a $A \subset \left( {A^ \bot  } \right)^ \bot  \text{ }$.(\*)
		    En effet, $\forall x \in A,\forall y \in A^ \bot  ,(x\mid y) = 0$.
		    C’est-à-dire, $\forall\:x\in A$, $x\in (A^{\perp})^{\perp}$.
		    Comme $E$ est un espace euclidien, $E=A\oplus A^{\perp}$ donc $\dim A= n-\dim A^{\perp}$.
		    De même, $E=A^{\perp}\oplus\left( A^{\perp}\right) ^{\perp}$ donc $\dim \left( A^ \bot\right) ^{\perp}   = n - \dim A^{\perp}$.
		    Donc $\dim \left( {A^ \bot  } \right)^ \bot   = \dim A$. (\*\*)
		    D’après (\*) et (\*\*), $\left( {A^ \bot  } \right)^ \bot   = A$.

		2.  1.  Procédons par double inclusion.
		        Prouvons que $F^{\perp}\cap G^{\perp}\subset \left(F+G \right)^{\perp}$.
		        Soit $x \in F^ \bot   \cap G^ \bot$.
		        Soit $y \in F + G$ .
		        Alors $\exists\:(f,g)\in F\times G$ tel que $y=f+g$.
		        $(x\mid y) = \underbrace{(x\mid f)}_{ \underset{\: \text{car} \:f\in F\:\text{et}\:x\in F^{\perp}}{=0}}+ \underbrace{(x\mid g)}_{ \underset{\: \text{car} \:g\in G \:\text{et}\:x\in G^{\perp}}{=0}} = 0$.
		        Donc $\forall \:y\in (F+G)$, $(x\mid y) =0$.
		        Donc $x \in (F + G)^ \bot$.
		        Prouvons que $\left(F+G \right)^{\perp}\subset F^{\perp}\cap G^{\perp}$.
		        Soit $x \in (F + G)^ \bot$.
		        $\forall \:y \in F$, on a $(x\mid y) = 0$ car $y \in F \subset F + G$.
		        Donc $x \in F^ \bot$.
		        De même, $\forall \:z \in G$, on a $(x\mid z) = 0$ car $z \in G \subset F + G$.
		        Donc $x \in G^ \bot$.
		        On en déduit que $x\in F^{\perp}\cap G^{\perp}$.
		        Finalement, par double inclusion, $\left( {F + G} \right)^ \bot   = F^ \bot   \cap G^ \bot$.

		    2.  D’après 2.(a), appliquée à $F^{\perp}$ et à $G^{\perp}$, on a $\left( F^{\perp} + G^{\perp} \right)^ {\perp  } = \left( F^{\perp}\right) ^{\perp}   \cap\left(  G^ {\perp}\right) ^{\perp}$.
		        Donc, d’après 1., $\left( F^{\perp} + G^{\perp} \right)^ {\perp  } = F   \cap G$.
		        Donc $\left( \left( F^{\perp} + G^{\perp} \right)^ {\perp  }\right) ^{\perp} =\left(  F   \cap G \right) ^{\perp}$.
		        C’est-à-dire, en utilisant 1. à nouveau, $F^{\perp}+G^{\perp }=\left( F\cap G\right) ^{\perp }$.





!!! exercice "Corrigé <a name="corrige-31"></a><a href="#enonce-31">31</a>"


		1.  Soit $h$ une fonction continue et positive de $[a,b]$ dans $\mathbb{R}$ telle que $\displaystyle\int_{a}^{b}h(x)\text{d}x=0$.
		    On pose $\forall \:x\in \left[ a,b\right]$, $F(x)=\displaystyle\int_{a}^{x} h(t)dt$.
		    $h$ est continue sur $\left[ a,b\right]$ donc $F$ est dérivable sur $\left[ a,b\right]$.
		    De plus, $\forall\:x\in\left[ a,b\right]$, $F'(x)=h(x)$.
		    Or $h$ est positive sur $\left[ a,b\right]$ donc $F$ est croissante sur $\left[ a,b\right]$.(\*)
		    Or $F(a)=0$ et, par hypothèse, $F(b)=0$. C’est-à-dire $F(a)=F(b)$.(\*\*)
		    D’après (\*) et (\*\*), $F$ est constante sur $\left[ a,b\right]$.
		    Donc $\forall\:x\in \left[ a,b\right]$, $F'(x)=0$.
		    C’est-à-dire, $\forall\:x\in \left[ a,b\right]$, $h(x)=0$.

		2.  On pose $\forall\:(f,g)\in E^2$, $\left( f|g\right) =\displaystyle\int_{a}^{b}f(x)g(x)\text{d}x$.
		    Par linéarité de l’intégrale, $\left(\:|\:\right)$ est linéaire par rapport à sa première variable.
		    Par commutativité du produit sur $\mathbb{R}$, $\left(\:|\:\right)$ est symétrique.
		    On en déduit que $\left(\:|\:\right)$ est une forme bilinéaire symétrique.(\*)
		    Soit $f\in E$. $\left(f|f\right)=\displaystyle\int_{a}^{b}f^2(x)\mathrm{d}x$.
		    Or $x\longmapsto f^2(x)$ est positive sur $\left[ a,b\right]$ et $a<b$ donc $\left(f|f\right)\geqslant 0$.
		    Donc $\left(\:|\:\right)$ est positive.(\*\*)
		    Soit $f\in E$ telle que $\left(f|f\right)=0$.
		    Alors $\displaystyle\int_{a}^{b}f^2(x)\mathrm{d}x=0$.
		    Or $x\longmapsto f^2(x)$ est positive et continue sur $\left[ a,b\right]$ .
		    Donc, d’après 1., $f$ est nulle sur $\left[ a,b\right]$ .
		    Donc $\left(\:|\:\right)$ est définie.(\*\*\*)
		    D’après (\*), (\*\*) et (\*\*\*), $\left(\:|\:\right)$ est un produit scalaire sur $E$.

		3.  L’inégalité de Cauchy-Schwarz donne $\displaystyle\int_{0}^{1} {\sqrt x {\mathrm{e}}^{ - x} \,{\mathrm{d}}x}  \leqslant \sqrt {\displaystyle\int_{0}^{1} {x\,{\mathrm{d}}x} } \sqrt {\displaystyle\int_0^1 {{\mathrm{e}}^{ - 2x} \,{\mathrm{d}}x} }  = \dfrac{{\sqrt {1 - {\mathrm{e}}^{ - 2} } }}{2}$.





!!! exercice "Corrigé <a name="corrige-32"></a><a href="#enonce-32">32</a>"


		1.  On pose $\forall\:(f,g)\in E^2$, $\left( f|g\right) =\dfrac{1}{2\pi}\displaystyle\int_{0}^{2\pi}f(t)g(t)\text{d}t$.
		    Par linéarité de l’intégrale, $\left(\:|\:\right)$ est linéaire par rapport à sa première variable.
		    Par commutativité du produit sur $\mathbb{R}$, $\left(\:|\:\right)$ est symétrique.
		    On en déduit que $\left(\:|\:\right)$ est une forme bilinéaire symétrique.(\*)
		    Soit $f\in E$. $\left(f|f\right)=\dfrac{1}{2\pi}\displaystyle\int_{0}^{2\pi}f^2(t)\mathrm{d}t$.
		    Or $t\longmapsto f^2(t)$ est positive sur $\left[ 0,2\pi\right]$ et $0<2\pi$, donc $\left(f|f\right)\geqslant 0$.
		    Donc $\left(\:|\:\right)$ est positive.(\*\*)
		    Soit $f\in E$ telle que $\left(f|f\right)=0$.
		    Alors $\displaystyle\int_{0}^{2\pi}f^2(t)\mathrm{d}t=0$.
		    Or $t\longmapsto f^2(t)$ est positive et continue sur $\left[ 0,2\pi\right]$.
		    Donc, $f$ est nulle sur $\left[ 0,2\pi\right]$.
		    Or $f$ est $2\pi$-périodique donc $f=0$.
		    Donc $\left(\:|\:\right)$ est définie.(\*\*\*)
		    D’après (\*), (\*\*) et (\*\*\*), $\left(\:|\:\right)$ est un produit scalaire sur $E$.

		2.  On a $\forall x \in \mathbb{R},\sin ^2 x = \dfrac{1}{2} -  \dfrac{1}{2}\cos (2x)$.
		    $x\longmapsto -\dfrac{1}{2}\cos (2x)$$\in F$.
		    De plus, si on note $h$ l’application $x \mapsto \dfrac{1}{2}$,
		    $\left(h|f\right)=\dfrac{1}{4\pi}\displaystyle\int_{0}^{2\pi}\cos x \mathrm{d}x=0$ et $\left(h|g\right)=\dfrac{1}{4\pi}\displaystyle\int_{0}^{2\pi}\cos (2x)\mathrm{d}x=0$ donc $h\in F^{\perp}$ (car $F=\mathrm{Vect}(f,g)$).
		    On en déduit que le projeté orthogonal de $u$ sur $F$ est $x\longmapsto -\dfrac{1}{2}\cos (2x)$.





!!! exerciceb "Corrigé <a name="corrige-33"></a><a href="#enonce-33">33</a>"


		1.  On a immédiatement ${\mathcal{F}} = \textrm{Vect} (\mathrm{I}_2,K)$ avec $K = \left( {
		    \begin{array}{cc}
		     0 & 1  \\
		     { - 1} & 0  \\
		    \end{array}
		    } \right)\text{ }$.
		    On peut donc affirmer que ${\mathcal{F}}$ est un sous-espace vectoriel de ${\mathcal{M}}_2 (\mathbb{R})$.

		    ${\mathcal{F}} = \textrm{Vect} (\mathrm{I}_2,K)$ donc $(\mathrm{I}_2,K)$ est une famille génératrice de $\mathcal{F}$.
		    De plus, $\mathrm{I}_2$ et $K$ sont non colinéaires donc la famille $(\mathrm{I}_2,K)$ est libre.
		    On en déduit que $(\mathrm{I}_2,K)$ est une base de $\mathcal{F}$.

		2.  Soit $M=\begin{pmatrix}
		     a&b\\
		     c&d
		     \end{pmatrix}\in{\mathcal{M}}_2\left( \mathbb{R}\right)$.
		    Comme $(\mathrm{I}_2,K)$ est une base de $\mathcal{F}$,
		    $M\in\mathcal{F}^{\perp}\Longleftrightarrow$ $\varphi(M,\mathrm{I}_2)=0$ et $\varphi(M,K)=0$.
		    C’est-à-dire, $M\in\mathcal{F}^{\perp}\Longleftrightarrow$ $a+d=0$ et $b-c=0$.
		    Ou encore, $M\in\mathcal{F}^{\perp}\Longleftrightarrow$ $d=-a$ et $c=b$.
		    On en déduit que $\mathcal{F}^{\perp}=\mathrm{Vect}\left( A,B\right)$ avec $A = \left(
		    \begin{array}{cc}
		     1 & 0  \\
		     0 & { - 1}  \\
		    \end{array}
		     \right)\text{ et }B = \left( {
		    \begin{array}{cc}
		     0 & 1  \\
		     1 & 0  \\
		    \end{array}
		    } \right)$.
		    $(A,B)$ est une famille libre et génératrice de $\mathcal{F}^{\perp}$ donc $(A,B)$ est une base de $\mathcal{F}^{\perp}$.

		3.  On peut écrire $J = \mathrm{I}_2 +  B\text{  avec }\mathrm{I}_2 \in {\mathcal{F}}\text{ et } B \in {\mathcal{F}}^ \bot$.
		    Donc le projeté orthogonal de $J$ sur $\mathcal{F}^{\perp}$ est $B=\begin{pmatrix}
		    0&1\\1&0
		    \end{pmatrix}$.

		4.  On note $d(J,\mathcal{F})$ la distance de $J$ à $\mathcal{F}$.
		    D’après le cours, $d(J,\mathcal{F})=||J-p_{\mathcal{F}}(J)||$ où $p_{\mathcal{F}}(J)$ désigne le projeté orthogonal de $J$ sur $\mathcal{F}$.
		    On peut écrire à nouveau que $J = \mathrm{I}_2 +  B\text{  avec }\mathrm{I}_2 \in {\mathcal{F}}\text{ et } B \in {\mathcal{F}}^ \bot$.
		    Donc $p_{\mathcal{F}}(J)=\mathrm{I}_{2}$.
		    On en déduit que $d(J,\mathcal{F})=||J-p_{\mathcal{F}}(J)||=||J-\mathrm{I}_2||=||B||=\sqrt{2}$.





!!! exercice "Corrigé <a name="corrige-34"></a><a href="#enonce-34">34</a>"


		1.  On pose $E=\mathcal{M}_2(\mathbb{R})$.
		    Pour $A=\begin{pmatrix}
		    a&b\\c&d\\
		    \end{pmatrix}\in E$ et $A'=\begin{pmatrix}
		    a'&b'\\c'&d'\\
		    \end{pmatrix}\in E$ , on pose $\left(A|A'\right)=aa'+bb'+cc'+dd'$.
		    Soit $A=\begin{pmatrix}
		    a&b\\c&d\\
		    \end{pmatrix}\in E$ , $A'=\begin{pmatrix}
		    a'&b'\\c'&d'\\
		    \end{pmatrix}\in E$ , $B=\begin{pmatrix}
		    a''&b''\\c''&d''\\
		    \end{pmatrix}\in E$ . Soit $\alpha\in\mathbb{R}$.
		    $\left(A+A'|B\right)=\left(\begin{pmatrix}
		    a+a'&b+b'\\c+c'&d+d'\\
		    \end{pmatrix}|\begin{pmatrix}
		    a''&b''\\c''&d''\\
		    \end{pmatrix}
		    \right)=(a+a')a''+(b+b')b''+(c+c')c''+(d+d')d''$.
		    Donc $\left(A+A'|B\right)=(aa''+bb''+cc''+dd'')+(a'a''+b'b''+c'c''+d'd'')=\left(A|B\right)+\left(A'|B\right)$.
		    $\left(\alpha A|B\right)=\left(\begin{pmatrix}
		    \alpha a& \alpha b\\
		    \alpha c&\alpha d\\
		    \end{pmatrix}|\begin{pmatrix}
		    a''&b''\\c''&d''
		    \end{pmatrix}\right )= \alpha aa''+\alpha bb''+\alpha cc''+ \alpha dd''=\alpha\left(A|B\right )$.
		    On en déduit que $\left( \,.\,|\,.\,\right)$ est linéaire par rapport à sa première variable.
		    De plus, par commutativité du produit sur $\mathbb{R}$, $\left( \,.\,|\,.\,\right)$ est symétrique.
		    Donc $\left( \,.\,|\,.\,\right)$ est une forme bilinéaire et symétrique.(\*)
		    Soit $A=\begin{pmatrix}
		    a&b\\c&d\\
		    \end{pmatrix}\in E$ .
		    $\left(A|A\right)=a^2+b^2+c^2+d^2\geqslant 0$. Donc $\left( .\:|\:.\right)$ est positive.(\*\*)
		    Soit $A=\begin{pmatrix}
		    a&b\\c&d\\
		    \end{pmatrix}\in E$ telle que $\left(A|A\right)=0$.
		    Alors $a^2+b^2+c^2+d^2= 0$.
		    Comme il s’agit d’une somme de termes tous positifs, on en déduit que $a=b=c=d=0$ donc $A=0$.
		    Donc $\left( \,.\,|\,.\,\right)$ est définie.(\*\*\*)
		    D’après (\*), (\*\*) et (\*\*\*), $\left( \,.\,|\,.\,\right)$ est un produit scalaire sur $E$.

		2.  $A=\begin{pmatrix}
		    1&0\\-1&2
		    \end{pmatrix}$.
		    On a $A= \begin{pmatrix}
		    1&0\\0&2
		    \end{pmatrix}+\begin{pmatrix}
		    0&0\\-1&0
		    \end{pmatrix}$.
		    $\begin{pmatrix}
		    1&0\\0&2
		    \end{pmatrix}\in F$ et $\begin{pmatrix}
		    0&0\\-1&0
		    \end{pmatrix}\in F^{\perp}$ car $\forall (a,b,d)\in\mathbb{R}^3$, $\left(\begin{pmatrix}
		    0&0\\-1&0
		    \end{pmatrix}|
		    \begin{pmatrix}
		    a&b\\0&d
		    \end{pmatrix}\right)=0$.
		    On en déduit que le projeté orthogonal, noté $p_F(A)$, de $A$ sur $F$ est la matrice $\begin{pmatrix}
		    1&0\\0&2
		    \end{pmatrix}$.
		    Ainsi, $d(A,F)=||A-p_F(A)||=||\begin{pmatrix}
		    0&0\\-1&0
		    \end{pmatrix}||=1.$





!!! exercice "Corrigé <a name="corrige-35"></a><a href="#enonce-35">35</a>"


		1.  $\left\langle\:,\right\rangle$ est linéaire par rapport à sa première variable par linéarité de la trace, de la transposition et par distributivité de la multiplication par rapport à l’addition dans $E$.
		    De plus, une matrice et sa transposée ayant la même trace, on a :
		    $\forall(A,B)\in E^2$,$\left\langle A\:,B\right\rangle=
		    \mathrm{tr}({}^t\! AB)=\mathrm{tr}({}^t\!\left( {}^t\! AB\right) )
		    =\mathrm{tr}({}^t\! BA)=\left\langle B\:,A\right\rangle$.
		    Donc $\left\langle\:,\right\rangle$ est symétrique.
		    On en déduit que $\left\langle\:,\right\rangle$ est bilinéaire et symétrique.(1)
		    Soit $A=\left( A_{i,j}\right)_{ _{1\leqslant i,j \leqslant n}}\in E$.
		    $\left\langle A\:,A\right\rangle
		    =\mathrm{tr}({}^t\! AA)=\displaystyle\displaystyle\sum\limits_{i=1}^{n}({}^t\!AA)_{i,i}
		     =\displaystyle\displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{k=1}^{n}({}^t\!A)_{i,k}A_{k,i}
		     =\displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{k=1}^{n}A_{k,i}^2$ donc $\left\langle A\:,A\right\rangle\geqslant 0$.

		    Donc $\left\langle\:,\right\rangle$ est positive. (2)
		    Soit $A=\left( A_{i,j}\right)_{ _{1\leqslant i,j \leqslant n}}\in E$ telle que $\left\langle A\:,A\right\rangle =0$.
		    Alors $\displaystyle\sum\limits_{i=1}^{n}\sum\limits_{k=1}^{n}A_{k,i}^2=0$. Or, $\forall i\in \llbracket 1,n \rrbracket$, $\forall k\in \llbracket 1,n \rrbracket$, $A_{k,i}^2\geqslant 0$.
		    Donc $\forall i\in \llbracket 1,n \rrbracket$, $\forall k\in \llbracket 1,n \rrbracket$, $A_{k,i}= 0$. Donc $A=0$.
		    Donc $\left\langle\:,\right\rangle$ est définie.(3)
		    D’après (1),(2) et (3), $\left\langle\:,\right\rangle$ est un produit scalaire sur $E$.

		    **Remarque importante**: Soit $(A,B)\in E^2$.
		    On pose $A=\left( A_{i,j}\right)_{ _{1\leqslant i,j \leqslant n}}$ et $B=\left( B_{i,j}\right)_{ _{1\leqslant i,j \leqslant n}}$.
		    Alors $\left\langle A\:,B\right\rangle=
		    \mathrm{tr}({}^t\! AB)
		    =\displaystyle\sum\limits_{i=1}^{n}({}^t\! AB)_{i,i}
		    =\displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{k=1}^{n}\left({}^t\!A\right)_{i,k}B_{k,i}
		    =\displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{k=1}^{n}A_{k,i}B_{k,i}$ .
		    Donc $\left\langle\:,\right\rangle$ est le produit scalaire canonique sur $E$.

		2.  1.  Soit $M\in S_n(\mathbb{R}) \cap A_n(\mathbb{R})$.
		        alors ${}^t\!M=M$ et ${}^t\!M=-M$ donc $M=-M$ et $M=0$.
		        Donc $S_n(\mathbb{R}) \cap A_n(\mathbb{R})=\{0\}$.(1)
		        Soit $M\in E$.
		        Posons $S=\dfrac{M+{}^t\!M}{2}$ et $A=\dfrac{M-{}^t\!M}{2}$.
		        On a $M=S+A$.
		        ${}^t\:S={}^t\!\left( \dfrac{M+{}^t\!M}{2}\right)
		         =\dfrac{1}{2}\left({}^t\!M+{}^t({}^t\!M) \right)
		          =\dfrac{1}{2}\left({}^t\!M +M\right)=S$, donc $S\in S_n(\mathbb{R})$.
		        ${}^t\:A={}^t\!\left( \dfrac{M-{}^t\!M}{2}\right)
		         =\dfrac{1}{2}\left({}^t\!M-{}^t({}^t\!M) \right)
		          =\dfrac{1}{2}\left({}^t\!M -M\right)=-A$, donc $A\in A_n(\mathbb{R})$.
		        On en déduit que $E=S_n(\mathbb{R}) + A_n(\mathbb{R})$.(2)
		        D’après (1) et (2), $E=S_n(\mathbb{R}) \oplus A_n(\mathbb{R}).$

		        **Remarque**: on pouvait également procéder par analyse et synthèse pour prouver que $E=S_n(\mathbb{R}) \oplus A_n(\mathbb{R}).$

		    2.  Prouvons que $S_n(\mathbb{R})\subset A_n(\mathbb{R})^\perp$.
		        Soit $S\in S_n(\mathbb{R})$.
		        Prouvons que $\forall \:A\in A_n(\mathbb{R})$, $\left\langle S\:,A\right\rangle=0$.
		        Soit $A\in A_n(\mathbb{R})$.
		        $\left\langle S\:,A\right\rangle=
		        \mathrm{tr}({}^t\! SA)
		        =\mathrm{tr}(SA)
		        =\mathrm{tr}(AS)
		        =\mathrm{tr}(-{}^t\! AS)
		        =-\mathrm{tr}({}^t\! AS)
		        =-\left\langle A\:,S\right\rangle
		        =-\left\langle S\:,A\right\rangle$.
		        Donc $2\left\langle S\:,A\right\rangle=0$ soit $\left\langle S\:,A\right\rangle=0$.
		        On en déduit que $S_n(\mathbb{R})\subset A_n(\mathbb{R})^\perp$ (1)
		        De plus, $\text{dim} \:A_n(\mathbb{R})^\perp=n^2-\text{dim}\:A_n(\mathbb{R})$.
		        Or, d’après 2.(a), $E=S_n(\mathbb{R}) \oplus A_n(\mathbb{R})$ donc $\text{dim}\:S_n(\mathbb{R})=n^2 -\text{dim}\:A_n(\mathbb{R})$.
		        On en déduit que $\text{dim} \:S_n(\mathbb{R})=\text{dim}\:A_n(\mathbb{R})^\perp$.(2)
		        D’après (1) et (2), $S_n(\mathbb{R})=A_n(\mathbb{R})^\perp$.

		3.  On introduit la base canonique de $\mathcal{M}_n(\mathbb{R})$ en posant:
		    $\forall i\in\llbracket 1,n \rrbracket$, $\forall j\in\llbracket 1,n \rrbracket$, $E_{i,j}=\left( e_{k,l}\right)_{ _{1\leqslant k,l \leqslant n}}$ avec $e_{k,l}=\left\lbrace \begin{array}{ll}
		    1&\:\text{si}\: k=i\:\text{et} \:l=j\\
		    0&\:\text{sinon}
		    \end{array}
		    \right.$
		    On a alors $F=\text{Vect}\left( E_{1,1},E_{2,2},...,E_{n,n}\right)$.
		    Soit $M=\left( m_{i,j}\right)_{ _{1\leqslant i,j \leqslant n}}\in E$.
		    Alors, en utilisant la remarque importante de la question 1.,
		    $M\in F^{\perp} \Longleftrightarrow \forall i \in \llbracket 1,n\rrbracket,\: \left\langle M\:,E_{i,i}\right\rangle=0
		    \Longleftrightarrow \forall i \in \llbracket 1,n\rrbracket,\: m_{i,i}=0$.
		    Donc $F^\perp=\text{Vect}\left( E_{i,j}\:\text{telles que}\:(i,j)\in {\llbracket 1,n\rrbracket}^2\:\text{et}\:i\neq j\right)$.
		    En d’autres termes, $F^\perp$ est l’ensemble des matrices comprenant des zéros sur la diagonale.





!!! exerciceb "Corrigé <a name="corrige-36"></a><a href="#enonce-36">36</a>"

		D'après le cours, $p(x)=<x|u>u$.

		Un vecteur orthogonal à ce plan est $(1,-2,1)$ de norme $\sqrt 6$ : on pose $u=\frac 1{\sqrt 6}(1,-2,1)$.   Notons $p$ la projection orthogonale sur $u$. Alors $q=id-p$. Or : $p(e_1)=<e_1|u>u=\frac1{\sqrt 6}u$, $p(e_2)=<e_2|u>u=-\frac2{\sqrt 6}u$ et $p(e_3)=<e_3|u>u=\frac1{\sqrt 6}u$. On a donc $mat(p)=\frac 1{6}\begin{pmatrix}1&-2&1\\-2&4&-2\\1&-2&1\end{pmatrix}$. On en déduit $mat(q)=I_3-mat(p)=\frac 16\begin{pmatrix}
		5 & 2 & -1\\2 & 2 & 2 \\-1 & 2 & 5
		\end{pmatrix}$






!!! exercice "Corrigé <a name="corrige-37"></a><a href="#enonce-37">37</a>"
		Posons $\displaystyle\varphi(x)=x+\sum_{i=1}^n(x|e_i)u_i$. $\varphi$ est évidemment une application linéaire de $E$ dans $E$. On remarque que pour $k\in\{1,\dots,n\}$, $\varphi(e_k)=e_k+u_k$. Ainsi la famille des $e_k+u_k$ est une base si et seulement si $\varphi$ est injective.

		Soit $x\in\ker\varphi$. On a alors $x=-\displaystyle\sum_{i=1}^n(x|e_i)u_i$. On a alors :

		\begin{align*}
		||x|| &\le \sum_{i=1}^n\big|(x|e_i)\big|||u_i||\\
		&\le \frac 1n\sum_{i=1}^n\big|(x|e_i)\big|\times 1\\
		&\le \frac 1n||x||\sqrt n\\
		&\le \frac{||x||}{\sqrt n}
		\end{align*}

		Or $n>1$ a priori donc $||x||=0$. On en déduit que $\varphi$ est injective, c'est un endomorphisme donc un automorphisme : l'image d'une base est une base, donc $(e_1+u_1,\dots,e_n+u_n)$ est une base.








!!! exerciceb "Corrigé <a name="corrige-38"></a><a href="#enonce-38">38</a>"

		la fonction $x\mapsto \frac 1{\sqrt x}$ est décroissante. On a donc de façon assez classique $\int_{k}^{k+1}\frac 1{\sqrt x}dx\le \frac 1{\sqrt k}\le \int_{k-1}^k\frac 1{\sqrt x}dx$. En sommant de $n+1$ à $2n$, on obtient :
		$$
		\int_{n+1}^{2n}\frac 1{\sqrt x}dx\le \sum_{k=n+1}^{2n}\frac 1{\sqrt k}\le \int_{n}^{2n-1}\frac 1{\sqrt x}dx
		$$

		Or $\int_{n+1}^{2n}\frac 1{\sqrt x}dx=2(\sqrt{2n}-\sqrt{n+1})=2\sqrt n(\sqrt 2-\sqrt{1+\frac 1n})\sim2\sqrt n(\sqrt 2-1)$ et $\int_{n}^{2n-1}\frac 1{\sqrt x}dx=2(\sqrt{2n-1}-\sqrt n)=2\sqrt n(\sqrt{2-\frac 1n}-1)\sim2\sqrt n(\sqrt 2-1)$. Par conservation de l'équivalent par encadrement, $\sum_{k=n+1}^{2n}\frac 1{\sqrt k}\sim 2\sqrt n(\sqrt 2-1)$.

		par les sommes de Riemann, on a :

		$$
		\begin{align*}
		\sum_{k=n+1}^{2n}\frac 1{\sqrt k}&=\sum_{k=1}^n\frac1{\sqrt{k+n}}\\
		&=\frac 1{\sqrt n}\sum_{k=1}^n\frac 1{\sqrt{1+\frac kn} }\\
		&=\sqrt n\frac 1n\sum_{k=1}^n\frac 1{\sqrt{1+\frac kn} }\\
		\end{align*}
		$$

		Or par somme de Riemann, $\frac 1n\sum_{k=1}^n\frac 1{\sqrt{1+\frac kn} }\to \int_0^1\frac1{\sqrt{1+x}}dx=2(\sqrt 2-1)$. On en déduit que $\sum_{k=n+1}^{2n}\frac 1{\sqrt k}\sim2\sqrt n(\sqrt 2-1)$.






!!! exerciceb "Corrigé <a name="corrige-39"></a><a href="#enonce-39">39</a>"

		 Posons $u=\tan(x)$, on a $du = (1+u^2)dx$. Appliquons ce changement de variable :

		$$
		\begin{align*}
		I_n&=\int_{0}^{\frac\pi4}(\tan x)^n dx\\
		&=\int_{0}^1 \frac{u^n}{1+u^2}du\\
		&=\left[\frac1{n+1}\frac{u^{n+1}}{1+u^2}\right]_0^1+\frac 2{n+1}\int_0^1\frac{u^{n+2}}{(1+u^2)^2}du
		\end{align*}
		$$

		Or $\int_0^1\frac{u^{n+2}}{(1+u^2)^2}du\le\int_0^1\frac{1}{(1+u^2)^2}du$ ce qui assure la convergence de tous les termes vers $0$.

		Ainsi $I_n$ converge vers $0$.

		On essaye d'obtenir une raltion de récurrence sur $I_n$ :

		$$
		\begin{align*}
		I_n+I_{n+2} &= \int_0^{\frac\pi4}(\tan x)^n(1+\tan^2(x))dx\\
		&=\left[\frac 1{n+1}(\tan x)^{n+1}\right]_0^{\frac\pi4}\\
		&=\frac 1{n+1}
		\end{align*}
		$$

		On doit maintenant utiliser la question précédente. On peut remarquer que $\frac 1{2n+1}=I_{2n}+I_{2n+2}$ et donc $\sum_{n=0}^N \frac{(-1)^n}{2n+1}=\sum_{n=0}^N(-1)^n(I_{2n}+I_{2n+2})=I_0+I_2-I_2-I_4+I_4+I_6-\dots +(-1)^NI_2N+(-1)^NI_{2N+2}=I_0+(-1)^NI_{2N+2}$. Par ailleurs $I_{2N+2}\to 0$ donc $\sum_{n=0}^{+\infty}\frac{(-1)^n}{2n+1}=I_0=\frac\pi4$.

		Enfin, $I_n$ est positive, décroissante et converge vers $0$. D'après le CSSA, $\sum(-1)^nI_n$ converge. Or on remarque $\sum(-1)^nI_n=I_0-I_1+I_2-I_3+I_4-...=(I_0+I_2)+(I_4+I_6)+\dots -(I_1+I_3)-(I_5+I_7)-\dots$ et donc $2\sum_{n=0}^{+\infty}(-1)^nI_n=I_0-I_1+\sum_{k=1}^{+\infty}\frac{(-1)^k}{k}=\frac\pi4-\frac 12\ln(2)+\ln(2)=\frac \pi4+\frac 12\ln(2)$. D'où $\sum_{n=0}^{+\infty} (-1)^nI_n=\frac\pi8+\frac 12\ln(2)$.




!!! exercice "Corrigé <a name="corrige-40"></a><a href="#enonce-40">40</a>"


		1.  Soit $z$ un complexe non nul. Posons $z=x+\mathrm{i}y$ avec $x$ et $y$ réels.
		    Un argument de $z$ est un réel $\theta$ tel que $\frac{z}{|z|} =\mathrm{e}^{\mathrm{i}\theta}$ avec $|z|=\sqrt{x^{2}+y^{2}}$.

		2.  $z=0$ n’est pas solution de l’équation $z^n=1$.
		    Les complexes solutions s’écriront donc sous la forme $z=r\mathrm{e}^{\mathrm{i}\theta}$ avec $r>0$ et $\theta\in\mathbb{R}$.
		    On a $z^n=1 \: \Longleftrightarrow \:\left\lbrace \begin{array}{c} r^{n}=1 \\
		     \text{et}\\
		      n\theta=0\mod2\pi
		      \end{array}
		      \right.$ $\: \Longleftrightarrow \:\left\lbrace \begin{array}{c} r=1 \\
		     \text{et}\\
		      \theta=\dfrac{2k\pi}{n}\:\text{avec}\:k\in{\mathbb{Z}}
		      \end{array}
		      \right.$
		    Les réels $\dfrac{2k\pi}{n}$, pour $k\in\llbracket0,n-1\rrbracket$, sont deux à deux distincts et $\forall\:k\in\llbracket0,n-1\rrbracket$, $\dfrac{2k\pi}{n} \in\left[0,2\pi \right[$.
		    Or $\begin{array}{lll}
		     \left[0,2\pi \right[&\longrightarrow &\mathbb{C}\\
		     \theta&\longmapsto&\mathrm{e}^{\mathrm{i}\theta}
		     \end{array}$ est injective.
		    Donc, $\left\lbrace  \mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\:\text{avec}\: k\in{\llbracket 0,n-1\rrbracket}\right\rbrace$ est constitué de $n$ solutions distinctes de l’équation $z^n=1$.
		    Les solutions de l’équation $z^n=1$ étant également racines du polynôme $X^n-1$, il ne peut y en avoir d’autres.
		    Finalement, l’ensemble des solutions de l’équation $z^n=1$ est $S=\left\lbrace  \mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\:\text{avec}\: k\in{\llbracket 0,n-1\rrbracket}\right\rbrace$.

		3.  $z=i$ n’étant pas solution de l’équation $\left( z+\mathrm{i}\right)^{n}=\left(z-\mathrm{i}\right)^{n}$,
		    $\begin{array}{lll}
		         \left( z+\mathrm{i}\right)^{n}=\left(z-\mathrm{i}\right)^{n} &  \Longleftrightarrow & \left( \dfrac{z+\mathrm{i}}{z-\mathrm{i}}\right) ^n=1  \\\\
		         &  \Longleftrightarrow &\exists\: k \in{\llbracket 0,n-1\rrbracket} \:\text{tel que}\:\dfrac{z+\mathrm{i}}{z-\mathrm{i}}=\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}} \:\: \\\\
		         &\Longleftrightarrow&\exists\: k \in{\llbracket 0,n-1\rrbracket} \:\text{tel que}\: z\left( 1-\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\right)=-\mathrm{i}\left(1+\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\right) \\\\
		      \end{array}$
		    En remarquant que $z\left( 1-\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\right)=-\mathrm{i}\left(1+\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\right)$ n’admet pas de solution pour $k=0$, on en déduit que:
		    $\begin{array}{lll}
		         \left( z+\mathrm{i}\right)^{n}=\left(z-\mathrm{i}\right)^{n} &  \Longleftrightarrow &
		        \exists\: k \in{\llbracket 1,n-1\rrbracket} \:\text{tel que}\:  z=\mathrm{i}\:\dfrac{\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}+1}{\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}-1}  
		      \end{array}$
		    En écrivant $\mathrm{i}\:\dfrac{\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}+1}{\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}-1}=\mathrm{i}\:\dfrac{\mathrm{e}^{\dfrac{\mathrm{i}k\pi}{n}}+\mathrm{e}^{-\dfrac{\mathrm{i}k\pi}{n}}}{\mathrm{e}^{\dfrac{\mathrm{i}k\pi}{n}}-\mathrm{e}^{-\dfrac{\mathrm{i}k\pi}{n}}}$ $= \mathrm{i}\dfrac{2\cos\left(\dfrac{k\pi}{n}\right)}{2\mathrm{i}\sin\left(\dfrac{k\pi}{n}\right)}= \dfrac{\cos\left(\dfrac{k\pi}{n}\right)}{\sin\left(\dfrac{k\pi}{n}\right)}$ , on voit que les solutions sont des réels.
		    On pouvait aussi voir que si $z$ est solution de l’équation $\left( z+\mathrm{i}\right)^{n}=\left(z-\mathrm{i}\right)^{n}$ alors $|z+\mathrm{i}|=|z-\mathrm{i}|$ et donc le point d’affixe $z$ appartient à la médiatrice de $\left[A,B\right]$, $A$ et $B$ étant les points d’affixes respectives $\mathrm{i}$ et $-\mathrm{i}$, c’est-à-dire à la droite des réels.





!!! exerciceb "Corrigé <a name="corrige-41"></a><a href="#enonce-41">41</a>"


		1.  $z^k-1 = e^{\mathrm{i}\,\dfrac{k2\pi}{n}} - 1
		       = \text{e}^{\mathrm{i}\,\dfrac{k\pi}{n}}\left(\text{e}^{\mathrm{i}\,\dfrac{k\pi}{n}} - \text{e}^{-\mathrm{i}\dfrac{k\pi}{n}}\right)
		       = \text{e}^{\mathrm{i}\dfrac{k\pi}{n}}2\mathrm{i} \sin\left(\frac{k\pi}{n}\right)$
		    c’est-à-dire $z^k-1 = 2 \sin\left(\dfrac{k\pi}{n}\right)\;\text{e}^{\mathrm{i}(\dfrac{k\pi}{n}+ \dfrac{\pi}{2})}$

		    Pour $k\in{\llbracket 1,n-1 \rrbracket}$, on a $0 < \frac{k\,\pi}{n} < \pi$, donc $\sin\left(\dfrac{k\,\pi}{n}\right) > 0$.
		    Donc le module de $z^k - 1$ est $\,2\: \sin\left(\dfrac{k\,\pi}{n}\right)$ et un argument de $\,z^k - 1$ est $\,\dfrac{k\pi}{n}+ \dfrac{\pi}{2}$.

		2.  On remarque que pour $k=0$, $|z^k-1|=0$ et $\sin\left(\dfrac{k\,\pi}{n}\right)=0$.
		    Donc d’après la question précédente, on a $S = 2\:\displaystyle\sum\limits_{k=0}^{n-1} \;\sin\left(\dfrac{k\,\pi}{n}\right).$
		    $S$ est donc la partie imaginaire de $\:T = 2 \displaystyle\sum\limits_{k=0}^{n-1} e^{\mathrm{i}\,\dfrac{k\pi}{n}}$.
		    Or, comme $\mathrm{e}^{\mathrm{i}\dfrac{\pi}{n}}\neq 1$, on a $T   =  2\dfrac{1 - \text{e}^{\mathrm{i}\pi}}{1 -
		    e^{\mathrm{i}\dfrac{\pi}{n}}}=\dfrac{4}{1 -
		    e^{\mathrm{i}\dfrac{\pi}{n}}}$.
		    Or $1-\mathrm{e} ^{\mathrm{i}\dfrac{\pi}{n}}
		    =\mathrm{e}^{\mathrm{i}\dfrac{\pi}{2n}}\left( \mathrm{e}^{-\mathrm{i}\dfrac{\pi}{2n}}-\mathrm{e}^{\mathrm{i}\dfrac{\pi}{2n}}\right)
		    =-2\mathrm{i}\mathrm{e}^{\mathrm{i}\dfrac{\pi}{2n}}\sin\left( \dfrac{\pi}{2n}\right)$.
		    On en déduit que $T=   \dfrac{4\text{e}^{-\mathrm{i}\dfrac{\pi}{2n}}}{-2\mathrm{i}\,\sin \dfrac{\pi}{2n}}
		      =   \dfrac{2}{\sin \dfrac{\pi}{2n}}\;\mathrm{i}\;\text{e}^{-\mathrm{i}\dfrac{\pi}{2n}}$.
		    En isolant la partie imaginaire de $T$, et comme $\cos\left( \dfrac{\pi}{2n}\right)\neq 0$ ($n\geqslant 2$), on en déduit que $S = \dfrac{2}{\tan\dfrac{\pi}{2n}}$.





!!! exerciceb "Corrigé <a name="corrige-42"></a><a href="#enonce-42">42</a>"

		pour l'équation $z^n=e^{i\pi/3}$ on doit se ramener à une équation de la forme $Z^n=1$. On pose donc ici $Z=\frac z{e^{i\pi/3n}}$ et on a donc $Z\in\mathbb U_n$. Les solutions sont donc $Z=e^{2ik\pi/n}$ et $z=e^{2ik\pi/n+i\pi/(3n)}=e^{i\pi\frac{1+6k}{3n}}$.

		Pour la seconde équation, on note que $z e1$ et $z \ne-1$, on pose $Z=(\frac{z-1}{z+1})^n$ et l'équation est équivalente à $Z^2-Z+1=0$ dont les racines sont $\frac{1\pm i\sqrt3}{2}=e^{\pm i\pi/3}$. On en déduit que $\frac{z-1}{z+1}=e^{i\pi\frac{\pm1+6k}{3n}}$.

		Or si $\frac{z-1}{z+1}=e^{i\theta}$, alors $z=\frac{e^{i\theta}+1}{1-e^{i\theta}}=i\frac{2\cos\frac\theta2}{2\sin\frac\theta2}=\frac i{\tan\frac\theta2}$.

		Dans le cas qui nous intéresse, on a donc les solutions : $\frac i{\tan\frac{\pm1+6k}{6n}\pi}$ pour $k\in\{0,\dots,n-1\}$.







!!! exercice "Corrigé <a name="corrige-43"></a><a href="#enonce-43">43</a>"

		La première question est triviale.

		On a $A_{n+1}+iB_{n+1}=(3+4i)(A_n+iB_n)=(3A_n-4B_n)+i(3B_n+4A_n)$.

		On montre facilement par récurrence grâce à la relation précédente que $A_n$ et $B_n$ sont des entiers.

		On montre les histoires de restes par récurrence aussi.

		La dernière question est plus intéressante : le complexe $(\frac {3+4i}5)^n$ est dans $\mathbb U$. Montrons que pour tout entier $n$, $(\frac {3+4i}5)^n \ne1$. On s'interesse pour cela à la valeur de $B_n$ : $B_n$ est un entier dont le reste modulo $5$ est $4$ : $B_n$ ne peut donc pas être nul. Or $(\frac {3+4i}5)^n=\frac {A_n}{5^n}+i\frac{B_n}{5^n}$ donc $(\frac {3+4i}5)^n \ne1$.






!!! exercice "Corrigé <a name="corrige-44"></a><a href="#enonce-44">44</a>"

		On passe en complexes en remplaçant $\cos(kx)$ par $e^{ikx}$.

		$$
		\begin{align*}
		\sum_{k=0}^n \binom nke^{ikx}&=(1+e^{ix})^n\\
		&=e^{i\frac n2x}(2\cos(\frac x2))^n\\
		\end{align*}
		$$

		Et ainsi $\sum_{k=0}^n \binom nk \cos(kx)=2^n\cos(\frac{nx}2)\cos^n(\frac x2)$.






!!! exercice "Corrigé <a name="corrige-45"></a><a href="#enonce-45">45</a>"


		1.  1.  $P(X)=\displaystyle\sum\limits_{k=0}^{n}\dfrac{P^{(k)}(a)}{k!}(X-a)^k$.

				2.  $$\begin{aligned}
				a\ \text{est une racine d'ordre }r\ \text{de }P &\Longleftrightarrow
				&\exists Q\in \mathbb{R}_{n-r}[X]\:\:\text{tel que}\: \:Q\left( a\right) \neq 0\ \text{et\
				\ }P=\left( X-a\right) ^{r}Q \\
				&\Longleftrightarrow &\exists \left( q_{0},\ldots ,q_{n-r}\right) \in
				\mathbb{R}^{n-r+1} \:\:\text{tel que}\:\: q_{0}\neq 0\ \text{et}\ P=\left( X-a\right)
				^{r}\displaystyle\sum\limits_{i=0}^{n-r}q_{i}\left( X-a\right) ^{i} \\
				&\Longleftrightarrow &\exists \left( q_{0},\ldots ,q_{n-r}\right) \in
				\mathbb{R}^{n-r+1}\:\text{tel que}\: \: q_{0}\neq 0\ \ \text{et\ \ }P=\displaystyle\sum%
				\limits_{i=0}^{n-r}q_{i}\left( X-a\right) ^{r+i} \\
				&\Longleftrightarrow &\exists \left( q_{0},\ldots ,q_{n-r}\right) \in
				\mathbb{R}^{n-r+1} \:\text{tel que}\: \: q_{0}\neq 0\ \ \ \text{et\ \ }P=\displaystyle\sum%
				\limits_{k=r}^{n}q_{k-r}\left( X-a\right) ^{k}\end{aligned}$$

				D’après la formule de Taylor (rappelée ci-dessus) et l’unicité de la décomposition de $P$ dans la base $\left( 1,\left( X-a\right)
				,\ldots ,\left( X-a\right) ^{n}\right)$ de $\mathbb{R}_{n}[X]$ il vient enfin :  

				$$
				a\ \text{est une racine d'ordre }r\ \text{de }P\Longleftrightarrow \forall
				k\in \left\{ 0,\ldots ,r-1\right\} \ \ P^{\left( k\right) }\left( a\right)
				=0\ \text{et\ \ }P^{\left( r\right) }\left( a\right) \neq 0\ $$

		2.  D’après la question précédente,

		$$
		\begin{aligned}
		1\ \text{est racine double de }P =X^{5}+aX^{2}+bX&\Longleftrightarrow&
		P\left( 1\right) =P^{\prime }\left( 1\right) =0\ \text{et }P^{\prime \prime
		}\left( 1\right) \neq 0 \\
		&\Longleftrightarrow &\left\{
		\begin{array}{c}
		1+a+b=0 \\
		5+2a+b=0 \\
		20+2a\neq 0%
		\end{array}%
		\right. \\
		&\Longleftrightarrow &a=-4\ \text{et }b=3\end{aligned}
		$$

		On obtient $X^{5}-4X^{2}+3X=X(X-1)^{2}(X^{2}+2X+3)$ et c’est la factorisation cherchée car le discriminant de $X^{2}+2X+3$ est strictement négatif.





!!! exercice "Corrigé <a name="corrige-46"></a><a href="#enonce-46">46</a>"


		1.  L’application $u$:$\begin{array} {lll}
		    \mathbb{R}_{n}[X]&\rightarrow &\mathbb{R}^{n+1}\\
		    P& \mapsto & \left( P\left( a_{0}\right) ,P\left( a_{1}\right) ,\cdots
		    ,P\left( a_{n}\right) \right)
		    \end{array}$ est linéaire.
		    Montrons que $\mathrm{Ker}
		    u=\left\{ 0\right\} .$

		    Si $P\in \mathrm{Ker} u$, alors $P\left( a_{0}\right) =P\left( a_{1}\right) =\cdots
		    =P\left( a_{n}\right) =0$ et le polynôme $P$, de degré inférieur ou égal à $n$, admet $n+1$ racines distinctes.
		    Donc $P=0$.
		    Ainsi $u$ est injective et comme $\dim \mathbb{R}_{n}[X]=n+1=\dim \mathbb{R}^{n+1}\ ,$ $u$ est un isomorphisme d'espaces vectoriels.

		    Enfin les conditions recherchées sont équivalentes à : $P\in
		    \mathbb{R}_{n}[X]$ et $u\left( P\right) =\left( b_{0},\ldots ,b_{n}\right)
		    .$

		    La bijectivité de $u$ dit que ce problème admet une unique solution $P$ et on a $P=u^{-1}\left(\left( b_{0},\ldots ,b_{n}\right)\right)$.

		2.  Pour ce choix de  $b_{0},b_{1},\cdots ,b_{n}$ le polynôme $L_{k}$ vérifie les conditions :

		    $$\deg L_{k}\leqslant n \:\:\text{et}\: \: \forall i\in \llbracket0,n\rrbracket, \:
		     L_k(a_i)=\left\lbrace
		    \begin{array}{lll}
		    0&\text{si}&i\neq k\\
		    1&\text{si}&i=k
		    \end{array}
		    \right.$$

		    Comme $a_{0},\ldots ,a_{k-1},a_{k+1},\ldots ,a_{n}$ sont $n$ racines distinctes de $L_{k}$ qui est de degré $\leqslant n$, il existe né cessairement $\lambda \in \mathbb{K}$ tel que $$L_{k}=\lambda \prod\limits_{\underset{i\neq k}{i=0}}^{n}\left( X-a_{i}\right)$$

		    La condition supplémentaire $L_{k}\left( a_{k}\right) =1$ donne $\lambda
		    =\dfrac{1}{\displaystyle\prod\limits_{\underset{i\neq k}{i=0}}^{n}\left( a_{k}-a_{i}\right) }$ et finalement : $$L_{k}=\prod\limits_{\underset{i\neq k}{i=0}}^{n}\frac{X-a_{i}}{a_{k}-a_{i}}$$

		3.  Soit $p\in \llbracket 0,n\rrbracket$.
		    Les polynômes $\displaystyle\sum
		    \limits_{k=0}^{n}a_{k}^{p}L_{k}$ et $X^{p}$ vérifient les mêmes conditions d’interpolation : $$\deg P\leqslant n\ \ \text{et}\ \ \ \forall i\in \{0,\cdots
		    ,n\}\;\;\;P\left( a_{i}\right) =a_{i}^{p}$$

		    Par l’unicité vue en première question, on a $\displaystyle\sum\limits_{k=0}^{n}a_{k}^{p}L_{k}=X^{p}$.




!!! exerciceb "Corrigé <a name="corrige-47"></a><a href="#enonce-47">47</a>"

		Les racines de $P$ sont les $w-1$ avec $w\in\mathbb U_n$. Or $e^{2ik\pi/n}-1=2ie^{ik\pi/n}\sin(k\pi n)$.

		Or $P=(X+1)^n-1=\sum_{k=0}^n\binom nk X^k-1=X\sum_{k=1}^n\binom nk X^{k-1}$. Les racines de second membre sont donc les $2ie^{ik\pi/n}\sin(k\pi/n)$ pour $k\in\{1\dots n-1\}$ : le produit des racines se lit sur le coefficient de degré $0$ avec un coefficient multiplicatif de $(-1)^{n-1}$ : $(-1)^{n-1}n=\prod_{k=1}^{n-1}2ie^{ik\pi/n}\sin(k\pi/n)=(2i)^{n-1}\prod_{k=1}^{n-1}\sin(k\pi/n) e^{i\sum_{k=1}^{n-1}k\pi/n}=(2i)^{n-1}\prod_{k=1}^{n-1}\sin(k\pi/n) e^{in(n-1)\pi/2n}=(2i)^{n-1}\prod_{k=1}^{n-1}\sin(k\pi/n) e^{i(n-1)\pi/2}=(-2)^{n-1}\prod_{k=1}^{n-1}\sin(k\pi/n)$.

		On en déduit : $\prod_{k=1}^{n-1}\sin(k\pi/n)=\frac{n}{2^{n-1}}$.








!!! exercice "Corrigé <a name="corrige-48"></a><a href="#enonce-48">48</a>"
		Tout d'abord on a évdemment les relations : $(i)\Rightarrow(ii)$ et $(i)\Rightarrow (iii)$ ainsi que $(ii)\Rightarrow (iii)$. Nous allons montrer que $(ii)\Rightarrow (i)$ et que $(iii)\Rightarrow (i)$.
		Commençons par faire une remarque plutôt simple à démontrer : on a pour tout $p\in\mathbb Z$, $H_k(p)\in \mathbb Z$ avec en particulier $H_k(k)=1$. De plus, $(1,H_1,\dots,H_n)$ étant une famille de $n+1$ éléments échelonnés de $\mathbb Q_n[X]$, cette famille est une base de $\mathbb Q_n[X]$.

		Supposons $(ii)$ vraie, alors en écrivant $P=q_0H_0+q_1H_1+\dots q_nH_n$, on obtient le système :
		$$\begin{cases}
		P(0)&=q_0\\
		P(1)&=q_0H_0(1)+q_1H_1(1)\\
		\vdots\\
		P(n)&=q_0H_0(n)+q_1H_1(n)+\dots+q_nH_n(n)
		\end{cases}$$
		système qui nous permet d'assurer que les $q_i$ sont des entiers.

		Soit maintenant un entier $q$ quelconque, alors $P(i)=q_0H_0(i)+q_1H_1(i)+\dots+q_nH_n(i)$ qui est une somme d'entier et ainsi $P(i)\in\mathbb Z$. Ainsi $(ii)\Rightarrow (i)$.

		Montrons maintenant que $(iii)\Rightarrow (i)$. Pour cela, posons $Q(X)=P(X+m)$. On a donc pour $k\in\{0,\dots n\}$, $Q(k)=P(m+k)\in\mathbb Z$. Alors d'après le cas précédent, on peut en déduire que $Q(\mathbb Z)\subset \mathbb Z$. Soit alors $k\in\mathbb Z$, $P(k)=Q(k-m)$. Or $k-m\in\mathbb Z$ donc $Q(k-m)\in\mathbb Z$. Ainsi $P(k)\in\mathbb Z$ et on en déduit que $P(\mathbb Z)\subset\mathbb Z$.  

		On a donc bien équivalence entre les trois propriétés.







!!! exerciceb "Corrigé <a name="corrige-49"></a><a href="#enonce-49">49</a>"


		1.  Soit $(\Omega,\mathcal{A},P)$ un espace probabilisé.
		    Soit $B$ un événement de probabilité non nulle et $\left( A_i\right) _{i\in I}$ un système complet d’événements de probabilités non nulles.
		    Alors, $\forall\: i_0 \in I$, $P_B(A_{i_0})=\dfrac{P(A_{i_0})P_{A_{i_0}}(B)}{\displaystyle\sum\limits_{i\in I}^{}P(A_i)P_{A_i}(B)}.$


		    **Preuve**: $P_B(A_{i_0})=\dfrac{P(A_{i_0}\cap B)}{P(B)}=\dfrac{P(A_{i_0})P_{A_{i0}}(B)}{P(B)}$.(1)
		    Or $\left( A_i\right) _{i\in I}$ un système complet d’événements donc $P(B)=\displaystyle\sum\limits_{i\in I}^{}P(A_{i}\cap B)$.
		    Donc $P(B)=\displaystyle\sum\limits_{i\in I}^{}P(A_i)P_{A_i}(B)$.(2).
		    (1) et (2) donnent le résultat souhaité.

		2.  1.  On tire au hasard un dé parmi les 100 dés.
		        Notons $T$ l’événement: «le dé choisi est pipé».
		        Notons $A$ l’événement :« On obtient le chiffre 6 lors du lancer ».
		        On demande de calculer $P_A(T)$.
		        Le système $(T,\overline{T})$ est un système complet d’événements de probabilités non nulles.
		        On a d’ailleurs, $P(T)=\dfrac{25}{100}=\dfrac{1}{4}$ et donc $P(\overline{T})=\dfrac{3}{4}$.
		        Alors, d’après la formule de Bayes, on a :
		        $P_A(T)=\dfrac{P(T)P_T(A)}{P_T(A)P(T)+P_{\overline{T}}(A)P(\overline{T})}
		         =\dfrac{\dfrac{1}{4}\times\dfrac{1}{2}}{\dfrac{1}{2}\times\dfrac{1}{4}+\dfrac{1}{6}\times\dfrac{3}{4}}=\dfrac{1}{2}.$

		    2.  Soit $n\in\mathbb{N}^*$.
		        On choisit au hasard un dé parmi les 100 dés.
		        $\forall \:k\in\llbracket1,n\rrbracket$, on note $A_k$ l’événement « on obtient le chiffre 6 au $k^{\text{ième}}$ lancer ».
		        On pose $A=\displaystyle\bigcap \limits_{k=1}^{n}A_k$.
		        On nous demande de calculer $p_n=P_A(T)$.
		        Le système $(T,\overline{T})$ est un système complet d’événements de probabilités non nulles.
		        On a d’ailleurs, $P(T)=\dfrac{25}{100}=\dfrac{1}{4}$ et donc $P(\overline{T})=\frac{3}{4}$.
		        Alors d’après la formule de Bayes, on a :

		        $p_n=P_A(T)=\dfrac{P(T)P_T(A)}{P_T(A)P(T)+P_{\overline{T}}(A)P\left( \overline{T}\right) }$
		        Donc $p_n=\dfrac{\dfrac{1}{4}\times\left( \dfrac{1}{2}\right) ^n}{\left( \dfrac{1}{2}\right) ^n\times\dfrac{1}{4}+\left(\dfrac{1}{6}\right) ^n\times\dfrac{3}{4}}=\dfrac{1}{1+\dfrac{1}{3^{n-1}}}.$

		    3.  $\forall \:n\in\mathbb{N}^*$, $p_n=\dfrac{1}{1+\dfrac{1}{3^{n-1}}}$ Donc $\lim\limits_{n\to +\infty}^{}p_n=1$.
		        Ce qui signifie que, lorsqu’on effectue un nombre élevé de lancers, si on n’obtient que des 6 sur ces lancers alors il y a de fortes chances que le dé tiré au hasard au départ soit pipé.





!!! exercice "Corrigé <a name="corrige-50"></a><a href="#enonce-50">50</a>"


		1.  Notons $U_1$ l’événement le premier tirage se fait dans l’urne $U_1$.
		    Notons $U_2$ l’événement le premier tirage se fait dans l’urne $U_2$.
		    $(U_1,U_2)$ est un système complet dévénements.
		    Donc d’après la formule des probabilités totales, $p_1=P(B_1)=P_{U_1}(B_1)P(U_1)+P_{U_2}(B_1)P(U_2)$.
		    Donc $p_1=\dfrac{2}{5}\times\dfrac{1}{2}+\dfrac{4}{7}\times\dfrac{1}{2}=\dfrac{17}{35}$
		    On a donc $p_1=\dfrac{17}{35}$.

		2.  Soit $n\in\mathbb{N}^*.$
		    $(B_n, \overline{B_n})$ est un système complet d’événements.
		    Donc, d’après la formule des probabilités totales, $P(B_{n+1})=P_{B_n}(B_{n+1})P(B_n)+P_{\overline{B_n}}(B_{n+1})P(\overline{B_n})$.
		    Alors en tenant compte des conditions de tirage, on a $p_{n+1}=\dfrac{2}{5}p_n+\dfrac{4}{7}(1-p_n)$.
		    Donc, $\forall\:n\in\mathbb{N}^*.$ $p_{n+1}=-\dfrac{6}{35}p_n+\dfrac{4}{7}$.

		3.  $\forall n\in\mathbb{N}^*$, $p_{n+1}=-\dfrac{6}{35}p_n+\dfrac{4}{7}.$
		    Donc $(p_n)_{n\in\mathbb{N}^*}$ est une suite arithmético-géométrique.
		    On résout l’équation $l=-\dfrac{6}{35}l+\dfrac{4}{7}$ et on trouve $l=\dfrac{20}{41}$.
		    On considère alors la suite $(u_n)_{n\in\mathbb{N}^*}$ définie par : $\forall \:n\in\mathbb{N}^*$ , $u_n=p_n-l$.
		    $(u_n)_{n\in\mathbb{N}^*}$ est géométrique de raison $-\dfrac{6}{35}$, donc, $\forall \:n\in\mathbb{N}^*$, $u_n=\left( -\dfrac{6}{35}\right) ^{n-1}u_1$.
		    Or $u_1=p_1-l=\dfrac{17}{35}-\dfrac{20}{41}=-\dfrac{3}{1435}.$
		    On en déduit que, $\forall \:n\in\mathbb{N}^*$, $p_n=u_n+l$, c’est-à-dire $p_n=-\dfrac{3}{1435}\left( -\dfrac{6}{35}\right) ^{n-1}+\dfrac{20}{41}.$





!!! exercice "Corrigé <a name="corrige-51"></a><a href="#enonce-51">51</a>"


		1.  On note $F=\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset B\right\rbrace$.
		Soit $p\in\llbracket 1,n\rrbracket$. On pose $F_p=\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset B\:\text{et}\: \text{card}B=p\right\rbrace$.
		Pour une partie $B$ à $p$ éléments donnée, le nombre de parties $A$ de $E$ telles que $A\subset B$ est $\text{card}\,\mathcal{P}(B)=2^p$.
		De plus, on a $\binom{n}{p}$ possibilités pour choisir une partie $B$ de $E$ à $p$ éléments.
		On en déduit que : $\forall \: p\in\llbracket 0,n\rrbracket$, $\text{card}\,F_p=\binom{n}{p}2^p$.
		Or $F=\bigcup\limits_{p=0}^{n}F_p$ avec $F_0,F_1,...,F_n$ deux à deux disjoints.
		Donc $a=\text{card}\,F=\displaystyle\sum\limits_{p=0}^{n}\text{card}\,F_p=\displaystyle\sum\limits_{p=0}^{n}\binom{n}{p}2^p=3^n$, d’après le binôme de Newton.
		Conclusion: $a=3^n$.   
		**Autre méthode:**
		Le raisonnement suivant (corrigé non détaillé) permet également de répondre à la question 1.
		Notons encore $F=\left\lbrace (A,B)\in\left( \mathcal{P}(E)\right) ^2\:/\:A\subset B\right\rbrace$.
		À tout couple $(A,B)$ de $F$, on peut associer l’application $\varphi_{A,B}$ définie par:
		$\varphi_{A,B}:\begin{array}{lll}
		E&\longrightarrow & \left\lbrace 1,2,3\right\rbrace \\
		x&\longmapsto&
		\left\lbrace \begin{array}{ccc}
		1&\:\text{si}\:&x\in A \\
		2&\:\text{si}\:& x\notin A \:\text{et}\: x \in B \\
		3&\:\text{si} &x \notin B
		\end{array}\right.
		\end{array}$
		On note $\mathcal{A}
		\left( E,\left\lbrace 1,2,3\right\rbrace \right)$ l’ensemble des applications de $E$ dans $\left\lbrace 1,2,3\right\rbrace$.
		Alors l’application $\Theta:
		\begin{array}{lll}
		{F}&\longrightarrow &\mathcal{A}
		\left( E,\left\lbrace 1,2,3\right\rbrace \right) \\
		(A,B)&\longmapsto &\varphi_{A,B}
		\end{array}$ est bijective.
		Le résultat en découle.

		2.  $\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\cap B=\emptyset \right\rbrace
		    =\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset \overline{B}\right\rbrace$.

		    $\begin{array}{lll}
		    \text{Or}\:\:\text{card}\,\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset \overline{B} \right\rbrace
		    &=&\text{card}\, \left\lbrace (A,\overline{B})\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset \overline{B}\right\rbrace \\
		    &=&\text{card}\, \left\lbrace (A,C)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset C\right\rbrace\\
		    &=&a.
		    \end{array}$

		    Donc $b =a.$

		3.  Compter tous les triplets $(A,B,C)$ tels que $A$, $B$ et $C$ soient deux à deux disjoints et tels que $A\cup B\cup C=E$ revient à compter tous les couples $(A,B)$ tels que $A\cap B=\emptyset$ car, alors, $C$ est obligatoirement égal à $\overline{A\cup B}$.
		    En d’autres termes, $c=\text{card} \,\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\cap B=\emptyset\right\rbrace=b=3^n$.




!!! exerciceb "Corrigé <a name="corrige-52"></a><a href="#enonce-52">52</a>"


		1.  1.  L’expérience est la suivante: l’épreuve "le tirage d’une boule dans l’urne" est répétée 5 fois.
		        Comme les tirages se font avec remise, ces 5 épreuves sont indépendantes.
		        Chaque épreuve n’a que deux issues possibles: le joueur tire une boule blanche (succès avec la probabilité $p=\dfrac{2}{10}=\dfrac{1}{5}$) ou le joueur tire une boule noire (échec avec la probabilité $\dfrac{4}{5}$).
		        La variable $X$ considérée représente donc le nombre de succès au cours de l’expérience et suit donc une loi binomiale de paramètre $(5,\dfrac{1}{5})$.
		        C’est-à-dire $X(\Omega)=\llbracket0,5\rrbracket$ et :  $\forall \:k\in \llbracket0,5\rrbracket$, $P(X=k)=\binom{5}{k}(\dfrac{1}{5})^k(\dfrac{4}{5})^{5-k}$.
		        Donc, d’après le cours, $E(X)=5\times\dfrac{1}{5}=1$ et $V(X)=5\times\dfrac{1}{5}\times\left( 1-\dfrac{1}{5}\right)=\dfrac{4}{5}=0,8$.

		    2.  D’après les hypothèses, on a $Y=2X-3(5-X)$, c’est-à-dire $Y=5X-15$.
		        On en déduit que $Y(\Omega)=\left\lbrace 5k-15\:\text{avec}\:k\in\llbracket0,5\rrbracket \right\rbrace$ .
		        Et on a  $\forall \:k\in \llbracket0,5\rrbracket$, $P(Y=5k-15)=
		        P(X=k)=\binom{5}{k}(\dfrac{1}{5})^k(\dfrac{4}{5})^{5-k}$.
		        $Y=5X-15$, donc $E(Y)=5E(X)-15=5-15=-10$.
		        De même, $Y=5X-15$, donc $V(Y)=25V(X)=25\times\dfrac{4}{5}=20$.

		2.  Dans cette question, le joueur tire successivement, sans remise, 5 boules dans cette urne.

		    1.  Comme les tirages se font sans remise, on peut supposer que le joueur tire les 5 boules dans l’urne en une seule fois au lieu de les tirer successivement. Cette supposition ne change pas la loi de $X$.
		        $X(\Omega)=\llbracket0,2 \rrbracket$.
		        Notons $A$ l’ensemble dont les éléments sont les 10 boules initialement dans l’urne.
		        $\Omega$ est constitué de toutes les parties à 5 éléments de $A$. Donc $\mathrm{card}\,\Omega=\dbinom{10}{5}$.
		        Soit $k\in\llbracket0,2\rrbracket$.
		        L’événement $(X=k)$ est réalisé lorsque le joueur tire $k$ boules blanches et $(5-k)$ boules noires dans l’urne. Il a donc $\dbinom{2}{k}$ possibilités pour le choix des boules blanches et $\dbinom{8}{5-k}$ possibilités pour le choix des boules noires.
		        Donc : $\forall\:k\in\llbracket0,2\rrbracket$, $P(X=k)=\dfrac{\dbinom{2}{k}\times\dbinom{8}{5-k}}{\dbinom{10}{5}}$.

		    2.  On a toujours $Y=5X-15$.
		        On en déduit que $Y(\Omega)=\left\lbrace 5k-15\:\text{avec}\:k\in\llbracket0,2\rrbracket \right\rbrace$ .
		        Et on a $\forall \:k\in \llbracket0,2\rrbracket$, $P(Y=5k-15)=
		        P(X=k)=\dfrac{\dbinom{2}{k}\times\dbinom{8}{5-k}}{\dbinom{10}{5}}$.





!!! exerciceb "Corrigé <a name="corrige-53"></a><a href="#enonce-53">53</a>"


		1.  L’expérience est la suivante: l’épreuve de l’appel téléphonique de la secrétaire vers un correspondant est répétée $n$ fois et ces $n$ épreuves sont mutuellement indépendantes.
		    De plus, chaque épreuve n’a que deux issues possibles: le correspondant est joint avec la probabilité $p$ (succès) ou le correspondant n’est pas joint avec la probabilité $1-p$ (échec).
		    La variable $X$ considérée représente le nombre de succès et suit donc une loi binômiale de paramètres $(n,p)$.
		    C’est-à-dire $X(\Omega)=\llbracket 0,n\rrbracket$ et $\forall k\in{\llbracket 0,n\rrbracket}$ $P(X=k)=\dbinom{n}{k}p^{k}(1-p)^{n-k}$.

		2.  1.  Soit $i$ $\in \llbracket 0,n \rrbracket$. Sous la condition $(X=i)$, la secrétaire rappelle $n-i$ correspondants lors de la seconde série d’appels et donc:

		        $P(Y=k|X=i)=\left\lbrace
		        \begin{array}{l}\dbinom{n-i}{k}p^{k}(1-p)^{n-i-k} \:\:\text{si}\:\: k\in \llbracket 0,n-i \rrbracket\\
		        0 \:\:\text{sinon}
		        \end{array}
		        \right.$

		    2.  $Z(\Omega)=\llbracket 0,n\rrbracket $ et $\forall k\in{\llbracket 0,n\rrbracket}$ $P(Z=k)=\displaystyle\sum\limits_{i=0}^{k}P(X=i\cap Y=k-i)=\displaystyle\sum\limits_{i=0}^{k}P(Y=k-i|X=i)P(X=i)$.  
					  Soit $k$ $\in \llbracket 0,n \rrbracket $. D'après les questions précédentes, $P(Z=k)=\displaystyle\sum\limits_{i=0}^{k}\dbinom{n-i}{k-i}\dbinom{n}{i}p^{k}(1-p)^{2n-k-i}$.  
					  Or, d'après l'indication, $\dbinom{n-i}{k-i}\dbinom{n}{i}=\dbinom{k}{i}\dbinom{n}{k}$.  
					  Donc $P(Z=k)=\displaystyle\sum\limits_{i=0}^{k}\dbinom{k}{i}\dbinom{n}{k}p^{k}(1-p)^{2n-k-i}=\dbinom{n}{k}p^{k}(1-p)^{2n-k}\displaystyle\sum\limits_{i=0}^{k}\dbinom{k}{i}\left( \dfrac{1}{1-p}\right) ^i$.  
					  Donc d'après le binôme de Newton, $P(Z=k)=\dbinom{n}{k}p^{k}(1-p)^{2n-k}\left( \dfrac{2-p}{1-p}\right) ^{k}=\dbinom{n}{k}\left( p(2-p)\right)^k\left( (1-p)^2\right)^{n-k}  $.  
					  On vérifie que $1-p(2-p)=(1-p)^2$ et donc on peut conclure que:  
					   $Z$ suit une loi binomiale de paramètre $(n,p(2-p))$.  

				3.  D'après le cours, comme $Z$ suit une loi binomiale de paramètre $(n,p(2-p))$, alors:  $E(Z)=np(2-p)$ et $V(Z)=np(2-p)\left( 1-p(2-p)\right)=np(2-p)(p-1)^{2} $.




!!! exercice "Corrigé <a name="corrige-54"></a><a href="#enonce-54">54</a>"


		1.  Soit $i\in\llbracket1,N\rrbracket$.
		    $X_i(\Omega)=\mathbb{N}^*$ et $\forall k\in{\mathbb{N}^*}$, $P(X_i=k)=p(1-p)^{k-1}=pq^{k-1}$.
		    Alors on a $P(X_i\leqslant n)=\displaystyle\sum\limits_{k=1}^{n}P(X_i=k)=\displaystyle\sum\limits_{k=1}^{n}pq^{k-1}=p\dfrac{1-q^n}{1-q}=1-q^n$.
		    Donc $P(X_i>n)=1-P(X_i\leqslant n)=q^n$.

		2.  1.  $Y(\Omega)=\mathbb{N}^*$.
		        Soit $n\in{\mathbb{N}^*}$.
		        $P(Y>n)=P((X_1>n)\cap\cdots\cap (X_N>n))$
		        Donc $P(Y>n)=\displaystyle\prod\limits_{i=1}^{N}P(X_i>n)$ car les variables $X_1,\cdots,X_N$ sont mutuellement indépendantes.
		        Donc $P(Y>n)=\displaystyle\prod\limits_{i=1}^{N}q^n=q^{nN}.$
		        Or $P(Y\leqslant n)=1-P(Y>n)$
		        donc $P(Y\leqslant n)=1-q^{nN}$.
		        Calcul de $P(Y=n)$:
		        Premier cas: si $n\geqslant 2$.
		        $P(Y=n)=P(Y\leqslant n)-P(Y\leqslant n-1)$.
		        Donc $P(Y=n)=q^{(n-1)N}(1-q^N)$.
		        Deuxième cas: si $n=1$.
		        $P(Y=n)=P(Y=1)=1-P(Y>1)=1-q^N$.
		        Conclusion: $\forall n\in{\mathbb{N}^*}$, $P(Y=n)=q^{(n-1)N}(1-q^N)$.

		    2.  D’après 2.(a), $\forall n\in{\mathbb{N}^*}$, $P(Y=n)=q^{(n-1)N}(1-q^N)$.
		        C’est-à-dire $\forall n\in{\mathbb{N}^*}$, $P(Y=n)=\left( 1-(1-q^{N})\right) ^{n-1}(1-q^N)$.
		        On en déduit que $Y$ suit une loi géométrique de paramètre $1-q^N$.
		        Donc, d’après le cours, $Y$ admet une espérance et $E(Y)=\dfrac{1}{1-q^N}$.




!!! exercice "Corrigé <a name="corrige-55"></a><a href="#enonce-55">55</a>"


		1.  $X(\Omega)=\llbracket 0,2\rrbracket$.

		2.  1.  Pour que l’événement $(X=2)$ se réalise, on a $\dbinom{3}{2}$ possibilités pour choisir les 2 compartiments restant vides. Les deux compartiments restant vides étant choisis, chacune des $n$ boules viendra se placer dans le troisième compartiment avec la probabilité $\dfrac{1}{3}$.
		        De plus les placements des différentes boules dans les trois compartiments sont indépendants.
		        Donc $P(X=2)=\dbinom{3}{2}\left( \dfrac{1}{3}\right) ^n=3\left( \dfrac{1}{3}\right) ^n=\left( \dfrac{1}{3}\right) ^{n-1}$.

		    2.  Déterminons $P(X=1)$.
		        Pour que l’événement $(X=1)$ se réalise, on a $\binom{3}{1}$ possibilités pour choisir le compartiment restant vide.
		        Le compartiment restant vide étant choisi, on note $A$ l’événement : «les $n$ boules doivent se placer dans les deux compartiments restants (que nous appellerons compartiment $a$ et compartiment $b$) sans laisser l’un d’eux vide».
		        Soit $k\in\llbracket1,n-1\rrbracket$.
		        On note $A_k$ l’événement : « $k$ boules se placent dans le compartiment $a$ et les $(n-k)$ boules restantes dans le compartiment $b$».
		        On a alors $A=\displaystyle\bigcup \limits_{k=1}^{n-1}A_k$.
		        On a $\forall\:k\in\llbracket1,n-1\rrbracket$, $P(A_k)=\dbinom{n}{k}\left( \dfrac{1}{3}\right) ^k\left( \dfrac{1}{3}\right) ^{n-k}=\dbinom{n}{k}\left( \dfrac{1}{3}\right) ^n$.
		        Donc $P(X=1) = \dbinom{3}{1}P(\displaystyle\bigcup \limits_{k=1}^{n-1}A_k)
		        =3\displaystyle\sum \limits_{k=1}^{n-1}P(A_k)$ car $A_1,A_2,...,A_{n-1}$ sont deux à deux incompatibles.
		        Donc $P(X=1)=3\displaystyle\sum \limits_{k=1}^{n-1}\dbinom{n}{k}\left( \dfrac{1}{3}\right) ^n
		        =\left( \frac{1}{3}\right) ^{n-1}\displaystyle\sum \limits_{k=1}^{n-1}\dbinom{n}{k}
		        =\left( \dfrac{1}{3}\right) ^{n-1}\left(\displaystyle \sum \limits_{k=0}^{n}\dbinom{n}{k}-2\right)
		        = \left( \dfrac{1}{3}\right) ^{n-1}\left( 2^n-2\right).$
		        Donc $P(X=1)=\left( \dfrac{1}{3}\right) ^{n-1}\left( 2^n-2\right).$
		        Enfin, $P(X=0)=1-P(X=2)-P(X=1)$ donc $P(X=0)=1-\left( \frac{1}{3}\right) ^{n-1}-\left( \frac{1}{3}\right) ^{n-1}\left( 2^n-2\right)$.
		        Donc $P(X=0)=1- \left( \dfrac{1}{3}\right) ^{n-1}\left( 2^n-1\right)$.


		 **Autre méthode**:
		    Une épreuve peut être assimilée à une application de $\llbracket1,n\rrbracket$ (ensemble des numéros des boules) dans $\llbracket1,3\rrbracket$ (ensemble des numéros des cases).
		    Notons $\Omega$ l’ensemble de ces applications.
		    On a donc : $\mathrm{card}\:\Omega=3^n$.
		    Les boules vont se "ranger aléatoirement dans les trois compartiments", donc il y a équiprobabilité sur $\Omega$.
		    (a) L’événement $(X=2)$ correspond aux applications dont les images se concentrent sur le même élément de $\llbracket1,3\rrbracket$, c’est-à-dire aux applications constantes.
		    Donc $P(X=2)=\dfrac{3}{3^n}=\dfrac{1}{3^{n-1}}$.
		    (b) Comptons à présent le nombre d’applications correspondant à l’événement $(X=1)$, c’est-à-dire le nombre d’applications dont l’ensemble des images est constitué de deux éléments exactement.
		    On a 3 possibilités pour choisir l’élément de $\llbracket1,3\rrbracket$ qui n’a pas d’antécédent et ensuite, chaque fois, il faut compter le nombre d’applications de $\llbracket1,n\rrbracket$ vers les deux éléments restants de $\llbracket1,3\rrbracket$, en excluant bien sûr les deux applications constantes.
		    On obtient donc $2^n-2$ applications.
		    D’où $P(X=1)=\dfrac{3\times \left( 2^n-2\right) }{3^n}=\dfrac{1}{3^{n-1}}\left( 2^n-2\right)$.
		    Enfin, comme dans la méthode précédente, $P(X=0)=1-P(X=2)-P(X=1)$ donc $P(X=0)=1-\left( \frac{1}{3}\right) ^{n-1}-\left( \frac{1}{3}\right) ^{n-1}\left( 2^n-2\right)$.

		3.  1.  $E(X)=0P(X=0)+1P(X=1)+2P(X=2)=\left( \dfrac{1}{3}\right) ^{n-1}\left( 2^n-2\right)+2\left( \dfrac{1}{3}\right) ^{n-1}$.
		        Donc $E(X)=3\left(\dfrac{2}{3}\right)^{n}.$

		    2.  D’après 3.(a), $\lim\limits_{n\to+\infty}^{}E(X)=\lim\limits_{n\to+\infty}^{}3\left(\dfrac{2}{3}\right)^{n}=0$.
		        Quand le nombre de boules tend vers $+\infty$, en moyenne aucun des trois compartiments ne restera vide.





!!! exercice "Corrigé <a name="corrige-56"></a><a href="#enonce-56">56</a>"


		1.  $X(\Omega)=\llbracket 1,3\rrbracket$.
		    $\forall \: i\in\llbracket1,n\rrbracket$, on note $B_i$ la $i ^{\text{ème}}$ boule blanche.
		    $\forall \: i\in\llbracket1,2\rrbracket$, on note $N_i$ la $i ^{\text{ème}}$ boule noire.
		    On pose $E=\left\lbrace B_1,B_2,...,B_n,N_1,N_2 \right\rbrace$.
		    Alors $\Omega$ est l’ensemble des permutations de $E$ et donc $\text{card}(\Omega)=(n+2)!$.

		    $(X=1)$ correspond aux tirages des $(n+2)$ boules pour lesquels la première boule tirée est blanche.
		    On a donc $n$ possibilités pour le choix de la première boule blanche et donc $(n+1)!$ possibilités pour les tirages restants.
		    Donc $P(X=1)=\dfrac{n\times(n+1)!}{(n+2)!}=\dfrac{n}{n+2}.$
		    $(X=2)$ correspond aux tirages des $(n+2)$ boules pour lesquels la première boule tirée est noire et la seconde est blanche.
		    On a donc 2 possibilités pour la première boule, puis $n$ possibilités pour la seconde boule et enfin $n!$ possibilités pour les tirages restants.
		    Donc $P(X=2)=\dfrac{2\times n\times(n)!}{(n+2)!}=\dfrac{2n}{(n+1)(n+2)}.$
		    $(X=3)$ correspond aux tirages des $(n+2)$ boules pour lesquels la première boule et la seconde boule sont noires.
		    On a donc 2 possibilités pour la première boule, puis une seule possibilité pour la seconde et enfin $n!$ possibilités pour les boules restantes.
		    Donc $P(X=3)=\dfrac{2\times 1\times(n)!}{(n+2)!}=\dfrac{2}{(n+1)(n+2)}.$

		 **Autre méthode**:
		    Dans cette méthode, on ne s’interesse qu’aux "premières" boules tirées, les autres étant sans importance.
		    $X(\Omega)=\llbracket 1,3\rrbracket$.
		    $(X=1)$ est l’événement: "obtenir une boule blanche au premier tirage".
		    Donc $P(X=1)=\dfrac{\text{nombre de boules blanches}}{\text{nombre de boules de l'urne}}=\dfrac{n}{n+2}$.
		    $(X=2)$ est l’événement: " obtenir une boule noire au premier tirage puis une boule blanche au second tirage".
		    D’où $P(X=2)=\dfrac{2}{n+2}\times\dfrac{n}{n+1}=\dfrac{2n}{(n+2)(n+1)}$, les tirages se faisant sans remise.
		    $(X=3)$ est l’événement : "obtenir une boule noire lors de chacun des deux premiers tirages puis une boule blanche au troisième tirage".
		    D’où $P(X=3)=\dfrac{2}{n+2}\times\dfrac{1}{n+1}\times\dfrac{n}{n}=\dfrac{2}{(n+2)(n+1)}$, les tirages se faisant sans remise.

		2.  $Y(\Omega)=\llbracket 1,n+1\rrbracket$.
		    Soit $k\in \llbracket 1,n+1\rrbracket$.
		    L’événement $(Y=k)$ correspond aux tirages des $(n+2)$ boules où les $(k-1)$ premières boules tirées ne sont ni $B_1$ ni $N_1$ et la $k^{\text{ième}}$ boule tirée est $B_1$ ou $N_1$.
		    On a donc, pour les $(k-1)$ premières boules tirées , $\dbinom{n}{k-1}$ choix possibles de ces boules et $(k-1)!$ possibilités pour leur rang de tirage sur les $(k-1)$ premiers tirages, puis 2 possibilités pour le choix de la $k^{\text{ième}}$ boule et enfin $(n+2-k)!$ possibilités pour les rangs de tirage des boules restantes.
		    Donc $P(Y=k)=\dfrac{\dbinom{n}{k-1}\times (k-1)!\times 2\times (n+2-k)!}{(n+2)!}
		    =\dfrac{2\dfrac{n!}{(n-k+1)!}\times(n+2-k)!}{(n+2)!}$
		    Donc $P(Y=k)=\dfrac{2(n+2-k)}{(n+1)(n+2)}$.

		 **Autre méthode**:
		    $Y(\Omega)=\llbracket 1,n+1\rrbracket$.
		    On note $A_k$ l’événement " une boule ne portant pas le numéro 1 est tirée au rang $k$".
		    Soit $k\in \llbracket 1,n+1\rrbracket$.
		    On a : $(Y=k)=A_1\cap A_2\cap....\cap A_{k-1}\cap \overline{A_k}$.
		    Alors, d’après la formule des probabilités composées,
		    $P(Y=k)=P(A_1)P_{A_1}(A_2)...P_{A_1\cap A_2\cap...\cap A_{k-2}}(A_{k-1})P_{A_1\cap A_2\cap...\cap A_{k-1}}(\overline{A_k})$.
		    $P(Y=k)=\dfrac{n}{n+2}\times\dfrac{n-1}{(n+2)-1}\times\dfrac{n-2}{(n+2)-2}\times ...\times\dfrac{n-(k-2)}{(n+2)-(k-2)}\times \dfrac{2}{(n+2)-(k-1)}$
		    $P(Y=k)=\dfrac{n}{n+2}\times\dfrac{n-1}{n+1}\times\dfrac{n-2}{n}\times...\times\dfrac{n-k+2}{n-k+4}\times \dfrac{2}{n-k+3}$.
		    $P(Y=k)=2\:\dfrac{n!}{(n-k+1)!}\times \dfrac{(n-k+2)!}{(n+2)!}$.
		    $P(Y=k)=\dfrac{2(n-k+2)}{(n+2)(n+1)}$.





!!! exercice "Corrigé <a name="corrige-57"></a><a href="#enonce-57">57</a>"

		 On a :

		$$
		\begin{align*}
		P(X=i)&=\sum_{j=1}^{n+1}P(X=i\cap Y=j)\\
		&=\frac 1{2^{2n}}\binom{n}{i-1}\sum_{j=1}^{n+1} \binom n{j-1}\\
		&=\frac 1{2^{2n}}\binom{n}{i-1}\sum_{j=0}^{n} \binom n{}\\
		&=\frac 1{2^{2n}}\binom{n}{i-1}2^n\\
		&=\frac 1{2^{n}}\binom{n}{i-1}\\
		\end{align*}
		$$

		ce qui est bien la valeur cherchée.

		La variable aléatoire $Z=X-1$ est à valeurs dans $\{0,\dots,n\}$ avec $P(Z=i)=P(X=i+1)=\frac 1{2^n}\binom n i$. On a alors $E(Z)=\frac1{2^n}\sum_{i=0}^n i\binom ni=\frac1{2^n}\sum_{i=1}^nn\binom{n-1}{i-1}=\frac n{2^n}\sum_{i=0}^{n-1}\binom {n-1}i=\frac n2$. On calcul de même la variance.

		Pour le calcul de $\mathbb E(e^X)$, on a :

		$$
		\begin{align*}
		\mathbb E(e^X) &= \sum_{i=1}^{n+1} e^i \mathbb P(X=i)\\
		&=\frac 1{2^n}\sum_{i=1}^{n+1}\binom n{i-1} e^i\\
		&=\frac e{2^n}\sum_{i=0}^{n}\binom ni e^i\\
		&=\frac e{2^n}(1+e)^n
		\end{align*}
		$$




!!! exercice "Corrigé <a name="corrige-58"></a><a href="#enonce-58">58</a>"
		$T$ est bien définie car les $X_{i}$ sont au nombre de $n+1$ et peuvent prendre $n$ valeurs : d'après le principe des paires de chaussette, on a l'existence de $i_{1}<i_{2}$ tels que $X_{i_{1}}=X_{i_{2}}$ pour n'importe quelle réalisation de l'expérience. L'ensemble dont on prend le minimum n'est donc pas vide (il contient $\left.i_{2}\right)$.


		```python
		def T (X) :
		    n = len (X)
		    jmin = 0
		    j=1
		    while jmin == 0 :
		        for l in range(j) :
		            if X[l]==X[j] :
		                jmin = j
		        j = j+1
		    return jmin+1
		T([1,2,3,4,5,3,7,9])
		```




		    6




		```python
		import random
		import math
		n=1000
		def moyenneT (N):
		    sommeT = 0
		    for i in range(N) :
		        X = [random.randint(1,n) for i in range (n+1)]
		        sommeT = sommeT+T(X)
		    return sommeT/N

		print(moyenneT(1000))
		math.sqrt(2*math.pi*n)
		```

		    40.361





		    79.26654595212021



		Les valeurs possibles pour $T$ sont $2,3, \ldots, n+1$.
		Prenons $i \in\{2, \ldots, n+1\}$. La probabilité que $T$ soit égal à $i$ est donnée par $P(T=i)=$ $\frac{n}{n} \times \frac{n-1}{n} \times \cdots \times \frac{n-(i-1-1)}{n} \times \frac{i-1}{n}=\frac{n !(i-1)}{n^{i}(n-i+1) !}:$ une injection jusqu'au rang $i-1$ puis seulement $i-1$ choix au rang $i$.
		On a $E(T)=\sum_{i=2}^{n+1} i P(T=i)$ par définition. Or
		$$
		\begin{aligned}
		\sum_{i=2}^{n+1} i P(T=i) &=\sum_{i=2}^{n+1} \sum_{k=0}^{i-1} P(T=i) \\
		&=\sum_{k=0}^{n} \sum_{i=k+1}^{n+1} P(T=i) \\
		&=\sum_{k=0}^{n} P(T>k)
		\end{aligned}
		$$
		Or la probabilité que $T>k$ est la probabilité de n'avoir eu aucune répétition jusqu'au rang $k$ : on a $P(T>k)=\frac{n \times(n-1) \times \cdots \times n-(k-1)}{n^{k}} \mathrm{~d}^{\prime} \mathrm{où} E(T)=\sum_{k=0}^{n} \frac{n !}{n^{k}(n-k) !}=\frac{n !}{n^{n}} \sum_{k=0}^{n} \frac{n^{n-k}}{(n-k) !}=\frac{n !}{n^{n}} \sum_{k=0}^{n} \frac{n^{k}}{k !}$.




!!! exercice "Corrigé <a name="corrige-59"></a><a href="#enonce-59">59</a>"

		On suit les recommandations du concours Centrale pour le calcul matriciel :


		```python
		from IPython.display import display,Latex

		import numpy as np
		import numpy.linalg as alg
		```

		On écrit une fonction qui créée une matrice du type demander lorsqu'on lui donne en entrée les $n$ valeurs $\left(a_{1}, a_{2}, \ldots, a_{n}\right)$.


		```python
		def matrice(L) :
		    n = len(L)
		    M = np.eye(n+1)
		    for i in range(n) :
		        M[i,i+1] = -1
		        M[i+1,i] = L[i]
		    return alg.det(M)
		```


		```python
		matrice ([1,2,3,4])
		```




		    25.99999999999999



		On commence par le cas où $a_i=1$.


		```python
		for i in range(1,30) :
		    display(Latex(r"$n=%d : \Delta_{%d}=%f$"%(i,i,matrice([1]*i))))
		```


		$n=1 : \Delta_{1}=2.000000$



		$n=2 : \Delta_{2}=3.000000$



		$n=3 : \Delta_{3}=5.000000$



		$n=4 : \Delta_{4}=8.000000$



		$n=5 : \Delta_{5}=13.000000$



		$n=6 : \Delta_{6}=21.000000$



		$n=7 : \Delta_{7}=34.000000$



		$n=8 : \Delta_{8}=55.000000$



		$n=9 : \Delta_{9}=89.000000$



		$n=10 : \Delta_{10}=144.000000$



		$n=11 : \Delta_{11}=233.000000$



		$n=12 : \Delta_{12}=377.000000$



		$n=13 : \Delta_{13}=610.000000$



		$n=14 : \Delta_{14}=987.000000$



		$n=15 : \Delta_{15}=1597.000000$



		$n=16 : \Delta_{16}=2584.000000$



		$n=17 : \Delta_{17}=4181.000000$



		$n=18 : \Delta_{18}=6765.000000$



		$n=19 : \Delta_{19}=10946.000000$



		$n=20 : \Delta_{20}=17711.000000$



		$n=21 : \Delta_{21}=28657.000000$



		$n=22 : \Delta_{22}=46368.000000$



		$n=23 : \Delta_{23}=75025.000000$



		$n=24 : \Delta_{24}=121393.000000$



		$n=25 : \Delta_{25}=196418.000000$



		$n=26 : \Delta_{26}=317811.000000$



		$n=27 : \Delta_{27}=514229.000000$



		$n=28 : \Delta_{28}=832040.000000$



		$n=29 : \Delta_{29}=1346269.000000$


		On passe au cas où $a_i=\frac 1{i^2}$.


		```python
		for i in range(1,30) :
		    display(Latex(r"$n=%d : \Delta_{%d}=%f$"%(i,i,matrice([1/n**2 for n in range(1,i)]))))
		```


		$n=1 : \Delta_{1}=1.000000$



		$n=2 : \Delta_{2}=2.000000$



		$n=3 : \Delta_{3}=2.250000$



		$n=4 : \Delta_{4}=2.472222$



		$n=5 : \Delta_{5}=2.612847$



		$n=6 : \Delta_{6}=2.711736$



		$n=7 : \Delta_{7}=2.784315$



		$n=8 : \Delta_{8}=2.839657$



		$n=9 : \Delta_{9}=2.883162$



		$n=10 : \Delta_{10}=2.918219$



		$n=11 : \Delta_{11}=2.947051$



		$n=12 : \Delta_{12}=2.971168$



		$n=13 : \Delta_{13}=2.991634$



		$n=14 : \Delta_{14}=3.009215$



		$n=15 : \Delta_{15}=3.024478$



		$n=16 : \Delta_{16}=3.037853$



		$n=17 : \Delta_{17}=3.049667$



		$n=18 : \Delta_{18}=3.060179$



		$n=19 : \Delta_{19}=3.069591$



		$n=20 : \Delta_{20}=3.078068$



		$n=21 : \Delta_{21}=3.085742$



		$n=22 : \Delta_{22}=3.092722$



		$n=23 : \Delta_{23}=3.099097$



		$n=24 : \Delta_{24}=3.104944$



		$n=25 : \Delta_{25}=3.110324$



		$n=26 : \Delta_{26}=3.115292$



		$n=27 : \Delta_{27}=3.119893$



		$n=28 : \Delta_{28}=3.124166$



		$n=29 : \Delta_{29}=3.128146$


		Dans le cas où $a_{n}=1$, on peut essayer d'établir une relation de récurrence d'ordre 2 probablement. On a en développant par rapport à la première ligne : $\Delta_{n}=\Delta_{n-1}+$ $\left|\begin{array}{ccccc}1 & -1 & 0 & \ldots & 0 \\ 0 & 1 & -1 & \ddots & \vdots \\ 0 & 1 & \ddots & \ddots & 0 \\ \vdots & \ddots & \ddots & \ddots & -1 \\ 0 & \ldots & 0 & 1 & 1\end{array}\right|=\Delta_{n-1}+\Delta_{n-2}$.

		Cette relation de récurrence admet pour polynôme caractéristique $X^{2}-X-1$ dont les racines sont $\frac{1+\sqrt{5}}{2}$ et $\frac{1-\sqrt{5}}{2}$. Avec $\Delta_{0}=1$ et $\Delta_{1}=2$, on en déduit que $\Delta_{n}=$ $\frac{1}{10}\left((5+3 \sqrt{5})\left(\frac{1+\sqrt{5}}{2}\right)^{n}-(5-3 \sqrt{5})\left(\frac{1-\sqrt{5}}{2}\right)^{n}\right)$.

		Ainsi, $\Delta_{n} \sim \frac{1}{10}(5+3 \sqrt{5})\left(\frac{1+\sqrt{5}}{2}\right)^{n}$ qui est le terme général d'une suite divergente (car $\left.\frac{1+\sqrt{5}}{2}>1\right)$
		Effectuons maintenant un développement par rapport à la dernière ligne :
		$\Delta_{n}=\Delta_{n-1}+a_{n} * \Delta_{n-2}$ En supposant qu'on ai fait un raisonnement par récurrence, par exemple à deux pas :
		$$
		\Delta_{n} \leq \prod_{k=1}^{n-1}\left(1+a_{k}\right)+a_{n} \prod_{k=1}^{n-2}\left(1+a_{k}\right)=\left(1+a_{n-1}+a_{n}\right) \prod_{k=1}^{n-2}\left(1+a_{k}\right)
		$$
		Or $1+a_{n-1}+a_{n} \leq\left(1+a_{n-1}\right)\left(1+a_{n}\right)$ car les $a_{k}$ sont positifs. On en déduit : $\Delta_{n} \leq \prod_{k=1}^{n}\left(1+a_{k}\right)$.
		Enfin, on montre facilement par récurrence grâce à la relation $\Delta_{n}=\Delta_{n-1}+a_{n} * \Delta_{n-2}$ que la suite des $\Delta_{n}$ est croissante strictement et positive. Elle converge donc si et seulement si elle est majorée.

		Supposons que la suite des $\Delta_{n}$ converge, alors d'après la relation $\Delta_{n}-\Delta_{n-1}=a_{n} \Delta_{n-2}$ nous permet de dire que la suite $\left(\Delta_{n}\right)$ et la série de terme général $a_{n} \Delta_{n-2}$ ont même nature. Or la suite $\Delta_{n}$ étant croissante strictement et positive, si elle converge c'est vers une limite $\ell>0$. On a donc $a_{n} \Delta_{n-2} \sim \ell a_{n}$ et par comparaison des séries à termes positifs, $\sum a_{n} \Delta_{n-2}$ et $\sum a_{n}$ ont même nature. FInalement, la suite $\left(\Delta_{n}\right)$ et la série $\sum a_{n}$ ont même nature.




!!! exercice "Corrigé <a name="corrige-60"></a><a href="#enonce-60">60</a>"
		```python
		from numpy.polynomial import Polynomial

		Q = Polynomial([-1,1])*Polynomial([-2,0,1])**2

		def f(x) :
		    global Q
		    return x-Q(x)/Q.deriv()(x)
		```


		```python
		f(3)
		```




		    2.5483870967741935




		```python
		def u(n) :
		    u=[10]
		    for i in range(n-1) :
		        u.append(f(u[-1]))
		    return u
		```


		```python
		u(10)
		```




		    [10,
		     8.074235807860262,
		     6.541527629026789,
		     5.3248556425345015,
		     4.362783830788522,
		     3.606278708437776,
		     3.0161124501029803,
		     2.560729998204561,
		     2.214504641077209,
		     1.9563488863912304]




		```python
		u(100)
		```




		    [10,
		     8.074235807860262,
		     6.541527629026789,
		     5.3248556425345015,
		     4.362783830788522,
		     3.606278708437776,
		     3.0161124501029803,
		     2.560729998204561,
		     2.214504641077209,
		     1.9563488863912304,
		     1.768665852013774,
		     1.6365973057160934,
		     1.547469779474154,
		     1.4903613736369952,
		     1.4558698005004111,
		     1.4362243881477756,
		     1.4255700433662408,
		     1.4199885948718396,
		     1.4171265872593901,
		     1.4156766295325953,
		     1.4149467577738428,
		     1.414580578483789,
		     1.4143971754037628,
		     1.414305395179342,
		     1.414259485352659,
		     1.414236525506241,
		     1.4142250443464226,
		     1.4142193034629025,
		     1.4142164329255331,
		     1.414214997664669,
		     1.4142142799203778,
		     1.4142139213246057,
		     1.4142137421152652,
		     1.4142136526433327,
		     1.414213606620553,
		     1.4142135884478826,
		     1.4142135678892096,
		     1.4142135921845287,
		     1.4142135786981842,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994,
		     1.414213570488994]



		La suite semble converger vers $\sqrt 2$.

		On a $Q'=(x^2-2)^2+2(x-1)\times 2x\times(x^2-2)=(x^2-2)(x^2-2+4x^2-4x)=(x^2-2)(5x^2-4x-2)$. Le second membre (noté $R$) est un trinôme dont le sommet se trouve en $\frac 25\in[-\sqrt 2,\sqrt 2]$. De plus  $R(\sqrt 2)=10-4\sqrt 2-2=4(2-\sqrt 2)>0$ et $R(-\sqrt 2)=10+4\sqrt2-2>0$. On en déduit que les racines de $R$ sont bien dans l'intervalle $[-\sqrt2,\sqrt2]$.

		On suppose donc qu'il en est de même pour $Q''$.

		On a $f'(x)=1-\frac{Q'(x)^2-Q(x)Q''(x)}{Q'(x)^2}=\frac{Q(x)Q''(X)}{Q'(x)^2}$ dont le signe est déterminé par le signe de $Q(x)Q''(x)$. Avec les données de l'énoncé, on ne peut pas dire grand chose de plus, si ce n'est que sur $[\sqrt2,+\infty[$ la fonction $f$ est croissante.


		```python
		from numpy import log
		from matplotlib import pyplot as plt
		def graphique(n) :
		    termes_u = u(n)
		    y =[log(termes_u[k]-2**(1/2)) for k in range(n)]
		    x = list(range(n))
		    plt.figure()
		    plt.plot(x,y)
		    plt.show()
		```


		```python
		graphique(150)
		```



		![png](exercices/CentralePython/rms2019-1011_files/rms2019-1011_9_0.png)



		L'interprétation est délicate : on semble avoir une décroissance linéaire, mais à partir du rang $\sim 40$ le comportement change.

		Montrons que $u_n>\sqrt2$. C'est le cas pour $u_0$. Intéressons-nous à l'hérédité : $u_{n+1} = u_n-\frac{Q(u_n)}{Q'(u_n)}$.  

		Or $\frac Q{Q'}=\frac{(X-1)(X^2-2)^2}{(X^2-2)(5X^2-4X-2)}=\frac{(X-1)(X^2-2)}{5X^2-4X-2}$. Or $f$ est croissante sur $[\sqrt2,+\infty[$ et $f(\sqrt2)=\sqrt2$. De plus $u_n>\sqrt2$ par hypothèse de récurrence dont $f(u_n)>\sqrt2$. On en déduit ainsi que $u_{n+1}>\sqrt 2$.

		On applique ensuite simplement le théorème des accroissements finis à $f$ entre $\sqrt 2$ et $u_n$ : il existe $c_n>\sqrt2$ tel que $u_{n+1}-\sqrt 2=f(u_n)-f(\sqrt 2)=f'(c_n)(u_n-\sqrt2)$.

		On a

		$$
		\begin{align*}
		f(x)&=x-\frac{(x-1)(x^2-2)}{(5x^2-4x-2}\\
		&=x-\frac{(x-1)(x^2-2)}{4(2-\sqrt2)+(10\sqrt 2-4)(x-\sqrt 2)+5(x-\sqrt 2)^2}\\
		&=\sqrt 2+(x-\sqrt2)-\frac{2\sqrt 2(\sqrt 2-1)(x-\sqrt 2)+o(x-\sqrt 2)}{4(2-\sqrt2)+(10\sqrt 2-4)(x-\sqrt 2)+o(x-\sqrt 2)}\\
		&=\sqrt 2+(x-\sqrt2)-\frac{1}{4(2-\sqrt2)}\times (2\sqrt 2(\sqrt 2-1)(x-\sqrt 2)+o(x-\sqrt 2))\times(1-\frac{(10\sqrt 2-4)}{4(2-\sqrt2)}(x-\sqrt 2)+o(x-\sqrt 2))\\
		&=\sqrt 2+(x-\sqrt2)-\frac{1}{4(2-\sqrt2)}\times (2\sqrt 2(\sqrt 2-1)(x-\sqrt 2)+o(x-\sqrt 2))\\
		&=\sqrt 2+\frac{1}{2}(x-\sqrt 2)+o(x-\sqrt2)
		\end{align*}
		$$

		$f$ étant dérivable sur $[\sqrt 2,+\infty[$, on en déduit que la limite de $f'$ en $\sqrt 2$ par valeurs supérieures est $\frac 12$.


		On pose $v_n=u_n-\sqrt 2$ et $w_n=\ln(v_{n+1})-\ln(v_n)$. La suite $w_n$ converge vers $-\ln(2)$. On en déduit que la série de terme général $w_n$ diverge et on peut donc donner un équivalent de la somme partielle : $\sum_{k=0}^N w_k\sim -\ln(2) N$. On en déduit que $\ln(v_N)-ln(v_0)\sim-\ln(2)N$. On a donc la décroissance linéaire observée : le chagement de comportement autour de $n=40$ est très probablement dû aux apporximations numériques.




!!! exercice "Corrigé <a name="corrige-61"></a><a href="#enonce-61">61</a>"

		Tout d'abord, $P$ n'admettant aucune racine réelles, la fonction $t\mapsto |P(e^{it})|$ est continue et ne s'annule pas. Par composition des fonctions continues, $t\mapsto \ln|P(e^{it})|$ est continue sur $[0,2\pi]$ et donc $M$ est bien définie.

		$P$ étant un polynôme de $\mathbb C[X]$, il est scindé et on pose $P=B(X-z_1)^{m_1}\cdots(X-z_s)^{m_s}$ avec $B$ un réel. On a alors

		$$
		\begin{align*}
		M(P)&=\int_0^{2\pi}\ln|B|\prod_{k=1}^s|e^{it}-z_k|^{m_k}dt\\
		&=\int_0^{2\pi}\ln|B|+\sum_{k=1}^sm_k\ln|e^{it}-z_k|dt\\
		&=2\pi\ln|B|+\sum_{k=1}^sm_k\int_0^{2\pi}\ln|e^{it}-z_k|dt\\
		&= A+\sum_{k=1}^sm_kM(X-z_k).
		\end{align*}
		$$

		```python
		import numpy as np
		import scipy.optimize as resol
		import scipy.integrate as integr
		from scipy.integrate import quad as integrale
		import matplotlib.pyplot as plt


		#La fonction d'intégration de SCipy ne semble pas bien marcher
		def integrale(f,a,b) :
		    n=1000
		    somme = 0
		    for i in range(n) :
		        somme += f(a+i*(b-a)/n)
		    return (b-a)/n*somme

		def malher(r,theta) :
		    def f(t) :
		        return np.log(np.sqrt((np.cos(t)-r*np.cos(theta))**2+(np.sin(t)-r*np.sin(theta))**2))
		    return integrale(f,0,2*np.pi)
		```


		```python
		malher(2,50)
		```




		    4.355172180607205




		```python
		xrange = np.arange(0,2*np.pi,0.1)
		```


		```python
		y1 = [malher(0.5,x) for x in xrange]
		y2 = [malher(1,x) for x in xrange]
		y3 = [malher(100,x) for x in xrange]
		y4 = [malher(2019,x) for x in xrange]
		```

		    /tmp/ipykernel_25466/1878609574.py:18: RuntimeWarning: divide by zero encountered in log
		      return np.log(np.sqrt((np.cos(t)-r*np.cos(theta))**2+(np.sin(t)-r*np.sin(theta))**2))



		```python
		plt.figure()
		plt.plot(xrange,y1)
		plt.plot(xrange,y2)
		plt.plot(xrange,y3)
		plt.plot(xrange,y4)
		plt.show()
		```



		![png](exercices/CentralePython/rms2019-1022_files/rms2019-1022_7_0.png)



		Il semblerait que la fonction $\theta\mapsto malher(r,\theta)$ soit constante.

		Manifestement, la valeur de $\theta$ importe peu. Prenons donc $\theta=0$.


		```python
		rrange = np.arange(0,3,0.1)
		```


		```python
		y1 = [malher(r,0) for r in rrange]
		```

		    /tmp/ipykernel_25466/1878609574.py:18: RuntimeWarning: divide by zero encountered in log
		      return np.log(np.sqrt((np.cos(t)-r*np.cos(theta))**2+(np.sin(t)-r*np.sin(theta))**2))



		```python
		plt.figure()
		plt.plot(rrange,y1)
		plt.show()
		```



		![png](exercices/CentralePython/rms2019-1022_files/rms2019-1022_12_0.png)




		```python
		rrange = np.arange(1,20,0.1)
		```


		```python
		y1 = [malher(r,0) for r in rrange]
		```

		    /tmp/ipykernel_25466/1878609574.py:18: RuntimeWarning: divide by zero encountered in log
		      return np.log(np.sqrt((np.cos(t)-r*np.cos(theta))**2+(np.sin(t)-r*np.sin(theta))**2))



		```python
		plt.figure()
		plt.plot(rrange,y1,label="malher")
		plt.plot(rrange,2*np.pi*np.log(rrange),label="log")
		plt.legend()
		plt.show()
		```



		![png](exercices/CentralePython/rms2019-1022_files/rms2019-1022_15_0.png)



		On conjecture que $malher(r,\theta)=2\pi\ln(r)$ pour $r>1$ et $0$ pour $0\le r\le 1$.

		On a $M(X-re^{i\theta})=\int_0^{2\pi} \ln|e^{it}-re^{i\theta}|dt = \int_0^{2\pi} \ln|e^{i(t-\theta)}-r|dt = \int_{-\theta}^{2\pi-\theta} \ln|e^{it}-r|dt \underset {2\pi\text{-periodique}}=  \int_{0}^{2\pi} \ln|e^{it}-r|dt=M(X-r)$

		Prenons maintenant $r>1$. On sait que $\int_0^{2\pi}\ln|e^{it}-re^{i\theta}|dt$ est constante par rapport à $\theta$ donc on peut remplacer $\theta$ par des racines de l'unité par exemple :
		$$\int_0^{2\pi}\ln|e^{it}-r|dt=\frac 1n\sum_{w\in \mathbb U_n}\int_0^{2\pi}\ln|e^{it}-rw|dt=\frac 1n \int_0^{2\pi}\ln|P_n(e^{it})|dt$$
		avec $P_n=X^n-r^n$ qui n'admet aucune racine de module $1$.

		Notons $f_n(t)=\frac1n\ln|e^{int}-r^n|$ alors $|f_n(t)-\ln(r)|=\frac1n\ln|\frac{e^{int}}{r^n}-1|\le \frac 1n\ln(1+\frac1{r^n})\to 0$. Donc $f_n$ converge uniformément vers la fonction constante égale  à $\ln(r)$. on en déduit que $\int_0^{2\pi}f_n(t)dt$ converge vers $2\pi\ln(r)$. Et finalement,  $\int_0^{2\pi}\ln|e^{it}-re^{i\theta}|dt = 2\pi\ln(r)$.




!!! exercice "Corrigé <a name="corrige-62"></a><a href="#enonce-62">62</a>"
		```python
		### Algorithme d'Euclide
		def pgcd(a,b) :
		    """On suppose a et b entiers naturels avec au moins un des deux non nul"""
		    while b != 0 :
		        r = a % b
		        a = b
		        b = r
		    return a
		```


		```python
		def phi(n) :
		    if n==0 or n==1 :
		        return n
		    elif n%2==0 :
		        return phi(n//2)
		    else :
		        return phi(n//2)+phi((n+1)//2)
		```


		```python
		[(phi(n),phi(n+1)) for n in range(100)]
		```




		    [(0, 1),
		     (1, 1),
		     (1, 2),
		     (2, 1),
		     (1, 3),
		     (3, 2),
		     (2, 3),
		     (3, 1),
		     (1, 4),
		     (4, 3),
		     (3, 5),
		     (5, 2),
		     (2, 5),
		     (5, 3),
		     (3, 4),
		     (4, 1),
		     (1, 5),
		     (5, 4),
		     (4, 7),
		     (7, 3),
		     (3, 8),
		     (8, 5),
		     (5, 7),
		     (7, 2),
		     (2, 7),
		     (7, 5),
		     (5, 8),
		     (8, 3),
		     (3, 7),
		     (7, 4),
		     (4, 5),
		     (5, 1),
		     (1, 6),
		     (6, 5),
		     (5, 9),
		     (9, 4),
		     (4, 11),
		     (11, 7),
		     (7, 10),
		     (10, 3),
		     (3, 11),
		     (11, 8),
		     (8, 13),
		     (13, 5),
		     (5, 12),
		     (12, 7),
		     (7, 9),
		     (9, 2),
		     (2, 9),
		     (9, 7),
		     (7, 12),
		     (12, 5),
		     (5, 13),
		     (13, 8),
		     (8, 11),
		     (11, 3),
		     (3, 10),
		     (10, 7),
		     (7, 11),
		     (11, 4),
		     (4, 9),
		     (9, 5),
		     (5, 6),
		     (6, 1),
		     (1, 7),
		     (7, 6),
		     (6, 11),
		     (11, 5),
		     (5, 14),
		     (14, 9),
		     (9, 13),
		     (13, 4),
		     (4, 15),
		     (15, 11),
		     (11, 18),
		     (18, 7),
		     (7, 17),
		     (17, 10),
		     (10, 13),
		     (13, 3),
		     (3, 14),
		     (14, 11),
		     (11, 19),
		     (19, 8),
		     (8, 21),
		     (21, 13),
		     (13, 18),
		     (18, 5),
		     (5, 17),
		     (17, 12),
		     (12, 19),
		     (19, 7),
		     (7, 16),
		     (16, 9),
		     (9, 11),
		     (11, 2),
		     (2, 11),
		     (11, 9),
		     (9, 16),
		     (16, 7)]




		```python
		[pgcd(phi(n),phi(n+1)) for n in range(100)]
		```




		    [1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1,
		     1]



		Il semblerait que $\phi(n)$ et $\phi(n+1)$ soient premiers entre eux.

		Montrons cette conjecture, par récurrence. On pose $H(n)="\phi(n)\wedge\phi(n+1)=1"$. Pour $n=0$, cela est bien vérifié. Soit $n>0$, supposons que $H(k)$ est vraie pour tout $k<n$. Il y a deux cas à traiter :

		**Si $n$ est pair :** Notons $n=2p$, on a alors $pgcd(\phi(n),\phi(n+1))=pgcd(\phi(p),\phi(p)+\phi(p+1))$. Or si $d$ est un diviseur commun à $\phi(p)$ et $\phi(p+1)+\phi(p)$ alors $d$ est un diviseur commun à $\phi(p)$ et $\phi(p+1)$ : c'est $1$ par hypothèse de récurrence forte. On en déduit que $pgcd(\phi(n),\phi(n+1))=1$.

		**Si $n$ est impair :** Notons $n=2p+1$ alors $pgcd(\phi(n),\phi(n+1))=pgcd(\phi(p)+\phi(p+1),\phi(p+1))$ et par le même raisonnement que précédemment, on trouve un pgcd égal à $1$.

		On a donc montré, par récurrence forte, que pour tout $n$ entier naturel, $\phi(n)$ et $\phi(n+1)$ sont premiers entre eux.

		On pose donc $P=\left\{(a, b) \in \mathbb{N}^{2}, \operatorname{pgcd}(a, b)=1\right\}$. On vient de montrer que $\Phi(\mathbb N)\subset P$. Montrons l'inclusion réciproque.

		Soit un couple $(a,b)$ tel que $pgcd(a,b)=1$.

		on construit la suite $(a_n,b_n)$ par $(a_0,b_0)=(a,b)$ et pour obtenir $(a_{i+1},b_{i+1})$ on soustrait le plus petit au plus grand. On exécute ainsi un algorithme d'euclide qui va donc se terminer sur le couple $(1,1)$.

		Dans cette suite, supposons que $(a_{i+1},b_{i+1})=(\phi(n),\phi(n+1))$. Alors deux cas sont possibles.
		Soit $a_{i+1}=a_i-b_i$ et $b_{i+1}=b_i$,  dans ce cas, $(a_i,b_i)=(a_{i+1}+b_{i+1},b_{i+1})=(\phi(n)+\phi(n+1),\phi(n+1))=(\phi(2n+1),\phi(2n+2))$ et donc $(a_i,b_i)\in \mathrm{im}(\Phi)$. Soit $(b_{i+1}=b_i-a_i)$ et alors $(a_i,b_i)=(a_{i+1},a_{i+1}+b_{i+1})=(\phi(n),\phi(n)+\phi(n+1))=(\phi(2n),\phi(2n+1))\in im(\Phi)$. On en déduit donc que si un terme de la suite est dans l'image de $\Phi$, alors tous les précédents aussi.

		Or si $(a,b)$ sont premiers entre eux, notre suite termine sur $(1,1)$ qui est dans l'image de $\Phi$ : $\Phi(1)=(1,1)$. On en déduit que $(a,b)$ est dans l'image de $\Phi$. Ainsi $P\subset \Phi(\mathbb N)$.




		Faisons une observation plus générale : si $n$ est un entier naturel pair, on pose $(a_0,b_0)=\Phi(n)$ avec par exemple $a_0>b_0$. On a alors $(a_1,b_1)=(\phi(2p),\phi(2p+1)-\phi(2p))=(\phi(p),\phi(p+1))=\Phi(n/2)=\Phi(\lfloor\frac n2\rfloor)$.

		Si $n$ est un entier naturel impair, alors avec les mêmes notations, $(a_1,b_1)=(\phi(2p+1)-\phi(2p+2),\phi(2p+2))=(\phi(p),\phi(p+1))=\Phi(\lfloor\frac n2\rfloor)$.

		Ainsi, si $\Phi(n)=\Phi(m)$ alors $\Phi(\lfloor \frac n2\rfloor)=\Phi(\lfloor \frac m2\rfloor)$. Un raisonnement par récurrence donnerait alors $\lfloor \frac m2\rfloor=\lfloor \frac n2\rfloor)$.

		Or on sait que $\phi(2p+1)>\phi(p)=\phi(2p)$ et $\phi(2p+1)>\phi(p+1)=\phi(2p+2)$. on en déduit que si $\Phi(n)=\Phi(m)$ alors $m$ et $n$ on la même parité (tous les deux pairs ou tous les deux impairs).

		Ainsi le raisonnement pas récurrence cité au dessus implique que $m=n$.

		On en déduit (pas très rigoureusement) que $\Phi$ est injective. Or $P$ et $\mathbb Q^+$ étant clairement en bijection par $(a,b)\mapsto \frac ab$ et $\Phi$ étant surjective et injective dans $P$, on en déduit que $\Phi$ réalise une bijection de $\mathbb N$ dans $P$ et donc il existe une bijection de $\mathbb N$ dans $\mathbb Q^+$ sous la forme : $n\mapsto \frac{\phi(n)}{\phi(n+1)}$.



		```python
		def reciproque(a,b) :
		    l=[0,1]
		    i=1
		    while l[-2] != a or l[-1] != b :
		        i+=1
		        if i%2==0 :
		            l.append(l[i//2])
		        else :
		            l.append(l[i//2]+l[i//2+1])
		    return i-1
		```


		```python
		def phi2(n) :
		    l=[0,1]
		    i=1
		    while i<n :
		        i+=1
		        if i%2==0 :
		            l.append(l[i//2])
		        else :
		            l.append(l[i//2]+l[i//2+1])
		    return l[-1],i
		```


		```python
		phi2(3**10),phi2(3**10+1)
		```




		    ((1002, 59049), (725, 59050))




		```python
		reciproque(1002,725)
		```




		    59049



		Cela semble marcher pour $3^{10}$. Je ne m'aventurerai pas au delà.




!!! exercice "Corrigé <a name="corrige-63"></a><a href="#enonce-63">63</a>"

		```python
		def pgcd(a,b) :
		    while b != 0 :
		        r = a%b
		        a=b
		        b=r
		    return int(a)
		```

		On a $\frac {A_k}{B_k}=\frac {A_{k-1}}{B_{k-1}}+\frac 1k=\frac{kA_{k-1}+B_{k-1}}{kB_{k-1}}$. On a donc $A_k=\frac{kA_{k-1}+B_{k-1}}{pgcd(kA_{k-1}+B_{k-1},kB_{k-1})}$ et $B_k=\frac{kB_{k-1}}{pgcd(kA_{k-1}+B_{k-1},kB_{k-1})}$.


		```python
		def A(n) :
		    lA=[1]
		    lB=[1]
		    for i in range(2,n+1) :
		        p = pgcd(i*lA[-1]+lB[-1],i*lB[-1])
		        lA.append((i*lA[-1]+lB[-1])//p)
		        lB.append((i*lB[-1])//p)
		    return int(lA[-1])
		```


		```python
		A(16)%(17*17)
		```




		    0




		```python
		import sympy

		[(A(p-1)%(p**2)) for p in sympy.primerange(1,500)]
		```




		    [1,
		     3,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0]



		La propriété semble vraie à partir de $p=5$ premier.

		*Théorème de Wilson* : Si $(p-1)!=-1[p]$ alors il existe $k$ tel que $-(p-1)!+kp=1$. d'après la réciproque du théorème de Bézout, on en déduit que $(p-1)!$ et $p$ sont premiers entre eux. Donc $p$ est premier.

		Réciproquement, supposons que $p$ est premier. Soit $k\in\{1,\dots,p-1\}$, notons $\phi_k:a \mapsto ak$ dans $(\mathbb Z/p\mathbb Z)^*$. Soient $a$ et $a'$ tels que $\phi_k(a)=\phi_k(a')$, alors $p|k(a-a')$ et donc soit $p|k$, soit $p|a-a'$. Or $p$ ne divise pas $k$ donc $p$ divise $a-a'$. On en déduit $a=a'$.

		Ainsi, $\phi_k$ est injective donc bijective ici car entre deux ensembles finis de même taille. On en déduit qu'il existe un unique $k'$ tel que $kk'\equiv 1[p]$. Pour $k=1$, on a $k'=1$ et pour $k=-1$, $k'=-1$. Pour tous les autres on a clairement $k\ne k'$. On peut donc, dans le produit $(p-1)!$ regrouper les termes par symétriques. On aura donc $\overline{(p-1)!}=\bar 1\times\bar 1\times \overline{-1}=-1[p]$.

		Calculons $\sum_{k=1}^{p-1}a_k=\sum_{k=1}^{p-1}\frac{(p-1)!}{k(p-k)}=\frac{(p-1)!}p\sum_{k=1}^{p-1}\frac{1}{k}+\frac1{p-k}=\frac{2(p-1)!}pH_{p-1}=\frac{A_{p-1}}{B_{p-1}}\times\frac{2(p-1)!}{p}$. Les $a_k$ étant entiers, on en déduit que $B_{p-1}p$ divise $2(p-1)!A_{p-1}$. En particulier, $p$ divise $2(p-1)!A_{p-1}$. Or d'après le théorème de Wilson, $p$ et $2(p-1)!$ sont premiers entre eux donc $p$ divise $A_{p-1}$.

		On a $k^2a_k=\frac{k(p-1)!}{p-k}=-(p-1)!+\frac{p!}{p-k}$. Or $p$ divise $\frac{p!}{p-k}$ puisque $k\ge 1$.donc $k^2a_k\equiv -(p-1)![p]=1[p]$. On en déduit que que $a_k\equiv (k^2)^{-1}[p]$. On a alors :

		$$
		\sum_{k=1}^{p-1}a_k\equiv\sum_{k=1}^{p-1}(k^2)^{-1}\equiv\sum_{k=1}^{p-1}(k^{-1})^2
		$$

		Or la fonction $k\mapsto k^{-1}$ est une bijection dans $(\mathbb Z/p\mathbb Z)^{\star}$ donc :

		$$
		\sum_{k=1}^{p-1}a_k\equiv\sum_{k=1}^{p-1}k^2\equiv\frac 16p(p-1)(2p-1).
		$$

		Or $p$ est premier donc impair : $2|p-1$. Par ailleurs, $p\ge5$ donc $p\equiv 1[3]$ ou $p\equiv2[3]$. Dans le premier cas, $3|p-1$ et $2$ et $3$ étant premiers entre eux, $6|p-1$. On en déduit que $p$ divise $\frac 16p(p-1)(2p-1)$ et donc $p$ divise $\sum_{k=1}^{p-1}a_k$. Dans le deuxième cas, $2p-1\equiv 4-1[3]\equiv 0[3]$ et donc $6$ divise $(p-1)(2p-1)$. On en déduit encore que $p$ divise $\sum_{k=1}^{p-1}a_k$.

		Ainsi dans tous les cas, $p$ divise $\sum_{k=1}^{p-1}a_k$. Or $2A_{p-1}=\frac p\sum_{k=1}^{p-1}a_k$. On en déduit que $p^2$ divise $2A_{p-1}$ et puisque $p\ge5$ et premier, $p\wedge 2=1$ et donc $p^2$ divise $A_{p-1}$ (Théorème de Wolstenholme)




!!! exercice "Corrigé <a name="corrige-64"></a><a href="#enonce-64">64</a>"
		```python
		def different(l) :
		    dico = {}
		    for elem in l :
		        if elem not in dico :
		            dico[elem]=1
		    return len(dico)

		different([1,2,3,1,2])
		```




		    3




		```python
		import numpy.random as rd

		def simulX(n) :
		    L = []
		    for i in range(n) :
		        urne = rd.randint(n)
		        L.append(urne)
		    return n-different(L)
		```


		```python
		simulX(5)
		```




		    1



		$Y_i$ suit une loi de Bernoulli. Pour la déterminer, il faut identifier son paramètre. On cherche à identifier l'évènement $\{Y_i=1\}$. Notons $B_i$ la variable aléatoire qui donne le numéro de l'urne choisie pour la boule numéro $i$. On a $\{Y_i=1\}=\{B_1\ne i\}\cap \cdots\cap \{B_n\ne i\}$. On a donc :

		$$
		\begin{align*}
		\mathbb P(Y_i=1)&=\mathbb P(\{B_1\ne i\}\cap \cdots\cap \{B_n\ne i\})\\
		&=\mathbb P(B_1\ne i)\times \cdots\times P (B_n\ne 0)&\text{par indépendance}\\
		&={\left(\frac{n-1}n\right)}^n
		\end{align*}
		$$

		Ainsi, $Y_i$ suit une loi de Bernoulli de paramètre $p={\left(\frac{n-1}n\right)}^n$. Son espérance est $p$ et sa variance $p(1-p)$.

		$X_n$ représente le nombre d'urnes vides, donc on a bien $X_n=Y_1+\dots+Y_n$. Mais les $Y_i$ n'étant a priori pas indépendants, on ne peut pas affirmer que $X_n$ suit une loi binomiale.

		Cependant, on a toujours : $\mathbb E(X_n)=\sum_{i=1}^n \mathbb E(Y_i)=np=n{\left(\frac{n-1}n\right)}^n$.




		```python
		def experanceX(n) :
		    nb_expe = 10000
		    X = 0
		    for i in range(nb_expe) :
		        X += simulX(n)
		    return X/nb_expe,n*((n-1)/n)**n


		```


		```python
		experanceX(50)
		```




		    (18.211, 18.20848400435584)



		Le résultat expérimental est bien cohérent avec le calcul.

		On a $Cov(Y_i,Y_j)=\mathbb E((Y_i-p)(Y_j-p))=\mathbb E(Y_iY_j)-p^2$.

		Les valeurs possibles pour $Y_iY_j$ sont $0$ et $1$ : $Y_iY_j$ suit donc une loi de Bernoulli, avec $\mathbb P(Y_iY_j=1)=\mathbb P(Y_i=1\cap Y_j=1)={\left(\frac{n-2}{n}\right)}^n$. On en déduit :

		$$
		Cov(Y_i,Y_j)={\left(\frac{n-2}{n}\right)}^n-{\left(\frac{n-1}{n}\right)}^{2n}
		$$

		Et on en déduit :

		$$
		\begin{align*}
		Var(X_n)&=Var(Y_1+\dots+Y_n)\\
		&=\sum_{i=1}^n Var(Y_i)+2\sum_{i<j}Cov(Y_i,Y_j)\\
		&=np(1-p)+n(n-1)\left({\left(\frac{n-2}{n}\right)}^n-{\left(\frac{n-1}{n}\right)}^{2n}\right)\\
		&=np(1-p)+n(n-1)\left({\left(\frac{n-2}{n}\right)}^n-p^{2}\right)\\
		&=np(1-np)+n(n-1){\left(\frac{n-2}{n}\right)}^n
		\end{align*}
		$$


		```python
		def varianceX(n) :
		    p = ((n-1)/n)**n
		    nb_expe = 100000
		    X = 0
		    for i in range(nb_expe) :
		        X += (simulX(n)-n*p)**2
		    return X/nb_expe,n*p*(1-p)+n*(n-1)*(((n-2)/n)**n-p**2)


		```


		```python
		varianceX(10)
		```




		    (0.9899172145498394, 0.9927953579430713)




		```python
		varianceX(34)
		```




		    (3.3238508948235177, 3.3245860034560524)




!!! exercice "Corrigé <a name="corrige-65"></a><a href="#enonce-65">65</a>"
		La formule de Stirling donne un équivalent de la factorielle : $n!\sim \sqrt{2\pi n}n^ne^{-1}$.

		La fonction $f_n$ est strictement croissante sur $[0,+\infty[$ et $f_n(0)=-1$ et $\lim_{n\to+\infty}f_n(x)=+\infty$. Par le TVi, $f_n$ étant continue, il existe $u_n$ tel que $f_n(u_n)=0$. Par stricte croissance, ce $u_n$ est unique.

		On remarque par ailleurs que $f_n(1)=n-1\ge 0$ pour $n\ge 1$. On en déduit que $u_n\in]0,1]$. On peut exclure le $1$ si $n>1$. On a par ailleurs $f_{n+1}(u_n)=u_n^{n+1}+f_n(u_n)=u_n^{n+1}>0$. On en déduit que $u_n> u_{n+1}$. La suite $u_n$ est donc bornée et décroissante, elle converge donc. Notons $\ell$ sa limite.




		```python
		import numpy as np
		import scipy.optimize as resol
		def u(n) :
		    def fn(x) :
		        puissx=x
		        somme=0
		        for i in range(n) :
		            somme+=puissx
		            puissx = puissx * x # attention, x et puissx étant des tableau numpy, le raccourcis *= n'aurait pas l'effet attendu
		        return somme-1
		    return resol.fsolve(fn,0.4)


		u(3)
		```




		    array([0.54368901])




		```python
		[u(n) for n in range(100,110)]
		```




		    [array([0.5]),
		     array([0.5]),
		     array([0.5]),
		     array([0.5]),
		     array([0.5]),
		     array([0.5]),
		     array([0.5]),
		     array([0.5]),
		     array([0.5]),
		     array([0.5])]



		Cela semble converger vers $\frac 12$.


		```python
		[2**n*(u(n)-1/2) for n in range(10,20)]
		```




		    [array([0.25135336]),
		     array([0.25073557]),
		     array([0.25039765]),
		     array([0.25021389]),
		     array([0.25011452]),
		     array([0.25006106]),
		     array([0.25003243]),
		     array([0.25001717]),
		     array([0.25000906]),
		     array([0.25000477])]



		Cela semble converger vers $\frac 14$.


		```python
		[2**n*(u(n)-1/2) for n in range(50,60)]
		```




		    [array([-0.9375]),
		     array([-2.25]),
		     array([-4.5]),
		     array([-9.]),
		     array([-18.]),
		     array([-36.]),
		     array([-72.]),
		     array([-144.]),
		     array([-288.]),
		     array([-576.])]



		On observe cependant des valeurs étonnantes quand on va trop loin : cela est dû aux imprecisions de la méthode de résolution.

		On a $\sum _{k=1}^n x^k-1=\frac{x-x^{n+1}}{1-x}-1=\frac{x^{n+1}-2x+1}{x-1}$ et ainsi $f_n(u_n)=0\Leftrightarrow u_n^{n+1}-2u_n+1=0$. En passant à la limite, sachant que $u_n<1$, on a $-2\ell+1=0$ c'est à dire $\ell=\frac 12$.

		On a donc $2(u_n-\frac 12)=-u_n^{n+1}$ et donc $2n(u_n-\frac 12)=-nu_{n}^{n+1}$. Or $u_n$ converge en décroissant vers $\frac 12$. Il existe donc $n$ tel que $0\le u_n-\frac 12\le \frac 14$. On a alors $0\le nu_n^{n+1}\le \frac n{4^ {n+1}}$. Par croissances comparées, $nu_n^{n+1}$ converge vers $0$ et donc $n(u_n-\frac 12)$ aussi.

		On en déduit que $2u_n-1=o(\frac 1n)$ et alors $2^n(u_n-\ell)=2^{n-1}(2u_n-1)=2^{n-1}(u_n^{n+1})=\frac 14(2u_n)^{n+1}$. Or $(2u_n)^{n+1}=\exp((n+1)\ln(1+2u_n-1))=\exp((n+1)\ln(1+o(\frac 1n))=\exp((n+1)\times o(\frac 1n))\to 1$. On en déduit que $2^n(u_n-\ell)\to\frac 14$. Cela signifie que $u_n-\frac 12\sim\frac1{2^{n+2}}$ soit une convergence très rapide de $u_n$ vers $\frac 12$.




!!! exercice "Corrigé <a name="corrige-66"></a><a href="#enonce-66">66</a>"
		On calcule $(A_n^2)_{ij}=\sum_{k=1}^n(A_n)_{ik}(A_n)_{kj}=\sum_{k=1}^n(A_n)_{ik}(A_n)_{jk}=\sum_{k=1}^nX_{ik}X_{jk}$. Les variables $X_{ij}$ étant indépendantes, on a pour $i\ne j$ et tout $k$, $\mathbb E(X_{ik}X_{jk})=0$ et pour $i=j$, $\mathbb E(X_{ik}X_{jk})=1$. On en déduit alors $\mathbb E((A_n^2)_{ij})=n\delta_{ij}$ avec $\delta_{ij}$ le symbole de Kronecker. On en déduit donc $\mathbb E(A_n^2)=nI_n$.


		```python
		import numpy as np
		import numpy.linalg as alg
		import numpy.random as rd

		def sym(n) :
		        a = rd.randint(0,2,(n,n))
		        for i in range(n) :
		            for j in range(0,i+1) :
		                if a[i,j]==0 :
		                    a[i,j]=-1
		                a[j,i]=a[i,j]
		        return a

		def esperanceDet(n) :
		    nb = 50000
		    somme=0
		    for i in range(nb) :
		        somme += alg.det(sym(n))
		    return somme/nb
		```


		```python
		print(esperanceDet(1))
		print(esperanceDet(2))
		print(esperanceDet(3))
		print(esperanceDet(4))
		print(esperanceDet(5))
		print(esperanceDet(6))

		```

		    0.002
		    -0.99748
		    -0.00544
		    2.955679999999999
		    -0.04767999999999999
		    -14.83904


		Ce n'est pas clair, mais il semblerait que pour $n$ impair, $\Delta_n=0$ et pour $n$ pair égal à $2p$, $\Delta_n=1\times 3\times \dots\times(2p-1)\times(-1)^p$.  

		Pour $n=1$, $\Delta_1=\mathbb E(\det(A_1))=\mathbb E(X_{11})=0$.

		Pour $n=2$, $\Delta_2=\mathbb E(\det(A_2))=\mathbb E(X_{11}X_{22}-X_{12}^2)=-1$.

		Notons $(A_n)_{ij}$ la matrice obtenue à partir de $A_n$ en supprimant la ligne i et la colonne j, puis $(A_n)_{ijkl}$la matrice obtenue à partir de $A_n$ en supprimant les lignes i,k et les colonnes j,l. On a par développement par rapport à une ligne puis une colonne :

		$$
		\begin{align*}
		\det(A_n)&=\sum_{j=1}^nX_{1j}\det((A_n)_{1j})\\
		&=X_{11}\det((A_n)_{11}+\sum_{j=1}^{n}\sum_{i=2}^nX_{1j}X_{i1}\det((A_n)_{1ji1})\\
		\end{align*}
		$$

		Or $X_{11}$ et $\det((A_n)_{11})$ sont indépendantes, de même $(X_{1j}X_{i1})$ et $\det((A_n)_{1ji1})$ sont indépendantes car on a supprimé la première ligne et la première colonne donc les variables aléatoire $X_{1j}$ et $X_{i1}$ n'apparaissent plus dans $(A_n)_{1ji1}$. On en déduit :

		$$
		\Delta_{2n+1}=\mathbb E(\det(A_n))=\mathbb E(X_{11})\mathbb E(\det((A_n)_{11})+\sum_{j=1}^{n}\sum_{i=2}^n\mathbb E(X_{1j}X_{i1})\mathbb E(\det((A_n)_{1ji1}))=0+\sum_{j=1}^{n}\sum_{i=2}^n\mathbb E(X_{1j}X_{i1})\Delta_{2n-1}
		$$

		On montre ainsi par récurrence (puisque $\Delta_1=0$) que $\Delta_{2n+1}=0$.

		Plaçons nous dans la cas pair.

		Supposons que pour tout $i\in\{1,\dots,2n\}$, $\sigma^2(i)=i$ (c'est à dire $\sigma^2=id$). Alors $\sigma'=\sigma\circ(1\sigma(1))$ laisse $1$ et $\sigma(1)$ invariants. $\sigma'$ est donc une permutation d'un ensemble à $2n-2$ éléments ne contenant ni $1$ ni $\sigma(1)$ et vérifiant $\sigma'^2=id$. En itérant le processus, on montre que $\sigma$ est un produit de $n$ transpositions disjointes.

		Réciproquement, si $\sigma$ est un produit de $n$ transpostions disjointes, il est clairque $\sigma^2=id$.


		Appuyons nous sur la définition du déterminant :

		$$
		\det(A_{2n})=\sum_{\sigma\in S_{2n}}\varepsilon(\sigma)\prod_{i=1}^{2n}X_{i\sigma(i)}.
		$$


		Dans cette somme, si $\sigma$ est un produit de $n$ transpositions disjointes $\sigma=\tau_1\tau_2\dots\tau_n$, en notant $\tau_k=(i_kj_k)$ avec $i_k<j_k$, on obtient $\prod_{i=1}^{2n}X_{i\sigma(i)}=\prod_{i=1}^{n}X_{i_kj_k}^2$ et donc $\mathbb E(\prod_{i=1}^{2n}X_{i\sigma(i)})=\prod_{i=1}^{n}\mathbb E(X_{i_kj_k}^2)=1$.


		Mais si $\sigma^2\ne id$, alors il existe $i_0$ tel que $\sigma(\sigma(i_0))\ne i$. Alors la variable aléatoire $X_{\sigma(i_0)i_0}$ n'apparaîtra pas dans $\prod_{i=1}^{2n}X_{i\sigma(i)}$ On aura alors $X_{i_0\sigma(i_0)}$ et  $\prod_{i=1,i\ne i_0}^{2n}X_{i\sigma(i)}$ qui seront indépendantes. Alors $\mathbb E(\prod_{i=1}^{2n}X_{i\sigma(i)})=\mathbb E(X_{i_0\sigma(i_0)})\mathbb E(\prod_{i=1,i\ne i_0}^{2n}X_{i\sigma(i)})=0$.

		Finalement, si on note $T_n$ l'ensemble des permutations vérifiant $\sigma^2=id$, on a:

		$$
		\mathbb E(\det(A_{2n}))=\sum_{\sigma\in T_n}\varepsilon(\sigma)\mathbb E(\prod_{i=1,i\ne i_0}^{2n}X_{i\sigma(i)})+\sum_{\sigma\notin T_n}\varepsilon(\sigma)\mathbb E(\prod_{i=1,i\ne i_0}^{2n}X_{i\sigma(i)})=\sum_{\sigma\in T_n}\varepsilon(\sigma)=(-1)^ncard(T_n).
		$$

		Or pour construire une permutation de $T_n$, il faut choisir les couples $(i,j)$ avec $i<j$ qui formeront les transpositions. Il y a $2n(2n-1)$ façons de choisir le premier couple, mais on veut l'ordonner donc on a $n(2n-1)$ façon de construire la première transposition. Puis $(n-1)(2n-3)$ pour la deuxième et ainsi de suite. Au total cela donne $n!(2n-1)\times(2n-3)\times\cdots\times 3\times1$ façon de construire les $n$ transpositions, mais comme elles sont disjointes, l'ordre dans l'écriture de $\sigma$ n'a pas d'importance. Cela donne donc $(2n-1)\times\cdots\times 3\times1$ éléments dans $T_n$. On en déduit que

		$$
		\Delta_{2n}=(-1)^n\times1\times3\times\cdots\times(2n-1).
		$$




!!! exercice "Corrigé <a name="corrige-67"></a><a href="#enonce-67">67</a>"

		```python
		import numpy as np
		def matrice(a,mu,alpha) :
		    n = len(a)
		    mat = np.zeros(n,n)
		    for k in range(n-1) :
		        mat[k,k+1] = 1
		        mat[k,k] = a[k]
		        mat[n,k] = mu[k]
		    mat[n,n]=alpha
		    return mat


		```

		On s'intéresse au problème $FL(2)$. Prenons une matrice dont les coefficients diagonaux sont $a_0$ et $a_1$ : $A=\begin{pmatrix} a_0 & b \\ c & a_1\end{pmatrix}$.

		Supposons que $\lambda_0$ et $\lambda_1$ soient les valeurs propres.
		On sait que le polynôme caractéristique s'écrit alors $X^2-\mathrm{tr}(A)X+\det(A)$. On en déduit en particulier, d'après les relations coefficients racines que $a_0+a_1=\lambda_0+\lambda_1$.

		Réciproquement, si $a_0+a_1=\lambda_0=\lambda_1$, montrons que l'on peut choisir $b$ et $c$ tels que $\lambda_0$ et $\lambda_1$ soient les valers propres. On a $\chi_A=X^2-(a_0+a_1)X+(a_0a_1-bc)=X^2-(\lambda_0+\lambda_1)X+(a_0a_1-bc)$. On cherche donc $b$ et $c$ tels que $a_0a_1-bc=\lambda_0\lambda_1$. Il suffit de prendre $c=1$ et $b=a_0a_1-\lambda_0\lambda_1$.

		Si le problème $FL(3)$ admet une solution, il est nécessaire que $a_0+a_1+a_2=\lambda_0+\lambda_1+\lambda_2$. Or ici $a_0+a_1+a_2=0$ et $\lambda_0+\lambda_1+\lambda_2=0$. Donc ce n'est pas discriminant.
		Essayons d'utiliser la matrice proposée ci-dessus. Cherchons $\mu_0$ et $\mu_1$ tels que $A=\begin{pmatrix} 1 & 1 & 0 \\ 0 & -1 & 1 \\ \mu_0 & \mu_1 & 0\end{pmatrix}$ admette $1,2,-3$ comme racines. On a $\chi_A=X^3 - (1+\mu_1)X+\mu_1-\mu_0$. La relation $\chi_A(1)=\chi_A(2)=\chi_A(-3)=0$ mène au système équivalent $\begin{cases}-\mu_0=0\\6-\mu_1-\mu_0=0\\-24+4\mu_1-\mu_0=0\end{cases}$ dont l'unique solution est $\mu_0=0$ et $\mu_1=6$. Donc la matrice $\begin{pmatrix} 1 & 1 & 0 \\ 0 & -1 & 1 \\ 0 & 6 & 0\end{pmatrix}$ semble être une solution.


		```python
		import numpy.linalg as alg
		```


		```python
		alg.eigvals([[1,1,0],[0,-1,1],[0,6,0]])
		```




		    array([ 1., -3.,  2.])



		$(P_0,P_1,\dots,P_n)$ est une famille de $n$ polynômes de degrés deux à deux distincts. Elle est donc libre et contient $n+1$ polynômes : c'est une base de $\mathbb R_n[X]$.

		Si l'on note $Q$ le polynôme $\prod_{i=0}^{n-1}(X-\lambda_i)$ alors il existe $\alpha_0,\dots,\alpha_n$ tels que $Q_k=\alpha_0P_0+\dots+\alpha_{n-1}P_{n-1}+\alpha_nP_n$. En considérant le coefficient de plus haut degré, on a $\alpha_n=1$ et  en posant alors $\mu_i=-\alpha_i$ on a la relation demandée.




		```python
		from numpy.polynomial import Polynomial
		```


		```python
		def famille_Pi(a) :
		    n = len(a)
		    P=[Polynomial([1])]
		    for i in range(1,n+1) :
		        P.append(P[-1]*Polynomial([-a[i-1],1]))
		    return P
		```


		```python
		famille_Pi([1,2,3])
		```




		    [Polynomial([1.], domain=[-1,  1], window=[-1,  1]),
		     Polynomial([-1.,  1.], domain=[-1.,  1.], window=[-1.,  1.]),
		     Polynomial([ 2., -3.,  1.], domain=[-1.,  1.], window=[-1.,  1.]),
		     Polynomial([-6., 11., -6.,  1.], domain=[-1.,  1.], window=[-1.,  1.])]



		En considérant l'égalité $Q=P_n-\sum_{i=0}^{n-1} \mu_iP_i$, et en prenant successivement les valeurs en $a_0$, $a_1$, $\dots$, $a_n$, on obtient la relation $\mu_k=-\frac{Q(a_k)+\sum_{i=0}^{k-1}\mu_iP_i(a_k)}{P_k(a_k)}$. On écrit donc une fonction python en conséquence :


		```python
		def coeffs_mu (a,lamb) :
		    n=len(a)
		    P = famille_Pi(a)
		    Q = famille_Pi(lamb)[-1]
		    mu=[Q(a[0])]
		    for i in range(1,n) :
		        mu.append(-(Q(a[i])+sum([mu[k]*P[k](a[i]) for k in range(i)]))/P[i](a[i]))
		    return mu
		```


		```python
		coeffs_mu([1,-1,0],[1,2,-3])
		```




		    [0.0, 6.0, 0.0]



		On peut vérifier que ces coefficients marchent bien.

		*Remarque* : on a répondu aux questions de l'exercice, mais c'est peu satisfaisant : on ne sait pas si le problème FL(n) possède des solutions ou non !




!!! exercice "Corrigé <a name="corrige-68"></a><a href="#enonce-68">68</a>"
		```python
		from numpy import sin,pi
		from numpy import random as rd
		```


		```python
		def u(n) :
		    L = [rd.rand()*pi]
		    for i  in range(1,n+1) :
		        somme = 0
		        for k in range(i) :
		            somme += sin(L[k]/i)
		        L.append(somme)
		    return L
		```


		```python
		u(100)
		```




		    [2.2608772684234584,
		     0.7711944901145663,
		     1.2807118454732385,
		     1.352584733085721,
		     1.3736817554299279,
		     1.3824249381349878,
		     1.3868217911271525,
		     1.3893252496306001,
		     1.390879450399448,
		     1.3919077979998946,
		     1.3926222703026903,
		     1.3931382157946035,
		     1.3935226404697414,
		     1.3938165729146907,
		     1.3940462487263632,
		     1.3942290648809603,
		     1.3943769192712105,
		     1.3944981681180253,
		     1.3945988174730646,
		     1.394683272673475,
		     1.3947548234783178,
		     1.3948159661704334,
		     1.3948686222916533,
		     1.394914290214045,
		     1.3949541520988076,
		     1.3949891506275174,
		     1.395020044879655,
		     1.3950474515850737,
		     1.3950718759643004,
		     1.3950937350528843,
		     1.3951133755309086,
		     1.3951310874876952,
		     1.3951471151465424,
		     1.3951616652927465,
		     1.3951749139499192,
		     1.3951870117084073,
		     1.3951980880078878,
		     1.3952082546021882,
		     1.3952176083799428,
		     1.3952262336743333,
		     1.3952342041649517,
		     1.3952415844520272,
		     1.3952484313659455,
		     1.3952547950617036,
		     1.3952607199377294,
		     1.39526624541053,
		     1.3952714065704566,
		     1.3952762347389676,
		     1.3952807579439497,
		     1.395285001326564,
		     1.3952889874906738,
		     1.3952927368039183,
		     1.3952962676579304,
		     1.3952995966939172,
		     1.3953027389987636,
		     1.3953057082759872,
		     1.3953085169951556,
		     1.3953111765228174,
		     1.395313697237511,
		     1.3953160886310254,
		     1.3953183593977638,
		     1.3953205175137713,
		     1.3953225703067798,
		     1.3953245245184165,
		     1.3953263863595582,
		     1.3953281615596733,
		     1.3953298554109137,
		     1.3953314728075512,
		     1.3953330182813346,
		     1.3953344960332361,
		     1.3953359099619957,
		     1.395337263689841,
		     1.3953385605856854,
		     1.3953398037860865,
		     1.3953409962142127,
		     1.3953421405970277,
		     1.3953432394808774,
		     1.3953442952456578,
		     1.3953453101177005,
		     1.3953462861815085,
		     1.3953472253904586,
		     1.395348129576581,
		     1.3953490004594844,
		     1.3953498396545378,
		     1.3953506486803582,
		     1.3953514289656836,
		     1.3953521818556782,
		     1.3953529086177325,
		     1.3953536104467936,
		     1.3953542884702816,
		     1.3953549437526107,
		     1.3953555772993727,
		     1.3953561900611926,
		     1.395356782937296,
		     1.3953573567788036,
		     1.395357912391789,
		     1.3953584505401124,
		     1.3953589719480417,
		     1.395359477302688,
		     1.3953599672562758,
		     1.3953604424282402]



		Il semble y avoir convergence de la suite.

		b) La question se traite bien avec des études de fonctions.

		c.i) Montrons cela par récurrence forte sur $n$. Aucun problème pour l'initialisation. Concentrons nous sur l'hérédité. On suppose que pour tout $k\le n$, $0\le u_k\le \pi$. On a alors pour tout $k\le n$, $0\le \frac{u_k}{n+1}<\pi$ et donc $0\le\sin\left(\frac {u_k}{n+1}\right)\le\frac{u_k}{n+1}\le \frac\pi{n+1}$. On a alors évidemment $u_{n+1}\ge 0$ et :

		$$
		\begin{align*}
		u_{n+1}&=\sum_{k=0}^n \sin\left(\frac{u_k}{n+1}\right)\\
		&\le \sum_{k=0}^n \frac\pi{n+1}\\
		&\le \pi
		\end{align*}
		$$

		ce qui donne l'encadrement voulu.


		c.ii) On a par ailleurs, grâce à l'encadrement de la question b) :

		$$
		\frac{u_k}{n+1}-\frac 16\frac {u_k^3}{(n+1)^3}\le\sin\left(\frac{u_k}{n+1}\right)\le \frac {u_k}{n+1}\qquad (1)
		$$

		Or $\frac{u_k^3}{(n+1)^3}\le \frac{\pi^3}{(n+1)^3}$ et donc $\sum_{k=0}^n\frac{u_k}{n+1}-\frac 16\frac {u_k^3}{(n+1)^3}\ge v_n-\frac 16\frac{\pi^3}{(n+1)^2}$.

		En sommant les inégalités $(1)$ pour $k$ de $0$ à $n$ on a donc :

		$$
		v_n-\frac{\pi^3}{6(n+1)^2}\le u_{n+1}\le v_n
		$$

		On peut donc écrire :

		$$
		-\frac{\pi^3}{6(n+1)^2}\le u_{n+1}-v_n\le 0
		$$

		Or $u_{n+1}-v_n=(n+1)v_{n+1}-nv_n - v_n=(n+1)(v_{n+1}-v_n)$. En divisant l'inégalité précédente par $n+1$ on obtient ainsi :

		$$
		-\frac{\pi^3}{6(n+1)^3}\le v_{n+1}-v_n\le 0
		$$


		c.iii) On en déduit, par théorème de comparaison des séries à termes positifs que la série $v_{n+1}-v_n$ est absolument convergente donc convergente. Or c'est une somme télescopique : la suite $v_n$ est une suite convergente. Notons $l$ sa limite, d'après la question c.i et le théorème d'encadrement, la suite $u_n$ converge vers la même limite.




!!! exercice "Corrigé <a name="corrige-69"></a><a href="#enonce-69">69</a>"
		a) Soit $x\in\mathbb R^{+*}\setminus\{1\}$. La fonction $t\mapsto \frac 1{\ln(t)}$ est continue sur $[x,x^2]$ ou $[x^2,x]$. L'intégrale est donc bien définie et donc $f$ est définie en $x$.


		```python
		import scipy.integrate as integr
		import numpy as np
		from matplotlib import pyplot as plt

		# inutile
		def integrer(f,a,b) :
		    n=100
		    somme = 0
		    for k in range(n) :
		        somme += f(a+k*(b-a)/n)
		    return somme*(b-a)/n

		def f(x) :
		    return integr.quad(lambda t:1/np.log(t),x,x*x)[0]


		xr = np.arange(0,3,0.011)
		yr = [f(x) for x in xr]
		plt.figure()
		plt.plot(xr,yr)
		plt.show()
		```

		    /tmp/ipykernel_25667/1325440301.py:14: RuntimeWarning: divide by zero encountered in log
		      return integr.quad(lambda t:1/np.log(t),x,x*x)[0]




		![png](exercices/CentralePython/rms2022-1109_files/rms2022-1109_2_1.png)



		On dirait bien que la limite en $1$ vaut $\ln(2)$.

		Soit $x\in[1,2]$, on a alors : $f(x)=\int_x^{x^2}\frac 1{\ln(t)}\mathrm dt=\int_x^{x^2}t \frac1{t\ln(t)}\mathrm dt$.
		En utilisant les inégalités de la moyenne (ici $x\le x^2$), on a alors :

		$$
		x\int_{x}^{x^2}\frac 1{t\ln(t)}\mathrm dt \le f(x)\le x^2\int_{x}^{x^2}\frac 1{t\ln(t)}\mathrm dt
		$$

		Soit encore :

		$$
		x\left( \ln\ln(x^2)-\ln\ln(x)\right)\le f(x)\le x^2\left(\ln\ln(x^2)-\ln\ln(x)\right)
		$$

		et finalement, puisque $\ln\ln(x^2)-\ln\ln(x)=\ln(2)$, on a $x\ln(2)\le f(x)\le x^2\ln(2)$.  Par théorème d'encadrement, on obtient $\lim_{x\to 1^+}f(x)=\ln(2)$.

		On peut appliquer la même relation entre $x^2$ et $x$ pour $x<1$ et obtenir la même limite.


		$f$ semble bien dérivable.

		On a pour $x\ne 1$, $f'(x)=2x\frac1{\ln(x^2)}-\frac 1{\ln(x)}=\frac {x-1}{\ln(x)}$. Or $\ln(x)=\ln(1+(x-1))\underset{x\to 1}\sim x-1$ donc $f'(x)$ converge vers $1$ en $1$. Par théorème de prolongement $\mathcal C^1$, $f$ est dérivable en $1$.


		```python
		plt.plot(xr,xr-1+np.log(2))
		plt.show()
		```



		![png](exercices/CentralePython/rms2022-1109_files/rms2022-1109_5_0.png)




!!! exercice "Corrigé <a name="corrige-70"></a><a href="#enonce-70">70</a>"

		a) La variable $T$ correspond au temps d'attente du premier arrivé.

		b) $T$ peut prendre des valeurs entre $0$ et $59$. Pour $k>0$, on a $\{T=k\}=\left(\sqcup_{i=0}^{59-k} \{X=i\cap Y=i+k\}\right)\sqcup\left(\sqcup_{i=0}^{59-k} \{X=k+i\cap Y=i\}\right)$. On en déduit que $\mathbb P(T=k)=2\sum_{i=0}^{59-k}\frac 1{60^2}=\frac{60-k}{30\times 60}$.

		Par ailleurs, $\mathbb P(T=0)=\sum_{i=0}^{59}\mathbb P(X=i\cap Y=i)=\frac 1{60}$.

		Vérifions que l'on a bien défini une probabilité : $\sum_{k=0}^{59}\mathbb P(T=k)=\frac 1{60} +\sum_{k=1}^{59}\frac1{60}-\frac k{60^2}=\frac 1{60}+\frac {59}{30}-\frac{59}{60}=1$. On est donc un peu rassurés sur ce résultat.


		```python
		from numpy import random as rd
		def rdv (n) :
		    L=[]
		    for i in range(n) :
		        X = rd.randint(0,60)
		        Y = rd.randint(0,60)
		        L.append(abs(X-Y))
		    return L

		rdv(10)
		```




		    [7, 29, 2, 10, 17, 15, 7, 32, 11, 11]




		```python
		sum(rdv(100000))/100000
		```




		    20.03062



		On observe une convergence vers quelque chose qui ressemble à $20$.

		d.i) Calculons l'espérance de $T$ :

		$$
		\begin{align*}
		\mathbb E(T)&=\sum_{k=0}^{59} k\times \mathbb P(T=k)\\
		&=\sum_{k=1}^{59} k\times \mathbb P(T=k)\\
		&=\sum_{k=1}^{59} k\times\frac{60-k}{30\times 60}\\
		&=59-\frac{59\times 60\times 119}{6\times 30\times 60}\\
		&=59\times(1-\frac {119}{180})\\
		&=59\times\frac{61}{180}\\
		&\simeq 20
		\end{align*}
		$$


		```python
		59*61/180
		```




		    19.994444444444444



		L'écart observé n'est tout d'abord pas énorme, et est en accord avec la loi des grands nombres : la moyenne calculée n'est qu'une approximation de l'espérance.


		```python
		def loi_de_T(n) :
		    L = rdv(n)
		    répartition = [0]*60
		    for i in L :
		        répartition[i]+=1
		    return [rép/n for rép in répartition]
		```


		```python
		loi_de_T(100000)
		```




		    [0.01679,
		     0.03323,
		     0.03191,
		     0.03175,
		     0.03151,
		     0.03075,
		     0.03001,
		     0.02859,
		     0.02926,
		     0.02771,
		     0.02814,
		     0.0278,
		     0.02651,
		     0.027,
		     0.02585,
		     0.02507,
		     0.0238,
		     0.02373,
		     0.02334,
		     0.02182,
		     0.02213,
		     0.02129,
		     0.02063,
		     0.01961,
		     0.02007,
		     0.019,
		     0.0197,
		     0.01757,
		     0.01776,
		     0.01743,
		     0.01662,
		     0.01676,
		     0.01628,
		     0.01445,
		     0.01442,
		     0.01325,
		     0.01407,
		     0.01261,
		     0.01275,
		     0.01152,
		     0.01111,
		     0.01048,
		     0.01009,
		     0.00933,
		     0.00878,
		     0.00884,
		     0.00792,
		     0.00725,
		     0.00636,
		     0.00648,
		     0.00568,
		     0.00496,
		     0.00469,
		     0.00402,
		     0.00359,
		     0.00257,
		     0.0022,
		     0.00149,
		     0.00115,
		     0.00052]




		```python
		1/60
		```




		    0.016666666666666666



		On a encore de légers écarts qui sont naturels dans une simulation.

		Dans cette partie, je comprends qu'on ne fait plus 60 divisions mais $n$ avec un $n$ quelconque. Je suppose que la loi suivie par l'heure d'arriver de chacun suit toujours une loi uniforme.

		On a $\mathbb P(X=Y)=\mathbb P(T=0)=\sum_{i=0}^{N-1} \mathbb P(X=i\cap Y=i)=\sum_{i=0}^{N-1}\frac 1{N^2}=\frac 1N$. On a donc un équivalent en $\frac 1N$ : plus il y a de subdivisions, plus il est rare que les deux amis arrivent en même temps.



<div style="page-break-before:always"></div>
## Sujets d'écrit

### Sujet 1 - INSA ARCHI 2021 (2h) (24 Juillet 2023)

{% include-markdown "../documents/cahier de vacances/ecrits/SujetINSA21.md" %}

<div style="page-break-before:always"></div>
### Sujet 2 - compilation d'exercices (3h) (7 Août 2023))

{% include-markdown "../documents/cahier de vacances/ecrits/sujet2.md" %}

<div style="page-break-before:always"></div>
### Sujet 3 - TPC 2004 (4h) (21 Août 2023)

{% include-markdown "../documents/cahier de vacances/ecrits/sujet3-TPC2004.md" %}
