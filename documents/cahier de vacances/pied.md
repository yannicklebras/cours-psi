
<div style="page-break-before:always"></div>
## Sujets d'écrit

### Sujet 1 - INSA ARCHI 2021 (2h) (24 Juillet 2023)

{% include-markdown "../documents/cahier de vacances/ecrits/SujetINSA21.md" %}

<div style="page-break-before:always"></div>
### Sujet 2 - compilation d'exercices (3h) (7 Août 2023))

{% include-markdown "../documents/cahier de vacances/ecrits/sujet2.md" %}

<div style="page-break-before:always"></div>
### Sujet 3 - TPC 2004 (4h) (21 Août 2023)

{% include-markdown "../documents/cahier de vacances/ecrits/sujet3-TPC2004.md" %}
