fichier = open("banqueAdmon.md","r")

exercices = fichier.read()
fichier.close()


exercices = exercices.split("!!!")

inclusion=""

i=1
bis =True
for exo in exercices[1:] :
	if i == 59 and bis :
		nom = "ccinp2023-058b.md"
		bis = False
	else :
		nom = "ccinp2023-%0.3d.md"%(i)
		i+=1
	fichier_exo = open(nom,"w")
	fichier_exo.write("!!!"+exo)
	inclusion += "\n"+"{% include-markdown \"../../exercicesBanquesCCINP/"+nom+"\" %}\n"
	fichier_exo.close()

fichier_inclusions=open("banqueCCINP.md","w")
fichier_inclusions.write("""---
title: Banque CCINP 2023
tags :
  - banque ccinp
---\n
""")

fichier_inclusions.write(inclusion)
fichier_inclusions.close()
