!!! exercice "RMS2022-909"
	=== "Enoncé"
		a) Déterminer le domaine de définition de $f : x \mapsto \sum_{n=2}^{+\infty} \frac{x\,  e^{-nx}}{\ln (n)}\cdot$  
	 	b) Montrer que $f$ est de classe $\mathcal{C}^1$ sur $\R_+^*$.  
	 	c) Montrer que $f$ n'est pas dérivable en $0$.


	=== "Corrigé"
		Posons $f_n(x)=x\frac{e^{-nx}}{\ln(n)}$.

		a) Pour $x< 0$, le terme général ne tend pas vers $0$ par croissances comparées. Donc il y a divergence grossière. Pour $x=0$ la série est nulle. Et pour $x>0$, $n^2x\frac{e^{-nx}}{\ln(n)}\xrightarrow[n\to+\infty]{}0$ donc $f_n(x)=o\left(\frac 1{n^2}\right)$ et par théorèmes de comparaison, la série $\sum f_n(x)$ converge. L'ensemble de définition est donc $\mathbb R^+$.   
		b) On a montré la convergence simple de $\sum f_n$ sur $\mathbb R^+_*$. De plus les $f_n$ sont de classe $\mathcal C^1$ et $f_n'(x)=\frac{e^{-nx}}{\ln(n)}(1-xn)$. Soit $a\in\mathbb R^+_*$ et $x>a$. Pour $n$ tel que $\frac 1n<a$, la fonction $f_n'$ est négative et croissante sur $[a,+\infty[$. On a alors $|f_n'(x)|\le\frac{e^{-na}}{\ln(n)}(an-1)=o\left(\frac 1{n^2}\right)$ et on a une majoration uniforme sur $[a,+\infty[$ des $f_n'$ à partir d'un certain rang. On en déduit que $\sum f_n'$ converge normalement sur $[a,+\infty[$ et donc uniformément. Par théorème de dérivation, la fonction $f$ est de classe $\mathcal C^1$ sur tout $[a,+\infty[$ et donc de classe $\mathcal C^1$ sur $R^+_*$.   
		c) On souhaite montrer que $f$ n'est pas dérivable en $0$. On passe par le taux d'accroissement : $\frac{f(x)-f(0)}{x-0}=\sum_{n\ge2}\frac{e^{-nx}}{\ln(n)}$ et on voudrait montrer que cette quantité ne converge pas en $0$. Pour cela, effectuons une comparaison série/intégrale. Pour $x>0$ donné, la fonction $t\mapsto \frac{e^{-tx}}{\ln(t)}$ est décroissante donc $\frac{e^{-nx}}{\ln(n)}\ge\int_{n}^{n+1}\frac{e^{-tx}}{\ln(t)}\d t$. On a par ailleurs pour un entier $N$ donné :

		$$
		\frac {f(x)}{x}\ge \sum_{n=2}^{N}\frac{e^{-nx}}{\ln(n)}\ge \int_2^{N+1}\frac{e^{-nx}}{\ln(n)}\d t.
		$$  

		Prenons en particulier $x_N=\frac 1{N+1}$ :

		$$
		\begin{aligned}
		\int_2^{N+1}\frac{e^{-nx_N}}{\ln(n)}\d t&\underset{u=\frac t{N+1}}= (N+1)\int_{\frac 2{N+1}}^{1}\frac{e^{-u}}{\ln((N+1)u)}\d u\\
		&\underset{0<\ln((N+1)u)<\ln(N+1)}\ge \frac{N+1}{\ln(N+1)}\int_{\frac 2{N+1}}^1 e^{-u}\d u\\
		&\ge  \frac{N+1}{\ln(N+1)}(e^{-\frac 2{N+1}}-e^{-1})
		&\xrightarrow[N\to+\infty]{}+\infty
		\end{aligned}
		$$

		On en déduit que $\frac{f(x_N)}{x_N}\xrightarrow[N\to+\infty]{}+\infty$ et donc $\frac{f(x)-f(0)}{x-0}$ n'admet pas de limite finie en $0$ : $f$ n'est pas dérivable en $0$.
