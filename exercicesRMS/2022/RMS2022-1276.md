!!! exercice "RMS2022-1276"  
	=== "Enoncé"  
		   **CCINP**  .   Calculer les dimensions de $\mathcal{A}_n(\K)$ et $\mathcal{S}_n(\K)$, sous-espaces des matrices antisymétriques et symétriques de ${\cal M}_n(\K)$. En déduire le déterminant de l'endomorphisme $u$ de $\mathcal{M}_n(\K)$ défini par $u(M)=M^{T }$.


	=== "Corrige"  

		On vérifie assez facilement que $A_n(\K)$ est engendré par les $E_{ij}-E_{ji}$ pour $i<j$. Cette famille étant évidemment libre, la dimension de $A_n(\K)$ est $\frac{n(n-1)}2$.

		De même $S_n(\K)$ est engendrée par les $E_{ij}+E_{ji}$ pour $i<j$ et les $E_{ii}$. Cette famille est une famille libre donc la dimension de $S_n(\K)$ est $\frac{n(n+1)}{2}$.

		On sait de plus que $A_n(\K)$ et $S_n(\K)$ sont supplémentaires dans $M_n(\K)$. Dans une base adaptée à cette décomposition, la matrice de l'application transposée est diagonale avec $\frac{n(n+1)}{2}$ nombres $1$ et $\frac{n(n-1)}{2}$ nombres $-1$. Le déterminant étant indépendant de la base de représentation, le déterminant de l'application transposeé est donc $(-1)^{\frac{n(n-1)}{2}}$.


