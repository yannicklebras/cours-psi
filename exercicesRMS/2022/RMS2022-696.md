!!! exercice "RMS2022-696"
	=== "Enoncé"
		Soit $J_n$ la matrice carrée de taille $n$ dont tous les coefficients valent $1$.

		a) Montrer que $J_n$ est diagonalisable et qu'il existe une matrice orthogonale $P_n\in{\cal O}_n(\mathbb R)$
		telle que : $J_n=P_n \mathrm{diag}(0,\ldots,0,n)P_n^{-1}$.

		b) Calculer $P_2$ et $P_3$.

		c) Montrer que l'espace vectoriel des matrices de ${\cal M}_3(\mathbb R)$ commutant avec $J_3$ est de dimension $5$.




	=== "Corrigé"
		a) une remarque classique est que $J_n^2=nJ_n$. Donc $J_n$ annule le polynôme caractéristique $X^2-nX$ : ainsi $J_n$ annule un polynôme scindé à racines simples, elle est diagonalisable, de valeurs propres $0$ et $n$. $J_n$ étant clairement de rang $1$, $0$ est de multiplicité $n-1$ et $n$ de multiplicité $1$. Or $J_n$ est symétrique, donc diagonalisable dans une base orthonormée : il existe $P\in\mathcal O_n$ telle que $P^{-1}J_nP=\mathrm{diag}(0,\ldots,0,n)$. On remarque par ailleurs que le vecteur $\begin{pmatrix}1\\\vdots\\1\end{pmatrix}$ est un vecteur propre associé à la valeur propre $n$ et que les vecteurs $\begin{pmatrix}0\\\vdots\\0\\1\\-1\\0\\\vdots\\0\end{pmatrix}$ sont dans le noyau de $J_n$. Ces vecteurs forment clairement une base de $\mathcal M_{n,1}(\mathbb R)$.

		b) D'après la remarque précédente, on a $P_2=\frac 1{\sqrt2}\begin{pmatrix}1 & 1 \\ -1 & 1\end{pmatrix}$ (la matrice de vecteurs propres est déjà orthogonale). Pour $P_3$, les deux espaces propres sont orthogonaux. On doit donc juste orthogonaliser la famille $\begin{pmatrix}1\\-1\\0\end{pmatrix},\begin{pmatrix}0\\1\\-1\end{pmatrix}$ ce qui donne $\begin{pmatrix}1\\-1\\0\end{pmatrix},\begin{pmatrix}\frac 12\\\frac 12\\-1\end{pmatrix}$. La matrice $P_3$ peut donc être  $P_3=\begin{pmatrix} 1/\sqrt2 & 1/\sqrt6 & 1/\sqrt3\\-1\sqrt2 & 1/\sqrt6 & 1/\sqrt3 \\ 0 & -2/\sqrt6 & 1/\sqrt3\end{pmatrix}$.

		c) Notons POur une matrice $B$ notons $\mathcal C(B)=\{A\in\mathcal M_3(\mathbb R)| AB=BA\}$ : $\mathcal C(B)$ est un sous espace vectoriel de $\mathcal M_3(\mathbb R)$. On peut montrer que $\mathcal C(J_3)$ est isomorphe à $\mathcal C(D_3)$ où $D_3=\mathrm{diag}(0,0,3)$. On va donc déterminer la dimension $\mathcal C(D_3)$ : les espaces étant isomorphes, ils ont même dimension.
		Soit $E_{ij}$ les élements de la base canonique de $\mathcal M_3(\mathbb R)$. Par le calcul on montre $E_{11},E_{22},E_{12},E_{21},E_{33}$ sont des éléments de $\mathcal C(D_3)$. Ces éléments forment une famille libre donc la dimension de $\mathcal C(D_3)$ est au moins de $5$. Soit $A\in\mathcal C(D_3)$. Posons $A=\begin{pmatrix}a_{11}&a_{12}&a_{13}\\a_{21}&a_{22}&a_{23}\\a_{31}&a_{32}&a_{33}\end{pmatrix}$, la relation $AD_3=D_3A$ donne $a_{13}=a_{23}=a_{31}=a_{32}=0$. On en déduit que $A\in\mathrm{Vect}(  E_{11},E_{22},E_{12},E_{21},E_{33})$. La famille est donc génératrice de $\mathcal C(D_3)$. Elle est bien une base et la dimension de $\mathcal C(D_3)$ est donc $5$. On en déduit que $\mathcal C(J_3)$ est de dimension $5$.




