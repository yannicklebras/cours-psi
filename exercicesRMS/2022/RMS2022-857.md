!!! exercice "RMS2022-857"  
    === "Enoncé"  
        Soient $(E,\|\|)$ un espace vectoriel normé de dimension finie et $A$ une partie non vide de $E$. Pour tout $x \in E$, on appelle distance de $x$ à $A$ le nombre noté $d(x, A)$ défini par $d(x, A)=\inf _{a \in A}\|x-a\|$. Pour $R>0$, on note $A(R)$ l'ensemble $\{x \in E, d(x, A) \leqslant R\}$. Montrer que si $A$ est convexe alors $A(R)$ est convexe et fermé.

	=== "Corrige"  
        Supposons donc $A$ convexe. Soient $x$ et $y$ deux éléments de $A(R)$ et $a_x$ et $a_y$ deux éléments de $A$. Soit $\lambda\in[0,1]$, on pose $z=\lambda x+(1-\lambda) y$ et $a_z=\lambda a_x+(1-\lambda)a_y$. $A$ étant convexe, $a_z\in A$. On peut donc écrire : 

        $$
        d(z,A)\le ||z-a_z||=||\lambda x+(1-\lambda)y-\lambda a_x-(1-\lambda)a_y||=||\lambda(x-a_x)+(1-\lambda)(y-a_y)||\le\lambda||x-a_x||+(1-\lambda)||y-a_y||
        $$

        On a donc $d(z,A)\le \lambda||x-a_x||+(1-\lambda)||y-a_y||$. Cette minoration étant indépendante de$a_x$ et $a_y$ on peut passer à la borne inf à droite en $a_x$ et $a_y$ : $d(z,A)\le \lambda d(x,A)+(1-\lambda) d(y,A)\le \lambda R+(1-\lambda) R=R$. Donc $z\in A(R)$ et $A(R)$ est convexe. 

        Montrons que $A(R)$ est fermé. Soit $x_n$ une suite d'éléments de $A(R)$ qui converge vers $x$, montrons que $x\in A(R)$. C'est en fait direct puisqu'on a montré en cours que le distance à une partie était continue. Ainsi, $d(x_n,A)\to d(x,A)$. Or pour tout $n$, $d(x_n,A)\le R$ donc par passage à la limite dans une inégalité, $d(x,A)\le R$ et $x\in A(R)$ : $A(R)$ est fermé.
