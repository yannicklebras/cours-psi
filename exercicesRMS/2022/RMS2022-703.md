!!! exercice "RMS2022-703"
	=== "Enoncé"
		Soit $q:(x_0,\ldots, x_{n-1})\in\mathbb R^n\mapsto x_0^2-\sum_{k=1}^{n-1}x_k^2$.

		Soit  $G=\left\{f\in\mathrm{GL}(\mathbb R^n)\, ;\ \forall x\in\mathbb R^n,\ q(f(x))=q(x)\right\}.$

		a)   Montrer que $G$ est stable par composition et passage à l'inverse.

		b)   Soit $\beta:(x,y)\mapsto \frac12(q(x+y)-q(x)-q(y))$. Montrer que $\beta$ est une forme bilinéaire symétrique sur $\mathbb R^n$.

		c)   Soient $J=\mathrm{Diag}  (-1,1,\ldots,1)$, $f\in \mathrm{GL}(\mathbb R^n)$ et $A$ la matrice de $f$ dans la base canonique de $\mathbb R^n$. Montrer que $f\in G$ si et seulement si  ${A}^TJA=J$.



	=== "Corrigé"
		a) Soit $f$ et $g$ dans $G$. Soit $x\in E$. On a $q(f\circ g(x))=q(f(g(x)))=q(g(x))=q(x)$ donc $f\circ g\in G$ : $G$ est stable par composition. Par ailleurs, si $f$ est dans $G$ alors $f$ est inversible et $q(f^{-1}(x)=q(f(f^{-1}(x)))=q(x)$. Donc $G$ est stable par inverse.

		b) Pour le bien des calculs, essayons de simplfier l'écriture de $b(x,y)$ :

		$$\begin{align*}
		q(x+y)-q(x)-q(y)&=(x-0+y_0)^2-\sum_{k=1}^{n-1}(x_k-y_k)^2-q(x)-q(y)\\
		&=x_0^2+2x_0y_0+y_0^2-\sum_{k=1}^{n-1}x_k^2+y_k^2+2x_ky_k-q(x)-q(y)\\
		&=2x_0y_0-2\sum_{k=1}^{n-1}x_ky_k
		\end{align*}
		$$

		Sous cette forme, la bilinéarité est évidente.

		c)

		Notons $B$ la matrice définie par : $B_{ij}=b(e_i,e_j)$. La matrice $B$ est donc symétrique. Soit $X$ la matrice de $x$ dans la base canonique et $Y$ la matrice de $y$ dans la base canonique. Alors $b(x,y)=X^\top BY$. Or $b(e_i,e_j)=\begin{cases} 0&\text{si }i\ne j\\1&\text{si }i=0=j\\-1&\text{si }i=j\ne0\end{cases}$. Donc $B=-J$. Notons $C_1,\dots,C_n$ les colonnes de $A$, Alors $(A^\top JA)_{ij}=-b(C_i,C_j)=-b(f(e_i),f(e_j))$. On en déduit que $A^\top JA=J$ ssi pour tout couple $(i,j)$, b(f(e_i),f(e_j))=\begin{cases} 0&\text{si }i\ne j\\-1&\text{si }i=j=0\\1&\text{si }i=j\ne0\end{cases}=b(e_i,e_j)$.

		Par ailleurs, si $q(f(x))=q(x)$ pour tout $x$ alors $b(f(e_i),f(e_j))=\frac 12(q(f(e_i+e_j))-q(f(e_i))-q(f(e_j)))=\frac 12(q(e_i+e_j)-q(e_i)-q(e_j))=b(e_i,e_j)$.  Réciproquement, si pour tout couple $i,j$ ,$b(e_i,e_j)=b(f(e_i),f(e_j))$ alors par bilinéarité $b(x,x)=b(f(x),f(x))$ c'est à dire $q(f(x))=q(x)$. On a donc au final bien l'équivalence demanc



