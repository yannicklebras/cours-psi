!!! exercice "RMS2022-1183"  
    === "Enoncé"  
        Soit $A \in \mathcal{M}_n(\mathbb{C})$.
        
        a) Montrer que $A$ est limite d'une suite de matrices diagonalisables.
        
        b) En déduire que $X_A(A)=0$.

	=== "Corrige"  
        a) $A$ étant dans $\mathcal M_n(\mathbb C)$, $A$ est trigonalisable. Posons $A=P^{-1}TP$ avec $T$ triangulaire dont les coefficients diagonaux sont $\lambda_1,\dots,\lambda_n$. Posons $A_k=P^{-1}(T-diag(\frac1k,\frac2k,\dots,\frac nk))P$. Il existe un rang $k_0$ tel que si $k>k_0$, alors pour tout couple $i\ne j$, $\lambda_i-\frac ik\ne \lambda_j-\frac jk$. A partir de ce rang, $\chi_{A_n}$ est scindé à racines simples et donc $A_k$ est diagonalisable. Or la suite $(A_k)_{k\ge k_0}$ converge vers $A$, c'est donc une suite de matrices diagonalisables qui converge vers $A$. 

        b) Soit $\varphi$ définie sur $\mathbb C[x]\times \mathcal M_n(\mathbb C)$ par $\varphi((P,M)) = P(M)$. $\varphi$ est polynômiale en les coefficients de $P$ et $M$ donc elle est continue. Ainsi, si $P_k$ converge vers $P$ et si $A_k$ converge vers $A$ alors $P_k(A_k)$ converge vers $P(A)$. 

        Par ailleurs, si $B$ est diagonalisable, $\chi_B(B)=0$. En effet si on écrit $B=P^{-1}\begin{pmatrix}\lambda _1 & & \\& \ddots & \\ & & \lambda_n\end{pmatrix}P$ alors $\chi_B(B)=P^{-1}\begin{pmatrix}\chi_B(\lambda _1) & & \\& \ddots & \\ & & \chi_B(\lambda_n)\end{pmatrix}P=0$ car les racines de $\chi_B$ sont ses valeurs propres. Ainsi avec nos notations, pour tout $k$, $\chi_{A_k}(A_k)=0$. Or $\chi_{A_k}\to \chi_A$ (l'application $M\mapsto \chi_M$ est polynômiale en les coefficients de $M$ donc continue) et $A_k\to A$ donc $\chi_{A_k}(A_k)\to \chi_A(A)$. On en déduit, par unicité de la limite, que $\chi_A(A)=0$. 