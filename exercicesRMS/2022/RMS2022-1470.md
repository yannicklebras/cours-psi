!!! exercice "RMS2022-1470"  
	=== "Enoncé"  
		  **CCINP**  .
		On étudie sur $]0, 1]$ l'équation différentielle $(E): x^2 y'' + 4 x y' + 2 y = \frac{1}{x\sqrt{x}}$.


		 - a)
		Donner les solutions de l'équation homogène de la forme $x \mapsto x^{\alpha}$.

		 - b)
		Chercher les solutions de $(E)$ sous la forme $x \mapsto \frac{z(x)}{x^2}$.

	=== "Corrige"  

		Soit $f(x)=x^\alpha$ une solution de $(E_0)$. Alors $\alpha(\alpha-1)x^\alpha+4\alpha x^\alpha+2x^\alpha = (\alpha^2+3\alpha+2)x^\alpha= 0$. On en déduit que $\alpha = -1$ ou $\alpha=-2$. On obtient alors deux solutions libres : c'est une base de l'ensemble des solutions de l'équation homogène.

		Cherchons maintenant une solution de $(E)$ $y$ sous la forme $y(x)=\frac{z(x)}{x^2}$ c'est à dire $z(x)=x^2y(x)$. On a alors $z'(x)=2xy(x)+x^2y'(x)$ et $z''(x)=2y(x)+4xy'(x)+x^2y''(x)$. L'équation $(E)$ devient $z''(x) = \frac1{x\sqrt x}$. Une solution particulière est donc $z(x)=-4\sqrt x$ et donc $y(x)=\frac {-4}{x\sqrt x}$.

		On en déduit que l'ensemble des solutions de $(E)$ est $y(x)=\frac {-4}{x\sqrt x}+\frac ax+\frac b{x^2}$.
