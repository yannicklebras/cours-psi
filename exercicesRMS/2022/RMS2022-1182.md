!!! exercice "RMS2022-1182"  
    === "Enoncé"  
        On note $E:=\left\{f \in \mathcal{C}^2([0,1], \mathbf{R}): f(0)=f(1)=0\right\}$.

        a) Montrer que $E$ est un sous-espace vectoriel de $\mathcal{C}^2([0,1], \mathbb{R})$.

        b) Déterminer les réels a pour lesquels $N_a: \hbar \mapsto \int_0^1\left|f^{\prime \prime}+a f\right|$ est une norme sur $E$.

        c) Lorsque $N_a$ est une norme sur $E$, déterminer s'il existe des réels $\alpha>0$ et $\beta>0$ tels que $\forall f \in E, \alpha\|f\|_{\infty} \leq N_a(f) \leq \beta\|f\|_{\infty}$.

	=== "Corrige"  
        a) Le fait que ce soit un sev ne devrait pas poser de problème.

        b) Les propriétés d'homogénéité et d'inégalité triangulaire découlent de celles de la valeur absolue et de la linéarité de l'intégrale. Le point décisif, c'est donc le caractère défini positif qui va être important. 

        On veut donc que $N_a(f)=0$ implique $f=0$. Par stricte positivité de l'intégrale, $N_a(f)=0$ implique que $f$ doit être solution de l'équation différentielle homogène $y''+ay=0$. Notons $\delta\in\mathbb C$ tel que $\delta^2=-a$. les solutions de cette équation sont de la forme $y(x)=\alpha e^{\delta x}+\beta e^{-\delta x}$. Avec la contrainte $f(0)=0$ on obtient $y(x)=\alpha(e^{\delta x}-e^{-\delta x}$ et la contrainte $f(1)=0$ indique que $\alpha(e^\delta-e^{-\delta})=0$ Pour être certain que cela entraîne $\alpha=0$ il est nécessaire d'avoir $e^\delta-e^{-\delta}\ne 0$ c'est à dire $\delta\ne ik\pi$ et donc $a\ne k^2\pi^2$, $k\in\mathbb Z$. 

        Et en effet, si $a$ est de cette forme, alors la fonction $y:x\mapsto \sin(k\pi x)$ vérifie $N_a(y)=0$ sans avoir $y=0$. 

        c) Prenons donc $a\ne k^2\pi^2$. Soit $k\in\mathbb N$, on pose $f_k(x)=\sin(k\pi x)$. On a alors $||f||\infty=1$ et $N_a(f)=\int_0^1 |(k^2\pi^2-a)\sin(k\pi t)|\d t=|k^2\pi^2-a|\int_0^1|\sin(k\pi t)|\d t$. Or :

        $$
        \begin{aligned}
        \int_0^1|\sin(k\pi t)|\d t&=\frac 1{k\pi}\int_0^{k\pi}|\sin(u)|\d u\\
        &=\frac 1{k\pi}\sum_{p=0}^{k-1}\int_{p\pi}^{(p+1)\pi}(-1)^p\sin(u)\d u\\
        &=\frac 1{k\pi}\sum_{p=0}^{k-1}(-1)^p[-\cos(u)]_{p\pi}^{(p+1)\pi}\\
        &=\frac 1{k\pi}\sum_{p=0}^{k-1}2\\
        &=\frac 2\pi
        \end{aligned}
        $$

        On a donc $N_a(f)=\frac 2\pi |k^2\pi^2-a|\xrightarrow[k\to+\infty]{}+\infty$ et on ne peut donc pas trouver $\beta$ tel que pour tout $f$ on ait $N_a(f)\le \beta ||f||_\infty$. 

        L'autre inégalité me semble vraie, mais démontrable avec des notions hors programme.