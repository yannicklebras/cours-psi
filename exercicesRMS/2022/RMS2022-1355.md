!!! exercice "RMS2022-1355"
    === "Énoncé"
        On considère la matrice $A=\left(\begin{array}{ccc}3 & -3 & 2 \\ -1 & 5 & -2 \\ -1 & 3 & 0\end{array}\right)$.
        
        a) La matrice $A$ est-elle diagonalisable ? Déterminer ses éléments propres.
        
        b) Déterminer une matrice $R$ telle que $R^2=A$.
        
        c) Montrer que toutes les matrices $R$ telles que $R^2=A$ sont diagonalisables.
    === "Corrigé"
        a) On calcule le polynôme caractéristique : 

        $$
        \begin{aligned}
        \chi_A &= \begin{vmatrix} X-3& 3 & -2 \\ 1 &X-5&2\\ 1 & -3 & X\end{vmatrix}\\
        &=  \begin{vmatrix} X-2& 3 & -2 \\ X-2 &X-5&2\\ X-2 & -3 & X\end{vmatrix}&C_1\leftarrow C_1+C_2+C_3\\
        &= (X-2)\begin{vmatrix}1& 3 & -2 \\ 1 &X-5&2\\ 1 & -3 & X\end{vmatrix}\\
        &= (X-2)\begin{vmatrix}1& 0 & 0 \\ 1 &X-8&4\\ 1 & -6 & X+2\end{vmatrix}\\
        &= (X-2)\begin{vmatrix}X-8 & 4\\-6 & X+2\end{vmatrix}\\
        &= (X-2)^2(X-4)
        \end{aligned}
        $$

        On en déduit que $A$ est trigonalisable car son polynôme caractéristique est scindé. Etudions $E_2(A)$ : 

        $$
        \begin{aligned}
        AX=2X &\Leftrightarrow \begin{cases} 3x-3y+2z=2x\\ -x+5y-2z=2y\\-x+3y = 2z\end{cases}\\
        &\Leftrightarrow \begin{cases} x-3y+2z=0\\ -x+3y-2z=0\\-x+3y-2z = 0\end{cases}\\
        &\Leftrightarrow x-3y+2z = 0\\
        \end{aligned}
        $$

        C'est l'équation d'un hyperplan donc l'ensemble des solutions est de dimension $2$, engendré par les vecteurs $\begin{pmatrix} 3\\1\\0\end{pmatrix}$ et $\begin{pmatrix}2\\0\\-1\end{pmatrix}$. Ainsi la dimension du sous-espace propre $E_{2}(A)$ est égale à la multiplicité de $2$ en tant que racine du polynôme caractéristique. C'est aussi évidemment le cas pour $E_4(A)$ (dimension = multiplicité=1). Ainsi $A$ est diagonalisable. Cherchons un vecteur propre associé à la valeur propre $4$ : 

        $$
        \begin{aligned}
        AX=4X&\Leftrightarrow \begin{cases} 3x-3y+2z=4x\\ -x+5y-2z=4y\\-x+3y = 4z\end{cases}\\
        &\Leftrightarrow \begin{cases} -x-3y+2z=0\\ -x+y-2z=0\\-x+3y-4z =0\end{cases}\\
        &\Leftrightarrow \begin{cases} -x-3y+2z=0\\ +4y-4z=0\\6y-6z =0\end{cases}\\
        &\Leftrightarrow \begin{cases} -x-3y+2z=0\\ y=z\end{cases}\\
        &\Leftrightarrow \begin{cases} -x-z=0\\ y=z\end{cases}\\
        &\Leftrightarrow \begin{cases} x=-z\\ y=z\end{cases}\\        
        \end{aligned}
        $$ 

        Ainsi, les vecteurs propres associés à la valeur propre $4$ sont de la forme $\alpha\begin{pmatrix}-1\\1\\1\end{pmatrix}$. 

        On a ainsi sous la main une base de vecteurs propres. Soit $P$ la matrice de cette base, alors $P^{-1}AP=\begin{pmatrix} 2 & 0 & 0 \\ 0 & 2 & 0 \\ 0 & 0 & 4\end{pmatrix}=D$. Posons $R_0=\begin{pmatrix}\sqrt 2 & 0 & 0\\0 & \sqrt 2 & 0 \\ 0 & 0 & 2\end{pmatrix}$ et $R=PR_0P^{-1}$, on a $R^2=PR_0^2P^{-1}=PDP^{-1}=PP^{-1}APP^{-1}=A$. On a bien $R^2=A$.

        Soit $R$ telle que $R^2=A$. $A$ étant diagonalisable, elle annule le polynôme $(X-2)(X-4)$. On a alors :

        $$
        0=(R^2-2 I_3)(R^2-4 I_3)=(R-\sqrt 2 I_3)(R+\sqrt 2 I_3)(R-2I_3)(R+2I_3)
        $$ 

        On peut donc dire que le polynôme $(X-\sqrt2)(X+\sqrt 2)(X-2)(X+2)$ annule $R$. $R$ annule donc un polynôme scindé à racines simples : $R$ est diagonalisable et ses valeurs propres sont des racines carrées des valeurs propres de $A$. 
