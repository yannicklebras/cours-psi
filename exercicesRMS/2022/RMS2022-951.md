!!! exercice "RMS2022-951"
	=== "Enoncé"
        Soient $I$ un intervalle de $\mathbb{R}$ et $(E)$ l'équation différentielle $y^{\prime \prime}+x y^{\prime}+2 y=0$. Rechercher les solutions de $(E)$ développables en série entière et préciser $I$.
    
    === "Corrigé"
        d'après ddmaths

        Commençons par remarquer que $(E)$ est une équation différentielle linéaire d'ordre 2 homogène définie sur $\mathbb{R}:$ l'ensemble de ses solutions est un plan vectoriel inclus dans l'espace $\mathscr{C}^2(\mathbb{R}, \mathbb{R})$.

        Soit $y$ la somme d'une série entière $\sum a_n x^n$ de rayon de convergence $R>0$.
        La fonction $y$ est de classe $\mathscr{C}^{\infty}$ sur $]-R ; R[$ avec
        
        $$
        y^{\prime}(x)=\sum_{n=1}^{+\infty} n a_n x^{n-1} \quad \text { et } \quad y^{\prime \prime}(x)=\sum_{n=0}^{+\infty}(n+2)(n+1) a_{n+2} x^n .
        $$

        Après calculs où l'on écrit les sommes avec une même puissance de la variable $x$, la fonction $y$ est solution sur ] $R ; R[$ de l'équation ( $E$ ) si, et seulement si,
        
        $$
        \left.\sum_{n=0}^{+\infty}(n+2)\left((n+1) a_{n+2}+a_n\right) x^n=0 \text { pour tout } x \in\right]-R ; R[.
        $$

        Par unicité des coefficients d'un développement en série entière, on obtient la relation de récurrence
        
        $$
        a_{n+2}=-\frac{1}{n+1} a_n \quad \text { pour tout } n \in \mathbb{N}
        $$
        
        ce qui conduit à
        
        $$
        a_{2 p}=(-1)^p \frac{2^p p !}{(2 p) !} a_0 \quad \text { et } \quad a_{2 p+1}=\frac{(-1)^p}{2^p p !} a_1 \quad \text { pour tout } p \in \mathbb{N} .
        $$

        Considérons alors les fonctions $\varphi$ et $\psi$ données par
        
        $$
        \varphi(x)=\sum_{p=0}^{+\infty} \frac{(-1)^p 2^p p !}{(2 p) !} x^{2 p} \quad \text { et } \quad \psi(x)=\sum_{p=0}^{+\infty} \frac{(-1)^p}{2^p p !} x^{2 p+1}
        $$

        Les séries entières définissant $\varphi$ et $\psi$ sont chacune de rayon de convergence $+\infty$ et l'étude au-dessus assure que $\varphi$ et $\psi$ sont solutions de l'équation différentielle ( $E$ ) sur $\mathbb{R}$. Au surplus, $\varphi$ et $\psi$ sont linéairement indépendantes et constituent donc un système fondamental de solutions de l'équation $(E)$. Toutes les solutions de ( $E$ ) sont donc combinaisons linéaires de $\varphi$ et $\psi$, elles sont développables en série entière sur $\mathbb{R}$.

        Notons que $\psi(x)=x \mathrm{e}^{-x^2 / 2}$ pour tout $x \in \mathbb{R}$.
                



