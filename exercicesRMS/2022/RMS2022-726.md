!!! exercice "RMS2022-726"
	=== "Enoncé"
		Soit, pour  $n\in\N$, $u_n=\int_0^{\frac{\pi}{2}}\cos^n(t)\sin(nt)\d    t$.

		- a)   Montrer que $(u_n)$ converge.
		- b)   Montrer que, pour tout $n\in\N$, $u_{n+1}-u_n=\frac1{n+1}-u_{n+1}$.
		- c)   Quelle est la nature de la série de terme général $u_n$? Donner un équivalent de la somme partielle de cette série.

	=== "Corrigé"
		On travaille ici sur une intégrale sur un segment. posons $f_n(t)=\cos^n(t)\sin(nt)$. Alors pour tout $t\in[0, \frac\pi2]$, $f_n(t)\to 0$. De plus $|f_n(t)|\le 1$ qui est intégrable sur $[0,\frac\pi2]$ donc par théorème de convergence dominée, $u_n\to 0$.

		Soit $n\in\mathbb N$,

		$$
		\begin{align*}
		u_{n+1}-u_n&=\int_0^{\frac\pi2}\cos^n(t)(\cos(t)\sin((n+1)t)-\sin(nt))\\
		&=\int_0^{\frac\pi2}\cos^n(t)(\cos(t)\sin((n+1)t)-\sin((n+1)t-t))\\
		&=\int_0^{\frac\pi2}\cos^n(t)(\cos(t)\sin((n+1)t)-\sin((n+1)t)\cos(t)+\sin(t)\cos((n+1)t))\\
		&=\int_0^{\frac\pi2}\cos^n(t)(\sin(t)\cos((n+1)t))\\
		&=\left[-\frac 1{n+1}\cos^{n+1}(t)\cos((n+1)t)\right]_0^{\frac\pi2}-\int_0^{\frac\pi2}\cos^{n+1}(t)\sin((n+1)t)dt\\
		&=\frac 1{n+1}-u_{n+1}
		\end{align*}
		$$

		On a donc $2u_{n+1}-u_n=\frac 1{n+1}$. Sommons de $0$ à $N$ et notons $S_N$ la somme partielle de $\sum u_n$ d'ordre $N$. On a :
		$2(S_{N+1}-u_0)-S_N=H_{N+1}$ où $H_N$ est la somme partielle de la série harmonique d'ordre $N$. On peut donc écrire : $S_N=H_{N+1}+2u_0-2u_{N+1}$. On en déduit que $S_N$ diverge car $H_N$ diverge. De plus $u_{N+1}$ tend vers $0$ donc $S_N\sim \ln(N)$.
	<div class="admonition-foot">convergence dominée,équivalent</div>

