!!! exercice "RMS2022-689"
	=== "Enoncé"
		Soient $a\in\mathbb R$ et $b\in\mathbb R^*$.

		a)   Soit $A\in\mathcal{M}_n(\mathbb R)$ telle que $A^2$ soit diagonalisable à valeurs propres strictement positives. Montrer que $A$ est diagonalisable.

		b)   Diagonaliser $B=(b_{i,j})_{1\leq i,j\leq n}\in\mathcal{M}_n(\mathbb R)$,  où $b_{i,i}=a$ et $b_{i,j}=b$ si $i\ne j $.

		c)   Diagonaliser $C=(c_{i,j})_{1\leq i,j\leq n}\in\mathcal{M}_n(\mathbb R)$, où  $c_{i,n+1-i}=a$  et $c_{i,j}=0$ si $j\ne n+1-i$.



	=== "Corrigé"

		a) $A^2$ est diagonalisable à valeurs propres strictement positives. Notons $\lambda_1,\dots,\lambda_p$ ses valeurs propres. Alors $A^2$ annule le polynôme $\prod_{i=1}^p(X-\lambda_i)$. On a donc $0=\prod_{i=1}^p(A^2-\lambda_iI)=\prod_{i=1}^p(A-\sqrt{\lambda_i}I)(A+\sqrt{\lambda_i}I)$. Ainsi $A$ annule le polynôme $\prod_{i=1}^p(X-\sqrt{\lambda_i})(X+\sqrt{\lambda_i})$ qui est scindé à racines simples. Donc $A$ est diagonalisable.

		b) Notons $X=\begin{pmatrix}1\\\vdots\\1\end{pmatrix}$, puis pour $i$ entre $1$ et $n-1$, $X_1=\begin{pmatrix}1\\-1\\0\\\vdots\\0\end{pmatrix}$, $X_2=\begin{pmatrix}0\\1\\-1\\0\\\vdots\\0\end{pmatrix}$,$\dots$, $X_{n-1}=\begin{pmatrix}0\\\vdots\\0\\1\\-1\end{pmatrix}$. On a $BX=(a+(n-1)b)X$ et $BX_i=(a-b)X_i$. La famille $(X,X_1,\dots,X_{n-1})$. Si $P$ est la matrice dont les colonnes sont les vecteurs de cette base, alors $P^{-1}BP=\mathrm{diag}(a+(n-1)b,a-b,\dots,a-b)$.

		c) On suppose $a\ne 0$ sinon la matrice est nulle. On peut calculer que $C^2=a^2I_n$ donc $C^2$ est diagonalisable à valeurs propres strictement positives. Donc $C$ est diagonalisable de valeurs propres $a$ et $-a$ (il ne peut pas y avoir que $a$ puisque $C\ne aI_n$). $C'=\frac 1aC$ est la matrice d'une symétrie, elle est donc diagonalisable sous la forme $\mathrm{diag}(1,\dots,1,-1,\dots,-1)$. Si $n$ est pair, alors $\mathrm{tr}(C')=0$ et alors le nombre de $1$ et de $-1$ sont les mêmes. Si $n$ est impair, alors $\mathrm{tr}(C')=1$ et il y a un $1$ de plus que de $-1$ : $\frac {n-1}2+1$ fois le $1$ et $\frac {n-1}2$ fois le $-1$. On en déduit la diagonalisation de $C$.



