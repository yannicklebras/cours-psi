!!! exercice "RMS2022-1423"  
	=== "Enoncé"  
		  **ENSEA**
		On munit $\mathcal{M}_2(\mathbb{R})$ du produit scalaire $\langle M, N \rangle = \tr(M^T   N)$.


		 - a)
		Vérifier que $\langle \;, \; \rangle$ est bien un produit scalaire sur $\mathcal{M}_2(\mathbb{R})$.

		 - b)
		Montrer que $F = \left\{\small\begin{pmatrix}
		a & b \\
		-b & a
		\end{pmatrix}, \ (a, b) \in \mathbb{R}^2\right\}$
		est un sous-espace vectoriel de $\mathcal{M}_2(\mathbb{R})$ et trouver une base $(E_1, E_2)$ de $F$.

		 - c)
		Déterminer une base orthonormée de $F^{\perp}$.

		 - d)
		En déduire la projection orthogonale de $J$ sur $F^{\perp}$.

	=== "Corrige"  

		$F$ est clairement un sev. Une base est $E_1=\begin{pmatrix} 1 & 0 \\ 0 & 1\end{pmatrix}$ et $E_2=\begin{pmatrix} 0 & 1 \\ -1 & 0\end{pmatrix}$.

		$F^\bot$ est de dimension $2$. On a $E_3=\begin{pmatrix} 1 & 0 \\ 0 & -1\end{pmatrix}$ et $E_4=\begin{pmatrix} 0 & 1 \\ 1 & 0\end{pmatrix}$ sont toutes les deux orthogonales à $E_1$ et $E_2$ et elles ne sont pas liées donc $(E_3,E_4)$ est une base orthogonale de $F^\bot$.

		On pose $J=\begin{pmatrix} 1 & 1 \\ 1 & 1\end{pmatrix}$. La projection de $J$ sur $F^\bot$ est donnée par $\frac{<J|E_3>}{||E_3||^2}E_3+\frac{<J|E_4>}{||E_4||^2}E_4=0+\frac{2}{2}E_4=E_4$.


