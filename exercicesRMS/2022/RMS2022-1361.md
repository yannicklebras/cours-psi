!!! exercice "RMS2022-1361"  
	=== "Enoncé"  
		 **IMT**Soit $E=\R_n[X]$. On considère $n+1$ nombres réels distincts $a_0,\ldots,a_n$ et on pose, pour $P,Q\in E$,
		$\phi(P,Q)=\sum_{k=0}^n P(a_k)\, Q(a_k)$.


		- a) Montrer que $\phi$ définit un produit scalaire sur $E$.

		- b) On pose $F=\left\{P\in E,\ P(a_0)+\cdots +P(a_n)=0\right\}$. Déterminer $F^{\perp}$.

		- c) Soit $P\in E$. Déterminer la distance de $P$ à $F$.

	=== "Corrige"  

		 On trouve que c'est un roduit scalaire : pour le caractère défini positif, on a $n+1$ racines pour un polynôme de degré $n$ : c'est trop.

		 $F$ est clairement le noyau de la forme linéaire : $P\mapsto P(a_0)+\cdots+P(a_n)$. Son orthogonal est donc de dimension $1$. Soit $P\in F$ et soit $Q=1$, alors $<P|Q>=P(a_0)+\cdots+P(a_n)=0$. On en déduit $F^\bot=Vect(1)=\mathbb R$. Calculons pour un polynôme $P$ quelconque, $<P|1> =\sum P(a_i)$. On en déduit que $d^2(P,F)=||P-\frac{<P|1>}{||1||^2}||^2=||P-\frac{<P|1>}{||1||^2}||^2=||P||^2-\frac 2{n+1}<P|1>^2+\frac{<P|1>^2}{n+1}=||P||^2-\frac{<P|1>^2}{n+1}$.


