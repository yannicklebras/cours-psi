!!! exercice "RMS2022-1368"  
	=== "Enoncé"  
		 **CCINP** Soit $A\in{\cal M}_n(\R)$ telle que $A(A^T A)^2=I_n$.

		- a) Montrer que $A$ est inversible.

		- b) Montrer que $A$ est symétrique.

		- c) En déduire que $A=I_n$.


	=== "Corrige"  

		$A$ est clairement inversible, d'inverse $(A^\top A)^2$.

		Passons l'égalité à la transposée :

		$$
		\begin{align*}
		(A^{-1})^\top &= (A^\top A)^2)^\top\\
		&=(A^\top AA^\top A)^\top\\
		&=(A^\top AA^\top A)\\
		&=A^{-1}
		\end{align*}
		$$

		Ainsi $A^{-1}$ est symétrique donc $A$ est symétrique.

		La condition de l'énoncé devient alors $A^5=I_n$. On a besoin d'un résultat de deuxième année : $A$ étant symétrique, elle est diagonalisable : il existe $P$ inversible réelle telle que $P^{-1} AP=D$. En multipliant à gauche par $P^{-1}$ et à droite par $P$, l'équation $A^5=I_n$ devient $D^5=I_n$ (c'est une équation équivalente. Si on note $\lambda_1,\dots,\lambda_n$ les coefficients diagonaux de $D$, on a sur chaque terme diagonal $\lambda_i^5=1$ c'est à dire $\lambda_i=1$. $A$ est donc semblable à la matrice identité : $A=I_n$.


