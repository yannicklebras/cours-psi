!!! exercice "RMS2022-749"  
	=== "Enoncé"  
        Soient $X$ et $Y$ deux variables aléatoires indépendantes de même loi géométrique de paramètre $p$.

        a) Déterminer la loi de la variable $T=\min (X, Y)$; préciser son espérance et sa fonction génératrice.

        b) Montrer que la variable $\frac{1}{T(T+1)}$ admet une espérance finie puis la calculer.

	=== "Corrigé"  
        a) On a $P(X\ge n)=\sum_{k=n}^{+\infty} p(1-p)^{k-1}=p(1-p)^{n-1}\times\frac 1{1-(1-p)}=(1-p)^{n-1}$. On a alors $\P(T\ge k)=\P(X\ge k,Y\ge k)=\P(X\ge k)\P(Y\ge k)$ par indépendance et donc $\P(T\ge k)=(1-p)^{2(k-1)}$. On a alors $\P(T=k)=\P(T\ge k)-\P(T\ge k+1)=(1-p)^{2(k-1)}\left(1-(1-p)^2\right)=(2p-p^2)(1-(2p-p^2))^{k-1}$. On en déduit que $T$ suit une loi géométrique de paramètre $(2p-p^2)$. Si on pose $q=1-p$ on obtient $T\sim \mathcal G(1-q^2)$. 

        b) On note ici $r=1-q^2$. La famille $\frac 1{k(k+1)}P(T=k)=\frac 1{k(k+1)}r(1-r)^{k-1}$ est sommable (c'est un $o(\frac 1{k^2})$) donc $Z=\frac 1{T(T+1)}$ admet une espérance par le théorème de transfert. On calcule:

        $$
        \begin{aligned}
        E(Z) &= \sum_{k=1}^{+\infty}\frac 1{k(k+1)}r(1-r)^{k-1}\\
        &= \sum_{k=1}^{+\infty}\left(\frac 1k-\frac 1{k+1}\right)r(1-r)^{k-1}\\
        &= \frac r{1-r}\sum_{k=1}^{+\infty}\frac{(1-p)^k}k - \frac r{(1-r)^2}\left(\sum_{k=1}^{+\infty}\frac{(1-r)^k}{k}-(1-r)\right)\\
        &= \frac r{1-r}(-\ln(r))-\frac r{(1-r)^2}(-\ln(r)-1+r)\\
        &= \frac{r^2\ln(r)+r-r^2}{(1-r)^2}
        \end{aligned}
        $$
 