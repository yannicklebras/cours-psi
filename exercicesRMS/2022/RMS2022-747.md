!!! exercice "RMS2022-747"  
	=== "Enoncé"  
        Soient $X_1$ et $X_2$ des variables aléatoires indépendantes suivant les lois de Poisson de paramètres respectifs $\lambda_1$ et $\lambda_2$. Soit $Y$ une variable aléatoire indépendante des deux autres telle que $Y(\Omega)=\{-1,1\}$ et $\mathbf{P}(Y=1)=p \in ]0 ; 1[$. On pose $M=$ $\left(\begin{array}{cc}X_1 & X_2 \\ Y X_2 & X_1\end{array}\right)$.
        
        a) Probabilité que $M$ soit inversible?
        
        b) Probabilité que les valeurs propres de $M$ soient réelles?
        
        c) Probabilité que $M$ soit diagonalisable sur $\mathbf{R}$ ?

	=== "Corrigé"  

        a) On a $\det(M)=X_1^2-YX^2_2$. Ainsi par formule des probabilités totales, $\P(\det(M)=0)=\P(\det(M)=0|Y=1)\P(Y=1)+\P(\det(M)=0|Y=-1)\P(Y=-1)=p\P(X_1^2=X_2^2)+(1-p)\P(X_1^2=-X_2^2)=p\P(X_1^2=X_2^2)+(1-p)\P(X_1=X_2=0)$.

        Or 

        $$
        \begin{aligned}
        P(X_1^2=X_2^2)&=\sum_{k=0}^{+\infty} \P(X_1=n,X_2=n)&\text{car les valeurs sont positives}\\
        &=\sum_{k=0}^{+\infty}\P(X_1=n)\P(X_2=n)&\text{par indépendance}\\
        &=\sum_{k=0}^{+\infty}\frac{(\lambda_1\lambda_2)^k}{(k!)^2}e^{-(\lambda_1+\lambda_2)}
        \end{aligned}
        $$

        et

        $$
        \P(X_1=X_2=0)=\P(X_1=0,X_2=0)=\P(X_1=0)\P(X_2=0)=e^{-(\lambda_1+\lambda_2)}.
        $$

        On a donc $\P(\det(M)=0)=p\sum_{k=0}^{+\infty}\frac{(\lambda_1\lambda_2)^k}{(k!)^2}e^{-(\lambda_1+\lambda_2)}+(1-p)e^{-(\lambda_1+\lambda_2)}$. Je ne suis pas certain qu'on puisse faire mieux.

        b) On a $\chi_M(\lambda)=(\lambda-X_1)^2-YX_2^2=\lambda^2-2X_1\lambda+(X_1^2-YX_2^2)$. $M$ admet des valeurs propres réelles si et seulement si le discriminant $\Delta$ est positif. Or $\Delta=4YX_2^2$ donc $\P(\Sp(M)\subset \mathbb R)=\P(\Delta\ge 0)=\P(\Delta\ge 0|Y=1)\P(Y=1)+\P(\Delta\ge 0|Y=-1)\P(Y=-1)=p+(1-p)\P(X_1=X_2=0)=p+(1-p)e^{-(\lambda_1+\lambda_2)}$. 

        c) La matrice $M$ est diagonalisable à deux conditions : soit le polynôme caractéristique est scindé à racines simples (on est en dim 2 ici), soit la matrice est diagonale. Cela se traduit par : $\P(M\ diago)=\underset{\text{racines distinctes}}{\underbrace{\P(\Delta>0)}}+\underset{\text{racine double et diagonale}}{\underbrace{\P(\Delta=0\cap X_2=0)}}=\P(X_2\ne 0)\P(Y=1)+\P(X_2=0)=p(e^{\lambda_2}-1)e^{-\lambda_2}+e^{-\lambda_2}$. Ce qui s'écrit encore sous la forme $\P(M\ diago)=p+(1-p)e^{-\lambda_2}$.
