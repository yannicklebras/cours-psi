!!! exercice "RMS2022-1293"  
	=== "Enoncé"  
		**CCINP**  .  
		Soit $E$ un $\R$-espace vectoriel de dimension finie $ n \ge 2$, $f \in \cl(E)$.

		- a) Donner un exemple d'endomorphisme pour lequel noyau et image ne sont pas supplémentaires.

		- b) Montrer que si $f$ est diagonalisable, $\Im f$ et $\Ker f$ sont supplémentaires. Que dire de la réciproque~?

		- c)
			- i) Montrer que la suite $(\dim \Ker f^k)_{k \in  }$ est croissante.

			- ii) Montrer qu'il existe $k_0$ tel que $\forall k >k_0, \Ker f^k = \Ker f^{k_0}$.

			- iii) Montrer que $\Ker f^{k_0}$ et $\Im f^{k_0}$ sont supplémentaires.

	=== "Corrige"  

		 L'endomorphisme de $\mathbb R_n[X]$  $P\mapsto P'$ a pour image $\mathbb R_{n-1}[X]$ et comme noyau $R_0[X]$ : l'un est inclus das l'autre donc ils ne sont pas supplémentaires.

		 Si l'endomorphisme est diagonalisable, alors les sous-espaces propres sont en somme directe. Ainsi le sep associé à $0$ est supplémentaire de tous les autres, qui engendrent l'image.

		 Si $x\in\ker f^k$ alors $f^{k+1}(x)=f(f^k(x))=f(0)=0$. Donc $x\in\ker f^{k+1}$. On a ainsi en particulier $\dim f^k\le \dim f^{k+1}$ : la suite est croissante.

		 C'est une suite croissante d'entiers majorée par $n$ donc elle est constante à partir d'un certain rang $k_0$. On a alors pour tout $k>k_0$, $\ker f^{k_0}\subset \ker f^{k}$ et par stabilité de la suite, on a alors $\ker f^{k_0}=\ker f^k$.

		 Il suffit de montrer que l'intersection est réduite à $0$, on aura les dimensions par théorème du rang.

		 Soit $x\in\ker f^{k_0}\cap \im f^{k_0}$. On pose $x=f^{k_0}(x_0)$. On a alors $f^{k_0}(f^{k_0}(x_0))=0$ et ainsi $f^{2k_0}(x_0)=0$. Or $\ker f^{k_0}=\ker f^{2k_0}$ donc $f^{k_0}(x_0)=0$ c'est à dire $x=0$.

		 Ainsi $\ker f^{k_0}\cap \mathrm{im} f^{k_0}=\{0\}$. On en déduit le caractère supplémentaire.


