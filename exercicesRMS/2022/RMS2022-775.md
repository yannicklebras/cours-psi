!!! exercice "RMS2022-775"  
	=== "Enoncé"  
		Soit $E$ un espace vectoriel de dimension $n$, $F_1$ et $F_2$ deux sous-espaces de $E$ de dimension $n-1$. Montrer qu'il existe un sous-espace vectoriel $G$ de $E$ tel que $E=F_1 \oplus G=F_2 \oplus G$.

	=== "Corrige"  
        On note $k$ la co-dimension de $F_1$ et $F_2$, c'est à dire la dimension d'un supplémentaire de $F_i$. On montre par récurrence sur $k$ la propriété suivante : $H_k=$«Si $F_1$ et $F_2$ sont de codimension $k$, alors $F_1$ et $F_2$ ont un supplémentaire en commun»

        ^^initialisation :^^  si $k=0$, alors $F_1=F_2=E$ et la propriété est triviale (le supplémentaire commun est $\{0\}$. 

        ^^hérédité :^^ Soit $k\in\mathbb N$, on suppose $H_k$ vraie. Soit $F_1$ et $F_2$ deux sous-espaces de $E$ de codimension $k+1$. Si $F_1$ et $F_2$ sont confondus, le problème est réglé, tout supplémentaire de l'un est supplémentaire de l'autre, et en dimension finie, les supplémentaires existent. Si $F_1$ et $F_2$ ne sont pas confondus, pour des questions de dimension on a $F_1\not\subset F_2$ et $F_2\not\subset F_1$. Il existe donc $x_1\in F_1$ tel que $x_1\notin F_2$ et $x_2\in F_2$ tel que $x_2\notin F_1$. Posons $u=x_1+x_2$. Alors $u\notin F_1$ : $F_1\oplus u$ est donc de codimension $k$. De même $F_2\oplus u$ est de codimension $k$. Par hypothèse de récurrence, $F_1\oplus u$ et $F_2\oplus u$ admettent un supplémentaire commun, qu'on note $H$. Alors $H\oplus u$ est un supplémentaire commun à $F_1$ et à $F_2$. 