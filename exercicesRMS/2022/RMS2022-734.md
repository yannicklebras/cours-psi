!!! exercice "RMS2022-734"
	=== "Enoncé"
        Soit $f: x \mapsto \int_1^{+\infty} \frac{e^{-t}}{x+t} d t$.

        a) Montrer que $f$ est dérivable sur $]-1,+\infty[$. Trouver une relation entre $f$ et $f'$.
        
        b) En déduire que $f$ est développable en série entière au voisinage de 0 et trouver son développement.

	=== "Corrigé"
        Posons $g(x,t)=\frac{e^{-t}}{x+t}$. 

        a) Soit $x\in]-1,+\infty[$, alors $t\mapsto\frac{e^{-t}}{x+t}$ est bien définie en $1$ et $t^2\frac{e^{-t}}{1+t}\xrightarrow[t\to+\infty]{}0$ donc la fonction est intégrable sur $]1,+\infty[$.   

        La fonction $x\mapsto g(x,t)$ est de classe $\mathcal C^1$ pour tout $t\in[1,+\infty[$ par théorèmes généraux car $x+t$ ne s'annule pas.

        Soit $x>-1$ et $t\in[1,+\infty[$, $\frac{\partial g}{\partial x}(x,t) = -\frac{e^{-t}}{(t+x)^2}$ est continue par morceaux par rapport à $t$. 

        Enfin si $-1<a<x$ alors $\left|\frac{\partial g}{\partial x}(x,t)\right|\le\frac{e^{-t}}{(a+t)^2}=\phi(t)$ (car $a+t>0$). $\phi(t)$ est intégrable sur $[1,+\infty[$.  

        On en déduit que $f$ est $\mathcal C^1$ sur tout $[a,+\infty[\subset ]-1,+\infty[$ donc sur $]-1,+\infty[$ et sa dérivée est $f'(x)=-\int_1^{+\infty}\frac{e^{-t}}{(x+t)^2}\d t$. Par une intégration par parties licite, on a $f'(x)-f(x)=-\frac{e^{-1}}{1+x}$.

        Supposons que $f$ soit développable en série entière avec $f(x)=\sum_{n\ge 0}a_nx^n$. L'équation différentielle s'écrit $\sum_{n\ge 0}(n+1)a_{n+1}x^n-\sum_{\ge 0}a_nx^n=-e^{-1}\sum_{n\ge 0}(-1)^nx^n$ ce qu'on peut traduire pour tout $n\ge 0$ par $(n+1)a_{n+1}-a_n=(-1)^{n+1}e^{-1}$. En multipliant par $n!$ on fait apparaître une somme télescopique et on a $n!a_n=\sum_{k=1}^{n}(-1)^{k}(k-1)!e^{-1}+a_0$ ou encore $a_n=\sum_{k=0}^{n-1}(-1)^{k+1}\frac{k!}{n!}+\frac {a_0}{n!}$.

        Vérifions qu'un tel terme général donne un rayon de convergence non nul : $|a_n|\le e^{-1}\sum_{k=0}^{n-1}\frac{(n-1)!}{n!}+|a_0|\le e^{-1}+|a_0|$. le terme général est donc bornée, et le rayon de convergence est ainsi au moins de $1$. Cette série entière est par construction solution de l'équation différentielle et si on impose $a_0=f(0)$ qui est bien défini, on trouve que $f$ est développable en série entière.

