!!! exercice "RMS2022-163"
	=== "Enoncé"
		Pour tout $x\in\mathcal{M}_{n,1}(\mathbb R)$, on note $x\geq 0$ si toutes les coordonnées de $x$ sont positives. On considère $A\in\mathcal{M}_n(\mathbb R)$ telle que, pour tout $x\in\mathcal{M}_{n,1}(\mathbb R)$ tel que $Ax\geq0$, on a $x\geq0$.

		   a) Montrer que $A$ est inversible.

		   b) Montrer que $A^{-1}$ est à coefficients positifs.


	=== "Corrigé"
		a) Soit $x$ un élément non nul de $\mathcal{M}_{n,1}(\mathbb R)$. Alors soit $x\not\ge 0$, soit $-x\not\ge 0$. Sans perte de généralité, supposons que $-x\not\ge 0$. On peut alors assurer que $Ax\not\ge0$ et donc au moins un des coefficients est strictement négatif, et donc non nul : ainsi $x\notin \ker A$. On en déduit que $\ker A$ ne contient que des vecteurs nuls et ainsi $A$ est inversible.

		b) Notons $e_1=\begin{pmatrix}1\\0\\\vdots\\0\end{pmatrix}$ alors $e_1\ge 0$. On remarque tout d'abord que si l'on note $c_1$ la première colonne de $A^{-1}$, alors $c_1=A^{-1}e_1$. Ici, on trouve $Ac_1=AA^{-1}e_1=e_1\ge 0$ d'où $c_1\ge0$. En faisant de même avec les autres colonnes, on peut affirmer que $A^{-1}$ possède uniquement des coefficients positifs ou nuls.
	<div class="admonition-foot">blahblah</div>		


!!! exercice "RMS2022-307"
	=== "Enoncé"
		a) Montrer que, pour tout point $(x,y)\not=(-1,0)$ du cercle unité, il existe $t\in \mathbb R$ tel que
		$\left(x=\frac{1-t^2}{1+t^2}\;,\;y=\frac{2t}{1+t^2}
		\right).$

		b) Montrer que $x$ et $y$ sont rationnels si et seulement si $t$ est rationnel.


	=== "Corrigé"
		a) Soit $t$ un point du cercle unité différent de $(-1,0)$, il existe alors un angle $\theta$ tel que $x=\cos\theta$ et $y=\sin\theta$ avec $\theta\in]-\pi,\pi[$. Posons $t=\tan\frac\theta2$ qui est bien défini pour ces valeurs de $\theta$. On a alors d'après des formules classiques $x=\frac{1-t^2}{1+t^2}$ et $y=\frac {2t}{1+t^2}$.

		b) Tout d'abord, si $t$ est rationnel alors évidemmet $x$ et $y$ sont rationnels.

		Voyons la réciproque : Supposons $x$ et $y$ rationnels. Tout d'abord, si $y=0$, alors $t=0$ est bien rationnel.Sinon, on a $\frac xy=\frac{1-t^2}{2t}$ et donc $yt^2+2xt-y=0$. Cette équation en $t$ admet pour discriminant $\Delta=4x^2+4y^2=4$ puisque $(x,y)$ est sur le cercle unité. Les deux valeurs possibles de $t$ sont donc $\frac{-2x\pm2}{2y}=\frac{-x\pm1}{y}$ qui sont bien des valeurs rationnelles. Donc $t$ est rationnel.



!!! exercice "RMS2022-676"
	=== "Enoncé"
		Soit $P\in\mathbb R[X]$ de degré au moins $2$.

		a) On suppose que $P$ est de la forme $P=\prod_{k=1}^n (X-\lambda_k)^{\alpha_k}$, où les $\lambda_k$ sont dans $\mathbb R$
		et les $\alpha_k$  dans $\mathbb N^*$. Montrer que $\frac{P'}{P}=\sum_{k=1}^n \frac{\alpha_k}{X-\lambda_k}$.

		b) On suppose que $P$ est scindé sur $\mathbb R$. Soit $x\in\mathbb R$ tel que $P'(x)=0$ et $P(x)\ne 0$. Montrer que $P''(x)P(x)<0$.

		c) Soient $x_1$ et $x_2$ deux racines consécutives de $P$. Montrer que $P'(x_1)P'(x_2)\le 0$.

		d) Soient $\alpha$ et $\beta$ des réels distincts tels que $P-\alpha$ et $P-\beta$ sont scindés sur $\mathbb R$. Montrer que $P'$ est scindé
		sur $\mathbb R$ à racines simples.


	=== "Corrigé"
		a) Question classique, qui est même une question de cours en MPSI : On a $\displaystyle P=\prod_{k=1}^n (X-\lambda_k)^{\alpha_k}$ et donc $\displaystyle P'=\sum_{i=1}^n \alpha_i(X-\lambda_i)^{\alpha_i-1}\prod_{k\ne i}(X-\lambda_k)^{\alpha_k}$. On en déduit $$\frac {P'}{P}=\frac{\sum_{i=1}^n \alpha_i(X-\lambda_i)^{\alpha_i-1}\prod_{k\ne i}(X-\lambda_k)^{\alpha_k}}{\prod_{k=1}^n (X-\lambda_k)^{\alpha_k}}=\sum_{i=1}^n\frac{\alpha_i(X-\lambda_i)^{\alpha_i-1}}{(X-\lambda_i)^{\alpha_i}}=\sum_{i=1}^n\frac{\alpha_i}{X-\lambda_i}$$.

		b) $P$ étant scindé sur $\mathbb R$, on peut lui appliquer la question précédente. On a alors, en dérivant : $\frac{P''P-P'^2}{P^2}=\sum_{i=1}^n-\frac{\alpha_i}{(X-\lambda_i)^2}$. En prenant cette expression en $x$ tel que $P(x)\ne 0$ et $P'(x)=0$, on obtient $\frac{P''(x)P(x)}{P(x)^2}=-\sum_{i=1}^n\frac{\alpha_i}{(x-\lambda_i)^2}$ et toutes les quantités étant réelles, on en déduit le signe de $P''(x)P(x)<0$.

		c) Soient $x_1$ et $x_2$ deux racines consécutives de $P$. Si au moins l'une des deux racines est multiple alors évidemment $P'(x_1)P'(x_2)=0\le 0$. Les racines étant consécutives, on peut assurer que $P$ est de signe constant sur $[x_1,x_2]$, par exemple, et sans perte de généralité, $P>0$. $P$ est dérivable en $x_1$ et en $x_2$ avec $P'(x_1)=\lim_{x\to x_1+}\frac{P(x)-P(x_1)}{x-x_1}=\frac{P(x)}{x-x_1}$. Or pour $x>x_1$, $x-x_1>0$ et donc $\frac{P(x)}{x-x_1}>0$. On en déduit que $P'(x_1)\ge 0$. De la même manière, $P$ étant dérivable en $x_2$, on a $P'(x_2)=\lim_{x\to x_2^-}=\frac{P(x)-P(x_2)}{x-x_2}=\frac{P(x)}{x-x_2}$ et si $x<x_2$ alors $x-x_2<0$ et $\frac{P(x)}{x-x_2}<0$. On en déduit que $P'(x_2)\le 0$. Ainsi $P'(x_1)P'(x_2)\le 0$.

		d) Quitte à transformer $P-\alpha$ en $P$ on peut considérer $\alpha=0$ avec $\beta\ne 0$. Soit $z_1,\dots,z_p$ les racines de $P'$. Si $z_i$ n'est pas racine de $P$ alors d'après la question b, $P$ étant scindé, on a $P(z_i)P''(z_i)<0$ et donc $z_i$ n'est pas racine de $P''$ : c'est une racine simple de $P'$. Si $z_i$ est racine de $P$ alors elle n'est pas racine de $P-\beta$ car $\beta\ne 0$. On en déduit que $z_i$ est racine de $(P-\beta)'$ mais pas de $P-\beta$ et puisque $P-\beta$ est scindé, on obtient d'après la question b, comme précédemment, que $(P-\beta)''(z_i)\ne 0$ c'est à dire $P''(z_i)\ne 0$. Donc $z_i$ est aussi une racine simple de $P'$. Ainsi toutes les racines de $P'$ sont simples : $P'$ est scindé à racines simples.



!!! exercice "RMS2022-312"
	=== "Enoncé"
		Soit $f:P\in \mathbb R_n[X]\mapsto X(X-1)P'(X)-nXP(X)\in \mathbb R[X]$.  

		a) Montrer que $f$ est un endomorphisme de $\mathbb R_n[X]$.

		b) Trouver les valeurs propres de $f$. Montrer que $f$ est diagonalisable.


	=== "Corrigé"
		a) $f$ est clairement linéaire. Le point important sera plutôt la stabilité de $\mathbb R_n[X]$ par $f$. Soit $P$ de degré $n$ alors $f(P)$ est une différence de deux polynômes de degré $n+1$. A priori, on n'est pas dans $R_n[X]$. On doit donc voir ce qu'il se passe sur le coefficient de degré $n+1$. Si $p_n$ est le coefficient de degré $n$ de $P$ (éventuellment $p_n=0$), alors le coefficient de degré $n+1$ de $X(X-1)P'$ est $np_n$. Le coefficient de degré $n+1$ de $nXP$ est aussi $np_n$. Donc le coefficient de degré $n+1$ de $f(P)$ vaut $0$ : $f(P)$ est donc de degré au plus $n$. Ainsi $f$ est bien un endomorphisme de $\mathbb R_n[X]$.

		b) Soit $P$ un vecteur propre de $f$. On a donc $f(p)=\lambda P$ avec $\lambda\in \mathbb R$. On a alors $\lambda P=X(X-1)P'-nXP$ c'est à dire $(nX+\lambda)P=X(X-1)P'$ ou encore $n(X+\frac\lambda n)P=X(X-1)P'$.
		On remarque que $0$ et $1$ semblent être de bons candidats de racines de $P$. Soit $k$ la multiplicité de $0$ en tant que racine de $P$. Dérivons $k$ fois avec la formule de Leibniz : $n(X+\frac\lambda n)P^{(k)}+nkP^{(k-1)}=X(X-1)P^{(k+1)}+k(2X-1)P^{(k)}+k(k-1)P^{(k-1)}$. On prend la valeur en $0$ : $\lambda P^{(k)}(0)=-kP^{(k)}(0)$. On en déduit $\lambda=-k$. Notons maintenant $q$ la multiplicité de $1$ en tant que racine de $P$. On dérive $q$ fois : $n(X-\frac k n)P^{(q)}+nqP^{(q-1)}=X(X-1)P^{(q+1)}+q(2X-1)P^{(q)}+q(q-1)P^{(q-1)}$ et on prend la valeur en $1$ : $(n-k)P^{(q)}(1)=qP^{(q)}(1)$. Or $P^{(q)}(1)\ne 0$ donc $q=n-k$. On en déduit ainsi la forme de $P$ : $P=\lambda X^k(X-1)^{n-k}$ car $P$ est de degré au plus $n$. Réciproquement, on vérifie bien $f(X^k(X-1)^{n-k})=-kX^k(X-1)^{n-k}$. $f$ admet donc $n+1$ valeurs propres distinctes : $f$ est diagonalisable dans la base des ${\left(X^k(X-1)^{n-k}\right)}_{k\in\{0,\dots,n\}}$.



!!! exercice "RMS2022-675"
	=== "Enoncé"
		Soit $P\in\mathbb Q_n[X]$. Montrer l'équivalence entre les propriétés:

		(i)  pour tout $k\in\mathbb Z$, $P(k)\in\mathbb Z$,

		(ii)   pour tout $k\in[\![  0,n]\!]$, $P(k)\in\mathbb Z$,

		(iii)   il existe $m\in\mathbb Z$ tel que, pour tout $k\in[\![m, m+n]\!]$, $P(k)\in\mathbb Z$.


		***Ind.*** On pourra introduire les polynômes $H_k=\frac1{k!}X(X-1)\cdots(X-k+1)$.


	=== "Corrigé"
		Tout d'abord on a évdemment les relations : $(i)\Rightarrow(ii)$ et $(i)\Rightarrow (iii)$ ainsi que $(ii)\Rightarrow (iii)$. Nous allons montrer que $(ii)\Rightarrow (i)$ et que $(iii)\Rightarrow (i)$.
		Commençons par faire une remarque plutôt simple à démontrer : on a pour tout $p\in\mathbb Z$, $H_k(p)\in \mathbb Z$ avec en particulier $H_k(k)=1$. De plus, $(1,H_1,\dots,H_n)$ étant une famille de $n+1$ éléments échelonnés de $\mathbb Q_n[X]$, cette famille est une base de $\mathbb Q_n[X]$.

		Supposons $(ii)$ vraie, alors en écrivant $P=q_0H_0+q_1H_1+\dots q_nH_n$, on obtient le système :
		$$\begin{cases}
		P(0)&=q_0\\
		P(1)&=q_0H_0(1)+q_1H_1(1)\\
		\vdots\\
		P(n)&=q_0H_0(n)+q_1H_1(n)+\dots+q_nH_n(n)
		\end{cases}$$
		système qui nous permet d'assurer que les $q_i$ sont des entiers.

		Soit maintenant un entier $q$ quelconque, alors $P(i)=q_0H_0(i)+q_1H_1(i)+\dots+q_nH_n(i)$ qui est une somme d'entier et ainsi $P(i)\in\mathbb Z$. Ainsi $(ii)\Rightarrow (i)$.

		Montrons maintenant que $(iii)\Rightarrow (i)$. Pour cela, posons $Q(X)=P(X+m)$. On a donc pour $k\in\{0,\dots n\}$, $Q(k)=P(m+k)\in\mathbb Z$. Alors d'après le cas précédent, on peut en déduire que $Q(\mathbb Z)\subset \mathbb Z$. Soit alors $k\in\mathbb Z$, $P(k)=Q(k-m)$. Or $k-m\in\mathbb Z$ donc $Q(k-m)\in\mathbb Z$. Ainsi $P(k)\in\mathbb Z$ et on en déduit que $P(\mathbb Z)\subset\mathbb Z$.  

		On a donc bien équivalence entre les trois propriétés.



!!! exercice "RMS2022-677"
	=== "Enoncé"
		Soit $E={\cal C}^{\infty}(\mathbb R^{+*},\mathbb C)$. Pour  $k\in\mathbb N$, soit $f_k:x\mapsto x^ke^{\frac{i}{x}}$.

		Pour $f\in E$, soit $\varphi(f):x\mapsto x^2f'(x)-(2-i)f(x)$.

		a)   Calculer $\varphi(f_k)$ pour $k\in\mathbb N$. Vérifier que $(f_k)_{k\in\mathbb N}$ est libre. L'endomorphisme $\varphi$ est-il injectif?

		b)  Pour  $\ell\in\mathbb N$, soit $F_{\ell}=\mathrm{Vect} (f_0,f_1,\ldots,f_{\ell})$.

		  Déterminer le rang de l'application $f\in F_{\ell}\mapsto \varphi(f)\in F_{\ell+1}$.

		c)   Montrer que $f_n$ est prolongeable par continuité en $0$ si et seulement si  $n\geq 1$.  Donner une condition nécessaire et suffisante pour que $f_n$ soit dérivable en $0$. Donner une condition nécessaire et suffisante poue que $f_n$ soit alors de classe $\mathcal{C}^1$.


	=== "Corrigé"
		Remarquons avant toute chose que $\varphi$ est un endomorphisme de $E$.

		a) Soit $k\in\mathbb N$, $\varphi(f_k)(x)=x^2\times(kx^{k-1}e^{\frac ix}-\frac{ix^k}{x^2}e^{\frac ix})-(2-i)x^ke^{\frac ix}=kx^{k+1}e^{\frac ix}-ix^ke^{\frac ix}-(2-i)x^ke^{\frac ix}=(kx-2)x^ke^{\frac ix}=(kx-2)f_k(x)=kf_{k+1}(x)-2f_k(x)$.

		Soit maintenant $k_1<\dots<k_n$ des entiers naturels et $\lambda_1,\dots,\lambda_n$ des complexes tels que $\lambda_1f_{k_1}+\dots+\lambda_n f_{k_n}=0$ alors pour tout $x\in\mathbb R^+_*$, on a $(\lambda_1x^{k_1}+\dots+\lambda_n x^{k_n})e^{\frac ix}=0$. L'exponentielle étant non nulle, on en déduit que pour tout $x$, $(\lambda_1x^{k_1}+\dots+\lambda_n x^{k_n})=0$. Or un olynôme est nul si et seulement si ses coefficients sont nul : $\lambda_1=\dots=\lambda_n=0$. Ainsi, toute sous famille finie de $(f_k)_{k\in\mathbb N}$ est libre donc la famille infinie est libre.

		Enfin, si $f$ est un élément du noyau de $\varphi$, alors $f$ est solution de l'équation différentielle $x^2f'(x)-(2-i)f(x)=0$ sur $\mathbb R^+_*$. On remarque en particularisant en $0$ que $f(0)=0$ et d'après le théorème de Cauchy, la seule solution est donc la solution nulle : $\varphi$ est injective.

		b) Etant donné que $\varphi(f_k)=kf_{k+1}-2f_k$, $\varphi$ définit bien une application de $F_\ell$ dans $F_{\ell+1}$. La famille des $f_k$ étant libre, $F_\ell$ est de dimension $\ell +1$. $\varphi$ étant injective, en appliquant le théorème du rang on a $\dim F_\ell=\mathrm{rg}(\varphi_{|F_\ell})+\underset{0}{\underbrace{\dim\ker( \varphi_{|F_\ell})}}$. Donc $\mathrm{rg}(\varphi_{|F_\ell})=\ell+1$.

		c) Si $n\ge 1$, alors pour $x>0$, $|f_n(x)|=|x|^n\times 1\underset{x\to 0}{\to}0$. Donc si $n\ge 1$, alors $f_n$ est prolongeable par cotinuité en 0$. Et si $n=0$, on peut prendre $x_n=\frac 1{2\pi n}$ et $y_n=\frac1{\pi+2\pi n}$. On a $x_n\to 0$ et $y_n\to 0$ mais $f_0(x_n)=1$ et $f_0(y_n)=-1$. Par caractérisation séquentielle de la limite, $f_0$ n'admet pas de limite en $0$ et donc $f_0$ n'est pas prolongeable par continuité.

		Soit maintenant $n\ge 1$. $f_n$ est donc prolongeable par continuité en $0$. Étudions sa dérivabilité : $\frac{f_n(x)-f_n(0)}{x-0}=\frac{x^ne^{\frac ix}-0}{x}=f_{n-1}(x)$. Ainsi, $f_n$ est dérivable en $0$ si et seulement si $f_{n-1}$ est prolongeable par continuité en $0$. C'est à dire ssi $n\ge2$. Dans ce cas, on a pour $x>0$ $f_n'(x)=(nx^{n-1}-\frac {ix^n}{x^2})e^{\frac ix}=(nx^{n-1}-ix^{n-2})e^{\frac ix}=nf_{n-1}(x)-if_{n-2}(x)$. Je pense que $f_n$ est alors de classe $\mathcal C^1$ ssi $n\ge 3$.



!!! exercice "RMS2022-678"
	=== "Enoncé"
		Soit $E$ un $\mathbb R$-espace vectoriel de dimension finie. Soient $p, q, r$ trois projecteurs de $E$.

		On suppose que $p+\sqrt{2}q+\sqrt{3}r$ est un projecteur.

		a) Montrer que la trace d'un projecteur est un entier naturel.

		b) Montrer que $q=r=0$.


	=== "Corrigé"
		a) Soit $p$ un projecteur de $E$. On note $V$ son image et $W$ son noyau. Soit $v_1,\dots,v_q,w_1,\dots,w_r$ une base adaptée à la décomposition $E=V\oplus W$. On a $p(v_i)=v_i$ et $p(w_k)=0$. La patrice de $p$ dans cette base est donc une matrice $J_q$, sa trace vaut $q\in\mathbb N$.

		b) Pour rappel, $\sqrt2$, $\sqrt3$ et $\sqrt6$ sont irrationnels.Posons $s=p+\sqrt2 q+\sqrt3r$. $p$ étant un projecteur, sa trace est un entier naturel, tout comme $q$ et $r$. Notons $\mathrm{tr}(q)=n_1$, $\mathrm{tr}(r)=n_2$. Par linéarité de la trace, nous avons donc $\mathrm{tr}(s-p)=\sqrt2n_1+\sqrt3n_2$. Or cette quantité est un entier $n_3$. On élève au carré : $n_3^2=2n_1^2+3n_2^2+\sqrt6n_1n_2$. On obtient $n_1n_2\sqrt6=n_3^2-2n_1^2-3n_2^2$. La partie droite est entière et la partie gauche est irrationnelle si elle est non nulle. C'est incompatible donc les deux parties sont nulles. On a donc $n_1n_2=0$. Si $n_1=0$ alors $n_3=\sqrt 3n_2$ et par les mêmes arguments, $n_3=n_2=0$. De même si $n_2=0$. On en déduit que $n_1=n_2=0$ et donc $q=r=0$.



!!! exercice "RMS2022-679"
	=== "Enoncé"
		Soient $n\in\mathbb N^*$, $A$ et $B\in\mathcal{M}_n(\mathbb R)$. Montrer que $\{M\in\mathcal{M}_n(\mathbb R),\ AMB=0\}$ est un sous-espace vectoriel de $\mathcal{M}_n(\mathbb R)$. Préciser sa dimension.


	=== "Corrigé"
		Notons $F_{A,B}=\{M\in\mathcal{M}_n(\mathbb R),\ AMB=0\}$. $F_{A,B}$ est non vide car il contient la maatrice nulle. Par ailleurs, si $M_1$ et $M_2$ sont deux éléments de $F_{A,B}$ et si $\lambda,\mu$ sont deux scalaires, alors $A(\lambda M_1+\mu M_2)B=\lambda AM_1B+\mu AM_2B=0$. Ainsi $\lambda M_1+\mu M_2\in F_{A,B}$ et donc $F_{A,B}$ est bien un sev de $\mathcal M_{n}(\mathbb R)$.

		Supposons dans un premier temps que $A=J_r$ et $B=J_q$. Soit $M$ une matrice de $\mathcal M_n(\mathbb R)$. Alors $AMB$ est la matrice extraite de $M$ dont on ne garde que le rectangle supérieur dont on ne garde que les $R$ premières lignes et les $q$ premières colonnes. Essayons de préciser un peu. Notons $\varphi$ l'application qui, à $M$ associe $AMB$. Si $1\le i\le r$ et si $1\le j\le q$ alors $AE_{ij}B=E_{ij}$ donc $E_{ij}\in\mathrm{im}(\varphi)$. Par ailleurs si $i>r$ ou $j>q$, alors $AE_{ij}B=0$ donc $E_{ij}\in\ker\varphi$. On en déduit que $\mathrm{rg}\varphi\ge r\times q$ et $\dim\ker\varphi\ge n^2-rq$. D'après le théorème du rang, on en déduit que ces inégalités sont des égalités et ainsi, puisque $F_{A,B}=\ker\varphi$, $\dim F_{J_r,J_q}=n^2-rq$.


		Passons maintenant au cas général : Si $A$ est de rang $r$ et si $B$ est de rang $q$ alors on peut écrire $A=VJ_r U$ et $B=V'J_qU'$.  Alors $M\in F_{A,B}$ ssi $AMB=0$ donc ssi $J_r(UMV')J_q=0$ puisque $V$ et $U'$ sont inversibles. On en déduit que $F_{A,B}$ est l'image par $\Phi:M\mapsto UMV'$ de $F_{J_r,J_q}$. Or $\Phi$ étant un isomorphisme, $\dim F_{A,B}=\dim F_{J_r,J_q}=n^2-rq$.




!!! exercice "RMS2022-680"
	=== "Enoncé"
		La relation $\cong$ sur $\mathcal{M}_n(\mathbb R)$ est définie par $A\cong B$ s'il existe $P\in\mathrm{GL}_n(\mathbb R)$ telle que $A=PB$.

		a)   Montrer que $\cong$ est une relation d'équivalence sur $\mathcal{M}_n(\mathbb R)$.

		b)   Caractériser les classes d'équivalence de $\cong$.



	=== "Corrigé"
		a) On doit montrer que la relation est symétrique, transitive et réflexive.

		Si $A$ est une matrice, alors $A=I_nA$ donc $A\cong A$. Donc $\cong$ est réflexive.

		Si $A$ et $B$ sont deux matrices telles que $A\cong B$ alors $A=PB$ avec $P$ inversible. On peut alors écrire $B=P^{-1}A$ et donc $B\cong A$.

		Enfin, si $A$, $B$ et $C$ sont trois matrices telles que $A\cong B$ et $B\cong C$ alors on peut écrire $A=PB$ et $B=QC$ avec $P$ et $Q$ inversibles. On a alors $A=PQC$ et $PQ$ est inversible. On en déduit que $A\cong C$.

		Ainsi la relation $\cong$ est réflexive.

		b) Soit $A$ et $B$ deux matrices congruentes. $A$ peut être réduite par ligne en une matrice $R$ unique. On a alors $A=PR$ avec $P$ inversible représentant les opérations sur les lignes. Or $B=QA$ avec $Q$ inversible. On en déduit $B=QPR$ et donc par unicité de la forme réduite par lignes, $A$ et $B$ ont la même forme réduite. Cette matrice $R$ est donc caractéristique de la classe d'équivalence.



!!! exercice "RMS2022-681"
	=== "Enoncé"
		Soit $J=\begin{pmatrix}0&1&0&...&0\\ 0&0&1&&\vdots\\ \vdots&&\ddots&\ddots&0\\0&&&0&1\\0&0&\cdots &0&0\end{pmatrix}\in{\cal M}_p(\mathbb R)$.

		a) Soit $A$ telle que $A^p=0$ et $A^{p-1}\not=0$. Montrer que $A$ est semblable à $J$.

		b) Soit $A$ telle que $A^p=0$ et rang $(A)=p-1$. Montrer que $A$ est semblable à $J$.



	=== "Corrigé"
		a) Soit $u$ l'application linéaire canoniquement associée à $A$, on a $u\in\mathcal L(\mathbb R^p)$. On a donc $u^{p-1}\ne 0$ et $u^p=0$. Soit $x$ tel que $u^{p-1}(x)\ne 0$. On peut montrer que la famille $(u^{p-1}(x),u^{p-2}(x),\dots,u(x),x)$ est libre et puisqu'elle contient $p$ vecteurs, c'est une base de $\mathbb R^p$. La matrice de $u$ dans cette base est donc la matrice $J$. On en déduit que $A$ est semblable à $J$.

		b) Soit $A$ telle que $A^p=0$ de rang $p-1$. Soit comme précédemmet $u$ l'application linéaire canoniquement associée à $A$. On a donc $\dim\ker(u))=1$ d'après le théorème du rang appliqué à $u$. Notons $u_1=u_{|\mathrm{im}(u)}$. Alors $\ker(u_1)=\ker u\cap\mathrm{im}(u)$ et $\mathrm{im}(u_1)=\mathrm{im}(u^2)$. En appliquant le théorème du rang à $u_1$, on obtient $\dim(\mathrm{im}(u))=\dim\ker(u_1)+\dim(\mathrm{im}(u_1))=\dim(\ker u\cap\mathrm{im}(u))+\dim(\mathrm{im}(u^2))\ge \dim(\ker u)+\dim(\mathrm{im}(u^2))\le 1+\dim(\mathrm{im}(u^2))$. On en déduit que $\dim(\mathrm{im}(u^2))\le p-2$. En itérant ce processus, on trouve $\dim(\mathrm{im}(u^{p-1}))\ge p-(p-1)=1$. On en déduit que $u^{p-1}\ne 0$. On peut donc dire que $A^{p-1}\ne 0$ et on se ramène au cas précédent.



!!! exercice "RMS2022-682"
	=== "Enoncé"
		Soient $E$ un espace vectoriel, $u\in{\cal L}(E)$ nilpotent d'indice $p$ et $S$ un sous-espace de $E$ stable par $u$ vérifiant $E=\mathrm{im} (u) +S$.

		a)   Montrer que, pour tout $k\geq 1$, $E=\mathrm{im}( u^k)+S$.

		b)   En déduire que $S=E$.



	=== "Corrigé"
		a) On peut faire un raisonnement par récurrence. Soit $k\ge 1$, on pose $H_k=$\og$E=\mathrm{im}(u^k)+S$\fg. L'énoncé nous donne l'initialisation. Concentrons nous sur l'hérédité. Soit $k\ge 1$, supposons que $H_k$ soit vraie. Montrons $H_{k+1}$.

		On sait bien sûr que $\mathrm{im}(u^{k+1})+S\subset E$. C'est donc l'inclusion réciproque qui nous intéresse. Soit $x\in E$, par hypothèse de récurrence, on peut écrire $x=y_1+s_1$ avec $x_1\in\mathrm{im}(u^k)$ et $s_1\in S$. Posons $y_1=u^k(x_1)$. $x_1\in E$ donc $x_1=u(x_0)+s_0$ d'après l'énoncé (ou $H_1$). On a alors $x=u^k(u(x_0)+s_0)+s_1=u^{k+1}(x_0)+u^{k}(s_0)+s_1$. Or $S$ est stable par $u$ donc par $u^k$ donc $u^k(s_0)+s_1=s\in S$. On a donc $x=u^{k+1}(x_0)+s\in\mathrm{im}(u^{k+1})+S$. Ainsi $H_{k+1}$ est vraie.

		On a donc montré par récurrence que pour tout $k\ge 1$, $E=\mathrm{im}(u^{k})+S$.

		b) En particulier, pour $k=p$, on a $u^k=0$ et donc $E=S$.  



!!! exercice "RMS2022-683"
	=== "Enoncé"
		a) Étudier la suite $(u_n)_{n\in\mathbb N}$ définie par $u_0\in\mathbb R^*$ et $\forall n\in\mathbb N$, $u_{n+1}=\frac 1 2 \left(u_n+\frac{1}{u_n} \right)$.

		 Soit $p\in\mathbb N^*$. Soit $N\in{\cal M}_p(\mathbb R)$ telle que $N^p=0$. On pose $A=I_p+N$.

		b) Montrer que $A$ est inversible.

		c) Justifier l'existence et étudier la suite de matrices $(M_n)_{n\in\mathbb N}$ définie par :

		 $M_0=A$ et $\forall n\in\mathbb N$, $M_{n+1}=\frac{1}{2}(M_n+M_n^{-1})$.



	=== "Corrigé"
		a) Si $u_0>0$, on montre que $u_n>1$ pour tout $n\ge 1$ puis que $u$ est décroissante à partir du rang $1$. On en déduit que $u$ est convergente vers un point fixe de la relation de récurrence. Soit $\ell$ la limite, on a $\ell=\frac 12(\ell+\frac 1\ell)$. On a donc $\ell^2=1$ et donc $\ell=1$. La situation symétrique se produit quand $u_0<0$.  

		b) Soit $N$ nilpotente d'indice $d$ et $A=I+N$. On a $I=I^d-(-N)^d =A\sum_{k=0}^{d-1}(-N)^k$. Donc $A$ est inversible, d'inverse $\sum_{k=0}^{d-1}(-N)^k$. On a alors si $d>1$, $A+A^{-1}=A+\sum_{k=0}^{d-1}(-N)^k=A+I-N+\sum_{k=2}^{d-1}(-N)^k=2I+\sum_{k=2}^{d-1}(-N)^k$ et donc $\frac 12(A+A^{-1})=I+N^2\sum_{k=2}^{d-1}(-N)^k$. Or   $N^2\sum_{k=2}^{d-1}(-N)^k$ est nilpotente d'indice $\le \frac d2$. On en déduit que dans les éléments de la suite $M_n$ sont tels que $M_n=I+N_n$ où $N_n$ est une matrice nilpotente d'indice de nilpotence $d_n$ avec la relation $d_{n+1}\le \frac{d_n}2$. On en déduit que la suite d'entiers $d_n$ est strictement décroissante et elle est donc stationnaire à partir d'un certain rang avec la valeur $d=1$. On en déduit qu'à partir d'un certain rang, $M_n=I$ : la suite $M_n$ est donc constante égale à $I$ à partir d'un certain rang.



!!! exercice "RMS2022-684"
	=== "Enoncé"
		Soient $E=\mathbb R_3[X]$, $A=X^4-1$ et $B=X^4-X$.

		Pour $P\in E$, on note $f(P)$ le reste de la division euclidienne de $AP$ par $B$.

		a) Montrer que $f$ est un endomorphisme.

		b) Déterminer $\ker f$ et calculer le rang de $f$.

		c) Étudier la diagonalisabilité de $f$.



	=== "Corrigé"
		a)  Soit $P\in\mathbb R_3[X]$, alors $f(P)$ est un reste de division euclidienne par $X^4-X$. $f(P)$ est donc un polynôme de degré au plus $3$ donc l'image de $f$ est bien inclue dans $\mathbb R_3[X]$.
		Soient $P_1$ et $P_2$ deux polynômes de $\mathbb R_3[X]$, on peut poser $AP_1=Q_1B+f(P_1)$ et $AP_2=Q_2B+f(P_2)$. Prenons $\lambda$ et $\mu$ deux réels,

		\begin{align*}
		A(\lambda P_1+\mu P_2)&=\lambda AP_1+\mu AP_2\\
		&=\lambda Q_1B+\lambda f(P_1)+\mu Q_2 B+\mu f(P_2)\\
		&=(\lambda Q_1+\mu Q_2)B+\lambda f(P_1)+\mu f(P_2)
		\end{align*}

		 Or $\lambda f(P_1)+\mu f(P_2)$ est un polynôme de degré inférieur à 3. Par unicité du reste de la division euclidienne, on a bien $f(\lambda P_1+\mu P_2)=\lambda f(P_1)+\mu f(P_2)$. Ainsi $f$ est un endomorphisme de $\mathbb R_3[X]$.

		b) Si $P\in\ker f$ alors $AP=QB$. On a alors $(X-1)(X^3+X^2+X+1)P=X(X-1)(X^2+X+1)Q$. D'après le théorème de Gauss, $P$ est divisible par $X^3+X^2+X$ qui est un polynôme de degré $3$. Ainsi $P=\lambda(X^3+X^2+X)$. Réciiproquement si $P$ est de cette forme alors $B$ divise $AP$ et le reste est nul donc $f(P)=0$. Ainsi $\ker f=<X^3+X^2+X>$ est de dimension $1$ et le rang de $f$ est donc $3$.

		c) Calculons la matrice de $f$ dans la base canonique. $f(1)=X-1$, $f(X)=X^2-X$, $f(X^2)=X^3-X^2$ et $f(X^3)=-X^3+X$. La matrice de $f$ dans la base canonique est donc $\begin{pmatrix} -1 & 0 & 0 & 0\\ 1 & -1 & 0 & 1 \\ 0 & 1 & -1 & 0\\ 0 & 0 & 1 & -1\end{pmatrix}$ dont le polynôme caractéristique est $X(X+1)(X^2+3X+3)$. Le polynôme caractéristique n'étant pas scindé, la matrice, et donc l'endomorphisme, n'est pas diagonnalisable.




!!! exercice "RMS2022-685"
	=== "Enoncé"

		Soient $\alpha$, $\beta$, $\gamma\in\mathbb R$, $A=\begin{pmatrix}\alpha^2&\alpha\beta&\alpha\gamma\\
		\alpha\beta&\beta^2&\beta\gamma\\
		\alpha\gamma&\beta\gamma&\gamma^2 \end{pmatrix}$ et $f$ l'endomorphisme canoniquement associé à la matrice $A$.

		a)  Montrer qu'il existe un vecteur colonne $C$ tel que $A=C{C}^T$.

		b)   Déterminer le noyau et l'image de $A$

		c)   Chercher les éléments propres de $A$. La matrice $A$ est-elle diagonalisable?

		d)   Retrouver le résultat en remarquant que $f$ est proportionnel à un projecteur.




	=== "Corrigé"
		a) On remarque que si on pose $C=\begin{pmatrix} \alpha \\ \beta \\ \gamma\end{pmatrix}$ alors $A=CC^T$.

		b) $A$ est une matrice de rang $1$. Son image est engendrée par $C$. D'après le théorème du rang, son noyau est de dimension $2$. Le noyau est le plan d'équation $\alpha x+\beta y+\gamma z=0$.

		c) $0$ est clairement valeur propre de multiplcité 2 avec un sous-espace propre de dimension $2$. Par ailleurs $AC=CC^TC=C(C^TC)=C||C||^2=||C||^2C$. Donc $C$ est vecteur propre associé à la valeur propre $||C||^2$. $A$ est bien diagonalisable.

		d) Calculons $A^2=CC^TCC^T=C(C^TC)C^T=||C||^2A$. Si on pose $u=\frac1{||C||^2}f$, on obtient $u^2=\frac1{||C||^4}f^2=\frac1{||C||^4}||C||^2f=u$. Ainsi $u$ est un projeteur, donc diagonalisable. $f$ étant proportionnel à $u$, $f$ est aussi diagonalisable.



!!! exercice "RMS2022-686"
	=== "Enoncé"
		Soit $f : M\in \mathcal{M}_n(\mathbb R)\mapsto M+M^T\in \mathcal{M}_n(\mathbb R)$. 	

		a) Montrer que $f$ est un endomorphisme de $\mathcal{M}_n(\mathbb R)$.

		b) Montrer que $f$ est diagonalisable et donner ses espaces propres.


	=== "Corrigé"
		a) $f$ est clairement linéaire de $\mathcal M_n(\mathbb R)$ dans $\mathcal M_n(\mathbb R)$.

		b) Si $M$ est symétrique, on a $f(M)=2M$ et si $M$ est antisymétrique, $f(M)=0$. L'ensemble des matrices symétriques et antisymétrique sont supplémentaires dans $\mathcal M_n(\mathbb R)$. $f$ est donc diagonalisable avec deux sous-espaces propres $A_n$ et $S_n$ associés respectivement aux valeurs propres $0$ et $2$.



!!! exercice "RMS2022-688"
	=== "Enoncé"
		Soient $A\in\mathcal{M}_n(\mathbb C)$  nilpotente et $P\in\mathbb C[X]$.

		a) Soient $\lambda\in\mathbb C$ et $M=A+\lambda I_n$. Montrer que $P(M)$ est diagonalisable si et seulement si $P(M)$ est une matrice scalaire (c'est à dire de la forme $\alpha I_n$).

		b) Soit $B\in\mathcal{M}_n(\mathbb C)$ diagonalisable telle que $AB=BA$. On pose  $M=A+B$. Montrer que $P(M)$ est diagonalisable si et seulement si  $P(M)$ est un polynôme en $B$.



	=== "Corrigé"
		a) Evidemment si $P(M)$ est une matrice scalaire, alors elle est diagonalisable. Supposons donc que $P(M)$ est diagonalisable. D'après la formule de Taylor en $\lambda$, si on note $d$ le degré de $p$, on peut écrire $P=P(\lambda)+\sum_{k=1}^d \alpha_i (X-\lambda)^i$ et donc $P(M)=\lambda I_n+\sum_{k=1}^dA^i$. Or $A$ est nilpotente donc semblable à une matrice de la forme
		$Q^{-1}AQ=\begin{pmatrix}
		0 & \cdots & 0 & 1      & 0      & \cdots & 0      \\
		  & \ddots &   & \ddots & \ddots & \ddots & \vdots \\
		  &        & \ddots &        & \ddots & \ddots & 0 \\
		  &        &        & \ddots &        & \ddots & 1 \\
		  & 0      &        &        & \ddots &        & 0 \\
		  &        &        &        &        & \ddots & \vdots\\
		  &        &        &        &        &        & 0
		\end{pmatrix}$
		La matrice $Q^{-1}P(M)Q$ est donc triangulaire supérieure avec des $P(\lambda)$ sur la diagonale. On en déduit que $P(\lambda)$ est l'unique valeur propre de $P(M)$ et donc que $P(M)$ est semblable à $P(\lambda)I_n$ et ainsi $P(M)$ est de la forme $P(\lambda)I_n$.

		b) encore une fois la réciproque est triviale.

		Soit $u$ l'application linéaire canoniquement associée à $M$, $a$ à $A$ et $b$ à $B$. Soit $E_\lambda$ un sous-espace propre de $b$. Soit $x\in E_\lambda$, alors

		\begin{align*}
		P(u)(x)&=\sum_{i=0}^d p_iu^i(x)\\
		&=\sum_{i=0}^d p_i\sum_{k=0}^i\binom ik a^kb^{i-k}(x)\\
		&=\sum_{i=0}^d p_i\sum_{k=0}^i\binom ik a^k\lambda^{i-k}(x)\\
		&=\sum_{i=0}^d p_i\sum_{k=0}^i\binom ik a^k\lambda^{i-k}id_E^{i-k}(x)\\
		&=\sum_{i=0}^d p_i(a+\lambda id_E)(x)\\
		&=P(a+\lambda id_E)(x)
		\end{align*}


		Or $P(M)$ est diagonalisable donc la restriction de $u$ à $E_\lambda$ est diagonalisable. D'après la question précédente, on en déduit que la restriction de $u$ à $E_\lambda$ est une homothétie de rapport $P(\lambda)$.   $B$ étant diagonalisable, les sous-espaces propres sont en somme directe dans $E$. Prenons une base de vacteurs propres de $b$. Si on note $\lambda_i$ les valeurs propres de $B$ (les $\lambda_i$ sont éventuellement égaux), la matrice de $b$ dans cette base est $\mathrm{diag}(\lambda_1,\dots,\lambda_n)$ et la matrice de $P(u)$ est donc diagonale de la forme $\mathrm{diag}(P(\lambda_1),\dots,P(\lambda_n))$. On en déduit que $P(u)=P(b)$ et donc $P(M)=P(B)$.



!!! exercice "RMS2022-689"
	=== "Enoncé"
		Soient $a\in\mathbb R$ et $b\in\mathbb R^*$.

		a)   Soit $A\in\mathcal{M}_n(\mathbb R)$ telle que $A^2$ soit diagonalisable à valeurs propres strictement positives. Montrer que $A$ est diagonalisable.

		b)   Diagonaliser $B=(b_{i,j})_{1\leq i,j\leq n}\in\mathcal{M}_n(\mathbb R)$,  où $b_{i,i}=a$ et $b_{i,j}=b$ si $i\ne j $.

		c)   Diagonaliser $C=(c_{i,j})_{1\leq i,j\leq n}\in\mathcal{M}_n(\mathbb R)$, où  $c_{i,n+1-i}=a$  et $c_{i,j}=0$ si $j\ne n+1-i$.



	=== "Corrigé"

		a) $A^2$ est diagonalisable à valeurs propres strictement positives. Notons $\lambda_1,\dots,\lambda_p$ ses valeurs propres. Alors $A^2$ annule le polynôme $\prod_{i=1}^p(X-\lambda_i)$. On a donc $0=\prod_{i=1}^p(A^2-\lambda_iI)=\prod_{i=1}^p(A-\sqrt{\lambda_i}I)(A+\sqrt{\lambda_i}I)$. Ainsi $A$ annule le polynôme $\prod_{i=1}^p(X-\sqrt{\lambda_i})(X+\sqrt{\lambda_i})$ qui est scindé à racines simples. Donc $A$ est diagonalisable.

		b) Notons $X=\begin{pmatrix}1\\\vdots\\1\end{pmatrix}$, puis pour $i$ entre $1$ et $n-1$, $X_1=\begin{pmatrix}1\\-1\\0\\\vdots\\0\end{pmatrix}$, $X_2=\begin{pmatrix}0\\1\\-1\\0\\\vdots\\0\end{pmatrix}$,$\dots$, $X_{n-1}=\begin{pmatrix}0\\\vdots\\0\\1\\-1\end{pmatrix}$. On a $BX=(a+(n-1)b)X$ et $BX_i=(a-b)X_i$. La famille $(X,X_1,\dots,X_{n-1})$. Si $P$ est la matrice dont les colonnes sont les vecteurs de cette base, alors $P^{-1}BP=\mathrm{diag}(a+(n-1)b,a-b,\dots,a-b)$.

		c) On suppose $a\ne 0$ sinon la matrice est nulle. On peut calculer que $C^2=a^2I_n$ donc $C^2$ est diagonalisable à valeurs propres strictement positives. Donc $C$ est diagonalisable de valeurs propres $a$ et $-a$ (il ne peut pas y avoir que $a$ puisque $C\ne aI_n$). $C'=\frac 1aC$ est la matrice d'une symétrie, elle est donc diagonalisable sous la forme $\mathrm{diag}(1,\dots,1,-1,\dots,-1)$. Si $n$ est pair, alors $\mathrm{tr}(C')=0$ et alors le nombre de $1$ et de $-1$ sont les mêmes. Si $n$ est impair, alors $\mathrm{tr}(C')=1$ et il y a un $1$ de plus que de $-1$ : $\frac {n-1}2+1$ fois le $1$ et $\frac {n-1}2$ fois le $-1$. On en déduit la diagonalisation de $C$.



!!! exercice "RMS2022-690"
	=== "Enoncé"
		Soient $M\in\mathcal{M}_2(\mathbb Z)$ et $n\in\mathbb N^*$ tels que $M^n=I_2$. Montrer que $M^{12}=I_2$.\\
		**Ind** : Montrer que $M$ est $\mathbb C$-diagonalisable et considérer $\mathrm{tr}(M)$.


	=== "Corrigé"
		Soit donc $M\in\mathcal{M}_2(\mathbb Z)$ et $n\ne 0$ tel que $M^n=I_2$. M annule donc le polynôme $X^n-1$ qui est scindé à racines simples dans $\mathbb C$. Ainsi, $M$ est $\mathbb C$-diagonalisable à valeurs propres dans $\mathbb U_n$. Or la trace de $M$ est entière (car somme de deux entiers). Soit $k_1$ et $k_2$ telles que $\exp(\frac{2ik_1\pi}n)$ et $\exp(\frac{2ik_2\pi}n)$ sont valeurs propres de $M$ alors $|\mathrm{tr}(M)|=|\exp(\frac{2ik_1\pi}n)+\exp(\frac{2ik_2\pi}n)|\le 2$. Il est donc nécessaire que $\mathrm{tr}(M)\in\{-2,-1,0,1,2\}$. De plus $|\det(M)|\in\mathbb Z=|\exp(\frac{2ik_1\pi}n)\times\exp(\frac{2ik_2\pi}|=1$ donc $\det(M)=\pm1$. Le polynôme caractéristique de $M$ est $X^2-\mathrm{tr}(M)X+\det(M)$ et puisque l'on a $5$ choix pour la trace et 2 pour le déterminant, cela laisse $10$ polynômes caractéristiques possibles :

		- $X^2-2X+1=(X-1)^2$ qui n'est le polynôme caractéristique d'une matrice diagonalisable que si $M=I_2$.  

		- $X^2-2X-1$ dont les racines ne sont pas de module $1$ : ce n'est pas un cas possible  

		- $X^2-X+1=(X+j)(X+\bar j)$ dans ce cas $M$ est semblable à $\begin{pmatrix}-j & 0\\0 & -\bar j\end{pmatrix}$  

		- $X^2-X-1$ dont les racines ne sont pas de module $1$ : ce n'est pas un cas possible  

		- $X^2+1=(X-i)(X+i)$ qui convient, dans ce cas $M$ est semblable à $\begin{pmatrix} i & 0 \\ 0 & -i \end{pmatrix}$  

		- $X^2-1=(X-1)(X+1)$ qui convient, dans ce cas $M$ est semblable à $\begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}$  

		- $X^2+X+1=(X-j)(X-\bar j)$ qui convient, dans ce cas $M$ est semblable à $\begin{pmatrix} j & 0 \\ 0 & \bar j\end{pmatrix}$  

		- $X^2+X-1$ dont les racines ne sont pas de module $1$  

		- $X^2+2X+1=(X+1)^2$ qui n'est le polynôme d'une matrice diagonalisable que si $M=-I_2$  

		- $X^2+2X-1$ dont les racines ne sont pas de module $1$.   

		Dans tous les cas utile, on remarque que $M^{12}=I_2$ car $j^12=(-j)^12=i^{12}=1^{12}=(-1)^{12}=\dots=1$.




!!! exercice "RMS2022-691"
	=== "Enoncé"
		Soient $A$, $B\in\mathcal{M}_2(\mathbb C)$. Pour  $z\in \mathbb C$, soit $\Delta(z)$ le discriminant du polynôme $\chi_{A+zB}$.

		a)   On suppose  $B$ diagonalisable et $\Delta$ de degré au plus $1$. Montrer que $AB=BA$.

		b)   Exhiber $B$ telle que $\Delta$ soit de degré au plus $1$ et $AB\neq BA$.

		c)   On suppose que, pour tout $z\in \mathbb C$, $A+zB$ est diagonalisable. A-t-on $AB=BA$?




	=== "Corrigé"
		a) Commençons par traiter le cas $B$ diagonale. $\chi_{A+zB}=X^2-\mathrm{tr}(A+zB)X+\det(A+zB)$ dont le discriminant vaut $\mathrm{tr}(A+zB)^2-4\det(A+zB)=(a_{11}+a_{22}+z(\lambda_1+\lambda_2))^2-4((a_{11}+z\lambda_1)(a_{22}+z\lambda_2)-a_{12}a_{21})$. Le coefficient en $z^2$ vaut $(\lambda_1+\lambda_2)^2-4\lambda_1\lambda_2=(\lambda_1-\lambda_2)^2$. Puisque $\Delta(z)$ est de degré au plus $1$, $\lambda_1=\lambda_2$ et donc $B$ est de la forme $\alpha I_2$ : elle commute avec toutes les matrices, en particulier $A$.

		a) Commençons par traiter le cas $B$ diagonale. $\chi_{A+zB}=X^2-\mathrm{tr}(A+zB)X+\det(A+zB)$ dont le discriminant vaut $\mathrm{tr}(A+zB)^2-4\det(A+zB)=(a_{11}+a_{22}+z(\lambda_1+\lambda_2))^2-4((a_{11}+z\lambda_1)(a_{22}+z\lambda_2)-a_{12}a_{21})$. Le coefficient en $z^2$ vaut $(\lambda_1+\lambda_2)^2-4\lambda_1\lambda_2=(\lambda_1-\lambda_2)^2$. Puisque $\Delta(z)$ est de degré au plus $1$, $\lambda_1=\lambda_2$ et donc $B$ est de la forme $\alpha I_2$ : elle commute avec toutes les matrices, en particulier $A$.

		b) La question me semble fausse car pour $A=0$ on ne peut pas trouver de telle $B$.

		c) En particulier $A$ est diagonalisable ($z=0$). Concentrons nous sur le cas $A$ diagonale, il n'est pas difficile de généraliser. En effet si la propriété est vraie pour $A$ diagonale, alors si $A$ n'est pas diagonale, on prend $P$ inversible telle que $P^{-1}AP=D$ diagonale. On a alors $A+zB$ diagonalisable donc $D+zPBP^{-1}$ diagonalisable pour tout $z$. On en déduirait que $D$ et $PBP^{-1}$ commutent : $DPBP^{-1}=PBP^{-1}D$ et en multipliant par $P^{-1}$ à gauche et $P$ à droite on aurait $AB=BA$.

		Étudions donc le cas où $A$ est diagonale.

		Si $A$ est une matrice scalaire, alors clairement $A$ et $B$ commutent. On se place donc dans le cas où $A=\mathrm{diag}(a_1,a_2)$ avec $a_1\ne a_2$.

		On sait que $A+zB$ est diagonalisable. Donc $A+zB$ annule un polynôme scindé à racines simples. Si il existe $z_0$ tel que ce polynôme soit de degré $1$ alors $A+z_0B$ est de la forme $\alpha I_2$ et donc $z_0B=\alpha I-A$ et ainsi $B$ est diagonale. On en déduit que $A$ et $B$ commutent car deux matrices diagonales commutent.


		Supposons donc maintenant que pour tout $z$, le polynôme scindé à racines simple annulé  soit de degré $2$. C'est donc le polynôme caractéristique et on en déduit que $\Delta(z)\ne 0$ pour tout $z$. C'est donc un polynôme en $z$ de degré au plus deux mais sans racines : c'est un polynôme constant.

		$\Delta(z)=(\mathrm{tr}(A+zB))^2-4\det(A+zB)=\mathrm{tr}(A)^2+2z\mathrm{tr}(A)\mathrm{tr}(B)+z^2\mathrm{tr}(B)^2-4\det(A+zB)$ et $det(A+zB)=(a_1+zb_{11})(a_2+zb_{22})-z^2b_{12}b_{21}=z^2\det(B)+\det(A)+z(a_2b_{11}+a_1b_{22})$. Ainsi $\Delta(z)=z^2(\mathrm{tr}(B)^2-4\det(B))+z(2\mathrm{tr}(A)\mathrm{tr}(B)-4(a_2b_{11}+a_1b_{22}))+\mathrm{tr}(A)^2-4\det(A)$. On a donc en particulier $\mathrm{tr}(B)^2-4\det(B)=0$ c'est à dire $(b_{11}-b_{22})^2+4b_{12}b_{21}=0$

		Le coefficient en $z$ est nul aussi, or celui-ci vaut $2(a_1-a_2)(b_{11}-b_{22})$. Puisque $a_1\ne a_2$ on en déduit $b_{11}=b_{22}$ et ainsi que $b_{12}b_{21}=0$. Supposons que $b_{21}=0$, alors $B$ est triangulaire supérieure avec des termes égaux sur la diagonale. Peut-on alors affirmer que $AB=BA$ ?

		Prenons par exemple $B=\begin{pmatrix}1&1\\0&1\end{pmatrix}$. On a alors $AB=\begin{pmatrix} a_1 & a_1 \\ 0 & a_2\end{pmatrix}$ et $BA=\begin{pmatrix}a_1 & a_2\\0 & a_2\end{pmatrix}$. On a donc $AB\ne BA$ et pourtant pour tout $z$, $A+zB$ est diagonalisable puisque le polynôme caractéristique est scindé à racines simples. On a donc pas nécessairement $AB=BA$.  



!!! exercice "RMS2022-692"
	=== "Enoncé"
		Soient $E$ un $\mathbb R$-espace vectoriel de dimension finie et $f$ un endomorphisme de $E$ tel que $f^2$ soit un projecteur.

		a)  Préciser un polynôme annulateur de $f$.

		b) Montrer qu'il existe deux sous-espaces supplémentaires $F$ et $G$ stables par $f$ tels que l'endomorphisme induit par $f$ sur $F$ soit inversible et l'endomorphisme induit par $f$ sur $G$ soit nilpotent.

		c) Montrer que $f$ est diagonalisable si et seulement si  $\mathrm{rg} (f^2)=\mathrm{rg} (f)$.



	=== "Corrigé"
		a) $f^2$ est un projecteur donc $(f^2)^2=f^2$. $f$ annule donc le polynôme $X^4-X^2$.

		b) $f$ annule donc $X^4-X^2=X^2(X^2-1)$. D'après le lemme de décomposition des noyaux, $E=\ker(f^2)\oplus\ker(f^2-id)$. On montre facilement que ces deux sous-espaces sont stables par $f$. Soit $x\in\ker f^2$, alors $f^2(x)=0$ : $f$ est bien nilotent d'ordre $2$ sur $\ker(f^2)$. De même sur $\ker(f^2-id)$, on a $f^2=id$ et donc $f$ est inversible. On pose donc $F=\ker(f^2-id)$ et $G=\ker(f^2)$.

		c) La restriction de $f$ à $\ker(f^2-id)$ est une symétrie. Elle est donc diagonalisable. Ainsi $f$ est diagonalisable ssi la restrition à $G=\ker f^2$ de $f$ est diagonalisable. Or sur $G$, $f$ est nilpotente : elle est diagonalisable ssi elle est nulle. Notons $g$ la restriction de $f$ à $G$. $g$ est nulle ssi $\mathrm{rg}(g)=0$. On a $\ker g=\ker(f)\cap \ker f^2$. D'après le théorème du rang appliqué à $g$, on a $\dim\ker f^2=\mathrm{rg} g+\dim\ker g=\mathrm{rg} g+\dim(\ker f\cap\ker f^2)$. Ainsi $g$ est nulle ssi $\dim(\ker f\cap\ker f^2)=\dim\ker f^2$ càd ssi $\ker f^2=\ker f$. Or d'après le théorème du rang appliqué à $f$ et $f^2$, $\dim\ker f+\mathrm{rg} f=\dim\ker f^2+\mathrm{rg} f^2$ donc la condition $\ker f^2=\ker f$ est équivalente à le condition $\mathrm{rg} f=\mathrm{rg} f^2$. Finalement, $f$ est diagonalisable ssi $\mathrm{rg} f=\mathrm{rg} f^2$.



!!! exercice "RMS2022-693"
	=== "Enoncé"
		Soient $E$ le $\mathbb R$-espace vectoriel des fonctions de classe ${\cal C}^{\infty}$ de $\mathbb R$ dans $\mathbb R$, $p$ et $q$ deux réels avec $p+q=1$ et $p\in ]-1,0[\cup]0,1[$.
		On pose $u(f)=g$ avec $g:x\mapsto f(px+q)$.

		a) Montrer que $u$ est un automorphisme de $E$.

		b) Montrer que les valeurs propres de $u$ sont dans $]-1,1]$.



	=== "Corrigé"
		a) Soit $f$ une fonction de $E$. Alors l'application $x\mapsto f(px+q)$ est évidemment dans $E$ par théorèmes généraux.  
		Soit $\lambda,\mu$ éléments de $\mathbb R$, on a $u(\lambda f+\mu g)(x)=(\lambda f+\mu g)(px+q)=\lambda f(px+q)+\mu g(px+q)=\lambda u(f)(x)+\mu u(g)(x)=(\lambda u(f)+\mu u(g))(x)$ donc $u(\lambda f+\mu g)=\lambda u(f)+\mu u(g)$. Ainsi $u$ est linéaire. Posons enfin $v$ définie par $v(f)(x)=f(\frac 1p x-\frac qp)$. Alors $v$ est linéaire de $E$ dans $E$ pour les mêmes raisons que $u$ et $u\circ v(f)(x)=f(\frac 1p(px+q)-\frac pq=f(x)$ donc $u\circ v=id$ et de même $v\circ u=id$. Donc $u$ est un automorphisme de $E$.

		b) Soit $f$ un vecteur propre (donc $f$ non nulle) de $u$ associé à la valeur propre $\lambda$. Alors pour tout $x$ dans $\mathbb R$, $f(px+q)=\lambda f(x)$. Si $f(1)$ est différent en $0$ on a $f(p+q)=\lambda f(1)$ c'est à dire $\lambda=1$ (car $p+q=1$).

		Supposons $f(1)=0$. Soit $x$ tel que $f(x)\ne 0$. On remarque que pour $n$ un entier, $u^n(f)(x)=f(p^nx+\sum_{k=0}^{n-1}p^kq)\rightarrow f(1)$ par continuité. Mais par ailleurs $u(f)(x)=\lambda f(x)$ et donc $u^n(f)(x)=\lambda^n f(x)$. Si $f(1)=0$ alors $\lambda^nf(x)\to 0$ par unicité de la limite et donc $\lambda\in]-1;1[$.



!!! exercice "RMS2022-694"
	=== "Enoncé"
		Soit $E$ un espace euclidien muni d'une base orthonormée $(e_1,\ldots,e_n)$.
		On considère une famille $(u_1,\ldots,u_n)$ de vecteurs de $E$ telle que, pour tout $k\in[\![1,n]\!]$, $\lVert u_k\rVert=\frac1n$. Est-ce que la famille $(e_1+u_1,\ldots,e_n+u_n)$ est une base de $E$?




	=== "Corrigé"
		Posons $\displaystyle\varphi(x)=x+\sum_{i=1}^n(x|e_i)u_i$. $\varphi$ est évidemment une application linéaire de $E$ dans $E$. On remarque que pour $k\in\{1,\dots,n\}$, $\varphi(e_k)=e_k+u_k$. Ainsi la famille des $e_k+u_k$ est une base si et seulement si $\varphi$ est injective.

		Soit $x\in\ker\varphi$. On a alors $x=-\displaystyle\sum_{i=1}^n(x|e_i)u_i$. On a alors :

		\begin{align*}
		||x|| &\le \sum_{i=1}^n\big|(x|e_i)\big|||u_i||\\
		&\le \frac 1n\sum_{i=1}^n\big|(x|e_i)\big|\times 1\\
		&\le \frac 1n||x||\sqrt n\\
		&\le \frac{||x||}{\sqrt n}
		\end{align*}

		Or $n>1$ a priori donc $||x||=0$. On en déduit que $\varphi$ est injective, c'est un endomorphisme donc un automorphisme : l'image d'une base est une base, donc $(e_1+u_1,\dots,e_n+u_n)$ est une base.




!!! exercice "RMS2022-696"
	=== "Enoncé"
		Soit $J_n$ la matrice carrée de taille $n$ dont tous les coefficients valent $1$.

		a) Montrer que $J_n$ est diagonalisable et qu'il existe une matrice orthogonale $P_n\in{\cal O}_n(\mathbb R)$
		telle que : $J_n=P_n \mathrm{diag}(0,\ldots,0,n)P_n^{-1}$.

		b) Calculer $P_2$ et $P_3$.

		c) Montrer que l'espace vectoriel des matrices de ${\cal M}_3(\mathbb R)$ commutant avec $J_3$ est de dimension $5$.




	=== "Corrigé"
		a) une remarque classique est que $J_n^2=nJ_n$. Donc $J_n$ annule le polynôme caractéristique $X^2-nX$ : ainsi $J_n$ annule un polynôme scindé à racines simples, elle est diagonalisable, de valeurs propres $0$ et $n$. $J_n$ étant clairement de rang $1$, $0$ est de multiplicité $n-1$ et $n$ de multiplicité $1$. Or $J_n$ est symétrique, donc diagonalisable dans une base orthonormée : il existe $P\in\mathcal O_n$ telle que $P^{-1}J_nP=\mathrm{diag}(0,\ldots,0,n)$. On remarque par ailleurs que le vecteur $\begin{pmatrix}1\\\vdots\\1\end{pmatrix}$ est un vecteur propre associé à la valeur propre $n$ et que les vecteurs $\begin{pmatrix}0\\\vdots\\0\\1\\-1\\0\\\vdots\\0\end{pmatrix}$ sont dans le noyau de $J_n$. Ces vecteurs forment clairement une base de $\mathcal M_{n,1}(\mathbb R)$.

		b) D'après la remarque précédente, on a $P_2=\begin{pmatrix}1 & 1 \\ -1 & 1\end{pmatrix}$ et $P_3=\begin{pmatrix} 1 & 0 & 1\\-1 & 1 & 1 \\ 0 & -1 & 1\end{pmatrix}$.

		c) Notons POur une matrice $B$ notons $\mathcal C(B)=\{A\in\mathcal M_3(\mathbb R)| AB=BA\}$ : $\mathcal C(B)$ est un sous espace vectoriel de $\mathcal M_3(\mathbb R)$. On peut montrer que $\mathcal C(J_3)$ est isomorphe à $\mathcal C(D_3)$ où $D_3=\mathrm{diag}(0,0,3)$. On va donc déterminer la dimension $\mathcal C(D_3)$ : les espaces étant isomorphes, ils ont même dimension.
		Soit $E_{ij}$ les élements de la base canonique de $\mathcal M_3(mathbb R)$. Par le calcul on montre $E_{11},E_{22},E_{12},E_{21},E_{33}$ sont des éléments de $\mathcal C(D_3)$. Ces éléments forment une famille libre donc la dimension de $\mathcal C(D_3)$ est au moins de $5$. Soit $A\in\mathcal C(D_3)$. Posons $A=\begin{pmatrix}a_{11}&a_{12}&a_{13}\\a_{21}&a_{22}&a_{23}\\a_{31}&a_{32}&a_{33}\end{pmatrix}$, la relation $AD_3=D_3A$ donne $a_{13}=a_{23}=a_{31}=a_{32}=0$. On en déduit que $A\in\mathrm{Vect}(  E_{11},E_{22},E_{12},E_{21},E_{33})$. La famille est donc génératrice de $\mathcal C(D_3)$. Elle est bien une base et la dimension de $\mathcal C(D_3)$ est donc $5$. On en déduit que $\mathcal C(J_3)$ est de dimension $5$.




!!! exercice "RMS2022-697"
	=== "Enoncé"
		(version modifiée parce que j'ai mal lu)
		Soit $A=(a_{i,j})_{1\leq i,j\leq n}\in\mathcal{M}_n(\mathbb R)$ où $a_{i,i}=a_{1,i}=a_{i,1}=1$ pour $i\in\{1,\dots ,n\}$, les autres coefficients étant nuls.

		a)   Montrer que $1$ est valeur propre de $A$ puis déterminer le sous-espace propre associé.

		b)   En remarquant que $A$ est symétrique, déterminer tous les éléments propres de $A$.



	=== "Corrigé"
		a) La matrice $A-I$ présente plusieurs fois la même colonne : elle est donc de rang $<n$ et n'est pas inversible. Ainsi $1$ est bien une valeur propre de $A$. On cherche donc $X=\begin{pmatrix}x_1\\\vdots\\ x_n\end{pmatrix}$ telle que $AX=X$. Cela donne le système :
		$$
		\begin{cases}
		x_1+\dots+x_n=x_1\\
		x_1+x_2=x_2\\
		x_1+x_3=x_3\\
		\vdots\\
		x_1+x_n=x_n
		\end{cases}
		$$
		soit encore $x_1=0$ et $x_2+\dots+x_n=0$. Ainsi l'espace propre associé à la valeur propre $1$ est l'intersection de deux hyperplans non confondus, donc de dimension $n-2$. Si $e_1,\dots,e_n$ est la base canonique, les vecteurs $e_2-e_3$, $e_2-e_4$, $\dots$, $e_2-e_n$ est une base de $E_1$.

		b) En effet $A$ est symétrique. Donc $A$ est diagonalisable dans $\mathbb R$ : elle annule un polynôme scindé à racines simples. On a identifié $1$ comme racine de multiplicité $n-2$. Il nous manque deux valeurs propres. Sa trace vaut $n$ donc la somme des deux valeurs propres manquante vaut $2$. Par ailleurs $\det(A_n)=-1+\det(A_{n-1})$ en développant par rapport à des colonnes. Or $\det(A_1)=1$ donc $\det(A_n)=-1\times(n-1)+1=-n+2$. On en déduit que les deux valeurs propres restants, $\lambda_1$ et $\lambda_2$ sont telles que $\lambda_1\lambda_2=-n+2$ et $\lambda_1+\lambda_2=2$.
		$\lambda_1$ et $\lambda_2$ sont donc les racines du polynôme $X^2-2X-n+2$. On calcule le disscriminant $\Delta=4+4(n-2)=4(n-1)$ Donc les deux valeurs propres restantes sont $1\pm\sqrt{n-1}$.

		Cherchons les vecteurs propres associés : on a le système
		$\begin{cases}x_1+x_2+\dots+x_n=(1\pm\sqrt{n-1})x_1\\
		x_1+x_2=(1\pm\sqrt{n-1})x_2\\
		\vdots\\
		x_1+x_n=(1\pm\sqrt{n-1})x_n
		\end{cases}$
		soit $x_2=x_3=\dots=x_n$ et $x_1=\pm\sqrt{n-1}x_2$. Un vecteur propre de chaque sous-espace est donc $\begin{pmatrix}\pm\sqrt{n-1}\\1\\\vdots\\1\end{pmatrix}$.




!!! exercice "RMS2022-698"
	=== "Enoncé"
		Soit $E$ un espace euclidien. On dit qu'un endomorphisme $f$ de $E$ est antisymétrique
		si $\forall x, y \in E,$ $\left<f(x),y\right>=-\left<x,f(y)\right>.$

		a) Montrer que $f$ est  antisymétrique si et seulement si $\forall x \in E,\ \left<f(x),x\right>=0$.

		Dans toute la suite $f$ est un endomorphisme antisymétrique.

		b) Montrer que $\ker f$ est orthogonal à $\mathrm{im} f$.


		c)  Soit $s=f\circ f$. Montrer que $s$ est un endomorphisme symétrique. Montrer que  toutes ses valeurs propres sont négatives ou nulles. Montrer que $\ker s= \ker f$.

		d) On suppose $n=3$.
		Montrer qu'il existe une base orthonormée dans laquelle la matrice de~$f$  est de la forme :
		$\begin{pmatrix}0&0&0\\ 0&0&-\alpha\\0&\alpha&0\end{pmatrix}$.



	=== "Corrigé"
		a) Soit $f$ antisymétrique. Soit $x\in E$, alors $<f(x),x)>=-<x,f(x)>$. Or un produit scalaire est symétrique donc $<x,f(x)>=<f(x),x>$ . On a donc $2<f(x),x>=0$ c'est à dire $<f(x),x>=0$. Réciproquement si pour tout $x$, $<f(x),x>=0$, alors soit $x$ et $y$ dans $E$,

		$$\begin{align*}
		0&=<f(x+y),x+y>\\
		&=<f(x),x>+<f(x),y>+<f(y),x>+<f(y),y>=<f(x),y>+<f(y),x>
		\end{align*}.$$

		On en déduit $<f(x),y>=-<f(y),x>=-<x,f(y)>$ donc $f$ est antisymétrique.

		b) Soit $x\in\ker f$ et $y\in\mathrm{im} f$, on pose $y=f(x_0)$. alors $<x,y>=<x,f(x_0)>=-<f(x),x_0>=-<O,x_0>=0$. Donc $\ker f$ est orthogonal à $\mathrm{im} f$.

		c) Soit $x,y$ deux éléments de $E$. On a $<s(x),y>=<f(f(x)),y>=-<f(x),f(y)>=+<x,f(f(y))>=<x,s(y)>$ donc $s$ est symétrique. Soit $\lambda$ une valeur propre de $s$ et $x$ un vecteur propre associé. Alors $<s(x),x>=\lambda<x,x>=-<f(x),f(x)>$. On a alors $||f(x)||^2=-\lambda||x||^2$. les normes étant positives, $\lambda\le 0$. Il est évident que $\ker f\subset \ker s$. Soit maintenant $x\in\ker s$. Alors $||f(x)||^2=<f(x),f(x)>=-<x,s(x)>=-<x,0>=0$. Donc $f(x)=0$ et ainsi $\ker s\subset \ker f$. D'où l'égalité.

		d) Soit $f$ antisymétrique, alors $pour $i,j$ deux éléments d'une bae orthonormée, $<f(e_i),e_j>=-<e_i,f(e_j)>=-<f(e_j),e_i>$. On en déduit que la matrice de $f$ dans une BON est antisymétrique. notons $A$ cette matrice. Ici, $n=3$ et $\det(A)=\det(-A^\top)=(-1)^3\det(A^\top)=-\det(A^\top)=-\det(A)$, donc $\det(A)=0$ et $A$ c'est pas inversible. Ainsi le noyau de $f$ est au moins de dimension $1$. So $f$ est nulle, n'ipmporte quelle base convient à avoir le résultat. Si $f$ est non nulle, alors $\ker f\ne E$ et donc $\ker s\ne E$ d'après une question précédente. Ainsi, $s$ étant diagonalisable, $x$ admet un vecteur propre associé à une valeur propre $\lambda<0$. Notons $\lambda=-\alpha^2$ et $x_0$ un vecteur propre associé à $\lambda$ ($x_0\ne 0$). Montrons que $(x_0,f(x_0))$ est libre. Soit $\lambda,\mu$ des réels tels que $\lambda x_0+\mu f(x_0)=0 (i)$. En composant par $f$ on obtient $\lambda f(x_0)-\alpha^2\mu x_0=0 (ii)$. En faisant $\lambda(i)-\mu(ii)$ on obtient $(\lambda^2+\alpha^2\mu^2)x_0=0$, et $\alpha\ne 0$ implique $\lambda=\mu=0$. Ainsi la famille est libre. C'est une famille de $\mathrm{im} f$ donc $\mathrm{im} f$ est de dimension au mois $2$. Or $E=\ker f\oplus\mathrm{im} f$, on en déduit $\dim\ker f=1$ et $\dim\mathrm{im} f=2$. Prenons $e_1$ un vecteur de $\ker f$ et $e_2=\alpha x_0$ et $e_3=f(x_0)$. La famille $e_1,e_2,e_3$ est alors une base de $E$. De plus $f(e_2)=\alpha f(x_0)=\alpha e_3$ et $f(e_3)=f(f(x_0))=-\alpha^2x_0=-\alpha e_2$. La matrice de $f$ dans cette base a donc bien la forme voulue.      



!!! exercice "RMS2022-699"
	=== "Enoncé"
		Soient $A\in \mathcal{M}_n(\mathbb R)$ et $(V_1,\ldots ,V_m)$ une famille libre de vecteurs  propres de $A$ avec $m>0$. Soit $Q$ la matrice de colonnes $V_1,\dots , V_m$
		 et $S=QQ^T$.
		Montrer que $S\in\mathcal{S}^+_n(\mathbb R)$, que $S$ est de rang $m$ et que $AS=SA^T$.



	=== "Corrigé"
		$S$ est symétrique par définition. Elle est donc diagonalisable dans une base orthonormée. Soit $x\in \mathcal M_{n,1}(\mathbb R)$, on a $x^\top Sx=x^\top QQ^\top x=(Q^\top x)^\top (Q^\top x)=||Q^\top x||^2\ge 0$. Ainsi $S\in\mathcal S_n^+$. On en déduit que $S$ est diagonalisable à valeurs propres positives.

		Soit $x$ tel que $Sx=0$, on a alors $x^\top Sx=0$ et donc d'après le calcul précédent, $Q^\top x=0$. La réciproque est bien sûr vraie. On en déduit que le noyau de $S$ est égal au noyaux de $Q^\top. Or $Q^\top est de rang $m$ car formé de $m$ lignes libres. Donc son noyaux est de dimension $n-m$. On en déduit que $S$ a un noyau de dimension $n-m$ et donc une image de dimension $m$ par el théorème du rang : ainsi $S$ est de rang $m$.

		On a $AS=AQQ^\top$. Or $AQ=A(V_1,V_2,\dots,V_m)=(AV_1,\dots,AV_m)=(\lambda_1 V_1,\dots,\lambda_m V_m)=Q\mathrm{diag}(\lambda_1,\dots,\lambda_m)$. Notons $D=\mathrm{diag}(\lambda_1,\dots,\lambda_m)$, on a $AQ=QD$. Ainsi $AS=AQQ^\top=QDQ^\top=QD^\top Q^\top=Q(QD)^\top=Q(AQ)^\top=QQ^\top A^\top=SA^\top$.




!!! exercice "RMS2022-700"
	=== "Enoncé"
		a) Prouver, pour toutes $A\in{\cal M}_{n,p}(\mathbb C)$ et $B\in{\cal M}_{p,q}(\mathbb C)$, l'égalité $\overline{AB}=\overline{A}\times \overline{B}$.

		b) Soient $A\in{\cal A}_n(\mathbb R)$ et $\lambda$ une valeur propre de $A$. En utilisant
		la question précédente, montrer que $\overline{\lambda}=-\lambda$.

		c) Soit  $A\in{\cal A}_n(\mathbb R)$.

		i) Donner la forme de $\chi_A$.

		ii) Que dire de $\chi_A(0)$ lorsque $n$ est impair ?

		iii) On suppose $n$ pair et $0\not\in\mathrm{Sp}(A)$. Montrer que $\det(A)>0$.



	=== "Corrigé"
		a) Cela découle des propriétés du conjugué.

		b) Soit $A$ antisymétrique dans $\mathbb R$. Soit $x$ un vecteur propre associé à la valeur propre $\lambda$. Notons $x^*$ le vecteur ligne dont les coefficients sont les conjugués des coefficients de $x$. On remarque que $x^*x$ est un réel strictement positif car $x\ne 0$. On a alors $x^*Ax=\lambda x^*x$. Or $\overline {x^* Ax}=\bar{x^*} \bar A\bar x=x^\top \bar A (x^*) ^\top=x^\top (-A)(x^*)^\top=-(x^*Ax)^\top=-\lambda x^*x$ car $x^*x$ est un réel. On en déduit $\bar\lambda=-\lambda$.

		ci) $A$ étant réelle ont sait que son polynôme caractéristique est réel, donc si $\lambda$ est racine, alors $\bar\lambda$ aussi. S'il possède une racine $\lambda$, alors $\bar\lambda = -\lambda$ et cette racine est imaginaire pure : $A$ ne possède pas de racines réelles non nulles. Le polynôme caractéristique de $A$ est donc de la forme $\chi_A=X^q\prod_{k=1}^p(X^2+a_k^2)$ où les $a_k$ sont des réels (pas nécessairement distincts).

		cii) Si $n$ est impair, alors $q\ne 0$ (sinon le degré du polynôme caractéristique est pair, ce qui ne va pas) donc $0$ est valeur propre de $A$ et ainsi $\chi_A(0)=0$ car $\chi_A(0)$ est, au signe près, le produit des valeurs propres.

		ciii) Si $n$ est pair et $0$ n'est pas valeur propre, on a $\chi_A(0)=(-1)^n\prod_{k=1}^p a_k^2>0$.




!!! exercice "RMS2022-701"
	=== "Enoncé"
		Soit $A\in {\cal S}_n(\mathbb R)$. On dit que $A$ est définie positive si Sp($A) \subset \mathbb R^{+*}$.

		a) Soit $A$  définie positive. Montrer qu'il existe une unique matrice $B$ symétrique définie positive telle que $B^2=A$.

		b) Soit $(c_1,\ldots ,c_n)$ une base orthonormée de vecteurs propres de $A$.
		Exprimer $\left<AX,X\right>$ en fonction de la décomposition de $X$ dans cette base.

		c)  Montrer que, pour  $X \in \mathbb R^n,$ $\|X\|^4\leq \left<AX,X\right>\left<A^{-1}X,X\right>.$
		Dans quel cas a t-on égalité?



	=== "Corrigé"
		a) L'implication réciproque est triviale.

		Voyons le sens direct. Soit $A$ une matrice symétrique définie positive. Alors $A$ est orthogonalement semblable à une matrice diagonale à coefficients strictement positfs. On note $A=P^\top DP$ avec $D=\mathrm{diag}(\lambda_1,\dots,\lambda_n)$. Posons pour $i\in \{1,\dots,n\}$ \delta_i=\sqrt{\lambda_i}$. On pose de plus $\Delta=\mathrm{diag}(\delta_1,\dots,\delta_n)$. On a alors $A=P^\top \Delta\Delta P=P^\top \Delta P P^\top \Delta P=B^2$ avec $B=P^\top\Delta P$. On a donc l'existence.

		Soit  donc $B$ symétrique définie positive telle que $A=B^2$. $A$ est diagonalisable dans une base orthonormée. $A$ et $B$ commutent donc les sous-espaces propres de $A$ sont stables par $B$. Soit $a$ et $b$ les endomorphismes canoniquement associés à $A$ et $B$ respectivement. Soit $F$ un sous-espace propre de $a$ associé à la valeur propre $\lambda$. Notons $a_F$ la restrictions de $a$ à $F$ et $b_F$ la restriction de $b$ à $F$. On a $a_F=\lambda \mathrm{id}_F$.Par ailleurs, $b_F$ est toujours symétrique définie positive donc diagonalisable sur $F$. Soit une valeur propre $\mu$ de $b_F$, associée à un vecteur propre $x$, on a $a_F(x)=b_F^2(x)=\mu^2 x$. Or $a_F(x)=\lambda x$ donc $\lambda = \mu^2$. Or $\mu>0$ donc $\mu=\sqrt \lambda$. Ainsi $b_F$ est aussi la même homothétie. On trouve que pour tout sous-espace propre $E_\lambda$ de $A$, $b_{E_\lambda}=\sqrt\lambda \mathrm{id}$. Les sous-espaces propres étant en somme directe, puisque $A$ est diagonalisable, cela détermine $b$ de manière unique.

		b) Posons $X=x_1c_1+\dots+x_nc_n$. On a $<AX,X>=\sum_{i=1}^n \lambda_i x_i^2$.

		c) Si $A$ est symétrique définie positive de valeurs propres strictement positives $\lambda_i$ alors $A$ est inversible et $A^{-1}X=\frac 1{\lambda _i}X$ pour tout vecteur propre $X$ associé à une valeur propre $\lambda$. Les vecteurs propres de $A^{-1}$ sont donc les mêmes, les valeurs propres associées sont les inverses.  On a donc d'après la question précédente, en utilisant la remarque classique :$x+\frac 1x^ge 2$ pour $x$ positif :

		$$\begin{align*}
		<AX,X><A^{-1}X,X>&=\sum_{i\ne j}\left(\frac {\lambda_i}{\lambda_j}+\frac {\lambda_j}{\lambda_i}\right)x_i^2x_j^2+\sum_{i=1}^n x_i^4\\
		&\ge \sum_{i\ne j}2x_i^2x_j^2+\sum_{i=1}^n x_i^4\\
		&=(\sum_{i=1}^n x_i^2)^2\\
		&=||X||^4.
		\end{align*}$$

		On aura égalité lorsque pour tout couple $i,j$,$\lambda_i=\lambda_j$ et donc lorsque $A$ est une homothétie.



!!! exercice "RMS2022-702"
	=== "Enoncé"

		Soit $A\in\mathcal{S}_n(\mathbb R)$.

		a)   Montrer que $A\in\mathcal{S}_n^+(\mathbb R)$ si et seulement s'il existe $B\in\mathcal{M}_n(\mathbb R)$ telle que $A={B}^TB$.

		b)   Montrer que $A\in\mathcal{S}_n^{++}(\mathbb R)$ si et seulement s'il existe $B\in\mathrm{GL}_n(\mathbb R)$ telle que $A={B}^TB$.

		c)   Soit $A\in\mathcal{S}_n^{++}(\mathbb R)$. Montrer que l'application $X\mapsto \sqrt{{X}^TAX}$ est une norme sur $\mathcal{M}_{n,1}(\mathbb R)$. Est-elle associée à un produit scalaire?



	=== "Corrigé"
		a) $A$ est symétrique positive. Donc $A$ est orthogalement semblable à une matrice diagonale dont les termes diagonaux sont positifs. Notons $D$ cette matrice diagonale. On note $d_1,\dots,d_n$ ses coefficients diagonaux et $A=P^\top DP$. Notons $\Delta=\mathrm{diag}(\sqrt {d_1},\dots,\sqrt{d_n})$. On a alors $A=P^\top \Delta^2 P=P^\top \Delta^\top\Delta P=(\Delta P)^\top\Delta P$. On pose $B=\Delta P$ et on a le résultat demandé.

		Réciproquement si $A=B^\top B$ alors $A$ est symétrique. Elle est donc diagonalisable. Soit $\lambda$ une valeur propre associée à un vecteur propre $X$. On a $X^\top AX=X^\top B^\top BX=||BX||^2\ge 0$. Mais $X^\top AX=\lambda X^\top X=\lambda ||X||^2$. On en déduit que $\lambda$ est positive. Donc $A$ est symétrique positive.

		b) Pour le sens direct : si $A$ est symétrique définie positive, alors ses valeurs propres sont stictement positives et la matrice $D$ et $\Delta$ sont diagonales à valeurs diagonales différentes de $0$ : elles sont inversible et par conséquent $B$ est inversible. Pour la réciproque, $X$ étant différent de $0$, $||BX||>0$ et $||X||>0$. Ce dont on déduit que $\lambda>0$. Les valeurs propres sont donc strictement positive, et $A$ est donc symétrique définie positive.

		c) Notons $N:X\mapsto \sqrt{{X}^TAX}$. Pour être une norme on doit vérifier le caractère homogène, défini positif et l'inégalité triangulaire (les valeurs sont évidemment positives). On note $A=B^\top B$ avec $B$ inversible. On remarque que $N(X)=||BX||$. Si $X=0$ alors $N(X)=0$. Si $N(X)=0$ alors $X^\top B^\top BX=0$ c'est à dire $||BX||=0$. On en déduit que $BX=0$ et puisque $B$ est inversible $X=0$. $N$ est bien définie positive, elle est clairement homogène et via l'expression $N(X)=||BX||$ on a l'inégalité triangulaire.

		Si on considère le produit scalaire $\varphi(X,Y)=X^TB^\top BY$, alors $N$ en est la norme associée.



!!! exercice "RMS2022-703"
	=== "Enoncé"
		Soit $q:(x_0,\ldots, x_{n-1})\in\mathbb R^n\mapsto x_0^2-\sum_{k=1}^{n-1}x_k^2$.

		Soit  $G=\left\{f\in\mathrm{GL}(\mathbb R^n)\, ;\ \forall x\in\mathbb R^n,\ q(f(x))=q(x)\right\}.$

		a)   Montrer que $G$ est stable par composition et passage à l'inverse.

		b)   Soit $\beta:(x,y)\mapsto \frac12(q(x+y)-q(x)-q(y))$. Montrer que $\beta$ est une forme bilinéaire symétrique sur $\mathbb R^n$.

		c)   Soient $J=\mathrm{Diag}  (-1,1,\ldots,1)$, $f\in \mathrm{GL}(\mathbb R^n)$ et $A$ la matrice de $f$ dans la base canonique de $\mathbb R^n$. Montrer que $f\in G$ si et seulement si  ${A}^TJA=J$.



	=== "Corrigé"
		a) Soit $f$ et $g$ dans $G$. Soit $x\in E$. On a $q(f\circ g(x))=q(f(g(x)))=q(g(x))=q(x)$ donc $f\circ g\in G$ : $G$ est stable par composition. Par ailleurs, si $f$ est dans $G$ alors $f$ est inversible et $q(f^{-1}(x)=q(f(f^{-1}(x)))=q(x)$. Donc $G$ est stable par inverse.

		b) Pour le bien des calculs, essayons de simplfier l'écriture de $b(x,y)$ :

		$$\begin{align*}
		q(x+y)-q(x)-q(y)&=(x-0+y_0)^2-\sum_{k=1}^{n-1}(x_k-y_k)^2-q(x)-q(y)\\
		&=x_0^2+2x_0y_0+y_0^2-\sum_{k=1}^{n-1}x_k^2+y_k^2+2x_ky_k-q(x)-q(y)\\
		&=2x_0y_0-2\sum_{k=1}^{n-1}x_ky_k
		\end{align*}
		$$

		Sous cette forme, la bilinéarité est évidente.

		c)

		Notons $B$ la matrice définie par : $B_{ij}=b(e_i,e_j)$. La matrice $B$ est donc symétrique. Soit $X$ la matrice de $x$ dans la base canonique et $Y$ la matrice de $y$ dans la base canonique. Alors $b(x,y)=X^\top BY$. Or $b(e_i,e_j)=\begin{cases} 0&\text{si }i\ne j\\1&\text{si }i=0=j\\-1&\text{si }i=j\ne0\end{cases}$. Donc $B=-J$. Notons $C_1,\dots,C_n$ les colonnes de $A$, Alors $(A^\top JA)_{ij}=-b(C_i,C_j)=-b(f(e_i),f(e_j))$. On en déduit que $A^\top JA=J$ ssi pour tout couple $(i,j)$, b(f(e_i),f(e_j))=\begin{cases} 0&\text{si }i\ne j\\-1&\text{si }i=j=0\\1&\text{si }i=j\ne0\end{cases}=b(e_i,e_j)$.

		Par ailleurs, si $q(f(x))=q(x)$ pour tout $x$ alors $b(f(e_i),f(e_j))=\frac 12(q(f(e_i+e_j))-q(f(e_i))-q(f(e_j)))=\frac 12(q(e_i+e_j)-q(e_i)-q(e_j))=b(e_i,e_j)$.  Réciproquement, si pour tout couple $i,j$ ,$b(e_i,e_j)=b(f(e_i),f(e_j))$ alors par bilinéarité $b(x,x)=b(f(x),f(x))$ c'est à dire $q(f(x))=q(x)$. On a donc au final bien l'équivalence demanc



!!! exercice "RMS2022-704"
	=== "Enoncé"
		a)   Soient $E$ un espace vectoriel muni de deux normes équivalentes $N_1$ et $N_2$, $(u_n)$ une suite de $E$ qui converge pour $N_1$. Montrer que $(u_n)$ converge pour $N_2$.

		Soit $E=\mathbb R[X]$. Soit, pour  $a\in\mathbb R$, $N_a:P\mapsto\lvert P(a)\rvert +\lVert P'\rVert_{\infty,[0,1]}$.

		b) Si $a\in\mathbb R$, montrer que $N_a$ est une norme.

		c)  Soient $a$, $b\in[0,1 ]$. Montrer que $N_a$ et $N_b$ sont des normes équivalentes.

		d)   Pour quelles valeurs de $a$, la suite $\left(\left({X}/{2}\right)^n\right)$ converge-t-elle pour $N_a$?



	=== "Corrigé"
		a) Soit $(u_n)$ une suite convergente pour $N_1$, de limite $\ell$. Alors $N_2(u_n-\ell)\le\alpha N_1(u_n-\ell)\to 0$ donc $N_2(u_n-\ell)\to 0$ et $u_n$ converge pour $N_2$.

		b) $N_a$ est clairement une norme. Détaillons surtout le caractère défini positif : Si $N_a(P)=0$ alors $P(a)=0$ et $P'=0$ sur $[0,1]$. Ainsi $P'$ est un polynôme ayant une infinité de racines, c'est donc le polynôme nul. On en déduit que $P$ est constant, et nul en $a$ donc $P$ est nul.

		c) Si $a$ et $b$ sont dans $[0,1]$, supposons sans perte de généralité que $a<b$, alors $P(b)-P(a)=\int_{a}^b P'(t)dt$. Ainsi $P(b)=P(a)+\int_a^b P'$ et $|P(b)|\le |P(a)|+\int_a^b||P'||\le |P(a)|+||P'||$. par inégalité triangulaire et positivité de l'intégrale. On en déduit que $N_b(P)\le 2N_a(P)$. Par ailleurs, $P(a)=P(b)-\int_{a}^b P'(t)dt$ donc $|P(a)|\le|P(b)|+\int_a^b ||P'||dt$ et on obtient la même majoration : $N_a(P)\in 2N_b(P)$. Les deux normes sont donc équivalentes.

		d) Calculons $N_a\left(\left(\frac X2\right)^n\right)=\frac{a^n}{2^n}+\frac n{2^n}$. Le terme de droite tend vers $0$ et le terme de gauche est de la forme $(\frac a2)^n$ qui converge si et seulement si $a\in]-2,2]$.  



!!! exercice "RMS2022-705"
	=== "Enoncé"
		 Pour $A\in\mathcal{M}_n(\mathbb R)$, on pose $N(A)=\mathrm{tr}(A{A}^T)$.

		 Montrer que, pour  $A$, $B\in\mathcal{M}_n(\mathbb R)$, $N(AB)\leq N(A)\, N(B)$.



	=== "Corrigé"
		On sait que $||A||=\sum_{i,j}a_{i,j}^2$. On a ici $N(AB)=\sum_{i,j}(\sum_{k=1}^na_{ik}b_{kj})^2\le \sum_{i,j}\sum_{k=1}^na_{ik}^2\sum_{k=1}^nb_{kj}^2=\sum_{i,k}a_{ik}^2+\sum_{j,k}b_{kj}^2=N(A)N(B)$.



!!! exercice "RMS2022-706"
	=== "Enoncé"
		Soient $E$ un espace vectoriel normé et $F$ un sous-espace de $E$.

		a)   Montrer que l'adhérence de $F$ est un sous-espace de $E$.

		b)   Que dire de  l'adhérence de $F$ lorsque $F$ est un hyperplan de $E$ ?  On commencera par le cas  où $E$ est de dimension finie.



	=== "Corrigé"
		a) $x$ et $y$ dans l'adhérence de $F$, soit $\lambda$ et $\mu$ deux scalaires. Soit $x_n$ une suite d'éléments de $F$ qui converge vers $x$ et $y_n$ une suite d'éléments de $F$ qui converge vers $y$. On a alors $\lambda x_n+\mu y_n$ qui converge ver $\lambda x+\mu y$. Or $\lambda x_n+\mu y_n$ est dans $F$, ainsi $\lambda x+\mu y$ est limite d'une suite d'éléments de $F$. Donc l'adhérence de $F$ est un sev de $E$.

		b) Soit $F$ un hyperplan. $\bar F$ est un sev de $E$. Si $\bar F\ne F$ alors il existe $x$ dans $\bar F$ tel que $x\ne F$. On peut montrer que $F$ et $<x>$ sont supplémentaires. On a donc $F\oplus <x>\subsect E$. Or $F$ est de codimension $1$ donc $F\oplus<x>=E$. On en déduit que $\bar F=E$. Ainsi $F$ est dense dans $E$. En résumé, soit $F$ est fermé, soit $F$ est dense dans $E$.

		Dans le cas dimensions finie, $F$ est fermé.



!!! exercice "RMS2022-707"
	=== "Enoncé"
		a) Soit $E$ un espace  préhilbertien réel avec $\|\;\|$ sa norme euclidienne. Montrer l'égalité du parallélogramme : $\forall x, y \in E$,
		$\|x+y\|^2+\|x-y\|^2=2(\|x\|^2+\|y\|^2)$.

		b) Donner un exemple de norme non euclidienne en dimension finie puis infinie.

		c) Soit $\|\;\|$ une norme    vérifiant l'égalité du  parallélogramme.

		 On pose, pour $x,y\in E$,  $\varphi(x,y)=\alpha ( \|x+y\|^2-\|x-y\|^2)$.
		Déterminer $\alpha \in \mathbb R$ pour que $\|\; \|$ soit la norme associée à $\varphi$.
		En déduire l'unicité de $\varphi$.
		Montrer que $\varphi$ est un produit scalaire de norme associée  $\|\;\|$.



	=== "Corrigé"
		a) C'est du cours.

		b) Le $||x||=\sup_{i}|x_i|$ est une norme qui n'est pas une norme euclidienne car elle ne respecte pas l'inégalité du parallélogramme. Probablement la même chose en dimension quelconque.

		c) On veut avoir $\|x\|=\varphi(x,x)$. Or $\varphi(x,x)=\alpha||2X||^2=4\alpha||x||^2$. On doit donc avoir $\alpha=\frac 14$ ce qui donne l'unicité des $\varphi$ sous cette forme.
		On cherche à montrer la linéarité par rapport à la première variable, la symétrie étant triviale. On se concentre pour le moment sur la compatibilité avec la somme.
		Soient $x,y,z$ trois éléments de $E$.


		\begin{align*}
		8\varphi(x+y,z)&=\|x+y+z\|^2-\|x+y-z\|^2+\|x+y+z\|^2-\|x+y-z\|^2\\
		&=\|x+z+y\|^2-\|x-z+y\|^2+\|y+z+x\|^2-\|y-z+x\|^2\\
		&=\|x+z+y\|^2+\|x+z-y\|^2-\|x+z-y\|^2\mspace{-100mu}///////////\\
		&-\|x-z+y\|^2-\|x-z-y\|^2+\|x-z-y\|^2\mspace{-100mu}\backslash\backslash\backslash\backslash\backslash\backslash\backslash\backslash\backslash\backslash\backslash\\
		&+\|y+z+x\|^2+\|y+z-x\|^2-\|y+z-x\|^2\mspace{-100mu}\backslash\backslash\backslash\backslash\backslash\backslash\backslash\backslash\backslash\backslash\backslash\\
		&-\|y-z+x\|^2-\|y-z-x\|^2+\|y-z-x\|^2\mspace{-100mu}///////////\\
		&=2\|x+z\|^2+2\|y\|\\
		&-2\|x-z\|^2-2\|y\|\\
		&+2\|y+z\|^2+2\|x\|\\
		&-2\|y-z\|^2-2\|x\|\\
		&=8\varphi(x,z)+8\varphi(y,z)
		\end{align*}

		On en déduit que $\varphi(x+y,z)=\varphi(x,z)+\varphi(y,z)$.

		Voyons alors le problème de la multiplication par un scalaire, c'est à dire que $\varphi(\lambda x,y)=\lambda\varphi(x,y)$.

		Si $\lambda$ est entier positif, cela découle de la propriété précédente. Si $\lambda$ est un entier négatif, on a $\varphi(\lambda x,y)+\varphi(|\lambda|x,y)=\varphi((\lambda+|\lambda|)x,y)=\varphi(0,y)=0$. Or $\varphi(|\lambda|x,y)=|\lambda|\varphi(x,y)$. D'où $\varphi(\lambda x,y)=-|\lambda|\varphi(x,y)=\lambda\varphi(x,y)$.

		Si maintenant $\lambda=\frac pq$ avec $(p,q)\in\mathbb Z\times\mathbb N^*$, alors $\varphi(\lambda x,y)=\frac 1q\times q\varphi(\frac pq x,y)=\frac 1q\varphi(q\times\frac pq x,y)=\frac 1q\varphi(px,y)=\frac pq\varphi(x,y)=\lambda\varphi(x,y)$.


		Montrons maintenant que pour tout $(x,y)\in E^2$, $|\varphi(x,y)|\le \|x\|\|y\|$. Tout d'abord, si $x=0$ cette inégalité est triviale. Soit donc $x\ne 0$. Posons $P(r)=\varphi(rx+y,rx+y)$. Si $r$ est rationnel, alors $P(r)=\varphi(x,x)r^2+2\varphi(x,y)r+\varphi(y,y)$. Or $P(r)$ est toujours positif donc pour tout rationnel $r$, $\varphi(x,x)r^2+2\varphi(x,y)r+\varphi(y,y)\ge 0$. On en déduit que le polynôme $\varphi(x,x)X^2+2\varphi(x,y)X+\varphi(y,y)$ est un polynôme de degré exactement 2 car $\varphi(x,x)\ne 0$ et positif sur $\mathbb Q$. Par densité, il est positif sur $\mathbb R$ et donc son discriminant est négatif, ce qui donne $|\varphi(x,y)|\le \|x\|\|y\|$.

		Finalement, soit $\lambda\in\mathbb R$ et $r_n$ une suite de rationnels qui converge vers $\lambda$, alors $\varphi(r_nx,y)=r_n\varphi(x,y)\to \lambda \varphi(x,y)$ et $|\varphi((r_n-\lambda)x,y)|\le|r_n-\lambda|\|x\|\|y\|\to 0$. Donc $\varphi(r_nx,y)\to \varphi(\lambda x,y)$. Par unicité de la limite, on a $\varphi(\lambda x,y)=\lambda\varphi(x,y)$. Finalement, $\varphi$ est symétrique, bilinéaire et definie positive : c'est un produit scalaire, dont la norme associée est bien la norme recherchée.



!!! exercice "RMS2022-708"
	=== "Enoncé"
		Soient $E={\cal C}^0([a,b],\mathbb R)$ et $u$ une forme linéaire sur $E$ telle que, pour toute fonction $f\in E$ positive, $u(f)\geq0$.

		a)   Montrer que, pour tout $f\in E$, $\lvert u(f)\rvert\leq u(\lvert f\rvert)$.

		b)   Soit $e$ la fonction constante égale à $1$. Trouver la borne supérieure des $\frac{\lvert u(f)\rvert}{\lVert f\rVert_{\infty}}$ pour $f\in E$ non nulle à l'aide de $u(e)$.



	=== "Corrigé"
		a) Soit $f\in E$, on pose $f^+=max(f,0)$ et $f^-=\min(f,0)$. On a $f=f^++f^-$ et $|f|=f^+-f^-$. On veut montrer que $-u(|f|)\le u(f)\le u(|f|)$. On a $u(f)-u(|f|)=u(f-|f|)=u(2f^-)=-2u(-f^-)$. Or $-f^-$ est une fonction positive donc $u(-f^-)\ge 0$. On en déduit que $u(f-|f|)\le 0$ c'est à dire $u(f)\le u(|f|)$. Par ailleurs, $u(f+|f|)=2u(f^2)\ge 0$ pour les mêmes raisons. Donc $u(f)\ge -u(|f|)$. On en déduit donc que $|u(f)|\le u(|f|)$.

		b) On a $\frac{|u(f)|}{\|f\|_\infty}\le u\left(\frac{|f|}{\|f\|_\infty}\right)$. Posons $g = \frac{|f|}{\|f\|_\infty}$, on a $g\le 1$ donc $g\le e$. On en déduit que $u(g)\le u(e)$. et ainsi $\frac{|u(f)|}{||f||_\infty}\le u(e)$, cette valeur étant atteinte pour $f=e$. C'est donc une borne sup et même un max.




!!! exercice "RMS2022-709"
	=== "Enoncé"
		Soient $H$ un espace préhilbertien réel et $(e_k)_{k\in\mathbb N}$ une famille orthonormée de $H$. Pour  $f\in H$, on pose  $c_k(f)=\left< f, e_k\right>$. On note $\ell^2(\mathbb N)$, l'ensemble des suites $(u_n)\in\mathbb R^{\mathbb N}$ telles que la série de terme général $u_n^2$ converge.

		a)   Montrer que $\ell^2(\mathbb N)$ est un espace vectoriel et que l'application $(u,v)\mapsto \sum_{n=0}^{+\infty}u_nv_n$ est un produit scalaire sur cet espace.

		b)   Justifier la définition de l'application $\Phi:H\to \ell^2(\mathbb N)$ définie par $\Phi(f)=(c_k(f))_k$. Montrer que $\Phi$ est continue.




	=== "Corrigé"
		a) On montre que $\ell^2$ est un sous espace vectoriel de $\mathbb R^{\mathbb N}$. Evidemment, la suite nulle est dans $\ell^2$ qui est donc non vide. Soit $u$ et $v$ deux éléments de $\ell^2$ et $\lambda,\mu$ deux réels. On a la relation

		$$\bagin{align*}
		\sum_{n=0}^N(\lambda u_n+\mu v_n)^2&=\lambda^2\sum_{n=0}^N u_n^2+\mu^2\sum_{n=0}^Nv_n^2+2\lambda\mu\sum_{n=0}^Nu_nv_n
		\end{align*}$$

		Les deux premières sommes sont des sommes partielles de séries convergentes donc elles sont convergentes. Par ailleurs $\sum_{n=0}^N|u_nv_n|\le\sqrt{\sum_{n=0}^Nu_n^2}\sqrt{\sum_{n=0}^Nv_n^2}$ par l'inégalité de Cauchy Schwarz. On peut donc dire que la séries des $u_nv_n$ est absolument convergente donc convergente. Ainsi la série des $(\lambda u_n+\mu v_n)^2$ est convergente et donc la suite de terme général $\lambda u_n+\mu v_n$ est dans $\ell^2$.

		b) Tout d'abord par le même argument de Cauchy Schwarz que précédemment, on est assuré de la convergence absolue de la série définissant l'application. La linéarité, la symétrie et le caractère défini positif sont triviaux.

		c) Nous devons ici montrer que la suite $(c_k(f))_k$ est bien dans $\ell^2$. Soit $f\in H$, notons $E_N=vect(e_1,\dots,e_N)$. En posant $f_N=\sum_{k=1}^N <f,e_k>e_k$, on a $f=f_N+(f-f_N)$ et $f_N$ est orthogonal à $f-f_n$. On peut donc appliquer le théorème de pythagore : $||f||^2=||f_N||^2+||f-f_N||^2$. On en déduit que $||f_N||^2\le ||f||^2$. Or $||f_N||^2=\sum_{k=1}^N c_k(f)^2$. Cette série est donc croissante et majorée par $||f||^2$ donc convergente. Ainsi la suite $(c_k(f))_k$ est bien un élément de $\ell^2$.

		Montrons maintenant que $\Phi$ est continue. $\Phi$ est clairement linéaire, on parle donc de continuité des applications linéaires. Or on vient de montrer que pour tout $N$, $\sum_{k=1}^N c_k(f)^2\le ||f||^2$. Par passage à la limite, on en déduit que $\|\Phi(f)\|\le ||f||$ et donc que l'application linéaire est continue.  (un peu limite : pas de continuité des applications linéaires en dehors de la dimension finie).   



!!! exercice "RMS2022-710"
	=== "Enoncé"
		Soit $E$ un espace préhilbertien réel.

		a)   Soient $(u_n)$ une suite à valeurs dans $E$ qui converge vers $u\in E$ et $(\lambda_n)$ une suite réelle qui converge vers $\lambda\in\mathbb R$. Montrer que la suite $(\lambda_nu_n)$ converge vers $\lambda u$.

		b)   On suppose, dans cette question, que $E$ est de dimension finie. Soient $(u_n)$, $(v_n)$ deux suites à valeurs dans $E$ qui convergent vers $u$, $v\in E$. On suppose que,  pour tout $n\in\mathbb N$, $u_n$ et $v_n$ sont colinéaires. Montrer que $u$ et $v$ sont colinéaires.

		c)   Le résultat de la question précédente demeure-t-il vrai si l'on ne suppose plus $E$ de dimension finie?



	=== "Corrigé"
		a) $\|\lambda_nu_n-\lambda u\|=\|\lambda_n u_n-\lambda u_n +\lambda u_n - \lambda _u\|\le \|\lambda_nu_n - \lambda u_n\|+\|\lambda u_n-\lambda u\|\le \|u_n\||\lambda_n-\lambda|+|\lambda|\|u-u_n\|$. Or $u_n$ est convergente donc bornée, $|\lambda_n-\lambda|$ converge vers $0$ et $\|u_n-u\|$ converge aussi vers $0$. On en déduit que $\|\lambda_nu_n-\lambda u\|$ converge vers $0$ et donc que $\lambda_nu_n\to\lambda u$.

		b) Supposons que $u$ et $v$ forment une famille libre et complétons là par le théorème de la base incomplète : $(u,v,e_3,\dots,e_n)$ est une base de $E$. Pour $n\in \mathbb N$, la famille $(u_p,v_p,e_3,\dots,e_n)$ est libre, donc de déterminant nul. Notons $u_p=\alpha_p u+\beta_p v+\lambda_{3,p}e_3+\dots +\lambda_{n,p}e_n$ et $v_p=\gamma_p u+\delta_p v+\mu_{3,p}e_3+\dots+\mu_{n,p}e_n$. par des opérations simples sur les colonnes, le déterminant de cette famille est :
		$$D_n\begin{vmatrix}
		\alpha_n & \beta_n & 0 &\dots  & \dots & 0\\
		\gamma_n & \delta_n& 0 &  &  & \vdots\\
		0        &    0   & 1      & \ddots    &  & \vdots\\
		\vdots   &        & \ddots & \ddots& \ddots& \vdots\\
		\vdots   &        &        & \ddots& \ddots&  0 \\
		0        & \dots  & \dots  & \dots &   0  &   1
		\end{vmatrix}      = 0.$$
		Or $\alpha_n\to 1$, $\beta_n\to 0$ , $\gamma_n\to 0$ et $\delta_n\to1$. Le déterminant étant continu par rapport à ses coefficients, on devrait donc avoir $D_n\to 1$. Or pour tout $n$, $D_n=0$ donc c'est incompatible. L'hypothèse de départ est donc fausse et $(u,v)$ est liée.

		c)   Passons donc sur un espace vectoriel quelconque (sans dimension finie). Pour tout $n$ il existe $\lambda_n, \mu_n$ tels que $\lambda_n u_n+\mu_n v_n=0$ et $(\lambda_n,\mu_n)\ne (0,0)$. Évacuons certains cas :
		- si $u=0$ ou $v=0$, alors la famille est évidemment liée.

		Ces cas évacués, si $u\ne 0$ et $v\ne 0$, alors $||u||>0$. Or la norme étant continue, $||u_n||>0$ à partir d'un certain rang, ainsi que $||v_n||>0$. On en déduit qu'à partir d'un certain rang $n_0$, $u_n\ne 0$ et $v_n\ne 0$. La relation $\lambda_n u_n+\mu_n v_n=0$ à partir du rang $n_0$ donne alors $\lambda_n\ne 0$ ET $\mu_n\ne 0$. Posons $\delta_n=-\frac{\lambda_n}{\mu_n}$, on a $v_n=\delta_n u_n$ à partir du rang $n_0$. Passons à la norme : $0=\|\delta_nu_n-v_n\|^2=\delta_n^2\|u_n\|^2-2\delta_n<u_n,v_n>+\|v_n\|^2$. On en déduit que $\delta_n$ est racine du polynôme $\|u_n\|^2X^2-2<u_n,v_n>X+\|v_n\|^2$ dont l'unique racine vaut $\frac{<u_n,v_n>}{\|u_n\|^2}$ qui converge vers $\frac{<u,v>}{\|u\|^2}$. On en déduit que $\delta_n$ converge vers une limite $\delta$ et d'après la première question, $\delta_nu_n$ converge vers $\delta u$ et ainsi $v=\delta u$ par unicité de la limite : $u$ et $v$ sont colinéaires.



!!! exercice "RMS2022-711"
	=== "Enoncé"
		Soit, pour  $n\geq 4$, $f_n : x\mapsto x^{3n} - \sqrt{n}x +1$.

		a) Montrer qu'il existe un unique $x_n\in [1,2]$ tel que $f_n(x_n)=0$.

		b) Étudier la convergence de la suite de terme général $\epsilon_n=x_n-1$.

		c)  Trouver un équivalent de $\epsilon_n$.


		d)  Donner un développement asymptotique à trois termes de $x_n$.



	=== "Corrigé"
		a) $f_n$ est dérivable de dérivée $fn'(x)=3nx^{3n-1}-\sqrt n=\sqrt n(3\sqrt n x^{3n-1}-1)$. Si $x\ge 1$, cette quantité est strictement positive, donc $f_n$ est strictement croissante sur $[1,2]$ en particulier. Or $f_n(1)=2-\sqrt n<0$ car $n\ge 4$ et $f_n(2)=2^{3n}-2\sqrt n+1>0$ On en déduit que $f_n(x)=0$ admet une unique solution entre $A$ et $2$.

		b) On calcule
		$$\begin{align*}
		f_{n+1}(x_n)&=x_n^{3n+3}-\sqrt{n+1}x_n+1\\
		&=x_n^{3n+3}-\sqrt{n+1}x_n-x_n^{3n}+\sqrt nx_n\\
		&=x^{3n}(x_n^3-1)+x_n(\sqrt{n+1}-\sqrt n)\\
		&\ge 0.
		\end{align*}$$
		 On en déduit que $x_n\ge x_{n+1}$. La suite $x_n$ est donc décroissante et minorée : elle converge vers $\alpha\ge1$. Supposons que $\alpha>1$, il existe alors $n_0$ tel que si $n\ge n_0$ alors $x_n\ge \frac{1+\alpha}{2}>1$. On a alors la suite $x_n^{3n}-\sqrt nx_n+1$ qui diverge et qui ne peut donc pas être égale à $0$. On en déduit que $\alpha =1$. Donc $x_n\to 1$.

		c) On a alors $x_n^{3n}=\sqrt nx_n-1$ donc $x_n^{3n-1}\sim \sqrt n$. c'est à dire $3n\ln(x_n)=\ln(\sqrt nx_n-1)=\ln(\sqrt nx_n)+\ln(1-\frac{1}{\sqrt nx_n})$. D'où $6n\ln(x_n)=\ln(n)+2\ln(x_n)+2\ln(1-\frac{1}{\sqrt nx_n}$ dont on déduit $6n\ln(x_n)\sim\\ln(n)$. Or $\ln(x_n)\sim x_n-1$ donc $x_n-1\sim\frac{\ln(n)}{6n}$. On en déduit $x_n=1+\frac{\ln(n)}{6n}+o(\frac{\ln(n)}{n})$.

		d) On cherche un terme en plus. On a
		$$\begin{align*}
		x_n&=(\sqrt n x_n-1)^{1/3n}\\
		&=\exp(\frac 1{3n}\ln(\sqrt n x_n-1))\\
		&=\exp(\frac 1{3n}\ln(\sqrt n+\frac{\ln(n)}{6\sqrt n}-1+o(\frac{\ln n}{\sqrt n})))\\
		&=\exp(\frac{\ln(n)}{6n}+\frac 1{3n}\ln(1-\frac 1{\sqrt n}+\frac{\ln(n)}{6n}+o(\frac{\ln n}{n})))\\
		&=\exp(\frac{\ln(n)}{6n}+\frac 1{3n}\ln(1-\frac 1{\sqrt n}+o(\frac{1}{\sqrt n})))\\
		&=\exp(\frac{\ln(n)}{6n}-\frac 1{3n\sqrt n}+o(\frac 1{n\sqrt n}))\\
		&=1+\frac{\ln(n)}{6n}-\frac 1{3n\sqrt n}+o(\frac 1{n\sqrt n})
		\end{align*}$$



!!! exercice "RMS2022-712"
	=== "Enoncé"
		Soient $\alpha >0$ et, pour $n\in\mathbb N^*$, $u_n=\sum_{k=1}^n k^{\alpha n}$. Trouver un équivalent de $u_n$.


	=== "Corrigé"
		Posons $v_n=\frac{u_n}{n^{\alpha n}}=\sum_{k=1}^n{\left(\frac{k}{n}\right)}^{\alpha n}$. La fonction $x\mapsto x^{n\alpha}$ est croissante sur $[0,1]$. On a donc pour $k\in \{1,\dots,n\}$ :

		$$
		\int_{(k-1)/n}^{k/n}x^{\alpha n}dx\le \frac 1n\left(\frac {k}n\right)^{n\alpha}\le \int_{k/n}^{(k+1)/n}x^{\alpha n} dx.
		$$

		En sommant de $k=1$ à $k=n$ à gauche et de $k=0$ à $n-1$ à droite on obtient $\int_0^{1}x^{n\alpha}dx\le \frac 1n\sum_{k=1}^n\left(\frac {k}n\right)^{n\alpha}\le\int_0^1x^{n\alpha}dx+\frac 1n$. On en déduit que $\frac 1nv_n\sim \int_0^1x^{n\alpha}dx$ et donc $u_n\sim n^{\alpha n+1}\int_0^1x^{n\alpha}dx=\frac{n^{\alpha n+1}}{n\alpha+1}$.



!!! exercice "RMS2022-713"
	=== "Enoncé"
		Soit $(u_n)$ une suite d'éléments de $\mathbb R^{+*}$. On pose, pour $n\in\mathbb N^*$,  $v_n=\frac{u_n}{u_1+\cdots+u_n}$.
		Comparer les natures des séries $\sum u_n$ et $\sum v_n$.



	=== "Corrigé"
		La série $\sum u_n$ est une série à termes positifs : soit elle diverge vers $+\infty$, soit elle converge si elle est bornée.

		Supposons que $\sum u_n$ soit bornée et donc convergente, vers une limite $\ell$ non nulle obligatoirement. Alors $v_n\sim\frac {u_n}{\ell}$ et ainsi par comparaison des séries à termes positifs, $\sum v_n$ converge.

		Le cas plus compliqué est donc le cas $\sum u_n$ diverge.
		Supposons que $\sum u_n$ diverge.

		Tout d'abord, si $v_n\not\to 0$, alors la série $\sum v_n$ est divergente.

		Supposons donc $v_n\to 0$. Posons $w_n=\ln(1-v_n)$, on a donc $w_n\sim -v_n$. De plus $w_n=\ln(U_{n-1})-\ln(U_n)$ où $U_n=\sum_{i=1}^nu_i$. On voit apparaître une somme télescopique : $\sum_{n=1}^N \ln(1-v_n)=-\ln(U_N)+\ln(u_1)\to-\infty$. Ainsi la série des $w_n$ est divergente à termes négatifs. Or $w_n\sim- v_n$ donc la série $\sum v_n$ est divergente par théorème de comparaison des séries à termes positifs (négatifs ici).




!!! exercice "RMS2022-714"
	=== "Enoncé"
		a)   Étudier la convergence et calculer la somme éventuelle de la série de terme général~$u_n$ définie par $u_0=1$ et, pour tout $n\in\mathbb N$, $u_{n+1}=\ln(e^{u_n}-u_n)$.

		b)   Étudier la convergence et calculer la somme éventuelle de la série de terme général $v_n$ définie par $v_0\in\mathbb R$ et, pour tout $n\in\mathbb N$, $v_{n+1}=e^{v_n}-1$.




	=== "Corrigé"
		On a $u_0=1$, on peut montrerque pour tout entier $n$, $u_n>0$. En effet une relation bien connue montre que pour tout $x$ réel, $e^x\ge1+x$, donc ici $e^{u_n}-u_n\ge1$ et ainsi $u_{n+1}\ge 0$.
		Ainsi, $u_n$ est une suite de réels positifs. Par ailleurs, $u_{n+1}-u_n=\ln(e^{u_n}-u_n)-u_n=\ln(1-\frac{u_n}{e^{u_n}})$. Comme dit précédemment, $\frac{u_n}{e^{u_n}}\in[0,1]$ donc $\ln(1-\frac{u_n}{e^{u_n}})<0$. On en déduit que la suite $u$ est décroissante et minorée par $0$ : elle converge. Soit $\ell$ sa limite, on a alors par continuité et unicité de la limite $\ell=\ln(e^\ell-\ell)$ ce qui équivaut à $\ln(1-\frac\ell{e^\ell})=0$ c'est à dire $\ell=0$. Ainsi $u_n\to 0$.

		On remarque alors par un calcul simple que $u_n=\ln(e^{u_0}-S_{n-1})$ où $S_n$ est la somme partielle des $u_n$. Puisque $u_n$ converge vers $0$, on en déduit que $e-S_n$ converge vers $1$. C'est à dire $S_n\to e-1$.

		Passons à la suite définie par $v_{n+1}=e^{v_n}-1$. Une relation classique montre que $e^x-1\ge x$ donc $v_{n+1}\ge v_n$ : la suite est croissante. On  peut aussi montrer que $v_n$ est de signe constant. On en déduit rapidement que si $v_0>0$ alors la suite $v_n$ ne converge pas vers $0$ et ainsi $\sum v_n$ diverge.

		Si par contre $v_0\le 0$ alors la suite est négative et croissante donc convergente. Par continuité de la fonction $x\mapsto e^x-1$, on en déduit que $v_n$ converge vers un point fixe $\ell$ tel que $\ell=e^\ell-1$ dont la seule solution est $0$. Ainsi $v_n$ tend vers $0$.

		On a alors

		$$\begin{align*}
		\frac 1{v_{n+1}}-\frac 1{v_n}&=\frac1{e^{v_n}-1}-\frac 1{v_n}\\
		&=\frac1{v_n+\frac {v_n^2}2+o(v_n^2)}-\frac 1{v_n}\\
		&=\frac 1{v_n}\left(\frac 1{1+\frac{v_n}2+o(v_n)}-1\right)\\
		&\sim -\frac 12
		\end{align*}.$$

		Ainsi la série de terme général $\frac 1{v_{n+1}}-\frac 1{v_n}$ diverge et par comaraison des sommes partielles, $\sum_{n=0}^{N-1} \frac{1}{v_{n+1}}-\frac 1{v_n}\sim -\frac N2$. On en déduit $v_n\sim -\frac 2N$. C'est le terme général d'une série à termes négatifs divergente. Donc la série des $v_n$ diverge par comparaison des séries à termes négatifs.



!!! exercice "RMS2022-715"
	=== "Enoncé"

		Soit $f:[0,1]\to [0,1].$

		a)   On suppose $f$ continue. Montrer que $f$ admet un point fixe.

		b)   On suppose $f$ croissante. Montrer que $f$ admet un point fixe

		**Ind.**  Considérer $\sup\, \{x\in [0,1],\ f(x)\geq x\}$.



	=== "Corrigé"
		a) C'est une application directe du TVI : Si on pose $g(x)=f(x)-x$, on a $g(0)=f(0)\ge 0$ et $g(1)=f(1)-1\le 0$. La fonction $g$ étant constante, d'après le TVI il existe $c\in[0,1]$ tel que $g(c)=0$ c'est à dire $f(c)=c$.

		b) On suppose $f$ croissante. Puisque $f(0)=0$, l'ensemble $F=\{x\in[0,1], f(x)\ge x\}$ est non vide. il est majoré par $1$ donc il admet une borne sup qu'on appelle $\alpha$. Soit $x_n$ une suite d'éléments de $F$ qui converge vers $\alpha$ alors, $f$ étant croissante, $f(x_n)\le f(\alpha)$. Or $x_n\le f(x_n)$ donc $x_n\le f(\alpha)$. Par passage à la limite dans l'inégalité, $\alpha\le f(\alpha)$. Soit maintenant $n\in \mathbb N^*$, $\alpha+\frac 1n\notin F$ donc $f(\alpha+\frac 1n)<\alpha+\frac 1n$. On en déduit que $f(\alpha)\le f(\alpha+\frac 1n)<\alpha+\frac 1n$. Ainsi par passage à la limite dans l'inégalité, $f(\alpha)\le \alpha$. Finalement par antisymétrie de la relation d'ordre, $f(\alpha)=\alpha$.



!!! exercice "RMS2022-716"
	=== "Enoncé"
		Soit $f: {[a,b]}\rightarrow\mathbb R$ de classe ${\cal C}^1$.

		a) Montrer que $\int_{a}^b f(t)\cos(nt)\,d t$ tend vers $0$, quand $n$ tend vers $ +\infty$.  
		On admet que le résultat est encore vrai si $f$ est seulement continue par morceaux.

		b) Calculer $\int_0^{\pi} \left(-x+\frac{x^2}{2\pi }\right)\cos (nx)\  dx$.

		c) En déduire la somme de la série de terme général $\frac{1}{n^2}$




	=== "Corrigé"
		a) c'est un résultat classique qu'on résout par une intégration par parties.

		$$\begin{align*}
		\left|\int_a^b f(t)\cos(nt)dt\right|&={\left[\frac 1n\sin(nt)f(t)\right]}_a^b-\frac 1n\int_a^b f'(t)\sin(nt)dt\\
		&\le \frac{2\sup |f|}{n}+\frac{(b-a)\sup |f'|}n\\
		&\to 0
		\end{align*}$$

		b) On a :

		$$\begin{align*}
		\int_0^{\pi} \left(-x+\frac{x^2}{2\pi }\right)\cos (nx)\  dx&=\frac 1n\left[(-x+\frac{x^2}{2\pi})\sin(nt)\right]_0^\pi-\frac 1n\int_0^\pi (\frac x\pi-1)\sin(nt)dt\\
		&=0-\frac 1n\int_0^\pi(\frac x\pi-1)\sin(nt)dt\\
		&=\frac 1{n^2}\left[(\frac x\pi-1)\cos(nt)\right]-\frac 1{\pi n^2}\int_0\pi\cos(nt)dt\\
		&=\frac 1{n^2}
		\end{align*}$$

		c) On en déduit que

		$$
		\begin{align*}
		\sum_{n=1}^N \frac 1{n^2}&=\sum_{n=1}^N\int_0^\pi f(t)\cos(nt)dt\\
		&=\int_0^\pi f(t)\sum_{n=1}^N\cos(nt)dt
		\end{align*}
		$$

		Or en passant en complexes, on a

		$$
		\begin{align*}
		\sum_{n=1}^N\cos(nt)&=Re(\sum_{n=1}^N e^{int})=Re(e^{it}\frac{1-e^{iNt}}{1-e^{it}})\\
		&=Re(e^{i\frac{N+1}{2}t}\frac{\sin(\frac N2t)}{\sin(\frac t2)})\\
		&=\cos(\frac {N+1}2t)\frac{\sin(\frac N2t)}{\sin(\frac t2)}=\frac12\left(\frac{\sin(Nt+\frac t2)}{\sin\frac t2}-1\right).
		\end{align*}
		$$


		On a donc :

		$$
		\begin{align*}
		\sum_{n=1}^N \frac 1{n^2}&=\sum_{n=1}^N\int_0^\pi f(t)\cos(nt)dt\\
		&=\int_0^\pi f(t)\sum_{n=1}^N\cos(nt)dt\\
		&=\frac 12\int_0^\pi \frac{f(t)}{\sin\frac t2}\sin((N+\frac 12)t)dt-\frac12\int_0^\pi f(t)dt
		\end{align*}
		$$

		Or $t\mapsto \frac{f(t)}{\sin\frac t2}$ est continue en $0$ et partout ailleurs et par un résultat similaire à la première question (on se demande pourquoi elle est tournée de cette façon), on a donc $\int_0^\pi \frac{f(t)}{\sin\frac t2}\sin((N+\frac 12)t)dt\to 0$. On en déduit que $\sum_{n=1}^N \frac 1{n^2}\to -\frac12\int_0^\pi f(t)dt=\frac{\pi^2}{6}$.



!!! exercice "RMS2022-717"
	=== "Enoncé"
		a)   Montrer que la fonction $F:x\mapsto \int_0^x(\pi\lvert \sin t\rvert -2)dt$ est bornée sur $\mathbb R^+$.

		b)   Déterminer la nature de la série de terme général $\int_{n\pi}^{+\infty}\frac{\pi\lvert \sin t\rvert -2}{t} dt$.



	=== "Corrigé"
		a) $\int_{0}^{\pi}f(t) dt=[-\pi\cos(t)-2t]_0^\pi=0$. D'où $F(k\pi)=0$. Ainsi $F(\pi)=0$ On a alors $F(x+\pi)=\int_0^{x+\pi} f(t)dt=\int_0^\pi f(t)dt+\int_\pi^{x+\pi} f(t)dt=0+\int_0^x f(t+\pi)dt=\int_0^xf(t)dt=F(x)$. Ainsi $F$ est $\pi$ périodique et donc puisqu'elle est continue, elle est bornée.

		b) On procède par intégration par parties pour faire apparaître le calcul précédent.

		$$\begin{align*}
		u_{n,N} &= \int_{n\pi}^{N}\frac{\pi\lvert \sin t\rvert -2}{t} dt\\
		&=\left[\frac{F(t)}{t}\right]_{n\pi}^{N}+\int_{n\pi}^{N}\frac{F(t)}{t^2}dt\\
		\end{align*}$$

		Or $F(n\pi)=0$  (calcul précédent) et $F$ bornée donc $\lim_{t\to+\infty}\frac{F(t)}{t}=0$. L'intégrale résultante de l'IPP est convergente par comparaison ($O(\frac 1{t^2})$ en $+\infty$ par caractère bornée de $F$). On a ainsi :

		$$\begin{align*}
		u_n &= \int_{n\pi}^{+\infty}\frac{F(t)}{t^2}dt\\
		\end{align*}$$

		Étudions plus précisément $F$.  

		- On a $F(k\pi)=0$. En effet : $F(k\pi)=\int_{0}^{k\pi} f(t) dt =\sum_{i=0}^{k-1}\int_{i\pi}^{(i+1)\pi}f(t) dt=k\times \int_{0}^{\pi}f(t)dt$ car $f$ est $\pi$ périodique. Or $\int_{0}^{\pi}f(t) dt=[-\pi\cos(t)-2t]_0^\pi=0$. D'où $F(k\pi)=0$.
		- $F$ est $\pi$-périodique (déjà montré)
		- $F$ est symétrique sur $[0,\pi]$ par rapport au point $(\frac\pi2,0)$. En effet $F(\pi -x)=\int_0^{\pi-x} f(t)dt=-\int_{\pi}^xf(\pi-u)du=\int_{x}^\pi f(u)du$ car $f$ est $\pi$-périodique et paire. Ainsi $F(\pi-x)=F(\pi)-F(x)=-F(x)$. On a donc une symétrie.
		- l'étude du signe de $f$ permet d'obtenir les variations de $F$ et de montrer que $F$ est négative sur $[0,\frac\pi2]$ et positive sur $[\frac\pi2,\pi]$
		- $\int_0^\pi F(t)dt=-\int_{\pi}^0F(\pi-t)dt=-\int_0^\pi F(t)dt=0$.

		Notons $G$ la primitive de $F$ qui s'annule en $0$. Pour un entier naturel $n$ on a $G(n\pi)=\sum_{k=0}^{n-1}\int_{k\pi}^{(k+1)\pi}F(t)dt=n\times\int_{0}^\pi F(t)dt$. Or $\int_0^\pi F(t)dt=0$. Ainsi $G(n\pi)=0$. De plus $G(t+\pi)=\int_{0}^{t+\pi}F(t) dt=\int_0^\pi F(t)dt+\int_\pi^{t+\pi}F(t)dt=0+\int_0^xF(t+\pi)dt=\int_0^x F(t)dt=G(x)$. Donc $G$ est $\pi$-périodique. $G$ étant continue on en déduit en particulier que $G$ est bornée.

		On a alors :

		$$\begin{align*}
		u_n&=\int_{n\pi}^{+\infty}\frac{F(t)}{t^2}dt\\
		&=\underset{0\text{car G bornée et }G(n\pi)=0}{\underbrace{\left[\frac {G(t)}{t^2}\right]_{n\pi}^{+\infty}}}+2\int_{n\pi}^{+\infty}\frac{G(t)}{t^3}dt
		\end{align*}$$

		On a ainsi $|u_n|\le2\int_{n\pi}^{+\infty}\frac{||G||_{\infty}}{t^3}dt\le \frac {||G||_{\infty}}{n^2\pi^2}$. Ainsi par comparaison, $\sum u_n$ est absolument convergente donc convergente.
		\end{align*}$$




!!! exercice "RMS2022-718"
	=== "Enoncé"
		Soient $a_0, \ldots, a_n\in\mathbb R^{+*}$ distincts et, pour  $i\in[\![  0,n]\!]$, $P_i=\prod_{1\leq k\leq n,\; k\neq i}(X+a_k)$

		a)   Montrer que $(P_0,\ldots,P_n)$ est une base de $\mathbb R_n[X]$. Donner les coordonnées d'un polynôme $Q\in\mathbb R_n[X]$ dans cette base en fonction des $Q(-a_i)$ et des $P_i$.

		b)   Montrer la convergence et calculer l'intégrale $\int_0^{+\infty}\frac1{(t+a_0)(t+a_1)\cdots(t+a_n)}d    t$.



	=== "Corrigé"
		a) Par un résultat classique de cours, si on note $P=(X+a_0)(\dots)(X+a_n)$, on a pour tout polynôme $Q$ de $\mathbb R_n[X]$ :

		$$\frac QP = \sum_{i=0}^n\frac{Q(-a_i)/P'(-a_i)}{X+a_i}$$

		Or $P'(-a_i)=P_i(-a_i)$. De plus $\frac P{X+a_i}=P_i$. On en déduit que $Q=\sum_{i=0}^n\frac{Q(-a_i)}{P_i(a_i)}P_i$. La famille des $P_i$ est une famille génératrice de $\mathbb R_n[X]$ à $n+1$ élément : est est génératrice minimale et c'est donc une base.

		b) Pour avoir convergence, il faut $n\ge 1$. En plus l'infini, l'intégrande est alors équivalente à $\frac 1{t^2}$ et est donc convergente. Pas de problème en $0$ puisqu'aucun des $a_i$ n'est nul.

		D'après la question précédente,

		$$
		\begin{align*}
		\int_0^{N}\frac1{(t+a_0)(t+a_1)\cdots(t+a_n)}dt&=\sum_{k=0}^n\int_{0}^N\frac{1/P_i(-a_i)}{t+a_i}dt\\
		&=\sum_{k=0}^n\frac{1}{P_i(-a_i)}(\ln(N+a_i)-\ln(a_i))\\
		&=\ln(N)\sum_{k=0}^n\frac{1}{P_i(-a_i)}+\sum_{k=0}^n\frac{1}{P_i(-a_i)}(\ln(1+\frac {a_i}N)-\ln(a_i))
		\end{align*}.$$

		Or $\sum_{k=0}^n\frac{1}{P_i(-a_i)}$ est le terme de degré $n$ de la décomposition de $1$ dans la base de $P_i$ donc $\sum_{k=0}^n\frac{1}{P_i(-a_i)}=0$. On en déduit que $\int_0^{+\infty}\frac1{(t+a_0)(t+a_1)\cdots(t+a_n)}dt=-\sum_{k=0}^n\frac{1}{P_i(-a_i)}\ln(a_i)$.



!!! exercice "RMS2022-719"
	=== "Enoncé"
		Soit $f:\mathbb R^{+*}\to\mathbb R$ de classe $\mathcal{C}^1$.

		a)   Montrer que, pour tout $n\in\mathbb N^*$, $\int_n^{n+1}f(t) dt=f(n)+\int_n^{n+1}(n+1-t)f'(t) dt.$

		b)   On suppose  $f'$ intégrable sur $[1,+\infty[$. On note, pour  $n\in\mathbb N^*$, $u_n=f(n)$. Montrer que la série de terme général $u_n$ converge si et seulement si  la suite de terme général  $\int_1^nf(t)dt$  converge.

		c)   Soit $\alpha>1/2$. Étudier la convergence de la série de terme général $\frac{\cos(\sqrt{n})}{n^{\alpha}}$.



	=== "Corrigé"
		a) On effectue une intégration par parties en primitivant $1$ en $(t-n-1)$.

		$$
		\begin{align*}
		\int_n^{n+1}f(t)dt &= \left[(t-n-1)f(t)\right]_n^{n+1}-\int_n^{n+1}(t-n-1)f'(t)dt\\
		&=f(n)-\int_n^{n+1}(t-n-1)f'(t)dt\\
		&=f(n)+\int_{n}^{n+1}(n+1-t)f'(t)dt.
		\end{align*}$$

		b) On a donc $\sum_{n=1}^N u_n = \int_1^{N+1}f(t)dt-\sum_{n=1}^N\int_{n}^{n+1}(n+1-t)f'(t)dt$.  Or $\sum_{n=1}^N\int_{n}^{n+1}(\lceil t\rceil-t)f'(t)dt=\int_1^{N+1}(\lceil t\rceil-t)f'(t)dt$. Or $|(\lceil t\rceil-t)f'(t)|\le |f'(t)|$ et la fonction $f'$ est intégrable donc par comparaisons, $(\lceil t\rceil-t)f'(t)$ est intégrable. On en déduit que $\sum f(n)$ et $\int_1^nf(t)dt$ ont même nature.

		c) Probablement qu'il faut utiliser le point précédent. On pose $u_n=\frac{\cos(\sqrt{n})}{n^{\alpha}}$. On pose $f(x)=\frac{\cos(\sqrt{x})}{x^{\alpha}}$.
		On a $f'(x)=\frac{-\frac1{2\sqrt x}\sin(\sqrt x)x^\alpha-\alpha\cos(\sqrt x)x^{\alpha -1}}{x^{2\alpha}}=\frac{-\frac1{2}\sin(\sqrt x)x^{\alpha-\frac 12}-\alpha\cos(\sqrt x)x^{\alpha -1}}{x^{2\alpha}}\sim_{+\infty}-\frac 12\frac{\sin(\sqrt x)}{x^{\alpha+\frac 12}}$. Puisque $\alpha+\frac 12>1$ on en déduit par comparaisons que $f'$ est intégrable sur $[1,+\infty[$
		On est ainsi dans le cas de la question b) : la série de terme général $u_n$ est convergente si et seulement si la suite $\int_1^n f(t)dt$ converge. On pose alors $\alpha'=\alpha-\frac12>0$.

		$$
		\begin{align*}
		\int_1^n \frac{\cos(\sqrt x)}{x^\alpha}dx &= \int_1^n\frac{\cos(\sqrt x)}{\sqrt x}\times\frac1{x^{\alpha'}}dx\\
		&=\left[2\sin(\sqrt x)\times\frac1{x^{\alpha'}}\right]_1^n+2\alpha'\int_1^n\frac{\sin(\sqrt x)}{x^{\alpha'+1}}dx\\
		&=2\frac{\sin(\sqrt n)}{n^{\alpha'}}-2\sin(1)+2\alpha'\int_1^n\frac{\sin(\sqrt x)}{x^{\alpha'+1}}dx
		\end{align*}
		$$

		Le premier terme converge vers $0$, et l'intégrale est une intégrale convergent car $\frac{\sin(\sqrt x)}{x^{\alpha'+1}}=o(\frac{1}{x^{1+\frac{\alpha'}2}})$. On en déduit que $\int_1^n\frac{\cos(\sqrt x)}{x^\alpha}dx$ est une suite convergente donc la série de terme général $\frac{\cos(\sqrt n)}{n^\alpha}$ est convergente.



!!! exercice "RMS2022-720"
	=== "Enoncé"
		Soit $f\in{\cal C}^0([0,T],\mathbb C)$. Pour  $n\in\mathbb N^*$, soit $\varphi_n:x\mapsto \sum_{k=0}^{+\infty}\frac{(-1)^k}{k!}\int_0^T f(t)\, e^{-nk(x-t)} d    t$.

		- a)   Montrer que, pour $n\in\mathbb N$, $\varphi_n$ est définie sur $\mathbb R$.

		- b)   Étudier la convergence simple de la suite de fonctions $(\varphi_n)$.


	=== "Corrigé"
		a) L'intégrale étant bien définie (on intègre sur un segment) on se pose plutôt la question de la convergence de la série. Majorons l'intégrale.
		Fixons $x$ et $n$, on a alors :

		$$\begin{align*}
		\big|\frac{(-1)^k}{k!}\int_0^Tf(t)e^{ink(x-t)}dt &\le\int_O^T||f||_\infty \frac{e^{-nk(x-t)}}{k!}dt\\
		&\le \int_O^T||f||_\infty \frac{e^{-nk(x-T)}}{k!}dt\\
		&\le T||f||_\infty \frac{(e^{-n(x-T)})^k}{k!}\\
		\end{align*}$$
		où l'on reconnaît le terme général d'une série convergente. On en déduit que la série $\varphi_n(x)$ est absolument convergente donc convergente.

		b) Tout d'abord, pour $n$ et $x$ fixés, vérifions que l'on peut intervertir série et intégrale. Posons $f_k(t)=\frac{(-1)^k}{k!}f(t)e^{ink(x-t)}$. Comme montré précédemment,

		$$|f_k(t)|\le ||f||_\infty \frac 1{k!}e^{-nk(x-T)}$$

		qui est le terme général d'un série convergente. Ainsi, la série de de fonctions des $f_k$ converge absolument donc uniformément. On peut donc intervertir série et intégrale :

		$$\begin{align*}
		\varphi_n(x)&=\sum_{k=0}^{+\infty} \frac{(-1)^k}{k!}\int_0^T f(t)e^{-nk(x-t)}dt \\
		&= \int_0^T\sum_{k=0}^{+\infty} \frac{(-1)^k}{k!}f(t)e^{-nk(x-t)}dt\\
		&= \int_0^Tf(t)\sum_{k=0}^{+\infty} \frac{(-e^{-n(x-t)})^k}{k!}dt\\
		&= \int_0^T f(t)e^{-e^{-n(x-t)}}dt
		\end{align*}$$

		On a donc : $\displaystyle \varphi_n(x)=\int_0^Tf(t)e^{-e^{-n(x-t)}}dt$. On se ramène donc à un problème d'intégration d'une suite de fonctions. Soit $x$ fixé, posons $g_n(t)= f(t)e^{-e^{-n(x-t)}}$.

		Posons $g(t) = \begin{cases} 0 & \text{si }t>x\\f(t)&\text{si }t<x & f(x)e^{-1}&\text{sinon}\end{cases}$. Ainsi $g$ converge simplement vers une fonction continue par morceaux. De plus $|g_n(t)|\le |f(t)|$ qui est intégrable sur $[0,T]$. On en déduit que l'on peut échanger limite et intégrale par le théorème de convergence dominée. On a ainsi : $\lim_{n\to+\infty}\int_0^T g_n(t)dt=\int_0^T g(t)dt$ c'est à dire :

		$$$\varphi_n(x)\to \begin{cases} 0 & \text{si }x<0\\ \int_0^x & \text{si }0\le x\le T\\ \int_0^T f(t)dt&\text{si } x>T\end{cases}.$$




!!! exercice "RMS2022-721"
	=== "Enoncé"
		Soit $f(x)=\sum_{n=0}^{+\infty} a_n x^n$ une série entière à termes positifs telle que $\sum_{n=0}^{+\infty} a_n =1$.

		a) Montrer que $f$ est continue et dérivable sur $[0,1[$.

		b) Montrer que $f$ est continue sur $[0,1]$.

		On suppose maintenant que $f$ est dérivable en 1.

		c)  Montrer que $f'$ est croissante sur $[0,1[$ et bornée sur  $[0,1]$.

		d) Soit $N\geq 1$. Montrer que $\sum_{n=0}^{N} na_n\leq f'(1)$.

		e) En déduire que $f'(1)=\sum_{n=0}^{+\infty} na_n$.


	=== "Corrigé"
		a) Les $a_n$ étant positif et de somme égale à $1$, on en déduit que pour tout entier naturel $n$, $a_n<1$. Or la série entière $\sum x^n$ est de rayon de convergence $1$. Donc $\sum a_nx^n$ est de rayon de convergence au moins $1$. Par propriété des séries entières sur leur rayon de convergence, $f$ est continue et dérivable sur $[0,1[$.

		b) $f$ est donc continue sur $[0,1[$. La question est donc de savoir s'il y a continuité en $1$. On va utiliser le théorème de la double limite. On pose, pour $n$ un entier naturel, $f_n(x)=a_nx^n$

		$$|\sum_{k=n+1}^{+\infty}f_k(x)|=\sum_{k=n+1}^{+\infty}a_k x^k\le \sum_{k=n+1}^{+\infty} a_k\to_{n\to+\infty} 0.$$

		On en déduit que la convergence uniforme de la série de fonctions $\sum f_n$ sur l'intervalle $[0,1[$. Or $f_n(x)\to a_n$ en $1$. Donc d'après le théorème de la double limite, $\sum_{n=0}^{+\infty} f_n(x)\to_{x\to 1} \sum_{n=0}^{+\infty} a_n$. Il y a donc continuité en $1$.

		c) On peut dériver sur l'intérieur du disque de convergence. On a alors pour $x\in[0,1[$, $f''(x)=\sum_{n=2}^{+\infty} n(n-1)a_n x^{n-2}$ qui est bien positive (les $a_n$ étant positifs). Donc $f''$ est positive sur $[0,1[$ et $f'$ est croissante sur $[0,1[$. Ainsi, soit $f'$ converge en $1$, soit elle diverge vers $+\infty$ en $1^-$. Notons donc $\ell$ sa limite, éventuellement infinie. Dans ce cas, pour $x\in [0,1[$ on a d'après le théorème des accroissements finis, $f(1)-f(x)=f'(c_x)(1-x)$ et donc $\frac{f(1)-f(x)}{1-x}=f'(c_x)$ et par passage à la limite, $f'(1)=\ell$ puisque $f$ est dérivable en $1$. On en déduit que $\ell$ est finie, avec $\ell=f'(1)$. Finalement, $f'$ est bornée sur $[0,1]$.  


		d) Soit $N\ge 1$, on a $\sum_{n=0}^N na_nx^n<f'(x)\le f'(1)$ par croissance de $f'$. On en déduit que pour tout $x\in[0,1[$, $\sum_{n=0}^N na_nx^n\le f'(1)$. Par passage à la limite dans l'inégalité, $\sum_{n=0}^N na_n\le f'(1)$.

		e) La suite $\sum_{n=0}^N na_n$ est donc croissante et majorée, donc convergente. Notons $\ell'=\sum_{n=0}^{+\infty} na_n$, on a alors $\ell'\le f'(1)$. De plus, pour $x\in[0,1[$, $f'(x)=\sum_{n=0}^{+\infty} na_nx^n\le \sum_{n=0}^{+\infty} na_n$. Or $f'$ admet une limite en $1$ égale à $f'(1)$. Par passage à la limite dans l'inégalité encore une fois, on a donc $f'(1)\le \sum_{n=0}^{+\infty}$. On en déduit l'égalité.




!!! exercice "RMS2022-722"
	=== "Enoncé"
		On cherche à dénombrer les $n$-uplets $(a_1,\ldots, a_n)$ à valeurs dans $\{\pm1\}$ vérifiant les conditions :
		 $\forall p\in[\![   1,n]\!]$, $\sum_{k=1}^p a_k\geq0$ et   $\sum_{k=1}^n a_k=0$.


		a)   Montrer qu'il n'existe pas de tels $n$-uplets si $n$ est impair.  
		b)   Soit $u_n$ le nombre de $(2n)$-uplets satisfaisant ces conditions (avec la convention $u_0=1$). Déterminer $u_1$, $u_2$ et $u_3$.   
		c)   Trouver une relation de récurrence entre $u_n$ et $u_0, \dots, u_{n-1}$.  
		d)   Déterminer $u_n$. *Ind.* Considérer la somme de la série entière $\sum u_nx^n$.  


	=== "Corrigé"
		a) La condition $\sum_{k=1}^n a_k=0$ impose qu'il y ait autant de $+1$ que de $-1$. Ainsi $n$ doit êre pair.

		b) Pour $n=1$ la seule solution est $(1,-1)$. Pour $n=2$ on a $(1,-1,1,-1)$ et $(1,1,-1,-1)$ donc $u_2=2$. Enfin pour $n=3$, les solutions sont : $(1,1,1,-1,-1,-1)$, $(1,1,-1,1,-1,-1)$, $(1,1,-1,-1,1,-1)$, $(1,-1,1,-1,1,-1)$, $(1,-1,1,1,-1,-1)$. Donc $u_3=5$.

		c) Soit $(a_1,\dots,a_{2n})$ une solution pour $n$ donné. Notons $2k$ la position du premier retour à $0$. On a obligatoirement, par la première contrainte, $a_1=1$ et $a_{2k}=-1$. Entre les deux, on ne peut par redescendre à $0$. Il s'agit donc d'avoir un  $2(k-1)$-uplet vérifiant les mêmes propriétés : il y a $u_{k-1}$ possibilités. Puis on construit ensuite une solution au problème entre $2k$ et $2n$ : il y a $u_{n-k}$ possibilités. Ainsi, la relation de récurrence est : $u_n=\sum_{k=1}^n u_{k-1}u_{n-k}=\sum_{k=0}^{n-1}u_ku_{n-1-k}$.

		d) Si on enlève la première contrainte, le nombre de solutions est $\binom{2n}{n}$. On a donc $0\le u_n\le \binom{2n}{n}\le 2^{2n}=4^n$. Ainsi, la série entière de terme général $u_n$ a un rayon de convergence au moins égal à celui $\sum 4^nx^n$ qui vaut $\frac 14$. Posons sur $[0,\frac 14[$, $f(x)=\sum_{n=0}^{+\infty}u_n x^n$. On a alors :

		$$\begin{align*}
		f(x) &= \sum_{n=0}^{+\infty}u_n x^n\\
		&= u_0+\sum_{n=1}^{+\infty}\sum_{k=0}^{n-1}u_ku_{n-1-k}x^n\\
		&=u_0+x\sum_{n=0}^{+\infty}\sum_{k=0}^{n}u_ku_{n-k}x^n\\
		&=u_0+xf(x)^2 &\text{(On reconnait un produit de Cauchy)}
		\end{align*}$$

		Ainsi pour tout $x\in[0,\frac 14[$, $xf(x)^2-f(x)+1=0$. En traitant cela comme un trinôme, il vient $f(x)=\frac{1\pm\sqrt{1-4x}}{2x}$ et puisque $f(0)=1$, on en déduit $f(x)=\frac{1-\sqrt{1-4x}}{2x}$. Or :

		$$\begin{align*}
		\sqrt{1-4x}&=\sum_{n=0}^{+\infty}\frac{\prod_{k=0}^{n-1}(\frac 12-k)}{n!}(-4x)^n\\
		&=\sum_{n=0}^{+\infty}\frac{\prod_{k=0}^{n-1}(1-2k)}{2^nn!}(-4x)^n\\
		&=\sum_{n=0}^{+\infty}\frac{\prod_{k=1}^{n-1}(1-2k)}{2^nn!}(-4x)^n\\
		&=1+\sum_{n=1}^{+\infty}\frac{(-1)^{n-1}\prod_{k=1}^{n-1}(2k-1)}{2^nn!}(-4x)^n\\
		&=1+\sum_{n=1}^{+\infty}\frac{(-1)^{n-1}((2(n-1))!}{2^nn!2^{n-1}(n-1)!}(-4x)^n\\
		&=1-2\sum_{n=1}^{+\infty}\frac{(2(n-1))!}{n!(n-1)!}x^n\\
		&=1-2x\sum_{n=0}^{+\infty}\frac 1{n+1}\binom{2n}{n}x^n
		\end{align*}$$

		On a alors :

		$$\begin{align*}
		f(x)&=\frac{1-\sqrt{1-4x}}{2x}\\
		&=\sum_{n=0}^{+\infty}\frac 1{n+1}\binom{2n}{n}x^n
		\end{align*}$$

		Et par identification, on a donc $u_n=\frac 1{n+1}\binom{2n}{n}$.



!!! exercice "RMS2022-723"
	=== "Enoncé"
		Soit, pour  $n\in\N$, $f_n:x\mapsto \frac{x^n}{n!}\, e^{-x}$.

		- a)   Étudier la convergence simple et uniforme de $(f_n)$ sur $\R^+$.  
		- b)   Montrer que, pour tout $n$, $f_n$ est intégrable sur $\R^+$ et calculer son intégrale.  
		- c)   Déterminer $\lim\int_0^{+\infty} f_n(t)dt$. Commentaire ?

	=== "Corrigé"
		Par croissances comparées, $f_n(x)$ converge simplement vers $0$. On a alors $f'_n(x)=\left(\frac{x^{n-1}}{(n-1)!}-\frac{x^{n}}{n!}\right)e^{-x}=(1-\frac xn)\frac{x^{n-1}}{(n-1)!}e^{-x}$ qui s'annule en $n$. Cette valeur est un maximum. On a alors $|f_n(x)|\le |f_n(n)|=\frac{n^n}{n!}e^{-n}\sim \frac{1}{\sqrt{2\pi n}}\to 0$. Donc la convergence est bien uniforme.

		Par ailleurs, $f_n$ est continue en $0$ et $f_n(x)\underset{x\to +\infty}{=}o(\frac{1}{x^2})$ donc $f_n$ est intégrable sur $\mathbb R^+$. On a :

		$$
		\begin{align*}
		\int_{0}^{+\infty} f_n(t)dt &= \frac 1{n!}\int_0^{+\infty} t^ne^{-t}dt\\
		&\underset{ipp}{=}\frac1{(n+1)!}\left[t^{n}e^{-t}\right]+\frac 1{(n-1)!}\int_{0}^{+\infty}t^{n-1}e^{-t}dt\\
		&=\int_0^{+\infty} f_{n-1}(t)dt
		\end{align*}
		$$

		Cette intégrale est indépendante de $n$ et vaut donc $\int_0^{+\infty}f_0(t)dt=\int_0^{+\infty}e^{-t}dt=1$.

		On a donc $\lim_{n\to+\infty}\int_0^{+\infty} f_n(t)dt=1\ne \int_{0}^{+\infty}\lim_{n\to+\infty}f_n(t)dt=0$.  On est donc dans un cas de non application du théorème de convergence dominée. A priori, c'est l'hypothèse de domination qui est mise en défaut : soit $\varphi$ intégrable qui majore $f_n$ pour tout $n$. Notons $T_n$ le triangle isocèle de base $[n-\frac 12,n+\frac 12]$ de hauteur $f_n(n)$. On a $f_n>T_n$. Soit $g$ la fonction dont le graphe est l'union de tous les $T_n$. L'aire de $T_n$ est $f_n(n)$, on a bien sûr $\varphi>g$ et $\int_0^{+\infty}g=\sum\mathcal A(T_n)$ qui diverge car $f_n(n)\sim\frac1{\sqrt{2\pi n}}$ qui est le terme général d'un série divergente. Donc $\varphi$ ne sera pas intégrable.  
	<div class="admonition-foot">convegence dominée, intégrabilité</div>


!!! exercice "RMS2022-724"
	=== "Enoncé"
		Soient $f\in{\cal C}^0([0,1], \R)$  et, pour $n\in\N$,  	$I_n=\int_0^1f(t^n)dt$. Limite de $(I_n)$ ?

	=== "Corrigé"
		Prenons une telle fonction $f$. Soit $f_n(t)=f(t^n)$. Si $0\le t<1$,, alors $t^n\underset{n\to+\infty}\to 0$ et $f$ étant continue en $0$, $f(t^n)\underset{n\to+\infty}\to f(0)$. On en déduit que $f_n$ converge simplement vers la fonction continue par morceaux $\ell$ définie par $\forall 0\le t<1$, $\ell(t)=f(0)$ et $\ell(1)=f(1)$.

		De plus $f$ est continue sur le segment $[0,1]$ donc d'après le théorème des bornes atteintes, $f$ est bornée. On a donc pour tout entier naturel  $n$, $|f_n(t)| \le M$ qui est une fonction constante donc intégrable sur $[0,1]$. D'après le théorème de convergence dominée, on peut affirmer que $\lim_{n\to+\infty}\int_0^1 f(t^n)dt=\int_{0}^1\ell(t)dt=f(0)$.
	<div class="admonition-foot">convergence dominée</div>


!!! exercice "RMS2022-725"
	=== "Enoncé"
		Soit $\alpha >0$.

		- a) Montrer que, pour $n$ assez grand, l'intégrale    $u_n= \int_0^{+\infty} \frac{1}{(1+t^{\alpha})^n} \d t$ est bien définie.
 		- b)Trouver une relation entre $u_n$ et $u_{n+1}$.
		- c) Trouver un équivalent de $u_n$.
 		- d) Montrer que la série $\sum (-1)^nu_n$ converge.

	=== "Corrigé"
		a) Posons $f_n(t)=\frac1{(1+t^\alpha)^n}$. L'intégrale est bien définie en $0$ où il n'y a pas d'ambiguité. En $+\infty$, $f_n(t)\sim \frac{1}{t^{n\alpha}}$. Or pour $n$ assez grand, $n\alpha>1$ et l'intégrale converge.

		b) A priori on considère $n\ge n_0$ avec $n_0$ l'entier identifié à la question précédente. On a :

		$$
		\begin{align*}
		u_{n+1}&=\int_{0}^{+\infty}\frac{1}{(1+t^\alpha)^{n+1}}dt\\
		&=\int_0^{+\infty}\frac1{(1+t^\alpha)^n}-t\times \frac{t^{\alpha-1}}{(1+t^\alpha)^{n+1}}dt\\
		&=u_n + \left[t\times \frac {1/(n\alpha)}{(1+t^\alpha)^n}\right]_0^{+\infty}-\frac 1{n\alpha}\int_0^{+\infty}\frac 1{(1+t^\alpha)^n}dt\\
		&=u_n-\frac 1{n\alpha}u_n\\
		&=\frac{n\alpha -1}{n\alpha}u_n
		\end{align*}
		$$

		On a donc la relation $u_{n+1}=\frac{n\alpha-1}{n\alpha}u_n$.

		c) Effectuons un changement de variable en $t^\alpha = \frac un$. On a :

		$$
		\begin{align*}
		u_n&=\int_0^{+\infty}\frac1{(1+t^\alpha)^n}dt\\
		&=\frac 1{n^{1/\alpha}\alpha}\int_0^{+\infty}\frac{u^{\frac 1\alpha-1}}{(1+\frac un)^n}du
		\end{align*}
		$$

		Posons $f_n(u)=\frac{u^{\frac 1\alpha-1}}{(1+\frac un)^n}$. On sait que $f_n(u)\underset{n\to+\infty}\to u^{\frac 1\alpha-1}e^{-u}$ (convergence simple). De plus $(1+\frac un)^n$ converge vers $e^u$ avec $u>0$ donc il existe un rang à partir duquel $(1+\frac un)^n>e^{\frac u2}$. On en déduit qu'à partir de ce rang, $|f_n(u)|\le u^{\frac 1\alpha-1}e^{-\frac u2}$ qui est intégrable sur $\mathbb R^+$. D'après le théorème de convergence dominée, on a $n^{\frac 1\alpha}u_n\to\frac 1\alpha\int_{0}^{+\infty}u^{\frac 1\alpha -1}e^{-u}du=\frac 1\alpha\Gamma(\frac 1\alpha)=\Gamma(\frac 1\alpha+1)=\Gamma(\frac{\alpha+1}\alpha)$. On en déduit un équivalent de $u_n\sim \frac{\Gamma(\frac {\alpha+1}{\alpha})}{n^{\frac 1\alpha}}$.

		d) On a $\frac{u_{n+1}}{u_n}=\frac{n\alpha-1}{n\alpha}<1$ et les $u_n$ sont positifs. Donc $(u_n)$ est décroissante et le résultat demandé est une application directe du critère spécial des séries alternées.
	<div class='admonition-foot'>intégration,convergence dominée,fonction gamma</div>

!!! exercice "RMS2022-726"
	=== "Enoncé"
		Soit, pour  $n\in\N$, $u_n=\int_0^{\frac{\pi}{2}}\cos^n(t)\sin(nt)\d    t$.

		- a)   Montrer que $(u_n)$ converge.
		- b)   Montrer que, pour tout $n\in\N$, $u_{n+1}-u_n=\frac1{n+1}-u_{n+1}$.
		- c)   Quelle est la nature de la série de terme général $u_n$? Donner un équivalent de la somme partielle de cette série.

	=== "Corrigé"
		On travaille ici sur une intégrale sur un segment. posons $f_n(t)=\cos^n(t)\sin(nt)$. Alors pour tout $t\in[0, \frac\pi2]$, $f_n(t)\to 0$. De plus $|f_n(t)|\le 1$ qui est intégrable sur $[0,\frac\pi2]$ donc par théorème de convergence dominée, $u_n\to 0$.

		Soit $n\in\mathbb N$,

		$$
		\begin{align*}
		u_{n+1}-u_n&=\int_0^{\frac\pi2}\cos^n(t)(\cos(t)\sin((n+1)t)-\sin(nt))\\
		&=\int_0^{\frac\pi2}\cos^n(t)(\cos(t)\sin((n+1)t)-\sin((n+1)t-t))\\
		&=\int_0^{\frac\pi2}\cos^n(t)(\cos(t)\sin((n+1)t)-\sin((n+1)t)\cos(t)+\sin(t)\cos((n+1)t))\\
		&=\int_0^{\frac\pi2}\cos^n(t)(\sin(t)\cos((n+1)t))\\
		&=\left[-\frac 1{n+1}\cos^{n+1}(t)\cos((n+1)t)\right]_0^{\frac\pi2}-\int_0^{\frac\pi2}\cos^{n+1}(t)\sin((n+1)t)dt\\
		&=\frac 1{n+1}-u_{n+1}
		\end{align*}
		$$

		On a donc $2u_{n+1}-u_n=\frac 1{n+1}$. Sommons de $0$ à $N$ et notons $S_N$ la somme partielle de $\sum u_n$ d'ordre $N$. On a :
		$2(S_{N+1}-u_0)-S_N=H_{N+1}$ où $H_N$ est la somme partielle de la série harmonique d'ordre $N$. On peut donc écrire : $S_N=H_{N+1}+2u_0-2u_{N+1}$. On en déduit que $S_N$ diverge car $H_N$ diverge. De plus $u_{N+1}$ tend vers $0$ donc $S_N\sim \ln(N)$.
	<div class="admonition-foot">convergence dominée,équivalent</div>

!!! exercice "RMS2022-727"
	=== "Enoncé"
		Soient, pour  $n\in \N^*$, $u_n=\int_0^{1/2} \frac{\sin^2(n\pi x)}{\tan(\pi x)} \d x $ et $v_n=\int_0^{1/2} \frac{\sin^2(n\pi x)}{\pi x} \d x $.

		- a) Montrer que $\int_{\pi}^{+\infty} \frac{\cos(u)}{u} \d u $ converge.
		- b) Montrer que $v_n\sim \frac{\ln(n)}{2\pi}$.
		- c) Soit $f:x\in ]0,1/2[\mapsto \frac{1}{\tan(\pi x)}-\frac{1}{\pi x}$.
		Montrer que $f$ se prolonge par continuité.
		- d) Trouver un équivalent de $u_n$.
	=== "Corrigé"
		On procède avec une intégration par parties :

		$$
		\int_\pi^{+\infty}\frac{\cos(u)}{u}du=[\frac{\sin(u)}{u}]_\pi^{+\infty}+\int_\pi^{+\infty}\frac{\sin(t)}{t^2}dt=\int_\pi^{+\infty}\frac{\sin(t)}{t^2}dt
		$$

		Or $\frac{\sin(t)}{t^2}=o(\frac{1}{t^{3/2}})$ donc est convergente.

		On a :

		$$
		\begin{align*}
		v_n &= \int_{0}^{\frac12}\frac{\sin^2(n\pi x)}{\pi x}\\
		&\underset{u=nx}=\int_0^{n/2}\frac{\sin^2(\pi u)}{\pi u}du\\
		&=\int_0^{n/2}\frac{1-\cos(2\pi u)}{2\pi u}du\\
		&=\underset{A}{\underbrace{\int_0^{\pi}\frac{1-\cos(2\pi u)}{2\pi u}du}}+\int_\pi^{n/2}\frac1{2\pi u}du+\underset{\text{converge}=B(n)}{\underbrace{\int_\pi^{n/2}\frac{\cos(2\pi u)}{2\pi u}du}}\\
		&=A+\frac 1{2\pi}(\ln(n/2)-\ln(\pi))+B(n)\\
		&\sim\frac 1{2\pi}\ln(n)
		\end{align*}
		$$

		Posons $f(x)=\frac{1}{\tan(\pi x)}-\frac 1{\pi x}$. On a

		$$
		\begin{align*}
		f(x) &= \frac{1}{\pi x+\frac{\pi^3x^3}{3}+o(x^3)}-\frac1{\pi x}\\
		&= \frac1{\pi x}\left(\frac1{1+\frac{\pi^2x^2}{3}+o(x^2)}-1\right)\\
		&\sim -\frac 1{\pi x}\times\frac{\pi^2x^2}3\\
		&\sim -\frac{\pi x}3
		&\to 0
		\end{align*}
		$$

		On a enfin  :

		$$
		\begin{align*}
		u_n &=\int_0^{1/2}\frac{\sin^2(n\pi x)}{\tan(\pi x)}dx\\
		&=\int_0^{1/2}\sin^2(n\pi x)f(x)dx+v_n
		\end{align*}
		$$

		Or $|\int_0^{1/2}\sin^2(n\pi x)f(x)dx|\le\int_0^{1/2}|f(x)|dx$ donc le premier membre est borné. On en déduit que $u_n\sim v_n$ c'est à dire $u_n\sim \frac{\ln(n)}{2\pi}$.
	<div class='admonition-foot'>intégration</div>



!!! exercice "RMS2022-728"
	=== "Enoncé"
		Soit, pour  $\alpha>0$ et  $n\in\N$, $u_n(\alpha)=\int_0^{\frac{\pi}{2}}\sin^{\alpha}(t)\, \cos^n(t)dt$.

		- a) Déterminer la nature de la série de terme général $u_n(\alpha)$ selon les valeurs de $\alpha$.  
		- b)   Calculer les sommes des séries de terme général $u_n(2)$ et $u_n(3)$.  

	=== "Corrigé"
		Si $\alpha=1$ alors $u_n(1)=\int_0^{\frac\pi2}\sin(t)\cos^n(t)dt=\frac 1{n+1}$ qui est le terme général d'une série divergente : $\sum u_n(1)$ est divergente.

		Si $\alpha<1$, alors pour $t\in[0,\frac\pi2]$, $\sin^\alpha(t)\ge\sin(t)$ et donc $\sum u_n(\alpha)$ diverge par théorème de comparaison des séries à termes positifs.

		Si $\alpha>1$, on a :

		$$
		\begin{align*}
		\sum_{n=0}^Nu_n(\alpha)&=\int_{0}^{\frac\pi2}\sin^\alpha(t)\frac{1-\cos^{N+1}(t)}{1-\cos(t)}dt\\
		\end{align*}
		$$

		En $0$, on a $\sin^\alpha(t)\frac{1-\cos^{N+1}(t)}{1-\cos(t)}\sim t^\alpha\frac{\frac{N+1}2t^2}{\frac{t^2}2}\sim (N+1)t^\alpha$ qui converge car $\alpha>1$. De plus pour $t\in[0,\frac\pi2]$, $\sin^\alpha(t)\frac{1-\cos^{N+1}(t)}{1-\cos(t)}\to\frac{\sin^\alpha(t)}{1-\cos(t)}$ et  $|\sin^\alpha(t)\frac{1-\cos^{N+1}(t)}{1-\cos(t)}|\le \frac{2\sin^\alpha(t)}{1-\cos(t)}$. Cette fonction est intégrable, $\sim 2t^{\alpha-2}$ en $0$, qui est intégrable en $0$ car $t-2>-1$. Par théorème de convergence dominée, on a $\sum_{n=0}^N u_n(\alpha)$ converge vers $\int_0^{\frac\pi2}\frac{\sin^\alpha(t)}{1-\cos(t)}dt$.

		Cas: $\alpha=2$.

		$$
		\int_0^{\pi / 2} \frac{\sin ^2(t)}{1-\cos (t)} \mathrm{d} t=\int_0^{\pi / 2} 1+\cos (t) \mathrm{d} t=\frac{\pi}{2}+1
		$$

		Cas: $\alpha=3$

		$$
		\int_0^{\pi / 2} \frac{\sin ^3(t)}{1-\cos (t)} \mathrm{d} t=\int_0^{\pi / 2} \sin (t)(1+\cos (t)) \mathrm{d} t=\frac{3}{2}
		$$
	<div class='admonition-foot'>convergence dominée,intégration</div>

!!! exercice "RMS2022-729"
	=== "Enoncé"
		- a)   Soit $\varphi\in{\cal C}^1([0,{\pi}/{2}], \R)$. Montrer que $\int_0^{\frac{\pi}{2}}\varphi(t)\sin \left((2n+1)t\right)dt\underset{n\rightarrow +\infty}\longrightarrow0$.
		- b)   Justifier la convergence des intégrales suivantes, pour $n\in\N$:

		$$
		\begin{align*}
		I&=\int_0^{+\infty}\frac{\sin (t)}{t}dt\\ I_n&=\int_0^{\frac{\pi}{2}}\frac{\sin((2n+1)t)}{\sin t}dt\\ J_n&=\int_0^{\frac{\pi}{2}}\frac{\sin((2n+1)t)}{t} dt.
		\end{align*}
		$$

		- c)   Calculer $I_n-I_{n-1}$, puis $I_n$, pour tout $n\in\N$.
		- d)   Montrer que $I=\frac{\pi}{2}.$
	=== "Corrigé"
		a) On exploite le caractère $\mathcal C^1$ :

		$$
		\begin{align*}
		\int_0^{\frac{\pi}{2}}\varphi(t)\sin \left((2n+1)t\right)dt &= \left[-\frac1{2n+1}\cos((2n+1)t)\varphi(t)\right]_0^{\frac \pi2}+\frac 1{2n+1}\int_0^{\frac\pi2}\varphi'(t)\cos((2n+1)t)dt\\
		&=\frac1{2n+1}\varphi(0)+\frac 1{2n+1}\int_0^{\frac\pi2}\varphi'(t)\cos((2n+1)t)dt
		\end{align*}
		$$

		Or $|\int_0^{\frac\pi2}\varphi'(t)\cos((2n+1)t)dt|\le\int_0^{\frac\pi2}|\varphi'(t)|dt$ qui est fini puisque $\varphi'$ est continue. On en déduit en passant à la limite que $\int_0^{\frac{\pi}{2}}\varphi(t)\sin \left((2n+1)t\right)dt\to 0$.

		b) $I$ est convergente car $\frac{\sin(t)}t\underset0\to 1$ donc l'intégrale est faussement impropre en $0$.

		$I_n$ et $J_n$ sont convergentes pour les mêmes raisons.

		c) On a :

		$$
		\begin{align*}
		I_n-I_{n-1}&=\int_0^{\frac\pi2}\frac{\sin((2n+1)t)-\sin((2n-1)t)}{\sin(t)}dt\\
		&=\int_0^{\frac\pi2}\frac{2\cos(2nt)\sin(t)}{\sin(t)}dt\\
		&=\int_0^{\frac\pi2}2\cos(2nt)dt\\
		&=\left[\frac 1{n}\sin(2nt)\right]_0^{\frac\pi2}\\
		&=0
		\end{align*}
		$$

		On trouve que $I_n$ est constante. On en déduit que pour tout $n$, I_n=I_0=\frac\pi2$.

		On a alors :

		$$
		\begin{align*}
		J_n &= \int_{0}^{\frac\pi2}\frac{\sin((2n+1)t)}{\sin(t)}dt\\
		&= \int_0^{(2n+1)\frac\pi2}\frac{\sin(u)}{u}du\\
		&\to I
		\end{align*}
		$$

		Or $J_n=J_n-I_n+I_n$ et $J_n-I_n=\int_{0}^{+\infty}\sin((2n+1)t)\left(\frac 1t-\frac 1{\sin(t)}\right)$.

		Posons $\varphi(t)=\frac 1t -\frac 1{\sin(t)}$ et $\varphi(0)=0$. D'après le théorème de prolongement $\mathcal C^1$, $\varphi$ est $\mathcal C^1$ sur $[0,\frac\pi2]$. D'après la première question, $J_n-I_n\to0$. On en déduit $J_n\to\frac\pi2$ et donc $I=\frac\pi2$.

	<div class="admonition-foot">suites d'intégrale,Dirichlet</div>



!!! exercice "RMS2022-730"
	=== "Enoncé"
		Soit  $f:x\mapsto \int_2^{+\infty}\frac1{\sqrt{t(t-2)(t-x)}}\d    t$.

		- a)   Déterminer le domaine de définition de $f$.  
		- b)   Montrer que $f$ est développable en série entière au voisinage de 0.

	=== "Corrigé"
		a) Pour que $f$ soit définie en $x$ il faut que pour tout $t\in ]2,+\infty]$, $t-x>0$ c'est à dire $x<2$.  
		Si $x=2$ alors $\frac{1}{\sqrt{t(t-2)(t-x)}}\underset2\sim\frac{1/\sqrt 2}{t-2}$ qui n'est pas intégrable au voisinage de $2$.  
		Si $x<2$ alors $\frac{1}{\sqrt{t(t-2)(t-x)}}\underset2\sim\frac{1/(\sqrt 2\sqrt {2-x})}{\sqrt{t-2}}$ qui est bien intégrable au voisinage de $0$. En $+\infty$, $\frac{1}{\sqrt{t(t-2)(t-x)}}\sim\frac1{t^{3/2}}$ qui est bien intégrable (Riemann).  
		On en déduit que l'ensemble de définition de $f$ est $]-\infty,2[$.

		b) On se pose maintenant la question du caractère développable en série entières de $f$.
		Par les calculs classiques on trouve pour $x\in]-2;2[$ :

		$$
		\frac1{\sqrt{t(t-2)(t-x)}}=\sum_{k=0}^{+\infty}\frac1{2^{2k}}\binom {2k}k\frac1{t^{k+1}\sqrt{t-2}}x^k.
		$$

		Posons $S_n(t)$ la somme partielle. $S_n(t)$ est continue par morceaux sur $]2,+\infty[$, et converge donc simplement vers $\frac1{\sqrt{t(t-2)(t-x)}}$, continue par morceaux sur $]2,+\infty[$. On a par ailleurs :

		$$
		\begin{align*}
		|S_n(t)|&=\sum_{k=0}^n\frac 1{2^{2k}}\binom {2k}k\frac{1}{t^{k+1}\sqrt{t-2}}x^k\\
		&\le\frac1{t\sqrt{t-2}}\sum_{k=0}^n\left(\frac xt\right)^k&\text{car }\frac 1{2^{2k}}\binom{2k}k\le 1\\
		&\le\frac1{t\sqrt{t-2}}\frac 1{1-\frac xt}&\text{car }\frac xt<1\\
		&\le\frac 1{\sqrt{t-2}(t-x)}
		\end{align*}
		$$

		qui est intégrable sur $]2,+\infty[$.

		D'après le théorème de convergence dominée, on a donc $\lim_{n\to+\infty}\int_2^{+\infty}S_n(t)dt=\int_2^{+\infty}\lim_{n\to +\infty}S_n(t) dt$ ce qui se traduit par :

		$$
		\sum_{k=0}^{+\infty}\frac 1{2^{2k}}\binom {2k}k I_k x^k=f(x)
		$$
		avec $I_k=\int_2^{+\infty}\frac 1{t^{k+1}\sqrt{t-2}}dt$.

		Ainsi $f$ est développable en séries entières.

	<div class="admonition-foot">séries entières,intégration</div>



!!! exercice "RMS2022-731"
	=== "Enoncé"
		Soit $\varphi:\R\to\R$ continue et $T$ -périodique. On pose $f:x\mapsto\int_0^{+\infty}e^{-tx}\varphi(t)\d    t$.

		- a)  Montrer que $f$ est définie sur $\R^{+*}$.
		- b)   Étudier la limite de $f$ en $+\infty$.
		- c)   Montrer que $f$ est de classe $\mathcal{C}^{\infty}$.
		- d)   Montrer que, pour tout $x\in\R^{+*}$, $f(x)=\frac1{1-e^{-Tx}}\int_0^Te^{-tx}\varphi(t)\d    t$. En déduire un équivalent de $f$ en $0$.

	=== "Corrigé"
		a) Soit $x\in\mathbb R^+_*$, $t\in \mathbb R^+$, posons $g(x,t)=e^{-tx}\varphi(t)$. La fonction $g(x,.)$ est intégrable en $0$ car elle est continue. Pour l'étude en $+\infty$, remarquons que $\varphi$ étant périodique et continue sur $\mathbb R$, alors nécessairement elle est bornée. On a alors $|t^2e^{-xt}\varphi(t)|\le t^2e^{-xt}M\to 0$. On en déduit que $e^{-tx}\varphi(t)=o(\frac 1{t^2})$ et ainsi $g(x,.)$ est intégrable en $+\infty$. $f$ est donc bien définie sur $\mathbb R^+_*$.

		b) En utilisant toujours le caractère borné de $\varphi$, on a $|f(x)|=\left|\int_0^{+\infty}e^{-xt}\varphi(t)dt\right|\le M\int_0^{+\infty}e^{-xt}dt=\frac 1x$. On en déduit par encadrement que $f(x)$ converge vers $0$ en $+\infty$.

		c) Soit $a\in\mathbb R^+_*$ et $n\in\mathbb N$. On a $\left|\frac{\partial^n g}{x^n}(x,t)\right|\le Mt^ne^{-at}$ qui est intégrable sur $\mathbb R^+$.

		De plus pour $0\le j<n$, $\frac{\partial^jg}{\partial x^j}(x,t)=(-t)^je^{-xt}\varphi(t)$. est bien intégrable sur $\mathbb R^+$.

		On en déduit que $f$ est de classe $\mathcal C^n$ pour tout $n$, donc $f$ est $\mathcal C^\infty$.

		d) Utilisons la relation de Chasles :

		$$
		\begin{align*}
		f(x) &= \int_{0}^{+\infty}e^{-xt}\varphi(t)dt\\
		&= \sum_{k=0}^{+\infty}\int_{kT}^{(k+1)T}e^{-xt}\varphi(t)dt\\
		&= \sum_{k=0}^{+\infty}\int_0^Te^{-x(t+kT)}\varphi(t+kT)dt\\
		&= \sum_{k=0}^{+\infty}e^{-kxT}\int_0^Te^{-xt}\varphi(t)dt&\text{par périodicité}\\
		&= \frac{1}{1-e^{-xT}}\int_0^{T}e^{-xt}\varphi(t)dt
		\end{align*}
		$$

		On en déduit un équivalent en $\frac 1{xT}\int_{0}^T\varphi(t)dt$ : en effet, 	$\left|\int_{0}^T\varphi(t)dt-\int_{0}^Te^{-xt}\varphi(t)dt\right|\le M\int_0^{T}1-e^{-xt}dt=M(T+\frac 1x e^{-xT}-\frac 1x)=M(T+\frac 1x(1-xT+\frac{x^2T^2}{2}+o(x^2))-\frac 1x)=\frac{xT^2}{2}+o(x)\underset{x\to 0}\to 0$ (ou par convergence dominée continue).
	<div class="admonition-foot">convegence dominée, intégrale à paramètre</div>

!!! exercice "RMS2022-732"
	=== "Enoncé"
		Soient $f : x \mapsto  \int_0^{+\infty} \frac {e^{-xt}}{1+t^2} \d    t$, $g : x \mapsto \cos(x)\int_x^{+\infty} \frac {\sin t}{t} \d    t - \sin(x)\int_x^{+\infty} \frac {\cos t}{t} \d    t$.

		- a)  Justifier que $\int_0^{+\infty} \frac {\sin t}{t} \d    t$ et $\int_1^{+\infty} \frac {\cos t}{t} \d    t$ convergent.  
		- b)   Montrer que $f$ et $g$ sont solutions de l'équation différentielle $y'' + y = \frac 1x$.  
		- c)   En déduire que $f = g$, puis en déduire les valeurs de $\int_0^{+\infty} \frac {\sin t}{t} \d    t$ et  $\int_0^{+\infty} \frac {\sin^2 t}{t^2} \d    t$.

	=== "Corrigé"
		a) La fonction $t\mapsto \frac{\sin(t)}{t}$ est prolongeable par continuité en $0$. De plus, $\int_0^N\frac{\sin(t)}{t}dt=\left[\frac {1-\cos(t)}t\right]_0^N+\int_0^N\frac{1-\cos(t)}{t^2}dt=\frac{\cos(N)-1}{N}+\int_0^N\frac{1-\cos(t)}{t^2}dt$. L'intégrale converge car l'intégrande est prolongeable par continuité en $0$ et un $o(\frac{1}{t^{3/2}})$ en $+\infty$. On en déduit que $\int_0^{+\infty}\frac{\sin (t)}tdt$ converge.  
		On fait le même raisonnement pour la seconde intégrale.

		b) Notons $\varphi(x,t)=\frac{e^{-xt}}{1+t^2}\d t$. Alors :
		- i) $\varphi(.,t)$ est $\mathcal C^1$ pour tout $t\in\mathbb R^+_*$
		- ii) pour tout $x\in\mathbb R^+_*$, $\varphi(x,.)$ est intégrable dur $\mathbb R^+_*$
		- iii) Soit $x\in\mathbb R^+_*$, $\frac{\partial \varphi}{\partial x}(x,t)=-t\times\frac{e^{-xt}}{1+t^2}$ est continue par morceaux sur $\mathbb R^+_*$
		- iv) soit $(x,t)\in [a,+\infty[\times\mathbb R^+_*$, $\left|\frac{\partial f}{\partial x}(x,t)\right|\le \frac {te^{-at}}{1+t^2}$ qui est intégrable sur $\mathbb R^+_*$.  
		On en déduit que $f$ est de classe $\mathcal C^1$ sur tout $[a,+\infty[$ et donc $f$ est de classe $\mathcal C^1$ sur $\mathbb R^+_*$. De plus $f'(x)=-\int_0^{+\infty}t\frac{e^{tx}}{1+t^2}\d t$.

		On montre de même que $f$ est $\mathcal C^2$ et $f''(x) = \int_0^{+\infty}t^2\frac{e^{-xt}}{1+t^2}\d t$. On a alors $f''(x)+f(x)=\int_0^{+\infty}e^{-xt}dt=\frac 1x$.

		En dérivant simplement deux fois $g$ (le paramètre est aux bornes cette fois) on trouve de même que $g''(x)+g(x)=\frac 1x$. On en déduit que $f(x)=g(x)+a\cos(x)+b\sin(x)$. Or par théorème de convergence dominée à paramètre continu, $f(x)\underset{x\to+\infty}{\to}0$ (majoration : $|\varphi(x,t)|\le\frac 1{1+t^2}$) et $g(x)\to 0$ (restes d'intégrales convergentes). On en déduit affectivement que $f=g$.

		c) Soit $x\in\mathbb R^+_*$, $|\sin(x)\int_{x}^{+\infty}\frac{\cos(t)}{t}\d t|\le x\int_x^{1}\frac 1tdt+x\int_1^{+\infty}\frac{\cos(t)}tdt=x\ln(x)+x\int_1^{+\infty}\frac{\cos(t)}tdt\underset{x\to 0}\to0$ par croissances comparées. On en déduit que $g(x)\underset{x\to0}\to \int_0^{+\infty}\frac{\sin(t)}{t}\d t$. Or $f(0)=\int_0^{+\infty}\frac 1{1+t^2}\d t=\frac\pi 2$. D'où $\int_{0}^{+\infty}\frac{\sin(t)}t\d t=\frac\pi2$.

		De plus $\int_0^{+\infty} \frac{\sin^2(t)}{t^2}\d t=-[\frac{\sin^2(t)}{t}]_0^{+\infty}+\int_0^{+\infty}\frac{2\sin(t)\cos(t)}{t}\d t=\int_0^{+\infty}\frac{\sin(2t)}{t}\d t\underset{u=2t}=\int_0^{+\infty}\frac{\sin u}{u}\d u=\frac\pi2$.
	<div class="admonition-foot">convergence dominée,intégrale à paramètre</div>
