!!! exercice "RMS2022-692"
	=== "Enoncé"
		Soient $E$ un $\mathbb R$-espace vectoriel de dimension finie et $f$ un endomorphisme de $E$ tel que $f^2$ soit un projecteur.

		a)  Préciser un polynôme annulateur de $f$.

		b) Montrer qu'il existe deux sous-espaces supplémentaires $F$ et $G$ stables par $f$ tels que l'endomorphisme induit par $f$ sur $F$ soit inversible et l'endomorphisme induit par $f$ sur $G$ soit nilpotent.

		c) Montrer que $f$ est diagonalisable si et seulement si  $\mathrm{rg} (f^2)=\mathrm{rg} (f)$.



	=== "Corrigé"
		a) $f^2$ est un projecteur donc $(f^2)^2=f^2$. $f$ annule donc le polynôme $X^4-X^2$.

		b) $f$ annule donc $X^4-X^2=X^2(X^2-1)$. D'après le lemme de décomposition des noyaux, $E=\ker(f^2)\oplus\ker(f^2-id)$. On montre facilement que ces deux sous-espaces sont stables par $f$. Soit $x\in\ker f^2$, alors $f^2(x)=0$ : $f$ est bien nilotent d'ordre $2$ sur $\ker(f^2)$. De même sur $\ker(f^2-id)$, on a $f^2=id$ et donc $f$ est inversible. On pose donc $F=\ker(f^2-id)$ et $G=\ker(f^2)$.

		c) La restriction de $f$ à $\ker(f^2-id)$ est une symétrie. Elle est donc diagonalisable. Ainsi $f$ est diagonalisable ssi la restrition à $G=\ker f^2$ de $f$ est diagonalisable. Or sur $G$, $f$ est nilpotente : elle est diagonalisable ssi elle est nulle. Notons $g$ la restriction de $f$ à $G$. $g$ est nulle ssi $\mathrm{rg}(g)=0$. On a $\ker g=\ker(f)\cap \ker f^2$. D'après le théorème du rang appliqué à $g$, on a $\dim\ker f^2=\mathrm{rg} g+\dim\ker g=\mathrm{rg} g+\dim(\ker f\cap\ker f^2)$. Ainsi $g$ est nulle ssi $\dim(\ker f\cap\ker f^2)=\dim\ker f^2$ càd ssi $\ker f^2=\ker f$. Or d'après le théorème du rang appliqué à $f$ et $f^2$, $\dim\ker f+\mathrm{rg} f=\dim\ker f^2+\mathrm{rg} f^2$ donc la condition $\ker f^2=\ker f$ est équivalente à le condition $\mathrm{rg} f=\mathrm{rg} f^2$. Finalement, $f$ est diagonalisable ssi $\mathrm{rg} f=\mathrm{rg} f^2$.



