!!! exercice "RMS2022-1341"  
	=== "Enoncé"  
		 **Navale**  . Soient $X$ et $Y$ deux variables aléatoires à valeurs dans un ensemble fini inclus dans $\R$. On suppose que, pour tout $k\in $, $\E  (X^k)=\E  (Y^k)$. Montrer que $X$ et $Y$ suivent la même loi.

	=== "Corrige"  

		 Tout d'abord, les valeurs $E(X^k)$ et $E(Y^k)$ sont des valeurs finies.

		 Notons $x_1,\dots,x_n$ les valeurs prises par $X$ et $y_1,\dots,y_p$ les valeurs prises paar $Y$. On pose de même $\{z_1,z_2,\dots,z_q\}$ l'union des valeurs prises par $X$ et $Y$. On peut écrire (quitte à rajouter des probas nulles) :

		 $$
		 E(X^k)=\sum_{i=1}^q z_i^q P(X=z_i)\quad \text{et}\quad E(Y^k)=\sum_{i=1}^q z_i^q P(Y=z_i).
		 $$

		 Or $E(X^k)=E(Y^k)$ donc finalement, pour tout entier naturel $k$,

		$$
		\sum_{i=1}^q z_i^q(P(Y=z_i)-P(X=z_i))=0.
		$$

		 En ne gardant que les valeurs de $k$ de $0$ à $q-1$, on obtient un système dont la matrice est une matrice de Vandermonde. Or les $z_i$ sont $q$ réels distinct donc cette matrice est inversible. La seule solution au système homogène est donc la solution nulle, ce qui se traduit ici par $P(X=z_i)=P(Y=z_i)$ pour tout $i$, les variables aléatoires $X$ et $Y$ suivent donc la même loi.


