!!! exercice "RMS2022-714"
	=== "Enoncé"
		a)   Étudier la convergence et calculer la somme éventuelle de la série de terme général~$u_n$ définie par $u_0=1$ et, pour tout $n\in\mathbb N$, $u_{n+1}=\ln(e^{u_n}-u_n)$.

		b)   Étudier la convergence et calculer la somme éventuelle de la série de terme général $v_n$ définie par $v_0\in\mathbb R$ et, pour tout $n\in\mathbb N$, $v_{n+1}=e^{v_n}-1$.




	=== "Corrigé"
		On a $u_0=1$, on peut montrerque pour tout entier $n$, $u_n>0$. En effet une relation bien connue montre que pour tout $x$ réel, $e^x\ge1+x$, donc ici $e^{u_n}-u_n\ge1$ et ainsi $u_{n+1}\ge 0$.
		Ainsi, $u_n$ est une suite de réels positifs. Par ailleurs, $u_{n+1}-u_n=\ln(e^{u_n}-u_n)-u_n=\ln(1-\frac{u_n}{e^{u_n}})$. Comme dit précédemment, $\frac{u_n}{e^{u_n}}\in[0,1]$ donc $\ln(1-\frac{u_n}{e^{u_n}})<0$. On en déduit que la suite $u$ est décroissante et minorée par $0$ : elle converge. Soit $\ell$ sa limite, on a alors par continuité et unicité de la limite $\ell=\ln(e^\ell-\ell)$ ce qui équivaut à $\ln(1-\frac\ell{e^\ell})=0$ c'est à dire $\ell=0$. Ainsi $u_n\to 0$.

		On remarque alors par un calcul simple que $u_n=\ln(e^{u_0}-S_{n-1})$ où $S_n$ est la somme partielle des $u_n$. Puisque $u_n$ converge vers $0$, on en déduit que $e-S_n$ converge vers $1$. C'est à dire $S_n\to e-1$.

		Passons à la suite définie par $v_{n+1}=e^{v_n}-1$. Une relation classique montre que $e^x-1\ge x$ donc $v_{n+1}\ge v_n$ : la suite est croissante. On  peut aussi montrer que $v_n$ est de signe constant. On en déduit rapidement que si $v_0>0$ alors la suite $v_n$ ne converge pas vers $0$ et ainsi $\sum v_n$ diverge.

		Si par contre $v_0\le 0$ alors la suite est négative et croissante donc convergente. Par continuité de la fonction $x\mapsto e^x-1$, on en déduit que $v_n$ converge vers un point fixe $\ell$ tel que $\ell=e^\ell-1$ dont la seule solution est $0$. Ainsi $v_n$ tend vers $0$.

		On a alors

		$$\begin{align*}
		\frac 1{v_{n+1}}-\frac 1{v_n}&=\frac1{e^{v_n}-1}-\frac 1{v_n}\\
		&=\frac1{v_n+\frac {v_n^2}2+o(v_n^2)}-\frac 1{v_n}\\
		&=\frac 1{v_n}\left(\frac 1{1+\frac{v_n}2+o(v_n)}-1\right)\\
		&\sim -\frac 12
		\end{align*}.$$

		Ainsi la série de terme général $\frac 1{v_{n+1}}-\frac 1{v_n}$ diverge et par comaraison des sommes partielles, $\sum_{n=0}^{N-1} \frac{1}{v_{n+1}}-\frac 1{v_n}\sim -\frac N2$. On en déduit $v_n\sim -\frac 2N$. C'est le terme général d'une série à termes négatifs divergente. Donc la série des $v_n$ diverge par comparaison des séries à termes négatifs.



