!!! exercice "RMS2019-1355"  
	=== "Énoncé"  
        Pour quelles valeurs de $n \in \mathbb{N}$ l'intégrale $\int_1^{+\infty} \frac{\mathrm{d} x}{\left(x+\sqrt{x^2-1}\right)^n}$ converge-t-elle ? Donner alors sa valeur.
    === "Corrigé"  
        En $1$ l'intégrande est bien définie, il n'y a pas de problème de convergence. En $+\infty$, $\frac 1{(x+\sqrt{x^2-1})^n}\sim \frac 1{2^nx^n}$ qui est convergente en $+\infty$ ssi $n\ge 2$. 

        Ainsi l'intégrale est convrgente ssi $n\ge 2$. Plaçons nous dans ce cas. 

        On effectue le changement de variable $t=\ch(u)$ ($\d t=\sh(u)\d u$). Ce changement de variable est bien $\mathcal C^1$, croissant, bijectif de $\mathbb R^+$ dans $[1,+\infty[$. On en déduit que 

        $$
        \begin{aligned}
        I &=\int_1^{+\infty}\frac{1}{x+\sqrt{x^2-1}}\d x\\
        &=\int_0^{+\infty}\sh(u)e^{-nu}\d u\\
        &=\frac 12\int_0^{+\infty}e^{(-n+1)u}-e^{(-n-1)u}\d u\\
        &=\frac 12\left[\frac {e^{-(n-1)u}}{-n+1}-\frac{e^{-(n+1)u}}{-n-1}\right]_0^{+\infty}\\
        &=\frac 12\left(\frac 1{n-1}-\frac 1{n+1}\right)\\
        &=\frac 1{n^2-1}
        \end{aligned}
        $$ 
        
