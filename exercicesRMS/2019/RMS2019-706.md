!!! exercice "RMS2019-706"  
	=== "Énoncé"  
        Soit $f: x \mapsto \int_x^{+\infty} \frac{\sin t}{t^2} \mathrm{~d} t$
        
        a) Montrer que $f$ est définie et dérivable sur $\mathbb{R}^{+^*}$.
        
        b) Établir que $g: t \mapsto \frac{\sin t-t}{t^2}$ est bornée sur $\mathbb{R}^{+^*}$. En déduire que $f(x) \sim_{x \rightarrow 0}-\ln x$.

        c) Montrer que $f$ est intégrable sur $\mathbb{R}^{+^{\star}}$ et que $\int_0^{+\infty} f=\int_0^{+\infty} \frac{\sin u}{u} \mathrm{~d} u$.

    === "Corrigé"  
        a) La majoration $\left|\frac{\sin(t)}{t^2}\right|\le \frac 1{t^2}$ permet d'assurer la convergence absolue et donc la convergence de l'intégrale $\int_x^{+\infty}\frac{\sin(t)}{t^2}\d t$. $f$ est donc bien définie sur $\mathbb R^+_*$.

        Par ailleurs, en tant qu'intégrale fonction d'une borne, on peut assurer que $f$ est dérivable et ici $f'(x)=-\frac{\sin(x)}{x^2}$. 

        b) La fonction $g$ est continue sur $\mathbb R^+_*$, de limites finies en $0$ et en $+\infty$. On en déduit qu'elle est bornée sur $\mathbb R^+_*$. 
        On a alors : 

        $$
        \begin{aligned}
        f(x)+\ln(x) &= \int_{x}^{+\infty}\frac{\sin(t)}{t^2}\d t+\int_1^x\frac 1t\d t\\
        &=\int_{x}^1\frac{\sin(t)}{t^2}\d t+f(1)+\int_{1}^x\frac 1t\d t\\
        &=\int_x^1\frac{\sin(t)-t}{t^2}\d t+f(1)
        \end{aligned}
        $$

        Or $g$ étant borné sur $\mathbb R^+_*$, l'intégrale $\int_x^1\frac{\sin(t)-t}{t^2}\d t$ est majorée en valeur absolue, par $M$. On a alors :

        $$
        \left|\frac{f(x)}{\ln(x)}+1\right|\le \frac{M+|f(1)|}{|\ln(x)|}\to 0
        $$

        donc $f(x)\sim_0-\ln(x)$. 

        c) On déduit de ce résultat, par théorème de comparaison, que $\int_x^\pi f(t)\d t$ est convergente. Il nous reste à nous intéresser à $\int_\pi^{+\infty}f(t)\d t$. Soit $x>\pi$ et  $n$ tel que $n\pi\le x<(n+1)\pi$ (c'est à dire $n=\lfloor\frac x\pi\rfloor$). On a :

        $$
        \begin{aligned}
        f(x)&=\int_x^{+\infty}\frac{\sin(t)}{t^2}\d t\\
        &=\int_x^{(n+1)\pi}\frac{\sin(t)}{t^2}\d t+\sum_{k=n+1}^{+\infty}\int_{k\pi}^{(k+1)\pi}\frac{\sin(t)}{t^2}\d t\\
        &=\int_x^{(n+1)\pi}\frac{\sin(t)}{t^2}\d t+\sum_{k=n+1}^{+\infty}\int_{0}^\pi\frac{(-1)^k\sin(t)}{(t+k\pi)^2}\d t\\
        &=\int_x^{(n+1)\pi}\frac{\sin(t)}{t^2}\d t+\sum_{k=n+1}^{+\infty}\int_{0}^\pi\frac{(-1)^k\sin(t)}{(t+k\pi)^2}\d t\\
        \end{aligned}
        $$

        On reconnait alors sur le second membre une série alternée (à démontrer) et la somme est donc un reste de série alternée que l'on sait majorer. Par ailleurs :

        $$
        \left|\int_x^{(n+1)\pi}\frac{\sin(t)}{t^2}\d t\right|\le\int_x^{(n+1)\pi}\frac {1}{t^2}\d t\le \frac \pi{x^2}
        $$

        et ainsi :

        $$
        |f(x)|\le\frac \pi{x^2}+\left|\int_0^\pi\frac{\sin(t)}{t^2}\right|\le\frac \pi{x^2}+\frac\pi{(n+1)^2\pi^2}\le \frac {2\pi}{x^2}
        $$

        On en déduit l'intégrabilité de $f$ en $+\infty$. 