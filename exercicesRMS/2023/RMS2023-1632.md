!!! exercice "RMS2023-1632"
    === "Énoncé"
         [CCINP] Soit $M \in \mathcal{M}_{n}(\mathbb{R})$ telle que $M^{3}-4 M^{2}+4 M=0$ et $\operatorname{tr}(M)=0$.
        
        a) Montrer que les valeurs propres de $M$ sont racines de $P=X^{3}-4 X^{2}+4 X$.
        
        b) Caractériser les matrices $M$.
        

    === "Corrigé"
         