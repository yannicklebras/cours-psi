!!! exercice "RMS2023-1295"
    === "Énoncé"
         a) Montrer la convergence de $I=\int_{0}^{\frac{\pi}{2}} \ln (\sin (t)) \mathrm{d} t$.
        
        b) On pose, pour $n \geqslant 2, P_{n}=\prod_{k=1}^{n-1} \sin \left(\frac{k \pi}{2 n}\right)$.
        
        i) Montrer que $\frac{\pi}{2 n} \ln \left(P_{n}\right) \underset{n \rightarrow+\infty}{\rightarrow} I$.
        
        ii) Trouver un lien entre $P_{n}^{2}$ et $P_{2 n}$. En déduire la valeur de $I$.
        

    === "Corrigé"
         