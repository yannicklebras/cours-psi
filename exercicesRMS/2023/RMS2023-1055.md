!!! exercice "RMS2023-1055"
    === "Énoncé"
         Soit $E$ un $\mathbb{C}$-espace vectoriel de dimension finie. Soit $f$ un automorphisme de $E$ tel que pour tout $x \in E$, l'ensemble $K_{x}=\left\{f^{n}(x), n \in \mathbb{N}\right\}$ est fini. Montrer qu'il existe $k \in \mathbb{N}^{*}$ tel que $f^{k}=\mathrm{id}$. Montrer, de plus, qu'il existe un polynôme annulateur de $f$ de la forme $X^{k-p}-X^{p}$ où $p \in \mathbb{N}^{*}$.

    === "Corrigé"
         