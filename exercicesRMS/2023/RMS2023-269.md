!!! exercice "RMS2023-269"
    === "Énoncé"
         Soient $X, Y$ deux variables aléatoires entières indépendantes qui suivent la même loi.
        
        a) On suppose que $X$ suit une loi géométrique commençant à zéro, c'est-à-dire qu'il existe $p \in] 0 ; 1\left[\right.$ tel que $\forall k \in \mathbb{N}, \mathbf{P}(X=k)=(1-p)^{k} p$.
        
        Montrer que $\forall n \in \mathbb{N}, \forall k \in \llbracket 0 ; n \rrbracket, \mathbf{P}(X=k \mid X+Y=n)=\frac{1}{n+1}$.
        
        b) Prouver la réciproque.
        

    === "Corrigé"
         