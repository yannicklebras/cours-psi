!!! exercice "RMS2023-1127"
    === "Énoncé"
         Soit $f:[0,1] \rightarrow \mathbb{R}^{+}$de classe $\mathcal{C}^{1}$ telle que, pour tout $x \in[0,1],\left|f^{\prime}(x)\right| \leqslant M$.
        
        a) Montrer que, pour tout $x \in[0,1],-2 M \int_{0}^{x} f \leqslant f^{2}(x)-f^{2}(0) \leqslant 2 M \int_{0}^{x} f$.
        
        b) Montrer que $\int_{0}^{1} f-f^{2}(0) \int_{0}^{1} f \leqslant M\left(\int_{0}^{1} f\right)^{2}$.
        

    === "Corrigé"
         