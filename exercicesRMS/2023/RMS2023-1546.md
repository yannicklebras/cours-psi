!!! exercice "RMS2023-1546"
    === "Énoncé"
         [IMT] Soit $A$ dans $\mathcal{M}_{n}(\mathbb{C})$ nilpotente
        
        a) Montrer que le polynôme caractéristique de $A$ est $X^{n}$. En déduire la valeur de $\operatorname{det}\left(A+I_{n}\right)$. Soit $M$ dans $\mathcal{M}_{n}(\mathbb{C})$ telle que $M A=A M$.
        
        b) Montrer que si $M$ est inversible alors $\operatorname{det}(A+M)=\operatorname{det}(M)$.
        
        c) Montrer que, si $M$ n'est pas inversible, alors $M_{k}=M+\frac{1}{k} I_{n}$ est inversible à partir d'un certain rang. Conclure que $\operatorname{det}(A+M)=\operatorname{det}(M)$.
        

    === "Corrigé"
         