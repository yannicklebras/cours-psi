!!! exercice "RMS2023-525"
    === "Énoncé"
         Soit $G$ un groupe commutatif de cardinal $p q$ avec $p, q$ deux nombres premiers distincts. Montrer que $G$ est cyclique. Trouver un contre-exemple dans le cas où $G$ n'est pas commutatif.

    === "Corrigé"
         