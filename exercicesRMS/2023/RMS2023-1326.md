!!! exercice "RMS2023-1326"
    === "Énoncé"
         Soit $(E,\langle\rangle$,$) un espace euclidien. Soit \left(e_{1}, \ldots, e_{p}\right)$ une famille de vecteurs de $E$ telle que
        
        $\forall x \in E,\|x\|^{2}=\sum_{k=1}^{p}\left\langle x, e_{k}\right\rangle^{2}$.
        
        a) Montrer que $\left(e_{1}, \ldots, e_{p}\right)$ est une famille génératrice de $E$.
        
        b) Montrer que $\forall k \in\{1,2, \ldots, p\},\left\|e_{k}\right\| \leqslant 1$.
        c) Montrer que $\left(e_{1}, \ldots, e_{p}\right)$ est une base orthonormée de $E$ si et seulement si les $e_{k}$ sont de norme 1 .
        

    === "Corrigé"
         