!!! exercice "RMS2023-1068"
    === "Énoncé"
         Soit $n \geqslant 2$. Pour $A \in \mathcal{M}_{n}(\mathbb{R})$, on note $F(A)=\left\{\frac{X^{T} A X}{X^{T} X}, X \in \mathbb{R}^{n} \backslash\{0\}\right\}$.
        
        a) Montrer que $\mathrm{Sp}_{\mathbb{R}}(A) \subset F(A)$.
        
        b) Si $A$ est symétrique, montrer que $F(A)$ est un segment.
        
        c) Montrer que c'est encore le cas pour $A \in \mathcal{M}_{n}(\mathbb{R})$. Ind. Écrire $A$ comme somme d'une matrice symétrique et d'une matrice antisymétrique.
        

    === "Corrigé"
         