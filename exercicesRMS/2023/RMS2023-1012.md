!!! exercice "RMS2023-1012"
    === "Énoncé"
         Soient $v \in \mathcal{L}(E, G)$ et $f \in \mathcal{L}(F, G)$, où $E, F, G$ sont des $K$-ev de dimensions arbitraires
        
        a) Montrer que $\operatorname{Im} v \subset \operatorname{Im} f$ si et seulement si il existe $u \in \mathcal{L}(E, F)$ telle que $v=f \circ u$.
        
        b) Soient $f_{1}, \cdots, f_{k} \in \mathcal{L}(F, G)$.
        
        Montrer que $\operatorname{Im} v \subset \sum_{i=1}^{k} \operatorname{Im} f_{i}$ si et seulement $\operatorname{si} \exists\left(u_{1}, \ldots, u_{k}\right) \in \mathcal{L}(F, G)^{k}, v=\sum_{i=1}^{k} f_{i} \circ u_{i}$.
        

    === "Corrigé"
         