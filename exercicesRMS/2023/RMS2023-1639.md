!!! exercice "RMS2023-1639"
    === "Énoncé"
         [CCINP] Soient $a, b, c$ trois réels distincts et soit $E=\mathbb{R}_{2}[X]$. Pour tout $(P, Q) \in E^{2}$, on pose $\langle P, Q\rangle=P(a) Q(a)+P(b) Q(b)+P(c) Q(c)$.
        
        a) Montrer que $\langle$,$\rangle est un produit scalaire sur E$.
        
        b) On pose $F=\{P \in E, P(a)+P(b)+P(c)=0\}$. Donner l'orthogonal de $F$ et calculer la distance de $X^{2}$ à $F$.
        

    === "Corrigé"
         