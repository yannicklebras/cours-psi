!!! exercice "RMS2023-1680"
    === "Énoncé"
         [IMT] Pour $n \in \mathbb{N}^{*}$, on pose $u_{n}=\int_{0}^{+\infty} \frac{\mathrm{d} t}{\left(1+t^{3}\right)^{n}}$.
        
        a) Montrer que $u_{n}$ est bien définie.
        
        b) Montrer que la suite $\left(u_{n}\right)$ converge et calculer sa limite.
        
        c) Montrer que $\sum(-1)^{n} u_{n}$ converge.
        
        d) Trouver une relation de récurrence entre $u_{n+1}$ et $u_{n}$.
        

    === "Corrigé"
         