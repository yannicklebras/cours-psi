!!! exercice "RMS2023-1370"
    === "Énoncé"
         Soit $f: x \mapsto \int_{0}^{+\infty} \frac{\sin ^{2}(x t)}{t^{2}} e^{-t} \mathrm{~d} t$. Préciser le domaine de définition de $f$. Donner un équivalent de $f$ en 0 et en $+\infty$.

    === "Corrigé"
         