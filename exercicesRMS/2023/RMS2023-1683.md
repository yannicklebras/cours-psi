!!! exercice "RMS2023-1683"
    === "Énoncé"
         [CCINP] a) Calculer $\lim _{t \rightarrow 0^{+}} \frac{1}{t} e^{-\frac{1}{t}}$.
        
        b) Montrer l'existence de $h(x)=\int_{0}^{x} \frac{1}{t} e^{-\frac{1}{t}} \mathrm{~d} t$.
        
        c) Décrire l'ensemble des solutions de l'équation différentielle $(E): x^{2} y^{\prime}+y=x$.
        d) Montrer que $e^{\frac{1}{x}} h(x)=x \int_{0}^{+\infty} \frac{e^{-u}}{1+x u} \mathrm{~d} u$.
        
        e) Montrer que $g: x \mapsto x \int_{0}^{+\infty} \frac{e^{-u}}{1+x u} \mathrm{~d} u$ est l'unique solution de classe $\mathcal{C}^{\infty}$ de $(E)$.
        
        f) Préciser la limite en $+\infty$ de $g$.
        

    === "Corrigé"
         