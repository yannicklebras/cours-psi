!!! exercice "RMS2023-1292"
    === "Énoncé"
         Soit $\left(a_{i}\right)_{i \in \mathbb{N}} \in \mathbb{R}^{\mathbb{N}}$ et $f: \mathbb{R} \rightarrow \mathbb{R}^{+*}$ telle que
        
        $\forall k \in \mathbb{N}, f(x) \underset{x \rightarrow+\infty}{=} a_{0}+\frac{a_{1}}{x}+\cdots+\frac{a_{k}}{x_{k}}+o\left(\frac{1}{x^{k}}\right)$.
        
        a) Donner une condition nécessaire et suffisante de convergence de $\sum f(n)$.
        b) Donner une condition nécessaire et suffisante de convergence de la suite $p_{n}=\prod_{k=0}^{n} f(k)$.
        
        c) Donner une condition nécessaire et suffisante de convergence de $\sum p_{n}$.
        

    === "Corrigé"
         