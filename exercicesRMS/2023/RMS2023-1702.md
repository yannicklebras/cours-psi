!!! exercice "RMS2023-1702"
    === "Énoncé"
        [Navale] Soit $c \in \mathbb{N}^{*}$. On dispose d'une urne dans laquelle on a initialement une boule blanche et une boule noire. Lorsque l'on tire une boule, on ajoute $c$ boules de la même couleur
        que celle tirée. On note $X_{n}$ la variable de Bernoulli qui vaut 1 si on tire une boule blanche et 0 si on tire une boule noire, lors du $n$ ième tirage. On note $S_{n}=X_{1}+\cdots+X_{n}$.
        
        a) Montrer que $X_{1}$ suit une loi de Bernoulli de paramètre $p=\frac{1}{2}$.
        
        b) Montrer que $\mathbf{P}_{\left(X_{1}=1\right)}\left(X_{2}=1\right)=\frac{c+1}{c+2}$.
        
        c) Montrer que $X_{2}$ suit une loi de Bernoulli.
        
        d) Montrer que, pour $k \in \llbracket 1, n \rrbracket, \mathbf{P}_{S_{n}=k}\left(X_{n+1}=1\right)=\frac{1+c k}{2+n c}$.
        
        e) Montrer que, pour tout $n \in \mathbb{N}^{*}, X_{n}$ suit la loi de Bernoulli de paramètre $1 / 2$.
        

    === "Corrigé"
        a) Au début on a deux boules de couleurs différentes et le tirage est donc uniforme sur les deux couleurs.

        b) Si $X_1=1$ alors on a maintenant dans l'urne $1$ boule noire et $c+1$ boules blanches et donc $c+2$ boules au total. La probabilité de tirer une boule blanche est donc de $\frac{c+1}{c+2}$. 

        c) $X_2$ a deux issues : $0$ ou $1$. $X_2$ suit donc une loi de Bernoulli. On a $P(X_2=1) = P(X_2=1|X_1=1)P(X_1=1)+P(X_2=1|X_1=0)P(X_1=0)=\frac{c+1}{c+2}\frac 12+\frac{1}{c+2}\frac 12=\frac 12$. Donc son paramètre est $\frac 12$. 

        d) Si $S_n=k$ alors on a tiré $k$ fois une boule blanche et $n-k$ fois une boule noire. On a donc rajouté $kc$ boules blanches et $(n-k)c$ boules noires : il y a au total $2+nc$ boules dans l'urne donc $1+kc$ boules blanches. Ainsi la probabilité de tirer une boule blanche est $\frac{1+kc}{2+nc}$. 

        e) On fait une démonstration par récurrence. On ne s'intéresse ici qu'à l'hérédité. D'après la formule des probabilités totales, on a :

        $$
        \begin{aligned}
        P(X_{n+1}=1)&= \sum_{k=0}^nP(X_{n+1}=1|S_n=k)P(S_n=k)\\
        &=\frac 1{2+cn}\sum_{k=0}^n(1+ck)P(S_n=k)\\
        &=\frac 1{2+cn}\left(\sum_{k=0}^nP(S_n=k)+c\sum_{k=0}^nkP(S_n=k)\right)\\
        &=\frac 1{2+cn}\left(1+cE(S_n)\right)
        \end{aligned}
        $$

        Or $S_n$ est une somme de $n$ variables de Bernoulli d'espérance $p=\frac 12$ et par linéarité : $E(S_n)=np=\frac n2$. On en déduit $P(X_{n+1}=1)=\frac 1{2+cn}(1+c\frac n2)=\frac 12$. Ainsi $X_{n+1}$ suit bien une loi de Bernoulli de paramètre $\frac 12$. 