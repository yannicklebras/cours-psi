!!! exercice "RMS2023-476"
    === "Énoncé"
         ${ }^{\star}$ Soit $\left(u_{n}\right)$ une suite bornée. Montrer qu'il y a équivalence entre :
        
        (i) $\frac{1}{n} \sum_{k<n}\left|u_{k}\right| \rightarrow 0$
        
        (ii) il existe $A \subset \mathbb{N}$ tel que $\frac{1}{n}|A \cap[0, n-1]| \underset{n \rightarrow+\infty}{\longrightarrow} 0$ et $\lim _{n \notin A} u_{n}=0$.
        

    === "Corrigé"
         