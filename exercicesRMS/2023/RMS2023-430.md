!!! exercice "RMS2023-430"
    === "Énoncé"
         a) Déterminer tous les polynômes $P \in \mathbb{C}[X]$ tels que, pour tout réel $\theta \in \mathbb{R}$, on ait $\left|P\left(e^{i \theta}\right)\right|=1$.
        
        b) Déterminer toutes les fractions rationnelles $F \in \mathbb{C}(X)$ telles que, pour tout réel $\theta \in \mathbb{R}$, on ait $\left|F\left(e^{i \theta}\right)\right|=1$.
        

    === "Corrigé"
         