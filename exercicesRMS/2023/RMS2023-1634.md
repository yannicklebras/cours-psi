!!! exercice "RMS2023-1634"
    === "Énoncé"
         [CCINP] Soit $A \in \mathcal{M}_{n}(\mathbb{R})$ non nulle. Soit $t: M \in \mathcal{M}_{n}(\mathbb{R}) \mapsto(\operatorname{tr} M) A-M \in$ $\mathcal{M}_{n}(\mathbb{R})$. L'application $t$ est clairement linéaire. Soit $P=X^{2}-(\operatorname{tr} A-2) X+1-\operatorname{tr} A$.
        
        a) Montrer que $P$ est annulateur de $t$.
        
        b) À quelle condition sur la trace de $A$ l'endomorphisme $t$ est-il diagonalisable?
        

    === "Corrigé"
         