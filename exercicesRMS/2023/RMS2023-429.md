!!! exercice "RMS2023-429"
    === "Énoncé"
         Soient $a_{1}<\cdots<a_{n}$ des réels et $\lambda_{1}, \ldots, \lambda_{n}$ des réels. Montrer que la fraction rationnelle $R=\sum_{k=1}^{n} \frac{\lambda_{k}}{X-a_{k}}$ admet une primitive qui est une fraction rationnelle si et seulement si tous les $\lambda_{k}$ sont nuls.

    === "Corrigé"
         