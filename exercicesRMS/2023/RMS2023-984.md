!!! exercice "RMS2023-984"
    === "Énoncé"
         Soit $X$ une variable aléatoire de loi $\mathcal{P}(\lambda)$.
        
        a) Montrer que, pour tout $n \in \mathbb{N}, \mathbf{P}(X \leqslant n)=\frac{1}{n !} \int_{\lambda}^{+\infty} t^{n} e^{-t} \mathrm{~d} t$.
        
        b) Donner un équivalent de $\int_{\lambda}^{+\infty} t^{n} e^{-t} \mathrm{~d} t$ quand $n \rightarrow \infty$.
        
        c) Grâce à la fonction génératrice, calculer la probabilité que $X$ soit pair.
        
        d) Soit $Y$ une variable aléatoire suivant la loi uniforme sur $\{1,2\}$, indépendante de $X$. Calculer la probabilité que $X Y$ soit paire.
        

    === "Corrigé"
         