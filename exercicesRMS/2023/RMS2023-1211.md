!!! exercice "RMS2023-1211"
    === "Énoncé"
         Soit $(G, \cdot)$ un groupe fini commutatif tel que le nombre d'automorphismes de $G$ est 3 . a) $i$ ) Donner la définition d'un automorphisme. Montrer que $\varphi: x \mapsto x^{-1}$ est un automorphisme de $G$.
        
        ii) Montrer que, pour tout $x \in G, x^{2}=e$.
        
        b) Montrer que $G$ possède un sous-groupe $V$ d'ordre 4 et préciser les automorphismes de $V$.
        
        ## Centrale - PSI
        
        ## Algèbre
        

    === "Corrigé"
         