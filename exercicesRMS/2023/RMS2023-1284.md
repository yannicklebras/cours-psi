!!! exercice "RMS2023-1284"
    === "Énoncé"
         Soit $A \in \mathcal{M}_{2}(\mathbb{Z})$.
        
        a) On suppose $A$ inversible. Montrer que $A^{-1} \in \mathcal{M}_{2}(\mathbb{Z})$ si et seulement si $\operatorname{det}(A) \in$ $\{-1,1\}$.
        
        b) On suppose qu'il existe $p \in \mathbb{N}^{*}$ tel que $A^{p}=I_{2}$. Montrer que $A$ est inversible, et que $A^{-1}$ est à coefficients entiers. Montrer qu'il n'existe qu'un nombre fini de polynômes caractéristiques possibles pour $A$.
        

    === "Corrigé"
         