!!! exercice "RMS2023-1125"
    === "Énoncé"
         Soit $z \in \mathbb{C}$ tel que $|z| \neq 1$. On pose $I=\int_{0}^{2 \pi} \frac{\mathrm{d} t}{z-e^{i t}}$.
        
        a) Calculer $I$ en utilisant la décomposition en éléments simples de $\frac{n X^{n-1}}{X^{n}-1}$.
        
        b) Calculer $I$ en utilisant une série de fonctions.
        

    === "Corrigé"
         