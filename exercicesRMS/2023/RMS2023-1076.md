!!! exercice "RMS2023-1076"
    === "Énoncé"
         Soient $p, q \in] 1,+\infty\left[\right.$ tels que $\frac{1}{p}+\frac{1}{q}=1$. Pour $x=\left(x_{1}, \ldots, x_{n}\right)$ et $y=\left(y_{1}, \ldots, y_{n}\right)$
        
        dans $\mathbb{R}^{n}$ on pose $\|x\|_{p}=\left(\sum_{k=1}^{n}\left|x_{k}\right|^{p}\right)^{1 / p},\|y\|_{p}=\left(\sum_{k=1}^{n}\left|y_{k}\right|^{q}\right)^{1 / q}$ et $\langle x, y\rangle=\sum_{k=1}^{p} x_{k} y_{k}$.
        
        a) On admet que $\forall(x, y) \in\left(\mathbb{R}^{n}\right)^{2},|\langle x, y\rangle| \leqslant\|x\|_{p}\|y\|_{q}$.
        
        Montrer que $\|x\|_{p}=\sup \left\{\langle x, y\rangle ; y \in \mathbb{R}^{n},\|y\|_{q}=1\right\}$.
        
        b) Montrer que \|\|$_{p}$ est une norme.
        
        c) Si \|\| est une norme euclidienne sur un $\mathbb{R}$-espace vectoriel $E$, montrer que, pour tout $(x, y) \in E^{2},\|x+y\|^{2}+\|x-y\|^{2}=2\left(\|x\|^{2}+\|y\|^{2}\right)$.
        
        d) Trouver une inclusion entre les boules unités de $\left(\mathbb{R}^{n},\|\|_{p}\right)$ et $\left(\mathbb{R}^{n},\|\|_{p^{\prime}}\right)$ avec $1 \leqslant p<p^{\prime}$.
        

    === "Corrigé"
         