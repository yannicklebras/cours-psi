!!! exercice "RMS2023-1143"
    === "Énoncé"
         Soit $f: x \mapsto \sum_{n \in \mathbb{Z}}(x+n)^{-2}$.
        
        a) Montrer que $f$ est bien définie sur $\mathbb{R} \backslash \mathbb{Z}$, qu'elle est 1-périodique et qu'elle vérifie $\forall x \in \mathbb{R} \backslash \mathbb{Z}, f\left(\frac{x}{2}\right)+f\left(\frac{1-x}{2}\right)=4 f(x)$.
        b) Soit $\phi: \mathbb{R} \rightarrow \mathbb{R}$ continue, 1-périodique et vérifiant $\forall x \in \mathbb{R}, \phi\left(\frac{x}{2}\right)+\phi\left(\frac{1-x}{2}\right)=4 \phi(x)$. Montrer que $\phi=0$.
        
        c) En déduire que $\forall x \in \mathbb{R} \backslash \mathbb{Z}, f(x)=\frac{\pi^{2}}{\sin (\pi x)^{2}}$.
        

    === "Corrigé"
         