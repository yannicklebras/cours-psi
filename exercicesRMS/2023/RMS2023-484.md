!!! exercice "RMS2023-484"
    === "Énoncé"
         a) Soit $f: \mathbb{R}^{+*} \rightarrow \mathbb{R}$ une fonction $C^{\infty}$. Montrer que pour tout $n>0$ et pour tout $x>0$ il existe $c \in] x, x+n\left[\right.$ tel que $\sum_{k=0}^{n}\left(\begin{array}{l}k \\ n\end{array}\right)(-1)^{n-k} f(x+k)=f^{(n)}(c)$.
        
        b) Soit $\lambda>0$ tel que $n^{\lambda} \in \mathbb{N}$ pour tout $n$. Montrer que $\lambda \in \mathbb{N}$.
        

    === "Corrigé"
         