!!! exercice "RMS2023-1044"
    === "Énoncé"
         Soient $A \in \mathcal{M}_{n}(\mathbb{C})$ et $\varphi: M \in \mathcal{M}_{n}(\mathbb{C}) \mapsto \operatorname{tr}(A) M-\operatorname{tr}(M) A$. On suppose que $\operatorname{tr}(A) \neq 0$.
        
        a) Déterminer le noyau et l'image de $\varphi$.
        
        b) En déduire les éléments propres de $\varphi$.
        
        c) Calculer $\operatorname{tr}(\varphi)$, $\operatorname{det}(\varphi)$ et le polynôme caractéristique de $\varphi$.
        
        d) On pose $\Psi: M \in \mathcal{M}_{n}(\mathbb{C}) \mapsto \Psi(M)=\operatorname{tr}(A) M-\operatorname{tr}(M) A$. Déterminer les éléments propres de $\Psi$.
        
        e) Montrer que $\Psi$ est bijective et calculer son inverse.
        

    === "Corrigé"
         