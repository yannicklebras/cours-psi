!!! exercice "RMS2023-271"
    === "Énoncé"
         ${ }^{\star} \star$ Soit $\left(X_{n}\right)_{n \in \mathbb{N}^{*}}$ une suite i.i.d. de variables aléatoires à valeurs dans $\mathbb{N}$. On suppose que $\mathbb{P}\left(X_{1}=0\right) \mathbb{P}\left(X_{1}=1\right) \neq 0$. On pose, pour $n \in \mathbb{N}, S_{n}=X_{1}+\cdots+X_{n}$. Montrer que $\mathbf{P}\left(4\right.$ divise $\left.S_{n}\right) \underset{n \rightarrow+\infty}{\longrightarrow} \frac{1}{4}$.

    === "Corrigé"
         