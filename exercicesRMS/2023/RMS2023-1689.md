!!! exercice "RMS2023-1689"
    === "Énoncé"
         [CCINP] On pose, pour $n \in \mathbb{N}, I_{n}=\int_{1}^{e} t(\ln (t))^{n} \mathrm{~d} t$ et $f: x \mapsto \sum_{n=0}^{+\infty} I_{n} x^{n}$.
        
        a) Montrer que $I_{n}$ existe pour tout $n \in \mathbb{N}$. Calculer $I_{0}$ et $I_{1}$.
        
        b) i) Étudier les variations de $\left(I_{n}\right)$.
        
        ii) Montrer, pour $n \in \mathbb{N}$, la relation $2 I_{n+1}=e^{2}-(n+1) I_{n}$.
        
        c) $i$ ) Montrer que, pour $n \in \mathbb{N}, \frac{e^{2}}{n+3} \leqslant I_{n} \leqslant \frac{e^{2}}{n+2}$. Déterminer un équivalent de $I_{n}$.
        
        ii) Déterminer le rayon de convergence de $\sum I_{n} x^{n}$.
        
        d) Donner le domaine de définition de $f$.
        
        e) Montrer que $\forall x \in]-1,1\left[, f(x)=\int_{1}^{e} \frac{t}{1-(x \ln (t))} \mathrm{d} t\right.$.
        

    === "Corrigé"
         