!!! exercice "RMS2023-436"
    === "Énoncé"
         Soit $n \geqslant 2$. Si $A \in \mathcal{M}_{n}(\mathbb{C})$ est nilpotente, déterminer les valeurs possibles du cardinal de l'ensemble $\left\{B \in \mathcal{M}_{n}(\mathbb{C}), A=B^{2}\right\}$.

    === "Corrigé"
         