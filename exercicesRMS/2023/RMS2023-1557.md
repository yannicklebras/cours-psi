!!! exercice "RMS2023-1557"
    === "Énoncé"
         [CCINP] Soit $A \in \mathcal{M}_{n}(\mathbb{R})$ non nulle vérifiant $A^{3}+9 A=0$.
        
        a) Montrer que le spectre complexe de $A$ est inclus dans $\{0,3 i,-3 i\}$.
        
        b) La matrice $A$ est-elle diagonalisable dans $\mathcal{M}_{n}(\mathbb{C})$ ? dans $\mathcal{M}_{n}(\mathbb{R})$ ?
        
        c) Montrer que si $n$ est impair alors $A$ n'est pas inversible.
        
        d) Montrer que $A$ ne peut pas être une matrice symétrique.
        

    === "Corrigé"
         