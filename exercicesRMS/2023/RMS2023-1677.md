!!! exercice "RMS2023-1677"
    === "Énoncé"
         [CCINP] Pour $n \in \mathbb{N}^{*}$ et $x \in \mathbb{R}^{+*}$, on pose $f_{n}(x)=\frac{\sin (n x)}{n x+x \sqrt{x}}$.
        
        a) Montrer que $f_{n}$ est intégrable sur $\mathbb{R}^{+*}$. On pose $u_{n}=\int_{0}^{+\infty} f_{n}(x) \mathrm{d} x$.
        
        b) Déterminer la limite de $\left(u_{n}\right)$.
        

    === "Corrigé"
         