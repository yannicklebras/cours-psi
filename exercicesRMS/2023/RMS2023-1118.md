!!! exercice "RMS2023-1118"
    === "Énoncé"
         a) Trouver les fonctions $f: \mathbb{R} \rightarrow \mathbb{R}$ dérivables en 0 telles que $\forall x \in \mathbb{R}, f(2 x)=2 f(x)$.
        
        b) Trouver les fonctions $f: \mathbb{R} \rightarrow]-1 ; 1[$ dérivables en 0 telles que $\forall x \in \mathbb{R}, f(2 x)=\frac{2 f(x)}{1+f(x)^{2}}$.
        

    === "Corrigé"
         