!!! exercice "RMS2023-1603"
    === "Énoncé"
         [IMT] Soit $n \in \mathbb{N}^{*}$. On considère un graphe aléatoire non orienté à $n$ sommets notés $A_{1}, \ldots, A_{n}$. La probabilité que les sommets $A_{i}$ et $A_{j}$, pour $i \neq j$, soient reliés est égale à $\left.p_{n} \in\right] 0,1\left[\right.$, et cela de façon indépendante. On note $X_{i}$ la variable aléatoire égale à $1 \mathrm{si}$ le sommet $A_{i}$ est isolé, c'est-à-dire s'il n'est relié à aucun autre sommet. On pose $S_{n}=$ $X_{1}+\cdots+X_{n}$.
        
        a) Donner la loi de $X_{1}$. En déduire $\mathbf{E}\left(S_{n}\right)$.
        
        b) Donner une majoration de la probabilité d'avoir au moins un sommet isolé.
        
        c) On suppose que, pour $n \geqslant 2, p_{n}=C \frac{\ln (n)}{n}$ avec $C>1$. Montrer : $\mathbf{P}\left(S_{n}=0\right) \underset{n \rightarrow+\infty}{\longrightarrow} 0$.
        

    === "Corrigé"
         