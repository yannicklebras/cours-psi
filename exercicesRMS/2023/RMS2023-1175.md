!!! exercice "RMS2023-1175"
    === "Énoncé"
         Soient $n \in \mathbb{N}^{*}$ et $x \in\left[0, \pi / 2\left[\right.\right.$. On pose $I_{n}(x)=\int_{0}^{x} \frac{\cos (n t)}{(\cos t)^{n}} \mathrm{~d} t$.
        
        a) Trouver une relation entre $I_{n}(x)$ et $I_{n-1}(x)$.
        
        b) Montrer que $\frac{I_{n}(x)}{2^{n}}=\frac{x}{2}-\frac{1}{2} \sum_{k=1}^{n-1} \frac{\sin (k x)}{k(2 \cos x)^{k}}$.
        
        c) En déduire que $\forall x \in[0, \pi / 3], x=\sum_{k=1}^{+\infty} \frac{\sin (k x)}{k(2 \cos x)^{k}}$.
        

    === "Corrigé"
         