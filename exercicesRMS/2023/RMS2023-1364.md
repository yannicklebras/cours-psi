!!! exercice "RMS2023-1364"
    === "Énoncé"
         On donne la valeur $\int_{0}^{+\infty} \frac{e^{-t}}{\sqrt{t}} \mathrm{~d} t=\sqrt{\pi}$.
        
        a) Prouver la convergence et donner la valeur de $\int_{0}^{+\infty} \frac{1-e^{-t}}{t^{2}} \mathrm{~d} t$.
        
        b) Pour $n \in \mathbb{N}^{*}$, on pose $u_{n}=\int_{0}^{+\infty} \frac{1-\cos ^{n} t}{t^{3 / 2}} \mathrm{~d} t$ et $v_{n}=\int_{0}^{+\infty} \frac{1-\cos ^{n}\left(\sqrt{\frac{2 t}{n}}\right)}{t^{3 / 2}} \mathrm{~d} t$.
        
        c) Établir la convergence des intégrales $u_{n}$ et $v_{n}$.
        
        d) Déterminer un équivalent de $u_{n}$ lorsque $n \rightarrow+\infty$.
        

    === "Corrigé"
         