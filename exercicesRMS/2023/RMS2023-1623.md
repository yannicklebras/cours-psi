!!! exercice "RMS2023-1623"
    === "Énoncé"
         [CCINP] Soit $A=\left(\begin{array}{ll}5 & 1 \\ 3 & 3\end{array}\right)$. On cherche à résoudre l'équation $M^{2}+M=A$, d'inconnue $M \in \mathcal{M}_{2}(\mathbb{R})$.
        
        a) Résoudre dans $\mathbb{R}$ les équations $x^{2}+x-2=0$ et $x^{2}-x-6=0$.
        
        b) Déterminer les valeurs propres de $A$ et les sous-espaces propres associés. La matrice $A$ est-elle diagonalisable?
        
        Soit $M \in \mathcal{M}_{2}(\mathbb{R})$ telle que $M^{2}+M=A$.
        
        c) Soit $X \in \mathcal{M}_{2,1}(\mathbb{R})$ un vecteur propre de $M$ associé à la valeur propre $\lambda$. Montrer que $X$ est un vecteur propre de $A$ et que $\lambda \in\{-3,-2,1,3\}$.
        
        d) Montrer que $A$ et $M$ commutent. En déduire que tout vecteur propre de $A$ est un vecteur propre de $M$.
        
        e) Montrer que $M$ n'a que des valeurs propres simples (on pourra raisonner par l'absurde) et en déduire que $M$ est diagonalisable.
        
        f) Résoudre l'équation $M^{2}+M=A$, d'inconnue $M \in \mathcal{M}_{2}(\mathbb{R})$.
        

    === "Corrigé"
         