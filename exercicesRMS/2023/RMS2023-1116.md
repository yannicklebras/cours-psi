!!! exercice "RMS2023-1116"
    === "Énoncé"
         Soient $\alpha, \beta, \gamma$ des réels positifs avec $\alpha+\beta+\gamma=\frac{\pi}{2}$. Montrer que $\sin \alpha \sin \beta \sin \gamma \leqslant \frac{1}{8}$.

    === "Corrigé"
         