!!! exercice "RMS2023-499"
    === "Énoncé"
         Soit $F: x \mapsto \sum_{k=0}^{+\infty}(-1)^{k} x^{2^{k}}$.
        
        a) Déterminer le domaine de définition de $F$.
        
        b) Trouver une relation entre $F(x)$ et $F\left(x^{2}\right)$.
        
        On pose $G: x \mapsto \sum_{k=0}^{+\infty} x^{4^{k}}\left(1-x^{4^{k}}\right)$.
        
        c) Montrer que $G(x)$ converge pour tout $x \in] 0,1[$.
        
        d) Trouver une relation entre $F$ et $G$.
        

    === "Corrigé"
         