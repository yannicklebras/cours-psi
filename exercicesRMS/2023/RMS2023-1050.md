!!! exercice "RMS2023-1050"
    === "Énoncé"
         Soit $M \in \mathcal{M}_{n}(\mathbb{R})$ telle que $M^{2}+M+I_{n}=0$.
        
        a) La matrice $M$ est-elle diagonalisable dans $\mathcal{M}_{n}(\mathbb{R})$ ? et dans $\mathcal{M}_{n}(\mathbb{C})$ ?
        
        b) Calculer $\operatorname{Tr}(M)$, $\operatorname{det}(M)$ puis $\chi_{M}$.
        
        c) La matrice $M^{2}$ est-elle diagonalisable dans $\mathcal{M}_{n}(\mathbb{R})$ ? Quelle est sa trace?
        

    === "Corrigé"
         