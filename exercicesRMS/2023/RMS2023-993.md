!!! exercice "RMS2023-993"
    === "Énoncé"
         Soient $\alpha_{1}, \ldots, \alpha_{n} \in \mathbb{R}$ avec $\alpha_{1}<\alpha_{2}<\ldots<\alpha_{n}$ et $f: x \mapsto \prod_{i=1}^{n}\left(x-\alpha_{i}\right)$.
        
        a) Montrer que $f^{\prime}$ possède $n-1$ racines distinctes $\beta_{1}, \ldots, \beta_{n-1}$ avec $\alpha_{1}<\beta_{1}<\alpha_{2}<\cdots<\beta_{n-1}<\alpha_{n}$.
        
        b) Exprimer $\frac{f^{\prime}(x)}{f(x)}$ en fonction de $\alpha_{1}, \ldots, \alpha_{n}$, et $\frac{f^{\prime \prime}(x)}{f^{\prime}(x)}$ en fonction de $\beta_{1}, \ldots, \beta_{n-1}$.
        
        c) Montrer que $\sum_{i=1}^{n} \frac{f^{\prime \prime}\left(\alpha_{k}\right)}{f^{\prime}\left(\alpha_{k}\right)}=0$.
        
        d) Soit $\alpha \in \mathbb{R}^{*}$. Montrer que $f^{\prime}+\alpha f$ possède $n-1$ racines distinctes. Tracer $g=\frac{f^{\prime}}{f}$. En déduire graphiquement que $f^{\prime}+\alpha f$ possède exactement $n$ racines.
        

    === "Corrigé"
         