!!! exercice "RMS2023-235"
    === "Énoncé"
         On considère la suite $\left(F_{n}\right)_{n \geqslant 0}$ définie par $F_{0}=0, F_{1}=1$ puis $F_{n+2}=F_{n}+F_{n+1}$ pour tout $n \in \mathbb{N}$.
        
        Montrer que tout entier $N \in \mathbb{N}^{*}$ s'écrit de manière unique $N=F_{p_{1}}+F_{p_{2}}+\cdots+F_{p_{m}}$ avec des entiers $p_{i}$ tels que $p_{i+1}-p_{i} \geqslant 2$ pour tout $i \in \llbracket 1 ; m-1 \rrbracket$ et $p_{1} \geqslant 2$. Prouver l'unicité de cette écriture.
        

    === "Corrigé"
         