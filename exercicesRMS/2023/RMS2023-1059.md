!!! exercice "RMS2023-1059"
    === "Énoncé"
         a) Montrer que l'application $(P, Q) \in \mathbb{R}[X] \mapsto \int_{0}^{+\infty} \frac{P(t) Q(t)}{\operatorname{ch} t} \mathrm{~d} t$ définit un produit scalaire.
        
        b) On considère l'orthonormalisée $\left(P_{0}, \ldots, P_{n}\right)$ de la base canonique $\left(1, \ldots, X^{n}\right)$. Calculer $P_{0}$ et montrer que, pour tout $k, P_{k}$ est scindé à racines simples toutes positives.
        

    === "Corrigé"
         