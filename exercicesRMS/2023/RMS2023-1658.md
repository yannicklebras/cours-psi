!!! exercice "RMS2023-1658"
    === "Énoncé"
         [CCINP] Soit $f \in \mathcal{C}^{1}\left(\mathbb{R}^{+}, \mathbb{R}\right)$ telle que $x f^{\prime}(x) \underset{x \rightarrow+\infty}{\longrightarrow} 1$. Montrer qu'il existe $a \in \mathbb{R}^{+}$ tel que $\forall x \geqslant a, f^{\prime}(x) \geqslant \frac{1}{2 x}$. En déduire que $f(x) \underset{x \rightarrow+\infty}{\longrightarrow}+\infty$.

    === "Corrigé"
         