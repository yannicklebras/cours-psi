!!! exercice "RMS2023-1631"
    === "Énoncé"
         [Navale] On considère les deux matrices $D=\left(\begin{array}{cc}-1 & 0 \\ 0 & 4\end{array}\right)$ et $A=\left(\begin{array}{cc}-1 & 0 \\ 10 & 4\end{array}\right)$.
        
        a) Quelles sont les racines réelles de $X^{3}-2 X+1$ et $X^{3}-2 X-4$ ?
        
        b) Trouver toutes les matrices de $\mathcal{M}_{2}(\mathbb{R})$ qui commutent avec $D$.
        c) Résoudre $M^{3}-2 M=D$ dans $\mathcal{M}_{2}(\mathbb{R})$.
        
        d) Résoudre $M^{3}-2 M=A$ dans $\mathcal{M}_{2}(\mathbb{R})$.
        

    === "Corrigé"
         