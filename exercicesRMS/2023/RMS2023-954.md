!!! exercice "RMS2023-954"
    === "Énoncé"
         On pose $S_{n}=\sum_{k=1}^{n}(-1)^{k+1} \sqrt{k}$.
        
        a) Montrer que $S_{n} \underset{n \rightarrow+\infty}{\sim} \frac{(-1)^{n+1}}{2} \sqrt{n}$.
        
        b) Montrer que $\left(S_{n}+S_{n+1}\right)$ admet une limite finie et que celle-ci est positive.
        

    === "Corrigé"
         