!!! exercice "RMS2023-510"
    === "Énoncé"
         Déterminer les extrema de $f:(x, y) \mapsto 3 x^{2}+2 x y+2 y^{2}-x^{4}$ sur le disque unité fermé et les points en lesquels ils sont atteints.
        
        ## Probabilités
        

    === "Corrigé"
         