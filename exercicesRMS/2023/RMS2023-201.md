!!! exercice "RMS2023-201"
    === "Énoncé"
         Soient $(E,\langle\rangle$,$) un plan euclidien, \mathcal{V}=\left(v_{1}, \ldots, v_{n}\right)$ une famille de vecteurs de $E$ de norme 1 telle que $\left\langle v_{1}, v_{2}\right\rangle=\left\langle v_{2}, v_{3}\right\rangle=\ldots=\left\langle v_{n}, v_{1}\right\rangle$. Soit $\mathbb{D}_{2 n}$ l'ensemble des isométries vectorielles de $E$ qui laissent invariantes la famille $\mathcal{V}$, c'est-à-dire :
        
        $\mathbb{D}_{2 n}=\left\{\sigma \in \mathcal{O}(E) ; \forall i \in \llbracket 1, n \rrbracket, \sigma\left(v_{i}\right) \in \mathcal{V}\right\}$.
        
        a) Trouver, pour $1 \leqslant i<j \leqslant n$, la valeur de l'angle $\left\langle v_{i}, v_{j}\right\rangle$.
        
        b) Montrer que $\mathbb{D}_{2 n}$ est un sous-ensemble finie de $\mathcal{O}(E)$.
        
        c) Montrer que $\mathbb{D}_{2 n}$ est stable par composition et passage à l'inverse.
        
        d) Exprimer $\mathbb{D}_{6}$ et $\mathbb{D}_{8}$.
        
        e) Si $\sigma \in \mathbb{D}_{2 n}$ vérifie $\sigma\left(v_{1}\right)=v_{i}$, montrer que $\sigma\left(v_{2}\right)=v_{i-1}$ ou $\sigma\left(v_{2}\right)=v_{i+1}$.
        
        f) En déduire que le cardinal de $\mathbb{D}_{2 n}$ est $2 n$.
        
        g) Montrer que $\mathbb{D}_{2 n}=\left\{\mathrm{id}, r, s r, r^{2}, s r^{2}, r^{3}, s r^{3}, \ldots\right\}$ où $s$ est une réflexion et $r$ une rotation d'angle $\operatorname{Arccos}\left(\left\langle v_{1}, v_{2}\right\rangle\right)$.
        
        h) On note $D=\bigcup_{n \geqslant 3} D_{2 n}$. Montrer que pour tout $\sigma \in \mathcal{O}(E)$, il existe une suite $\left(\sigma_{k}\right)_{k \geqslant 0} \in$ $D^{\mathbb{N}}$ telle que $\sigma=\lim _{k \rightarrow+\infty} \sigma_{k}$.
        

    === "Corrigé"
         