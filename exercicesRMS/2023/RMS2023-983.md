!!! exercice "RMS2023-983"
    === "Énoncé"
         Soit $\mathcal{A}$ un ensemble de parties de $\llbracket 1, n \rrbracket$. On suppose que, pour tous $A, B \in \mathcal{A}$, l'inclusion $A \subset B$ implique l'égalité $A=B$. Soit $\mathcal{S}_{n}$ l'ensemble des permutations de $\llbracket 1, n \rrbracket$. On munit $\mathcal{S}_{n}$ de la distribution uniforme de probabilité. Si $A$ est une partie de $\llbracket 1, n \rrbracket$, soit $X_{A}$ la variable de Bernoulli égale à 1 si $\sigma(A)=A$.
        
        a) Quelle est la loi de $X_{A}$ ?
        
        b) Si $A, B \in \mathcal{A}$, les variables $X_{A}$ et $X_{B}$ sont-elles indépendantes?
        

    === "Corrigé"
         