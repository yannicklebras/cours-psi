!!! exercice "RMS2023-1165"
    === "Énoncé"
         Soient $b>a>0$ deux réels. On pose $F(x)=\int_{0}^{+\infty} \frac{e^{-a t}-e^{-b t}}{t} \cos (x t) \mathrm{d} t$.
        
        a) Montrer que $F$ est définie sur $\mathbb{R}$ et que c'est une fonction de classe $\mathcal{C}^{1}$.
        
        b) Montrer qu'il existe $C \in \mathbb{R}$ tel que : $\forall x \in \mathbb{R}, F(x)=\ln \left(\frac{a^{2}+x^{2}}{b^{2}+x^{2}}\right)+C$.
        
        c) Montrer qu'il existe une fonction $h$ telle que $\forall x \in \mathbb{R}^{*}, F(x)=\frac{1}{x} \int_{0}^{+\infty} h(t) \sin (x t) \mathrm{d} t$.
        
        Déterminer la constante $C$.
        

    === "Corrigé"
         