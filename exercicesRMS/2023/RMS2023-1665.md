!!! exercice "RMS2023-1665"
    === "Énoncé"
         [CCINP] Soit $E=\left\{f \in \mathcal{C}^{1}([0,1], \mathbb{R}), f(0)=f(1)=0\right\}$.
        
        a) Montrer que $E$ est un espace vectoriel.
        
        b) Donner l'ensemble de définition $D$ de $x \mapsto \operatorname{cotan}(\pi x)=\frac{\cos (\pi x)}{\sin (\pi x)}$.
        c) Donner des équivalents de $x \mapsto \operatorname{cotan}(\pi x)$ en 0 et en 1 .
        
        d) Soit $f \in E$.
        
        i) Justifier que $I=\int_{0}^{1} f(x) f^{\prime}(x) \operatorname{cotan}(\pi x) \mathrm{d} x$ converge.
        
        ii) Démontrer l'égalité $2 \pi I=\pi^{2} \int_{0}^{1} f^{2}(x)\left(1+\operatorname{cotan}^{2}(\pi x)\right) \mathrm{d} x$.
        
        iii) En déduire qu'il existe une constante $\alpha \in \mathbb{R}$ telle que $\forall f \in E, \int_{0}^{1} f^{2} \leqslant \alpha \int_{0}^{1}\left(f^{\prime}\right)^{2}$.
        
        Ind. Considérer l'intégrale $\int_{0}^{1}\left|f^{\prime}(x)-\pi f(x) \operatorname{cotan}(\pi x)\right|^{2} \mathrm{~d} x$.
        
        iv) Étudier le cas d'égalité.
        

    === "Corrigé"
         