!!! exercice "RMS2023-209"
    === "Énoncé"
         Soit $\left(u_{n}\right)_{n \geqslant 0}$ la suite de fonctions définie par :
        
        $\forall x \in \mathbb{R}, u_{0}(x)=0$ et $\forall n \in \mathbb{N}^{*}, \forall x \in \mathbb{R}, u_{n}(x)=\frac{x}{n\left(1+n x^{2}\right)}$.
        
        a) Étudier la convergence de $\sum u_{n}$.
        
        b) Sur quel domaine a-t-on $\left(\sum u_{n}\right)^{\prime}=\sum u_{n}^{\prime}$ ?
        
        c) La fonction $\sum_{n=0}^{+\infty} u_{n}$ est-elle dérivable en 0 ?
        

    === "Corrigé"
         