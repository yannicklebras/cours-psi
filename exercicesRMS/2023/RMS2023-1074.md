!!! exercice "RMS2023-1074"
    === "Énoncé"
         On dit que $A \in \mathcal{S}_{n}(\mathbb{R})$ est positive lorsque, pour tout $X \in \mathcal{M}_{n, 1}(\mathbb{R})$, on a $X^{T} A X \geqslant 0$. a) Montrer que $A$ est positive si et seulement si toutes ses valeurs propres sont positives.
        b) Montrer que si $A=\left(a_{i, j}\right)$ et $B=b(i, j)$ sont symétriques positives, alors la matrice $C=\left(a_{i, j} b_{i, j}\right)$ est symétrique positive.
        
        ## Analyse
        

    === "Corrigé"
         