!!! exercice "RMS2023-267"
    === "Énoncé"
         Existe-t-il des variables aléatoires $X, Y$ telles que $X \sim \mathcal{B}(p), Y \sim \mathcal{P}(p)$ et telles que l'on ait $\mathbf{P}(X=Y)=1-p+p e^{-p}$ ?

    === "Corrigé"
         