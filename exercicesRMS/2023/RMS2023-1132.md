!!! exercice "RMS2023-1132"
    === "Énoncé"
         Convergence et calcul éventuel de l'intégrale : $\int_{1}^{+\infty}\left(\frac{1}{t}-\arcsin \left(\frac{1}{t}\right)\right) \mathrm{d} t$.

    === "Corrigé"
         