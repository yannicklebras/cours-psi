!!! exercice "RMS2023-1663"
    === "Énoncé"
         [CCINP] Soient $(a, b) \in \mathbb{R}^{2}$ tel que $a<b$ et $f \in \mathcal{C}^{2}([a, b], \mathbb{R})$.
        
        On suppose que $f(a)=f(b)=0$ et on pose $P=\frac{1}{2}(X-a)(X-b)$.
        
        a) Exprimer $\int_{a}^{b} f(t) \mathrm{d} t$ en fonction de $\int_{a}^{b} f^{\prime \prime}(t) P(t) \mathrm{d} t$.
        
        b) En déduire que $\left|\int_{a}^{b} f(t) \mathrm{d} t\right| \leqslant \frac{(b-a)^{3}}{12}\left\|f^{\prime \prime}\right\|_{\infty}$.
        

    === "Corrigé"
         