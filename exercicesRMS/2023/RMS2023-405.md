!!! exercice "RMS2023-405"
    === "Énoncé"
         Soit $E$ un espace vectoriel de dimension finie. Montrer qu'il existe une base de $\mathcal{L}(E)$ formée de projecteurs.

    === "Corrigé"
         