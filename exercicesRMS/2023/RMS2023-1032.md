!!! exercice "RMS2023-1032"
    === "Énoncé"
         Soit $A=(\min (i, j))_{1 \leqslant i, j \leqslant n}$.
        
        a) Trouver $L$ matrice triangulaire inférieure avec des 1 pour coefficients diagonaux et $U$ matrice triangulaire supérieure telles que $A=L U$. On montrera l'unicité de la décomposition.
        
        b) Soit $N=\left(n_{i j}\right)_{1 \leqslant i, j \leqslant n}$ avec $n_{i j}=1$ si $j=i+1$ et 0 sinon. Exprimer $A^{-1}$ en fonction de $N$.
        
        c) Montrer que $\operatorname{Sp}(A) \subset[0,4]$.
        

    === "Corrigé"
         