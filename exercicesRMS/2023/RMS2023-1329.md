!!! exercice "RMS2023-1329"
    === "Énoncé"
         Soit $(E,\langle\rangle$,$) un espace euclidien.$
        
        a) Pour $a \in E$, on note $\varphi_{a}: x \mapsto\langle a, x\rangle$. Montrer que $\psi: a \mapsto \varphi_{a}$ est un isomorphisme de $E$ vers $\mathcal{L}(E, \mathbb{R})$.
        
        b) Montrer qu'il existe un unique $A \in \mathbb{R}_{2}[X]$ tel que
        
        $\forall P \in \mathbb{R}_{2}[X], P\left(\frac{1}{2}\right)=\int_{-1}^{1} A(t) P(t) \mathrm{d} t$. Déterminer explicitement le polynôme $A$.
        

    === "Corrigé"
         