!!! exercice "RMS2023-1304"
    === "Énoncé"
         Soit $f: x \mapsto \sum_{n=0}^{+\infty} \cos \left(n^{2} x\right) e^{-n}$.
        
        a) Montrer que $f$ est de classe $C^{\infty}$. Exprimer $f^{(4 p)}(0)$ sous forme de somme.
        
        b) Montrer : $\forall p \in \mathbb{N}^{*}, \int_{0}^{8 p} t^{8 p} e^{-t} \mathrm{~d} t \leqslant \sum_{n=0}^{8 p} n^{8 p} e^{-n} \int_{8 p}^{+\infty} t^{8 p} e^{-t} \mathrm{~d} t \leqslant \sum_{n=8 p}^{+\infty} n^{8 p} e^{-n}$.
        
        c) Montrer que, pour tout $r>0$, la série $\sum \frac{f^{(4 p)(0)}}{(4 p) !} r^{4 p}$ diverge.
        
        d) Montrer que $f$ n'est pas développable en série entière en 0 .
        

    === "Corrigé"
         