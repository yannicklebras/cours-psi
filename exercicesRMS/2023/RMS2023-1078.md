!!! exercice "RMS2023-1078"
    === "Énoncé"
         Soit $g: \mathbb{U} \rightarrow \mathbb{U}$ une fonction continue telle que : $\forall z_{1}, z_{2} \in \mathbb{U}, g\left(z_{1} z_{2}\right)=g\left(z_{1}\right) g\left(z_{2}\right)$. Pour $t \in \mathbb{R}$, on pose $f(t)=g\left(e^{i t}\right)$.
        
        a) Quelle égalité fonctionnelle vérifie $f$ ?
        
        b) En introduisant $F: t \mapsto \int_{0}^{t} f(s) \mathrm{d} s$, montrer que $f$ est de classe $\mathcal{C}^{1}$.
        
        c) Montrer qu'il existe $\lambda \in \mathbb{R}$ tel que : $\forall t \in \mathbb{R}, f(t)=e^{i \lambda t}$.
        d) Montrer qu'il existe $n \in \mathbb{Z}$ tel que : $\forall z \in \mathbb{U}, g(z)=z^{n}$.
        
        e) Déterminer l'ensemble des fonctions continues $h: \mathbb{C}^{*} \rightarrow \mathbb{C}^{*}$ telles que :
        
        $\forall z_{1}, z_{2} \in \mathbb{C}^{*}, h\left(z_{1} z_{2}\right)=h\left(z_{1}\right) h\left(z_{2}\right)$.
        

    === "Corrigé"
         