!!! exercice "RMS2023-1205"
    === "Énoncé"
         Soient $X, Y$ deux variables aléatoires discrètes à valeurs dans $\mathbb{R}^{+*}$, indépendantes et identiquement distribuées. Montrer que $\mathbf{E}(X / Y) \geqslant 1$. À quelle condition a-t-on égalité?

    === "Corrigé"
         