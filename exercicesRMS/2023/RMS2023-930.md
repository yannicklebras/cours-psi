!!! exercice "RMS2023-930"
    === "Énoncé"
         ${ }^{\star}$ Soient $E$ un $\mathbb{R}$ espace vectoriel de dimension finie, $u \in \mathcal{L}(E)$ nilpotent, $F$ est sousespace vectoriel de $E$ tel que $u(F) \subset F$. On suppose que $E=F+\operatorname{Im}(u)$. Montrer que $E=F$.

    === "Corrigé"
         