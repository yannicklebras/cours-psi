!!! exercice "RMS2023-444"
    === "Énoncé"
         Soit $A \in \mathcal{M}_{n}(\mathbb{C})$ admettant $n$ valeurs propres non nulles distinctes $\lambda_{1}, \ldots, \lambda_{n}$.
        a) Montrer qu'il existe des nombres complexes $c_{i, j}$, avec $1 \leqslant i \leqslant n, 0 \leqslant j \leqslant n-1$, tels que $\forall k \in \mathbb{N}, A^{k}=\sum_{i=1}^{n} \sum_{j=0}^{n-1} c_{i, j} \lambda_{i}^{k} A^{j}$.
        
        b) Montrer l'unicité des $c_{i, j}$.
        
        c) On suppose de plus $A$ inversible. Montrer que la formule reste vraie si $k \in \mathbb{Z}$.
        

    === "Corrigé"
         