!!! exercice "RMS2023-1378"
    === "Énoncé"
         Soit $\left(X_{n}\right)_{n \geqslant 1}$ une suite i.i.d. de variables aléatoires suivant la loi de Bernoulli de paramètre $p$. Soit $S_{n}=\sum_{k=1}^{n}\left(X_{k}+X_{k+1}\right)$. Calculer l'espérance et la variance de $S_{n}$.

    === "Corrigé"
         