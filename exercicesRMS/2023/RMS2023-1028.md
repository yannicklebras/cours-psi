!!! exercice "RMS2023-1028"
    === "Énoncé"
         a) Soient $A$ une matrice réelle et $t \in \mathbb{R}^{+}$. Montrer que $\operatorname{det}\left(A^{2}-t I_{n}\right) \geqslant 0$.
        
        b) Soit un entier $n$ impair. Montrer que l'on ne peut pas écrire $-I_{n}$ comme la somme de deux carrés de matrices réelles.
        

    === "Corrigé"
         