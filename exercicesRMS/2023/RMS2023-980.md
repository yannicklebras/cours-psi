!!! exercice "RMS2023-980"
    === "Énoncé"
         Soient $u \in \mathcal{C}^{0}\left(\mathbb{R}^{+}, \mathbb{R}\right)$ intégrable sur $\mathbb{R}^{+}$et $f \in \mathcal{C}^{2}\left(\mathbb{R}^{+}, \mathbb{R}\right)$ telle que $f^{\prime \prime}+(1+u) f=0$.
        
        Soit $g: x \in \mathbb{R}^{+} \mapsto f(x)+\int_{0}^{x} \sin (x-t) f(t) u(t) \mathrm{d} t$.
        
        a) Trouver une équation différentielle linéaire vérifiée par $g$.
        
        b) En déduire l'existence de $c$ positif tel que : $\forall x \in \mathbb{R}^{+},|f(x)| \leqslant c+\int_{0}^{x}|f(t) u(t)| \mathrm{d} t$.
        c) Montrer que $f$ est bornée.
        

    === "Corrigé"
         