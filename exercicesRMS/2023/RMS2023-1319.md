!!! exercice "RMS2023-1319"
    === "Énoncé"
         Soit $M \in \mathcal{M}_{n}(\mathbb{C})$.
        
        a) Pour $k \in \mathbb{N}$, montrer que $\operatorname{Ker}\left(M^{k}\right) \subset \operatorname{Ker}\left(M^{k+1}\right)$ puis montrer qu'il existe $k_{0} \in \mathbb{N}^{*}$ tel que $\operatorname{Ker}\left(M^{k_{0}}\right) \neq \operatorname{Ker}\left(M^{k_{0}-1}\right)$ et, pour tout $p \geqslant k_{0}, \operatorname{Ker}\left(M^{p}\right)=\operatorname{Ker}\left(M^{k_{0}}\right)$.
        
        b) On suppose qu'il existe $p \in \mathbb{N}^{*}$ tel que $M^{p-1} \neq 0$ et $M^{p}=0$. Montrer que $M$ est semblable à une matrice triangulaire supérieure à diagonale nulle.
        
        c) Montrer que le noyau de la trace est égal au sous-espace vectoriel engendré par les matrices $M$ pour lesquelles il existe $p \in \mathbb{N}^{*}$ tel que $M^{p}=0$.
        

    === "Corrigé"
         