!!! exercice "RMS2023-1117"
    === "Énoncé"
         Soit $n \in \mathbb{N}^{*}$. Montrer que $\sum_{k=0}^{n-1}|\cos k| \geqslant \frac{2 n}{5}$.

    === "Corrigé"
         