!!! exercice "RMS2023-1199"
    === "Énoncé"
         Soient $n \in \mathbb{N}^{*}, X_{1}, \ldots, X_{n}$ et $Y_{1}, \ldots, Y_{n}$ des variables aléatoires i.i.d. suivant la loi de Bernoulli de paramètre $p \in] 0,1\left[\right.$. On pose $U=\left(X_{1}, \ldots X_{n}\right), V=\left(Y_{1}, \ldots, Y_{n}\right)$ et $M=U^{T} V$.
        
        a) Déterminer la loi du rang de $M$.
        
        b) Quelle est la probabilité que $M$ soit un projecteur?
        
        c) Quelle est la probabilité que $M$ soit diagonalisable?
        

    === "Corrigé"
         