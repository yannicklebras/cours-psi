!!! exercice "RMS2023-946"
    === "Énoncé"
         Soit $A \in \mathcal{M}_{2}(\mathbb{R})$ telle que $A^{T}=A^{2}$.
        
        a) Montrer que $A^{T} A$ est semblable à une matrice diagonale $D$. Quel est le nombre de possibilités pour $D$ ?
        
        b) Montrer que $A$ est orthogonalement semblable à une des matrices suivantes :
        
        $0_{2}, I_{2}, \quad\left(\begin{array}{ll}1 & 0 \\ 0 & 0\end{array}\right),\left(\begin{array}{cc}-\frac{1}{2} & -\frac{\sqrt{3}}{2} \\ \frac{\sqrt{3}}{2} & -\frac{1}{2}\end{array}\right)$.
        

    === "Corrigé"
         