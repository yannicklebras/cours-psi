!!! exercice "RMS2023-1550"
    === "Énoncé"
         [IMT] Soit $T=\left(\begin{array}{lllll}1 & 1 & 1 & 1 & 1 \\ 0 & 0 & 1 & 0 & 0 \\ 0 & 0 & 1 & 0 & 0 \\ 0 & 0 & 1 & 0 & 0 \\ 0 & 0 & 1 & 0 & 0\end{array}\right)$. Préciser le rang de $T$. Déterminer ses valeurs propres sans calculer le polynôme caractéristique. La matrice $T$ est-elle diagonalisable ?

    === "Corrigé"
         