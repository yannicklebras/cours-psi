!!! exercice "RMS2023-1363"
    === "Énoncé"
         Pour $n \in \mathbb{N}$, on pose $a_{n}=\int_{0}^{\pi / 2} \cos ^{n}(x) \sin (n x) \mathrm{d} x$ et $b_{n}=\int_{0}^{\pi / 2} \cos ^{n}(x) \mathrm{d} x$.
        
        a) Les suites $\left(a_{n}\right)$ et $\left(b_{n}\right)$ sont-elles convergentes ?
        
        b) Convergence et somme de la série $\sum_{n \geqslant 0}(-1)^{n} a_{n}$.
        

    === "Corrigé"
         