!!! exercice "RMS2023-1072"
    === "Énoncé"
         Soit $A \in \mathcal{S}_{n}(\mathbb{R})$ inversible et semblable à son inverse. Montrer que $\operatorname{tr}\left(A^{2}\right) \geqslant n$.

    === "Corrigé"
         