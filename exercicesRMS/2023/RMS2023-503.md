!!! exercice "RMS2023-503"
    === "Énoncé"
         Soit une suite $\left(a_{n}\right)_{n \in \mathbb{N}}$ telle que $\forall n \in \mathbb{N}, a_{n+2}=\frac{n+3}{n+2} a_{n+1}+\frac{3 n+7}{n+1} a_{n}$. Montrer que le rayon de convergence de $\sum a_{n} z^{n}$ est strictement positif et trouver un minorant de ce rayon.

    === "Corrigé"
         