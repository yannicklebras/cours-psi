!!! exercice "RMS2023-253"
    === "Énoncé"
         Soient $k \in \mathbb{N}^{*}$ et $f \in C^{0}([0,1], \mathbb{R})$. Soit $\left(f_{n}\right)_{n \geqslant 0} \in\left(C^{k}([0,1], \mathbb{R})\right)^{\mathbb{N}}$. On suppose que les deux conditions sont réalisées :
        
        i) $\left(f_{n}\right)_{n \geqslant 0}$ converge simplement vers $f$ sur $[0,1]$, ii) $\exists C>0, \forall n \in \mathbb{N},\left\|f_{n}^{(k)}\right\|_{\infty} \leqslant C$.
        
        Montrer que $\left(f_{n}\right)_{n}$ converge uniformément vers $f$ sur $[0,1]$.
        

    === "Corrigé"
         