# Progression PSI

- compléments sur les séries numériques
- régularité suites de fonctions
- régularité séries de fonctions

- intégration sur un intervalle quelconque
- suites et séries de fonctions intégrables
- régularité des intégrales à paramètre

- algèbre linéaire et polynômes d'endomorphismes
- réduction ++
- espaces préhilbertiens réels
- endomorphismes d'un espace euclidien

- séries entières
- Probas ++

- espaces vectoriels normés

- calcul diff ++
