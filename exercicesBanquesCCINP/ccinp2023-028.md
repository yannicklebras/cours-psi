!!! exercice "EXERCICE 28 analyse  "
	=== "Enoncé"  


		*N.B. : les deux questions sont indépendantes.*

		1.  La fonction $x\longmapsto \dfrac{e^{-x}}{\sqrt{x^{2}-4}}$ est-elle intégrable sur $]2,+\infty[$?

		2.  Soit $a$ un réel strictement positif.
		    La fonction $x\longmapsto \dfrac{\ln x}{\sqrt{1+x^{2a}}}$ est-elle intégrable sur $]0,+\infty[$?


	=== "Corrigé"  


		1.  Soit $f:x\longmapsto \dfrac{e^{-x}}{\sqrt{x^{2}-4}}$.
		$f$ est continue sur $]2,+\infty[$.
		$f(x)= \dfrac{e^{-x}}{\sqrt{(x-2)(x+2)}}\underset{2}{\thicksim}\dfrac{e^{-2}}{2}\times\dfrac{1}{(x-2)^{\frac{1}{2}}}$.
		Or $x\longmapsto \dfrac{1}{(x-2)^{\frac{1}{2}}}$ est intégrable sur $\left] 2,3\right]$ (fonction de Riemann intégrable sur $\left] 2,3\right]$ car $\dfrac{1}{2} <1$).
		Donc, par règle d’équivalence pour les fonctions positives, $f$ est intégrable sur $\left] 2,3\right]$.(\*)
		$f(x)\underset{+\infty}{\thicksim}\dfrac{\mathrm{e}^{-x}}{x}=g(x)$.
		Or $\lim\limits_{x\to+\infty}^{}x^2g(x)=0$ donc, au voisinage de $+\infty$, $g(x)=o(\dfrac{1}{x^2})$.
		Comme $x\longmapsto\dfrac{1}{x^2}$ est intégrable sur $\left[ 3,+\infty\right[$, on en déduit que $g$ est intégrable sur $\left[ 3,+\infty\right[$.
		Donc, par règle d’équivalence pour les fonctions positives, $f$ est intégrable sur $\left[ 3,+\infty\right[$. (\*\*)
		D’après (\*) et (\*\*), $f$ est intégrable sur $\left]2,+\infty \right[$.

		2.  Soit $a$ un réel strictement positif.
		On pose $\forall\:x\in \left]0,+\infty \right[$, $f(x)=  \dfrac{\ln x}{\sqrt{1+x^{2a}}}$.
		$f$ est continue sur $\left]0,+\infty \right[$.
		$|f(x)|\underset{0}{\thicksim}|\ln x|=g(x)$.
		Or $\lim\limits_{x\to 0}^{}x^\frac{1}{2}g(x)=0$ donc, au voisinage de 0, $g(x)=o\left( \dfrac{1}{x^{\frac{1}{2}}}\right)$.
		Or $x\longmapsto \dfrac{1}{x^{\frac{1}{2}}}$ est intégrable sur $\left] 0,1\right]$ (fonction de Riemann intégrable sur $\left] 0,1\right]$ car $\dfrac{1}{2}<1$).
		Donc $g$ est intégrable sur $\left] 0,1\right]$.
		Donc, par règle d’équivalence pour les fonctions positives, $|f|$ est intégrable sur $\left] 0,1\right]$.
		Donc, $f$ est intégrable sur $\left] 0,1\right]$ (\*)
		$f(x)\underset{+\infty}{\thicksim}\dfrac{\ln x}{x^a}=h(x)$.

		**Premier cas: si $a>1$.**
		$\lim\limits_{x\to +\infty}^{}x^{\frac{1+a}{2}}h(x)=\lim\limits_{x\to +\infty}^{}x^{\frac{1-a}{2}}\ln x=0$, donc, au voisinage de $+\infty$, $h(x)=o\left( \dfrac{1}{x^{\frac{1+a}{2}}}\right)$.
		Or $x\longmapsto\dfrac{1}{x^{\frac{1+a}{2}}}$ est intégrable sur $\left[ 1,+\infty\right[$ (fonction de Riemann intégrable sur $\left[ 1,+\infty\right[$ car $\dfrac{1+a}{2}>1$).
		Donc, $h$ est intégrable sur $\left[ 1,+\infty\right[$.
		Donc, par règle d’équivalence pour les fonctions positives, $f$ est intégrable sur $\left[ 1,+\infty\right[$.(\*\*).
		D’après (\*) et (\*\*), $f$ est intégrable sur $\left] 0,+\infty\right[$.

		**Deuxième cas: si $a\leqslant 1$**
		$\forall\:x\in \left[ \mathrm{e},+\infty\right[$, $h(x)\geqslant \dfrac{1}{x^a}$.
		Or $x\longmapsto \dfrac{1}{x^a}$ non intégrable sur $\left[ \mathrm{e},+\infty\right[$.(fonction de Riemann avec $a\leqslant 1$)
		Donc, par règle de minoration pour les fonctions positives, $h$ non intégrable sur $\left[ \mathrm{e},+\infty\right[$
		Donc, par règle d’équivalence pour les fonctions positives, $f$ non intégrable sur $\left[ \mathrm{e},+\infty\right[$.
		Donc, $f$ non intégrable sur $\left] 0,+\infty\right[$.

