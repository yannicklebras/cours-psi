!!! exercice "EXERCICE 34 analyse  "
	=== "Enoncé"  


		Soit $A$ une partie non vide d’un $\mathbb{R}$-espace vectoriel normé $E$.

		1.  Rappeler la définition d’un point adhérent à $A$, en termes de voisinages ou de boules.

		2.  Démontrer que: $x\in\bar{A}\Longleftrightarrow\exists(x_n)_{n\in\mathbb N}$ telle que, $\forall n\in\mathbb{N}, x_n\in A$ et $\displaystyle\lim_{n\to+\infty}x_n=x$.

		3.  Démontrer que, si $A$ est un sous-espace vectoriel de $E$, alors $\bar{A}$ est un sous-espace vectoriel de $E$.

		4.  Démontrer que si $A$ est convexe alors $\bar{A}$ est convexe.


	=== "Corrigé"  


		1.  Soit $A$ une partie non vide de $E$.
		    $\mathcal{V}(a)$ désigne l’ensemble des voisinages de $a$.
		    $\forall\:r>0$, $B_{0}(a,r)$ désigne la boule ouverte de centre $a$ et de rayon $r$.
		    Soit $a\in A$.
		    $a\in{\bar{A}}\:\Longleftrightarrow\: \forall\:V\in{\mathcal{V}(a)},\: V\cap A\neq \emptyset$.
		    Ou encore:
		    $a\in{\bar{A}}\:\Longleftrightarrow\: \forall\:r>0,\: B_{0}(a,r)\cap A\neq \emptyset$.

		2.  Soit $x\in \overline{A}$.
		    Prouvons que $\exists(x_n)_{n\in\mathbb N}$ telle que, $\forall n\in\mathbb N,\ x_n\in A$ et $\displaystyle\lim_{n\to+\infty}x_n=x$.
		    Par hypothèse, $\forall\:r>0,\: B_{0}(a,r)\cap A\neq \emptyset$.
		    Donc $\forall n\in\mathbb{N}^*$, $B_0(x,\dfrac{1}{n})\cap A\neq \emptyset$.
		    C’est-à-dire $\forall n\in\mathbb{N}^*$, $\exists\:x_n\in B_0(x,\dfrac{1}{n})\cap A$.
		    On fixe alors, pour tout entier naturel $n$ non nul, un tel $x_n$.
		    Ainsi, la suite $(x_n)_{n\in\mathbb{N}^{*}}$ est une suite à valeurs dans $A$ et $\forall n\in\mathbb{N}^*$, $||x_n-x||<\dfrac{1}{n}$.
		    C’est-à-dire la suite $(x_n)_{n\in\mathbb{N}^{*}}$ converge vers $x$.
		    Soit $x\in E$. On suppose que $\exists(x_n)_{n\in\mathbb N}$ telle que $\forall n\in\mathbb N$, $x_n\in A$ et $\displaystyle\lim_{n\to+\infty}x_n=x$.
		    Prouvons que $x\in\bar{A}$.
		    Soit $V\in{\mathcal{V}(x)}$. Alors, $\exists\:\varepsilon> 0$ tel que $B_0(x,\varepsilon )\subset V$.
		    On fixe un tel $\varepsilon$ strictement positif.
		    $\displaystyle\lim_{n\to+\infty}x_n=x$ donc $\exists\:N\in\mathbb{N}$ tel que $\forall n\in\mathbb{N}$, $n\geqslant N \Longrightarrow ||x_n-x||< \varepsilon$.
		    On fixe un tel entier $N$.
		    Donc, comme $(x_n)$ est à valeurs dans $A$, on en déduit que $\forall n\in\mathbb{N}$, $n\geqslant N \Longrightarrow x_n\in B_0(x,\varepsilon )\cap A$.
		    Or $B_0(x,\varepsilon )\subset V$, donc $\forall n\in\mathbb{N}$, $n\geqslant N \Longrightarrow x_n\in V\cap A$, c’est-à-dire $V\cap A\neq \emptyset$.
		    On peut en conclure que $x\in \bar{A}$.

		3.  $\bar{A} \subset E$ et $0_E  \in \bar{A}$ car $0_E  \in A$ et $A \subset \bar{A}$.
		    Soit $(x,y) \in \left( \bar{A}\right) ^2$ et $\lambda \in \mathbb{K}$.
		    D’après 1., Il existe deux suites $(x_n )$ et $(y_n )$ d’éléments de $A$ convergeant respectivement vers $x$ et $y$.
		    On a alors $\lim\limits_{n\to +\infty}^{}\left(  x_n  + \lambda y_n\right)   = x + \lambda y$.
		    Or $A$ est un sous-espace vectoriel de $E$ et $\forall n\in\mathbb{N}$, $(x_n,y_n)\in A^2$ , donc $x_n  + \lambda y_n  \in A$.
		    On en déduit que la suite $( x_n  + \lambda y_n )_{n\in\mathbb{N}}$ est à valeurs dans $A$ et converge vers $x + \lambda y$.
		    On a bien $x + \lambda y \in \bar{A}$.

		4.  On suppose que $A$ partie non vide et convexe de $E$. Prouvons que $\overline{A}$ est convexe.
		    Soit $(x,y)\in{\left( \overline{A}\right) ^2}$. Soit $t\in{[0,1]}$.
		    Prouvons que $z=tx+(1-t)y\in{\overline{A}}$.
		    $x\in{\overline{A}}$, donc il existe une suite $(x_n)$ à valeurs dans $A$ telle que $\mathop {\lim }\limits_{n \to  + \infty } {x_n} = x$.
		    $y\in{\overline{A}}$, donc il existe une suite $(y_n)$ à valeurs dans $A$ telle que $\mathop {\lim }\limits_{n \to  + \infty } {y_n} = y$.
		    On pose $\forall n \in \mathbb{N},{z_n} = t{x_n} + (1 - t){y_n}$.
		    $\forall n \in \mathbb{N}$, $x_n\in{A}$, $y_n\in{A}$ et $A$ est convexe, donc $z_n\in{A}$. De plus $\mathop {\lim }\limits_{n \to  + \infty } {z_n} = z$.
		    Donc $z$ est limite d’une suite à valeurs dans $A$, c’est-à-dire $z\in{\overline{A}}$.

