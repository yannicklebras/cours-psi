!!! exercice "EXERCICE 43 analyse  "
	=== "Enoncé"  


		Soit $x_0 \in \mathbb{R}$.
		On définit la suite $(u_n)$ par $u_0=x_0$ et, $\forall n\in\mathbb{N}\:,\: u_{n+1}=\mathrm{Arctan}(u_n)$.

		1.  1.  Démontrer que la suite $(u_n)$ est monotone et déterminer, en fonction de la valeur de $x_0$, le sens de variation de $(u_n)$.

		    2.  Montrer que $(u_n)$ converge et déterminer sa limite.

		2.  Déterminer l’ensemble des fonctions $h$, continues sur $\mathbb{R}$, telles que : $\forall x \in \mathbb{R}$, $h(x)=h(\mathrm{Arctan}\: x)$.


	=== "Corrigé"  


		On pose $f(x)=\mathrm{Arctan}\: x$.

		1.  1.  **Premier cas**: Si $u_1<u_0$
		        Puisque la fonction $f:x \mapsto \mathrm{Arctan}\: x$ est strictement croissante sur $\mathbb{R}$ alors $\mathrm{Arctan}(u_1)< \mathrm{Arctan}(u_0)$ c’est-à-dire $u_2 < u_1$.
		        Par récurrence, on prouve que $\forall n \in{\mathbb{N}} \:,\: u_{n+1} < u_n$. Donc la suite $(u_n)$ est strictement décroissante.  
		        **Deuxième cas**: Si $u_1>u_0$
		        Par un raisonnement similaire, on prouve que la suite $(u_n)$ est strictement croissante.  
		        **Troisième cas** : Si $u_1=u_0$
		        La suite $(u_n)$ est constante.

		        Pour connaître les variations de la suite $(u_n)$, il faut donc déterminer le signe de $u_1-u_0$, c’est-à-dire le signe de $\mathrm{Arctan} (u_0)-u_0$.
		        On pose alors $g(x)=\mathrm{Arctan} x -x$ et on étudie le signe de la fonction $g$.
		        On a $\forall x\in{\mathbb{R} }$, $g'(x)=\dfrac{-x^2}{1+x^2}$ et donc $\forall\:x\in\mathbb{R}^*$, $g'(x)<0$.
		        Donc $g$ est strictement décroissante sur $\mathbb{R}$ et comme $g(0)=0$ alors :
		        $\forall x\in{ \left]  0,+\infty\right[ }$, $g(x)< 0$ et $\forall x\in{ \left]-\infty,0\right[ }$, $g(x)> 0$.
		        On a donc trois cas suivant le signe de $x_0$:
		        - Si $x_0>0$, la suite$(u_n)$ est strictement décroissante.
		        - Si $x_0=0$, la suite $(u_n)$ est constante.
		        - Si $x_0<0$, la suite$(u_n)$ est strictement croissante.

		    2.  La fonction $g$ étant strictement décroissante et continue sur $\mathbb{R}$, elle induit une bijection de $\mathbb{R}$ sur $g(\mathbb{R})=\mathbb{R}$.
		        $0$ admet donc un unique antécédent par $g$ et, comme $g(0)=0$, alors 0 est le seul point fixe de $f$.
		        Donc si la suite $(u_n)$ converge, elle converge vers 0, le seul point fixe de $f$.  
		        **Premier cas**: Si $u_0>0$
		        L’intervalle $\left]  0,+\infty\right[$ étant stable par $f$, on a par récurrence, $\forall n\in\mathbb{N}$, $u_n > 0$. Donc la suite $(u_n)$ est décroissante et minorée par $0$, donc elle converge et ce vers 0, unique point fixe de $f$.  
		        **Deuxième cas**: Si $u_0<0$
		        Par un raisonnement similaire, on prouve que $(u_n)$ est croissante et majorée par 0, donc elle converge vers 0.   
		        **Troisième cas**: Si $u_0=0$
		        La suite $(u_n)$ est constante.

		    Conclusion: $\forall\:u_0\in\mathbb{R}$, $(u_n)$ converge vers 0.

		2.  Soit $h$ une fonction continue sur $\mathbb{R}$ telle que, $\forall x \in \mathbb{R}$, $h(x)=h(\mathrm{Arctan}\: x)$.
		    Soit $x \in \mathbb{R}$.
		    Considérons la suite $(u_n)$ définie par $u_0=x$ et $\forall n\in\mathbb{N}\:,\: u_{n+1}=\mathrm{Arctan}(u_n)$.
		    On a alors $h(x)=h(u_0)=h(\mathrm{Arctan}(u_0))=h(u_1)=h(\mathrm{Arctan}(u_1))=h(u_2)=\dots$.
		    Par récurrence, on prouve que, $\forall n \in {\mathbb{N}}$, $h(x)=h(u_n)$.
		    De plus $\lim\limits_{n\rightarrow+\infty} h(u_n)=h(0)$ par convergence de la suite $(u_n)$ vers $0$ et par continuité de $h$.
		    On obtient ainsi: $h(x)=h(0)$ et donc $h$ est une fonction constante.
		    Réciproquement, toutes les fonctions constantes conviennent.
		    Conclusion: Seules les fonctions constantes répondent au problème.

