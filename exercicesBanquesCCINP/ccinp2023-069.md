!!! exercice "EXERCICE 69 algèbre  "
	=== "Enoncé"  


		On considère la matrice $A=\begin{pmatrix}
		0&a&1\\
		a&0&1\\
		a&1&0
		\end{pmatrix}$ où $a$ est un réel.

		1.  Déterminer le rang de $A$.

		2.  Pour quelles valeurs de $a$, la matrice $A$ est-elle diagonalisable?


	=== "Corrigé"  


		1.  Après calcul, on trouve $\det A=a(a+1)$.
		    **Premier cas**: $a\neq 0$ et $a\neq -1$
		    Alors, $\det A\neq 0$ donc $A$ est inversible.
		    Donc $\mathrm{rg}A=3$.
		    **Deuxième cas** : $a=0$
		    $A=\begin{pmatrix}
		    0&0&1\\
		    0&0&1\\
		    0&1&0
		    \end{pmatrix}$ donc $\mathrm{rg}A=2$.
		    **Troisième cas**: $a=-1$
		    $A=\begin{pmatrix}
		    0&-1&1\\
		    -1&0&1\\
		    -1&1&0
		    \end{pmatrix}$ donc $\mathrm{rg}A\geqslant 2$ car les deux premières colonnes de $A$ sont non colinéaires.
		    Or $\det A=0$ donc $\mathrm{rg}A\leqslant 2$.
		    On en déduit que $\mathrm{rg}A=2$.

		2.  Notons $\chi_A$ le polynôme caractéristique de $A$.
		    $\det(\lambda I_n-A)=\begin{vmatrix}
		    \lambda&-a&-1\\
		    -a&\lambda&-1\\
		    -a&-1&\lambda
		    \end{vmatrix}$
		    Alors, en ajoutant à la première colonne la somme des deux autres puis, en soustrayant la première ligne aux deux autres lignes, on trouve successivement:
		    $\det(\lambda I_n-A)=
		    (\lambda-a-1)\begin{vmatrix}
		    1&-a&-1\\
		    1&\lambda&-1\\
		    1&-1&\lambda
		    \end{vmatrix}$ $=(\lambda-a-1)\begin{vmatrix}
		    1&-a&-1\\
		    0&\lambda+a&0\\
		    0&-1+a&\lambda+1
		    \end{vmatrix}$.
		    Donc, en développant par rapport à la première colonne,
		    $\det(\lambda I_n-A)=(\lambda-a-1)(\lambda+a)(\lambda+1)$.
		    Donc $\chi_A=(X-a-1)(X+a)(X+1)$.
		    Les racines de $\chi_A$ sont $a+1$, $-a$ et $-1$.
		    $a+1=-a\Longleftrightarrow a=-\dfrac{1}{2}$.
		    $a+1=-1\Longleftrightarrow a=-2$.
		    $-a=-1\Longleftrightarrow a=1$.
		    Ce qui amène aux quatre cas suivants:
		    **Premier cas**: $a\neq 1$, $a\neq-2$ et $a\neq-\dfrac{1}{2}$
		    Alors $A$ admet trois valeurs propres disctinctes.
		    Donc $A$ est diagonalisable.
		    **Deuxième cas**: $a=1$
		    $\chi_A=(X-2)(X+1)^2$.
		    Alors $A$ est diagonalisable si et seulement si $\dim E_{-1}=2$, c’est-à-dire $\mathrm{rg}(A+\mathrm{I}_3)=1$.
		    Or $A+\mathrm{I}_3=\begin{pmatrix}
		    1&1&1\\
		    1&1&1\\
		    1&1&1
		    \end{pmatrix}$ donc $\mathrm{rg}(A+\mathrm{I}_3)=1$.
		    Donc $A$ est diagonalisable.

		    **Troisième cas**: $a=-2$
		    Alors, $\chi_A=(X+1)^2(X-2)$.
		    $A+\mathrm{I}_3=\begin{pmatrix}
		    1&-2&1\\
		    -2&1&1\\
		    -2&1&1
		    \end{pmatrix}$
		    Les deux premières colonnes de $A+\mathrm{I}_3$ ne sont pas colinéaires, donc $\mathrm{rg(A+\mathrm{I}_3)}\geqslant2$.
		    De plus, $-1$ est valeur propre de $A$, donc $\mathrm{rg(A+\mathrm{I}_3)}\leqslant2$.
		    Ainsi, $\mathrm{rg}(A+\mathrm{I}_3)=2$ et $\dim E_{-1}=1$.
		    Or l’ordre multiplicité de la valeur propre $-1$ dans le polynôme caractéristique est 2.
		    On en déduit que $A$ n’est pas diagonalisable.
		    **Quatrième cas**: $a=-\dfrac{1}{2}$
		    $\chi_A=(X-\dfrac{1}{2})^2(X+1)$.

		    1,8 $A-\dfrac{1}{2}\mathrm{I}_3=\begin{pmatrix}
		    -\dfrac{1}{2}&-\dfrac{1}{2}&1\\
		    -\dfrac{1}{2}&-\dfrac{1}{2}&1\\
		    -\dfrac{1}{2}&1&-\dfrac{1}{2}
		    \end{pmatrix}$.
		    Les deux premières colonnes de $A-\dfrac{1}{2}\mathrm{I}_3$ sont non colinéaires, donc $\mathrm{rg}(A-\dfrac{1}{2}\mathrm{I}_3)\geqslant 2$.
		    De plus, $\dfrac{1}{2}$ est valeur propre donc $\mathrm{rg}(A-\dfrac{1}{2}\mathrm{I}_3)\leqslant 2$.
		    Ainsi, $\mathrm{rg}(A-\dfrac{1}{2}\mathrm{I}_3)= 2$ et $\dim E_{\frac{1}{2}}=1$.
		    Or l’ordre de multiplicité de la valeur propre $\dfrac{1}{2}$ dans le polynôme caractéristique est 2.
		    On en déduit que $A$ est non diagonalisable.

