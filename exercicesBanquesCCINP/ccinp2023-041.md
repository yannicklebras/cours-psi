!!! exercice "EXERCICE 41 analyse  "
	=== "Enoncé"  


		Énoncer quatre théorèmes différents ou méthodes permettant de prouver qu’une partie d’un espace vectoriel normé est fermée et, pour chacun d’eux, donner un exemple concret d’utilisation dans $\mathbb{R}^2$.
		Les théorèmes utilisés pourront être énoncés oralement à travers les exemples choisis.  
		**Remarques** :

		1.  On utilisera au moins une fois des suites.

		2.  On pourra utiliser au plus une fois le passage au complémentaire.

		3.  Ne pas utiliser le fait que $\mathbb{R}^2$ et l’ensemble vide sont des parties ouvertes et fermées.


	=== "Corrigé"  


		1.  Soit $E$ et $F$ deux espaces vectoriels normés.
		    Soit $f:E\longrightarrow F$ une application continue.
		    L’image réciproque d’un fermé de $F$ par $f$ est un fermé de $E$.  
		    **Exemple**: $A=\left\lbrace (x,y)\in{\mathbb{R}^{2}}\:/\:xy=1\right\rbrace$ est un fermé de $\mathbb{R}^2$ car c’est l’image réciproque du fermé $\left\lbrace 1 \right\rbrace$ de $\mathbb{R}$ par l’application continue $f:\begin{array}{ll}
		     \mathbb{R}^{2}&\longrightarrow \mathbb{R}\\
		    (x,y)&\longmapsto xy
		    \end{array}$.

		2.  Soit $E$ un espace vectoriel normé. Soit $F\subset E$.
		    $F$ est un fermé de $E$ si et seulement si $\complement_EF$ est un ouvert de $E$.  
		    **Exemple**: $B=\left\lbrace (x,y)\in\mathbb{R}^2\:/\:x^2+y^2\geqslant1\right\rbrace$ est un fermé de $\mathbb{R}^2$ car $\complement_{\mathbb{R}^2}B$ est un ouvert de $\mathbb{R}^2$.
		    En effet, $\complement_{\mathbb{R}^2}B=\left\lbrace (x,y)\in\mathbb{R}^2\:/\:x^2+y^2<1\right\rbrace=B_o(0,1)$ où $B_o(0,1)$ désigne la boule ouverte de centre 0 et de rayon 1 pour la norme euclidienne sur $\mathbb{R}^2$.
		    Puis, comme toute boule ouverte est un ouvert, on en déduit que $\complement_{\mathbb{R}^2}B$ est un ouvert.

		3.  Caractérisation séquentielle des fermés:
		    Soit $A$ une partie d’un espace vectoriel normé $E$.
		    $A$ est un fermé de $E$ si et seulement si, pour toute suite $(x_n)$ à valeurs dans $A$ telle que $\lim\limits_{n\to +\infty}^{}x_n=x$, alors $x\in A$.  
		    **Exemple**: $C=\left\lbrace (x,y)\in{\mathbb{R}^{2}}\:/\:xy\geqslant 1\right\rbrace$ est un fermé.
		    En effet, soit $\left( \left( x_n,y_n\right) \right)  _{n\in{\mathbb{N}}}$ une suite de points de $C$ qui converge vers $(x,y)$.
		    $\forall\;n\: \in{\mathbb{N}}\:,\: x_ny_n \geqslant 1$, donc, par passage à la limite, $xy\geqslant 1$ donc $(x,y)\in{C}$.

		4.  Une intersection de fermés d’un espace vectoriel normé $E$ est un fermé de $E$.  
		    **Exemple**: $D=\left\lbrace (x,y)\in{\mathbb{R}^{2}}\:/\:xy\geqslant 1\:\text{et}\: x\geqslant 0\right\rbrace$.
		    On pose $D_1=\left\lbrace (x,y)\in{\mathbb{R}^{2}}\:/\:xy\geqslant 1\right\rbrace$ et $D_2=\left\lbrace (x,y)\in{\mathbb{R}^{2}}\:/\: x\geqslant 0\right\rbrace$.
		    D’après 3., $D_1$ est un fermé.
		    $D_2$ est également un fermé.
		    En effet, $D_2$ est l’image réciproque du fermé $\left[ 0,+\infty\right[$ de $\mathbb{R}$ par l’application continue $f:\begin{array}{ll}
		    \mathbb{R}^2&\longrightarrow \mathbb{R}\\
		    (x,y)&\longmapsto x
		    \end{array}$.
		    On en déduit que $D=D_1\cap D_2$ est un fermé de $E$.

		**Remarque**:
		On peut aussi utiliser le fait qu’un produit de compacts est un compact et qu’un ensemble compact est fermé.
		Exemple: $E=\left[ 0;1\right] \times\left[2;5 \right]$ est un fermé de $\mathbb{R}^2$.
		En effet, comme $\left[0;1 \right]$ et $\left[ 2;5\right]$ sont fermés dans $\mathbb{R}$ et bornés, ce sont donc des compacts de $\mathbb{R}$.
		On en déduit que $E$ est un compact de $\mathbb{R}^2$ donc un fermé de $\mathbb{R}^2$.

