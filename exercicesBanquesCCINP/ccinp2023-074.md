!!! exercice "EXERCICE 74 algèbre  "
	=== "Enoncé"  


		1.  On considère la matrice $A=\begin{pmatrix}
		    1 & 0 & 2 \\
		    0 & 1 & 0\\
		    2 & 0 & 1
		    \end{pmatrix}$.

		    1.  Justifier sans calcul que $A$ est diagonalisable.

		    2.  Déterminer les valeurs propres de $A$ puis une base de vecteurs propres associés.

		2.  On considère le système différentiel $\left\{\begin{array}{l}
		     x'=x+2z\\
		     y'=y \\
		     z'=2x+z  
		    \end{array}\right.$ , $x,y,z$ désignant trois fonctions de la variable $t$, dérivables sur $\mathbb{R}$.

		    En utilisant la question 1. et en le justifiant, résoudre ce système.


	=== "Corrigé"  


		1.  1.  A est symétrique réelle donc diagonalisable.

		    2.  $P_A(X)=\det(X\mathrm{I}_3-A)=\begin{vmatrix}
		        -1+X&0&-2\\
		        0&-1+X&0\\
		        -2&0&-1+X\\
		        \end{vmatrix}$.
		        En développant par rapport à la première ligne, on obtient, après factorisation: $P_A(X)=(X-1)(X+1)(X-3)$.
		        On obtient aisément, $E_{1}= \mathrm{Vect}\left( \begin{pmatrix}
		        0\\1\\0
		        \end{pmatrix}\right)$, $E_{{-1}}=\mathrm{Vect}\left(\begin{pmatrix}
		        1\\0\\-1
		        \end{pmatrix} \right)$ et $E_3= \mathrm{Vect}\left( \begin{pmatrix}
		        1\\0\\1
		        \end{pmatrix}\right)$.
		        On pose $e'_1=(0,1,0)$, $e'_2=(1,0,-1)$ et $e'_3=(1,0,1)$.
		        Alors, $e'=(e'_1,e'_2,e'_3)$ est une base de vecteurs propres pour l’endomorphisme $f$ canoniquement associé à la matrice $A$.

		2.  Notons $(S)$ le système $\left\{\begin{array}{l}
		     x'=x+2z \\
		     y'=y \\
		     z'=2x+z
		    \end{array}\right.$.
		    Posons $X(t)=\begin{pmatrix}
		    x(t)\\
		    y(t)\\
		    z(t)\\
		    \end{pmatrix}$ .
		    Alors, $(S) \Longleftrightarrow X'=AX$.
		    On note $P$ la matrice de passage de la base canonique $e$ de $\mathbb{R}^3$ à la base $e'$.
		    D’après 1., $P=\begin{pmatrix}
		    0&1&1\\
		    1&0&0\\
		    0&-1&1
		    \end{pmatrix}$.
		    Et, si on pose $D=\begin{pmatrix}
		    1&0&0\\
		    0&-1&0\\
		    0&0&3
		    \end{pmatrix}$, alors $A=PDP^{-1}$.
		    Donc $(S) \Longleftrightarrow P^{-1}X'=DP^{-1}X$.
		    On pose alors $X_1=P^{-1}X$ et $X_1(t)=\begin{pmatrix}
		    x_1(t)\\
		    y_1(t)\\
		    z_1(t)\\
		    \end{pmatrix}$ .
		    Ainsi, par linéarité de la dérivation, $(S)\Longleftrightarrow X'_1=DX_1\Longleftrightarrow\left\lbrace
		    \begin{array}{lll}
		    x'_1&=&x_1\\
		    y'_1&=&-y_1\\
		    z'_1&=&3z_1
		    \end{array}\right.$
		    On résout alors chacune des trois équations différentielles d’ordre 1 qui constituent ce système.

		     On trouve $\left\lbrace \begin{array}{lll}
		    x_1(t)&=&a\mathrm{e}^{t}\\
		    y_1(t)&=&b\mathrm{e}^{-t}\\
		    z_1(t)&=&c\mathrm{e}^{3t}
		    \end{array}\right.$ avec $(a,b,c)\in\mathbb{R}^3$.
		    Enfin, on détermine $x,y,z$ en utilisant la relation $X=PX_1$.
		    On obtient: $\left\lbrace \begin{array}{lll}
		    x(t)&=&b\mathrm{e}^{-t}+c\mathrm{e}^{3t}\\
		    y(t)&=&a\mathrm{e}^{t}\\
		    z(t)&=&-b\mathrm{e}^{-t}+c\mathrm{e}^{3t}\\
		    \end{array}\right.$ avec $(a,b,c)\in\mathbb{R}^3$.

