!!! exercice "EXERCICE 30 analyse  "
	=== "Enoncé"  


		1.  Énoncer le théorème de dérivation sous le signe intégrale.

		2.  Démontrer que la fonction $f:x\longmapsto \displaystyle\int_{0}^{+\infty }e^{-t^{2}}\cos \left( xt\right) \text{d}t$ est de classe $C^{1}$ sur $\mathbb{R}$.

		3.  1.  Trouver une équation différentielle linéaire $\left(E\right)$ d’ordre $1$ dont $f$ est solution.

		    2.  Résoudre $\left(E\right)$.


	=== "Corrigé"  


		1.  Soit $u:(x,t) \mapsto u(x,t)$ une fonction définie de $X \times I$ vers $\mathbb{C}$, avec $X$ et $I$ intervalles contenant au moins deux points de $\mathbb{R}$.
		    On suppose que :  
		    i) $\forall\:x \in X$, $t\longmapsto u(x,t)$ est continue par morceaux et intégrable sur $I$.
		    On pose alors $\forall\:x \in X$, $f(x)=\int_{I}^{}u(x,t)\mathrm{d}t$.
		    ii) $u$ admet une dérivée partielle $\dfrac{{\partial u}}{{\partial x}}$ sur $X\times I$ vérifiant :
		    - $\forall x \in X,t \mapsto \dfrac{{\partial u}}{{\partial x}}(x,t)$ est continue par morceaux sur $I$.
		    - $\forall t \in I,x \mapsto \dfrac{{\partial u}}{{\partial x}}(x,t)$ est continue sur $X$.  
		    iii) il existe $\varphi :I \to \mathbb{R}^ +$ continue par morceaux, positive et intégrable sur $I$ vérifiant: $\forall (x,t) \in X \times I,\left| {\dfrac{{\partial u}}{{\partial x}}(x,t)} \right| \leqslant \varphi (t)$.
		    Alors la fonction $f$ est de classe $\mathcal{C}^1$ sur $X$ et $\forall x \in X,f'(x) = \displaystyle\int_I {\dfrac{{\partial u}}{{\partial x}}(x,t)\,{\mathrm{d}}t}$.

		2.  On pose $\forall\:(x,t)\in\mathbb{R}\times \left[ 0,+\infty\right[$, $u(x,t) = {\mathrm{e}}^{ - t^2 } \cos (xt)$.
		    i) $\forall \:x\in\mathbb{R}$, $t\longmapsto u(x,t)$ est continue sur $\left[ 0,+\infty\right[$.
		    De plus, $\forall \:x\in\mathbb{R}$, $|u(x,t)|\leqslant {\mathrm{e}}^{ - t^2 }$.
		    Or $\lim\limits_{t\to +\infty}^{}t^2{\mathrm{e}}^{ - t^2 }=0$, donc, au voisinage de $+\infty$, $\mathrm{e}^{ - t^2 }=o\left(\dfrac{1}{t^2} \right)$.
		    Donc, $t\longmapsto u(x,t)$ est intégrable sur $\left[ 0,+\infty\right[$.
		    ii) $\forall\:(x,t)\in\mathbb{R}\times \left[ 0,+\infty\right[$, ${\dfrac{{\partial u}}{{\partial x}}(x,t)}  =  { - t{\mathrm{e}}^{ - t^2 } \sin (xt)}$.
		    - $\forall x \in \mathbb{R}$, $t \mapsto \dfrac{{\partial u}}{{\partial x}}(x,t)$ est continue par morceaux sur $\left[ 0,+\infty\right[$. .
		    - $\forall t \in \left[ 0,+\infty \right]$, $x \mapsto \dfrac{{\partial u}}{{\partial x}}(x,t)$ est continue sur $\mathbb{R}$ .  
		    -iii) $\forall\:(x,t)\in\mathbb{R}\times \left[ 0,+\infty\right[$,$\left| \dfrac{\partial u}{\partial x}(x,t) \right| \leqslant t {\mathrm{e}}^{ - t^2 }=\varphi(t)$ avec $\varphi$ continue par morceaux, positive et intégrable sur $\left[ 0,+\infty\right[$.
		    En effet, $\lim\limits_{t\to +\infty}^{}t^2\varphi(t)=0$ donc, au voisinage de $+\infty$, $\varphi(t)=o(\dfrac{1}{t^2})$.
		    On en déduit que $\varphi$ est intégrable sur $\left[1,+\infty\right[$ et comme elle est continue sur $\left[ 0,1 \right[$, alors $\varphi$ est bien intégrable sur $\left[ 0,+\infty \right[$.
		    Donc $f$ est de classe $\mathcal{C}^1$ sur $\mathbb{R}$ et :
		    $\forall\:x\in\mathbb{R}$, $f'(x)=\displaystyle\int_{0}^{+\infty}{ - t{\mathrm{e}}^{ - t^2 } \sin (xt)}\mathrm{d}t$

		3.  1.  On a, $\forall\:x\in\mathbb{R}$, $f'(x) = \displaystyle\int_0^{ + \infty } { - t{\mathrm{e}}^{ - t^2 } \sin (xt)\,{\mathrm{d}}t}$.
		        Procédons à une intégration par parties. Soit $A \geqslant 0$.

				$$
				\displaystyle\int_0^A { - t{\mathrm{e}}^{ - t^2 } \sin (xt)\,{\mathrm{d}}t}  = \left[ {\dfrac{1}{2}{\mathrm{e}}^{ - t^2 } \sin (xt)} \right]_0^A  - \int_0^A {\dfrac{x}{2}{\mathrm{e}}^{ - t^2 } \cos (xt)\,{\mathrm{d}}t}
				$$

				En passant à la limite quand $A \to  + \infty$, on obtient $f'(x) + \dfrac{x}{2}f(x) = 0$.
		        Donc $f$ est solution de l’équation différentielle $(E)$: $y'+\dfrac{x}{2}y=0$.

		    2.  Les solutions de $(E)$ sont les fonctions $y$ définies par $y(x)=A\mathrm{e}^{-\dfrac{x^2}{4}}$, avec $A\in\mathbb{R}$.

