!!! exercice "EXERCICE 75 algèbre  "
	=== "Enoncé"  


		On considère la matrice $A=\begin{pmatrix}
		-1 & -4 \\
		1 & 3
		\end{pmatrix}$ .

		1.  Démontrer que $A$ n’est pas diagonalisable.

		2.  On note $f$ l’endomorphisme de $\mathbb{R}^{2}$ canoniquement associé à $A$.
		    Trouver une base $\left( v_{1},v_{2}\right)$ de $\mathbb{R}^{2}$ dans laquelle la matrice de $f$ est de la forme $\begin{pmatrix}
		    a & b \\
		    0 & c
		    \end{pmatrix}$.
		    On donnera explicitement les valeurs de $a$, $b$ et $c$.

		3.  En déduire la résolution du système différentiel $\left\{
		    \begin{array}{l}
		    x^{\prime }=-x-4y \\
		    y^{\prime }=x+3y
		    \end{array}
		    \right.$ .


	=== "Corrigé"  


		1.  On obtient le polynôme caractéristique $\chi _A(X)  = (X - 1)^2$, donc $\textrm{Sp} A = \left\{ 1 \right\}$.
		    Si $A$ était diagonalisable, alors $A$ serait semblable à $\mathrm{I}_2$, donc égale à $\mathrm{I}_2$.
		    Ce n’est visiblement pas le cas et donc $A$ n’est pas diagonalisable.

		2.  $\chi _A(X)$ étant scindé, $A$ est trigonalisable. $E_1 (A) = \textrm{Vect} \left(
		    \begin{pmatrix}
		    2\\
		    -1
		    \end{pmatrix}
		     \right)$.
		    Pour $v_1  = (2, - 1)$ et $v_2  = ( - 1,0)$ (choisi de sorte que $f(v_2 ) = v_2  + v_1$) on obtient une base $(v_1 ,v_2 )$ dans laquelle la matrice de $f$ est $T = \left( {
		    \begin{array}{cc}
		     1 & 1  \\
		     0 & 1  \\
		    \end{array}
		    } \right)$.

		3.  On a $A = PTP^{ - 1}$ avec $P = \begin{pmatrix}
		     2 & { - 1}  \\
		     { - 1} & 0  \\
		    \end{pmatrix}$.
		    Posons $X =\begin{pmatrix}
		      x  \\
		      y  \\
		    \end{pmatrix}\text{ et }Y = P^{ - 1} X =
		    \begin{pmatrix}
		      a  \\
		      b  \\
		    \end{pmatrix}$.

		    Le système différentiel étudié équivaut à l’équation $X' = AX$ qui équivaut encore , grâce à la linéarité de la dérivation, à l’équation $Y' = TY$.  
		    Cela nous amène à résoudre le système $\begin{cases}
		      a' = a + b \\
		      b' = b \\
		    \end{cases}$ de solution générale $\begin{cases}
		      a(t) = \lambda {\mathrm{e}}^t  + \mu t{\mathrm{e}}^t\\
		      b(t) = \mu {\mathrm{e}}^t\\
		    \end{cases}$.
		    Enfin, par la relation $X = PY$ on obtient la solution générale du système initial: $\left\{
		    \begin{gathered}
		      x(t) = \left( {(2\lambda  - \mu ) + 2\mu t} \right){\mathrm{e}}^t  \\
		      y(t) = \left( {-\lambda  - \mu t} \right){\mathrm{e}}^t  \\
		    \end{gathered}
		     \right.$

