!!! exercice "EXERCICE 1analyse  "
	=== "Enoncé"  


		1.  On considère deux suites numériques $\left( u_{n}\right)_{n\in \mathbb{N}}$ et $\left( v_{n}\right) _{n\in \mathbb{N}}$ telles que $(v_n)_{n\in \mathbb{N}}$ est non nulle à partir d’un certain rang et $u_{n}\underset{+\infty}\thicksim v_{n}$.
		    Démontrer que $u_{n}$ et $v_{n}$ sont de même signe à partir d’un certain rang.

		2.  Déterminer le signe, au voisinage de l’infini, de : $u_{n}=\text{sh}\left( \dfrac{1}{n}\right) -\tan \left( \dfrac{1}{n}\right)$.


	=== "Corrigé"  


		1.  Par hypothèse, $\exists\:N_0\in\mathbb{N}/\forall\:n\in\mathbb{N}, n\geqslant N_0\Longrightarrow v_n\neq 0$.
		    Ainsi la suite $\left( \dfrac{u_n}{v_n}\right)$ est définie à partir du rang $N_0$.
		    De plus, comme $u_{n}\underset{+\infty}\thicksim v_{n}$, on a $\lim\limits_{n\to +\infty}^{}\dfrac{u_n}{v_n}=1$.
		    Alors, $\forall\:\varepsilon>0$, $\exists N\in\mathbb{N}/N\geqslant N_0$ et $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow\left|\dfrac{u_n}{v_n}-1\right|\leqslant\varepsilon$.(1)
		    Prenons $\varepsilon=\dfrac{1}{2}$. Fixons un entier $N$ vérifiant $(1)$.
		    Ainsi, $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow\left|\dfrac{u_n}{v_n}-1\right|\leqslant\dfrac{1}{2}$.
		    C’est-à-dire, $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow -\dfrac{1}{2}\leqslant\dfrac{u_n}{v_n}-1\leqslant\dfrac{1}{2}$.
		    On en déduit que $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow \dfrac{u_n}{v_n}\geqslant \dfrac{1}{2}$.
		    Et donc, $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow \dfrac{u_n}{v_n}> 0$.
		    Ce qui implique que $u_n$ et $v_n$ sont de même signe à partir du rang $N$.

		2.  Au voisinage de $+ \infty$, $\textrm{sh} (\dfrac{1}{n}) = \dfrac{1}{n} + \dfrac{1}{{6n^3 }} + o\left( {\dfrac{1}{{n^3 }}} \right)\text{ et }\tan \dfrac{1}{n} = \dfrac{1}{n} + \dfrac{1}{{3n^3 }} + o\left( {\dfrac{1}{{n^3 }}} \right)$. Donc $u_n  \underset{+\infty}\thicksim - \dfrac{1}{{6n^3 }}$.
		    On en déduit, d’après 1., qu’à partir d’un certain rang, $u_n$ est négatif.

!!! exercice "EXERCICE 2 analyse  "
	=== "Enoncé"  


		On pose $f(x)=\dfrac{3x+7}{(x+1)^{2}}$ .

		1.  Décomposer $f(x)$ en éléments simples.

		2.  En déduire que $f$ est développable en série entière sur un intervalle du type $\left]-r,r \right[$ (où $r>0$).
		    Préciser ce développement en série entière et déterminer, en le justifiant, le domaine de validité $D$ de ce développement en série entière.

		3.  1.  Soit $\sum a_nx^n$ une série entière de rayon $R>0$.
		        On pose, pour tout $x\in \left]-R,R \right[$, $g(x)=\displaystyle\sum_{n=0}^{+\infty}a_nx^n$.
		        Exprimer, pour tout entier $p$, en le prouvant, $a_p$ en fonction de $g^{(p)}(0)$.

		    2.  En déduire le développement limité de $f$ à l’ordre 3 au voisinage de 0.


	=== "Corrigé"  


		1.  En utilisant les méthodes habituelles de décomposition en éléments simples, on trouve:
		    $f(x)=\dfrac{3}{x+1}+\dfrac{4}{(x+1)^2}$.

		2.  D’après le cours, $x\longmapsto\dfrac{1}{x+1}$ et $x\longmapsto \dfrac{1}{(x+1)^2}$ sont développables en série entière à l’origine.
		    De plus, on a $\forall \:x\in \left]-1,1 \right[$, $\dfrac{1}{1+x}=\sum\limits_{n=0}^{+\infty}(-1)^{n}x^n$.
		    Et, $\forall \:x\in \left]-1,1 \right[$, $\dfrac{1}{(1+x)^2}=\sum\limits_{n=1}^{+\infty}(-1)^{n+1}nx^{n-1}$ ( obtenu par dérivation du développement précédent).
		    On en déduit que $f$ est développable en série entière en tant que somme de deux fonctions développables en série entière.
		    Et $\forall \:x\in \left]-1,1 \right[$, $f(x)=3\sum\limits_{n=0}^{+\infty}(-1)^{n}x^n
		    +4\sum\limits_{n=0}^{+\infty}(-1)^{n}(n+1)x^{n}$.
		    C’est-à-dire : $\forall \:x\in \left]-1,1 \right[$, $f(x)=\displaystyle\sum\limits_{n=0}^{+\infty}(4n+7)(-1)^n x^n$.
		    Notons $D$ le domaine de validité du développement en série entière de $f$.
		    D’après ce qui précéde, $\left]-1,1 \right[\subset D$.
		    Notons $R$ le rayon de convergence de la série entière $\displaystyle\sum\limits_{}^{}(4n+7)(-1)^n x^n$.
		    D’après ce qui précéde $R\geqslant 1$.
		    Posons, pour tout entier naturel $n$, $a_n=(4n+7)(-1)^n$.
		    Pour $x=1$ et $x=-1$, $\lim\limits_{n\to +\infty}^{}\left|a_nx^n\right|=+\infty$ donc $\displaystyle\sum\limits_{}^{}(4n+7)(-1)^n x^n$ diverge grossièrement.
		    Donc $R\leqslant 1$, $1\not\in D$ et $-1\not\in D$.
		    On en déduit que $D=\left] -1,1 \right[$.

		3.  1.  Soit $\sum a_nx^n$ une série entière de rayon $R>0$.
		        On pose, pour tout $x\in \left]-R,R \right[$, $g(x)=\displaystyle\sum_{n=0}^{+\infty}a_nx^n$.
		        D’après le cours, $g$ est de classe $C^{\infty}$ sur $\left]-R,R \right[$.
		        De plus, $\forall\:x\in \left]-R,R \right[$,
		        $g'(x)=\displaystyle\sum_{n=1}^{+\infty}na_nx^{n-1}=\displaystyle\sum_{n=0}^{+\infty}(n+1)a_{n+1}x^{n}$
		        $g''(x)=\displaystyle\sum_{n=1}^{+\infty}n(n+1)a_{n+1}x^{n-1}=\displaystyle\sum_{n=0}^{+\infty}(n+1)(n+2)a_{n+2}x^{n}$.
		        et, par récurrence, on a :
		        $\forall\:p\in\mathbb{N}$, $\forall\:x\in \left]-R,R \right[$, $g^{(p)}(x)=\displaystyle\sum_{n=0}^{+\infty}(n+1)(n+2)...(n+p)a_{n+p}x^{n}=\displaystyle\sum_{n=0}^{+\infty}\dfrac{(n+p)!}{n!}a_{n+p}x^{n}$.
		        Ainsi, pour tout $\:p\in\mathbb{N}$, $g^{(p)}(0)=p!a_p$.
		        C’est-à-dire, pour tout $p\in\mathbb{N}$, $a_p=\dfrac{g^{(p)}(0)}{p!}$.

		    2.  $f$ est de classe $C^{\infty}$ sur $\left] -1,1\right[$.
		        Donc d’après la formule de Taylor-Young, au voisinage de $0$, $f(x)=\displaystyle\sum_{p=0}^{3}\dfrac{f^{(p)}(0)}{p!}x^p+o(x^3)$.(\*)
		        Or, d’après 3.(a), pour tout entier $p$, $\dfrac{f^{(p)}(0)}{p!}$ est aussi la valeur du $p^{\text{ième}}$ coefficient du développement en série entière de $f$.
		        Donc, d’après 2., pour tout entier $p$, $\dfrac{f^{(p)}(0)}{p!}=(4p+7)(-1)^p$. (\*\*)
		        Ainsi, d’après (\*) et (\*\*), au voisinage de $0$, $f(x)=\displaystyle\sum_{p=0}^{3}(4p+7)(-1)^px^p+o(x^3)$.
		        C’est-à-dire, au voisinage de $0$, $f(x)=7-11x+15x^2-19x^3+o(x^3)$.

!!! exercice "EXERCICE 3analyse  "
	=== "Enoncé"  


		1.  On pose $g(x)=\mathrm{e}^{2x}$ et $h(x)=\dfrac{1}{1+x}$.
		    Calculer, pour tout entier naturel $k$, la dérivée d’ordre $k$ des fonctions $g$ et $h$ sur leurs ensembles de définitions respectifs.

		2.  On pose $f(x)=\dfrac{\mathrm{e}^{2x}}{1+x}$.
		    En utilisant la formule de Leibniz concernant la dérivée $n^{\text{ième}}$ d’un produit de fonctions, déterminer, pour tout entier naturel $n$ et pour tout $x\in\mathbb{R}\backslash\left\lbrace -1\right\rbrace$, la valeur de $f^{(n)}(x)$.

		3.  Démontrer, dans le cas général, la formule de Leibniz, utilisée dans la question précédente.


	=== "Corrigé"  


		1.  $g$ est de classe $C^{\infty}$ sur $\mathbb{R}$ et $h$ est de classe $C^{\infty}$ sur $\mathbb{R}\backslash\left\lbrace-1 \right\rbrace$.
		    On prouve, par récurrence, que :
		    $\forall\:x\in\mathbb{R}$, $g^{(k)}(x)=2^k\mathrm{e}^{2x}$ et $\forall\:x\in\mathbb{R}\backslash\left\lbrace-1 \right\rbrace$, $h^{(k)}(x)=\dfrac{(-1)^kk!}{(1+x)^{k+1}}$.

		2.  $g$ et $h$ sont de classe $C^{\infty}$ sur $\mathbb{R}\backslash\left\lbrace-1 \right\rbrace$ donc, d’après la formule de Leibniz, $f$ est de classe $C^{\infty}$ sur $\mathbb{R}\backslash\left\lbrace-1 \right\rbrace$ et $\forall\:x\in \mathbb{R}\backslash\left\lbrace-1 \right\rbrace$:
		    $f^{(n)}(x)=\displaystyle\sum\limits_{k=0}^{n}\dbinom{n}{k}g^{(n-k)}(x)h^{(k)}(x)=
		    \displaystyle\sum\limits_{k=0}^{n}\dbinom{n}{k}2^{n-k}\mathrm{e}^{2x}\dfrac{(-1)^k k!}{(1+x)^{k+1}}= n!\mathrm{e}^{2x}\sum\limits_{k = 0}^n \dfrac{{( - 1)^k 2^{n - k} }}{{(n - k)!}(1 + x)^{k + 1}}$.

		3.  Notons $(P_n)$ la propriété:
		    Si $f:I \to \mathbb{R}$ et $g:I \to \mathbb{R}$ sont $n$ fois dérivables sur $I$ alors, $fg$ est $n$ fois dérivable sur $I$ et :
		    $\forall \:x\in I$, $(fg)^{(n)}(x)=\displaystyle\sum\limits_{k=0}^{n} \dbinom{n}{k}f^{(n-k)}(x) g^{(k)}(x)$.
		    Prouvons que $(P_n)$ est vraie par récurrence sur $n$.
		    La propriété est vraie pour $n = 0$ et pour $n=1$ (dérivée d’un produit).
		    Supposons la propriété vraie au rang $n \geqslant 0$.
		    Soit $f:I \to \mathbb{R}$ et $g:I \to \mathbb{R}$ deux fonctions $n + 1$ fois dérivables sur $I$.
		    Les fonctions $f$ et $g$ sont, en particulier, $n$ fois dérivables sur $I$ et donc par hypothèse de récurrence la fonction $fg$ l’est aussi avec $\forall \:x\in I$, $(fg)^{(n)}(x)=\displaystyle\sum\limits_{k=0}^{n} \dbinom{n}{k}f^{(n-k)}(x) g^{(k)}(x)$.

		    Pour tout $k \in \left\{ {0, \ldots ,n} \right\}$, les fonctions $f^{(n - k)}$ et $g^{(k)}$ sont dérivables sur $I$ donc par opération sur les fonctions dérivables, la fonction $(fg)^{(n)}$ est encore dérivable sur $I$.
		    Ainsi la fonction $fg$ est $(n + 1)$ fois dérivable et: $\forall \:x\in I$,$(fg)^{(n+1)}(x)=\displaystyle\sum\limits_{k=0}^{n} \dbinom{n}{k}\left( f^{(n+1-k)}(x) g^{(k)}(x)+f^{(n-k)}(x) g^{(k+1)}(x)\right)$.
		    En décomposant la somme en deux et en procédant à un décalage d’indice sur la deuxième somme, on obtient: $\forall \:x\in I$, $(fg)^{(n+1)}(x)=\displaystyle\sum\limits_{k=0}^{n} \dbinom{n}{k} f^{(n+1-k)}(x) g^{(k)}(x)
		    + \displaystyle\sum\limits_{k=1}^{n+1} \dbinom{n}{k-1}f^{(n+1-k)}(x) g^{(k)}(x)$.
		    C’est-à-dire $(fg)^{(n+1)}(x)=\displaystyle\sum\limits_{k=1}^{n}\left( \dbinom{n}{k}+\dbinom{n}{k-1}\right)f^{(n+1-k)}(x) g^{(k)}(x)+\dbinom{n}{0}f^{(n+1)}(x)g^{(0)}(x)+
		     \dbinom{n}{n}f^{(0)}(x)g^{(n+1)}(x)$.
		    Or, en utilisant le triangle de Pascal, on a $\dbinom{n}{k}+\dbinom{n}{k-1}=\dbinom{n+1}{k}$.
		    On remarque également que $\dbinom{n}{0}=1=\dbinom{n+1}{0}$ et $\dbinom{n}{n}=1=\dbinom{n+1}{n+1}$.
		    On en déduit que $(fg)^{(n+1)}(x)=\displaystyle\sum\limits_{k=0}^{n+1} \dbinom{n+1}{k}f^{(n+1-k)}(x) g^{(k)}(x)$.
		    Donc $(P_{n+1})$ est vraie.

!!! exercice "EXERCICE 4 analyse  "
	=== "Enoncé"  


		1.  Énoncer le théorème des accroissements finis.

		2.  Soit $f:\left[ a,b\right]\longrightarrow \mathbb{R}$ et soit $x_0\in \left]a,b \right[$.
		    On suppose que $f$ est continue sur $[a,b]$ et que $f$ est dérivable sur $]a,x_0[$ et sur $]x_0,b[$.
		    Démontrer que, si $f'$ admet une limite finie en $x_0$, alors $f$ est dérivable en $x_0$ et $f'(x_0)=\lim\limits_{x\rightarrow x_0}f'(x)$.

		3.  Prouver que l’implication : ( $f$ est dérivable en $x_0$) $\Longrightarrow$ ($f'$ admet une limite finie en $x_0$) est fausse.

		    **Indication**: on pourra considérer la fonction $g$ définie par: $g(x)=x^2\sin\dfrac{1}{x}$ si $x\not=0$ et $g(0)=0$.


	=== "Corrigé"  


		1.  Théorème des accroissements finis :
		    Soit $f:\left[ a,b\right] \longrightarrow \mathbb{R}$.
		    On suppose que $f$ est continue sur $\left[ a,b\right]$ et dérivable sur $\left]a,b \right[$.
		    Alors $\exists\:c\in \left]a,b \right[$ tel que $f(b)-f(a)=f'(c)(b-a)$.

		2.  On pose $l=\lim\limits_{x\to x_0}^{}\ f'(x )$.
		    Soit $h \ne 0$ tel que $x_0  + h \in \left[ {a,b} \right]$.
		    En appliquant le théorème des accroissements finis, à la fonction $f$, entre $x_0$ et $x_0  + h$, on peut affirmer qu’il existe $c_h$ strictement compris entre $x_0$ et $x_0  + h$ tel que $f(x_0  + h) - f(x_0 ) = f'(c_h )h$.
		    Quand $h \to 0$ (avec $h \ne 0$), on a, par encadrement, $c_h  \to x_0$.
		    Donc $\lim\limits_{h\to 0}^{}\dfrac{1}{h}\left( {f(x_0  + h) - f(x_0 )} \right) =\lim\limits_{h\to 0}^{}\ f'(c_h ) =\lim\limits_{x\to x_0}^{}\ f'(x ) =l$.
		    On en déduit que $f$ est dérivable en $x_0$ et $f'(x_0 ) = l$.

		3.  La fonction $g$ proposée dans l’indication est évidemment dérivable sur $\left] { - \infty ,0} \right[$ et $\left] {0, + \infty } \right[$.
		    $g$ est également dérivable en 0 car $\dfrac{1}{h}\left( {g(h) - g(0)} \right) = h\sin \left( \dfrac{1}{h} \right)$.
		    Or $\lim\limits_{\underset{h\neq 0}{h\to 0}}^{}h\sin \left( \dfrac{1}{h} \right)=0$ car $|h\sin \left( \dfrac{1}{h} \right)|\leqslant |h|$.
		    Donc, $g$ est dérivable en $0$ et $g'(0)=0$.
		    Cependant, $\forall \:x\in\mathbb{R}\backslash \left\lbrace 0\right\rbrace$, $g'(x) = 2x\sin \left( \dfrac{1}{x}\right)  - \cos \left( \dfrac{1}{x} \right)$.
		    $2x\sin \left( \dfrac{1}{x}\right) \xrightarrow[{x \to 0}]{}0$ (car $|2x\sin(\dfrac{1}{x})|\leqslant2|x|)$, mais $x\longmapsto\cos \left( \dfrac{1}{x}\right)$ n’admet pas de limite en 0.
		    Donc $g'$ n’a pas de limite en $0$.

!!! exercice "EXERCICE 5 analyse  "
	=== "Enoncé"  


		1.  On considère la série de terme général $u_{n}=\dfrac{1}{n\left( \ln n\right) ^{\alpha }}$ où $n\geqslant 2$ et $\alpha \in \mathbb{R}$.

		    1.  **Cas $\bm{\alpha \leqslant 0}$**

		        En utilisant une minoration très simple de $u_{n}$, démontrer que la série diverge.

		    2.  **Cas $\bm{\alpha >0}$**

		        Étudier la nature de la série.

		 **Indication**: on pourra utiliser la fonction $f$ définie par $f(x) =\dfrac{1}{x(\ln x) ^{\alpha }}$.

		2.  Déterminer la nature de la série $\displaystyle\sum\limits_{n\geqslant 2}^{}\dfrac{\left( \mathrm{e}-\left( 1+\dfrac{1}{n}\right)^n \right)\mathrm{e}^{\frac{1}{n}}}{\left( \ln(n^2+n)\right) ^2}$.


	=== "Corrigé"  


		1.  1.  Cas $\alpha \leqslant 0$
		        $\forall \:n \geqslant 2$, $\ln n \geqslant \ln 2$ donc $\left( \ln n\right)^{\alpha}\leqslant \left( \ln 2\right)^{\alpha}$.
		        On en déduit que : $\forall \:n \geqslant 2$, $u_n  \geqslant\dfrac{1}{ \left( \ln 2\right)^{\alpha}} \dfrac{1}{n}$.
		        Or $\displaystyle\sum\limits_{n\geqslant 2}^{}\dfrac{1}{n}$ diverge.
		        Donc , par critère de minoration pour les séries à termes positifs, on en déduit que $\displaystyle\sum\limits_{n\geqslant 2}^{} u_n$ diverge.

		    2.  Cas $\alpha  > 0$
		        La fonction $f:x \mapsto \dfrac{1}{{x(\ln x)^\alpha  }}$ est continue par morceaux, décroissante et positive sur $\left[ 2,+\infty\right[$ donc:
		        $\displaystyle\sum\limits_{n\geqslant 2}^{} f(n)$ et $\displaystyle\int_{2}^{+\infty} {f(x)\,{\mathrm{d}}x}$ sont de même nature.
		        Puisque $\displaystyle\int_2^X {f(x)\,{\mathrm{d}}x} \mathop  = \limits_{t = \ln x} \displaystyle\int_{\ln 2}^{\ln (X)} {\dfrac{{\,{\mathrm{d}}t}}{{t^\alpha  }}}$, on peut affirmer que : $\displaystyle\int_{2}^{+\infty} {f(x)\,{\mathrm{d}}x}$ converge $\Longleftrightarrow$ $\alpha  > 1$.
		        On en déduit que : $\displaystyle\sum\limits_{n\geqslant 2}^{} f(n)$ converge $\Longleftrightarrow$ $\alpha  > 1$.

		2.  On pose, pour tout entier naturel $n\geqslant 2$, $u_n=\dfrac{\left(\mathrm{e}- \left( 1+\dfrac{1}{n}\right)^n \right)\mathrm{e} ^{\frac{1}{n}}}{\left( \ln(n^2+n)\right) ^2}$.
		    Au voisinage de $+\infty$, $\mathrm{e}-\left( 1+\dfrac{1}{n}\right)^n=\mathrm{e}-\mathrm{e} ^{n\ln\left( 1+\frac{1}{n}\right) }=\mathrm{e}-\mathrm{e}^{n\left(\frac{1}{n} -\frac{1}{2n^2}+o\left( \frac{1}{n^2}\right) \right) }
		    =\mathrm{e}-\mathrm{e}^{1-\frac{1}{2n}+o\left(\frac{1}{n} \right) }=\dfrac{\mathrm{e}}{2n}+o\left(\dfrac{1}{n}\right)$.
		    On en déduit qu’au voisinage de $+\infty$, $\mathrm{e}-\left( 1+\dfrac{1}{n}\right)^n\underset{+\infty}{\thicksim}\dfrac{\mathrm{e}}{2n}$.
		    De plus, au voisinage de $+\infty$, $\ln\left( n^2+n\right)=2\ln n+\ln\left( 1+\dfrac{1}{n}\right)=2\ln n+\dfrac{1}{n} +o\left(\dfrac{1}{n} \right)$.
		    Donc $\ln\left( n^2+n\right)\underset{+\infty}{\thicksim}2\ln n$.
		    Et comme $\mathrm{e}^{\frac{1}{n}}\underset{+\infty}{\thicksim}1$, on en déduit que $u_n\underset{+\infty}{\thicksim}\dfrac{\mathrm{e}}{8}\times\dfrac{1}{n\left( \ln n\right) ^2}$.
		    Or, d’après 1.(b), $\displaystyle\sum\limits_{n\geqslant 2}^{}\dfrac{1}{n\left( \ln n\right) ^2}$ converge.
		    Donc, par critère d’équivalence pour les séries à termes positifs, $\displaystyle\sum\limits_{n\geqslant 2}^{} u_n$ converge.

!!! exercice "EXERCICE 6 analyse  "
	=== "Enoncé"  


		Soit $\left( u_{n}\right) _{n\in \mathbb{N}}$ une suite de réels strictement positifs et $l$ un réel positif strictement inférieur à 1.

		1.  Démontrer que si $\underset{n\rightarrow +\infty }{\lim }\dfrac{u_{n+1}}{u_{n}}=l$, alors la série $\displaystyle\sum u_{n}$ converge.

		    **Indication**: écrire, judicieusement, la définition de $\underset{n\rightarrow +\infty }{\lim }\dfrac{u_{n+1}}{u_{n}}=l$, puis majorer, pour $n$ assez grand, $u_{n}$ par le terme général d’une suite géométrique.

		2.  Quelle est la nature de la série $\displaystyle\sum_{n\geqslant1}{} \dfrac{n!}{n^{n}}$?


	=== "Corrigé"  


		1.  Par hypothèse: $\forall \:\varepsilon >0,\:\exists\:N\in\mathbb{N}/\:\forall n\geqslant N,\:|\dfrac{u_{n+1}}{u_n}-l|\leqslant \varepsilon$.(1)
		    Prenons $\varepsilon=\dfrac{1-l}{2}$.
		    Fixons un entier $N$ vérifiant (1).
		    Alors $\forall\:n\in\mathbb{N},\: n\geqslant N\Longrightarrow\:|\dfrac{u_{n+1}}{u_n}-l|\leqslant \dfrac{1-l}{2}$.
		    Et donc, $\forall n\geqslant N,\:\dfrac{u_{n+1}}{u_n}\leqslant \dfrac{1+l}{2}$.
		    On pose $q=\dfrac{1+l}{2}$. On a donc $q\in \left] 0,1\right[$.
		    On a alors $\forall n\geqslant N,\:u_{n+1}\leqslant q u_n$.
		    On en déduit, par récurrence, que $\forall n\geqslant N,\:u_{n}\leqslant q^{n-N} u_N.$
		    Or $\displaystyle\sum\limits_{n\geqslant N}^{}q^{n-N}u_N=u_Nq^{-N}\displaystyle\sum\limits_{n\geqslant N}^{}q^n$ et $\displaystyle\sum\limits_{n\geqslant N}^{}q^n$ converge car $q\in \left] 0,1\right[$.
		    Donc, par critère de majoration des séries à termes positifs, $\displaystyle\sum u_n$ converge.

		2.  On pose : $\forall \:n\in\mathbb{N}^*$, $u_n=\dfrac{n!}{n^n}$.
		    $\forall \:n\in\mathbb{N}^{*}$, $u_n> 0$ et $\forall \:n\in\mathbb{N}^*$, $\dfrac{u_{n+1}}{u_n}=\dfrac{n^n}{(n+1)^{n}}=\mathrm{e}^{-n\ln(1+\dfrac{1}{n})}$.
		    Or $-n\ln(1+\dfrac{1}{n})\underset{+\infty}{\thicksim}-1$ donc $\lim\limits_{n\to +\infty}^{}\dfrac{u_{n+1}}{u_n}=\mathrm{e}^{-1}<1$.
		    Donc $\displaystyle\sum u_n$ converge.

!!! exercice "EXERCICE 7 analyse  "
	=== "Enoncé"  
		1.  Soient $\left( u_{n}\right) _{n\in \mathbb{N}}$ et $\left(v_{n}\right) _{n\in \mathbb{N}}$ deux suites de nombres réels positifs.
		On suppose que $(u_n)_{n\in \mathbb{N}}$ et $(v_n)_{n\in \mathbb{N}}$ sont non nulles à partir d’un certain rang.
		Montrer que:

		$$
		u_{n}\underset{+\infty}{\thicksim} v_{n} \  \Longrightarrow  \ \displaystyle\sum u_{n}\ \text{et }\displaystyle\sum v_{n}\ \text{sont de m\^{e}me nature}.
		$$

		2.  Étudier la convergence de la série $\displaystyle\sum\limits_{n\geqslant 2}^{} \dfrac{\left((-1)^n+\mathrm{i}\right)\ln n \sin \left( \dfrac{1}{n}\right) }{\left(\sqrt{n+3}-1\right)}$.

		**Remarque** : $\mathrm{i}$ désigne le nombre complexe de carré égal à $-1$.


	=== "Corrigé"  


		1.  Par hypothèse, $\exists\:N_0\in\mathbb{N}/\:\:\forall\:n\in\mathbb{N}, n\geqslant N_0\Longrightarrow v_n\neq 0$.
		    Ainsi la suite $\left( \dfrac{u_n}{v_n}\right)$ est définie à partir du rang $N_0$.
		    De plus, on suppose que $u_{n}\underset{+\infty}\thicksim v_{n}$.
		    On en déduit que $\lim\limits_{n\to +\infty}^{}\dfrac{u_n}{v_n}=1$.
		    Alors, $\forall\:\varepsilon>0$, $\exists N\in\mathbb{N}\:\text{tel que}\:N\geqslant N_0$ et $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow\left|\dfrac{u_n}{v_n}-1\right|\leqslant\varepsilon$.(1)
		    Prenons $\varepsilon=\dfrac{1}{2}$. Fixons un entier $N$ vérifiant $(1)$.
		    Ainsi, $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow\left|\dfrac{u_n}{v_n}-1\right|\leqslant\dfrac{1}{2}$.
		    C’est-à-dire, $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow -\dfrac{1}{2}\leqslant\dfrac{u_n}{v_n}-1\leqslant\dfrac{1}{2}$.
		    On en déduit que $\forall\:n\in\mathbb{N},n\geqslant N\Longrightarrow \dfrac{1}{2}\leqslant\dfrac{u_n}{v_n}\leqslant\dfrac{3}{2}$.(\*)

		    **Premier cas**: Si $\displaystyle\sum v_n$ converge
		    D’après (\*), $\forall \:n\geqslant N$, $u_n\leqslant \dfrac{3}{2}v_n$.
		    Donc, par critère de majoration des séries à termes positifs, $\displaystyle\sum u_n$ converge.

		    **Deuxième cas**: Si $\displaystyle\sum v_n$ diverge
		    D’après (\*), $\forall \:n\geqslant N$, $\dfrac{1}{2}v_n\leqslant u_n$.
		    Donc, par critère de minoration des séries à termes positifs, $\sum u_n$ diverge.
		    Par symétrie de la relation d’équivalence, on obtient le résultat.

		2.  On pose $\forall \:n\geqslant 2$, $u_n=\dfrac{\left((-1)^n+\mathrm{i}\right)\ln n \sin \left( \dfrac{1}{n}\right) }{\left(\sqrt{n+3}-1\right)}$.
		    $|u_n|=\dfrac{\sqrt{2}\ln n\sin(\dfrac{1}{n})}{\left( \sqrt{n+3}-1\right) }$.
		    De plus $|u_n|\underset{+\infty}\thicksim\dfrac{\sqrt{2}\ln n}{n^{\frac{3}{2}}}=v_n$
		    On a $n^{\frac{5}{4}}v_n=\dfrac{\sqrt{2}\ln n}{n^{\frac{1}{4}}}$, donc $\lim\limits_{n\to +\infty}^{}n^{\frac{5}{4}}v_n=0$. On en déduit que $\displaystyle\sum v_n$ converge.
		    D’après 1., $\displaystyle\sum\limits_{n\geqslant 2}^{}|u_n|$ converge.
		    Donc $\displaystyle\sum\limits_{n\geqslant 2}^{}u_n$ converge absolument.
		    De plus, la suite $(u_n)_{n\geqslant2}$ est à valeurs dans $\mathbb{C}$, donc $\displaystyle\sum\limits_{n\geqslant 2}^{}u_n$ converge.

!!! exercice "EXERCICE 8 analyse  "
	=== "Enoncé"  


		1.  Soit $\left( u_{n}\right) _{n\in \mathbb{N}}\ $une suite décroissante positive de limite nulle.

		    1.  Démontrer que la série $\displaystyle\sum \left( -1\right) ^{k}u_{k}$ est convergente.

		        **Indication**: on pourra considérer $\left( S_{2n}\right) _{n\in \mathbb{N}}$ et $\left( S_{2n+1}\right) _{n\in \mathbb{N}}$ avec $S_{n}=\displaystyle\sum_{k=0}^{n}\left( -1\right) ^{k}u_{k}$.

		    2.  Donner une majoration de la valeur absolue du reste de la série $\displaystyle\sum \left( -1\right) ^{k}u_{k}$.

		2.  On pose : $\forall \:n\in\mathbb{N}^*$, $\forall\:x\in\mathbb{R}$, $f_n(x)=\dfrac{\left(-1\right)^{n}e^{-nx}}{n}$.

		    1.  Étudier la convergence simple sur $\mathbb{R}$ de la série de fonctions $\displaystyle\sum_{n\geqslant1}^{}f_n$.

		    2.  Étudier la convergence uniforme sur $\left[ 0,+\infty\right[$ de la série de fonctions $\displaystyle\sum_{n\geqslant1}^{}f_n$.


	=== "Corrigé"  


		1.  1.  $S_{2n + 2}  - S_{2n }  = u_{2n + 2}  - u_{2n + 1}  \leqslant 0$, donc $(S_{2n})_{n\in\mathbb{N}}$ est décroissante.
		        De même $S_{2n + 3}  - S_{2n + 1}  \geqslant 0$, donc $(S_{2n+1})_{n\in\mathbb{N}}$ est croissante.
		        De plus $S_{2n}  - S_{2n + 1}  = u_{2n + 1}$ et $\lim\limits_{n\to +\infty}^{}u_{2n + 1}=0$, donc $\lim\limits_{n\to +\infty}^{}(S_{2n}  - S_{2n + 1}) =0$.
		        On en déduit que les suites $(S_{2n} )_{n \in \mathbb{N}}$ et $(S_{2n + 1} )_{n \in \mathbb{N}}$ sont adjacentes. Donc elles convergent et ce vers une même limite.
		        Comme $(S_{2n} )_{n \in \mathbb{N}}$ et $(S_{2n + 1} )_{n \in \mathbb{N}}$ recouvrent l’ensemble des termes de la suite $(S_n)_{n\in\mathbb{N}}$, on en déduit que la suite $(S_n )_{n \in \mathbb{N}}$ converge aussi vers cette limite.
		        Ce qui signifie que la série $\displaystyle\sum {( - 1)^k u_k }$ converge.

		    2.  Le reste $R_n  = \displaystyle\sum\limits_{k = n + 1}^{ + \infty } {( - 1)^k u_k }$ vérifie $\forall\:n\in\mathbb{N}$, $\left| {R_n } \right| \leqslant u_{n + 1}$.

		2.  On pose : $\forall \:x\in\mathbb{R}$, $\forall\:n\in\mathbb{N}^*$, $f_n(x)=\dfrac{\left(-1\right)^{n}e^{-nx}}{n}$.
		    On a alors $\forall\:n\in\mathbb{N}^*$, $f_n(x)=(-1)^nu_n(x)$ avec $u_n(x)=\dfrac{e^{-nx}}{n}$.

		    1.  Soit $x\in\mathbb{R}$.
		        Si $x<0$, alors $\lim\limits_{n\to +\infty}^{}|f_n(x)| =+\infty$, donc $\displaystyle\sum\limits_{n\geqslant1}^{}f_n(x)$ diverge grossièrement.
		        Si $x\geqslant 0$, alors $(u_n(x))_{n \in \mathbb{N}}$ est positive, décroissante et $\lim\limits_{n\to +\infty}^{}u_n(x)=0$.
		        Donc d’après 1.(a), $\sum\limits_{n\geqslant1}^{}f_n(x)$ converge.
		        Donc $\displaystyle\sum\limits_{n\geqslant1}^{}f_n$ converge simplement sur $\left[ 0,+\infty\right[$.

		        **Remarque**: pour $x> 0$, on a aussi convergence absolue de $\sum\limits_{n\geqslant1}^{}f_n(x)$.
		        En effet, pour tout réel $x> 0$, $n^2|f_n(x)|=n\mathrm{e}^{-nx}\underset{n\to +\infty}{\longrightarrow}0$ donc, au voisinage de $+\infty$, $|f_n(x)|=o\left( \dfrac{1}{n^2}\right)$.

		    2.  Comme $\displaystyle\sum\limits_{n\geqslant1}^{}f_n$ converge simplement sur $\left[ 0,+\infty\right[$, on peut poser $\forall \:x\in \left[ 0,+\infty\right[$, $R_n(x)=\displaystyle\sum\limits_{k=n+1}^{+\infty}f_k(x)$.
		        Alors, comme, $\forall \:x\in \left[ 0,+\infty\right[$, $(u_n(x))_{n \in \mathbb{N}}$ est positive, décroissante et $\lim\limits_{n\to +\infty}^{}u_n(x)=0$, on en déduit, d’après 1.(b), que:
		        $\forall \:x\in \left[ 0,+\infty\right[$, $|R_n(x)|\leqslant \dfrac{e^{-(n+1)x}}{n+1}$.
		        Et donc $\forall \:x\in \left[ 0,+\infty\right[$, $|R_n(x)|\leqslant \dfrac{1}{n+1}$. (majoration indépendante de $x$)
		        Et comme $\lim\limits_{n\to +\infty}^{} \dfrac{1}{n+1}=0$, alors $(R_n)$ converge uniformément vers $0$ sur $\left[ 0,+\infty\right[$.
		        C’est-à-dire $\displaystyle\sum\limits_{n\geqslant1}^{}f_n$ converge uniformément sur $\left[ 0,+\infty\right[$.

!!! exercice "EXERCICE 9 analyse  "
	=== "Enoncé"  


		1.  Soit $X$ un ensemble, $\left( g_{n}\right)$ une suite de fonctions de $X$ dans $\mathbb{C}$ et $g$ une fonction de $X$ dans $\mathbb{C}$.
		    Donner la définition de la convergence uniforme sur $X$ de la suite de fonctions $\left(g_n\right)$ vers la fonction $g$.

		2.  On pose $f_{n}(x) =\dfrac{n+2}{n+1}\mathrm{e}^{-n x^{2}}\cos \left( \sqrt{n}x\right)$.

		    1.  Étudier la convergence simple de la suite de fonctions $\left(f_{n}\right)$.

		    2.  La suite de fonctions $\left(f_{n}\right)$ converge-t-elle uniformément sur $\left[ 0,+\infty\right[$ ?

		    3.  Soit $a>0$. La suite de fonctions $\left(f_{n}\right)$ converge-t-elle uniformément sur $[a,+\infty[$ ?

		    4.  La suite de fonctions $\left(f_{n}\right)$ converge-t-elle uniformément sur $]0,+\infty[$?


	=== "Corrigé"  


		1.  Soit $g_n:X\longrightarrow \mathbb{C}$ et $g:X\longrightarrow \mathbb{C}$.
		    Dire que $(g_n)$ converge uniformément vers $g$ sur $X$ signifie que : $$\forall \varepsilon>0,\exists\:N\in\mathbb{N}  /  \forall \:n\in\mathbb{N}, n\geqslant N \Longrightarrow \forall \:x \in X, |g_n(x)-g(x)|\leqslant \varepsilon.$$ Ou encore, $(g_n)$ converge uniformément vers $g$ sur $X$ $\Longleftrightarrow$ $\lim\limits_{n\to +\infty}^{}\left( \sup\limits_{x\in X}^{}|g_n(x)-g(x)|\right) =0$.

		2.  1.  On pose pour tout $x\in\mathbb{R}$, $f_{n}(x) =\dfrac{n+2}{n+1}\mathrm{e}^{-n x^{2}}\cos \left( \sqrt{n}x\right)$.
		        Soit $x\in\mathbb{R}$.
		        Si $x=0$, alors $f_n(0)=\dfrac{n+2}{n+1}$, donc $\lim\limits_{n\to+\infty}^{}f_n(0)=1$.
		        Si $x\neq 0$, alors $\lim\limits_{n\to+\infty}^{}f_n(x)=0$.
		        En effet, $|f_n(x)|\underset{+\infty}{\thicksim}\mathrm{e}^{-nx^2}|\cos \left( \sqrt{n}x\right)|$ et $0\leqslant\mathrm{e}^{-nx^2}|\cos \left( \sqrt{n}x\right)|\leqslant \mathrm{e}^{-nx^2}\underset{n\to +\infty}{\longrightarrow}0$.
		        On en déduit que $(f_n)$ converge simplement sur $\mathbb{R}$ vers la fonction $f$ définie par: $$f(x)=\left\lbrace \begin{array}{lll}
		         0&\:\text{si}\:& x\neq 0\\
		         1&\:\text{si}\:& x=0
		         \end{array}\right.$$

		    2.  Pour tout $n\in\mathbb{N}$, $f_n$ est continue sur $\left[0,+\infty \right[$ et $f$ non continue en $0$ donc $(f_n)$ ne converge pas uniformément vers $f$ sur $\left[0,+\infty \right[$.

		    3.  Soit $a>0$.
		        On a: $\forall \,x\in \left[a,+\infty \right[$, $|f_n(x)-f(x)|=|f_n(x)|\leqslant\dfrac{n+2}{n+1}\mathrm{e}^{-n a^{2}}$ (majoration indépendante de $x$).
		        Par ailleurs, $\lim\limits_{n\to +\infty}^{}\dfrac{n+2}{n+1}\mathrm{e}^{-n a^{2}}=0$ (car $\dfrac{n+2}{n+1}\mathrm{e}^{-n a^{2}}\underset{+\infty}{\thicksim}\mathrm{e}^{-n a^{2}})$.
		        Donc $(f_n)$ converge uniformément vers $f$ sur $\left[a,+\infty \right[$.

		    4.  On remarque que pour tout $n\in\mathbb{N}$, $f_n$ est bornée sur $\left] 0,+\infty\right[$ car pour tout $x\in \left] 0,+\infty\right[$, $|f_n(x)|\leqslant \dfrac{n+2}{n+1}\leqslant 2$.
		        D’autre part, $f$ est bornée sur $\left]  0,+\infty\right[$, donc, pour tout $n\in\mathbb{N}$, $\sup\limits_{x\in \left] 0,+\infty\right[ }^{}|f_n(x)-f(x)|$ existe.
		        On a $|f_n(\dfrac{1}{\sqrt{n}})-f(\dfrac{1}{\sqrt{n}})|=\dfrac{(n+2)\mathrm{e}^{-1}\cos 1}{n+1}$ donc $\lim\limits_{n\to+\infty}^{}|f_n(\dfrac{1}{\sqrt{n}})-f(\dfrac{1}{\sqrt{n}})|=\mathrm{e}^{-1}\cos 1\neq 0$.
		        Or $\sup\limits_{x\in \left] 0,+\infty\right[ }^{}|f_n(x)-f(x)|\geqslant|f_n(\dfrac{1}{\sqrt{n}})-f(\dfrac{1}{\sqrt{n}})|$, donc $\sup\limits_{x\in \left] 0,+\infty\right[ }^{}|f_n(x)-f(x)|\underset{n\to +\infty}{\not\to}0$.
		        Donc $(f_n)$ ne converge pas uniformément vers $f$ sur $\left]0,+\infty \right[$.

!!! exercice "EXERCICE 10 analyse  "
	=== "Enoncé"  


		On pose $f_{n}\left( x\right) =\left( x^{2}+1\right) \dfrac{ne^{x}+xe^{-x}}{n+x}$.

		1.  Démontrer que la suite de fonctions $\left( f_{n}\right)$ converge uniformément sur $[0,1]$.

		2.  Calculer $\underset{n\rightarrow +\infty }{\lim }\displaystyle\int\limits_{0}^{1}\left( x^{2}+1\right) \dfrac{ne^{x}+xe^{-x}}{n+x}\text{d}x$.


	=== "Corrigé"  


		1.  Pour $x \in \left[ {0,1} \right]$, $\lim\limits_{n\to +\infty}^{}f_n (x)=(x^2  + 1){\mathrm{e}}^x$.
		    La suite de fonctions $(f_n )$ converge simplement vers $f:x \mapsto (x^2  + 1){\mathrm{e}}^x$ sur $\left[ {0,1} \right]$.
		    On a $\forall\: x\in \left[ 0,1\right]$, $f_n (x) - f(x) = (x^2  + 1)\dfrac{{x({\mathrm{e}}^{ - x}  - {\mathrm{e}}^x )}}{{n + x}}$, et donc: $\forall x\in \left[ 0,1\right]$, $\left| {f_n (x) - f(x)} \right| \leqslant \dfrac{{2\textrm{e}}}{n}$.
		    Ce majorant indépendant de $x$ tend vers 0 quand $n\to +\infty$, donc la suite de fonctions $(f_n)$ converge uniformément vers $f$ sur $\left[ {0,1} \right]$.

		2.  Par convergence uniforme sur le segment $\left[ 0,1\right]$ de cette suite de fonctions continues sur $\left[ 0,1\right]$, on peut intervertir limite et intégrale.
		    On a donc $\mathop {\lim }\limits_{n \to  + \infty } \displaystyle\int_0^1 {(x^2  + 1)\dfrac{{n{\mathrm{e}}^x  + x{\mathrm{e}}^{ - x} }}{{n + x}}\,{\mathrm{d}}x}  = \displaystyle\int_0^1 {(x^2  + 1){\mathrm{e}}^x \,{\mathrm{d}}x}$.
		    Puis, en effectuant deux intégrations par parties, on trouve $\displaystyle\int_0^1 {(x^2  + 1){\mathrm{e}}^x \,{\mathrm{d}}x}  = 2{\mathrm{e}} - 3$.

!!! exercice "EXERCICE 11 analyse  "
	=== "Enoncé"  


		1.  Soit $X$ une partie de $\mathbb{R}$, $\left( f_{n}\right)$ une suite de fonctions de $X$ dans $\mathbb{R}$ convergeant simplement vers une fonction $f$.
		    On suppose qu’il existe une suite $\left( x_{n}\right)_{n\in \mathbb{N}}$ d’éléments de $X$ telle que la suite $\left( f_{n}(x_{n})-f\left( x_{n}\right) \right) _{n\in \mathbb{N}}$ ne tende pas vers $0$.

		    Démontrer que la suite de fonctions $\left( f_{n}\right)$ ne converge pas uniformément vers $f$ sur $X$.

		2.  Pour tout $x\in\mathbb{R}$, on pose $f_{n}(x) =\dfrac{\sin \left( nx\right) }{1+n^{2}x^{2}}$.

		    1.  Étudier la convergence simple de la suite $\left( f_{n}\right)$.

		    2.  Étudier la convergence uniforme de la suite $\left( f_{n}\right)$ sur $[a,+\infty[$ (avec $a>0$), puis sur $]0,+\infty[$.


	=== "Corrigé"  


		1.  Par contraposée :
		    si $(f_n )$ converge uniformément vers $f$ alors :
		    il existe un entier $N$ tel que $\forall n\geqslant N$, $\left\| {f_n  - f} \right\|_\infty   = \mathop {\sup }\limits_{x \in X} \left| {f_n (x) - f(x)} \right|$ existe et $\lim\limits_{n\to+\infty}^{}\left\| {f_n  - f} \right\|_\infty   = 0$.
		    Or, $\forall\:n\in\mathbb{N}$, $x_n\in X$ donc $\forall\:n\in\mathbb{N}$, $n\geqslant N \Longrightarrow\left| {f_n (x_n ) - f(x_n )} \right| \leqslant \left\| {f_n  - f} \right\|_\infty$.
		    Or $\lim\limits_{n\to+\infty}^{}\left\| {f_n  - f} \right\|_\infty   = 0$.
		    Donc $\lim\limits_{n\to+\infty}^{}\left| {f_n (x_n ) - f(x_n )} \right|=0$.
		    C’est-à-dire la suite $( {f_n (x_n ) - f(x_n )})_{n\in\mathbb{N}}$ converge vers $0$.

		2.  1.  Soit $x\in\mathbb{R}$.
		        Si $x=0$, alors $f_n(0)=0$.
		        Si $x \neq 0$, alors $\lim\limits_{n\to+\infty}^{}f_n (x)=0$ car $|f_n(x)|\leqslant\dfrac{1}{n^2x^2}$.
		        Donc la suite $(f_n )$ converge simplement vers la fonction nulle sur $\mathbb{R}$.

		    2.  Soit $a>0$.
		        $\forall\:x\in \left[ a,+\infty\right[$, $\left| {f_n (x)} -f(x)\right|=|f_n(x)| \leqslant \dfrac{1}{{1 + n^2a^2  }}$.
		        Cette majoration est indépendante de $x$ et $\lim\limits_{n\to+\infty}^{}\dfrac{1}{{1 + n^2a^2  }}=0$.
		        On en déduit que la suite de fonctions $(f_n )$ converge uniformément vers la fonction nulle sur $\left[ {a, + \infty } \right[$ .

		        On pose, $\forall \:n\in\mathbb{N}^*$, $x_n  = \dfrac{\pi}{2n}$.
		        On a $\forall \:n\in\mathbb{N}^*$, $x_n\in \left]  0,+\infty\right[$ et $|f_n (x_n )-f(x_n)| = \dfrac{1}{{1 + \dfrac{{\pi ^2 }}{4}}}$ qui ne tend pas vers 0 quand $n\rightarrow +\infty$.
		        On en déduit, d’après 1., que la suite de fonctions $(f_n )$ ne converge pas uniformément sur $\left] {0, + \infty } \right[$.

!!! exercice "EXERCICE 12analyse  "
	=== "Enoncé"  


		1.  Soit $(f_n)$ une suite de fonctions de $[a,b]$ dans $\mathbb{R}$.

		    On suppose que la suite de fonctions $(f_n)$ converge uniformément sur $[a,b]$ vers une fonction $f$, et que, pour tout $n\in\mathbb{N}$, $f_n$ est continue en $x_0$, avec $x_0\in[a,b]$.

		    Démontrer que $f$ est continue en $x_0$.

		2.  On pose : $\forall\:n\in\mathbb{N}^*$, $\forall\:x\in[0;1]$, $g_n(x)=x^n$.
		    La suite de fonctions $(g_n)_{n\in\mathbb{N}^*}$ converge-t-elle uniformément sur $[0;1]$?


	=== "Corrigé"  


		1.  Soit $x_0\in \left[ a,b\right]$.
		    Prouvons que $f$ est continue en $x_0$.
		    Soit $\varepsilon  > 0$.
		    Par convergence uniforme, il existe un entier $N$ tel que $\forall \: n \in \mathbb{N}$, $n \geqslant N \Longrightarrow \left( {\forall x \in \left[ {a,b} \right],\left| {f(x) - f_n (x)} \right| \leqslant \varepsilon } \right)$.
		    En particulier pour $n = N$, on a $\forall x \in \left[ {a,b} \right],\left| {f(x) - f_N (x)} \right| \leqslant \varepsilon$.(\*)
		    Or la fonction $f_N$ est continue en $x_0$ donc $\exists\:\alpha  > 0$ tel que :
		    $\forall x \in \left[ {a,b} \right],\left| {x - x_0 } \right| \leqslant \alpha  \Rightarrow \left| {f_N (x) - f_N (x_0 )} \right| \leqslant \varepsilon$.(\*\*)
		    D’après l’inégalité triangulaire, $\forall\:x\in \left[ a,b\right]$, $\left| {f(x) - f(x_0 )} \right| \leqslant \left| {f(x) - f_N (x)} \right| + \left| {f_N (x) - f_N (x_0 )} \right| + \left| {f_N (x_0 ) - f(x_0 )} \right|$.
		    Alors d’après (\*) et (\*\*),
		    $\forall x \in \left[ {a,b} \right],\left| {x - x_0 } \right| \leqslant \alpha  \Rightarrow \left| {f(x) - f(x_0 )} \right| \leqslant 3\varepsilon$.
		    On en déduit que $f$ est continue en $x_0$.

		2.  La suite $(g_n)_{n\in\mathbb{N}^*}$ converge simplement sur $\left[ {0,1} \right]$ vers la fonction $g:x \mapsto \left\{ {
		    \begin{array}{ll}
		     0 & {{\text{si }}x \in \left[ {0,1} \right[}  \\
		     1 & {{\text{si }}x = 1}  \\
		    \end{array}
		    } \right.$
		    $\forall \:n\in\mathbb{N}^*$, $g_n$ est continue en 1 alors que $g$ est discontinue en 1.
		    D’après la question précédente, on en déduit que $(g_n)_{n\in\mathbb{N}^*}$ ne converge pas uniformément vers $g$ sur $\left[ {0,1} \right]$.

!!! exercice "EXERCICE 13analyse  "
	=== "Enoncé"  


		1.  Rappeler, oralement, la définition, par les suites de vecteurs, d’une partie compacte d’un espace vectoriel normé.

		2.  Démontrer qu’une partie compacte d’un espace vectoriel normé est une partie fermée de cet espace.

		3.  Démontrer qu’une partie compacte d’un espace vectoriel normé est une partie bornée de cet espace.

		    **Indication** : On pourra raisonner par l’absurde.

		4.  On se place su $E=\mathbb{R}[X]$ muni de la norme $||\:||_{1}$ définie pour tout polynôme $P=a_0+a_1X+....+a_nX^n$ de $E$ par : $||P||_{1}=\displaystyle\sum_{i=0}^{n}|a_{i}|$.

		    1.  Justifier que $S(0,1)=\left\lbrace  P\in\mathbb{R}[X]\:/\: ||P||_{1}=1\right\rbrace$ est une partie fermée et bornée de $E$.

		    2.  Calculer $||X^n-X^m||_{1}$ pour $m$ et $n$ entiers naturels distincts.
		        $S(0,1)$ est-elle une partie compacte de $E$? Justifier.


	=== "Corrigé"  


		1.  Une partie $A$ d’un espace vectoriel normé $(E,||\:||)$ est une partie compacte si et seulement si pour toute suite $(x_n)$ à valeurs dans $A$ on peut extraire une sous-suite qui converge dans $A$.
		    C’est-à-dire $A$ est une partie compacte si et seulement pour toute suite $(x_n)$ à valeurs dans $A$ il existe $\varphi:\mathbb{N}\longrightarrow \mathbb{N}$ strictement croissante telle que $\left( x_{\varphi(n)}\right)$ converge vers $l\in A$.

		    **Remarque**: $\varphi:\mathbb{N}\longrightarrow \mathbb{N}$ étant strictement croissante, on a, par récurrence immédiate, $\forall n\in\mathbb{N},\: \varphi(n)\geqslant n$.

		2.  Soit $(E,||\:||)$ un espace vectoriel normé.
		    Soit $A$ une partie compacte de $E$.
		    Montrons que $A$ est une partie fermée de $E$.
		    C’est-à-dire montrons que toute suite à valeurs dans $A$ qui converge, converge dans $A$.
		    Soit $(x_n)$ une suite à valeurs dans $A$ telle que $(x_n)$ converge vers $l$.
		    $A$ est une partie compacte donc il existe $\varphi:\mathbb{N}\longrightarrow \mathbb{N}$ strictement croissante telle que $\left( x_{\varphi(n)}\right)$ converge vers $l'\in A$. Or, $(x_{n})$ converge vers $l$ donc $\left( x_{\varphi(n)}\right)$ converge vers $l$ ( sous-suite de $(x_{n}))$.
		    Par unicité de la limite, $l'=l$.
		    Or, $l'\in A$ ,donc $l\in A$.

		3.  Soit $(E,||\:||)$ un espace vectoriel normé.

		    **Rappel** : Soit $B$ une partie de $E$.
		    $B$ est bornée si et seulement si $\exists \:M\in\mathbb{R}\:/\: \forall  x \in B, ||x||\leqslant M$.

		    Soit $A$ une partie compacte de $E$.
		    Montrons que $A$ est une partie bornée de $E$.
		    Raisonnons par l’absurde.
		    Supposons que $A$ soit non bornée.
		    C’est-à-dire, $\forall M\in \mathbb{R}$, $\exists \:x\in  A\:/\: ||x||> M$.
		    Donc, $\forall n\in \mathbb{N},\:\exists x_n \in A\:/\: ||x_n||>n$ $(*)$
		    $(x_n)$ est une suite à valeurs dans $A$ et $A$ est une partie compacte de $E$ donc il existe $\varphi:\mathbb{N}\longrightarrow \mathbb{N}$ strictement croissante telle que $\left( x_{\varphi(n)}\right)$ converge vers $l\in A$.
		    Donc, d’après $(*)$, $\forall n \in\mathbb{N}$,$||x_{\varphi(n)}|| >\varphi(n)$.
		    Or $\forall n\in\mathbb{N},\: \varphi(n)\geqslant n$.
		    Donc, $\forall n \in\mathbb{N}$, $||x_{\varphi(n)}|| > n$.
		    Donc, $\displaystyle\lim_{n \rightarrow +\infty}||x_{\varphi(n)}||=+\infty$.
		    Absurde car $(x_{\varphi(n)})$ converge donc $(x_{\varphi(n)})$ est bornée.

		4.  Posons $S=S(0,1)$.

		    1.  $\forall x\in S$,$||x||=1$ donc $S$ est bornée.
		        Soit $f:\begin{array}{lll}
		           E&\longrightarrow&\mathbb{R}\\
		           x&\longmapsto &||x||
		           \end{array}$
		        $f$ est continue sur $E$.
		        Or, $S=f^{-1}({1})$ et $\left\lbrace 1\right\rbrace$ est un fermé de $\mathbb{R}$ donc $S$ est une partie fermée de $E$, en tant qu’image réciproque par une application continue d’un fermé.

		    2.  Soit $(m,n)\in\mathbb{N}^2$ avec $m\neq n$.
		        $||X^n-X^m||_{1}=2$.
		        Supposons que $S$ soit une partie compacte de $E$.
		        $\forall n\in \mathbb{N}$, $||X^n||_{1}=1$, donc $(X_n)$ est une suite à valeurs dans $S$.
		        Or, $S$ est une partie compacte de $E$, donc il existe $\varphi:\mathbb{N}\longrightarrow \mathbb{N}$ strictement croissante telle que $\left( X^{\varphi(n)}\right)$ converge vers $l \in S$.
		        Alors $\displaystyle \lim_{n\rightarrow +\infty}||X^{\varphi(n)}-X^{\varphi(n+1)}||_{1}=||l-l||=0$.
		        Contredit le fait que: $\forall n\in\mathbb{N}$, $||X^{\varphi(n)}-X^{\varphi(n+1)}||_{1}=2$.
		        Donc $S$ est non compact.

!!! exercice "EXERCICE 14 analyse  "
	=== "Enoncé"  


		1.  Soit $a$ et $b$ deux réels donnés avec $a<b$.
		    Soit $\left( f_{n}\right)$ une suite de fonctions continues sur $[a,b],$ à valeurs réelles.
		    Démontrer que si la suite $\left( f_{n}\right)$ converge uniformément sur $\left[ a,b\right]$ vers $f$, alors la suite $\left( \displaystyle\int_{a}^{b}f_{n}\left( x\right)\text{d}x\right)_{n\in \mathbb{N}}$ converge vers $\displaystyle\int_{a}^{b}f\left(x\right) \text{d}x$.

		2.  Justifier comment ce résultat peut être utilisé dans le cas des séries de fonctions.

		3.  Démontrer que $\displaystyle\int_{0}^{\frac{1}{2}}\left( \displaystyle\sum_{n=0}^{+\infty}x^{n}\right) \text{d}x=\displaystyle\sum\limits_{n=1}^{+\infty }\dfrac{1}{n2^{n}}~.$


	=== "Corrigé"  


		1.  Comme la suite $(f_n)$ converge uniformément sur $\left[a,b \right]$ vers $f$, et que, $\forall\:n\in\mathbb{N}$, $f_n$ est continue sur $\left[ a,b\right]$, alors $f$ est continue sur $\left[ a,b\right]$.
		    Ainsi, $\forall n\in\mathbb{N}$, $f_n-f$ est continue sur le segment $\left[ a,b\right]$.
		    On pose alors, $\forall n\in\mathbb{N}$, $\left\| {f_n  - f} \right\|_\infty   = \mathop {\sup }\limits_{x \in \left[ {a,b} \right]} \left| {f_n (x) - f(x)} \right|$.
		    On a $\left| {\displaystyle\int_a^b {f_n (x)\,{\mathrm{d}}x}  - \int_a^b {f(x)\,{\mathrm{d}}x} } \right| =\left|\displaystyle \int_a^b\left( f_n(x)-f(x)\right)\mathrm{d}x\right| \leqslant \displaystyle\int_a^b|f_n(x)-f(x)|\mathrm{d}x\leqslant (b - a)\left\| {f_n  - f} \right\|_\infty$.(\*)
		    Or $(f_n)$ converge uniformément vers $f$ sur $\left[ a,b\right]$, donc $\lim\limits_{n\to+\infty}^{}\left\| {f_n  - f} \right\|_\infty =0$.
		    Donc d’après (\*), $\lim\limits_{n\to+\infty}^{}\displaystyle\int_a^b f_n (x)\,{\mathrm{d}}x= \int_a^b {f(x)\,{\mathrm{d}}x}$.

		2.  On suppose que $\forall n\in\mathbb{N}$, $f_n$ est continue sur $\left[ a,b\right]$ et $\displaystyle\sum f_n$ converge uniformément sur $\left[ a,b\right]$.
		    On pose $S_n=\displaystyle\sum\limits_{k=0}^{n}f_k$.
		    $\displaystyle\sum f_n$ converge uniformément sur $\left[ a,b\right]$, donc converge simplement sur $\left[ a,b\right]$.
		    On pose alors, également, $\forall x\in \left[a,b \right]$, $S(x)=\displaystyle\sum\limits_{k=0}^{+\infty}f_k(x)$.
		    $\displaystyle\sum f_n$ converge uniformément sur $\left[ a,b\right]$ signifie que $(S_n)$ converge uniformément sur $\left[ a,b\right]$ vers $S$.
		    De plus, $\forall\:n\in\mathbb{N}$, $S_n$ est continue sur $\left[ a,b\right]$, car $S_n$ est une somme finie de fonctions continues.
		    On en déduit que $S$ est continue sur $\left[ a,b\right]$.
		    Et d’après 1., $\lim\limits_{n\to+\infty}^{}\displaystyle\int_a^b S_n (x)\,{\mathrm{d}}x= \displaystyle\int_a^b {S(x)\,{\mathrm{d}}x}$.
		    Or $\displaystyle\int_{a}^{b}S_n(x)\mathrm{d}x=\displaystyle\int_{a}^{b}\displaystyle\sum\limits_{k=0}^{n}f_k(x)\mathrm{d}x=\displaystyle\sum\limits_{k=0}^{n}\displaystyle\int_{a}^{b}f_k(x)\mathrm{dx}$ car il s’agit d’une somme finie.
		    Donc $\lim\limits_{n\to+\infty}^{}\displaystyle\sum\limits_{k=0}^{n}\displaystyle\int_a^b f_k (x)\,{\mathrm{d}}x= \displaystyle\int_a^b {S(x)\,{\mathrm{d}}x}$.
		    Ou encore $\lim\limits_{n\to+\infty}^{}\displaystyle\sum\limits_{k=0}^{n}\displaystyle\int_a^b f_k (x)\,{\mathrm{d}}x= \displaystyle\int_a^b\displaystyle\sum\limits_{k=0}^{+\infty}f_k(x) {\,{\mathrm{d}}x}$.
		    Ce qui signifie que $\displaystyle\sum\limits_{}^{}\displaystyle\int_a^b f_k (x)\,{\mathrm{d}}x$ converge et $\displaystyle\sum\limits_{k=0}^{+\infty}\displaystyle\int_a^b f_k (x)\,{\mathrm{d}}x= \displaystyle\int_a^b\displaystyle\sum\limits_{k=0}^{+\infty}f_k(x) {\,{\mathrm{d}}x}$.  

		    **Bilan**: La convergence uniforme de la série de fonctions $\displaystyle\sum f_n$ où les $f_n$ sont continues sur $\left[ {a,b} \right]$ permet d’ intégrer terme à terme, c’est-à-dire: $\displaystyle\int_{a}^{b} {\displaystyle\sum\limits_{n = 0}^{ + \infty } {f_n (x)\,{\mathrm{d}}x}  = \displaystyle\sum\limits_{n = 0}^{ + \infty } {\displaystyle\int_a^b {f_n (x)\,{\mathrm{d}}x} } }$.

		3.  La série entière $\displaystyle\sum {x^n }$ est de rayon de convergence $R = 1$ donc cette série de fonctions converge normalement et donc uniformément sur le compact $\left[ {0,\dfrac{1}{2}} \right] \subset \left] { - 1,1} \right[$.
		    De plus, $\forall\: n\in\mathbb{N}$, $x\longmapsto x^n$ est continue sur $\left[0,\dfrac{1}{2} \right]$.
		    On en déduit alors, en utilisant 2., que: $\displaystyle\int_0^{\frac{1}{2}} \left( {\displaystyle\sum\limits_{n = 0}^{ + \infty } {x^n } } \right) \,{\mathrm{d}}x =\displaystyle\sum\limits_{n=0}^{+\infty}\displaystyle\int_{0}^{\frac{1}{2}} x^n\mathrm{d}x=\displaystyle\sum\limits_{n = 0}^{ + \infty } {\dfrac{1}{{n + 1}}\dfrac{1}{{2^{n + 1} }}}
		    =\displaystyle\sum\limits_{n = 1}^{ + \infty } {\dfrac{1}{{n }}\dfrac{1}{{2^{n } }}}.$

!!! exercice "EXERCICE 15analyse  "
	=== "Enoncé"  


		Soit $X$ une partie de $\mathbb{R}$ ou $\mathbb{C}$ .

		1.  Soit $\displaystyle\sum f_n$ une série de fonctions définies sur $X$ à valeurs dans $\mathbb{R}$ ou $\mathbb{C}$.
		    Rappeler la définition de la convergence normale de $\displaystyle\sum f_n$ sur $X$, puis celle de la convergence uniforme de $\displaystyle\sum f_n$ sur $X$.

		2.  Démontrer que toute série de fonctions, à valeurs dans $\mathbb{R}$ ou $\mathbb{C}$, normalement convergente sur $X$ est uniformément convergente sur $X$.

		3.  La série de fonctions $\displaystyle\sum \dfrac{n^{2}}{n!}z^{n}$ est-elle uniformément convergente sur le disque fermé de centre $0$ et de rayon $R\in \mathbb{R}_{+}^{\ast}$?


	=== "Corrigé"  


		1.  On suppose que $\forall \:n\in\mathbb{N}$, $f_n$ est bornée sur $X$.
		    On pose alors $\forall \:n\in\mathbb{N}$, $\left\| {f_n } \right\|_\infty=\sup\limits_{t\in X}^{} |f_n(t)|$.
		    $\displaystyle\sum f_n$ converge normalement sur $X$ $\Longleftrightarrow$ $\displaystyle\sum \left\| {f_n } \right\|_\infty$ converge.
		    On pose $\forall \:n\in\mathbb{N}$, $S_n=\displaystyle\sum\limits_{k=0}^{n}f_k$.
		    $\displaystyle\sum f_n$ converge uniformément sur $X$ $\Longleftrightarrow$ la suite de fonctions $(S_n)$ converge uniformément sur $X$.

		2.  On suppose que $\displaystyle\sum {f_n }$ converge normalement sur $X$.
		    Les fonctions $f_n$ sont donc bornées sur $X$ et la série numérique $\displaystyle\sum {\left\| {f_n } \right\|_\infty  } \text{ converge}$.
		    Or, $\forall\:x \in X$, $\left| {f_n (x)} \right| \leqslant \left\| {f_n } \right\|_\infty$.
		    Donc, par comparaison des séries à termes positifs, la série $\displaystyle\sum {f_n (x)}$ est absolument convergente et donc convergente, puisque les fonctions $f_n$ sont à valeurs dans $\mathbb{R}$ ou $\mathbb{C}$.
		    Ainsi la série de fonctions $\displaystyle\sum {f_n }$ converge simplement sur $X$.
		    On peut donc poser $\forall \:x\in X$, $\forall\:n\in\mathbb{N}$, $R_n(x)=\displaystyle\sum\limits_{k=n+1}^{+\infty}f_k(x)$.
		    $\forall\:x\in X$, $\forall\:n\in\mathbb{N}$, $\forall\:N\in\mathbb{N}$, $N\geqslant n+1\Longrightarrow \left| {\displaystyle\sum\limits_{k = n + 1}^{N } {f_k (x)} } \right| \leqslant \displaystyle\sum\limits_{k = n + 1}^{ N } {\left| {f_k (x)} \right|}  \leqslant \displaystyle\sum\limits_{k = n + 1}^{N} {\left\| {f_k } \right\|_\infty  }$.
		    Alors, en faisant tendre $N$ vers $+\infty$, on obtient:
		    $\forall \:x \in X$, $|R_n(x)|=\left| {\displaystyle\sum\limits_{k = n + 1}^{ + \infty } {f_k (x)} } \right| \leqslant \displaystyle\sum\limits_{k = n + 1}^{ + \infty } {\left| {f_k (x)} \right|}  \leqslant \displaystyle\sum\limits_{k = n + 1}^{ + \infty } {\left\| {f_k } \right\|_\infty  }$. (majoration indépendante de $x$)
		    Or $\displaystyle\sum {f_n }$ converge normalement sur $X$ donc $\lim\limits_{n\to +\infty}^{}\displaystyle\sum\limits_{k = n + 1}^{ + \infty } {\left\| {f_k } \right\|_\infty  }=0$.
		    On en déduit alors que la suite de fonctions $(R_n)$ converge uniformément vers 0 sur $X$.
		    Comme $R_n=S-S_n$, la suite $(S_n)$ converge uniformément vers $S$ sur $X$.
		    C’est-à-dire $\displaystyle\sum f_n$ converge uniformément sur $X$.

		3.  On pose, $\forall \:n\in \mathbb{N}$, $a_n=\dfrac{n^2}{n!}$.
		    $\forall \:n\in \mathbb{N}^*$, $\dfrac{a_{n+1}}{a_n}=\dfrac{n+1}{n^2}$.
		    Donc $\lim\limits_{n\to +\infty}^{}\dfrac{a_{n+1}}{a_n}=0$.
		    On en déduit que série entière $\displaystyle\sum {\dfrac{{n^2 }}{{n!}}z^n }$ a un rayon de convergence égal à $+ \infty$.
		    Cette série entière converge donc normalement sur tout compact de $\mathbb{C}$.
		    En particulier, cette série entière converge normalement et donc uniformément, d’après 2., sur tout disque de centre $O$ et de rayon $R$.

!!! exercice "EXERCICE 16 analyse  "
	=== "Enoncé"  


		On considère la série de fonctions de terme général $u_{n}$ définie par:
		$$
		\forall n\in \mathbb{N}^{*},\ \forall x\in \lbrack 0,1], \ \ u_{n}\left(x\right) =\ln \left( 1+\dfrac{x}{n}\right) -\dfrac{x}{n}~.
		$$

		On pose, lorsque la série converge, $S(x)=\displaystyle\sum\limits_{n=1}^{+\infty }\left[ \ \ln \left( 1+\dfrac{x}{n}\right) -\dfrac{x}{n}\right]$.

		1.  Démontrer que $S$ est dérivable sur $[0,1]$.

		2.  Calculer $S'(1)$.


	=== "Corrigé"  


		1.  Soit $x \in \left[ {0,1} \right]$.
		    Si $x=0$, $u_n(0)=0$ et donc $\displaystyle\sum u_n(0)$ converge.
		    Si $x\neq 0$, comme au voisinage de $+ \infty$, $u_n (x) =  - \dfrac{{x^2 }}{2{n^2 }} + o\left( {\dfrac{1}{{n^2 }}} \right)$, alors $|u_n(x)|\underset{+\infty}{\thicksim}\dfrac{x^2 }{2n^2 }$.
		    Or $\displaystyle\sum\limits_{n\geqslant 1}^{}\dfrac{1}{n^2}$ converge donc, par critère de comparaison des séries à termes positifs, $\displaystyle\sum u_n(x)$ converge absolument, donc converge.
		    On en déduit que la série des fonctions $u_n$ converge simplement sur $\left[ {0,1} \right]$.
		    La fonction $S$ est donc définie sur $\left[ {0,1} \right]$.
		    $\forall \:n\in\mathbb{N}^*$, $u_n$ est de classe $\mathcal{C}^1$ sur $\left[ {0,1} \right]$ et $\forall \:x\in\left[ {0,1} \right]$, $u'_n (x) = \dfrac{1}{{x + n}} - \dfrac{1}{n} = \dfrac{{ - x}}{{n(x + n)}}$.
		    Donc $\forall\:n\in\mathbb{N}^*$, $\forall \:x\in\left[ {0,1} \right]$, $|u_n'(x)|\leqslant \dfrac{1}{n^2}$.
		    On en déduit que $\left\| {u'_n } \right\|_\infty   = \mathop {\sup }\limits_{x \in \left[ {0,1} \right]} \left| {u'_n (x)} \right| \leqslant \dfrac{1}{{n^2 }}$.
		    Or $\displaystyle\sum\limits_{n\geqslant 1}^{}\dfrac{1}{n^2}$ converge.
		    Donc $\displaystyle\sum\limits_{n\geqslant 1}^{} u'_n$ converge normalement, donc uniformément sur $\left[ {0,1} \right]$.
		    On peut alors affirmer que la fonction $S$ est de classe $\mathcal{C}^1$. Elle est donc dérivable sur $\left[ {0,1} \right]$.
		    Et on a: $\forall\:x\in \left[0;1 \right]$, $S'(x)=\displaystyle\sum_{n=1}^{+\infty}u_n'(x)$.

		2.  En vertu de ce qui précède, $S'(1) = \displaystyle\sum\limits_{n = 1}^{ + \infty } {u'_n (1)}  =\displaystyle\sum\limits_{n = 1}^{ + \infty } {\left( {\dfrac{1}{{n + 1}} - \dfrac{1}{n}} \right)}$.
		    Or $\displaystyle\sum\limits_{n = 1}^N {\left( {\dfrac{1}{{n + 1}} - \dfrac{1}{n}} \right)}  = \dfrac{1}{{N + 1}} - 1\xrightarrow[{N \to  + \infty }]{} -1$.
		    Donc $S'(1) =-1$.

!!! exercice "EXERCICE 17 analyse"
	=== "Enoncé"  
		Soit $A\subset \mathbb{C}$ et $\left( f_{n}\right)$ une suite de fonctions de $A$ dans $\mathbb{C}$.

		1) Démontrer l'implication:  

		$$
		\left(\text{la s\'{e}rie de fonctions }\displaystyle\sum f_{n}\text{ converge uniform\'{e}ment sur $A$}\right)
		$$

		$$
		\Downarrow
		$$

		$$
		\left( \text{la suite de fonctions\ }\left(f_{n}\right) \text{ converge uniform\'{e}ment vers 0 sur $A$}\right)
		$$

		2) On pose: $\forall\:n\in\mathbb{N}$, $\forall\:x\in\left[ 0;+\infty\right[$, $f_n(x)=nx^2\mathrm{e}^{-x\sqrt{n}}$.
		Prouver que $\displaystyle\sum f_n$ converge simplement sur $\left[ 0;+\infty\right[$.
		$\displaystyle\sum f_n$ converge-t-elle uniformément sur $\left[ 0;+\infty\right[$? Justifier.  
	=== "Corrigé"  
		1)  On suppose que $\displaystyle\sum f_n$ converge uniformément sur $A$.
		On en déduit que $\displaystyle\sum f_n$ converge simplement sur $A$.
		On pose alors, $\forall\:x\in A$, $S(x)=\displaystyle\sum\limits_{k=0}^{+\infty}f_k(x)$ et $\forall\:n\in\mathbb{N}$, $S_n(x)=\displaystyle\sum\limits_{k=0}^{n}f_k(x)$.
		$\displaystyle\sum f_n$ converge uniformément sur $A$, c’est-à-dire $(S_n)$ converge uniformément vers $S$ sur $A$, c’est-à-dire $\lim\limits_{n\to+\infty}^{}||S_n-S||_{\infty}=0$, avec $||S_n-S||_{\infty}=\sup\limits_{x\in A}^{}|S_n(x)-S(x)|$.
		On a $\forall\:n\in\mathbb{N}^*$, $\forall \:x\in A$, $|f_n(x)|=|S_n(x)-S_{n-1}(x)|\leqslant|S_n(x)-S(x)|+|S(x)-S_{n-1}(x)|$.
		Donc $\forall\:n\in\mathbb{N}^*$, $\forall \:x\in A$,$|f_n(x)|\leqslant ||S_n-S||_{\infty} +||S_{n-1}-S||_{\infty}$ (majoration indépendante de $x$).
		Or $\lim\limits_{n\to+\infty}^{}||S_n-S||_{\infty}=0$, donc $\lim\limits_{n\to+\infty}^{}\left( ||S_n-S||_{\infty}+||S_{n-1}-S||_{\infty}\right) =0$.
		Donc $(f_n)$ converge uniformément vers 0 sur $A$.  
		2) On pose: $\forall\:n\in\mathbb{N}$, $\forall\:x\in\left[ 0;+\infty\right[$, $f_n(x)=nx^2\mathrm{e}^{-x\sqrt{n}}$.
		Soit $x\in \left[ 0;+\infty\right[$.
		Si $x=0$:
		$\forall\:n\in\mathbb{N}$, $f_n(0)=0$ donc $\displaystyle\sum f_n(0)$ converge.
		Si $x\neq 0$:
		$\lim\limits_{n\to +\infty}^{}n^2f_n(x)=0$, donc au voisinage de $+\infty$, $f_n(x)=o\left(\dfrac{1}{n^2} \right)$.
		Or $\displaystyle\sum\limits_{n\geqslant 1}^{} \dfrac{1}{n^2}$ converge absolument donc, par critère de domination, $\displaystyle\sum f_n(x)$ converge absolument.
		On en déduit que $\displaystyle\sum f_n$ converge simplement sur $\left[0;+\infty \right[$.
		$\forall\:n\in\mathbb{N}^*$, $f_n$ est continue sur $\left[0;+\infty \right[$ et $\lim\limits_{x\to +\infty}^{}f_n(x)=0$, donc $f_n$ est bornée sur $\left[0;+\infty \right[$.
		Comme $f_0$ est bornée ($f_0=0$), on en déduit que $\forall\:n\in\mathbb{N}$, $f_n$ est bornée.
		De plus, la suite de fonctions $(f_n)$ converge simplement vers la fonction nulle.
		En effet, si $x=0$ alors $f_n(0)=0$ et si $x\neq 0$, $\lim\limits_{n\to +\infty}^{}f_n(x)=0$.
		On a $\forall\:n\in\mathbb{N}^*$, $f_n\left( \dfrac{1}{\sqrt{n}}\right) =\mathrm{e}^{-1}$.
		Or, $\forall\:n\in\mathbb{N}^*$, $f_n\left( \dfrac{1}{\sqrt{n}}\right) =|f_n\left( \dfrac{1}{\sqrt{n}}\right) |\leqslant \underset{t\in \left[0;+\infty \right[ }{\sup}|f_n(t)|$; donc $\underset{t\in \left[0;+\infty \right[ }{\sup}|f_n(t)|\geqslant \mathrm{e}^{-1}$.
		Ainsi, $\underset{t\in \left[0;+\infty \right[ }{\sup}|f_n(t)|\underset{n\to +\infty}{\nrightarrow}0$.
		On en déduit que $(f_n)$ ne converge pas uniformément vers la fonction nulle sur $\left[0;+\infty \right[$.
		Donc, d’après 1., $\displaystyle\sum f_n$ ne converge pas uniformément sur $\left[0;+\infty \right[$.


!!! exercice "EXERCICE 18 analyse  "
	=== "Enoncé"  


		On pose: $\forall\:n\in\mathbb{N}^*$, $\forall\:x\in\mathbb{R}$, $u_n (x) =\dfrac{ ( - 1)^n x^n } { n}$.
		On considère la série de fonctions $\displaystyle\sum\limits_{n\geqslant1}^{}u_n$.

		1.  Étudier la convergence simple de cette série.

		    On note $D$ l’ensemble des $x$ où cette série converge et $S(x)$ la somme de cette série pour $x\in D$.

		2.  1.  Étudier la convergence normale, puis la convergence uniforme de cette série sur $D$.

		    2.  La fonction $S$ est-elle continue sur $D$?


	=== "Corrigé"  


		1.  La série de fonctions étudiée est une série entière de rayon de convergence $R = 1$.
		    En $x = 1$, il y a convergence par le critère spécial des séries alternées.
		    En $x =  - 1$, la série diverge (série harmonique).
		    On a donc $D = \left] { - 1,1} \right]$.

		2.  1.  $\forall \:x\in D$, $u_n (x) =\dfrac{ ( - 1)^n x^n } { n}$.
		        $\left\| {u_n } \right\|_\infty   = \mathop {\sup }\limits_{x \in \left] { - 1,1} \right]} \left| {u_n (x)} \right| = \dfrac{1}{n}$ et $\displaystyle\sum\limits_{n\geqslant 1}{\dfrac{1}{n}}$ diverge.
		        Donc $\displaystyle\sum\limits_{n\geqslant1}^{}\dfrac{(-1)^n}{n}~x^n$ ne converge pas normalement sur $D$.
		        $\displaystyle\sum\limits_{n\geqslant1}^{}\dfrac{(-1)^n}{n}~x^n$ ne converge pas uniformément sur $D$ non plus car, sinon, on pourrait employer le théorème de la double limite en $- 1$ et cela entraînerait la convergence de la série $\displaystyle\sum\limits_{n\geqslant 1}{\dfrac{1}{n}}$, ce qui est absurde.

		    2.  En tant que somme d’une série entière de rayon de convergence 1, $S$ est continue sur $\left]-1,1 \right[$ .(\*)
		        Pour étudier la continuité en 1, on peut se placer sur $\left[ 0,1\right]$ .
		        $\forall \:x \in \left[ {0,1} \right]$, la série numérique $\displaystyle\sum \limits_{n\geqslant 1}{}{u_n (x)}$ satisfait le critère spécial des séries alternées ce qui permet de majorer son reste.
		        On a, $\forall \:x \in \left[ {0,1} \right]$, $\left| {\displaystyle\sum\limits_{k = n + 1}^{ + \infty } {u_k (x)} } \right| \leqslant \left| {u_{n + 1} (x)} \right| = \dfrac{{x^{n + 1} }}{{n + 1}} \leqslant \dfrac{1}{{n + 1}}$.(majoration indépendante de $x$)
		        Et, $\lim\limits_{n\to +\infty}^{}\dfrac{1}{n+1}=0$.
		        Donc, $\displaystyle\sum \limits_{n\geqslant 1}{}{u_n }$ converge uniformément sur $\left[ {0,1} \right]$.
		        Les fonctions $u_n$ étant continues sur $\left[ {0,1} \right]$ , la somme $S$ est alors continue sur $\left[ {0,1} \right]$.
		        Donc, en particulier, $S$ est continue en 1.(\*\*)
		        Donc, d’après (\*) et (\*\*), $S$ est continue sur $D$.

!!! exercice "EXERCICE 19 analyse  "
	=== "Enoncé"  


		1.  Prouver que, pour tout entier naturel $n$, $f_n: t\longmapsto t^n \ln t$ est intégrable sur $\left]0,1 \right]$ et calculer $I_n=\displaystyle\int_{0}^{1}t^n\ln tdt$.

		2.  Prouver que $f:t\longmapsto \mathrm{e}^t\ln t$ est intégrable sur $\left]0,1 \right]$ et que $\displaystyle\int_{0}^{1}\mathrm{e}^t\ln tdt=-\sum\limits_{n=1}^{+\infty}\frac{1}{nn!}$.
		    **Indication**: utiliser le développement en série entière de la fonction exponentielle.


	=== "Corrigé"  


		1.  On pose, pour tout entier naturel $n$, pour tout $t\in \left]0,1 \right]$, $f_n(t)= t^n \ln t$.
		    Pour tout entier naturel $n$, $f_n$ est continue par morceaux sur $\left]0,1 \right]$.
		    On a $t^{\frac{1}{2}}|f_n(t)|\underset{t\to 0}{\longrightarrow}0$ donc, au voisinage de 0, $|f_n(t)|=o\left( \dfrac{1}{t^{\frac{1}{2}}}\right)$.
		    Or, $t\mapsto\dfrac{1}{t^{\frac{1}{2}}}$ est intégrable sur $\left]0,1 \right]$(fonction de Riemann intégrable).
		    Donc $f_n$ est intégrable sur $\left]0,1 \right]$.
		    De plus,pour $x\in \left]0,1 \right]$, par intégration par parties:
		    $\displaystyle\int_{x}^{1}t^n\ln tdt=\left[ \dfrac{t^{n+1}\ln t}{n+1}\right]_x^1-\displaystyle\int_{x}^{1}\dfrac{t^n}{n+1}dt=-\dfrac{x^{n+1}\ln x}{n+1}-\dfrac{1}{(n+1)^2}+\dfrac{x^{n+1}}{(n+1)^2}$.
		    On en déduit, en faisant tendre $x$ vers 0, que $I_n=-\dfrac{1}{(n+1)^2}$.

		2.  $\forall\:t\in\mathbb{R}$, $\mathrm{e}^t=\displaystyle\sum_{n=0}^{+\infty}\dfrac{t^n}{n!}$ donc, pour tout $t\in\left]0,1 \right]$, $f(t)=\displaystyle\sum_{n=0}^{+\infty}\dfrac{t^n\ln t}{n!}$.
		    On pose : $\forall\:n\in\mathbb{N}$, $\forall\:t\in\left]0,1 \right]$, $g_n(t)=\dfrac{t^n\ln t}{n!}$.  
		    i) $\forall n\in{\mathbb{N}}$, $g_n$ est continue par morceaux et intégrable sur $\left]0,1 \right]$ d’après la question 1.  
		    ii) $\displaystyle\sum g_n$ converge simplement sur $\left]0,1 \right]$ et a pour somme $f$.  
		    iii) $f$ est continue par morceaux sur $\left]0,1 \right]$.  
		    iv) $\displaystyle\sum\displaystyle\int_{0}^{1 }\left\vert
		    g_{n}\left( t\right) \right\vert  \mathrm{d}t=$ $\displaystyle\sum\limits\displaystyle\int_{0}^{1 }\dfrac{-t^n \ln t}{n!}\mathrm{d}t=\displaystyle\sum\limits\dfrac{-I_n}{n!} =\displaystyle\sum\limits\dfrac{1}{(n+1)^2n!}$.
		    On a : $\forall\:n\in\mathbb{N}$, $0\leqslant\dfrac{1}{(n+1)^2n!}\leqslant\dfrac{1}{(n+1)^2}$.
		    De plus, $\displaystyle\sum_{}^{}\dfrac{1}{(n+1)^2}=\displaystyle\sum_{n\geqslant1}^{}\dfrac{1}{n^2}$ et $\displaystyle\sum_{n\geqslant1}^{}\dfrac{1}{n^2}$ converge.
		    Donc, par critère de majoration des séries à termes positifs, $\sum\limits\dfrac{1}{(n+1)^2n!}$ converge.
		    Alors, d’après le théorème d’intégration terme à terme pour les séries de fonctions,
		    $f$ est intégrable sur $\left]0,1 \right]$ et on a :
		    $\displaystyle\int_{0}^{1 }\mathrm{e}^t\ln t\mathrm{d}t
		    =\displaystyle\int_{0}^{1 } \displaystyle\sum\limits_{n=0}^{+\infty } g_n(t)\mathrm{d}t
		    =\displaystyle\sum\limits_{n=0}^{+\infty}\displaystyle\int_{0}^{1 }\dfrac{t^n\ln t}{n!} \mathrm{d}t
		    =\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{I_n}{n!}
		    =-\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{1}{n!(n+1)^2}
		    =-\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{1}{(n+1)!(n+1)}$.
		    C’est-à-dire, $\displaystyle\int_{0}^{1 }\mathrm{e}^t\ln t\mathrm{d}t=-\displaystyle\sum\limits_{n=1}^{+\infty}\dfrac{1}{n n!}$.

!!! exercice "EXERCICE 20 analyse  "
	=== "Enoncé"  


		1.  Donner la définition du rayon de convergence d’une série entière de la variable complexe.

		2.  Déterminer le rayon de convergence de chacune des séries entières suivantes:

		    1.  $\displaystyle\sum \dfrac{\left(n!\right)^{2}}{\left(2n\right)!}z^{2n+1}$.

		    2.  $\displaystyle\sum n^{\left( -1\right)^{n}} z^{n}$.

		    3.  $\displaystyle\sum \cos nz^{n}$.


	=== "Corrigé"  


		1.  Soit $\displaystyle\sum a_nz^n$ une série entière.
		    Le rayon de convergence $R$ de la série entière $\displaystyle\sum a_nz^n$ est l’unique élément de $\mathbb{R}^+\cup \left\lbrace +\infty\right\rbrace$ défini par:
		    $R=\sup \left\lbrace r\geqslant 0\:/ (a_nr^n)\:\text{est bornée} \right\rbrace$.
		    On peut aussi définir le rayon de convergence de la manière suivante:

		    $\exists\:!\:R\in\mathbb{R}^{+}\cup\left\lbrace +\infty\right\rbrace$ tel que:
		    i) $\forall z\in \mathbb{C}$, $|z|<R\Longrightarrow \displaystyle\sum a_nz^n$ converge absolument.
		    ii) $\forall z\in \mathbb{C}$, $|z|>R\Longrightarrow \displaystyle\sum a_nz^n$ diverge (grossièrement).
		    $R$ est le rayon de convergence de la série entière $\displaystyle\sum a_nz^n$ .
		    Remarque: pour une série entière de la variable réelle, la définition est identique.

		2.  1.  Notons $R$ le rayon de convergence de $\displaystyle\sum \dfrac{\left(n!\right)^{2}}{\left(2n\right)!}z^{2n+1}$ et posons : $\forall n\in\mathbb{N}$, $\forall\:z\in\mathbb{C}$, $u_n(z) = \dfrac{(n!)^2}{(2n)!}z^{2n+1}$.
		        Pour $z=0$, $\displaystyle\sum u_n(0)$ converge.
		        Pour $z\neq 0$, $\left| {\dfrac{{u_{n + 1} (z)}}{{u_n (z)}}} \right| =\dfrac{n+1}{4n+2}|z|^2$. Donc $\lim\limits_{n\to +\infty}^{} \left| {\dfrac{{u_{n + 1} (z)}}{{u_n (z)}}} \right|=\dfrac{\left| z \right|^2}{4}$.
		        D’après la règle de d’Alembert,
		        Pour $\left| z \right| < 2$, la série numérique $\displaystyle\sum u_n(z)$ converge absolument.
		        Pour $\left| z \right| > 2$, la série numérique diverge grossièrement.
		        On en déduit que $R$=2.

		    2.  Notons $R$ le rayon de convergence de $\displaystyle\sum n^{\left( -1\right)^{n}} z^{n}$ et posons : $\forall \:n\in\mathbb{N}$, $a_n= n^{\left( -1\right)^{n}}$.
		        On a, $\forall \:n\in\mathbb{N}$, $\forall \:z\in\mathbb{C}$, $|a_nz^n|\leqslant |nz^n|$ et le rayon de convergence de la série entière $\displaystyle\sum nz^n$ vaut 1.
		        Donc $R\geqslant 1$.(\*)
		        De même, $\forall \:n\in\mathbb{N}^*$, $\forall \:z\in\mathbb{C}$, $|\dfrac{1}{n}z^n|\leqslant|a_nz^n|$ et le rayon de convergence de la série $\displaystyle\sum\limits_{n\geqslant 1}^{}\dfrac{1}{n}z^n$ vaut 1.
		        Donc $R\leqslant 1$.(\*\*)
		        D’après (\*) et (\*\*), $R=1$.

		    3.  Notons $R$ le rayon de convergence de $\displaystyle\sum \cos nz^{n}$et posons: $\forall \:n\in\mathbb{N}$, $a_n= \cos n$.
		        On a, $\forall \:n\in\mathbb{N}$, $\forall \:z\in\mathbb{C}$, $|a_nz^n|\leqslant |z^n|$ et le rayon de convergence de la série entière $\displaystyle\sum z^n$ vaut 1.
		        Donc $R\geqslant 1$.(\*)
		        Pour $z=1$, la série $\displaystyle\sum \cos nz^{n}= \displaystyle\sum \cos n$ diverge grossièrement car $\cos n\underset{n\to +\infty}{\not\longrightarrow}0$.
		        Donc $R\leqslant 1$.(\*\*)
		        D’après (\*) et (\*\*), $R=1$.

!!! exercice "EXERCICE 21 analyse  "
	=== "Enoncé"  


		1.  Donner la définition du rayon de convergence d’une série entière de la variable complexe.

		2.  Soit $\left( a_{n}\right) _{n\in \mathbb{N}}$ une suite bornée telle que la série $\displaystyle\sum a_{n}$ diverge.
		    Quel est le rayon de convergence de la série entière $\displaystyle\sum a_{n}z^{n}$? Justifier.

		3.  Quel est le rayon de convergence de la série entière $\displaystyle\sum \limits_{n\geqslant 1}^{}\left( \sqrt{n}\right)^{(-1)^n} \ln \left(1+\dfrac{1}{\sqrt{n}} \right)  z^{n}$?


	=== "Corrigé"  


		1.  Soit $\displaystyle\sum a_nz^n$ une série entière.
		    Le rayon de convergence $R$ de la série entière $\displaystyle\sum a_nz^n$ est l’unique élément de $\mathbb{R}^+\cup \left\lbrace +\infty\right\rbrace$ défini par:
		    $R=\sup \left\lbrace r\geqslant 0\:/ (a_nr^n)\:\text{est bornée} \right\rbrace$.
		    On peut aussi définir le rayon de convergence de la manière suivante:

		    $\exists\:!\:R\in\mathbb{R}^{+}\cup\left\lbrace +\infty\right\rbrace$ tel que:
		    i) $\forall z\in \mathbb{C}$, $|z|<R\Longrightarrow \displaystyle\sum a_nz^n$ converge absolument.
		    ii) $\forall z\in \mathbb{C}$, $|z|>R\Longrightarrow \displaystyle\sum a_nz^n$ diverge (grossièrement).
		    $R$ est le rayon de convergence de la série entière $\displaystyle\sum a_nz^n$ .
		    Pour une série entière de la variable réelle, la définition est identique.

		2.  La série numérique $\displaystyle\sum {a_n z^n }$ diverge pour $z = 1$.
		    Donc $R \leqslant 1$.(\*)
		    De plus, la suite $(a_n )_{n\in \mathbb{N}}$ étant bornée, la suite $(a_n1^n )_{n\in \mathbb{N}}$ est bornée.
		    Donc $1\in \left\lbrace r\geqslant 0\:/ (a_nr^n)\:\text{est bornée} \right\rbrace$.
		    Donc $R\geqslant 1$.(\*\*)
		    D’après (\*) et (\*\*), $R=1$.

		3.  Notons $R$ le rayon de convergence de $\displaystyle\sum \limits_{n\geqslant 1}^{}\left( \sqrt{n}\right)^{(-1)^n} \ln \left(1+\dfrac{1}{\sqrt{n}} \right)  z^{n}$.
		    On pose, $\forall\:n\in\mathbb{N}^*$, $a_n=\left( \sqrt{n}\right)^{(-1)^n} \ln \left(1+\dfrac{1}{\sqrt{n}} \right)$.
		    $\forall\:n\in\mathbb{N}^*$, $a_n\geqslant\dfrac{1}{\sqrt{n}}\ln \left(1+\dfrac{1}{\sqrt{n}} \right)=b_n$.
		    Or $b_n\underset{+\infty}{\thicksim}\dfrac{1}{n}$ et $\displaystyle\sum \limits_{n\geqslant 1}^{}\dfrac{1}{n}$ diverge donc $\displaystyle\sum \limits_{n\geqslant 1}^{}b_n$ diverge.
		    Donc, par critère de minoration pour les séries à termes positifs,$\displaystyle\sum \limits_{n\geqslant 1}^{}a_n$ diverge .(\*\*\*)
		    De plus, $\forall\:n\in\mathbb{N}^*$, $|a_n|=a_n\leqslant \sqrt{n}\ln \left(1+\dfrac{1}{\sqrt{n}} \right)\leqslant 1$ car $\forall \:x\in \left[0,+\infty \right[$, $\ln (1+x)\leqslant x$.
		    Donc $(a_n)_{n\in\mathbb{N}}$ est bornée.(\*\*\*\*)
		    D’après (\*\*\*) et (\*\*\*\*), on peut appliquer 2. et on en déduit que $R=1$.

!!! exercice "EXERCICE 22 analyse  "
	=== "Enoncé"  


		1.  Que peut-on dire du rayon de convergence de la somme de deux séries entières? Le démontrer.

		2.  Développer en série entière au voisinage de $0$, en précisant le rayon de convergence, la fonction $f\ :\ x\longmapsto \ln \left( 1+x\right)+\ln \left( 1-2x\right)$ .

		    La série obtenue converge-t-elle pour $x=\dfrac{1}{4}$? $x=\dfrac{1}{2}$? $x=-\dfrac{1}{2}$?


	=== "Corrigé"  


		1.  On note $R_a$ et $R_b$ les rayons de convergence respectifs de $\displaystyle\sum a_nz^n$ et $\displaystyle\sum b_nz^n$.
		    On note $R$ le rayon de convergence de la série entière somme de $\displaystyle\sum {a_n z^n }$ et $\displaystyle\sum {b_n z^n }$, c’est-à-dire le rayon de convergence de la série entière $\displaystyle\sum (a_n+b_n)z^n$.
		    On a toujours $R \geqslant \min (R_a ,R_b )$.
		    De plus, si $R_a  \ne R_b$ alors $R =\min (R_a ,R_b )$.
		    **Preuve:**
		    On suppose par exemple que $R_a\leqslant R_b$.
		    Premier cas: $R_a=0$.
		    $R\geqslant 0=\min (R_a;R_b)$.
		    Deuxième cas: $R_a>0$.
		    Soit $z\in\mathbb{C}$ tel que $|z|< \min(R_a,R_b)=R_a$.
		    Comme $|z|<R_a$, alors $\displaystyle\sum a_nz^n$ converge absolument.
		    De même, comme $|z|<R_b$, alors $\displaystyle\sum b_nz^n$ converge absolument.
		    De plus, $\forall\:n\in\mathbb{N}$, $|(a_n+b_n)z^n|\leqslant |a_nz^n|+[b_nz^n|$.(\*)
		    Or $\displaystyle\sum\left(|a_nz^n|+[b_nz_n| \right)$ converge car somme de deux séries convergentes.
		    Donc, par critère de majoration pour les séries à termes positifs et en utilisant (\*), on en déduit que $\displaystyle\sum|(a_n+b_n)z^n|$ converge, c’est-à-dire $\displaystyle\sum(a_n+b_n)z^n$ converge absolument.
		    Donc $z\in D_0(O,R)$.
		    On en déduit que $R\geqslant \min(R_a,R_b)$.(\*\*)
		    On suppose maintenant que $R_a\neq R_b$, c’est-à-dire $R_a<R_b$.
		    Soit $z\in\mathbb{C}$ tel que $R_a<|z|<R_b$.
		    $|z|<R_b$, donc $\displaystyle\sum b_nz^n$ converge.
		    $|z|>R_a$, donc $\displaystyle\sum a_nz^n$ diverge.
		    Donc $\displaystyle\sum (a_n+b_n)z^n$ diverge (somme d’une série convergente et d’une série divergente).
		    On en déduit que $|z|\geqslant R$.
		    On a donc prouvé que $\forall\:z\in\mathbb{C}$, $R_a<|z|<R_b$$\Rightarrow$ $|z|\geqslant R$.
		    Donc $R\leqslant R_a$.
		    C’est-à-dire $R\leqslant \min(R_a,R_b)$.(\*\*\*)
		    Donc, d’après (\*\*) et (\*\*\*), $R=\min(R_a,R_b)$.

		2.  Pour $\left| x \right| < 1$, $\ln (1 + x) = \displaystyle\sum\limits_{n = 1}^{ + \infty } {\dfrac{{( - 1)^{n - 1} }}{n}x^n }$.
		    Pour $\left| x \right| < \dfrac{1}{2}$, $\ln (1 - 2x) =  - \displaystyle\sum\limits_{n = 1}^{ + \infty } {\dfrac{{2^n }}{n}x^n }$.
		    D’après 1., le rayon de convergence de $\displaystyle\sum\limits_{n \geqslant 1}^{  } {\dfrac{{( - 1)^{n - 1}  - 2^n }}{n}x^n }$ vaut $\dfrac{1}{2}$.
		    Donc le domaine de validité du développement en série entière à l’origine de $f$ contient $\left] -\dfrac{1}{2},\dfrac{1}{2}\right[$ et est contenu dans $\left[-\dfrac{1}{2},\dfrac{1}{2} \right]$.
		    Et, pour $|x|<\dfrac{1}{2}$, $f(x) = \displaystyle\sum\limits_{n = 1}^{ + \infty } {\dfrac{{( - 1)^{n - 1}  - 2^n }}{n}x^n }$.
		    Pour $x = \dfrac{1}{4}$:
		    la série entière $\displaystyle\sum\limits_{n \geqslant 1}^{  } {\dfrac{{( - 1)^{n - 1}  - 2^n }}{n}x^n }$ converge car $\left|\dfrac{1}{4} \right| <  \dfrac{1}{2}$.
		    Pour $x = \dfrac{1}{2}$:
		    la série entière $\displaystyle\sum\limits_{n \geqslant 1}^{  } {\dfrac{{( - 1)^{n - 1}  - 2^n }}{n}x^n }$ diverge car elle est la somme d’une série convergente ($\dfrac{1}{2}$ appartient au disque de convergence de la série entière $\displaystyle\sum\limits_{n \geqslant 1}^{  } {\dfrac{{( - 1)^{n - 1} }}{n}x^n }$) et d’une série divergente (série harmonique).
		    Pour $x=-\dfrac{1}{2}$:
		    la série entière $\displaystyle\sum\limits_{n \geqslant 1}^{  } {\dfrac{{( - 1)^{n - 1}  - 2^n }}{n}x^n }$ converge comme somme de deux séries convergentes.
		    En effet:
		    D’une part, $\displaystyle\sum\limits_{n \geqslant 1}^{  }\dfrac{(-1)^{n-1}}{n}\left( -\dfrac{1}{2}\right) ^n$ converge car $-\dfrac{1}{2}$ appartient au disque de convergence de la série entière $\displaystyle\sum\limits_{n \geqslant 1}^{  } {\dfrac{{( - 1)^{n - 1} }}{n}x^n }$ .
		    D’autre part,
		    $\displaystyle\sum\limits_{n \geqslant 1}^{  }
		     -\dfrac{2^n}{n}\left( -\dfrac{1}{2}\right) ^n=
		     -\displaystyle\sum\limits_{n \geqslant 1}^{  }\dfrac{(-1)^n}{n}$ converge d’après le critère spécial des séries alternées ( la suite $(\frac{1}{n})_{n\in\mathbb{N}^*}$ est bien positive, décroissante et de limite nulle).

!!! exercice "EXERCICE 23 analyse  "
	=== "Enoncé"  


		Soit $\left( a_{n}\right) _{n\in \mathbb{N}}$ une suite complexe telle que la suite $\left( \dfrac{\left\vert a_{n+1}\right\vert }{\left\vert a_{n}\right\vert }\right) _{n\in \mathbb{N}}$ admet une limite.

		1.  Démontrer que les séries entières $\displaystyle\sum a_{n}x^{n}$ et $\displaystyle\sum (n+1)a_{n+1}x^{n}$ ont le même rayon de convergence.

		    On le note $R$.

		2.  Démontrer que la fonction $x\longmapsto \displaystyle\sum\limits_{n=0}^{+\infty }a_{n}x^{n}$ est de classe $\mathcal{C}^1$ sur l’intervalle $]-R,R[$.


	=== "Corrigé"  


		1.  Pour $x \ne 0$, posons $u_n(x)  = a_n x^n \text{ et }v_n (x) = (n+1)a_{n+1} x^{n }$.
		    On pose $\ell=\lim\limits_{n\to\infty}^{}\dfrac{|a_{n+1}|}{|a_n|}$.
		    On a, alors, $\lim\limits_{n\to\infty}^{}\dfrac{|u_{n+1}(x)|}{|u_n(x)|}=\ell |x|$ et $\lim\limits_{n\to\infty}^{}\dfrac{|v_{n+1}(x)|}{|v_n(x)|}=\ell |x|$.
		    On en déduit que le rayon de convergence des deux séries entières $\displaystyle\sum {a_n x^n }$ et $\displaystyle\sum {(n+1)a_{n+1} x^{n } }$ vaut $R = 1 / \ell$ (avec $R =  + \infty$ dans le cas $\ell  = 0$ et $R=0$ dans le cas $\ell=+\infty$).

		2.  Soit $R$ le rayon de convergence de $\displaystyle\sum a_nx^n$.
		    On pose, $\forall \:n\in\mathbb{N}$,$\forall \:x\in \left] -R,R\right[$, $f_n(x)=a_nx^n$.
		    Soit $r\in\left[ 0,R\right[$. On pose $D_r= \left[ -r,r\right]$.
		    i) $\displaystyle\sum f_n$ converge simplement sur $D_r$.
		    ii) $\forall \:n\in\mathbb{N}$, $f_n$ est de classe $\mathcal{C}^1$ sur $D_r$ .
		    iii) D’après 1., $\displaystyle\sum f'_n$ est une série entière de rayon de convergence $R$.
		    Donc, d’après le cours, $\displaystyle\sum f'_n$ converge normalement donc uniformément sur tout compact inclus dans $\left] -R,R\right[$, donc converge uniformément sur $D_r$.
		    On en déduit que $\forall \:r\in\left[ 0,R\right[$, $S:x \mapsto \displaystyle\sum\limits_{n = 0}^{ + \infty } {a_n x^n }$ est de classe $\mathcal{C}^1$ sur $D_r$.
		    Donc, $S$ est de classe $\mathcal{C}^1$ sur $\left] -R,R\right[$.

!!! exercice "EXERCICE 24 analyse  "
	=== "Enoncé"  


		1)  Déterminer le rayon de convergence de la série entière $\displaystyle\sum\dfrac{x^n}{(2n)!}$ .

		On pose $S(x)=\displaystyle\sum_{n=0}^{+\infty}\dfrac{x^n}{(2n)!}$ .

		2)  Rappeler, sans démonstration, le développement en série entière en 0 de la fonction $x\mapsto \text{ch}(x)$ et préciser le rayon de convergence.

		3)  a)  Déterminer $S(x)$.

		b)  On considère la fonction $f$ définie sur $\mathbb{R}$ par:   

		$$
		f(0)=1,f(x)=\cosh(\sqrt x)\text{ si $x>0$},f(x)=\cos\sqrt{-x}\text{ si $x<0$}.
		$$

		Démontrer que $f$ est de classe $C^{\infty}$ sur $\mathbb{R}$.


	=== "Corrigé"  


		1.  Notons $R$ le rayon de convergence de la série entière $\displaystyle\sum\dfrac{x^n}{(2n)!}$.
		    Pour $x \ne 0$, posons $u_n  = \dfrac{x^n }{(2n)!}$.
		    $\lim\limits_{n\to+\infty}^{}\left| \dfrac{u_{n + 1} }  {u_n } \right|=\lim\limits_{n\to+\infty}^{} \dfrac{|x|}{(2n+2)(2n+1)}=0$.
		    On en déduit que la série entière $\displaystyle\sum {\dfrac{{x^n }}{{(2n)!}}}$ converge pour tout $x \in \mathbb{R}$ et donc $R =  + \infty$.

		2.  $\forall \:x\in\mathbb{R}$, $\textrm{ch} (x) = \displaystyle\sum\limits_{n = 0}^{ + \infty } {\dfrac{{x^{2n} }}{{(2n)!}}}$ et le rayon de convergence du développement en série entière de la fonction $\textrm{ch}$ est égal à $+ \infty$.

		3.  1.  Pour $x \geqslant 0$, on peut écrire $x = t^2$ et $S(x) = \displaystyle\sum\limits_{n = 0}^{ + \infty } {\dfrac{{x^n }}{{(2n)!}}}  = \displaystyle\sum\limits_{n = 0}^{ + \infty } {\dfrac{{t^{2n} }}{{(2n)!}}}  = \textrm{ch} (t) = \textrm{ch} \sqrt x$.
		        Pour $x < 0$, on peut écrire $x =  - t^2$ et $S(x) = \displaystyle\sum\limits_{n = 0}^{ + \infty } {\dfrac{{x^n }}{{(2n)!}}}  = \displaystyle\sum\limits_{n = 0}^{ + \infty } {\dfrac{{( - 1)^n t^{2n} }}{{(2n)!}}}  = \cos (t) = \cos \sqrt { - x}$.

		    2.  D’après la question précédente, la fonction $f$ n’est autre que la fonction $S$.
		        $S$ est de classe $\mathcal{C}^\infty$ sur $\mathbb{R}$ car développable en série entière à l’origine avec un rayon de convergence égal à $+\infty$.
		        Cela prouve que $f$ est de classe $\mathcal{C}^\infty$ sur $\mathbb{R}$.

!!! exercice "EXERCICE 25 analyse  "
	=== "Enoncé"  


		1.  Démontrer que, pour tout entier naturel $n$, la fonction $t\longmapsto \dfrac{1}{1+t^{2}+t^{n}e^{-t}}$ est intégrable sur $[0,+\infty[$.

		2.  Pour tout $n\in\mathbb{N}$, on pose $u_{n}=\displaystyle\int_{0}^{+\infty }\dfrac{\text{d}t}{1+t^{2}+t^{n}e^{-t}}$. Calculer $\underset{n\rightarrow +\infty }{\lim }u_{n}$.


	=== "Corrigé"  


		1.  $f_n :t \mapsto \dfrac{1}{{1 + t^2  + t^n {\mathrm{e}}^{ - t} }}$ est définie et continue par morceaux sur $\left[ {0, + \infty } \right[$.
		    De plus, $\forall\:t\in\left[ {0, + \infty } \right[$, $\left| {f_n (t)} \right| \leqslant \dfrac{1}{{1 + t^2 }} = \varphi (t)$.
		    Or $\varphi(t)\underset{+\infty}{\thicksim}\dfrac{1}{t^2}$ et $t\longmapsto \dfrac{1}{t^2}$ est intégrable sur $\left[1,+\infty \right[$, donc $\varphi$ est intégrable sur $\left[1,+\infty \right[$.
		    Donc, par critère de majoration pour les fonctions positives, $f_n$ est intégrable sur $\left[1,+\infty \right[$.
		    Or $f_n$ est continue sur $\left[ 0,1\right]$ donc $f_n$ est intégrable sur $\left[ {0, + \infty } \right[$.

		2.  i\) La suite de fonctions $(f_n )$ converge simplement sur $\left[ {0, + \infty } \right[$ vers la fonction $f$ définie par : $f (t)=  \left\{ {
		    \begin{array}{ll}
		     {\dfrac{1}{1+t^2}} & {{\text{si }}t \in \left[ {0,1} \right[}  \\
		     {\dfrac{1}{ {2 + {\mathrm{e}}^{ - 1} }}} & {{\text{si }}t = 1}  \\
		     0 & {{\text{si }}t \in \left] {1, + \infty } \right[}  \\
		    \end{array}
		    } \right.$
		    ii) Les fonctions $f_n$ et $f$ sont continues par morceaux sur $\left[ {0, + \infty } \right[$.
		    iii) $\forall t \in \left[ {0, + \infty } \right[,\left| {f_n (t)} \right| \leqslant \varphi (t)\text{ avec }\varphi \text{ intégrable }$ sur $\left[ {0, + \infty } \right[$.
		    Alors, d’après le théorème de convergence dominée, $\lim\limits_{n\to+\infty}^{}u_n  = \lim\limits_{n\to+\infty}^{}\displaystyle\int_0^{ + \infty } {f_n (t)\,{\mathrm{d}}t} =\displaystyle\int_0^{ + \infty } {f(t)\,{\mathrm{d}}t}$.
		    Or $\displaystyle\int_0^{ + \infty } {f(t)\,{\mathrm{d}}t}  = \displaystyle\int_0^1 {\dfrac{{{\mathrm{d}}t}}{{1 + t^2 }}}  = \dfrac{\pi }{4}$.
		    Donc, $\lim\limits_{n\to+\infty}^{}u_n  = \dfrac{\pi }{4}$.

!!! exercice "EXERCICE 26 analyse  "
	=== "Enoncé"  


		Pour tout entier $n\geqslant 1$, on pose $I_{n}=\displaystyle\int_{0}^{+\infty } \dfrac{1}{\left( 1+t^{2}\right) ^n} \text{d}t$.

		1.  Justifier que $I_{n}$ est bien définie.

		2.  1.  Étudier la monotonie de la suite $\left( I_{n}\right) _{n\in\mathbb{N}^*}$.

		    2.  Déterminer la limite de la suite $\left( I_{n}\right) _{n\in\mathbb{N}^*}$.

		3.  La série $\displaystyle\sum\limits_{n\geqslant 1}^{}(-1)^n I_{n}$ est-elle convergente ?


	=== "Corrigé"  


		Posons pour tout $n\in\mathbb{N}^*$ et $t\in \left[ 0,+\infty\right[$, $f_n(t)=\dfrac{1}{\left( 1+t^{2}\right) ^n}$.

		1.  $\forall\:n\in\mathbb{N}^*$, $f_n$ est continue sur $\left[ {0, + \infty } \right[$.
		    De plus, $|f_n(t)|\underset{+\infty}{\thicksim}\dfrac{1}{t^{2n}}$.
		    Or $n\geqslant 1$, alors $t\longmapsto\dfrac{1}{t^{2n}}$ est intégrable sur $\left[ 1,+\infty\right[$.
		    Donc, par règle d’équivalence pour les fonctions positives, $f_n$ est intégrable sur $\left[ 1,+\infty\right[$ .
		    Or $f_n$ est continue sur$\left[ 0,1\right]$, donc $f_n$ est intégrable sur $\left[ {0, + \infty } \right[$.

		2.  1.  $\forall\:t \in \left[ {0, + \infty } \right[$, $\dfrac{1}{{(1 + t^2 )^{n + 1} }} \leqslant \dfrac{1}{{(1 + t^2 )^n }}$ car $1+t^2\geqslant 1$.
		        En intégrant, on obtient : $\forall\:n\in\mathbb{N}^*$, $I_{n + 1}  \leqslant  I_n$.
		        Donc $\left( I_{n}\right) _{n\in\mathbb{N}^*}$ est décroissante.

		    2.  Remarque: $\left( I_{n}\right) _{n\in\mathbb{N}^*}$ est décroissante et clairement positive ce qui nous assure la convergence de la suite $\left( I_{n}\right) _{n\in\mathbb{N}^*}$ .
		        Déterminons la limite de la suite $\left( I_{n}\right) _{n\in\mathbb{N}^*}$.
		        i) $\forall\:n\in\mathbb{N}^*$, $f_n$ est continue par morceaux sur $\left[  {0, + \infty } \right[$.
		        ii) La suite de fonctions $(f_n)_{n\geqslant1}$ converge simplement sur $\left[  {0, + \infty } \right[$ vers la fonction $f$ définie sur $\left[0, +\infty\right[$ par $f(x)=\left\{\begin{aligned} 0\, \, \, \, \, &{\text si } \, \, x>0\\
		        1\, \, \, \, \, &{\text si } \, \, x=0
		        \end{aligned}\right.$ .
		        De plus, $f$ est continue par morceaux sur $\left[  {0, + \infty } \right[$.
		        iii) $\forall t \in \left[  {0, + \infty } \right[,\forall n \in \mathbb{N}^*,\left| {{f_n (t)}} \right| \leqslant \dfrac{1}{{1 + t^2 }} = \varphi (t)$ avec $\varphi$ intégrable sur $\left[  {0, + \infty } \right[$.
		        En effet $\varphi$ est continue sur $[0,+\infty[$ et $\varphi(t)\underset{+\infty}{\thicksim}\dfrac{1}{t^2}$. Comme $t\longmapsto \dfrac{1}{t^2}$ est intégrable sur $\left[1,+\infty \right[$, donc $\varphi$ est intégrable sur $\left[1,+\infty \right[$. Comme $\varphi$ est continue sur $\left[ 0,1\right]$, donc $\varphi$ est intégrable sur $[0,1]$ donc sur $\left[  {0, + \infty } \right[$.
		        Par le théorème de convergence dominée on obtient :
		        $$\lim\limits_{n\to +\infty}^{} I_n  = \lim\limits_{n\to +\infty}^{}\displaystyle\int_0^{ + \infty } {f_n (t)\,{\mathrm{d}}t}  =\displaystyle\int_0^{ + \infty } {f (t)\,{\mathrm{d}}t}= 0$$ et la suite $\left( I_{n}\right) _{n\in\mathbb{N}^*}$ a pour limite 0.

		3.  D’après les questions précédentes, la suite $\left( I_{n}\right) _{n\in\mathbb{N}^*}$ est positive, décroissante et converge vers 0.
		    Donc, par application du théorème spécial des séries alternées, on peut affirmer la convergence de la série $\displaystyle\sum\limits_{n\geqslant 1}^{}(-1)^n I_{n}$.

!!! exercice "EXERCICE 27 analyse  "
	=== "Enoncé"  


		Pour tout $n\in \mathbb{N}^*$, on pose $f_{n}\left( x\right) =\dfrac{e^{-x}}{%
		1+n^{2}x^{2}}\ \ $et $u_{n}=\displaystyle\int_{0}^{1}f_{n}\left( x\right) \mathrm{d}x$.

		1.  Étudier la convergence simple de la suite de fonctions $\left( f_{n}\right)$ sur $[0,1]$.

		2.  Soit $a\in\left] 0,1 \right[$. La suite de fonctions $\left( f_{n}\right)$ converge-t-elle uniformément sur $\left[a,1 \right]$?

		3.  La suite de fonctions $\left( f_{n}\right)$ converge-t-elle uniformément sur $[0,1]$?

		4.  Trouver la limite de la suite $\left( u_{n}\right) _{n\in \mathbb{N}^*}.$


	=== "Corrigé"  


		1.  Soit $x\in \left[0,1 \right]$.
		    Si $x = 0$, $f_n (0) = 1$.
		    Si $x \in \left] {0,1} \right]$, pour $n$ au voisinage de $+\infty$, $f_n(x)\underset{+\infty}{\thicksim} \dfrac{\mathrm{e}^{-x}}{x^2}\dfrac{1}{n^2}$, donc $\lim\limits_{n\to +\infty}^{}f_n (x) = 0$.
		    On en déduit que la suite de fonctions $(f_n )$ converge simplement sur $\left[ {0,1} \right]$ vers la fonction $f$ définie par:
		    $f(x) = \left\{ {
		    \begin{array}{ll}
		     0 & {{\text{si }}x \in \left] {0,1} \right]}  \\
		     1 & {{\text{si }}x = 0}  \\
		    \end{array}
		    } \right.$

		2.  Soit $a\in\left] 0;1 \right[$.
		    $\forall\:n\in\mathbb{N}^*$, $\forall\:x\in \left[a,1 \right]$, $|f_n(x)-f(x)|=f_n(x)\leqslant \dfrac{\mathrm{e}^{-a}}{1+n^2a^2}$ (majoration indépendante de $x$).
		    Donc $\underset{t\in \left[a,1 \right]}{\sup }|f_n(t)-f(t)|\leqslant \dfrac{\mathrm{e}^{-a}}{1+n^2a^2}$.
		    Or $\lim\limits_{n\to +\infty}^{}\dfrac{\mathrm{e}^{-a}}{1+n^2a^2}=0$, donc $\lim\limits_{n\to +\infty}^{}\underset{t\in \left[a,1 \right]}{\sup }|f_n(t)-f(t)|=0$
		    On en déduit que $\left( f_{n}\right)$ converge uniformément vers $f$ sur $\left[a,1 \right]$.

		3.  Les fonctions $f_n$ étant continues sur $\left[0,1 \right]$ et la limite simple $f$ ne l’étant pas, on peut assurer qu’il n’y a pas convergence uniforme sur $\left[ {0,1} \right]$.

		4.  i\) Les fonctions $f_n$ sont continues par morceaux sur $\left[ {0,1} \right]$.
		    ii) $(f_n)$ converge simplement vers $f$ sur $\left[ {0,1} \right]$, continue par morceaux sur $\left[ {0,1} \right]$ .
		    iii) De plus, $\forall x \in \left[ {0,1} \right],\left| {f_n (x)} \right| \leqslant {\mathrm{e}}^{ - x}  \leqslant 1 = \varphi (x)$ avec $\varphi :\left[ {0,1} \right] \to \mathbb{R}^ +$ continue par morceaux et intégrable sur $\left[ {0,1} \right]$ .
		    D’après le théorème de convergence dominée, on peut donc affirmer que:
		    $\lim\limits_{n\to +\infty}^{}u_n  = \lim\limits_{n\to +\infty}^{}\displaystyle\int_0^1 {f_n (x)\,{\mathrm{d}}x} =\displaystyle\int_0^1 {f(x)\,{\mathrm{d}}x}  = 0$.

!!! exercice "EXERCICE 28 analyse  "
	=== "Enoncé"  


		*N.B. : les deux questions sont indépendantes.*

		1.  La fonction $x\longmapsto \dfrac{e^{-x}}{\sqrt{x^{2}-4}}$ est-elle intégrable sur $]2,+\infty[$?

		2.  Soit $a$ un réel strictement positif.
		    La fonction $x\longmapsto \dfrac{\ln x}{\sqrt{1+x^{2a}}}$ est-elle intégrable sur $]0,+\infty[$?


	=== "Corrigé"  


		1.  Soit $f:x\longmapsto \dfrac{e^{-x}}{\sqrt{x^{2}-4}}$.
		$f$ est continue sur $]2,+\infty[$.
		$f(x)= \dfrac{e^{-x}}{\sqrt{(x-2)(x+2)}}\underset{2}{\thicksim}\dfrac{e^{-2}}{2}\times\dfrac{1}{(x-2)^{\frac{1}{2}}}$.
		Or $x\longmapsto \dfrac{1}{(x-2)^{\frac{1}{2}}}$ est intégrable sur $\left] 2,3\right]$ (fonction de Riemann intégrable sur $\left] 2,3\right]$ car $\dfrac{1}{2} <1$).
		Donc, par règle d’équivalence pour les fonctions positives, $f$ est intégrable sur $\left] 2,3\right]$.(\*)
		$f(x)\underset{+\infty}{\thicksim}\dfrac{\mathrm{e}^{-x}}{x}=g(x)$.
		Or $\lim\limits_{x\to+\infty}^{}x^2g(x)=0$ donc, au voisinage de $+\infty$, $g(x)=o(\dfrac{1}{x^2})$.
		Comme $x\longmapsto\dfrac{1}{x^2}$ est intégrable sur $\left[ 3,+\infty\right[$, on en déduit que $g$ est intégrable sur $\left[ 3,+\infty\right[$.
		Donc, par règle d’équivalence pour les fonctions positives, $f$ est intégrable sur $\left[ 3,+\infty\right[$. (\*\*)
		D’après (\*) et (\*\*), $f$ est intégrable sur $\left]2,+\infty \right[$.

		2.  Soit $a$ un réel strictement positif.
		On pose $\forall\:x\in \left]0,+\infty \right[$, $f(x)=  \dfrac{\ln x}{\sqrt{1+x^{2a}}}$.
		$f$ est continue sur $\left]0,+\infty \right[$.
		$|f(x)|\underset{0}{\thicksim}|\ln x|=g(x)$.
		Or $\lim\limits_{x\to 0}^{}x^\frac{1}{2}g(x)=0$ donc, au voisinage de 0, $g(x)=o\left( \dfrac{1}{x^{\frac{1}{2}}}\right)$.
		Or $x\longmapsto \dfrac{1}{x^{\frac{1}{2}}}$ est intégrable sur $\left] 0,1\right]$ (fonction de Riemann intégrable sur $\left] 0,1\right]$ car $\dfrac{1}{2}<1$).
		Donc $g$ est intégrable sur $\left] 0,1\right]$.
		Donc, par règle d’équivalence pour les fonctions positives, $|f|$ est intégrable sur $\left] 0,1\right]$.
		Donc, $f$ est intégrable sur $\left] 0,1\right]$ (\*)
		$f(x)\underset{+\infty}{\thicksim}\dfrac{\ln x}{x^a}=h(x)$.

		**Premier cas: si $a>1$.**
		$\lim\limits_{x\to +\infty}^{}x^{\frac{1+a}{2}}h(x)=\lim\limits_{x\to +\infty}^{}x^{\frac{1-a}{2}}\ln x=0$, donc, au voisinage de $+\infty$, $h(x)=o\left( \dfrac{1}{x^{\frac{1+a}{2}}}\right)$.
		Or $x\longmapsto\dfrac{1}{x^{\frac{1+a}{2}}}$ est intégrable sur $\left[ 1,+\infty\right[$ (fonction de Riemann intégrable sur $\left[ 1,+\infty\right[$ car $\dfrac{1+a}{2}>1$).
		Donc, $h$ est intégrable sur $\left[ 1,+\infty\right[$.
		Donc, par règle d’équivalence pour les fonctions positives, $f$ est intégrable sur $\left[ 1,+\infty\right[$.(\*\*).
		D’après (\*) et (\*\*), $f$ est intégrable sur $\left] 0,+\infty\right[$.

		**Deuxième cas: si $a\leqslant 1$**
		$\forall\:x\in \left[ \mathrm{e},+\infty\right[$, $h(x)\geqslant \dfrac{1}{x^a}$.
		Or $x\longmapsto \dfrac{1}{x^a}$ non intégrable sur $\left[ \mathrm{e},+\infty\right[$.(fonction de Riemann avec $a\leqslant 1$)
		Donc, par règle de minoration pour les fonctions positives, $h$ non intégrable sur $\left[ \mathrm{e},+\infty\right[$
		Donc, par règle d’équivalence pour les fonctions positives, $f$ non intégrable sur $\left[ \mathrm{e},+\infty\right[$.
		Donc, $f$ non intégrable sur $\left] 0,+\infty\right[$.

!!! exercice "EXERCICE 29 analyse  "
	=== "Enoncé"  


		On pose : $\forall\:x\in ]0,+\infty[$, $\forall\:t\in \left] 0,+\infty\right[$, $f(x,t)=\text{e}^{-t}t^{x-1}$ .

		1.  Démontrer que : $\forall \:x\in \left]0,+\infty \right[$, la fonction $t\mapsto f(x,t)$ est intégrable sur $\left] 0,+\infty\right[$.

		    On pose alors: $\forall \:x\in]0,+\infty[$, $\Gamma(x)=\displaystyle\int_0^{+\infty}\text{e}^{-t}t^{x-1}\text{d}t$.

		2.  Pour tout $x\in]0,+\infty[$, exprimer $\Gamma(x+1)$ en fonction de $\Gamma(x)$.

		3.  Démontrer que $\Gamma$ est de classe $C^1$ sur $]0,+\infty[$ et exprimer $\Gamma~\!'(x)$ sous forme d’intégrale.


	=== "Corrigé"  


		1.  Soit $x\in \left] 0,+\infty\right[$.
		    La fonction $t \mapsto {\mathrm{e}}^{ - t} t^{x - 1}$ est définie, positive et continue par morceaux sur $\left] {0, + \infty } \right[$.
		    $f(x,t)\mathop  \sim \limits_{t \to 0^ +  } t^{x - 1}$ et $t\longmapsto  t^{x - 1}=\dfrac{1}{t^{1-x}}$ est intégrable sur $\left] 0,1\right]$ (fonction de Riemann avec $1-x<1$).
		    Donc, par critère d’équivalence pour les fonctions positives, $t\longmapsto f(x,t)$ est intégrable sur $\left] 0,1\right]$ .(\*)  
		    De plus, $\lim\limits_{t\to+\infty}^{}t^2 f(x,t)=0$, donc, pour $t$ au voisinage de $+\infty$, $f(x,t)=o(\dfrac{1}{t^2})$.
		    Or $t\longmapsto \dfrac{1}{t^2}$ est intégrable sur $\left[ 1,+\infty\right[$ (fonction de Riemann intégrable).
		    Donc $t\longmapsto f(x,t)$ est intégrable sur $\left[ 1,+\infty\right[$.(\*\*)  
		    Donc, d’après (\*) et (\*\*), $t \mapsto f(x,t)$ est intégrable sur $\left] {0, + \infty } \right[$.

		2.  Par intégration par parties $\displaystyle\int_\varepsilon ^A {{\mathrm{e}}^{ - t} t^x \,{\mathrm{d}}t}  = \left[ { - {\mathrm{e}}^{ - t} t^x } \right]_\varepsilon ^A  + x\displaystyle\int_\varepsilon ^A {{\mathrm{e}}^{ - t} t^{x - 1} \,{\mathrm{d}}t}$.
		    On passe ensuite à la limite quand $\varepsilon  \to 0^ +$ et $A \to  + \infty$ et on obtient:
		    $\displaystyle\int_0^{+\infty} {{\mathrm{e}}^{ - t} t^x \,{\mathrm{d}}t}  = x\displaystyle\int_0 ^{+\infty} {{\mathrm{e}}^{ - t} t^{x - 1} \,{\mathrm{d}}t}$.
		    C’est-à-dire $\Gamma(x+1)=x\Gamma(x)$.

		3.  i\) pour tout $x>0$, $t\longmapsto f(x,t)$ est continue par morceaux et intégrable sur $\left] {0, + \infty } \right[$ (d’après la question 1.).
		    ii) $\forall \:t\in \left] {0, + \infty } \right[$, la fonction $x \mapsto f(x,t)$ est dérivable et $\forall \:(x,t)\in \left] {0, + \infty } \right[^2$,$\dfrac{{\partial f}}{{\partial x}}(x,t) = (\ln t){\mathrm{e}}^{ - t} t^{x - 1}$.
		    iii) Pour tout $x > 0$, $t \mapsto \dfrac{{\partial f}}{{\partial x}}(x,t)$ est continue par morceaux sur $\left] {0, + \infty } \right[$.  
		    iv) Pour tout $t > 0$, $x \mapsto \dfrac{{\partial f}}{{\partial x}}(x,t)$ est continue sur $\left] {0, + \infty } \right[$.  
		    v) Pour tout $\left[ {a,b} \right] \subset \left] {0, + \infty } \right[$ et $\forall\,
		    (t,x) \in \left] {0, + \infty } \right[ \times \left[ {a,b} \right]$ :
		    $\left| {\dfrac{{\partial f}}{{\partial x}}(x,t)} \right| \leqslant  \varphi (t)$ avec $\varphi(t)=\left\lbrace
		    \begin{array}{lll}
		    |\ln t|\mathrm{e}^{-t}t^{a-1}&\text{si}&t\in \left]  0,1\right[ \\
		    |\ln t|\mathrm{e}^{-t}t^{b-1}&\text{si}&t\in \left[ 1,+\infty\right[
		    \end{array}
		     \right.$

		    avec $\varphi$ continue par morceaux et intégrable sur $\left] {0, + \infty } \right[$.
		    En effet:
		    $\varphi(t)\underset{0^{+}}{\thicksim}|\ln t|t^{a-1}=\varphi_1(t)$ et $\lim\limits_{t\to 0^{+}}^{}t^{1-\dfrac{a}{2}}\varphi_1(t)=\lim\limits_{t\to 0}^{}t^{\dfrac{a}{2}}|\ln t|=0$.
		    Donc, au voisinage de $0^{+}$, $\varphi_1(t)=o\left( \dfrac{1}{t^{1-\dfrac{a}{2}}}\right)$.
		    Or $t\longmapsto \dfrac{1}{t^{1-\dfrac{a}{2}}}$ est intégrable sur $\left]  0,1\right[$(fonction de Riemann avec $1-\dfrac{a}{2}<1$).
		    Donc, $\varphi_1$ est intégrable sur $\left]  0,1\right[$.
		    Donc, par critère d’équivalence pour les fonctions positives, $\varphi$ est intégrable sur $\left]  0,1\right[$.(\*)
		    $\lim\limits_{t\to+\infty}^{}t^2\varphi(t)=0$.
		    Donc, pour $t$ au voisinage de $+\infty$, $\varphi(t)=o(\dfrac{1}{t^2})$.
		    Or, $t\longmapsto \dfrac{1}{t^2}$ est intégrable sur $\left[ 1,+\infty\right[$ (fonction de Riemann intégrable).
		    Donc $\varphi$ est intégrable sur $\left[1,+\infty \right[$.(\*\*)
		    D’après (\*) et (\*\*), $\varphi$ est intégrable sur $\left] 0,+\infty\right[$.
		    D’où, d’après le théorème de dérivation des intégrales à paramètres, $\Gamma$ est de classe ${\mathcal{C}}^1$ sur $\left] {0, + \infty } \right[$.
		    De plus, $\forall\:x\in\left] {0, + \infty } \right[$, $\Gamma '(x) = \displaystyle\int_0^{ + \infty } {(\ln t){\mathrm{e}}^{ - t} t^{x - 1} \,{\mathrm{d}}t}$.

!!! exercice "EXERCICE 30 analyse  "
	=== "Enoncé"  


		1.  Énoncer le théorème de dérivation sous le signe intégrale.

		2.  Démontrer que la fonction $f:x\longmapsto \displaystyle\int_{0}^{+\infty }e^{-t^{2}}\cos \left( xt\right) \text{d}t$ est de classe $C^{1}$ sur $\mathbb{R}$.

		3.  1.  Trouver une équation différentielle linéaire $\left(E\right)$ d’ordre $1$ dont $f$ est solution.

		    2.  Résoudre $\left(E\right)$.


	=== "Corrigé"  


		1.  Soit $u:(x,t) \mapsto u(x,t)$ une fonction définie de $X \times I$ vers $\mathbb{C}$, avec $X$ et $I$ intervalles contenant au moins deux points de $\mathbb{R}$.
		    On suppose que :  
		    i) $\forall\:x \in X$, $t\longmapsto u(x,t)$ est continue par morceaux et intégrable sur $I$.
		    On pose alors $\forall\:x \in X$, $f(x)=\int_{I}^{}u(x,t)\mathrm{d}t$.
		    ii) $u$ admet une dérivée partielle $\dfrac{{\partial u}}{{\partial x}}$ sur $X\times I$ vérifiant :
		    - $\forall x \in X,t \mapsto \dfrac{{\partial u}}{{\partial x}}(x,t)$ est continue par morceaux sur $I$.
		    - $\forall t \in I,x \mapsto \dfrac{{\partial u}}{{\partial x}}(x,t)$ est continue sur $X$.  
		    iii) il existe $\varphi :I \to \mathbb{R}^ +$ continue par morceaux, positive et intégrable sur $I$ vérifiant: $\forall (x,t) \in X \times I,\left| {\dfrac{{\partial u}}{{\partial x}}(x,t)} \right| \leqslant \varphi (t)$.
		    Alors la fonction $f$ est de classe $\mathcal{C}^1$ sur $X$ et $\forall x \in X,f'(x) = \displaystyle\int_I {\dfrac{{\partial u}}{{\partial x}}(x,t)\,{\mathrm{d}}t}$.

		2.  On pose $\forall\:(x,t)\in\mathbb{R}\times \left[ 0,+\infty\right[$, $u(x,t) = {\mathrm{e}}^{ - t^2 } \cos (xt)$.
		    i) $\forall \:x\in\mathbb{R}$, $t\longmapsto u(x,t)$ est continue sur $\left[ 0,+\infty\right[$.
		    De plus, $\forall \:x\in\mathbb{R}$, $|u(x,t)|\leqslant {\mathrm{e}}^{ - t^2 }$.
		    Or $\lim\limits_{t\to +\infty}^{}t^2{\mathrm{e}}^{ - t^2 }=0$, donc, au voisinage de $+\infty$, $\mathrm{e}^{ - t^2 }=o\left(\dfrac{1}{t^2} \right)$.
		    Donc, $t\longmapsto u(x,t)$ est intégrable sur $\left[ 0,+\infty\right[$.
		    ii) $\forall\:(x,t)\in\mathbb{R}\times \left[ 0,+\infty\right[$, ${\dfrac{{\partial u}}{{\partial x}}(x,t)}  =  { - t{\mathrm{e}}^{ - t^2 } \sin (xt)}$.
		    - $\forall x \in \mathbb{R}$, $t \mapsto \dfrac{{\partial u}}{{\partial x}}(x,t)$ est continue par morceaux sur $\left[ 0,+\infty\right[$. .
		    - $\forall t \in \left[ 0,+\infty \right]$, $x \mapsto \dfrac{{\partial u}}{{\partial x}}(x,t)$ est continue sur $\mathbb{R}$ .  
		    -iii) $\forall\:(x,t)\in\mathbb{R}\times \left[ 0,+\infty\right[$,$\left| \dfrac{\partial u}{\partial x}(x,t) \right| \leqslant t {\mathrm{e}}^{ - t^2 }=\varphi(t)$ avec $\varphi$ continue par morceaux, positive et intégrable sur $\left[ 0,+\infty\right[$.
		    En effet, $\lim\limits_{t\to +\infty}^{}t^2\varphi(t)=0$ donc, au voisinage de $+\infty$, $\varphi(t)=o(\dfrac{1}{t^2})$.
		    On en déduit que $\varphi$ est intégrable sur $\left[1,+\infty\right[$ et comme elle est continue sur $\left[ 0,1 \right[$, alors $\varphi$ est bien intégrable sur $\left[ 0,+\infty \right[$.
		    Donc $f$ est de classe $\mathcal{C}^1$ sur $\mathbb{R}$ et :
		    $\forall\:x\in\mathbb{R}$, $f'(x)=\displaystyle\int_{0}^{+\infty}{ - t{\mathrm{e}}^{ - t^2 } \sin (xt)}\mathrm{d}t$

		3.  1.  On a, $\forall\:x\in\mathbb{R}$, $f'(x) = \displaystyle\int_0^{ + \infty } { - t{\mathrm{e}}^{ - t^2 } \sin (xt)\,{\mathrm{d}}t}$.
		        Procédons à une intégration par parties. Soit $A \geqslant 0$.

						$$
						\displaystyle\int_0^A { - t{\mathrm{e}}^{ - t^2 } \sin (xt)\,{\mathrm{d}}t}  = \left[ {\dfrac{1}{2}{\mathrm{e}}^{ - t^2 } \sin (xt)} \right]_0^A  - \int_0^A {\dfrac{x}{2}{\mathrm{e}}^{ - t^2 } \cos (xt)\,{\mathrm{d}}t}
						$$

						En passant à la limite quand $A \to  + \infty$, on obtient $f'(x) + \dfrac{x}{2}f(x) = 0$.
		        Donc $f$ est solution de l’équation différentielle $(E)$: $y'+\dfrac{x}{2}y=0$.

		    2.  Les solutions de $(E)$ sont les fonctions $y$ définies par $y(x)=A\mathrm{e}^{-\dfrac{x^2}{4}}$, avec $A\in\mathbb{R}$.

!!! exercice "EXERCICE 31 analyse  "
	=== "Enoncé"  


		1.  Déterminer une primitive de $x\longmapsto \cos^4x$.

		2.  Résoudre sur $\mathbb{R}$ l’équation différentielle: $y''+y=\cos^3 x$ en utilisant la méthode de variation des constantes.


	=== "Corrigé"  


		1.  En linéarisant $\cos^4x$, on obtient $\cos^4x=\dfrac{1}{8}\left( \cos(4x)+4\cos(2x)+3\right)$.
		    Donc, $x\longmapsto\dfrac{1}{32}\sin (4x) + \dfrac{1}{4}\sin(2x)+\dfrac{3}{8}x$ est une primitive de $x\longmapsto \cos^4x$.

		2.  Notons $(E)$ l’équation différentielle $y''+y=\cos^3x$ .
		    C’est une équation différentielle linéaire d’ordre 2 à coefficients constants.
		    Les solutions de l’équation homogène associée sont les fonctions $y$ définies par : $y(x) = \lambda \cos x + \mu \sin x$.
		    Par la méthode de variation des constantes,
		    on cherche une solution particulière de $(E)$ de la forme $y_p(x) = \lambda (x)\cos x + \mu (x)\sin x$ avec $\lambda ,\mu$ fonctions dérivables vérifiant :
				$\begin{cases}\lambda'(x)\cos x + \mu'(x)\sin x = 0\\-\lambda '(x)\sin x + \mu '(x)\cos x = \cos^3 x\end{cases}\text{ i.e. }\begin{cases}\lambda '(x) =  - \sin x\cos^3x\\\mu '(x) =\cos^4x\end{cases}$.

		    $\lambda (x) =   \dfrac{1}{4}\cos ^4 x$ convient.
		    D’après la question 1., $\mu (x) = \dfrac{1}{32}\sin (4x) + \dfrac{1}{4}\sin(2x)+\dfrac{3}{8}x$ convient.
		    On en déduit que la fonction $y_p$ définie par $y_p(x) =  \dfrac{1}{4}\cos ^5 x+\left(\dfrac{1}{32}\sin (4x) + \dfrac{1}{4}\sin(2x)+\dfrac{3}{8}x \right)\sin x$ est une solution particulière de (E).
		    Finalement, les solutions de l’équation $(E)$ sont les fonctions $y$ définies par : $y(x) =  \lambda \cos x + \mu \sin x+ y_p(x), \text{ avec } (\lambda ,\mu ) \in \mathbb{R}^2$.

!!! exercice "EXERCICE 32 analyse  "
	=== "Enoncé"  


		Soit l’équation différentielle: $x(x-1)y''+3xy'+y=0$.

		1.  Trouver les solutions de cette équation différentielle développables en série entière sur un intervalle $\left]-r,r \right[$ de $\mathbb{R}$, avec $r>0$.
		    Déterminer la somme des séries entières obtenues.

		2.  Est-ce que toutes les solutions de $x(x-1)y''+3xy'+y=0$ sur $\left]0;1 \right[$ sont les restrictions d’une fonction développable en série entière sur $\left]-1,1 \right[$?


	=== "Corrigé"  


		1.  Soit $\displaystyle\sum {a_n x^n }$ une série entière de rayon de convergence $R > 0$ et de somme $S$.
		    Pour tout $x \in \left] { - R,R} \right[$, $S(x) = \displaystyle\sum\limits_{n = 0}^{ + \infty } {a_n x^n } \text{, }S'(x) = \displaystyle\sum\limits_{n = 1}^{ + \infty } {na_n x^{n - 1} } \text{ et }S''(x) = \displaystyle\sum\limits_{n = 2}^{ + \infty } {n(n - 1)a_n x^{n - 2} }  = \displaystyle\sum\limits_{n = 1}^{ + \infty } {(n + 1)na_{n + 1} x^{n - 1} }$.
		    Donc $x(x - 1)S''(x) + 3xS'(x) + S(x) = \displaystyle\sum\limits_{n = 0}^{ + \infty } {\left( {(n + 1)^2 a_n  - n(n + 1)a_{n + 1} } \right)x^n }$.
		    Par unicité des coefficients d’un développement en série entière, la fonction $S$ est solution sur $\left] { - R,R} \right[$ de l’équation étudiée si, et seulement si, $\forall\:n\in\mathbb{N}$, $(n + 1)^2 a_n  - n(n + 1)a_{n + 1}=0$.
		    C’est-à-dire : $\forall n \in \mathbb{N}$, $na_{n + 1}  = (n + 1)a_n$.
		    Ce qui revient à : $\forall n \in \mathbb{N},a_n  = na_1$.
		    Le rayon de convergence de la série entière $\displaystyle\sum {nx^n }$ étant égal à 1, on peut affirmer que les fonctions développables en série entière solutions de l’équation sont les fonctions :
		    $x \mapsto a_1 \displaystyle\sum\limits_{n = 0}^{ + \infty } {nx^n }  = a_1 x\dfrac{{{\mathrm{d}}}}{{{\mathrm{d}}x}}\left( {\dfrac{1}{{1 - x}}} \right) = \dfrac{{a_1 x}}{{(1 - x)^2 }}$ définies sur $\left] { - 1,1} \right[$, avec $a_1\in\mathbb{R}$.

		2.  Notons $(E)$ l’équation $x(x-1)y''+3xy'+y=0$.
		    Prouvons que les solutions de $(E)$ sur $\left] 0;1\right[$ ne sont pas toutes développables en série entière à l’origine. Raisonnons par l’absurde.
		    Si toutes les solutions de $(E)$ sur $\left] 0;1\right[$ étaient développables en série entière à l’origine alors, d’après 1., l’ensemble des solutions de $(E)$ sur $\left] 0;1\right[$ serait égal à la droite vectorielle $\mathrm{Vect} (f)$ où $f$ est la fonction définie par $\forall\:x\in \left] 0;1\right[$, $f(x)=\dfrac{x}{(1-x)^2}$.
		    Or, d’après le cours, comme les fonctions $x\longmapsto x(x-1)$, $x\longmapsto 3x$ et $x\longmapsto 1$ sont continues sur $\left] 0;1\right[$ et que la fonction $x\longmapsto x(x-1)$ ne s’annule pas sur $\left] 0;1\right[$, l’ensemble des solutions de $(E)$ sur $\left] 0;1\right[$ est un plan vectoriel.
		    D’où l’absurdité.

!!! exercice "EXERCICE 33 analyse  "
	=== "Enoncé"  


		On pose : $\forall\:(x,y)\in\mathbb{R}^2\backslash \left\lbrace (0,0)\right\rbrace$, $f\left( x,y\right) =\dfrac{xy}{\sqrt{x^{2}+y^{2}}}$ et $f\left( 0,0\right) =0$.

		1.  Démontrer que $f$ est continue sur $\mathbb{R}^{2}$.

		2.  Démontrer que $f$ admet des dérivées partielles en tout point de $\mathbb{R}^{2}$.

		3.  $f$ est-elle de classe $C^{1}$ sur $\mathbb{R}^{2}$? Justifier.


	=== "Corrigé"  


		1.  Par opérations sur les fonctions continues, $f$ est continue sur l’ouvert $\mathbb{R}^2 \backslash \left\{ {(0,0)} \right\}$.
		    On considère la norme euclidienne sur $\mathbb{R}^2$ définie par $\forall\:(x,y)\in\mathbb{R}^2$, $||(x,y)||_2=\sqrt{x^2+y^2}$.
		    On a $\forall\:(x,y)\in\mathbb{R}^2$, $|x|\leqslant ||(x,y)||_2$ et $|y|\leqslant||(x,y)||_2$.
		    On en déduit que $\forall\:(x,y)\in\mathbb{R}^2\backslash\left\lbrace (0,0)\right\rbrace$, $|f(x,y)-f(0,0)|=\dfrac{|x||y|}{||(x,y)||_2}\leqslant\dfrac{\left( ||(x,y)||_2\right) ^2}{||(x,y)||_2}=||(x,y)||_2\underset{(x,y)\to (0,0)}{\longrightarrow} 0$.
		    On en déduit que $f$ est continue en $(0,0)$.
		    Ainsi $f$ est continue sur $\mathbb{R}^2$.

		2.  Par opérations sur les fonctions admettant des dérivées partielles, $f$ admet des dérivées partielles en tout point de l’ouvert $\mathbb{R}^2 \backslash \left\{ {(0,0)} \right\}$.
		    En $(0,0)$:
		    $\mathop {\lim }\limits_{t \to 0} \dfrac{1}{t}\left( {f(t,0) - f(0,0)} \right) = 0\text{ }$, donc $f$ admet une dérivée partielle en $(0,0)$ par rapport à sa première variable et $\dfrac{{\partial f}}{{\partial x}}(0,0) = 0$.
		    De même, $\mathop {\lim }\limits_{t \to 0} \dfrac{1}{t}\left( {f(0,t) - f(0,0)} \right) = 0\text{ }$. Donc $f$ admet une dérivée partielle en $(0,0)$ par rapport à sa seconde variable et $\dfrac{{\partial f}}{{\partial y}}(0,0) = 0$.

		3.  D’après le cours, $f$ est de classe $C^{1}$ sur $\mathbb{R}^{2}$ si et seulement si $\dfrac{\partial f}{ \partial x}$ et $\dfrac{\partial f}{ \partial y }$ existent et sont continues sur $\mathbb{R}^{2}$.
		    Or, $\forall (x,y)\in\mathbb{R}^2 \backslash \left\{ {(0,0)} \right\}$, $\dfrac{\partial f}{ \partial x}(x,y)=\dfrac{y^3}{\left( x^2+y^2\right)^{\frac{3}{2}} }$.
		    On remarque que $\forall\:x>0$, $\dfrac{\partial f}{ \partial x}(x,x)=\dfrac{1}{2\sqrt{2}}$.
		    Donc, $\lim\limits_{x\to 0^{+}}^{}\dfrac{\partial f}{ \partial x}(x,x)=\dfrac{1}{2\sqrt{2}}\neq \dfrac{\partial f}{ \partial x}(0,0)$.
		    On en déduit que $\dfrac{\partial f}{ \partial x}$ n’est pas continue en $(0,0)$.
		    Donc $f$ n’est pas de classe $C^{1}$ sur $\mathbb{R}^{2}$.

!!! exercice "EXERCICE 34 analyse  "
	=== "Enoncé"  


		Soit $A$ une partie non vide d’un $\mathbb{R}$-espace vectoriel normé $E$.

		1.  Rappeler la définition d’un point adhérent à $A$, en termes de voisinages ou de boules.

		2.  Démontrer que: $x\in\bar{A}\Longleftrightarrow\exists(x_n)_{n\in\mathbb N}$ telle que, $\forall n\in\mathbb{N}, x_n\in A$ et $\displaystyle\lim_{n\to+\infty}x_n=x$.

		3.  Démontrer que, si $A$ est un sous-espace vectoriel de $E$, alors $\bar{A}$ est un sous-espace vectoriel de $E$.

		4.  Démontrer que si $A$ est convexe alors $\bar{A}$ est convexe.


	=== "Corrigé"  


		1.  Soit $A$ une partie non vide de $E$.
		    $\mathcal{V}(a)$ désigne l’ensemble des voisinages de $a$.
		    $\forall\:r>0$, $B_{0}(a,r)$ désigne la boule ouverte de centre $a$ et de rayon $r$.
		    Soit $a\in A$.
		    $a\in{\bar{A}}\:\Longleftrightarrow\: \forall\:V\in{\mathcal{V}(a)},\: V\cap A\neq \emptyset$.
		    Ou encore:
		    $a\in{\bar{A}}\:\Longleftrightarrow\: \forall\:r>0,\: B_{0}(a,r)\cap A\neq \emptyset$.

		2.  Soit $x\in \overline{A}$.
		    Prouvons que $\exists(x_n)_{n\in\mathbb N}$ telle que, $\forall n\in\mathbb N,\ x_n\in A$ et $\displaystyle\lim_{n\to+\infty}x_n=x$.
		    Par hypothèse, $\forall\:r>0,\: B_{0}(a,r)\cap A\neq \emptyset$.
		    Donc $\forall n\in\mathbb{N}^*$, $B_0(x,\dfrac{1}{n})\cap A\neq \emptyset$.
		    C’est-à-dire $\forall n\in\mathbb{N}^*$, $\exists\:x_n\in B_0(x,\dfrac{1}{n})\cap A$.
		    On fixe alors, pour tout entier naturel $n$ non nul, un tel $x_n$.
		    Ainsi, la suite $(x_n)_{n\in\mathbb{N}^{*}}$ est une suite à valeurs dans $A$ et $\forall n\in\mathbb{N}^*$, $||x_n-x||<\dfrac{1}{n}$.
		    C’est-à-dire la suite $(x_n)_{n\in\mathbb{N}^{*}}$ converge vers $x$.
		    Soit $x\in E$. On suppose que $\exists(x_n)_{n\in\mathbb N}$ telle que $\forall n\in\mathbb N$, $x_n\in A$ et $\displaystyle\lim_{n\to+\infty}x_n=x$.
		    Prouvons que $x\in\bar{A}$.
		    Soit $V\in{\mathcal{V}(x)}$. Alors, $\exists\:\varepsilon> 0$ tel que $B_0(x,\varepsilon )\subset V$.
		    On fixe un tel $\varepsilon$ strictement positif.
		    $\displaystyle\lim_{n\to+\infty}x_n=x$ donc $\exists\:N\in\mathbb{N}$ tel que $\forall n\in\mathbb{N}$, $n\geqslant N \Longrightarrow ||x_n-x||< \varepsilon$.
		    On fixe un tel entier $N$.
		    Donc, comme $(x_n)$ est à valeurs dans $A$, on en déduit que $\forall n\in\mathbb{N}$, $n\geqslant N \Longrightarrow x_n\in B_0(x,\varepsilon )\cap A$.
		    Or $B_0(x,\varepsilon )\subset V$, donc $\forall n\in\mathbb{N}$, $n\geqslant N \Longrightarrow x_n\in V\cap A$, c’est-à-dire $V\cap A\neq \emptyset$.
		    On peut en conclure que $x\in \bar{A}$.

		3.  $\bar{A} \subset E$ et $0_E  \in \bar{A}$ car $0_E  \in A$ et $A \subset \bar{A}$.
		    Soit $(x,y) \in \left( \bar{A}\right) ^2$ et $\lambda \in \mathbb{K}$.
		    D’après 1., Il existe deux suites $(x_n )$ et $(y_n )$ d’éléments de $A$ convergeant respectivement vers $x$ et $y$.
		    On a alors $\lim\limits_{n\to +\infty}^{}\left(  x_n  + \lambda y_n\right)   = x + \lambda y$.
		    Or $A$ est un sous-espace vectoriel de $E$ et $\forall n\in\mathbb{N}$, $(x_n,y_n)\in A^2$ , donc $x_n  + \lambda y_n  \in A$.
		    On en déduit que la suite $( x_n  + \lambda y_n )_{n\in\mathbb{N}}$ est à valeurs dans $A$ et converge vers $x + \lambda y$.
		    On a bien $x + \lambda y \in \bar{A}$.

		4.  On suppose que $A$ partie non vide et convexe de $E$. Prouvons que $\overline{A}$ est convexe.
		    Soit $(x,y)\in{\left( \overline{A}\right) ^2}$. Soit $t\in{[0,1]}$.
		    Prouvons que $z=tx+(1-t)y\in{\overline{A}}$.
		    $x\in{\overline{A}}$, donc il existe une suite $(x_n)$ à valeurs dans $A$ telle que $\mathop {\lim }\limits_{n \to  + \infty } {x_n} = x$.
		    $y\in{\overline{A}}$, donc il existe une suite $(y_n)$ à valeurs dans $A$ telle que $\mathop {\lim }\limits_{n \to  + \infty } {y_n} = y$.
		    On pose $\forall n \in \mathbb{N},{z_n} = t{x_n} + (1 - t){y_n}$.
		    $\forall n \in \mathbb{N}$, $x_n\in{A}$, $y_n\in{A}$ et $A$ est convexe, donc $z_n\in{A}$. De plus $\mathop {\lim }\limits_{n \to  + \infty } {z_n} = z$.
		    Donc $z$ est limite d’une suite à valeurs dans $A$, c’est-à-dire $z\in{\overline{A}}$.

!!! exercice "EXERCICE 35 analyse  "
	=== "Enoncé"  


		$E$ et $F$ désignent deux espaces vectoriels normés.

		1.  Soient $f$ une application de $E$ dans $F$ et $a$ un point de $E$.

		    On considère les propositions suivantes:

		    P1.

		    :   $f$ est continue en $a$.

		    P2.

		    :   Pour toute suite $(x_n)_{n\in\mathbb{N}}$ d’éléments de $E$ telle que $\displaystyle\lim_{n\to+\infty}x_n=a$, alors $\displaystyle\lim_{n\to+\infty}f(x_n)=f(a)$.

		    Prouver que les propositions P1 et P2 sont équivalentes.

		2.  Soit $A$ une partie dense dans $E$, et soient $f$ et $g$ deux applications continues de $E$ dans $F$.

		    Démontrer que si, pour tout $x\in A$, $f(x)=g(x)$, alors $f=g$.


	=== "Corrigé"  


		1.  Prouvons que $P1.\Longrightarrow P2.$.
		    Supposons $f$ continue en $a$.
		    Soit $(x_n )_{n\in\mathbb{N}}$ une suite d’éléments de $E$ convergeant vers $a$. Prouvons que $\displaystyle\lim_{n\to+\infty}f(x_n)=f(a)$.
		    Soit $\varepsilon  > 0$.
		    Par continuité de $f$ en $a$, $\exists\:\alpha  > 0$/ $\forall x \in E,\left\| {x - a} \right\| \leqslant \alpha  \Rightarrow \left\| {f(x) - f(a)} \right\| \leqslant \varepsilon$.(\*)
		    On fixe un tel $\alpha$ strictement positif.
		    Par convergence de $(x_n )_{n\in\mathbb{N}}$ vers $a$, $\exists\:N\in\mathbb{N}$ / $\forall n \in \mathbb{N},n \geqslant N \Rightarrow \left\| {x_n  - a} \right\| \leqslant \alpha$.
		    On fixe un $N$ convenable.
		    Alors, d’après (\*), $\forall n \in \mathbb{N},n \geqslant N \Rightarrow \left\| {f(x_n ) - f(a)} \right\| \leqslant \varepsilon$.
		    On peut donc conclure que $\lim\limits_{n\to+\infty}^{}f(x_n )=f(a)$.
		    Prouvons que $P2.\Longrightarrow P1.$
		    Supposons $P2.$ vraie.
		    Raisonnons par l’absurde en supposant que $f$ non continue en $a$.
		    C’est-à-dire $\exists\:\varepsilon >0$ / $\forall\:\alpha>0$, $\exists\:x\in E$ tel que $||x-a||\leqslant \alpha$ et $||f(x)-f(a)||> \varepsilon$.
		    On fixe un tel $\varepsilon$ strictement positif.
		    Alors, $\forall\:n\in\mathbb{N}^*$, en prenant $\alpha=\dfrac{1}{n}$, il existe $x_n\in E$ tel que $||x_n-a||\leqslant \dfrac{1}{n}$ et $||f(x_n)-f(a)||> \varepsilon$. (\*)
		    Comme $\forall\:n\in\mathbb{N}^*$, $||x_n-a||\leqslant\dfrac{1}{n}$, la suite $(x_n)_{n\in\mathbb{N}^*}$ ainsi construite converge vers $a$.
		    Donc, d’après l’hypothèse, la suite $(f(x_n))_{n\in\mathbb{N}^*}$ converge vers $f(a)$.
		    Donc $\exists\:N\in\mathbb{N}^{*}$ tel que $\forall\:n\in\mathbb{N}$, $n\geqslant N\Longrightarrow ||f(x_n)-f(a)||\leqslant\dfrac{\varepsilon}{2}$.
		    Ainsi, on obtient une contradiction avec (\*).

		2.  Soit $x \in E$.
		    Puisque la partie $A$ est dense dans $E$, il existe une suite $(x_n )_{n\in\mathbb{N}}$ d’éléments de $A$ telle que $\lim\limits_{n\to +\infty}^{}x_n= x$.
		    On a alors : $\forall n \in \mathbb{N},f(x_n ) = g(x_n )$.
		    Et en passant à la limite, sachant que $f$ et $g$ sont continues sur $E$, on obtient $f(x) = g(x)$.

!!! exercice "EXERCICE 36 analyse  "
	=== "Enoncé"  


		Soient $E$ et $F$ deux espaces vectoriels normés sur le corps $\mathbb R$.

		1.  Démontrer que si $f$ est une application linéaire de $E$ dans $F$, alors les propriétés suivantes sont deux à deux équivalentes:

		    P1.

		    :   $f$ est continue sur $E$.

		    P2.

		    :   $f$ est continue en $0_E$.

		    P3.

		    :   $\exists k>0$ tel que : $\forall x\in E, \left\Vert f(x)\right\Vert_{F} \leqslant k\left\Vert x\right\Vert_E$.

		2.  Soit $E$ l’espace vectoriel des applications continues de $[0;1]$ dans $\mathbb{R}$ muni de la norme définie par: $\Vert f\Vert_{\infty}=\sup\limits_{x\in[0;1]}|f(x)|$ . On considère l’application $\varphi$ de $E$ dans $\mathbb{R}$ définie par: $\varphi(f)=\displaystyle\int_0^1 f(t)\text{d}t$.

		    Démontrer que $\varphi$ est linéaire et continue.


	=== "Corrigé"  


		1.  P1 $\Rightarrow$ P2 de manière évidente.
		    Prouvons que P2 $\Rightarrow$ P3.
		    Supposons $f$ continue en $0_E$.
		    Pour $\varepsilon  = 1 > 0$, il existe $\alpha  > 0$ tel que $\forall x \in E,\left\| {x - 0_E } \right\| \leqslant \alpha  \Rightarrow \left\| {f(x) - f(0_E )} \right\| \leqslant 1$.
		    Soit $x \in E$
		    Si $x \ne 0_E$, posons $y = \dfrac{\alpha }{{\left\| x \right\|}}x$. Puisque $\left\| y \right\| = \alpha$, on a $\left\| {f(y)} \right\| \leqslant 1$.
		    Donc, par linéarité de $f$ on obtient $\left\| {f(x)} \right\| \leqslant \dfrac{1}{\alpha}\left\| x \right\|$.
		    Si $x = 0_E$ l’inégalité précédente est encore vérifiée.
		    En prenant alors $k = \dfrac{1}{\alpha}$, on obtient le résultat voulu.
		    Prouvons que P3 $\Rightarrow$ P1.
		    Supposons que $\exists k>0$ tel que $\forall x\in E, \left\Vert f(x)\right\Vert \leqslant k\left\Vert x\right\Vert$ .
		    Comme $f$ est linéaire, $\forall (x,y )\in E^2$, $\left\| {f(y) - f(x)} \right\| = \left\| {f(y - x)} \right\| \leqslant k\left\| {y - x} \right\|$.
		    La fonction $f$ est alors lipschitzienne, donc continue sur $E$.

		2.  L’application $\varphi$ est une forme linéaire par linéarité de l’intégrale et continue car:
		    $\forall \:f\in E$, $\left| {\varphi (f)} \right| = \left| {\displaystyle\int_0^1 {f(t)\,{\mathrm{d}}t} } \right| \leqslant \displaystyle\int_0^1 {\left| {f(t)} \right|\,{\mathrm{d}}t}  \leqslant \displaystyle\int_0^1 {\left\| f \right\|} \mathrm{d}t = \left\| f \right\|$.

!!! exercice "EXERCICE 37 analyse  "
	=== "Enoncé"  


		On note $E$ l’espace vectoriel des applications continues de $[0;1]$ dans $\mathbb{R}$.
		On pose: $\forall \:f\in E$, $N_{\infty}(f)=\sup\limits_{x\in[0;1]} |f(x)|$ et $N_1(f)=\displaystyle\int_0^1|f(t)| \text{d}t$.

		1.  1.  Démontrer que $N_{\infty}$ et $N_1$ sont deux normes sur $E$.

		    2.  Démontrer qu’il existe $k>0$ tel que, pour tout $f$ de $E$, $N_1(f)\leq k N_{\infty}(f)$.

		    3.  Démontrer que tout ouvert pour la norme $N_1$ est un ouvert pour la norme $N_{\infty}$.

		2.  Démontrer que les normes $N_1$ et $N_{\infty}$ ne sont pas équivalentes.


	=== "Corrigé"  


		1.  1.  Prouvons que $N_{\infty}$ est une norme sur $E$.
		        $\forall \:f\in E$, $|f$\| est positive et continue sur le segment $\left[0,1 \right]$ donc $f$ est bornée et donc $N_{\infty}(f)$ existe et est positive.
		        i) Soit $f\in E$ telle que $N_{\infty}(f)=0$.
		        Alors, $\forall \:t\in \left[ 0,1\right]$, $|f(t)|=0$, donc $f=0$.
		        ii) Soit $\lambda\in\mathbb{R}$. Soit $f\in E$.
		        Si $\lambda=0$ alors $N_{\infty}(\lambda f)=0=|\lambda|N_{\infty}(f)$.
		        Si $\lambda \neq 0$ :
		        $\forall \:t\in \left[ 0,1\right]$, $|\lambda f(t)|= |\lambda||f(t)|\leqslant|\lambda|N_{\infty}(f)$.
		        Donc $N_{\infty}(\lambda f)\leqslant |\lambda|N_{\infty}(f)$.(1)
		        $\forall \:t\in \left[ 0,1\right]$, $|f(t)|=\dfrac{1}{|\lambda|}|\lambda f(t)|\leqslant\dfrac{1}{|\lambda|}N_{\infty}(\lambda f)$.
		        Donc $N_{\infty}(f) \leqslant\dfrac{1}{|\lambda|}N_{\infty}(\lambda f)$.
		        C’est-à-dire, $|\lambda|N_{\infty}(f)\leqslant N_{\infty}(\lambda f)$. (2)
		        Donc, d’après (1) et (2), $N_{\infty}(\lambda f)= |\lambda|N_{\infty}(f)$.
		        iii) Soit $(f,g)\in E^2$.
		        $\forall \:t\in \left[ 0,1\right]$,$|(f+g)(t)|\leqslant |f(t)|+|g(t)|\leqslant N_{\infty}(f)+N_{\infty}(g)$.
		        Donc $N_{\infty}(f+g)\leqslant N_{\infty}(f)+N_{\infty}(g)$.
		        On en déduit que $N_{\infty}$ est une norme.
		        Prouvons que $N_1$ est une norme sur $E$.
		        $\forall \:f\in E$, $|f|$ est continue et positive sur $\left[ 0,1\right]$ donc $N_1(f)$ existe et est positive.
		        i) Soit $f\in E$ telle que $N_1(f)=0$.
		        Or $|f|$ est continue et positive sur $\left[ 0,1\right]$, donc $|f|$ est nulle.
		        C’est-à-dire $f=0$.
		        ii) Soit $\lambda\in \mathbb{R}$. Soit $f\in E$.
		        $N_1(\lambda f)=\displaystyle\int\limits_{0}^{1}|\lambda f(t)|\mathrm{d}t=|\lambda|\displaystyle\int\limits_{0}^{1}| f(t)|\mathrm{d}t=|\lambda|N_1(f)$.
		        iii) Soit $(f,g)\in E^2$.
		        $\forall \:t\in \left[ 0,1\right]$, $|(f+g)(t)|\leqslant |f(t)|+|g(t)|$. Donc, par linéarité de l’intégrale, $N_1(f+g)\leqslant N_1(f)+N_1(g)$.
		        On en déduit que $N_1$ est une norme sur $E$.

		    2.  $k = 1$ convient car, $\forall \:f\in E$, $\displaystyle\int_0^1 {\left| {f(t)} \right|\,{\mathrm{d}}t}  \leqslant \displaystyle\int_0^1{ N_{\infty}(f){\mathrm{d}}t}=N_{\infty}(f)$.

		    3.  L’application identité de $E$, muni de la norme $N_\infty$, vers $E$, muni de la norme $N_1$, est continue car linéaire et vérifiant $\forall\: f\in E$, $N_1 (f) \leqslant kN_\infty  (f)$.
		        L’image réciproque d’un ouvert par une application continue étant un ouvert, on en déduit que :
		        un ouvert pour la norme $N_1$ est un ouvert pour la norme $N_\infty$.
		        On peut aussi raisonner de façon plus élémentaire par inclusion de boules et retour à la définition d’un ouvert.

		2.  Pour $f_n (t) = t^n$, on a $N_{_1 } (f_n ) = \dfrac{1}{{n + 1}}$ et $N_\infty  (f_n ) = 1$, donc $\lim\limits_{n\to +\infty}^{}\dfrac{N_{\infty}(f_n)}{N_1(f_n)}=+\infty$.
		    Donc ces deux normes ne sont donc pas équivalentes.

!!! exercice "EXERCICE 38 analyse  "
	=== "Enoncé"  


		On note $\mathbb{R}[X]$ l’espace vectoriel des polynômes à coefficients réels.
		On pose : $\forall\:P\in\mathbb{R}[X]$, $N_1(P)=\displaystyle\sum\limits_{i=0}^{n} |a_i|$ et $N_{\infty}(P)=\underset{0\leq i\leq n}{\max}|a_i|$ où $P=\displaystyle\sum_{i=0}^na_i X^i$ avec $n\geqslant \deg P$.

		1.  1.  Démontrer que $N_{\infty}$ est une norme sur $\mathbb{R}[X]$.
		        Dans la suite de l’exercice, on admet que $N_1$ est une norme sur $\mathbb{R}[X]$.

		    2.  Démontrer que tout ouvert pour la norme $N_{\infty}$ est un ouvert pour la norme $N_1$.

		    3.  Démontrer que les normes $N_1$ et $N_{\infty}$ ne sont pas équivalentes.

		2.  On note $\mathbb{R}_k[X]$ le sous-espace vectoriel de $\mathbb{R}[X]$ constitué par les polynômes de degré inférieur ou égal à $k$. On note $N'_1$ la restriction de $N_1$ à $\mathbb{R}_k[X]$ et $N'_{\infty}$ la restriction de $N_{\infty}$ à $\mathbb{R}_k[X]$.

		    Les normes $N'_1$ et $N'_{\infty}$ sont-elles équivalentes?


	=== "Corrigé"  


		1.  1.  On pose $E=\mathbb{R}[X]$.
		        Montrons que $N_{\infty}$ est une norme sur E.
		        Par définition, $N_{\infty}(P)\geqslant 0$.
		        i) Soit $P=\displaystyle\sum\limits_{i=0}^{n}a_iX^i\in E$ tel que $N_{\infty}(P)=0$.
		        C’est-à-dire $\max\limits_{0\leq i\leq n}|a_i|=0$, donc, $\forall\:i\in \llbracket 0,n\rrbracket$, $|a_i|=0$.
		        On en déduit que $P=0$.
		        ii) Soit $P=\displaystyle\sum\limits_{i=0}^{n}a_iX^i\in E$ et $\lambda \in\mathbb{R}$.
		        $N_{\infty}(\lambda P)=\max\limits_{0\leq i\leq n}|\lambda|\:|a_i|=|\lambda|N_{\infty}( P)$.
		        iii) Soit $(P,Q)\in E^2$.
		        On considère un entier $n$ tel que $n\geqslant \max(\deg P, \deg Q)$.
		        Alors, $P=\displaystyle\sum\limits_{i=0}^{n}a_iX^i$ et $Q=\displaystyle\sum\limits_{i=0}^{n}b_iX^i$.
		        Ainsi, $P+Q=\displaystyle\sum\limits_{i=0}^{n}(a_i+b_i)X^i$ et $N_{\infty}(P+Q)=\underset{0\leqslant i\leqslant n}{\max}
		        |a_i+b_i|$.
		        Or, $\forall\:i\in \llbracket 0, n\rrbracket$, $|a_i+b_i|\leqslant |a_i|+|b_i|\leqslant
		        N_{\infty}(P)+N_{\infty}(Q)$.
		        Donc, $N_{\infty}(P+Q)\leqslant N_{\infty}(P)+N_{\infty}(Q)$.
		        On en déduit que $N_{\infty}$ est une norme.

		    2.  L’application identité de $\mathbb{R}\left[ X \right]$, muni de la norme $N_1$, vers $\mathbb{R}\left[ X \right]$, muni de la norme $N_{\infty}$, est continue car linéaire et vérifiant, $\forall P \in \mathbb{R}\left[ X \right],N_{\infty} (P) \leqslant N_1 (P)$.
		        L’image réciproque d’un ouvert par une application continue étant un ouvert, on en déduit qu’un ouvert pour la norme $N_{\infty}$ est un ouvert pour la norme $N_1$.
		        On peut aussi raisonner, de façon plus élémentaire, par inclusion de boules et retour à la définition d’un ouvert.

		    3.  Pour $P_n  = 1 + X + X^2  +  \cdots  + X^n$ on a $N_1 (P_n ) = n + 1$ et $N_{\infty} (P_n ) = 1$.
		        Donc $\lim\limits_{n\to +\infty}^{}\dfrac{N_1(P_n)}{N_{\infty} (P_n )}=+\infty$.
		        On en déduit que les normes $N_1$ et $N_{\infty}$ ne sont pas équivalentes.

		2.  En dimension finie, toutes les normes sont équivalentes, en particulier $N'_1$ et $N'_{\infty}$.

!!! exercice "EXERCICE 39 analyse  "
	=== "Enoncé"  


		On note $l^2$ l’ensemble des suites $x=(x_n)_{n\in\mathbb{N}}$ de nombres réels telles que la série $\displaystyle\sum x_n^2$ converge.

		1.  1.  Démontrer que, pour $x=(x_n)_{n\in\mathbb{N}} \in l^2$ et $y=(y_n) _{n\in\mathbb{N}}\in l^2$, la série $\displaystyle\sum x_ny_n$ converge.
		        On pose alors $(x|y)=\displaystyle\sum_{n=0}^{+\infty} x_ny_n$.

		    2.  Démontrer que $l^2$ est un sous-espace vectoriel de l’espace vectoriel des suites de nombres réels.

		    Dans la suite de l’exercice, on admet que $(\:|\:)$ est un produit scalaire dans $l^2$.
		    On suppose que $l^2$ est muni de ce produit scalaire et de la norme euclidienne associée.

		2.  Soit $p\in\mathbb{N}$. Pour tout $x=(x_n)\in l^2$, on pose $\varphi(x)=x_p$.
		    Démontrer que $\varphi$ est une application linéaire et continue de $l^2$ dans $\mathbb{R}$.

		3.  On considère l’ensemble $F$ des suites réelles presque nulles c’est-à-dire l’ensemble des suites réelles dont tous les termes sont nuls sauf peut-être un nombre fini de termes.
		    Déterminer $F^{\perp}$ (au sens de $(\:|\:)$).
		    Comparer $F$ et $\left( F^{\perp}\right) ^{\perp}$.


	=== "Corrigé"  


		1.  1.  Soit $(x,y)\in \left( l^2\right)^2$ avec $x=(x_n)_{n\in\mathbb{N}}$ et $y=(y_n)_{n\in\mathbb{N}}$.
		        $\forall\:n\in\mathbb{N}$, $|x_ny_n|\leqslant\dfrac{1}{2}\left( x_n^2+y_n^2\right)$.
		        Or $\displaystyle\sum x_n^2$ et $\displaystyle\sum y_n^2$ convergent donc, par critère de majoration des séries à termes positifs, $\displaystyle\sum x_ny_n$ converge absolument, donc converge.

		    2.  La suite nulle appartient à $l^2$.
		        Soit $(x,y)\in \left( l^2\right)^2$ avec $x=(x_n)_{n\in\mathbb{N}}$ et $y=(y_n)_{n\in\mathbb{N}}$. Soit $\lambda \in \mathbb{R}$.
		        Montrons que $z=x+\lambda y\in l^2$.
		        On a $z=(z_n)_{n\in\mathbb{N}}$ avec $\forall\:n\in\mathbb{N}$, $z_n=x_n+\lambda y_n$.
		        $\forall\:n\in\mathbb{N}$, $z_n^2=(x_n+\lambda y_n)^2=x_n^2+\lambda^2y_n^2+2\lambda x_ny_n$.(1)
		        Par hypothèse, $\displaystyle\sum x_n^2$ et $\displaystyle\sum y_n^2$ convergent et d’après 1.(a), $\displaystyle\sum x_ny_n$ converge.
		        Donc, d’après $(1)$, $\displaystyle\sum z_n^2$ converge.
		        Donc $z\in l^2$.

		        On en déduit que $l^2$ est un sous-espace vectoriel de l’ensemble des suites réelles.

		2.  Soit $(x,y)\in l^2$ où $x=(x_n)_{n\in\mathbb{N}}$ et $y=(y_n)_{n\in\mathbb{N}}$. Soit $\lambda\in\mathbb{R}$.
		    On pose $z=x+\lambda y$ avec $z=(z_n)_{n\in\mathbb{N}}$.
		    On a $\forall\:n\in\mathbb{N}$, $z_n=x_n+\lambda y_n$.
		    Ainsi, $\varphi(x+\lambda y)=\varphi(z)=z_p=x_p+\lambda y_p=\varphi (x)+\lambda\varphi(y)$.
		    Donc $\varphi$ est linéaire sur $l^2$.(\*)
		    $\forall\:x=(x_n)\in l^2$, $|x_p|^2\leqslant\displaystyle\sum\limits_{n=0}^{+\infty}x_n^2$, donc $|x_p|\leqslant ||x||$.
		    Donc $\forall\:x=(x_n)_{n\in\mathbb{N}}\in l^2$, $|\varphi(x)|=|x_p|\leqslant ||x||$ (\*\*)
		    D’après (\*) et (\*\*), $\varphi$ est continue sur $l^2$.

		3.  On remarque déjà que $F\subset l^2$.
		    Analyse:
		    Soit $x=(x_n)_{n\in\mathbb{N}}\in F^{\perp}$.
		    Alors $\forall\:y\in F$, $(x|y)=0$.
		    Soit $p\in\mathbb{N}$.
		    On considère la suite $y=(y_n)_{n\in\mathbb{N}}$ de $F$ définie par:
		    $\forall\:n\in\mathbb{N}$, $y_n=\left\lbrace \begin{array}{ll}
		    1&\:\text{si}\:n=p\\
		    0&\:\text{sinon}
		    \end{array}\right.$
		    $y\in F$, donc $(x|y)=0$, donc $x_p=0$.
		    On en déduit que, $\forall\:p\in\mathbb{N}$, $x_p=0$.
		    C’est-à-dire $x=0$.
		    Synthèse:
		    la suite nulle appartient bien à $F^{\perp}$.
		    Conclusion: $F^{\perp}=\left\lbrace 0\right\rbrace$.
		    Ainsi, $(F^{\perp})^{\perp}=l^2$.
		    On constate alors que $F\neq (F^{\perp})^{\perp}$.

!!! exercice "EXERCICE 40 analyse  "
	=== "Enoncé"  


		Soit $A$ une algèbre de dimension finie admettant $e$ pour élément unité et munie d’une norme notée \|\|\|\|.
		On suppose que : $\forall (u,v)\in A^2$, $||u.v||\leqslant||u||.||v||$.

		1.  Soit $u$ un élément de $A$ tel que $\Vert u\Vert<1$.

		    1.  Démontrer que la série $\displaystyle\sum u^n$ est convergente.

		    2.  Démontrer que $(e-u)$ est inversible et que $(e-u)^{-1}=\displaystyle\sum_{n=0}^{+\infty}u^n$.

		2.  Démontrer que, pour tout $u\in A$, la série $\displaystyle\sum\dfrac{u^n}{n!}$ converge.


	=== "Corrigé"  


		1.  1.  Soit $u$ un élément de $A$ tel que $\Vert u\Vert<1$.
		        D’après les hypothèses, on a $||u^2||\leqslant||u||^2$.
		        On en déduit, par récurrence, que $\forall n \in \mathbb{N}^{*},\left\| {u^n } \right\| \leqslant \left\| u \right\|^n$.
		        Puisque $\left\| u \right\| < 1$, la série numérique $\displaystyle\sum {\left\| u \right\|^n }$ est convergente et, par comparaison des séries à termes positifs, on peut affirmer que la série vectorielle $\displaystyle\sum {u^n }$ est absolument convergente.
		        Puisque l’algèbre $A$ est de dimension finie, la série $\displaystyle\sum {u^n }$ converge.

		    2.  Pour tout $N \in \mathbb{N}$, on a $(e - u)\displaystyle\sum\limits_{n = 0}^N {u^n }  = e - u^{N + 1}$. (1)
		        L’application $\varphi :\begin{array}{lll}
		        A&\longrightarrow &A\\
		        x&\longmapsto &(e-u)x
		        \end{array}$ est linéaire.
		        Et, comme $A$ est de dimension finie, on en déduit que $\varphi$ est continue sur $A$.
		        Posons alors pour tout $n\in\mathbb{N}$, $S_N=\displaystyle\sum\limits_{n = 0}^N {u^n }$ et $S=\displaystyle\sum\limits_{n = 0}^{+\infty} {u^n }$.
		        $\lim\limits_{N\to +\infty}^{}S_N=S$ (d’après 1.a) et $\varphi$ est continue sur $A$ donc, par caractérisation séquentielle de la continuité, $\lim\limits_{N\to +\infty}^{}\varphi (S_N)=\varphi(S)$.
		        C’est-à-dire $\lim\limits_{N\to +\infty}^{}(e - u)\displaystyle\sum\limits_{n = 0}^N {u^n }=(e - u)\displaystyle\sum\limits_{n = 0}^{ + \infty } {u^n }$.(2)
		        De plus, $\left\| {u^{N + 1} } \right\| \leqslant \left\| u \right\|^{N + 1}  \underset{N\to +\infty}{\longrightarrow} 0$.
		        Donc $\lim\limits_{N\to +\infty}^{}\left( e-u^{N+1}\right) =e$.(3)
		        Ainsi, d’après (1), (2) et (3), on en déduit que: $(e - u)\displaystyle\sum\limits_{n = 0}^{ + \infty } {u^n }  = e$.
		        On prouve, de même, que $\left( {\displaystyle\sum\limits_{n = 0}^{ + \infty } {u^n } }\right)  (e - u) = e$.
		        Et donc, $e - u$ est inversible avec $(e - u)^{ - 1}  = \displaystyle\sum\limits_{n = 0}^{ + \infty } {u^n }$.

		2.  On a $\left\| {\dfrac{{u^n }}{{n!}}} \right\| \leqslant \dfrac{{\left\| u \right\|^n }}{{n!}}$. De plus, la série exponentielle $\displaystyle\sum \dfrac{||u||^n}{n!}$ converge.
		    Donc, par comparaison des séries à termes positifs, la série vectorielle $\displaystyle\sum \dfrac{u^n}{n!}$ est absolument convergente et donc convergente, car $A$ est de dimension finie.

!!! exercice "EXERCICE 41 analyse  "
	=== "Enoncé"  


		Énoncer quatre théorèmes différents ou méthodes permettant de prouver qu’une partie d’un espace vectoriel normé est fermée et, pour chacun d’eux, donner un exemple concret d’utilisation dans $\mathbb{R}^2$.
		Les théorèmes utilisés pourront être énoncés oralement à travers les exemples choisis.  
		**Remarques** :

		1.  On utilisera au moins une fois des suites.

		2.  On pourra utiliser au plus une fois le passage au complémentaire.

		3.  Ne pas utiliser le fait que $\mathbb{R}^2$ et l’ensemble vide sont des parties ouvertes et fermées.


	=== "Corrigé"  


		1.  Soit $E$ et $F$ deux espaces vectoriels normés.
		    Soit $f:E\longrightarrow F$ une application continue.
		    L’image réciproque d’un fermé de $F$ par $f$ est un fermé de $E$.  
		    **Exemple**: $A=\left\lbrace (x,y)\in{\mathbb{R}^{2}}\:/\:xy=1\right\rbrace$ est un fermé de $\mathbb{R}^2$ car c’est l’image réciproque du fermé $\left\lbrace 1 \right\rbrace$ de $\mathbb{R}$ par l’application continue $f:\begin{array}{ll}
		     \mathbb{R}^{2}&\longrightarrow \mathbb{R}\\
		    (x,y)&\longmapsto xy
		    \end{array}$.

		2.  Soit $E$ un espace vectoriel normé. Soit $F\subset E$.
		    $F$ est un fermé de $E$ si et seulement si $\complement_EF$ est un ouvert de $E$.  
		    **Exemple**: $B=\left\lbrace (x,y)\in\mathbb{R}^2\:/\:x^2+y^2\geqslant1\right\rbrace$ est un fermé de $\mathbb{R}^2$ car $\complement_{\mathbb{R}^2}B$ est un ouvert de $\mathbb{R}^2$.
		    En effet, $\complement_{\mathbb{R}^2}B=\left\lbrace (x,y)\in\mathbb{R}^2\:/\:x^2+y^2<1\right\rbrace=B_o(0,1)$ où $B_o(0,1)$ désigne la boule ouverte de centre 0 et de rayon 1 pour la norme euclidienne sur $\mathbb{R}^2$.
		    Puis, comme toute boule ouverte est un ouvert, on en déduit que $\complement_{\mathbb{R}^2}B$ est un ouvert.

		3.  Caractérisation séquentielle des fermés:
		    Soit $A$ une partie d’un espace vectoriel normé $E$.
		    $A$ est un fermé de $E$ si et seulement si, pour toute suite $(x_n)$ à valeurs dans $A$ telle que $\lim\limits_{n\to +\infty}^{}x_n=x$, alors $x\in A$.  
		    **Exemple**: $C=\left\lbrace (x,y)\in{\mathbb{R}^{2}}\:/\:xy\geqslant 1\right\rbrace$ est un fermé.
		    En effet, soit $\left( \left( x_n,y_n\right) \right)  _{n\in{\mathbb{N}}}$ une suite de points de $C$ qui converge vers $(x,y)$.
		    $\forall\;n\: \in{\mathbb{N}}\:,\: x_ny_n \geqslant 1$, donc, par passage à la limite, $xy\geqslant 1$ donc $(x,y)\in{C}$.

		4.  Une intersection de fermés d’un espace vectoriel normé $E$ est un fermé de $E$.  
		    **Exemple**: $D=\left\lbrace (x,y)\in{\mathbb{R}^{2}}\:/\:xy\geqslant 1\:\text{et}\: x\geqslant 0\right\rbrace$.
		    On pose $D_1=\left\lbrace (x,y)\in{\mathbb{R}^{2}}\:/\:xy\geqslant 1\right\rbrace$ et $D_2=\left\lbrace (x,y)\in{\mathbb{R}^{2}}\:/\: x\geqslant 0\right\rbrace$.
		    D’après 3., $D_1$ est un fermé.
		    $D_2$ est également un fermé.
		    En effet, $D_2$ est l’image réciproque du fermé $\left[ 0,+\infty\right[$ de $\mathbb{R}$ par l’application continue $f:\begin{array}{ll}
		    \mathbb{R}^2&\longrightarrow \mathbb{R}\\
		    (x,y)&\longmapsto x
		    \end{array}$.
		    On en déduit que $D=D_1\cap D_2$ est un fermé de $E$.

		**Remarque**:
		On peut aussi utiliser le fait qu’un produit de compacts est un compact et qu’un ensemble compact est fermé.
		Exemple: $E=\left[ 0;1\right] \times\left[2;5 \right]$ est un fermé de $\mathbb{R}^2$.
		En effet, comme $\left[0;1 \right]$ et $\left[ 2;5\right]$ sont fermés dans $\mathbb{R}$ et bornés, ce sont donc des compacts de $\mathbb{R}$.
		On en déduit que $E$ est un compact de $\mathbb{R}^2$ donc un fermé de $\mathbb{R}^2$.

!!! exercice "EXERCICE 42 analyse  "
	=== "Enoncé"  


		On considère les deux équations différentielles suivantes:
		$2xy'-3y=0$ $(H)$
		$2xy'-3y=\sqrt{x}$ $(E)$

		1.  Résoudre l’équation $(H)$ sur l’intervalle $\left]  0,+\infty\right[$.

		2.  Résoudre l’équation $(E)$ sur l’intervalle $\left]  0,+\infty\right[$.

		3.  L’équation $(E)$ admet-elle des solutions sur l’intervalle $\left[ 0,+\infty\right[$?


	=== "Corrigé"  


		1.  On trouve comme solution de l’équation homogène sur $\left]  0,+\infty\right[$ la droite vectorielle engendrée par $x\longmapsto x^{\frac{3}{2}}$.
		    En effet, une primitive de $x\longmapsto\dfrac{3}{2x}$ sur $\left] 0,+\infty\right[$ est $x\longmapsto\dfrac{3}{2}\ln x$.

		2.  On utilise la méthode de variation de la constante en cherchant une fonction $k$ telle que $x\longmapsto k(x)x^\frac{3}{2}$ soit une solution de l’équation complète $(E)$ sur $\left]  0,+\infty\right[$.
		    On arrive alors à $2k'(x)x^\frac{5}{2}=\sqrt{x}$ et on choisit $k(x)=-\dfrac{1}{2x}$.
		    Les solutions de $(E)$ sur $\left]  0,+\infty\right[$ sont donc les fonctions $x\longmapsto kx^\frac{3}{2}-\dfrac{1}{2}\sqrt{x}$ avec $k\in\mathbb{R}$.

		3.  Si on cherche à prolonger les solutions de $(E)$ sur $\left[  0,+\infty\right[$, alors le prolongement par continuité ne pose pas de problème en posant $f(0)=0$.
		    Par contre, aucun prolongement ne sera dérivable en 0 car $\dfrac{f(x)-f(0)}{x-0}=k\sqrt{x}-\dfrac{1}{2}\dfrac{1}{\sqrt{x}}\underset{x\to 0}{\longrightarrow}-\infty$.
		    Conclusion: l’ensemble des solutions de l’équation différentielle $2xy'-3y=\sqrt{x}$ sur $\left[  0,+\infty\right[$ est l’ensemble vide.

!!! exercice "EXERCICE 43 analyse  "
	=== "Enoncé"  


		Soit $x_0 \in \mathbb{R}$.
		On définit la suite $(u_n)$ par $u_0=x_0$ et, $\forall n\in\mathbb{N}\:,\: u_{n+1}=\mathrm{Arctan}(u_n)$.

		1.  1.  Démontrer que la suite $(u_n)$ est monotone et déterminer, en fonction de la valeur de $x_0$, le sens de variation de $(u_n)$.

		    2.  Montrer que $(u_n)$ converge et déterminer sa limite.

		2.  Déterminer l’ensemble des fonctions $h$, continues sur $\mathbb{R}$, telles que : $\forall x \in \mathbb{R}$, $h(x)=h(\mathrm{Arctan}\: x)$.


	=== "Corrigé"  


		On pose $f(x)=\mathrm{Arctan}\: x$.

		1.  1.  **Premier cas**: Si $u_1<u_0$
		        Puisque la fonction $f:x \mapsto \mathrm{Arctan}\: x$ est strictement croissante sur $\mathbb{R}$ alors $\mathrm{Arctan}(u_1)< \mathrm{Arctan}(u_0)$ c’est-à-dire $u_2 < u_1$.
		        Par récurrence, on prouve que $\forall n \in{\mathbb{N}} \:,\: u_{n+1} < u_n$. Donc la suite $(u_n)$ est strictement décroissante.  
		        **Deuxième cas**: Si $u_1>u_0$
		        Par un raisonnement similaire, on prouve que la suite $(u_n)$ est strictement croissante.  
		        **Troisième cas** : Si $u_1=u_0$
		        La suite $(u_n)$ est constante.

		        Pour connaître les variations de la suite $(u_n)$, il faut donc déterminer le signe de $u_1-u_0$, c’est-à-dire le signe de $\mathrm{Arctan} (u_0)-u_0$.
		        On pose alors $g(x)=\mathrm{Arctan} x -x$ et on étudie le signe de la fonction $g$.
		        On a $\forall x\in{\mathbb{R} }$, $g'(x)=\dfrac{-x^2}{1+x^2}$ et donc $\forall\:x\in\mathbb{R}^*$, $g'(x)<0$.
		        Donc $g$ est strictement décroissante sur $\mathbb{R}$ et comme $g(0)=0$ alors :
		        $\forall x\in{ \left]  0,+\infty\right[ }$, $g(x)< 0$ et $\forall x\in{ \left]-\infty,0\right[ }$, $g(x)> 0$.
		        On a donc trois cas suivant le signe de $x_0$:
		        - Si $x_0>0$, la suite$(u_n)$ est strictement décroissante.
		        - Si $x_0=0$, la suite $(u_n)$ est constante.
		        - Si $x_0<0$, la suite$(u_n)$ est strictement croissante.

		    2.  La fonction $g$ étant strictement décroissante et continue sur $\mathbb{R}$, elle induit une bijection de $\mathbb{R}$ sur $g(\mathbb{R})=\mathbb{R}$.
		        $0$ admet donc un unique antécédent par $g$ et, comme $g(0)=0$, alors 0 est le seul point fixe de $f$.
		        Donc si la suite $(u_n)$ converge, elle converge vers 0, le seul point fixe de $f$.  
		        **Premier cas**: Si $u_0>0$
		        L’intervalle $\left]  0,+\infty\right[$ étant stable par $f$, on a par récurrence, $\forall n\in\mathbb{N}$, $u_n > 0$. Donc la suite $(u_n)$ est décroissante et minorée par $0$, donc elle converge et ce vers 0, unique point fixe de $f$.  
		        **Deuxième cas**: Si $u_0<0$
		        Par un raisonnement similaire, on prouve que $(u_n)$ est croissante et majorée par 0, donc elle converge vers 0.   
		        **Troisième cas**: Si $u_0=0$
		        La suite $(u_n)$ est constante.

		    Conclusion: $\forall\:u_0\in\mathbb{R}$, $(u_n)$ converge vers 0.

		2.  Soit $h$ une fonction continue sur $\mathbb{R}$ telle que, $\forall x \in \mathbb{R}$, $h(x)=h(\mathrm{Arctan}\: x)$.
		    Soit $x \in \mathbb{R}$.
		    Considérons la suite $(u_n)$ définie par $u_0=x$ et $\forall n\in\mathbb{N}\:,\: u_{n+1}=\mathrm{Arctan}(u_n)$.
		    On a alors $h(x)=h(u_0)=h(\mathrm{Arctan}(u_0))=h(u_1)=h(\mathrm{Arctan}(u_1))=h(u_2)=\dots$.
		    Par récurrence, on prouve que, $\forall n \in {\mathbb{N}}$, $h(x)=h(u_n)$.
		    De plus $\lim\limits_{n\rightarrow+\infty} h(u_n)=h(0)$ par convergence de la suite $(u_n)$ vers $0$ et par continuité de $h$.
		    On obtient ainsi: $h(x)=h(0)$ et donc $h$ est une fonction constante.
		    Réciproquement, toutes les fonctions constantes conviennent.
		    Conclusion: Seules les fonctions constantes répondent au problème.

!!! exercice "EXERCICE 44 analyse  "
	=== "Enoncé"  


		Soit $E$ un espace vectoriel normé. Soient $A$ et $B$ deux parties non vides de $E$.

		1.  1.  Rappeler la caractérisation de l’adhérence d’un ensemble à l’aide des suites.

		    2.  Montrer que : $A \subset B \Longrightarrow \overline{A} \subset \overline{B}$.

		2.  Montrer que : $\overline{A \cup B}=\overline{A} \cup \overline{B}$.  
		    **Remarque**: une réponse sans utiliser les suites est aussi acceptée.

		3.  1.  Montrer que : $\overline{A \cap B} \subset \overline{A} \cap \overline{B}$.

		    2.  Montrer, à l’aide d’un exemple, que l’autre inclusion n’est pas forcément vérifiée (on pourra prendre $E=\mathbb{R}$).


	=== "Corrigé"  


		Soit $E$ un espace vectoriel normé. On note $A$ et $B$ deux parties non vides de $E$.

		1.  1.  $x \in \overline{A}$ si et seulement si il existe une suite à valeurs dans $A$ qui converge vers $x$.

		    2.  On suppose $A \subset B$. Prouvons que $\overline{A}\subset \overline{B}$.
		        Soit $x \in \overline{A}$.
		        Il existe une suite $(u_n)_{n\in\mathbb{N}}$ telle que $\forall n\in {\mathbb{N}}$, $u_n \in{A}$ et $\lim\limits_{n\rightarrow +\infty}u_n=x$.
		        Or $A\subset B$, donc, $\forall n\in {\mathbb{N}}$, $u_n \in{B}$ et $\lim\limits_{n\rightarrow +\infty}u_n=x$ .
		        Donc $x \in \overline{B}$.

		2.  D’après la question précédente,
		    $A \subset A \cup B$, donc $\overline{A} \subset \overline{A \cup B}$.
		    $B \subset A \cup B$, donc $\overline{B} \subset \overline{A \cup B}$.
		    Donc $\overline{A} \cup \overline{B} \subset \overline{A \cup B}$.
		    Prouvons que $\overline{A\cup B} \subset \overline{A}\cup\overline{B}$.
		    Soit $x \in \overline{A \cup B}$.
		    Il existe une suite $(u_n)_{n\in\mathbb{N}}$ telle que, $\forall n\in {\mathbb{N}}$, $u_n \in{A\cup B}$ et $\lim\limits_{n\rightarrow +\infty}u_n=x$.
		    On considère les ensembles $A_1=\lbrace n\in \mathbb{N}\:\text{tels que}\: u_n \in{A}\rbrace$ et $A_2=\lbrace n\in \mathbb{N}\:\text{tels que}\: u_n \in{B}\rbrace$.
		    Comme $\forall n\in {\mathbb{N}}$, $u_n \in{A\cup B}$, $A_1$ ou $A_2$ est de cardinal infini.
		    On peut donc extraire de $(u_n)_{n\in\mathbb{N}}$ une sous-suite $(u_{\varphi(n)})_{n\in\mathbb{N}}$ à valeurs dans $A$ ou une sous-suite $(u_{\varphi(n)})_{n\in\mathbb{N}}$ à valeurs dans $B$ telle que $\lim\limits_{n\rightarrow +\infty}u_{\varphi(n)}=x$.
		    Donc $x\in{\overline{A}\cup\overline{B}}$.  
		    **Remarque**:On peut aussi prouver que $\overline{A\cup B} \subset \overline{A}\cup\overline{B}$ sans utiliser les suites:
		    $\overline{A}$ et $\overline{B}$ sont fermés, donc $\overline{A}\cup \overline{B}$ est un fermé contenant $A\cup B$. Or $\overline{A\cup B}$ est le plus petit fermé contenant $A\cup B$, donc $\overline{A\cup B} \subset \overline{A}\cup \overline{B}$.

		3.  1.  D’après la question 1. ,
		        $A \cap B \subset A$, donc $\overline{A \cap B} \subset \overline{A}$.
		        $A \cap B \subset B$, donc $\overline{A \cap B} \subset \overline{B}$.
		        Donc $\overline{A\cap B}\subset \overline{A}\cap \overline{B}$.

		 **Autre méthode**:
		        Comme $A\subset\overline{A}$ et $B\subset\overline{B}$ alors $A\cap B\subset\overline{A}\cap\overline{B}$.
		        Comme $\overline{A}\cap \overline{B}$ est un fermé contenant $A\cap B$, alors par minimalité de $\overline{A\cap B}$, on a $\overline{A\cap B}\subset\overline{A}\cap \overline{B}$.

		2.  $A=]0,1[$ et $B=]1,2[$.
		        $\overline{A\cap B}=\emptyset$ et $\overline{A}\cap \overline{B}=\lbrace1\rbrace$.

!!! exercice "EXERCICE 45 analyse  "
	=== "Enoncé"  


		**Les questions 1. et 2. sont indépendantes**.
		Soit $E$ un $\mathbb{R}$-espace vectoriel normé. Soit $A$ une partie non vide de $E$.
		On note $\overline{A}$ l’adhérence de $A$.

		1.  1.  Donner la caractérisation séquentielle de $\overline{A}$.

		    2.  Prouver que, si $A$ est convexe, alors $\overline{A}$ est convexe.

		2.  On pose : $\forall x \in E,\:{d_A}(x) = \mathop {\inf }\limits_{a \in A} \left\| {x - a} \right\|$.

		    1.  Soit $x \in{E}$. Prouver que $d_{A}(x) = 0\Longrightarrow x \in \overline A$.

		    2.  On suppose que $A$ est fermée et que: $\forall (x,y) \in{E^2}$, $\forall t\in{[0,1]}$, $d_A(tx+(1-t)y)\leqslant td_A(x)+(1-t)d_A(y)$.
		        Prouver que $A$ est convexe.


	=== "Corrigé"  


		1.  1.  Soit $A$ une partie d’un ensemble $E$.
		        $x\in{\overline{A}} \Longleftrightarrow$ il existe une suite $(x_n)_{n\in\mathbb{N}}$ à valeurs dans $A$ telle que $\mathop {\lim }\limits_{n \to  + \infty } {x_n} = x$.

		    2.  On suppose que $A$ est une partie non vide et convexe de $E$. Prouvons que $\overline{A}$ est convexe.
		        Soit $(x,y)\in{\left( \overline{A}\right) ^2}$. Soit $t\in{[0,1]}$.
		        Prouvons que $z=tx+(1-t)y\in{\overline{A}}$.
		        $x\in{\overline{A}}$ donc, il existe une suite $(x_n)_{n\in\mathbb{N}}$ à valeurs dans $A$ telle que $\mathop {\lim }\limits_{n \to  + \infty } {x_n} = x$.
		        $y\in{\overline{A}}$ donc, il existe une suite $(y_n)_{n\in\mathbb{N}}$ à valeurs dans $A$ telle que $\mathop {\lim }\limits_{n \to  + \infty } {y_n} = y$.
		        On pose : $\forall n \in \mathbb{N},{z_n} = t{x_n} + (1 - t){y_n}$.
		        $\forall n \in \mathbb{N}$, $x_n\in{A}$, $y_n\in{A}$ et $A$ est convexe, donc $z_n\in{A}$. De plus $\mathop {\lim }\limits_{n \to  + \infty } {z_n} = z$.
		        Donc $z$ est limite d’une suite à valeurs dans $A$, c’est-à-dire $z\in{\overline{A}}$.

		2.  1.  Soit $A$ une partie non vide de $E$. Soit $x \in{E}$ tel que $d_A(x)=0$.
		        Par définition de la borne inférieure, nous avons : $\forall \epsilon>0$, $\exists \:a \in {A} \text{ tel que } \|x-a\|<\epsilon$.
		        Donc, $\forall n\in{\mathbb{N}^*}$, pour $\epsilon = \dfrac{1}{n}$, il existe $a_n \in {A} \text{ tel que } \|x-a_n\|<\frac{1}{n}$.
		        Alors la suite $(a_n)_{n\in{\mathbb{N}^*}}$ ainsi construite est à valeurs dans $A$ et converge vers $x$, donc $x\in{\overline{A}}$.

		    2.  On suppose que $A$ est fermée et que, $\forall (x,y) \in{E^2}$, $\forall t\in{[0,1]}$, $d_A(tx+(1-t)y)\leqslant td_A(x)+(1-t)d_A(y)$.
		        Soit $(x,y)\in{\left( A\right) ^2}$. Soit $t\in{[0,1]}$.
		        Prouvons que $z=tx+(1-t)y\in{A}$.
		        Par hypothèse, on a $d_A(z)\leqslant td_A(x)+(1-t)d_A(y)$.(1)
		        Or $x\in{A}$ et $y\in{A}$, donc $d_A(x)=d_A(y)=0$ et donc, d’après (1), $d_A(z)=0$.
		        Alors, d’après 2.(a), $z\in{\overline{A}}$. Or $A$ est fermée, donc $\overline{A}=A$ et donc $z\in{A}$.

!!! exercice "EXERCICE 46 analyse  "
	=== "Enoncé"  


		On considère la série:$\displaystyle\sum\limits_{n\geqslant1}{}\cos \left( \pi \sqrt{n^2+n+1}\right)$.

		1.  Prouver que, au voisinage de $+\infty$, $\pi\sqrt{n^2+n+1}=n\pi+\dfrac{\pi}{2}+\alpha\dfrac{\pi}{n}+O\left(\dfrac{1}{n^2} \right)$ où $\alpha$ est un réel que l’on déterminera.

		2.  En déduire que $\displaystyle\sum\limits_{n\geqslant 1}{}\cos \left( \pi \sqrt{n^2+n+1}\right)$ converge.

		3.  $\displaystyle\sum\limits_{n\geqslant 1}{}\cos \left( \pi \sqrt{n^2+n+1}\right)$ converge-t-elle absolument?


	=== "Corrigé"  


		1.  $\pi \sqrt{n^2+n+1}=n\pi\sqrt{1+\dfrac{1}{n}+\dfrac{1}{n^2}}$.
		    Or, au voisinage de $+\infty$, $\sqrt{1+\dfrac{1}{n}+\dfrac{1}{n^2}}=1+\dfrac{1}{2}(\dfrac{1}{n}
		    +\dfrac{1}{n^2})-\dfrac{1}{8n^2}+O(\dfrac{1}{n^3})=
		    1+\dfrac{1}{2n}
		    +\dfrac{3}{8n^2}+O(\dfrac{1}{n^3})$.
		    Donc, au voisinage de $+\infty$, $\pi \sqrt{n^2+n+1}=n\pi+\dfrac{\pi}{2}+\dfrac{3}{8}\dfrac{\pi}{n}+O(\dfrac{1}{n^2})$.

		2.  On pose $\forall n \in{\mathbb{N}^*}$, $v_n=\cos \left( \pi \sqrt{n^2+n+1}\right)$.



		    D’après 1., $v_n=\cos\left( n\pi+\dfrac{\pi}{2}+\dfrac{3}{8}\dfrac{\pi}{n}+O(\dfrac{1}{n^2})\right)
		    =(-1)^{n+1}\sin\left(\dfrac{3}{8}\dfrac{\pi}{n}+O(\dfrac{1}{n^2})\right)$.
		    Donc $v_n=\dfrac{3\pi}{8}\dfrac{(-1)^{n+1}}{n}+O(\dfrac{1}{n^2})$.

		    Or $\displaystyle\sum\limits_{n\geqslant 1}{}\dfrac{(-1)^{n+1}}{n}$ converge (d’après le critère spécial des séries alternées) et $\displaystyle\sum\limits_{n\geqslant 1}{}O(\dfrac{1}{n^2})$ converge (par critère de domination), donc $\displaystyle\sum\limits_{n\geqslant 1}{v_n}$ converge.

		3.  D’après le développement asymptotique du 2., on a $|v_n| \underset{+\infty}{\thicksim}
		    \dfrac{3\pi}{8n}$.
		    Or $\displaystyle\sum\limits_{n\geqslant 1}{}\dfrac{1}{n}$ diverge (série harmonique), donc $\displaystyle\sum\limits_{n\geqslant 1}|{v_n}|$ diverge, c’est-à-dire $\sum\limits_{n\geqslant 1}{v_n}$ ne converge pas absolument.

!!! exercice "EXERCICE 47 analyse  "
	=== "Enoncé"  


		Pour chacune des séries entières de la variable réelle suivantes, déterminer le rayon de convergence et calculer la somme de la série entière sur l’intervalle ouvert de convergence:

		1.  $\displaystyle\sum_{n\geqslant 1}^{} \dfrac{3^nx^{2n}}{n}$.

		2.  $\displaystyle\sum a_nx^n$ avec $\left\lbrace \begin{array}{l}
		    a_{2n}=4^n\\
		    a_{2n+1}=5^{n+1}
		    \end{array}\right.$


	=== "Corrigé"  


		1.  On note $R$ le rayon de convergence de $\displaystyle\sum_{n\geqslant 1}^{} \dfrac{3^nx^{2n}}{n}$ et pour tout réel $x$, on pose $u_n(x)=\dfrac{3^nx^{2n}}{n}$.
		    Pour $x$ non nul, $\left|\dfrac{u_{n+1}(x)}{u_n(x)}\right|=\left|\dfrac{3nx^2}{n+1}\right|\underset{n \to +\infty}{\longrightarrow}\left|3x^2\right|$.
		    Donc, d’après la règle de d’Alembert:
		    si $\left|3x^2\right|<1$ c’est-à-dire si $|x|<\dfrac{1}{\sqrt{3}}$ alors $\displaystyle\sum_{n\geqslant 1}^{} \dfrac{3^nx^{2n}}{n}$ converge absolument
		    et si $\left|3x^2\right|>1$ c’est-à-dire si $|x|>\dfrac{1}{\sqrt{3}}$ alors $\displaystyle\sum_{n\geqslant 1}^{} \dfrac{3^nx^{2n}}{n}$ diverge.
		    On en déduit que $R=\dfrac{1}{\sqrt{3}}$.
		    On pose: $\forall\:x\in \left] -\dfrac{1}{\sqrt{3}},\dfrac{1}{\sqrt{3}}\right[$, $S(x)=\displaystyle\sum_{n= 1}^{+\infty} \dfrac{3^nx^{2n}}{n}$.
		    On a: $\forall\:x\in \left] -\dfrac{1}{\sqrt{3}},\dfrac{1}{\sqrt{3}}\right[$, $S(x)=\displaystyle\sum_{n= 1}^{+\infty} \dfrac{(3x^2)^n}{n}$.
		    Or, d’après les développements en séries entières usuels, on a : $\forall\:t\in \left] -1,1\right[$, $\displaystyle\sum_{n= 1}^{+\infty} \dfrac{t^n}{n}=-\ln (1-t)$.
		    Ainsi : $\forall\:x\in \left] -\dfrac{1}{\sqrt{3}},\dfrac{1}{\sqrt{3}}\right[$, $S(x)=-\ln (1-3x^2)$.

		2.  Notons $R$ le rayon de convergence de $\displaystyle\sum a_nx^n$.
		    On considère les séries $\displaystyle\sum a_{2n}x^{2n}=\displaystyle\sum 4^nx^{2n}$ et $\displaystyle\sum a_{2n+1}x^{2n+1}=\displaystyle\sum 5^{n+1}x^{2n+1}$.
		    Notons $R_1$ le rayon de convergence de $\displaystyle\sum 4^nx^{2n}$ et $R_2$ le rayon de convergence de $\displaystyle\sum 5^{n+1}x^{2n+1}$.
		    Le rayon de convergence de $\displaystyle\sum x^n$ vaut 1.
		    Or, $\displaystyle\sum 4^nx^{2n}=\displaystyle\sum (4x^2)^n$.
		    Donc pour $|4x^2|<1$ c’est-à-dire $|x|<\dfrac{1}{2}$, $\displaystyle\sum 4^nx^{2n}$ converge absolument
		    et pour $|4x^2|>1$ c’est-à-dire $|x|>\dfrac{1}{2}$, $\displaystyle\sum 4^nx^{2n}$ diverge.
		    On en déduit que $R_1=\dfrac{1}{2}$.
		    Par un raisonnement similaire et comme $\displaystyle\sum 5^{n+1}x^{2n+1}=5x\displaystyle\sum (5x^2)^n$, on trouve $R_2=\dfrac{1}{\sqrt{5}}$.
		    $\displaystyle\sum a_nx^n$ étant la série somme des séries $\displaystyle\sum a_{2n}x^{2n}$ et $\displaystyle\sum a_{2n+1}x^{2n+1}$, on en déduit, comme $R_1\neq R_2$, que $R=\min(R_1,R_2)=\dfrac{1}{\sqrt{5}}$.
		    D’après ce qui précéde, on en déduit également que:
		    $\forall\:x\in \left] -\dfrac{1}{\sqrt{5}},\dfrac{1}{\sqrt{5}}\right[$, $S(x)=\displaystyle\sum_{n= 0}^{+\infty} a_nx^n=\displaystyle\sum_{n= 0}^{+\infty}(4x^2)^n+5x\sum_{n= 0}^{+\infty}(5x^2)^n=\dfrac{1}{1-4x^2}+\dfrac{5x}{1-5x^2}$.

!!! exercice "EXERCICE 48 analyse  "
	=== "Enoncé"  


		$C^{0}\left( \left[ 0,1\right] ,\mathbb{R}\right)$ désigne l’espace vectoriel des fonctions continues sur $\left[ 0,1\right]$ à valeurs dans $\mathbb{R}$.
		Soit $f\in C^{0}\left( \left[ 0,1\right] ,\mathbb{R}\right)$ telle que : $\forall n\in \mathbb{N},\:
		\displaystyle\int_{0}^{1}t^{n}f\left( t\right) \mathrm{d}t=0$.

		1.  Énoncer le théorème de Weierstrass d’approximation par des fonctions polynomiales.

		2.  Soit $\left( P_{n}\right)$ une suite de fonctions polynomiales convergeant uniformément sur le segment $\left[ 0,1\right]$ vers $f$.

		    1.  Montrer que la suite de fonctions $\left( P_{n}f\right)$ converge uniformément sur le segment $\left[ 0,1\right]$ vers $f^2$.

		    2.  Démontrer que $\displaystyle\int_{0}^{1} f^2(t)
		         \ \mathrm{d}t=\underset{n\rightarrow +\infty }{\lim }
		        \displaystyle\int_{0}^{1}P_{n}(t)f(t)\mathrm{d}t$.

		    3.  Calculer $\displaystyle\int
		        _{0}^{1}P_{n}\left( t\right) f\left( t\right) \mathrm{d}t$.

		3.  En déduire que $f$ est la fonction nulle sur le segment $\left[ 0,1
		    \right] .$


	=== "Corrigé"  


		1.  Toute fonction $f$ continue sur un segment $\left[ a,b\right]$ et à valeurs réelles ou complexes est limite uniforme sur ce segment d’une suite de fonctions polynomiales.

		2.  On pose : $\forall f\in C^0\left(\left[ 0,1\right] ,\mathbb{R}\right)$, $N_{\infty }(f)=\underset{t\in{\left[0,1 \right] }}{\sup}|f(t)|.$

		    1.  $f$ et $P_n$ étant continues sur $\left[0,1 \right]$,
		        $\forall t\in{\left[0,1 \right]}$, $|P_n(t)f(t)-f^2(t)|=|f(t)||P_n(t)-f(t)|\leqslant N_{\infty}(f) N_{\infty}(P_n-f)$.
		        On en déduit que $N_{\infty}(P_nf-f^2) \leqslant N_{\infty}(f) N_{\infty}(P_n-f)$(1)
		        Or $(P_n)$ converge uniformément vers $f$ sur $\left[ 0,1\right]$ donc $\lim\limits_{n\rightarrow +\infty}N_{\infty}(P_n-f)=0$.
		        Donc, d’après (1), $\lim\limits_{n\rightarrow +\infty}N_{\infty}(P_nf-f^2)=0$.
		        Donc $\left( P_{n}f\right)$ converge uniformément sur $\left[ 0,1\right]$ vers $f^2$.

		    2.  D’après la question précédente, $\left( P_{n}f\right)$ converge uniformément sur le segment $\left[ 0,1\right]$ vers $f^2$.
		        De plus, $\forall n\in\mathbb{N}$, $P_nf$ est continue sur $\left[0,1\right]$.
		        Donc, d’après le théorème d’intégration d’une limite uniforme de fonctions continues, $\underset{n\rightarrow +\infty }{\lim}
		        \displaystyle\int_{0}^{1}P_{n}(t)f(t)\mathrm{d}t=\displaystyle\int_{0}^{1}\:\lim\limits_{n\rightarrow +\infty}(P_n(t)f(t)) \mathrm{d}t=\displaystyle\int_{0}^{1} f^2(t)
		         \ \mathrm{d}t$.

		    3.  Pour tout entier naturel $n$, $P_n\in \mathbb{R}[X]$ donc il existe un entier naturel $d_n$ tel que $P_n=\displaystyle\sum\limits_{k=0}^{d_{n}}a_{n,k}\,t^{k}$ où les $a_{n,k}$ sont des réels.
		        Par linéarité de l’intégrale, on a: $\displaystyle\int_{0}^{1}P_{n}(t)f(t)\mathrm{d}t=
		        \displaystyle\int_{0}^{1}
		        \left(\displaystyle\sum\limits_{k=0}^{d_{n}}a_{n,k}\,t^{k}\right)f(t)\mathrm{d}t
		        =\displaystyle\int_{0}^{1}
		        \left(\displaystyle\sum\limits_{k=0}^{d_{n}}a_{n,k}\,t^k\,f(t)\right)\mathrm{d}t
		        =\displaystyle\sum\limits_{k=0}^{d_n}a_{n,k}\displaystyle\int_{0}^{1}t^kf(t)\mathrm{d}t$.
		        Or, par hypothèse, $\forall k\in \mathbb{N},\:
		        \displaystyle\int_{0}^{1}t^{k}f\left( t\right) \mathrm{d}t=0$, donc $\displaystyle\int_{0}^{1}P_{n}(t)f(t)\mathrm{d}t=0$.

		3.  D’après les questions 2.(b) et 2.(c), on a $\displaystyle\int_{0}^{1} f^2(t)
		     \ \mathrm{d}t=0$.
		    Or $f^2$ est positive et continue sur $\left[0,1\right]$, donc $f^2$ est nulle sur $\left[0,1\right]$ et donc $f$ est nulle sur $\left[0,1\right]$.

!!! exercice "EXERCICE 49 analyse  "
	=== "Enoncé"  


		Soit $\displaystyle\sum a_{n}$ une série absolument convergente à termes complexes. On pose $M=$ $\displaystyle\sum\limits_{n=0}^{+\infty }\left\vert
		a_{n}\right\vert .$
		On pose : $\forall n\in \mathbb{N},\: \forall t\in \left[
		0,+\infty \right[,\: f_{n}\left( t\right) =\dfrac{a_{n}t^{n}}{n!}
		\mathrm{e}^{-t}.$

		1.  1.  Justifier que la suite $(a_n)$ est bornée.

		    2.  Justifier que la série de fonctions $\sum\limits_{}^{ }f_{n}$ converge simplement sur $\left[ 0,+\infty \right[$.
		        On admettra, pour la suite de l’exercice, que $f$: $t\mapsto \displaystyle\sum\limits_{n=0}^{+\infty }f_{n}(t)$ est continue sur $\left[ 0,+\infty \right[ .$

		2.  1.  Justifier que, pour tout $n\in{\mathbb{N}}$, la fonction $g_n:t\mapsto t^n\text{e}^{-t}$ est intégrable sur $\left[ 0,+\infty \right[$ et calculer $\displaystyle\int_{0}^{+\infty }g_n(t)  \mathrm{d}t$.
		        En déduire la convergence et la valeur de $\displaystyle\int_{0}^{+\infty }\left\vert
		        f_{n}\left( t\right) \right\vert \, \mathrm{d}t$.

		    2.  Prouver que $\displaystyle\int_{0}^{+\infty }\left(
		        \displaystyle\sum\limits_{n=0}^{+\infty }\dfrac{a_{n}\,t^{n}}{n!}\,\mathrm{e}^{-t}\right) \mathrm{d}t=$ $\displaystyle\sum\limits_{n=0}^{+\infty }a_{n}.$


	=== "Corrigé"  


		1.  Rappelons que, $\forall x\in{\mathbb{R}}$, $\displaystyle\sum\dfrac{x^n}{n!}$ converge ($\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{x^n}{n!}=\text{e}^x$).

		    1.  $\sum a_n$ converge absolument, donc converge ; donc la suite $(a_n)$ converge vers 0 et donc elle est bornée.
		        Autre méthode: On remarque que $\forall n\in{\mathbb{N}}$ $|a_n|\leqslant M=\displaystyle\sum\limits_{p=0}^{+\infty }\left\vert
		        a_{p}\right\vert .$

		    2.  La suite $(a_n)$ est bornée donc $\exists\:K\in\mathbb{R}^{+}\:/ \: \forall n\in \mathbb{N}, |a_n|\leqslant K$.
		        Soit $t\in\left[ 0,+\infty\right[.$
		        On a : $\forall n\in \mathbb{N},
		         \: \left\vert f_{n}\left( t\right) \right\vert \leqslant
		        K\,\dfrac{t^{n}}{n!}$. Or la série $\displaystyle\sum\frac{t^{n}}{n!}$ converge, donc $\displaystyle\sum f_n(t)$ converge absolument, donc converge.
		        On a donc vérifié la convergence simple de $\displaystyle\sum f_n$ sur $\left[ 0,+\infty\right[$.

		2.  1.  $\forall n\in{\mathbb{N}}$, $g_n$ est continue sur $\left[ 0,+\infty \right[$.
		        De plus, $\lim\limits_{t\rightarrow +\infty}t^2g_n(t)=0$, donc, au voisinage de $+\infty$, $g_n(t)=o\left(\dfrac{1}{t^2}\right)$.
		        Or $t\mapsto \frac{1}{t^2}$ est intégrable sur $\left[ 1,+\infty \right[$, donc $g_n$ est intégrable sur $\left[ 1,+\infty \right[$, donc sur $\left[ 0,+\infty \right[$.
		        On pose alors : $\forall n\in{\mathbb{N}}$, $I_n=\displaystyle\int_{0}^{+\infty}g_n(t) \mathrm{d}t$.
		        En effectuant une intégration par parties, on prouve que $I_{n}=nI_{n-1}$.
		        On en déduit par récurrence que $I_n=n!I_0=n!$.
		        Alors $t\mapsto |f_n(t)|$ est intégrable sur $\left[ 0,+\infty \right[$ car $|f_n(t)|=\dfrac{|a_n|}{n!}g_n(t)$.
		        Et on a $\displaystyle\int_{0}^{+\infty}|f_n(t)| \mathrm{d}t=\dfrac{|a_n|}{n!}n!=|a_n|$.

		    2.  i\) $\forall n\in{\mathbb{N}}$, $f_n$ est continue par morceaux et intégrable sur $\left[ 0,+\infty \right[$ d’après la question 2.(a)
		        ii) $\displaystyle\sum f_n$ converge simplement sur $\left[ 0,+\infty \right[$ et a pour somme $f=\displaystyle\sum\limits_{n=0}^{+\infty }f_{n}$ d’après 1.(b).
		        iii) $f$ est continue par morceaux sur $\left[ 0,+\infty \right[$ car continue sur $\left[ 0,+\infty \right[$ d’après la question 1.(b) (admis)
		        iv) $\displaystyle\sum\displaystyle\int_{0}^{+\infty }\left\vert
		        f_{n}\left( t\right) \right\vert  \mathrm{d}t=$ $\displaystyle\sum\limits\left\vert a_{n}\right\vert$ et $\sum |a_n|$ converge par hypothèse, donc $\displaystyle\sum\displaystyle\int_{0}^{+\infty }\left\vert
		        f_{n}\left( t\right) \right\vert  \mathrm{d}t$ converge.
		        Alors, d’après le théorème d’intégration terme à terme pour les séries de fonctions, $f$ est intégrable sur $\left[ 0,+\infty \right[$ et on a : $\displaystyle\displaystyle\int_{0}^{+\infty }\left( \displaystyle\sum\limits_{n=0}^{+\infty }\frac{
		        a_{n}\,t^{n}}{n!}\,e^{-t}\right)  \mathrm{d}t=\displaystyle\sum\limits_{n=0}^{+\infty
		        }\displaystyle\int_{0}^{+\infty }\dfrac{a_{n}\,t^{n}}{n!}\,e^{-t} \mathrm{d}t=\sum
		        \limits_{n=0}^{+\infty }\dfrac{a_{n}\,}{n!}\displaystyle\int_{0}^{+\infty
		        }t^{n}\,e^{-t} \mathrm{d}t =\displaystyle\sum\limits_{n=0}^{+\infty }\dfrac{a_{n}\,}{n!}
		        \,n!=\displaystyle\sum\limits_{n=0}^{+\infty }a_{n}.$

!!! exercice "EXERCICE 50 analyse  "
	=== "Enoncé"  


		On considère la fonction $\;F : x \mapsto  \displaystyle\int_{0}^{+\infty} \dfrac{\text{e}^{-2\,t}}{x+t} \mathrm{d}t$.

		1.  Prouver que $F$ est définie et continue sur $\left]  0;+\infty\right[$.

		2.  Prouver que $x\longmapsto xF(x)$ admet une limite en $+\infty$ et déterminer la valeur de cette limite.

		3.  Déterminer un équivalent, au voisinage de $+\infty$, de $F(x)$.


	=== "Corrigé"  


		1.  Notons $f:
		     \left\lbrace
		     \begin{array}{ccc}
		     ]0;+\infty[\times [0;+\infty[&\mapsto&\mathbb{R}\\
		     (x,t)&\mapsto &\frac{e^{-2\,t}}{x+t}
		     \end{array}
		     \right.$

		    1.  $\forall x \in ]0;+\infty[,\;t\mapsto f(x,t)$ est continue par morceaux sur $[0;+\infty[$.

		    2.  $\forall t \in [0;+\infty[,\;x\mapsto f(x,t) = \dfrac{\text{e}^{-2\,t}}{x+t}$ est continue sur $]0;+\infty[$.

		    3.  Soit $[a,b]$ un segment de $]0;+\infty[$.
		        $\forall x \in [a,b],\;\forall t \in [0;+\infty[,$ $|f(x,t)|\leqslant \dfrac{1}{a}\text{e}^{-2\,t}$ et $\varphi:t\mapsto \dfrac{1}{a}\text{e}^{-2\,t}$ est continue par morceaux, positive et intégrable sur $\left[ 0;+\infty\right[$.
		        En effet, $\lim\limits_{t\to +\infty}t^2\varphi(t)=0$, donc $\:\varphi(t) = \underset{t\to +\infty}{o}\left(\dfrac{1}{t^2}\right)$.
		        Donc $\varphi$ est intégrable sur $\left[ 1,+\infty\right[$, donc sur $\left[ 0;+\infty\right[$ .

		    On en déduit que, d’après le théorème de continuité des intégrales à paramètres,
		    $F : x \mapsto \displaystyle\int_{0}^{+\infty} \frac{\text{e}^{-2\,t}}{x+t} \mathrm{d}t$ est définie et continue sur $]0;+\infty[$.

		2.  $\forall\:x\in\left]0;+\infty\right[$, $xF(x) = \displaystyle\int_{0}^{+\infty} \frac{x}{x+t}\:\text{e}^{-2\,t} \mathrm{d}t$.
		    Posons $\forall\:x\in\left]0;+\infty\right[$, $\forall\:t\in\left[0;+\infty \right[$, $\:h_x(t) = \frac{x}{x+t}\:\text{e}^{-2\,t}$.
		    i) $\forall\:x\in\left]0;+\infty\right[$, $t\longmapsto h_x(t)$ est continue par morceaux sur $\left[ 0,+\infty\right[$.
		    ii) $\forall t \in\left[ 0;+\infty\right[$, $\displaystyle\lim\limits_{x\to+\infty}{}h_x(t)=  \:e^{-2\,t}.$
		    La fonction $h : t\mapsto \:e^{-2\,t}$ est continue par morceaux sur $[0;+\infty[$.
		    iii) $\forall\:x\in\left]0;+\infty\right[$, $\forall t \in [0;+\infty[,\;|h_x(t)| \leqslant  e^{-2\,t}$ et $t \mapsto e^{-2\,t}$ est continue par morceaux, positive et intégrable sur $[0;+\infty[$.
		    Donc, d’après l’extension du théorème de convergence dominée à $(h_x)_{x\in \left] 0;+\infty\right[ }$, $\lim\limits_{x\to+\infty}{}\displaystyle \int_{0}^{+\infty} h_x(t) \mathrm{d}t = \displaystyle \int_{0}^{+\infty} h(t) \mathrm{d}t  =\displaystyle \int_{0}^{+\infty} e^{-2\,t} \mathrm{d}t = \frac{1}{2}$.
		    Conclusion: $\lim\limits_{x\to+\infty}xF(x)= \frac{1}{2}$.

		3.  D’après 2., $\lim\limits_{x\to+\infty}xF(x)= \frac{1}{2}$, donc $F(x) \underset{x\to +\infty}{\thicksim} \frac{1}{2\,x}$.

!!! exercice "EXERCICE 51 analyse  "
	=== "Enoncé"  


		1.  Montrer que la série $\sum\dfrac{(2n)!}{(n!)^{2}2^{4n}(2n+1)}$ converge.

		    On se propose de calculer la somme de cette série.

		2.  Donner le développement en série entière en 0 de $t\longmapsto\dfrac{1}{\sqrt{1-t}}$ en précisant le rayon de convergence.
		    **Remarque**: dans l’expression du développement, on utilisera la notation factorielle.

		3.  En déduire le développement en série entière en 0 de $x\longmapsto\mathrm{Arcsin}\,x$ ainsi que son rayon de convergence.

		4.  En déduire la valeur de $\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{(2n)!}{(n!)^{2}2^{4n}(2n+1)}$ .


	=== "Corrigé"  


		1.  On pose : $\forall n\in\mathbb N, u_{n}=\dfrac{(2n)!}{(n!)^{2}2^{4n}(2n+1)}$.
		    On a : $\forall n\in\mathbb N$, $u_n>0$.
		    $\forall n\in\mathbb N, \dfrac{u_{n+1}}{u_{n}}=\dfrac{(2n+2)(2n+1)(2n+1)}{(n+1)^{2}2^{4}(2n+3)}=\dfrac{(2n+1)^2}{8(n+1)(2n+3)}\underset{+\infty}{\thicksim}\dfrac{1}{4}$ .
		    Ainsi, $\dfrac{u_{n+1}}{u_{n}}\underset{n\to+\infty}{\longrightarrow}\dfrac{1}{4}<1$.

		    Donc, d’après la règle de d’Alembert, $\sum u_{n}$ converge.

		2.  D’après le cours, $\forall\alpha\in\mathbb R$, $u\mapsto(1+u)^{\alpha}$ est développable en série entière en $0$ et le rayon de convergence $R$ de son développement en série entière vaut $1$ si $\alpha\not\in \mathbb{N}.$
		    De plus, $\forall\:u\in \left] -1,1\right[$, $(1+u)^\alpha=1+\sum\limits_{n=1}^{+\infty}\dfrac{\alpha(\alpha-1)...(\alpha-n+1)}{n!}u^n$.
		    En particulier, pour $\alpha=-\dfrac{1}{2}$ et $u=-t$:
		    $R=1$ et $\forall t\in]-1,1[,  \dfrac{1}{\sqrt{1-t}}=1+\displaystyle\sum\limits_{n=1}^{+\infty}\dfrac{(-1)(-3)\cdots\big(-(2n-1)\big)}{2^{n}n!}\,(-t)^{n}$.

		    En multipliant numérateur et dénominateur par $2.4.\ldots2n=2^{n}n!$, on obtient :

		    $\forall t\in]-1,1[,\: \dfrac{1}{\sqrt{1-t}}=1+\displaystyle\sum\limits_{n=1}^{+\infty}\dfrac{(2n)!}{(2^{n}n!)^{2}}\,t^{n}$
		    Conclusion: $R=1$ et $\forall t\in]-1,1[$, $\dfrac{1}{\sqrt{1-t}}=\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{(2n)!}{(2^{n}n!)^{2}}\,t^{n}.$

		3.  D’après la question précédente, en remarquant que : $x\in]-1,1[\Leftrightarrow t=x^{2}\in[0,1[$ et $[0,1[\subset]-1,1[$, il vient:

		    $\forall x\in]-1,1[,\ \,\dfrac{1}{\sqrt{1-x^{2}}}=\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{(2n)!}{(2^{n}n!)^{2}}\,x^{2n}$ avec un rayon de convergence $R=1$.

		    $\mathrm{Arcsin}$ est dérivable sur $]-1,1[$ avec $\mathrm{Arcsin}':x\longmapsto\dfrac{1}{\sqrt{1-x^{2}}}$ .
		    D’après le cours sur les séries entières, on peut intégrer terme à terme le développement en série entière de $x\mapsto\dfrac{1}{\sqrt{1-x^2}}$ et le rayon de convergence est conservé.
		    De plus, on obtient:
		    $\forall x\in]-1,1[,\ \,\mathrm{Arcsin}\,x=\underbrace{\mathrm{Arcsin\,}0}_{=0}+\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{(2n)!}{(2^{n}n!)^{2}(2n+1)}\,x^{2n+1}$ avec un rayon de convergence $R=1$.

		4.  Prenons $x=\dfrac{1}{2}\in]-1,1[$ dans le développement précédent.
		    On en déduit que $\mathrm{Arcsin\,}\left(\dfrac{1}{2}\right)=\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{(2n)!}{2^{2n}(n!)^{2}(2n+1)}\,\dfrac{1}{2^{2n+1}}$ .
		    C’est-à-dire, en remarquant que $\mathrm{Arcsin\,}\left(\dfrac{1}{2}\right)=\dfrac{\pi}{6}$, on obtient $\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{(2n)!}{(n!)^{2}2^{4n}(2n+1)}=\dfrac{\pi}{3}$.

!!! exercice "EXERCICE 52 analyse  "
	=== "Enoncé"  


		Soit $\alpha\in\mathbb R$.
		On considère l’application définie sur $\mathbb{R}^2$ par $f(x,y)=\begin{cases}
		\dfrac{y^{4}}{x^{2}+y^{2}-xy} & \mathrm{si}\  (x,y)\not=(0,0)\\
		\hfil\alpha & \mathrm{si}\  (x,y)=(0,0).
		\end{cases}$

		1.  Prouver que : $\forall(x,y)\in\mathbb R^{2},\:x^{2}+y^{2}-xy\geqslant\dfrac{1}{2}(x^{2}+y^{2})$.

		2.  1.  Justifier que le domaine de définition de $f$ est bien $\mathbb{R}^2$.

		    2.  Déterminer $\alpha$ pour que $f$ soit continue sur $\mathbb R^{2}$.

		3.  Dans cette question, on suppose que $\alpha=0$.

		    1.  Justifier l’existence de $\dfrac{\partial f}{\partial x}$ et $\dfrac{\partial f}{\partial y}$ sur $\mathbb R^{2}\setminus\big\{(0,0)\big\}$ et les calculer.

		    2.  Justifier l’existence de $\dfrac{\partial f}{\partial x}(0,0)$ et $\dfrac{\partial f}{\partial y}(0,0)$ et donner leur valeur.

		    3.  $f$ est-elle de classe ${\cal C}^{1}$ sur $\mathbb R^{2}$?


	=== "Corrigé"  


		-5truemm

		1.  Soit $(x,y)\in\mathbb R^{2}$. $x^{2}+y^{2}-xy-\dfrac{1}{2}(x^{2}+y^{2})=\dfrac{1}{2}(x^{2}+y^{2}-2xy)=\dfrac{1}{2}\left( x-y\right)^2\geqslant 0$.
		    Donc $x^{2}+y^{2}-xy\geqslant\dfrac{1}{2}(x^{2}+y^{2})$.

		2.  1.  Soit $(x,y)\in\mathbb R^{2}$.
		        D’après 1., $x^{2}+y^{2}-xy=0\Longleftrightarrow x^{2}+y^{2}=0\Longleftrightarrow x=y=0$.
		        Ainsi, $f$ est définie sur $\mathbb R^{2}$.

		    2.  D’après les théorèmes généraux, $f$ est continue sur $\mathbb R^{2}\setminus\big\{(0,0)\big\}$.

		        D’après **1.**, pour $(x,y)\not=(0,0),\: 0\leqslant f(x,y)\leqslant\dfrac{2y^{4}}{x^{2}+y^{2}}\leqslant\dfrac{2(x^{2}+y^{2})^{2}}{x^{2}+y^{2}}$ .

		        Ainsi, $0\leqslant f(x,y)\leqslant 2(x^{2}+y^{2})\underset{(x,y)\to(0,0)}{\longrightarrow}0.$
		        Or : $f$ est continue en $(0,0)$ $\Longleftrightarrow$ $f(x,y)\underset{(x,y)\to(0,0)}{\longrightarrow}f(0,0)=\alpha.$
		        Donc : $f$ est continue en $(0,0)$ $\Longleftrightarrow$ $\alpha=0$.
		        Conclusion : $f$ est continue sur $\mathbb{R}^2$ $\Longleftrightarrow$ $\alpha=0$.

		3.  1.  D’après les théorèmes généraux, $f$ est de classe ${\cal C}^{1}$ sur $\mathbb R^{2}\setminus\big\{(0,0)\big\}$.
		        $\forall(x,y)\in\mathbb R^{2}\setminus\big\{(0,0)\big\}$, $\dfrac{\partial f}{\partial x}(x,y)=\dfrac{-y^{4}(2x-y)}{(x^{2}+y^{2}-xy)^{2}}$   et   $\dfrac{\partial f}{\partial y}(x,y)=\dfrac{2y^{5}-3xy^{4}+4x^{2}y^{3}}{(x^{2}+y^{2}-xy)^{2}}$.

		    2.  Pour tout $x\not=0,\:\dfrac{f(x,0)-f(0,0)}{x-0}=0\underset{x\to 0}{\longrightarrow}0$, donc $\dfrac{\partial f}{\partial x}(0,0)$ existe et $\dfrac{\partial f}{\partial x}(0,0)=0$.

		        Pour tout $y\not=0,\: \dfrac{f(0,y)-f(0,0)}{y-0}=y\underset{y\to 0}{\longrightarrow}0$, donc $\dfrac{\partial f}{\partial y}(0,0)$ existe et $\dfrac{\partial f}{\partial y}(0,0)=0$.

		    3.  Pour montrer que $f$ est de classe ${\cal C}^{1}$ sur $\mathbb R^{2}$, montrons que $\dfrac{\partial f}{\partial x}$ et $\dfrac{\partial f}{\partial y}$ sont continues sur $\mathbb R^{2}$.
		        Pour cela, il suffit de montrer qu’elles sont continues en $(0,0)$.

		        $\forall(x,y)\in\mathbb R^{2}\setminus\big\{(0,0)\big\}$, on note $r=\sqrt{x^{2}+y^{2}}$. On a alors $|x|\leqslant r$ et $|y|\leqslant r$.
		        De plus, $(x,y)\to (0,0) \Longleftrightarrow r\to 0$.
		        D’après **1.** et l’inégalité triangulaire,

		        $\left|\dfrac{\partial f}{\partial x}(x,y)-\dfrac{\partial f}{\partial x}(0,0)\right|\leqslant4\,\dfrac{\big|y^{4}(2x-y)\big|}{(x^{2}+y^{2})^{2}}\leqslant4\,\dfrac{r^{4}(2r+r)}{r^{4}}=12r\underset{r\to 0}{\longrightarrow}0$.

		        $\left|\dfrac{\partial f}{\partial y}(x,y)-\dfrac{\partial f}{\partial y}(0,0)\right|\leqslant4\,\dfrac{\big|2y^{5}-3xy^{4}+4x^{2}y^{3}\big|}{(x^{2}+y^{2})^{2}}\leqslant4\,\dfrac{2r^{5}+3r^{5}+4r^{5}}{r^{4}}=36r\underset{r\to 0}{\longrightarrow}0$.

		        Donc $\dfrac{\partial f}{\partial x}$ et $\dfrac{\partial f}{\partial y}$ sont continues en $(0,0)$ et par suite sur $\mathbb R^{2}$.
		        Ainsi, $f$ est de classe ${\cal C}^{1}$ sur $\mathbb R^{2}$.

!!! exercice "EXERCICE 53 analyse  "
	=== "Enoncé"  


		On considère, pour tout entier naturel $n$ non nul, la fonction $f_n$ définie sur $\mathbb{R}$ par $f_n(x)=\dfrac{x}{1+n^4x^4}$.

		1.  1.  Prouver que $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n$ converge simplement sur $\mathbb{R}$.
		        On pose alors : $\forall x\in\mathbb{R}$, $f(x)=\displaystyle\sum\limits_{n=1}^{+\infty}f_n(x)$.

		    2.  Soit $(a,b)\in\mathbb{R}^2$ avec $0<a<b$.
		        $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n$ converge-t-elle normalement sur $\left[a,b \right]$? sur $\left[ a,+\infty\right[$?

		    3.  $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n$ converge-t-elle normalement sur $\left[ 0,+\infty\right[$?

		2.  Prouver que $f$ est continue sur $\mathbb{R}^*$.

		3.  Déterminer $\lim\limits_{x\to+\infty}^{}f(x)$.


	=== "Corrigé"  


		1.  1.  Soit $x\in\mathbb{R}$.
		        Si $x=0$, alors $f_n(0)=0$ et donc $\sum\limits_{n\geqslant 1}^{}f_n(0)$ converge.
		        Si $x \neq 0$, $f_n(x)\underset{+\infty}{\thicksim}\dfrac{1}{n^4x^3}$.
		        Or $\sum\limits_{n\geqslant 1}^{}\dfrac{1}{n^4}$ est une série de Riemann convergente donc, par critère d’équivalence pour les séries à termes de signe constant, $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n(x)$ converge.
		        Conclusion:$\sum\limits_{n\geqslant 1}^{}f_n$ converge simplement sur $\mathbb{R}$.

		    2.  Soit $(a,b)\in\mathbb{R}^2$ tel que $0<a<b$.  
		        $\bullet$ Prouvons que $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n$ converge normalement sur $[a,b]$.
		        $\forall x \in [a,b]$, $|f_n(x)|\leqslant \dfrac{b}{n^4a^4}$(majoration indépendante de $x$).
		        De plus, $\displaystyle\sum\limits_{n\geqslant 1}{}\dfrac{1}{n^4}$ converge (série de Riemann convergente).
		        Donc $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n$ converge normalement sur $[a,b]$.  
		        $\bullet$ Prouvons que $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n$ converge normalement sur $[a,+\infty[$.
		        $\forall x \in [a,+\infty [$, $|f_n(x)|\leqslant \dfrac{x}{n^4x^4}=\dfrac{1}{n^4x^3}\leqslant\dfrac{1}{n^4a^3}$ (majoration indépendante de $x$).  
		        De plus, $\displaystyle\sum\limits_{n\geqslant 1}{}\dfrac{1}{n^4}$ converge (série de Riemann convergente).
		        Donc $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n$ converge normalement sur $[a,+\infty[$.

		    3.  On remarque que $f_n$ est continue sur le compact $[0,1]$, donc $f_n$ est bornée sur $[0,1]$.
		        De plus, d’après 1.(b), $\forall x \in [1,+\infty [$, $|f_n(x)|\leqslant \dfrac{1}{n^4}$, donc $f_n$ est bornée sur $[1,+\infty [$.
		        On en déduit que $f_n$ est bornée sur $\left[ 0,+\infty\right[$ et que $\sup\limits_{x\in \left[ 0,+\infty\right[ }{}|f_n(x)|$ existe.
		        $\forall n\in\mathbb{N}^*$, $\sup\limits_{x\in \left[ 0,+\infty\right[ }{}|f_n(x)|\geqslant f_n(\dfrac{1}{n})=\dfrac{1}{2n}$.
		        Or $\displaystyle\sum\limits_{n\geqslant 1}^{}\dfrac{1}{n}$ diverge (série harmonique).
		        Donc, par critère de minoration des séries à termes positifs, $\displaystyle\sum\limits_{n\geqslant 1}^{}\sup\limits_{\:x\in \left[ 0,+\infty\right[ }{}|f_n(x)|$ diverge.
		        Donc $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n$ ne converge pas normalement sur $\left[ 0,+\infty\right[$.

		 **Autre méthode**:
		        $\forall n\in\mathbb{N}^*$, $f_n$ est dérivable sur $\left]  0,+\infty\right[$ et $\forall x\in \left]  0,+\infty\right[$, $f_n'(x)=\dfrac{1-3n^4x^4}{\left( 1+n^4x^4\right)^2 }$.
		        On en déduit que $f_n$ est croissante sur $\left] 0,\frac{1}{3^{\frac{1}{4}}n} \right]$ et décroissante sur $\left[\frac{1}{3^{\frac{1}{4}}n},+\infty \right[$.
		        $f_n$ étant positive sur $\mathbb{R}$, on en déduit que $f_n$ est bornée.
		        Donc $\sup\limits_{x\in \left[ 0,+\infty\right[ }{}|f_n(x)|$ existe et $\sup\limits_{x\in \left[ 0,+\infty\right[ }{}|f_n(x)|=f_n(\dfrac{1}{3^{\frac{1}{4}}n})=\dfrac{3^{\frac{3}{4}}}{4 n}$.
		        Or $\displaystyle\sum\limits_{n\geqslant 1}^{}\dfrac{1}{n}$ diverge (série harmonique), donc $\displaystyle\sum\limits_{n\geqslant 1}^{}\sup\limits_{\:x\in \left[ 0,+\infty\right[ }{}|f_n(x)|$ diverge.
		        Donc $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n$ ne converge pas normalement sur $\left[ 0,+\infty\right[$.

		2.  $\forall n\in\mathbb{N}^*$, $f_n$ est continue sur $\left]  0,+\infty\right[$.(1)
		    $\displaystyle\sum\limits_{n\geqslant 1}^{}f_n$ converge normalement, donc uniformément, sur tout segment $[a,b]$ inclus dans $\left]0,+\infty \right[$.(2)
		    Donc, d’après (1) et (2), $f$ est continue sur $\left]0,+\infty \right[$.
		    Comme $f$ est impaire, on en déduit que $f$ est également continue sur $\left]-\infty,0\right[$.
		    Conclusion: $f$ est continue sur $\mathbb{R}^*$.

		3.  $\forall n\in\mathbb{N}^*$, $\lim\limits_{x\to+\infty}^{}f_n(x)=0$ car, au voisinage de $+\infty$, $f_n(x)\underset{+\infty}{\thicksim}\dfrac{1}{n^4x^3}$.
		    D’après 1.(b), $\sum\limits_{n\geqslant 1}^{}f_n$ converge normalement, donc uniformément, sur $[1,+\infty[$.
		    Donc, d’après le cours, $f$ admet une limite finie en $+\infty$ et $\lim\limits_{x\to+\infty}^{}f(x)=
		       \lim\limits_{x\to+\infty}\sum\limits_{n=1}^{+\infty}f_n(x)
		       =\displaystyle\sum\limits_{n=1}^{+\infty}\lim\limits_{x\to+\infty}^{}f_n(x)=0$.
		    Conclusion: $\lim\limits_{x\to+\infty}^{}f(x)=0$.

!!! exercice "EXERCICE 54 analyse  "
	=== "Enoncé"  


		Soit $E$ l’ensemble des suites à valeurs réelles qui convergent vers $0$.

		1.  Prouver que $E$ est un sous-espace vectoriel de l’espace vectoriel des suites à valeurs réelles.

		2.  On pose : $\forall\: u=(u_n)_{n\in\mathbb{N}}\in E$, $||u||=\sup\limits_{n\in\mathbb{N}}|u_n|$.

		    1.  Prouver que $||\:.\:||$ est une norme sur $E$.

		    2.  Prouver que : $\forall\: u=(u_n)_{n\in\mathbb{N}}\in E$, $\displaystyle\sum\limits_{}^{}\dfrac{u_n}{2^{n+1}}$ converge.

		    3.  On pose : $\forall\: u=(u_n)_{n\in\mathbb{N}}\in E$, $f(u)=\displaystyle\sum\limits_{n=0}^{+\infty}\dfrac{u_n}{2^{n+1}}$.
		        Prouver que $f$ est continue sur $E$.


	=== "Corrigé"  


		1.  La suite nulle appartient à $E$.
		    Soit $(u,v)=\left( (u_n)_{n\in\mathbb{N}},(v_n)_{n\in\mathbb{N}}\right) \in E^2$. Soit $\alpha \in \mathbb{R}$.
		    Posons $\forall n\in \mathbb{N}$, $w_n=u_n+\alpha v_n$. Montrons que $w\in E$.
		    $u\in E$ donc $\lim\limits_{n\to+\infty}^{}u_n=0$ et $v\in E$ donc $\lim\limits_{n\to+\infty}^{}v_n=0$.
		    On en déduit que $\lim\limits_{n\to+\infty}^{}w_n=0$, donc $w\in E$.
		    On en déduit que $E$ est bien un sous-espace vectoriel de l’espace vectoriel des suites à valeurs réelles.

		2.  1.  $\forall\: u=(u_n)_{n\in\mathbb{N}}\in E$, $||u||$ existe car $\lim\limits_{n\to+\infty}^{}u_n=0$ donc $(u_n)_{n\in\mathbb{N}}$ converge et donc elle est bornée.
		        $||.||$ est à valeurs dans $\mathbb{R}^+$.
		        i) Soit $u=(u_n)_{n\in\mathbb{N}}\in E$ telle que $||u||=0$.
		        Alors $\sup\limits_{n\in\mathbb{N}}|u_n|=0$ c’est-à-dire $\forall n\in\mathbb{N}$, $u_n=0$.
		        Donc $u=0$.
		        ii) Soit $u=(u_n)_{n\in\mathbb{N}}\in E$. Soit $\lambda \in \mathbb{R}$.
		        Si $\lambda=0$
		        $||\lambda u||=|\lambda|||u||=0.$
		        Si $\lambda\neq 0$
		        $\forall n\in \mathbb{N}$, $|\lambda u_n|= |\lambda||u_n|\leqslant|\lambda|||u_n||$ donc $||\lambda u||\leqslant
		        |\lambda|||u||$.(1)
		        $\forall n\in \mathbb{N}$, $|u_n|=|\dfrac{1}{\lambda}||\lambda u_n|\leqslant |\dfrac{1}{\lambda}|.||\lambda u||$ donc $||\lambda u||\geqslant |\lambda |.||u||$.(2)
		        D’après (1) et (2), $||\lambda u||=|\lambda|||u||.$
		        iii) Soit $(u,v)=\left( (u_n)_{n\in\mathbb{N}},(v_n)_{n\in\mathbb{N}}\right) \in E^2$.
		        $\forall n\in \mathbb{N}$, $|u_n+v_n|\leqslant |u_n|+|v_n|\leqslant ||u||+||v||$ donc $||u+v||\leqslant ||u||+||v||.$

		    2.  Soit $u= (u_n)_{n\in\mathbb{N}}\in E$.
		        $\forall n \in\mathbb{N}$, $|u_n|\leqslant ||u||$ donc $\forall n \in\mathbb{N}$, $|\dfrac{u_n}{2^{n+1}}|\leqslant \dfrac{||u||}{2^{n+1}}$.
		        Or $\displaystyle\sum  \dfrac{1}{2^{n+1}}=\dfrac{1}{2}\displaystyle\sum  \left( \dfrac{1}{2 }\right)^n$ converge (série géométrique de raison strictement inférieure à 1 en valeur absolue).
		        Donc, par critère de majoration pour les séries à termes positifs, $\displaystyle\sum |\dfrac{u_n}{2^{n+1}}|$ converge.
		        C’est-à-dire, $\displaystyle\sum \dfrac{u_n}{2^{n+1}}$ converge absolument, donc converge.
		        De plus, $|\displaystyle\sum\limits_{n=0}^{+\infty} \dfrac{u_n}{2^{n+1}}|\leqslant \displaystyle\sum\limits_{n=0}^{+\infty} \dfrac{||u||}{2^{n+1}}=||u||$.

		    3.  $f$ est clairement linéaire.
		        De plus, d’après la questions précédente, $\forall u= (u_n)_{n\in\mathbb{N}}\in E$, $|f(u)|\leqslant ||u||.$
		        On en déduit que $f$ est continue sur $E$.

!!! exercice "EXERCICE 55 analyse  "
	=== "Enoncé"  


		Soit $a$ un nombre complexe.
		On note $E$ l’ensemble des suites à valeurs complexes telles que :
		$\forall\:n\in\mathbb{N}$, $u_{n+2}=2au_{n+1}+4(\mathrm{i}a-1)u_n$ avec $(u_0,u_1)\in  \mathbb{C} ^2$.

		1.  1.  Prouver que $E$ est un sous-espace vectoriel de l’ensemble des suites à valeurs complexes.

		    2.  Déterminer, en le justifiant, la dimension de $E$.

		2.  Dans cette question, on considère la suite de $E$ définie par: $u_0=1$ et $u_1=1$.
		    Exprimer, pour tout entier naturel $n$, le nombre complexe $u_n$ en fonction de $n$.
		    **Indication**: discuter suivant les valeurs de $a$.


	=== "Corrigé"  


		1.  1.  Montrons que $E$ est un sous-espace-vectoriel de l’ensemble des suites à valeurs complexes.
		        La suite nulle appartient à $E$ ( obtenue pour ($u_0,u_1)=(0,0))$.
		        Soit $u=(u_n)_{n\in\mathbb{N}}$ et $v=(v_n)_{n\in\mathbb{N}}$ deux suites de $E$. Soit $\lambda\in\mathbb{C}$.
		        Montrons que $w=u+\lambda v\in E$.
		        On a $\forall\:n\in\mathbb{N}$, $w_n=u_n+\lambda v_n$.
		        Soit $n\in\mathbb{N}$.
		        $w_{n+2}=u_{n+2}+\lambda v_{n+2}$.
		        Or $(u,v)\in E^2$, donc $w_{n+2}=2au_{n+1}+4(ia-1)u_n+\lambda
		        \left( 2av_{n+1}+4(ia-1)v_n\right)$
		        c’est-à-dire $w_{n+2}=2a\left( u_{n+1}+\lambda v_{n+1}\right)+4(ia-1)\left( u_{n}+\lambda v_{n}\right)$
		        ou encore $w_{n+2}=2aw_{n+1}+4(ia-1)w_n$.
		        Donc $w\in E$.
		        Donc $E$ est un sous-espace vectoriel de l’ensemble des suites à valeurs complexes.

		    2.  On considère l’application $\varphi$ définie par:
		        $\varphi:\,
		        \begin{array}{lll}
		        E &\longrightarrow& \mathbb{C}^2\\
		        u=(u_n)_{n\in\mathbb{N}}&\longmapsto &(u_0,u_1)
		        \end{array}$
		        Par construction, $\varphi$ est linéaire et bijective.
		        Donc $\varphi$ est un isomorphisme d’espaces vectoriels.
		        On en déduit que $\dim E=\dim \mathbb{C}^2=2$.

		2.  Il s’agit d’une suite récurrente linéaire d’ordre 2 à coefficients constants.
		    On introduit l’équation caractéristique $(E)$ : $r^2-2ar-4(ia-1)=0.$
		    On a deux possibilités :

		    -   si $(E)$ admet deux racines distinctes $r_1$ et $r_2$, alors $\forall\:n\in\mathbb{N}$, $u_n= \alpha r_1^n+\beta r_2^n$
		        avec $(\alpha,\beta)$ que l’on détermine à partir des conditions initiales.

		    -   si $(E)$ a une unique racine double $r$, alors $\forall\:n\in\mathbb{N}$, $u_n=(\alpha n +\beta) r^n$
		        avec $(\alpha,\beta)$ que l’on détermine à partir des conditions initiales.

		    Le discriminant réduit de $(E)$ est $\Delta'=a^2+4ia-4=(a+2i)^2.$
		    **Premier cas** : $a=-2i$
		    $r=a=-2i$ est racine double de $(E)$.
		    Donc, $\forall\:n\in\mathbb{N}$, $u_n=(\alpha n+\beta )(-2i)^n$.
		    Or $u_0=1$ et $u_1=1$, donc $1=\beta$ et $1=(\alpha +\beta)(-2i)$.
		    On en déduit que $\alpha=\dfrac{i}{2}-1$ et $\beta=1$.

		    **Deuxième cas**: $a\neq -2i$
		    On a deux racines distinctes $r_1=2(a+i)$ et $r_2=-2i$.
		    Donc $\forall\:n\in\mathbb{N}$, $u_n=\alpha \left( 2(a+i)\right)^n +\beta\left(-2i \right) ^n$.
		    Or $u_0=1$ et $u_1=1$, donc $\alpha+\beta=1$ et $2(a+i)\alpha-2i\beta=1$.
		    On en déduit, après résolution, que $\alpha=\dfrac{1+2i}{2a+4i}$ et $\beta=\dfrac{2a+2i-1}{2a+4i}$.

!!! exercice "EXERCICE 56 analyse  "
	=== "Enoncé"  


		On considère la fonction $H$ définie sur $\left] 1;+\infty\right[$ par $H(x)=\displaystyle\int_x^{x^2}\frac{\mathrm{d}t}{\ln t}.$

		1.  Montrer que $H$ est $C^1$ sur $]1;+\infty[$ et calculer sa dérivée.

		2.  Montrer que la fonction $u$ définie par $u(x)= \dfrac{1}{\ln x}-\dfrac{1}{x-1}$ admet une limite finie en $x=1$.

		3.  En utilisant la fonction $u$ de la question 2., calculer la limite en $1^+$ de la fonction $H$.


	=== "Corrigé"  


		1.  Soit $x_0$ un réel de $\left] 1;+\infty\right[$.
		    Posons : $\forall\:x\in\left] 1;+\infty\right[$, $F(x)=\displaystyle\int_{x_0}^{x}\dfrac{\mathrm{d}t}{\ln t}$.
		    $t\longmapsto \dfrac{1}{\ln t}$ est continue sur $\left] 1;+\infty\right[$.
		    Donc, d’après le théorème fondamental du calcul intégral, $F$ est dérivable sur $\left] 1;+\infty\right[$ et $\forall\:x\in\left] 1;+\infty\right[$, $F'(x)=\dfrac{1}{\ln x}$.
		    De plus, $\forall\:x\in\left] 1;+\infty\right[$, $H(x)=F(x^2)-F(x)$ et $\forall \:x\in]1;+\infty[$ on a $[x;x^2]\subset ]1;+\infty[$.
		    On en déduit que $H$ est dérivable sur $]1;+\infty[$.
		    De plus, $\forall\: x \in ]1;+\infty[$, $H'(x)=2x\cdot\dfrac{1}{\ln x^2}-\dfrac{1}{\ln x}=\dfrac{x-1}{\ln x}.$
		    On en déduit que $H$ est de classe $\mathcal{C}^1$ sur $]1;+\infty[$.

		2.  En posant $x=1+h$, $u(x)=v(h)$ avec $v(h)=\frac{h-\ln(1+h)}{h\cdot\ln(1+h)}$.
		    Or au voisinage de 0, $h-\ln(1+h)=\frac{1}{2}h^2+o(h^2)$ donc $h-\ln(1+h)\underset{0}{\thicksim}\frac{1}{2}h^2$.
		    Et $h\ln(1+h)\underset{0}{\thicksim}h^2$ donc $v(h)\underset{0}{\thicksim}\frac{1}{2}$.
		    Donc $\lim\limits_{x\to 1}^{}u(x)=\lim\limits_{h\to 0}^{}v(h)=\frac{1}{2}$.

		3.  En utilisant $u$, on a $H(x)=\displaystyle\int_x^{x^2} u(t)\mathrm{d}t +\displaystyle\int_x^{x^2}\frac{\mathrm{d}t}{t-1}=\displaystyle\int_x^{x^2} u(t)\mathrm{d}t +\ln(x+1)$.(1)
		    $\forall\:x\in\left]1;+\infty \right[$, $u$ est continue sur l’intervalle $\left[x,x^2 \right]$. $u$ est continue sur $]1;+\infty[$ et admet une limite finie en 1, donc $u$ est prolongeable par continuité en 1.
		    Notons $u_1$ ce prolongement continu sur $[1;+\infty[$.
		    Alors, $\forall x \in \left] 1;+\infty\right[$, $\displaystyle\int_x^{x^2} u(t)\mathrm{d}t=\displaystyle\int_x^{x^2} u_1(t)\mathrm{d}t$.(2)
		    On pose alors $\forall\:x\in \left[ 1;+\infty\right[$, $U_1(x)=\displaystyle\int_x^{x^2} u_1(t)\mathrm{d}t$.
		    Pour les mêmes raisons que dans la question 1., $U_1$ est dérivable, donc continue, sur $\left[ 1,+\infty\right[$.
		    Donc $\lim\limits_{x\to 1}^{}U_1(x)=U_1(1)=0$.
		    Donc, d’après (2), $\lim\limits_{x\to 1}\displaystyle\int_x^{x^2} u(t)\mathrm{d}t=\lim\limits_{x\to 1}U_1(x)=0$.
		    On en déduit, d’après (1), que $\lim\limits_{x\to 1}^{}H(x)=\ln2$.

!!! exercice "EXERCICE 57 analyse  "
	=== "Enoncé"  


		1.  Soit $f$ une fonction de $\mathbb{R}^{2}$ dans $\mathbb{R}$.

		    1.  Donner, en utilisant des quantificateurs, la définition de la continuité de $f$ en $(0,0)$.

		    2.  Donner la définition de «$f$ différentiable en $(0,0)$».

		2.  On considère l’application définie sur $\mathbb{R}^2$ par $f(x,y)=\left\lbrace
		      \begin{array}{ll}
		     xy\dfrac{x^2-y^2}{x^2+y^2}& \text{si}\: (x,y)\neq (0,0)\\
		     0 &\text{si} \:(x,y)=(0,0)
		     \end{array}
		     \right.$

		    1.  Montrer que $f$ est continue sur $\mathbb{R}^2$.

		    2.  Montrer que $f$ est de classe $\mathcal {C}^1$ sur $\mathbb{R}^2$.


	=== "Corrigé"  


		1.  1.  $f$ est continue en $(0,0)$ $\Longleftrightarrow$ $\forall\:\varepsilon>0,\; \exists\alpha>0/\: \forall (x,y)\in\mathbb{R}^2,\; \|(x,y)\|<\alpha\Longrightarrow |f(x,y)-f(0,0)|<\varepsilon$.
		        $\|\cdot\|$ désigne une norme quelconque sur $\mathbb{R}^2$ puisque toutes les normes sont équivalentes sur $\mathbb{R}^2$ (espace de dimension finie) .

		    2.  $f$ est différentiable en $(0,0)$ $\Longleftrightarrow$ $\exists L\in\mathcal{L}_{\mathcal{C}}(\mathbb{R}^2,\mathbb{R})$/ au voisinage de $(0,0)$, $f(x,y)=f(0,0)+L(x,y)+o(\|(x,y)\|).$

		        **Remarque** : Comme $\mathbb{R}^2$ est de dimension finie, si $L\in\mathcal{L}(\mathbb{R}^2,\mathbb{R})$ alors $L\in\mathcal{L}_{\mathcal{C}}(\mathbb{R}^2,\mathbb{R})$.

		2.  On notera $\|.\|$ la norme euclidienne usuelle sur $\mathbb{R}^2$.
		    On remarque que $\forall\:(x,y)\in\mathbb{R}^2$, $|x|\leqslant \|(x,y)\|$ et $|y|\leqslant \|(x,y)\|$(\*).

		    1.  $(x,y)\mapsto x^2+y^2$ et $(x,y)\mapsto xy(x^2-y^2)$ sont continues sur $\mathbb{R}^2\backslash\{(0,0)\}$ et $(x,y)\mapsto x^2+y^2$ ne s’annule pas sur $\mathbb{R}^2\backslash\{(0,0)\}$ donc, $f$ est continue sur $\mathbb{R}^2\backslash\{(0,0)\}$.
		        Continuité en (0,0):
		        On a, en utilisant (\*) et l’inégalité triangulaire, $|f(x,y)-f(0,0)|=\left|xy\frac{x^2-y^2}{x^2+y^2}\right|\leqslant |x|.|y|\leqslant \|(x,y)\|^2$.
		        Donc $f$ est continue en $(0,0)$.

		    2.  $f$ est de classe ${\cal C}^1$ sur $\mathbb{R}^2$ si et seulement si $\frac{\partial f}{\partial x}$ et $\frac{\partial f}{\partial y}$ existent sur $\mathbb{R}^2$ et sont continues sur $\mathbb{R}^2$.
		        $f$ admet des dérivées partielles sur $\mathbb{R}^2\backslash\{(0,0)\}$ et elles sont continues sur $\mathbb{R}^2\backslash\{(0,0)\}$.
		        De plus, $\forall\:(x,y)\in\mathbb{R}^2-\left\lbrace (0,0)\right\rbrace$, $\dfrac{\partial f}{\partial x}(x,y)=\dfrac{x^4y+4x^2y^3-y^5}{(x^2+y^2)^{2}}$ et $\dfrac{\partial f}{\partial y}(x,y)=\dfrac{x^5-4x^3y^2-xy^4}{(x^2+y^2)^{2}}.$(\*\*)
		        Existence des dérivées partielles en $(0,0)$:
		        $\forall\:x\in\mathbb{R}^*$, $\frac{f(x,0)-f(0,0)}{x-0}=0$, donc $\lim\limits_{x\rightarrow 0}\frac{f(x,0)-f(0,0)}{x-0}=0$; donc $\frac{\partial f}{\partial x}(0,0)$ existe et $\frac{\partial f}{\partial x}(0,0)=0$.
		        De même, $\forall\:y\in\mathbb{R}^*$, $\frac{f(0,y)-f(0,0)}{y-0}=0$, donc $\lim\limits_{y\rightarrow 0}\frac{f(0,y)-f(0,0)}{y-0}=0$; donc $\frac{\partial f}{\partial y}(0,0)$ existe et $\frac{\partial f}{\partial y}(0,0)=0$.
		        Continuité des dérivées partielles en $(0,0)$:
		        D’après (\*) et (\*\*), $\forall \:(x,y)\in \mathbb{R}^2\backslash\{(0,0)\}$,
		        $\left|\dfrac{\partial f}{\partial x}(x,y)\right|\leqslant \dfrac{6\|(x,y)\|^5}{\|(x,y)\|^4}=6\|(x,y)\|$ et $\left|\dfrac{\partial f}{\partial y}(x,y)\right|\leqslant\dfrac{6\|(x,y)\|^5}{\|(x,y)\|^4}=6\|(x,y)\|$.
		        Donc $\lim\limits_{(x,y)\to (0,0)}\frac{\partial f}{\partial x}(x,y)=0=\frac{\partial f}{\partial x}(0,0)$ et $\lim\limits_{(x,y)\to (0,0)}\frac{\partial f}{\partial y}(x,y)=0=\frac{\partial f}{\partial y}(0,0)$.
		        Donc $\frac{\partial f}{\partial x}$ et $\frac{\partial f}{\partial y}$ sont continues en $(0,0)$.
		        Conclusion:$\frac{\partial f}{\partial x}$ et $\frac{\partial f}{\partial y}$ existent et sont continues sur $\mathbb{R}^2$, donc $f$ est de classe $C^1$ sur $\mathbb{R}^2$.

!!! exercice "EXERCICE 58 analyse  "
	=== "Enoncé"  


		1.  Soit $E$ et $F$ deux $\mathbb{R}$-espaces vectoriels normés de dimension finie.
		    Soit $a\in E$ et soit $f:E \longrightarrow F$ une application.
		    Donner la définition de «$f$ différentiable en $a$».

		2.  Soit $n\in\mathbb{N}^*$. Soit $E$ un $\mathbb{R}$-espace vectoriel de dimension finie $n$.
		    Soit $e=(e_1,e_2,\dots,e_n)$ une base de $E$.
		    On pose : $\forall x\in E,\; \|x\|_{\infty}=\underset{1\leqslant i\leqslant n}{\max}|x_i|,\text{ o\`u}x=\displaystyle\sum_{i=1}^{n}x_ie_i$.
		    On pose : $\forall (x,y)\in E\times E,\; \|(x,y)\|=\max(\|x\|_\infty,\|y\|_\infty)$.

		    On admet que $\|.\|_{\infty}$ est une norme sur $E$ et que $\|.\|$ est une norme sur $E\times E$.
		    Soit $B:E\times E\longrightarrow \mathbb{R}$ une forme bilinéaire sur $E$.

		    1.  Prouver que : $\exists\: C\in\mathbb{R}^+/\:\forall\:(x,y)\in E\times E,\:|B(x,y)|\leqslant C\|x\|_{\infty}\|y\|_{\infty}$.

		    2.  Montrer que $B$ est différentiable sur $E\times E$ et déterminer sa différentielle en tout $(u_0,v_0)\in E\times E$.


	=== "Corrigé"  


		1.  Soit $f:E\mapsto F$ une application. Soit $a\in E$.
		    $f$ est différentiable en $a$ $\Longleftrightarrow$ $\exists L\in\mathcal{L}_{\mathcal{C}}(E,F)$/ au voisinage de $0$, $f(a+h)=f(a)+L(h)+o(\|h\|).$
		    Auquel cas, $f$ est différentiable en $a$ et $df(a)=L$.
		    **Remarque 1**: $||.||$ désigne une norme quelconque sur $E$ car, comme $E$ est de dimension finie, toutes les normes sur $E$ sont équivalentes.
		    **Remarque 2** : Comme $E$ est de dimension finie, si $L\in\mathcal{L}(E,F)$, alors $L\in\mathcal{L}_{\mathcal{C}}(E,F)$.

		2.  1.  Soit $(x,y)\in E^2$.
		        $\exists\:!\:(x_1,x_2,...,x_n)\in \mathbb{R}^n$/$x=\displaystyle\sum\limits_{i=1}^{n}x_ie_i$ et $\exists\:!\:(y_1,y_2,...,y_n)\in \mathbb{R}^n$/$y=\displaystyle\sum\limits_{j=1}^{n}y_je_j$.
		        Par bilinéarité de $B$, on a $B(x,y)=\displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{j=1}^{n}x_iy_jB(e_i,e_j)$.
		        Donc $|B(x,y)|\leqslant \displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{j=1}^{n}|x_i|.|y_j|.|B(e_i,e_j)|
		        \leqslant \left( \displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{j=1}^{n}|B(e_i,e_j)|\right) \|x\|_{\infty}.\|y\|_{\infty}$.
		        Alors $C=\displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{j=1}^{n}|B(e_i,e_j)|$ convient .

		    2.  Soit $(u_0,v_0)\in E\times E$.
		        Par bilinéarité de $B$ on a :
		        $\forall (u,v)\in E\times E$, $B(u_0+u,v_0+v)=B(u_0,v_0)+B(u_0,v)+B(u,v_0)+B(u,v)$.(\*)
		        On pose $L((u,v))=B(u_0,v)+B(u,v_0)$.
		        Vérifions que $L$ est linéaire sur $E\times E$.
		        Soit $(x,y)\in E\times E$. Soit $(x',y')\in E\times E$. Soit $\alpha \in \mathbb{R}$.
		        $L\left( (x,y)+\alpha(x',y')\right) =L((x+\alpha x', y+\alpha y'))
		        =B((u_0,y+\alpha y'))+B((x+\alpha x',v_0)).$
		        Donc par bilinéarité de $B$, $L\left( (x,y)+\alpha(x',y')\right)
		        =B((u_0,y))+\alpha B((u_0,y'))+B((x,v_0))+\alpha B((x',v_0))$.
		        C’est-à-dire $L\left( (x,y)+\alpha(x',y')\right)=L((x,y))+\alpha L((x',y'))$.
		        On en déduit que $L\in\mathcal{L}(E\times E, \mathbb{R})$.
		        Donc, comme $E\times E$ est de dimension finie, $L\in\mathcal{L}_{\mathcal{C}}(E\times E, \mathbb{R})$.(\*\*)
		        De plus, d’après 2.(a), $\exists\: C\in\mathbb{R}^+/\:\forall\:(x,y)\in E^2,\:|B(x,y)|\leqslant C\|(x\|_{\infty}\|y\|_{\infty}$.
		        Donc $\forall\:(x,y)\in E^2,\:|B(x,y)|\leqslant C\|(x,y)\|^2$.
		        On en deduit que, au voisinage de $(0,0)$, $|B(x,y)|=o\left( \|(x,y)\|\right)$.(\*\*\*)
		        D’après (\*),(\*\*) et (\*\*\*), $B$ est différentiable en $(u_0,v_0)$ et $\mathrm{d}B((u_0,v_0))=L$.

!!! exercice "EXERCICE 58 bis analyse  "
	=== "Enoncé"  


		1.  Rappeler, oralement, la définition, par les suites de vecteurs, d’une partie compacte d’un espace vectoriel normé.

		2.  Démontrer qu’une partie compacte d’un espace vectoriel normé est une partie fermée de cet espace.

		3.  Démontrer qu’une partie compacte d’un espace vectoriel normé est une partie bornée de cet espace.
		    **Indication** : On pourra raisonner par l’absurde.

		4.  On se place su $E=\mathbb{R}[X]$ muni de la norme $||\:||_{1}$ définie pour tout polynôme $P=a_0+a_1X+....+a_nX^n$ de $E$ par : $||P||_{1}=\displaystyle\sum_{i=0}^{n}|a_{i}|$.

		    1.  Justifier que $S(0,1)=\left\lbrace  P\in\mathbb{R}[X]\:/\: ||P||_{1}=1\right\rbrace$est une partie fermée et bornée de $E$.

		    2.  Calculer $||X^n-X^m||_{1}$ pour $m$ et $n$ entiers naturels distincts.
		        $S(0,1)$ est-elle une partie compacte de $E$? Justifier.


	=== "Corrigé"  


		1.  Une partie $A$ d’un espace vectoriel normé $(E,||\:||)$ est compacte si de toute suite à valeurs dans $A$ on peut extraire une sous-suite qui converge dans $A$.
		    C’est-à-dire il existe $\varphi:\mathbb{N}\longrightarrow \mathbb{N}$ strictement croissante telle que $\left( x_{\varphi(n)}\right)$ converge vers $l'\in A$.
		    **Remarque**: $\varphi:\mathbb{N}\longrightarrow \mathbb{N}$ étant strictement croissante, on a, par récurrence immédiate, $\forall n\in\mathbb{N},\: \varphi(n)\geqslant n$.

		2.  Soit $(E,||\:||)$ un espace vectoriel normé.
		    Soit $A$ une partie compacte de $E$.
		    Montrons que $A$ est une partie fermée de $E$.
		    C’est-à-dire montrons que toute suite à valeurs dans $A$ qui converge, converge dans $A$.
		    Soit $(u_n)$ une suite à valeurs dans $A$ telle que $(u_n)$ converge vers $l$.
		    $A$ est une partie compacte donc il existe $\varphi:\mathbb{N}\longrightarrow \mathbb{N}$ strictement croissante telle que $\left( x_{\varphi(n)}\right)$ converge vers $l'\in A$.
		    Or, $(x_{n})$ converge vers $l$ donc $\left( x_{\varphi(n)}\right)$ converge vers $l$, en tant que sous-suite de $(x_{n})$ . Par unicité de la limite, $l'=l$.
		    Or, $l'\in A$ donc $l\in A$.

		3.  Soit $(E,||\:||)$ un espace vectoriel normé.
		    **Rappel** : Soit $B$ une partie de $E$.
		    $B$ est bornée si et seulement si $\exists \:M\in\mathbb{R}\:/\: \forall  x \in B, ||x||\leqslant M$.
		    Soit $A$ une partie compacte de $E$.
		    Montrons que $A$ est une partie bornée de $E$.
		    Raisonnons par l’absurde.
		    Supposons que $A$ est non bornée.
		    C’est-à-dire, $\forall M\in \mathbb{R}$, $\exists \:x\in  A\:/\: ||x||> M$.
		    Donc, $\forall n\in \mathbb{N},\:\exists x_n \in A\:/\: ||x_n||>n$ $(*)$
		    $(x_n)$ est une suite à valeurs dans $A$ et $A$ est une partie compacte donc il existe $\varphi:\mathbb{N}\longrightarrow \mathbb{N}$ strictement croissante telle que $\left( x_{\varphi(n)}\right)$ converge vers $l\in A$.
		    Donc, d’après $(*)$, $\forall n \in\mathbb{N}$,$||x_{\varphi(n)}|| >\varphi(n)$.
		    Or $\forall n\in\mathbb{N},\: \varphi(n)\geqslant n$.
		    Donc, $\forall n \in\mathbb{N}$,$||x_{\varphi(n)}|| > n$.
		    Donc, $\displaystyle\lim_{n \rightarrow +\infty}||x_n||=+\infty$.
		    Absurde car $(x_{\varphi(n)})$ converge donc $(x_{\varphi(n)})$ est bornée.

		4.  1.  $\forall x\in S$,$||x||=1$ donc $s$ est bornée.
		        Soit $f:\begin{array}{lll}
		           E&\longrightarrow&\mathbb{R}\\
		           x&\longmapsto &||x||
		           \end{array}$ $\forall (x,y)\in E^2$, $\left| \:||x||-||y||\:\right| \leqslant ||x-y||$ donc $f$ est 1-lipschitzienne.
		        Donc $f$ est continue sur $E$.
		        Or, $S=f^{-1}({1})$ et $\left\lbrace 1\right\rbrace$ est un fermé de $\mathbb{R}$ donc $S$ est une partie fermée de $E$, en tant qu’image réciproque par une application continue d’un fermé.

		    2.  Soit $(m,n)\in\mathbb{N}^2$.
		        $||X^n-X^m||_{1}=2$.
		        Supposons que $S$ soit une partie compacte.
		        $(X^n)$ est une partie compacte de $E$ donc existe $\varphi:\mathbb{N}\longrightarrow \mathbb{N}$ strictement croissante telle que $\left( X^{\varphi(n)}\right)$ converge vers $l \in S$.
		        Alors $\displaystyle \lim_{n\rightarrow +\infty}||X^{\varphi(n)}-X^{\varphi(n+1)}||_{1}=||l-l||=0$.
		        Contredit $||X^{\varphi(n)}-X^{\varphi(n+1)}||_{1}=2$.
		        Donc $S$ est non compact.

!!! exercice "EXERCICE 59 algèbre  "
	=== "Enoncé"  


		Soit $n$ un entier naturel tel que $n\geqslant 2$.
		Soit $E$ l’espace vectoriel des polynômes à coefficients dans $\mathbb{K}$ ($\mathbb{K}=\mathbb{R}$ ou $\mathbb{K}=\mathbb{C}$) de degré inférieur ou égal à $n$.
		On pose : $\forall\:P\in E$, $f\left( P\right)=P-P^{\prime }$.

		1.  Démontrer que $f$ est bijectif de deux manières:

		    1.  sans utiliser de matrice de $f$,

		    2.  en utilisant une matrice de $f$.

		2.  Soit $Q\in E.$ Trouver $P$ tel que $f\left( P\right) =Q$ .

		    **Indication** : si $P\in E$, quel est le polynôme $P^{\left(n+1\right) }$?

		3.  $f$ est-il diagonalisable?


	=== "Corrigé"  


		1.  $f$ est clairement linéaire.(\*) De plus, $\forall \:P\in E\backslash\left\lbrace 0\right\rbrace$, $\deg P'< \deg P$ donc $\deg (P-P')=\deg P$.
		    Et, si $P=0$, alors $P-P'=0$ donc $\deg( P-P')=\deg P=-\infty$.
		    On en déduit que $\forall\:P\in E$, $\deg f(P)=\deg P$.
		    Donc $f(E)\subset E$.(\*\*)
		    D’après (\*) et (\*\*), $f$ est bien un endomorphisme de $E$.

		    1.  Déterminons $\mathrm{Ker} f$.
		        Soit $P\in\mathrm{Ker} f$.
		        $f(P)=0$ donc $P-P'=0$ donc $\deg (P-P')=-\infty$.
		        Or, d’après ce qui précéde, $\deg(P-P')=\deg P$ donc $\deg P=-\infty$.
		        Donc $P=0$.
		        On en déduit que $\mathrm{Ker} f=\left\lbrace0\right\rbrace$.
		        Donc $f$ est injectif.
		        Or, $f\in\mathcal{L}\left( E\right)$ et $E$ est de dimension finie ($\dim E=n+1$) donc $f$ est bijectif.

		    2.  Soit $e=(1,X,...,X^n)$ la base canonique de $E$. Soit $A$ la matrice de $f$ dans la base $e$.
		        $A=\left( {
		        \begin{array}{cccc}
		         1 & { - 1} & {} & {(0)}  \\
		         {} & 1 &  \ddots  & {}  \\
		         {} & {} &  \ddots  & { - n}  \\
		         {(0)} & {} & {} & 1  \\
		        \end{array}
		        } \right)\in\mathcal{M}_{n+1}\left( \mathbb{R}\right) .$
		        $\det A=1$ d’où $\det A\neq 0$.
		        Donc $f$ est bijectif.

		2.  Soit $Q\in E$.
		    D’après 1.: $\exists\:!P\in E$, tel que $f(P) = Q$.
		    $P - P' = Q\text{, }P' - P'' = Q'\text{,\ldots , }P^{(n)}  - P^{(n + 1)}  = Q^{(n)}$.
		    Or $P^{(n + 1)}  = 0$, donc, en sommant ces $n+1$ égalités, $P = Q + Q' +  \cdots  + Q^{(n)}$.

		3.  Reprenons les notations de 1.(b).
		    Tout revient à se demander si $A$ est diagonalisable.
		    Notons $P_A(X)$ le polynôme caractéristique de $A$.
		    D’après 1.(b), on a $P_A(X)=(X-1)^{n+1}$.
		    Donc $1$ est l’unique valeur propre de $A$.
		    Ainsi, si $A$ était diagonalisable, alors $A$ serait semblable à la matrice unité $\mathrm{I}_{n+1}$.
		    On aurait donc $A=\mathrm{I}_{n+1}$.
		    Ce qui est manifestement faux car $f\neq \mathrm{Id}$.
		    Donc $A$ n’est pas diagonalisable et par conséquent, $f$ n’est pas diagonalisable.

!!! exercice "EXERCICE 60 algèbre  "
	=== "Enoncé"  


		Soit la matrice $A=
		\begin{pmatrix}
		1 & 2 \\
		2 & 4
		\end{pmatrix}$ et $f$ l’endomorphisme de $\mathcal{M}_{2}\left( \mathbb{R}%
		\right)$ défini par :   $f\left( M\right) =AM.$

		1.  Déterminer une base de $\mathrm{Ker}f$.

		2.  $f$ est-il surjectif ?

		3.  Déterminer une base de $\text{Im}f.$

		4.  A-t-on $\mathcal{M}_{2}\left( \mathbb{R}%
		    \right)=\mathrm{Ker}f\oplus \mathrm{Im}f$?


	=== "Corrigé"  


		1.  Posons $M = \left( {
		    \begin{array}{cc}
		     a & b  \\
		     c & d  \\
		    \end{array}
		    } \right) \in {\mathcal{M}}_2 (\mathbb{R})$.
		    On a $f(M) = \left( {
		    \begin{array}{cc}
		     {a + 2c} & {b + 2d}  \\
		     {2a + 4c} & {2b + 4d}  \\
		    \end{array}
		    } \right)$.
		    Alors $M\in\mathrm{Ker} f\Longleftrightarrow$ $\exists\:(a,b,c,d)\in\mathbb{R}^4$ tel que $M=\begin{pmatrix}
		    a&b\\c&d
		    \end{pmatrix}$ avec $\left\lbrace
		    \begin{array}{lll}
		    a&=&-2c\\
		    b&=&-2d
		    \end{array}
		    \right.$.
		    C’est-à-dire, $M\in\mathrm{Ker} f$ $\Longleftrightarrow$ $\exists\:(c,d)\in\mathbb{R}^2$ tel que $M=\begin{pmatrix}
		    -2c&-2d\\
		    c&d
		    \end{pmatrix}$.
		    On en déduit que $\mathrm{Ker} f = \textrm{Vect} \left\{ {\left( {
		    \begin{array}{cc}
		     -2 & 0  \\
		     { 1} & 0  \\
		    \end{array}
		    } \right),\left( {
		    \begin{array}{cc}
		     0 & -2  \\
		     0 & { 1}  \\
		    \end{array}
		    } \right)} \right\}$.(\*)
		    On pose $M_1=\left(
		    \begin{array}{cc}
		     -2 & 0  \\
		     {  1} & 0  \\
		    \end{array}
		     \right)$ et $M_2=\left(
		    \begin{array}{cc}
		     0 & -2  \\
		     0 & {  1}  \\
		    \end{array}
		     \right)$.
		    D’après (\*), la famille $(M_1,M_2)$ est génératrice de $\mathrm{Ker} f$.
		    De plus, $M_1$ et $M_2$ sont non colinéaires; donc $(M_1,M_2)$ est libre.
		    Donc $(M_1,M_2)$ est une base de $\mathrm{Ker} f$.

		2.  $\mathrm{Ker} f\neq \left\lbrace 0\right\rbrace$, donc $f$ est non injectif.
		    Or $f$ est un endomorphisme de $\mathcal{M}_2(\mathbb{R})$ et $\mathcal{M}_2(\mathbb{R})$ est de dimension finie.
		    On en déduit que $f$ est non surjectif.

		3.  Par la formule du rang, $\textrm{rg} f = 2$.
		    On pose $M_3=
		    f(E_{1,1} ) = \left( {
		    \begin{array}{cc}
		     1 & 0  \\
		     2 & 0  \\
		    \end{array}
		    } \right)$ et $M_4=f(E_{2,2} ) = \left( {
		    \begin{array}{cc}
		     0 & 2  \\
		     0 & 4  \\
		    \end{array}
		    } \right)$.
		    $M_3$ et $M_4$ sont non colinéaires, donc $(M_3,M_4)$ est une famille libre de $\mathrm{Im}f$.
		    Comme $\textrm{rg} f = 2$, $(M_3,M_4)$ est une base de $\mathrm{Im}f$.

		4.  On a $\dim \mathcal{M}_2\left(\mathbb{R} \right)=\dim \mathrm{Ker} f+\dim\mathrm{Im}f$.(1)
		    Prouvons que $\mathrm{Ker}f\cap \mathrm{Im}f=\left\lbrace0 \right\rbrace$.
		    Soit $M\in \mathrm{Ker}f\cap \mathrm{Im}f$.
		    D’après 1. et 3., $\exists(a,b,c,d)\in\mathbb{R}^4$ tel que $M=aM_1+bM_2$ et $M=cM_3+dM_4$.
		    On a donc $\left\lbrace \begin{array}{lll}
		    -2a&=&c\\
		    -2b&=&2d\\
		    a&=&2c\\
		    b&=&4d
		    \end{array}\right.$.
		    On en déduit que $a=b=c=d=0$.
		    Donc $M=0$.
		    Donc $\mathrm{Ker}f\cap \mathrm{Im}f=\left\lbrace0 \right\rbrace$(2)
		    Donc, d’après (1) et (2), $\mathcal{M}_{2}\left( \mathbb{R}%
		    \right)=\mathrm{Ker}f\oplus \mathrm{Im}f$.

!!! exercice "EXERCICE 61 algèbre  "
	=== "Enoncé"  


		On note $\mathcal{M}_{n}\left( \mathbb{C}\right)$ l’espace vectoriel des matrices carrées d’ordre $n$ à coefficients complexes.

		Pour $A=\left( a_{i,j}\right) _{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}\in \mathcal{M}_{n}\left( \mathbb{C}\right)$, on pose : $\left\Vert A\right\Vert =\underset{_{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}}{\sup }\left\vert a_{i,j}\right\vert$.

		1.  Prouver que $||\:||$ est une norme sur $\mathcal{M}_{n}\left( \mathbb{C}\right)$.

		2.  Démontrer que: $\forall\:(A,B)\in \left( \mathcal{M}_{n}\left( \mathbb{C}\right)\right) ^2$, $\left\Vert AB\right\Vert \leqslant n\left\Vert A\right\Vert \left\Vert B\right\Vert$.
		    Puis, démontrer que, pour tout entier $p\geqslant 1$, $\left\Vert A^{p}\right\Vert \leqslant n^{p-1}\left\Vert A\right\Vert ^{p}$.

		3.  Démontrer que, pour toute matrice $A\in \mathcal{M}_{n}\left( \mathbb{C}\right)$, la série $\displaystyle\displaystyle\sum \dfrac{A^{p}}{p!}$ est absolument convergente.

		    Est-elle convergente?


	=== "Corrigé"  


		1.  On remarque que $\forall\:A\in \mathcal{M}_{n}\left( \mathbb{C}\right)$, $\left\Vert A\right\Vert\geqslant 0$.
		    i- Soit $A=\left( a_{i,j}\right) _{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}\in \mathcal{M}_{n}\left( \mathbb{C}\right)$ telle que $||A||= 0$.
		    Comme $\forall \:(i,j)\in\left( \llbracket1,n\rrbracket\right) ^2$, $|a_{i,j}|\geqslant 0$, on en déduit que $\forall \:(i,j)\in\left( \llbracket1,n\rrbracket\right) ^2$, $|a_{i,j}|=0$, c’est-à-dire $a_{i,j}=0$.
		    Donc $A=0$.
		    ii- Soit $A=\left( a_{i,j}\right) _{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}\in \mathcal{M}_{n}\left( \mathbb{C}\right)$ et soit $\lambda\in\mathbb{C}$.
		    $\left\Vert\lambda A\right\Vert=\underset{_{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}}{\sup }\left\vert\lambda a_{i,j}\right\vert
		    =\underset{_{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}}{\sup }|\lambda|\left\vert a_{i,j}\right\vert
		    =|\lambda|\underset{_{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}}{\sup }\left\vert a_{i,j}\right\vert
		    =|\lambda|\left\Vert A\right\Vert$.
		    iii- Soit $(A,B)\in \left( \mathcal{M}_{n}\left( \mathbb{C}\right)\right) ^2$ avec $A=\left( a_{i,j}\right) _{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}$ et $B=\left( b_{i,j}\right) _{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}$.
		    On a $\left\Vert A+B\right\Vert=\underset{_{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}}{\sup }\left\vert a_{i,j}+b_{i,j}\right\vert$.
		    Or, $\forall \:(i,j)\in\left( \llbracket1,n\rrbracket\right) ^2$, $|a_{i,j}+b_{i,j}|\leqslant |a_{i,j}|+|b_{i,j}|\leqslant \left\Vert A\right\Vert+\left\Vert B\right\Vert$.
		    On en déduit que $\left\Vert A+B\right\Vert\leqslant\left\Vert A\right\Vert+\left\Vert B\right\Vert$.

		2.  Soit $(A,B)\in \left( \mathcal{M}_{n}\left( \mathbb{C}\right)\right) ^2$ avec $A=\left( a_{i,j}\right) _{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}$, $B=\left( b_{i,j}\right) _{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}$.
		    Posons $C=AB$.
		    On a $C=\left( c_{i,j}\right) _{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}$ avec $\forall \:(i,j)\in\left( \llbracket1,n\rrbracket\right) ^2$, $c_{i,j}  = \displaystyle\sum\limits_{k = 1}^n {a_{i,k} b_{k,j} }$.
		    Donc, $\forall \:(i,j)\in\left( \llbracket1,n\rrbracket\right) ^2$, $\left| c_{i,j} \right| \leqslant\displaystyle\sum\limits_{k=1}^{n}|a_{i,k}|.|b_{k,j}|\leqslant \displaystyle\sum\limits_{k = 1}^n {\left\| A \right\|\left\| B \right\|}  = n\left\| A \right\|\left\| B \right\|$.
		    On en déduit que $\forall\:(A,B)\in \left( \mathcal{M}_{n}\left( \mathbb{C}\right)\right) ^2$, $\left\| {AB} \right\| \leqslant n\left\| A \right\|\left\| B \right\|$.(\*)
		    Pour tout entier naturel $p\geqslant 1$, notons $(P_p)$ la propriété : $\left\| {A^p } \right\| \leqslant n^{p - 1} \left\| A \right\|^p$.
		    Prouvons que $(P_p)$ est vraie par récurrence.
		    Pour $p=1$, $\left\Vert A^{1}\right\Vert=n^{0}\left\Vert A\right\Vert^1$, donc $(P_1)$ est vraie.
		    Supposons la propriété $(P_p)$ vraie pour un rang $p\geqslant 1$, c’est-à-dire $\left\Vert A^p\right\Vert\leqslant n^{p-1}\left\Vert A\right\Vert^{p}$.
		    Prouvons que $(P_{p+1})$ est vraie.
		    $\left\Vert A^{p+1}\right\Vert=\left\Vert A\times A^p\right\Vert$ donc, d’après (\*), $\left\Vert A^{p+1}\right\Vert\leqslant n\left\Vert A \right\Vert\left\Vert A^p \right\Vert$.
		    Alors, en utilisant l’hypothèse de récurrence, $\left\Vert A^{p+1}\right\Vert\leqslant n\left\Vert A\right\Vert n^{p-1}\left\Vert A\right\Vert^p=n^p\left\Vert A\right\Vert^{p+1}$.
		    On en déduit que $(P_{p+1})$ est vraie.

		3.  On a $\forall\:p\in\mathbb{N}^*$, $\left\| {\dfrac{{A^p }}{{p!}}} \right\| \leqslant \dfrac{1}{n}\dfrac{{(n\left\| A \right\|)^p }}{{p!}}$.
		    Or, $\forall\:x\in\mathbb{R}$, la série exponentielle $\displaystyle\sum {\dfrac{{x^p }}{{p!}}}$ converge, donc $\displaystyle\sum \dfrac{(n||A||)^p}{p!}$ converge.
		    Donc, par comparaison de séries à termes positifs, la série $\displaystyle\sum {\dfrac{{A^p }}{{p!}}}$ est absolument convergente.
		    Or $\mathcal{M}_n (\mathbb{C})$ est de dimension finie, donc $\displaystyle\sum {\dfrac{{A^p }}{{p!}}}$ converge.

!!! exercice "EXERCICE 62 algèbre  "
	=== "Enoncé"  


		Soit $E\ $un espace vectoriel sur $\mathbb{R}$ ou $\mathbb{C}$.
		Soit $f\in\mathcal{L}(E)$ tel que $f^2-f-2\mathrm{Id}=0$.

		1.  Prouver que $f$ est bijectif et exprimer $f^{-1}$ en fonction de $f$.

		2.  Prouver que $E=\mathrm{Ker} (f+\mathrm{Id})\oplus\mathrm{Ker}(f-2\mathrm{Id})$:

		    1.  en utilisant le lemme des noyaux.

		    2.  sans utiliser le lemme des noyaux.

		3.  Dans cette question, on suppose que $E$ est de dimension finie.
		    Prouver que $\mathrm{Im}(f+\mathrm{Id})=\mathrm{Ker}(f-2\mathrm{Id})$.


	=== "Corrigé"  


		1.  $f$ est linéaire donc:
		    $f^2-f-2\mathrm{Id}=0\Longleftrightarrow f\circ(f-\mathrm{Id})=(f-\mathrm{Id})\circ f=2\mathrm{Id}\Longleftrightarrow f\circ(\frac{1}{2}f-\frac{1}{2}\mathrm{Id})=(\frac{1}{2}f-\frac{1}{2}\mathrm{Id})\circ f=\mathrm{Id}$.
		    On en déduit que $f$ est inversible, donc bijectif, et que $f^{-1}=\frac{1}{2}f-\frac{1}{2}\mathrm{Id}$.

		2.  1.  On pose $P=X^2-X-2$. On a $P=(X+1)(X-2)$.
		        $P_1=X+1$ et $P_2=X-2$ sont premiers entre eux.
		        Donc, d’après le lemme des noyaux, $\mathrm{Ker}P(f)=\mathrm{Ker}P_1(f)\oplus\mathrm{Ker}P_2(f)$.
		        Or $P$ est annulateur de $f$, donc $\mathrm{Ker}P(f)=E$.
		        Donc $E=\mathrm{Ker}(f+\mathrm{Id})\oplus \mathrm{Ker}(f-2\mathrm{Id})$.

		    2.  **Analyse (unicité):**
		        Soit $x\in E$. Supposons que $x=a+b$ avec $a\in \mathrm{Ker}(f+\mathrm{Id})$ et $b\in \mathrm{Ker}(f-2\mathrm{Id})$.
		        Alors par linéarité de $f$, $f(x)=f(a)+f(b)=-a+2b$.
		        On en déduit que $a=\dfrac{2x-f(x)}{3}$ et $b=\dfrac{x+f(x)}{3}$.
		        **Synthèse (existence):**
		        Soit $x\in E$. On pose $a=\dfrac{2x-f(x)}{3}$ et $b=\dfrac{x+f(x)}{3}$.
		        On a bien $x=a+b$.(\*)
		        $(f+\mathrm{Id})(a)=\dfrac{1}{3}\left( 2f(x)-f^2(x)+2x-f(x)\right) =\dfrac{1}{3}\left( -f^2(x)+f(x)+2x\right)=0$ car $f^2-f-2\mathrm{Id}=0$.
		        Donc $a\in\mathrm{Ker}(f+\mathrm{Id})$. (\*\*)
		        $(f-2\mathrm{Id})(b)=\dfrac{1}{3}\left( f(x)+f^2(x)-2x-2f(x)\right) =\dfrac{1}{3}\left( f^2(x)-f(x)-2x\right)=0$ car $f^2-f-2\mathrm{Id}=0$.
		        Donc $b\in\mathrm{Ker}(f-2\mathrm{Id})$. (\*\*\*)
		        D’après (\*), (\*\*) et (\*\*\*), $x=a+b$ avec $a\in \mathrm{Ker}(f+\mathrm{Id})$ et $b\in \mathrm{Ker}(f-2\mathrm{Id})$.
		        Conclusion: $E=\mathrm{Ker} (f+\mathrm{Id})\oplus\mathrm{Ker}(f-2\mathrm{Id})$.

		3.  Prouvons que $\mathrm{Im}(f+\mathrm{Id})\subset \mathrm{Ker}(f-2\mathrm{Id})$.
		    Soit $y\in \mathrm{Im}(f+\mathrm{Id})$.
		    $\exists\:x\in E\:/\:y=f(x)+x$.
		    Alors $(f-2\mathrm{Id})(y)=f(y)-2y=f^2(x)+f(x)-2f(x)-2x=f^2(x)-f(x)-2x=0$ car $f^2-f-2\mathrm{Id}=0$.
		    Donc $y\in \mathrm{Ker}(f-2\mathrm{Id})$.
		    Donc $\mathrm{Im}(f+\mathrm{Id})\subset \mathrm{Ker}(f-2\mathrm{Id})$.(\*)
		    Posons $\dim E=n$.
		    D’après 2., $n=\dim  \mathrm{Ker}(f+\mathrm{Id})+\dim  \mathrm{Ker}(f-2\mathrm{Id}) .$
		    De plus, d’après le théorème du rang, $n=\dim   \mathrm{Ker}(f+\mathrm{Id}) +\dim   \mathrm{Im}(f+\mathrm{Id})$.
		    On en déduit que $\dim  \mathrm{Im}(f+\mathrm{Id})=\dim  \mathrm{Ker}(f-2\mathrm{Id})$.(\*\*)
		    Donc, d’après (\*) et (\*\*), $\mathrm{Im}(f+\mathrm{Id})= \mathrm{Ker}(f-2\mathrm{Id}).$

!!! exercice "EXERCICE 63 algèbre  "
	=== "Enoncé"  
		Soit un entier $n\geqslant 1.$ On considère la matrice carrée d’ordre $n$ à coefficients réels :

		$$
		A_n=\begin{pmatrix}
		2 & -1 & 0 & \cdots & 0 \\
		-1 & 2 & -1 & \ddots & \vdots \\
		0 & -1 & \ddots & \ddots & 0 \\
		\vdots & \ddots & \ddots & 2 & -1 \\
		0 & \cdots & 0 & -1 & 2
		\end{pmatrix}
		$$

		Pour $n\geqslant 1$, on désigne par $D_{n}$ le déterminant de $A_n$.

		1.  Démontrer que $D_{n+2}=2D_{n+1}-D_{n}$.

		2.  Déterminer $D_{n}$ en fonction de $n$.

		3.  Justifier que la matrice $A_n$ est diagonalisable. Le réel $0$ est-il valeur propre de $A_n$?


	=== "Corrigé"  


		1)  C’est un déterminant tri-diagonal, il suffit de développer selon la première ligne.

		$$
		D_{n+2}  = 2D_{n +1}  +
		\begin{vmatrix}
		{ - 1} & { - 1} & {} & {} & {(0)}\\
		0 & 2 & { - 1} & {} & {}\\
		{} & { - 1} & 2 &  \ddots  & {}\\
		{} & {} &  \ddots  &  \ddots  & {-1}\\
		{} & {(0)} & {} & { - 1} & 2\\
		\end{vmatrix}
		$$

		Puis, en développant le second déterminant obtenu selon la première colonne, on obtient $D_{n+2}=2D_{n+1}-D_n$.

		2) $(D_n )_{n\geqslant 1}$ est une suite récurrente linéaire d’ordre 2 d’équation caractéristique $r^2  - 2r + 1 = 0$.
		Donc, son terme général est de la forme $D_n  = (\lambda n + \mu ) \times 1^n$.
		Puisque $D_1  = 2$ et $D_2  = 3$, on obtient $D_n  = n + 1$.

		3)  La matrice $A_n$ est symétrique réelle donc diagonalisable.
		$D_n=n+1\neq 0$ donc $A_n$ est inversible.
		Donc l’endomorphisme canoniquement associé à $A_n$ est injectif.
		On en déduit que $0$ n’est pas valeur propre de $A_n$.

!!! exercice "EXERCICE 64 algèbre  "
	=== "Enoncé"  


		Soit $f$ un endomorphisme d’un espace vectoriel $E$ de dimension finie $n$.

		1.  Démontrer que: $E=\text{Im} f \oplus \mathrm{Ker} f \Longrightarrow \text{Im} f = \text{Im} f^2$.

		2.  1.  Démontrer que: $\text{Im} f = \text{Im} f^2 \Longleftrightarrow \mathrm{Ker} f = \mathrm{Ker} f^2$.

		    2.  Démontrer que: $\text{Im} f = \text{Im} f^2 \Longrightarrow E=\text{Im} f \oplus \mathrm{Ker} f$.


	=== "Corrigé"  


		1.  Supposons $E = \textrm{Im} f \oplus \mathrm{Ker} f$.
		    Indépendamment de l’hypothèse, on peut affirmer que $\textrm{Im} f^2  \subset \textrm{Im} f$ (\*)
		    Montrons que $\mathrm{Im}f\subset\mathrm{Im}f^2$.
		    Soit $y \in \textrm{Im} f$.
		    Alors, $\exists\:x\in E$ tel que $y = f(x)$.
		    Or $E = \textrm{Im} f \oplus \mathrm{Ker} f$, donc $\exists\:(a,b)\in E\times\mathrm{Ker} f$ tel que $x = f(a) + b$.
		    On a alors $y = f^2 (a) \in \textrm{Im} f^2$.
		    Ainsi $\textrm{Im} f \subset \textrm{Im} f^2$(\*\*)
		    D’après (\*) et (\*\*), $\textrm{Im} f = \textrm{Im} f^2$.

		2.  1.  On a $\textrm{Im} f^2  \subset \textrm{Im} f$ et $\mathrm{Ker} f \subset \mathrm{Ker} f^2$.
		        On en déduit que $\textrm{Im} f^2 = \textrm{Im} f$ $\Longleftrightarrow$ $\mathrm{rg}f^2=\mathrm{rg}f$ et $\mathrm{Ker} f = \mathrm{Ker} f^2$ $\Longleftrightarrow$ $\dim \mathrm{Ker} f=\dim \mathrm{Ker} f^2$.
		        Alors, en utilisant le théorème du rang, $\textrm{Im} f = \textrm{Im} f^2  \Leftrightarrow \textrm{rg} f = \textrm{rg} f^2  \Leftrightarrow \dim \mathrm{Ker} f = \dim \mathrm{Ker} f^2  \Leftrightarrow \mathrm{Ker} f = \mathrm{Ker} f^2$.

		    2.  Supposons $\textrm{Im} f = \textrm{Im} f^2$.
		        Soit $x \in \textrm{Im} f \cap \mathrm{Ker} f$.
		        $\exists\:a\in E$ tel que $x = f(a)$ et $f(x) = 0_E$.
		        On en déduit que $f^2 (a) = 0_E$ c’est-à-dire $a \in \mathrm{Ker} f^2$.
		        Or, d’après l’hypothèse et 2.(a), $\mathrm{Ker} f^2  = \mathrm{Ker} f$ donc $a\in \mathrm{Ker} f$ c’est-à-dire $f(a) = 0_E$.
		        C’est-à-dire $x=0$.
		        Ainsi $\textrm{Im} f \cap \mathrm{Ker} f = \left\{ {0_E } \right\}$.(\*\*\*)
		        De plus, d’après le théorème du rang, $\dim \mathrm{Im}f + \dim \mathrm{Ker} f = \dim E$. (\*\*\*\*)

		        Donc, d’après (\*\*\*) et (\*\*\*\*), $E = \textrm{Im} f \oplus \mathrm{Ker} f$.

!!! exercice "EXERCICE 65 algèbre  "
	=== "Enoncé"  


		Soit $u$ un endomorphisme d’un espace vectoriel $E$ sur le corps $\mathbb{K}$ ($=\mathbb{R}$ ou $\mathbb{C}$). On note $\mathbb{K}[X]$ l’ensemble des polynômes à coefficients dans $\mathbb{K}$.

		1.  Démontrer que : $\forall (P,Q)\in \mathbb{K}[X]\times \mathbb{K}[X],~ (PQ)(u)=P(u)\circ Q(u)$ .

		2.  1.  Démontrer que : $\forall (P,Q)\in \mathbb{K}[X]\times \mathbb{K}[X],~ P(u)\circ Q(u)=Q(u)\circ P(u)$ .

		    2.  Démontrer que, pour tout $(P,Q)\in \mathbb{K}[X]\times \mathbb{K}[X]$: $\text{($P$ polynôme annulateur de $u$)$\Longrightarrow$ ($PQ$ polynôme annulateur de $u$)}$

		3.  Soit $A=\begin{pmatrix}
		    -1 & -2 \\
		    1 & 2
		    \end{pmatrix}$.
		    Écrire le polynôme caractéristique de $A$, puis en déduire que le polynôme $R=X^4+2X^3+X^2-4X$ est un polynôme annulateur de $A$.


	=== "Corrigé"  


		1.  Soit $(P,Q)\in\left(\mathbb{K}[X]\right) ^2$.
		    $P=\displaystyle\sum\limits_{p=0}^{n}a_pX^p$ et $Q=\displaystyle\sum\limits_{q=0}^{m}b_qX^q$.
		    Donc $PQ=\displaystyle\sum\limits_{p=0}^{n}\displaystyle\sum\limits_{q=0}^{m}\left( a_pb_qX^{p+q}\right)$.
		    Donc $(PQ)(u)=\displaystyle\sum\limits_{p=0}^{n}\displaystyle\sum\limits_{q=0}^{m}\left( a_pb_qu^{p+q}\right)$(\*)
		    Or $P(u)\circ Q(u)=\left( \displaystyle\sum\limits_{p=0}^{n}a_pu^p\right) \circ\left( \displaystyle\sum\limits_{q=0}^{m}b_qu^q\right)= \displaystyle\sum\limits_{p=0}^{n}\left(a_pu^p \circ\displaystyle \displaystyle\sum\limits_{q=0}^{m}b_qu^q\right)$.
		    Donc, par linéarité de $u$, $P(u)\circ Q(u)=\displaystyle\displaystyle\sum\limits_{p=0}^{n}\left( \displaystyle\displaystyle\sum\limits_{q=0}^{m}a_pu^p \circ b_qu^q\right)=\displaystyle\displaystyle\sum\limits_{p=0}^{n}\displaystyle\displaystyle\sum\limits_{q=0}^{m}\left( a_pb_qu^{p+q}\right)$.(\*\*)
		    D’après (\*) et (\*\*), $(PQ)(u)=P(u)\circ Q(u)$.

		2.  1.  Soit $(P,Q)\in\left(\mathbb{K}[X]\right) ^2$.
		        D’après 1., $P(u)\circ Q(u)=(PQ)(u)$.
		        De même, d’après 1., $Q(u)\circ P(u)=(QP)(u)$.
		        Or $PQ=QP$ donc $(PQ)(u)=(QP)(u)$.
		        On en déduit que $P(u)\circ Q(u)=Q(u)\circ P(u)$.

		    2.  Soit $(P,Q)\in\left(\mathbb{K}[X]\right) ^2$.
		        On suppose que $P$ est annulateur de $u$.
		        Prouvons que $PQ$ est annulateur de $u$.
		        D’après 1. et 2.(a), $(PQ)(u)=P(u)\circ Q(u)=Q(u)\circ P(u)$.(\*\*\*)
		        Or $P$ est annulateur de $u$ donc $P(u)=0$ donc, d’après (\*\*\*), $(PQ)(u)=0$.
		        On en déduit que $PQ$ est annulateur de $u$.

		3.  Notons $P_A(X)$ le polynôme caractéristique de $A$.
		    $P_A(X)=\det (XI_2-A)$. On trouve $P_A(X)=X(X-1)$.
		    Soit $R=X^4+2X^3+X^2-4X$.
		    On remarque que $R(0)=R(1)=0$ et on en déduit que $R$ est factorisable par $X(X-1)$.
		    C’est-à-dire: $\exists\:Q\in\mathbb{K}\left[ X\right]$ / $R=X(X-1)Q$.
		    Or, d’après le théorème de Cayley-Hamilton, $P_A(X)=X(X-1)$ annule $A$.
		    Donc, d’après 2.b., comme $R=P_A(X)Q$, $R$ est annulateur de $A$.

!!! exercice "EXERCICE 66 algèbre "
	=== "Enoncé"  


		On note $p$ un entier naturel supérieur ou égal à 2.

		On considère dans $\mathbb Z$ la relation d’équivalence $\cal R$ définie par:   $x\,{\cal R}\,y\stackrel{\hbox{\scriptsize déf.}}{\Longleftrightarrow}\exists k\in\mathbb Z$ tel que $x-y=kp$.

		On note $\mathbb Z / p\mathbb Z$ l’ensemble des classes d’équivalence pour cette relation ${\cal R}$.

		1.  Quelle est la classe d’équivalence de 0? Quelle est celle de $p$?

		2.  Donner soigneusement la définition de l’addition usuelle et de la multiplication usuelle dans $\mathbb Z / p\mathbb Z$.
		    On justifiera que ces définitions sont cohérentes.

		3.  On admet que, muni de ces opérations, $\mathbb Z / p\mathbb Z$ est un anneau.

		    Démontrer que $\mathbb Z / p\mathbb Z$ est un corps si et seulement si $p$ est premier.


	=== "Corrigé"  


		1.  Les classes d’équivalences de 0 et de $p$ sont toutes deux égales à l’ensemble des multiples de $p$, c’est-à-dire à $p\mathbb{Z}$.

		2.  Soit $(\overline{a},\overline{b} )\in \left( \mathbb{Z} / {p\mathbb{Z}}\right) ^2$.
		    On pose $\overline{a} + \overline{b}=\overline {a + b}$ et $\overline{a} \times \overline{b}=\overline{ab}$.
		    Cette définition est cohérente car elle ne dépend pas des représentants $a$ et $b$ choisis pour $\overline{a}$ et $\overline{b}$.
		    En effet, soit $(a',b')\in \mathbb{Z}^2$ tel que $\overline{a'}=\overline{a}$ et $\overline{b'}=\overline{b}$.
		    Alors il existe $n\in\mathbb{Z}$ tel que $a'=a+np$ et il existe $m\in\mathbb{Z}$ tel que $b'=b+mp$.
		    Donc $a'+b'=a+b+(n+m)p$, c’est-à-dire $\overline{a'+b'}=\overline{a+b}$.
		    Et $a'b'=ab+(am+bn+nmp)p$, c’est-à-dire $\overline{a'b'}=\overline{ab}$.

		3.  Supposons $p$ premier.
		    Alors $\mathbb{Z} / {p\mathbb{Z}}$ est commutatif et non réduit à $\left\{ {\bar{0}} \right\}$ car $p \geqslant 2$.
		    Soit $\bar{a} \in \mathbb{Z} / {p\mathbb{Z}}$ tel que $\bar{a} \ne \bar{0}$.
		    $\bar{a} \ne \bar{0}$ donc $p$ ne divise pas $a$ . Or $p$ est premier donc $p$ est premier avec $a$.
		    Par le théorème de Bézout, il existe $(u,v)\in\mathbb{Z}^2$ tel que $au + pv = 1$ donc $\bar{a} \times \bar{u} = \bar{1}$.
		    Donc $\overline{a}$ est inversible et $(\bar{a})^{-1}=\bar{u}$.
		    Ainsi, les éléments non nuls de $\mathbb{Z} / {p\mathbb{Z}}$ sont inversibles et finalement $\mathbb{Z} / {p\mathbb{Z}}$ est un corps.
		    Supposons que $\mathbb{Z} / {p\mathbb{Z}}$ est un corps.
		    Soit $k\in\llbracket2,p-1\rrbracket$.
		    $\overline{k}\neq \overline{0}$ donc, comme $\mathbb{Z} / {p\mathbb{Z}}$ est un corps, il existe $k'\in \mathbb{Z}$ tel que $\overline{k}\:\overline{k'}=\overline{1}$.
		    C’est-à-dire il existe $v\in\mathbb{Z}$ tel que $kk'=1+vp$ c’est-à-dire $k'k-vp=1$.
		    Donc, d’après le théorème de Bézout, $k\wedge p=1$ et donc, comme $k\neq 1$, $k$ ne divise pas $p$.
		    On en déduit que les seuls diviseurs positifs de $p$ sont 1 et $p$.
		    Donc $p$ est premier.

!!! exercice "EXERCICE 67 algèbre "
	=== "Enoncé"  


		Soit la matrice $M=\begin{pmatrix}
		0 & a & c \\
		b & 0 & c \\
		b & -a & 0
		\end{pmatrix}$ où $a,b,c$ sont des réels.

		$M$ est-elle diagonalisable dans $\mathcal{M}_{3}\left(\mathbb{R}\right)$? $M$ est-elle diagonalisable dans $\mathcal{M}_{3}\left( \mathbb{C}\right)$?


	=== "Corrigé"  


		$\chi _M (X)=\det (XI_3-M)$.
		Après calculs, on trouve, $\chi _M (X) =   X(X^2  + ca - ba - bc)$.
		**Premier cas**: $ca-ba-bc <0$
		$M$ est diagonalisable dans ${\mathcal{M}}_3 (\mathbb{R})$ car $M$ possède trois valeurs propres réelles distinctes.
		Elle est, a fortiori, diagonalisable dans ${\mathcal{M}}_3 (\mathbb{C})$.
		**Deuxième cas**: $ca-ba-bc=0$
		Alors, 0 est la seule valeur propre de $M$.
		Ainsi, si $M$ est diagonalisable, alors $M$ est semblable à la matrice nulle c’est-à-dire $M=0$ ou encore $a=b=c=0$. Réciproquement, si $a=b=c=0$ alors $M=0$ et donc $M$ est diagonalisable.
		On en déduit que $M$ est diagonalisable si et seulement si $a=b=c=0$.
		**Troisième cas**: $ca-ba-bc>0$
		Alors 0 est la seule valeur propre réelle et donc $M$ n’est pas diagonalisable dans ${\mathcal{M}}_3 (\mathbb{R})$ car $\chi _M (X)$ n’est pas scindé sur $\mathbb{R}[X]$.
		En revanche, $M$ est diagonalisable dans ${\mathcal{M}}_3 (\mathbb{C})$ car elle admet trois valeurs propres complexes distinctes.

!!! exercice "EXERCICE 68 algèbre  "
	=== "Enoncé"  


		Soit la matrice $A=\begin{pmatrix}
		1 & -1 & 1 \\
		-1 & 1 & -1 \\
		1 & -1 & 1
		\end{pmatrix}$ .

		1.  Démontrer que $A$ est diagonalisable de quatre manières:

		    1.  sans calcul,

		    2.  en calculant directement le déterminant $\text{det}(\lambda \mathrm{I}_3-A)$, où $\mathrm{I}_3$ est la matrice identité d’ordre 3, et en déterminant les sous-espaces propres,

		    3.  en utilisant le rang de la matrice,

		    4.  en calculant $A^2$.

		2.  On suppose que $A$ est la matrice d’un endomorphisme $u$ d’un espace euclidien dans une base orthonormée.
		    Trouver une base orthonormée dans laquelle la matrice de $u$ est diagonale.


	=== "Corrigé"  


		1.  1.  La matrice $A$ est symétrique réelle donc diagonalisable dans une base orthonormée de vecteurs propres.

		    2.  On obtient $\det ( \lambda \mathrm{I}_3 -A) =   \lambda ^2 (\lambda  - 3)$.
		        $E_3(A)= \mathrm{Vect} \left(\begin{pmatrix}
		        1\\-1\\1
		         \end{pmatrix}\right)$ et $E_0 (A):x - y + z = 0$.
		        Donc $A$ est diagonalisable car $\dim E_3 (A) + \dim E_0 (A) = 3$.

		    3.  $\textrm{rg} A = 1$ donc $\dim E_0 (A) = 2$.
		        On en déduit que 0 est valeur propre au moins double de la matrice $A$.
		        Puisque $\textrm{tr} A = 3$ et que $\textrm{tr} A$ est la somme des valeurs propres complexes de $A$ comptées avec leur multiplicité, la matrice $A$ admet une troisième valeur propre qui vaut 3 et qui est nécessairement simple.
		        Comme dans la question précédente, on peut conclure que $A$ est diagonalisable car $\dim E_3 (A) + \dim E_0 (A) = 3$.

		    4.  On obtient $A^2  = 3A$ donc $A$ est diagonalisable car cette matrice annule le polynôme $X^2  - 3X$ qui est scindé à racines simples.

		2.  On note $e=\left( \vec{u},\vec{v},\vec{w}\right)$ la base canonique de $\mathbb{R}^3$.
		    On note $(\:|\:)$ le produit scalaire canonique sur $\mathbb{R}^3$.
		    Soit $f$ l’endomorphisme canoniquement associé à $A$.
		    $A$ est symétrique réelle et $e$ est une base orthonormée, donc $f$ est un endomorphisme symétrique et, d’après le théorème spectral, $f$ est diagonalisable dans une base orthonormée de vecteurs propres.
		    On sait également que les sous-espaces propres sont orthogonaux donc il suffit de trouver une base orthonormée de chaque sous-espace propre pour construire une base orthonormée de vecteurs propres.
		    $E_3 (f) = \textrm{Vect} (1, - 1,1)\text{ et }E_0 (f):x - y + z = 0$.
		    Donc $\vec{u} = \dfrac{1}{{\sqrt 3 }}(\vec{i} - \vec{j} + \vec{k})$ est une base orthonormée de $E_3 (f)$.
		    $\vec i+\vec j$ et $\vec i-\vec j-2\vec k$ sont deux vecteurs orthogonaux de $E_0(f)$.
		    On les normalise et on pose $\vec{v} = \dfrac{1}{{\sqrt 2 }}(\vec{i} + \vec{j})$ et $\vec{w} = \dfrac{1}{{\sqrt 6 }}\left( {\vec{i} - \vec{j} - 2\vec{k}} \right)$.
		    Alors $\left( \vec{v} ,\vec{w} \right)$ une base orthonormée de $E_0(f)$.
		    On en déduit que $\left( \vec{u},\vec{v},\vec{w}\right)$ est une base orthonormée de vecteurs propres de $f$.

!!! exercice "EXERCICE 69 algèbre  "
	=== "Enoncé"  


		On considère la matrice $A=\begin{pmatrix}
		0&a&1\\
		a&0&1\\
		a&1&0
		\end{pmatrix}$ où $a$ est un réel.

		1.  Déterminer le rang de $A$.

		2.  Pour quelles valeurs de $a$, la matrice $A$ est-elle diagonalisable?


	=== "Corrigé"  


		1.  Après calcul, on trouve $\det A=a(a+1)$.
		    **Premier cas**: $a\neq 0$ et $a\neq -1$
		    Alors, $\det A\neq 0$ donc $A$ est inversible.
		    Donc $\mathrm{rg}A=3$.
		    **Deuxième cas** : $a=0$
		    $A=\begin{pmatrix}
		    0&0&1\\
		    0&0&1\\
		    0&1&0
		    \end{pmatrix}$ donc $\mathrm{rg}A=2$.
		    **Troisième cas**: $a=-1$
		    $A=\begin{pmatrix}
		    0&-1&1\\
		    -1&0&1\\
		    -1&1&0
		    \end{pmatrix}$ donc $\mathrm{rg}A\geqslant 2$ car les deux premières colonnes de $A$ sont non colinéaires.
		    Or $\det A=0$ donc $\mathrm{rg}A\leqslant 2$.
		    On en déduit que $\mathrm{rg}A=2$.

		2.  Notons $\chi_A$ le polynôme caractéristique de $A$.
		    $\det(\lambda I_n-A)=\begin{vmatrix}
		    \lambda&-a&-1\\
		    -a&\lambda&-1\\
		    -a&-1&\lambda
		    \end{vmatrix}$
		    Alors, en ajoutant à la première colonne la somme des deux autres puis, en soustrayant la première ligne aux deux autres lignes, on trouve successivement:
		    $\det(\lambda I_n-A)=
		    (\lambda-a-1)\begin{vmatrix}
		    1&-a&-1\\
		    1&\lambda&-1\\
		    1&-1&\lambda
		    \end{vmatrix}$ $=(\lambda-a-1)\begin{vmatrix}
		    1&-a&-1\\
		    0&\lambda+a&0\\
		    0&-1+a&\lambda+1
		    \end{vmatrix}$.
		    Donc, en développant par rapport à la première colonne,
		    $\det(\lambda I_n-A)=(\lambda-a-1)(\lambda+a)(\lambda+1)$.
		    Donc $\chi_A=(X-a-1)(X+a)(X+1)$.
		    Les racines de $\chi_A$ sont $a+1$, $-a$ et $-1$.
		    $a+1=-a\Longleftrightarrow a=-\dfrac{1}{2}$.
		    $a+1=-1\Longleftrightarrow a=-2$.
		    $-a=-1\Longleftrightarrow a=1$.
		    Ce qui amène aux quatre cas suivants:
		    **Premier cas**: $a\neq 1$, $a\neq-2$ et $a\neq-\dfrac{1}{2}$
		    Alors $A$ admet trois valeurs propres disctinctes.
		    Donc $A$ est diagonalisable.
		    **Deuxième cas**: $a=1$
		    $\chi_A=(X-2)(X+1)^2$.
		    Alors $A$ est diagonalisable si et seulement si $\dim E_{-1}=2$, c’est-à-dire $\mathrm{rg}(A+\mathrm{I}_3)=1$.
		    Or $A+\mathrm{I}_3=\begin{pmatrix}
		    1&1&1\\
		    1&1&1\\
		    1&1&1
		    \end{pmatrix}$ donc $\mathrm{rg}(A+\mathrm{I}_3)=1$.
		    Donc $A$ est diagonalisable.

		    **Troisième cas**: $a=-2$
		    Alors, $\chi_A=(X+1)^2(X-2)$.
		    $A+\mathrm{I}_3=\begin{pmatrix}
		    1&-2&1\\
		    -2&1&1\\
		    -2&1&1
		    \end{pmatrix}$
		    Les deux premières colonnes de $A+\mathrm{I}_3$ ne sont pas colinéaires, donc $\mathrm{rg(A+\mathrm{I}_3)}\geqslant2$.
		    De plus, $-1$ est valeur propre de $A$, donc $\mathrm{rg(A+\mathrm{I}_3)}\leqslant2$.
		    Ainsi, $\mathrm{rg}(A+\mathrm{I}_3)=2$ et $\dim E_{-1}=1$.
		    Or l’ordre multiplicité de la valeur propre $-1$ dans le polynôme caractéristique est 2.
		    On en déduit que $A$ n’est pas diagonalisable.
		    **Quatrième cas**: $a=-\dfrac{1}{2}$
		    $\chi_A=(X-\dfrac{1}{2})^2(X+1)$.

		    1,8 $A-\dfrac{1}{2}\mathrm{I}_3=\begin{pmatrix}
		    -\dfrac{1}{2}&-\dfrac{1}{2}&1\\
		    -\dfrac{1}{2}&-\dfrac{1}{2}&1\\
		    -\dfrac{1}{2}&1&-\dfrac{1}{2}
		    \end{pmatrix}$.
		    Les deux premières colonnes de $A-\dfrac{1}{2}\mathrm{I}_3$ sont non colinéaires, donc $\mathrm{rg}(A-\dfrac{1}{2}\mathrm{I}_3)\geqslant 2$.
		    De plus, $\dfrac{1}{2}$ est valeur propre donc $\mathrm{rg}(A-\dfrac{1}{2}\mathrm{I}_3)\leqslant 2$.
		    Ainsi, $\mathrm{rg}(A-\dfrac{1}{2}\mathrm{I}_3)= 2$ et $\dim E_{\frac{1}{2}}=1$.
		    Or l’ordre de multiplicité de la valeur propre $\dfrac{1}{2}$ dans le polynôme caractéristique est 2.
		    On en déduit que $A$ est non diagonalisable.

!!! exercice "EXERCICE 70 algèbre  "
	=== "Enoncé"  


		Soit $A=\begin{pmatrix}
		0 & 0 & 1 \\
		1 & 0 & 0 \\
		0 & 1 & 0
		\end{pmatrix} \in \mathcal{M}_{3}\left( \mathbb{C}\right)$ .

		1.  Déterminer les valeurs propres et les vecteurs propres de $A$. $A$ est-elle diagonalisable?

		2.  Soit $\left( a,b,c\right) \in \mathbb{C}^{3}$ et $B=a\mathrm{I}_{3}+bA+cA^{2}$, où $\mathrm{I}_3$ désigne la matrice identité d’ordre 3.
		    Déduire de la question **1.** les éléments propres de $B$.


	=== "Corrigé"  


		1.  $\chi _A (X) =   (X^3  - 1)$ donc $\textrm{Sp} A = \left\{ {1,j,j^2 } \right\}$.
		    On en déduit que $A$ est diagonalisable dans $\mathcal{M}_3(\mathbb{C})$ car elle admet trois valeurs propres distinctes.
		    On pose $E_1 (A) =\mathrm{Ker} (A-\mathrm{I}_3)$, $E_j (A) =\mathrm{Ker} (A-j\mathrm{I}_3)$ et $E_{j^2} (A) =\mathrm{Ker} (A-j^2\mathrm{I}_3)$.
		    Après résolution, on trouve $E_1 (A) = \textrm{Vect}\left( \begin{pmatrix}
		    1\\1\\1
		    \end{pmatrix}\right)$ et $E_j (A) = \textrm{Vect}\left(\begin{pmatrix}
		    1\\j^2\\j
		    \end{pmatrix} \right)$.
		    Et, par conjugaison (comme $A$ est à coefficients réels), $E_{j^2} (A) = \textrm{Vect}\left( \begin{pmatrix}
		    1\\j\\j^2
		    \end{pmatrix}\right)$.

		2.  Soit $e=(e_1,e_2,e_3)$ la base canonique de $\mathbb{C}^3$, vu comme un $\mathbb{C}$-espace vectoriel.
		    Soit $f$ l’endomorphisme canoniquement associé à $A$.
		    On pose $e'_1=(1,1,1)$, $e'_2=(1,j^2,j)$, $e'_3=(1,j,j^2)$ et $e'=(e'_1,e'_2,e'_3)$.
		    D’après 1., $e'$ est une base de vecteurs propres pour $f$.
		    Soit $P$ la matrice de passage de $e$ à $e'$. On a $P=\begin{pmatrix}
		    1&1&1\\
		    1&j^2&j\\
		    1&j&j^2\\
		    \end{pmatrix}$. Soit $D=\begin{pmatrix}
		    1&0&0\\
		    0&j&0\\
		    0&0&j^2\\
		    \end{pmatrix}$.
		    Alors, $D=P^{-1}AP$, c’est-à-dire $A=PDP^{-1}$.
		    On en déduit que $B=a\mathrm{I}_3+bPDP^{-1}+cPD^2P^{-1}=P\left(aI_3+bD+cD^2 \right) P^{-1}$.
		    C’est-à-dire, si on pose $Q=a+bX+cX^2$, alors $B=P\begin{pmatrix}
		    Q(1)&0&0\\
		    0&Q(j)&0\\
		    0&0&Q(j^2)\\
		    \end{pmatrix}P^{-1}$.
		    On en déduit que $B$ est diagonalisable et que les valeurs propres, distinctes ou non, de $B$ sont $Q(1)$, $Q(j)$ et $Q(j^2)$.  
		    **Premier cas**: $Q(1)$, $Q(j)$ et $Q(j^2)$ sont deux à deux distincts
		    $B$ possède trois valeurs propres distinctes : $Q(1)$, $Q(j)$ et $Q(j^2)$.
		    De plus, on peut affirmer que :
		    $E_{Q(1)}(B)=E_1(A)=\mathrm{Vect}\left(\begin{pmatrix}
		    1\\1\\1\end{pmatrix}\right)$ $E_{Q(j)}(B)=E_j(A)=\mathrm{Vect}\left(\begin{pmatrix}1\\j^2 \\j\end{pmatrix}\right)$ et $E_{Q(j^2)}(B)=E_{j^2}(A)=\mathrm{Vect}\left( \begin{pmatrix}
		    1\\j\\j^2\end{pmatrix}\right)$.  
		    **Deuxième cas** : deux valeurs exactement parmi $Q(1)$, $Q(j)$ et $Q(j^2)$ sont égales.
		    Supposons par exemple que $Q(1)=Q(j)$ et $Q(j^2)\neq Q(1)$.
		    $B$ possède deux valeurs propres distinctes : $Q(1)$ et $Q(j^2)$.
		    De plus, on peut affirmer que:
		    $E_{Q(1)}(B)=\mathrm{Vect}\left( \begin{pmatrix}
		    1\\1\\1\end{pmatrix}, \begin{pmatrix}
		    1\\j^2 \\j\end{pmatrix}\right)$ et $E_{Q(j^2)}(B)=\mathrm{Vect}\left( \begin{pmatrix}
		    1\\j\\j^2\end{pmatrix}\right)$.  
		    **Troisième cas**: $Q(1)=Q(j)=Q(j^2)$.
		    $B$ possède une unique valeur propre : $Q(1)$.
		    De plus, on peut affirmer que $B=Q(1)\mathrm{I}_3$ et $E_{Q(1)}(B)=\mathbb{C}^3$.

!!! exercice "EXERCICE 71 algèbre "
	=== "Enoncé"  


		Soit $P$ le plan d’équation $x+y+z=0$ et $D$ la droite d’équation $x=\dfrac{y}{2}=\dfrac{z}{3}$.

		1.  Vérifier que $\mathbb{R}^3=P\oplus D$.

		2.  Soit $p$ la projection vectorielle de $\mathbb{R}^3$ sur $P$ parallèlement à $D$.
		    Soit $u=(x,y,z)\in\mathbb{R}^3$.
		    Déterminer $p(u)$ et donner la matrice de $p$ dans la base canonique de $\mathbb{R}^3$.

		3.  Déterminer une base de $\mathbb{R}^3$ dans laquelle la matrice de $p$ est diagonale.


	=== "Corrigé"  


		1.  $D=\mathrm{Vect}\left( (1,2,3)\right)$.
		    $(1,2,3)\not\in P$ car les coordonnées du vecteur $(1,2,3)$ ne vérifient pas l’équation de $P$.
		    Donc $D\cap P=\left\lbrace 0\right\rbrace$.(\*)
		    De plus, $\dim D+\dim P= 1+2=\dim\mathbb{R}^3$.(\*\*)
		    D’après (\*) et (\*\*), $\mathbb{R}^3=P\oplus D$.

		2.  Soit $u=(x,y,z)\in\mathbb{R}^3$.
		    Par définition d’une projection, $p(u)\in P$ et $u-p(u)\in D$.
		    $u-p(u)\in D$ signifie que $\exists\:\alpha \in\mathbb{R}$ tel que $u-p(u)=\alpha (1,2,3)$.
		    On en déduit que $p(u)=(x-\alpha, y-2\alpha, z-3\alpha)$.(\*\*\*)
		    Or $p(u)\in P$ donc $(x-\alpha)+( y-2\alpha)+(z-3\alpha)=0$, c’est-à-dire $\alpha=\dfrac{1}{6}(x+y+z)$.
		    Et donc, d’après (\*\*\*), $p(u)=\dfrac{1}{6}(5x-y-z,-2x+4y-2z,-3x-3y+3z)$.
		    Soit $e=(e_1,e_2,e_3)$ la base canonique de $\mathbb{R}^3$.
		    Soit $A$ la matrice de $p$ dans la base $e$. On a $A=\dfrac{1}{6}\begin{pmatrix}
		     5&-1&-1\\
		     -2&4&-2\\
		     -3&-3&3\\
		     \end{pmatrix}$.

		3.  On pose $e'_1=(1,2,3)$, $e'_2=(1,-1,0)$ et $e'_3=(0,1,-1)$.
		    $e'_1$ est une base de $D$ et $(e'_2,e'_3)$ est une base de $P$.
		    Or $\mathbb{R}^3=P\oplus D$ donc $e'=(e'_1,e'_2,e'_3)$ est une base de $\mathbb{R}^3$.
		    De plus $e'_1\in D$ donc $p(e'_1)=0$. $e'_2\in P$ et $e'_3\in P$ donc $p(e'_2)=e'_2$ et $p(e'_3)=e'_3$.
		    Ainsi, $M(p,e')=\begin{pmatrix}
		     0&0&0\\
		     0&1&0\\
		     0&0&1\\
		     \end{pmatrix}$.

!!! exercice "EXERCICE 72algèbre  "
	=== "Enoncé"  


		Soit $n$ un entier naturel non nul.
		Soit $f$ un endomorphisme d’un espace vectoriel $E$ de dimension $n$, et soit $e=\left( e_1,\ldots,e_n\right)$ une base de $E$.

		On suppose que $f(e_1)=f(e_2)=\cdots=f(e_n)=v$, où $v$ est un vecteur donné de $E$.

		1.  Donner le rang de $f$.

		2.  $f$ est-il diagonalisable? (discuter en fonction du vecteur $v$)


	=== "Corrigé"  


		1.  Si $v = 0_E$ alors $f$ est l’endomorphisme nul et donc $\mathrm{rg}f=0$.
		    Si $v\neq 0$ alors $\mathrm{rg}f=1$ car, si on note $c_1,c_2,...,c_n$ les colonnes de la matrice $A$ de $f$ dans la base canonique $e$, alors $c_1\neq 0$ et $c_1=c_2=...=c_n$.

		2.  **Premier cas**: $v = 0_E$
		    alors $f$ est l’endomorphisme nul et donc $f$ est diagonalisable.
		    **Deuxième cas**: $v \ne 0_E$.
		    Alors $\textrm{rg} f = 1$ et donc $\dim \mathrm{Ker} f = n - 1$.
		    Donc 0 est valeur propre de $f$ et, si on note $m_0$ l’ordre de multiplicité de la valeur propre 0 dans le polynôme caractéristique de $f$, alors $m_0\geqslant n-1$.
		    On en déduit alors que: $\exists\lambda\in\mathbb{K}\:/\:P_f(X)=X^{n-1}(X-\lambda)$.(\*)
		    Et donc, $\mathrm{tr}(f)=\lambda$.
		    $e$ est une base de $E$ donc : $\exists\:!\:(x_1,x_2,...,x_n)\in\mathbb{K}^n\:/\:v=x_1e_1+x_2e_2+...+x_ne_n$.
		    En écrivant la matrice de $f$ dans la base $e$, on obtient alors $\mathrm{tr}(f)=x_1+x_2+...+x_n$.
		    Ainsi, $\lambda=x_1+x_2+...+x_n$.(\*\*)
		    Ce qui amène à la discussion suivante:
		    **Premier sous- cas**: si $x_1+x_2+...+x_n\neq 0$
		    D’après (\*) et (\*\*), $\lambda=x_1+x_2+...+x_n$ est une valeur propre non nulle de $f$ et $\dim E_{\lambda}=1$.
		    Ainsi, $\dim E_0+\dim E_{\lambda}=n$ et donc $f$ est diagonalisable.
		    **Deuxième sous- cas**: si $x_1+x_2+...+x_n=0$
		    Alors, d’après (\*) et (\*\*), $P_f(X)=X^n$.
		    Donc 0 est valeur propre d’ordre de multiplicité $n$ dans le polynôme caractéristique.
		    Or $\dim E_0=n-1$.
		    Donc $f$ n’est pas diagonalisable.
		    **Remarque dans le cas où $v\neq 0$**
		    Comme $v=x_1e_1+x_2e_2+...+x_ne_n$, alors, par linéarité de $f$, $f(v)=x_1f(e_1)+x_2f(e_2)+...+x_nf(e_n)$.
		    C’est-à-dire, $f(v)=(x_1+x_2+...+x_n)v$.(\*\*\*)
		    On en déduit que: $x_1+x_2+...+x_n=0\Longleftrightarrow f(v)=0$.
		    De plus, dans le cas où $x_1+x_2+...x_n\neq 0$, alors, d’après (\*\*\*), $v$ est un vecteur propre associé à la valeur propre $\lambda=x_1+x_2+...+x_n$ et d’après ce qui précéde, $E_f(\lambda)=\mathrm{Vect}(v)$.

!!! exercice "EXERCICE 73 algèbre "
	=== "Enoncé"  


		On pose $A=\begin{pmatrix}
		2 & 1 \\
		4 & -1
		\end{pmatrix}$.

		1.  Déterminer les valeurs propres et les vecteurs propres de $A$.

		2.  Déterminer toutes les matrices qui commutent avec la matrice $\begin{pmatrix}
		    3 & 0 \\
		    0 & -2
		    \end{pmatrix}$.
		    En déduire que l’ensemble des matrices qui commutent avec $A$ est $\mathrm{Vect}\left( \mathrm{I}_2,A\right)$.


	=== "Corrigé"  


		1.  On obtient le polynôme caractéristique $\chi _A  = (X - 3)(X + 2)$ et donc $\textrm{Sp} A = \left\{ { - 2,3} \right\}$.
		    Après résolution des équations $AX = 3X$ et $AX =  - 2X$, on obtient:
		    $E_3=\mathrm{Vect}\left( \begin{pmatrix}
		    1\\1
		    \end{pmatrix}\right)$ et $E_{-2}=\mathrm{Vect}\left( \begin{pmatrix}
		    1\\-4
		    \end{pmatrix}\right)$.

		2.  Soit $N = \left( {
		    \begin{array}{cc}
		     a & b  \\
		     c & d  \\
		    \end{array}
		    } \right)$.
		    $ND=DN\Longleftrightarrow\left\lbrace\begin{array}{lll}
		    -2b&=&3b\\
		    3c&=&-2c
		    \end{array}\right.$ $\Longleftrightarrow$ $b = c = 0$ $\Longleftrightarrow$ $N$ diagonale.
		    On a $A = PDP^{ - 1}$ avec $P = \left( {
		    \begin{array}{cc}
		     1 & 1  \\
		     1 & { - 4}  \\
		    \end{array}
		    } \right)$ et $D=\begin{pmatrix}
		    3&0\\
		    0&-2\\
		    \end{pmatrix}$.
		    Soit $M \in {\mathcal{M}}_2 (\mathbb{R})$.
		    $AM = MA \Leftrightarrow PDP^{-1}M=MPDP^{-1}$ $\Leftrightarrow D(P^{-1}MP)=(P^{-1}MP)D$ $\Leftrightarrow$ $P^{-1}MP$ commute avec $D$.
		    C’est-à-dire, $AM = MA \Leftrightarrow$ $P^{-1}MP=\begin{pmatrix}
		    a&0\\0&d
		    \end{pmatrix}$$\Leftrightarrow M=P\begin{pmatrix}
		    a&0\\0&d
		    \end{pmatrix}P^{-1}$.
		    Donc, l’espace des matrices commutant avec $A$ est $C(A)=\left\{ {P\left( {
		    \begin{array}{cc}
		     a & 0  \\
		     0 & d  \\
		    \end{array}
		    } \right)P^{ - 1}\: \text{avec}\:(a,d) \in \mathbb{R}^2} \right\}$.
		    C’est un plan vectoriel.
		    De plus, pour des raisons d’inclusion ($\mathrm{I}_2\in C(A)$ et $A\in C(A)$) et d’égalité des dimensions, $C(A)= \textrm{Vect} (\mathrm{I}_2 ,A)$.

!!! exercice "EXERCICE 74 algèbre  "
	=== "Enoncé"  


		1.  On considère la matrice $A=\begin{pmatrix}
		    1 & 0 & 2 \\
		    0 & 1 & 0\\
		    2 & 0 & 1
		    \end{pmatrix}$.

		    1.  Justifier sans calcul que $A$ est diagonalisable.

		    2.  Déterminer les valeurs propres de $A$ puis une base de vecteurs propres associés.

		2.  On considère le système différentiel $\left\{\begin{array}{l}
		     x'=x+2z\\
		     y'=y \\
		     z'=2x+z  
		    \end{array}\right.$ , $x,y,z$ désignant trois fonctions de la variable $t$, dérivables sur $\mathbb{R}$.

		    En utilisant la question 1. et en le justifiant, résoudre ce système.


	=== "Corrigé"  


		1.  1.  A est symétrique réelle donc diagonalisable.

		    2.  $P_A(X)=\det(X\mathrm{I}_3-A)=\begin{vmatrix}
		        -1+X&0&-2\\
		        0&-1+X&0\\
		        -2&0&-1+X\\
		        \end{vmatrix}$.
		        En développant par rapport à la première ligne, on obtient, après factorisation: $P_A(X)=(X-1)(X+1)(X-3)$.
		        On obtient aisément, $E_{1}= \mathrm{Vect}\left( \begin{pmatrix}
		        0\\1\\0
		        \end{pmatrix}\right)$, $E_{{-1}}=\mathrm{Vect}\left(\begin{pmatrix}
		        1\\0\\-1
		        \end{pmatrix} \right)$ et $E_3= \mathrm{Vect}\left( \begin{pmatrix}
		        1\\0\\1
		        \end{pmatrix}\right)$.
		        On pose $e'_1=(0,1,0)$, $e'_2=(1,0,-1)$ et $e'_3=(1,0,1)$.
		        Alors, $e'=(e'_1,e'_2,e'_3)$ est une base de vecteurs propres pour l’endomorphisme $f$ canoniquement associé à la matrice $A$.

		2.  Notons $(S)$ le système $\left\{\begin{array}{l}
		     x'=x+2z \\
		     y'=y \\
		     z'=2x+z
		    \end{array}\right.$.
		    Posons $X(t)=\begin{pmatrix}
		    x(t)\\
		    y(t)\\
		    z(t)\\
		    \end{pmatrix}$ .
		    Alors, $(S) \Longleftrightarrow X'=AX$.
		    On note $P$ la matrice de passage de la base canonique $e$ de $\mathbb{R}^3$ à la base $e'$.
		    D’après 1., $P=\begin{pmatrix}
		    0&1&1\\
		    1&0&0\\
		    0&-1&1
		    \end{pmatrix}$.
		    Et, si on pose $D=\begin{pmatrix}
		    1&0&0\\
		    0&-1&0\\
		    0&0&3
		    \end{pmatrix}$, alors $A=PDP^{-1}$.
		    Donc $(S) \Longleftrightarrow P^{-1}X'=DP^{-1}X$.
		    On pose alors $X_1=P^{-1}X$ et $X_1(t)=\begin{pmatrix}
		    x_1(t)\\
		    y_1(t)\\
		    z_1(t)\\
		    \end{pmatrix}$ .
		    Ainsi, par linéarité de la dérivation, $(S)\Longleftrightarrow X'_1=DX_1\Longleftrightarrow\left\lbrace
		    \begin{array}{lll}
		    x'_1&=&x_1\\
		    y'_1&=&-y_1\\
		    z'_1&=&3z_1
		    \end{array}\right.$
		    On résout alors chacune des trois équations différentielles d’ordre 1 qui constituent ce système.

		     On trouve $\left\lbrace \begin{array}{lll}
		    x_1(t)&=&a\mathrm{e}^{t}\\
		    y_1(t)&=&b\mathrm{e}^{-t}\\
		    z_1(t)&=&c\mathrm{e}^{3t}
		    \end{array}\right.$ avec $(a,b,c)\in\mathbb{R}^3$.
		    Enfin, on détermine $x,y,z$ en utilisant la relation $X=PX_1$.
		    On obtient: $\left\lbrace \begin{array}{lll}
		    x(t)&=&b\mathrm{e}^{-t}+c\mathrm{e}^{3t}\\
		    y(t)&=&a\mathrm{e}^{t}\\
		    z(t)&=&-b\mathrm{e}^{-t}+c\mathrm{e}^{3t}\\
		    \end{array}\right.$ avec $(a,b,c)\in\mathbb{R}^3$.

!!! exercice "EXERCICE 75 algèbre  "
	=== "Enoncé"  


		On considère la matrice $A=\begin{pmatrix}
		-1 & -4 \\
		1 & 3
		\end{pmatrix}$ .

		1.  Démontrer que $A$ n’est pas diagonalisable.

		2.  On note $f$ l’endomorphisme de $\mathbb{R}^{2}$ canoniquement associé à $A$.
		    Trouver une base $\left( v_{1},v_{2}\right)$ de $\mathbb{R}^{2}$ dans laquelle la matrice de $f$ est de la forme $\begin{pmatrix}
		    a & b \\
		    0 & c
		    \end{pmatrix}$.
		    On donnera explicitement les valeurs de $a$, $b$ et $c$.

		3.  En déduire la résolution du système différentiel $\left\{
		    \begin{array}{l}
		    x^{\prime }=-x-4y \\
		    y^{\prime }=x+3y
		    \end{array}
		    \right.$ .


	=== "Corrigé"  


		1.  On obtient le polynôme caractéristique $\chi _A(X)  = (X - 1)^2$, donc $\textrm{Sp} A = \left\{ 1 \right\}$.
		    Si $A$ était diagonalisable, alors $A$ serait semblable à $\mathrm{I}_2$, donc égale à $\mathrm{I}_2$.
		    Ce n’est visiblement pas le cas et donc $A$ n’est pas diagonalisable.

		2.  $\chi _A(X)$ étant scindé, $A$ est trigonalisable. $E_1 (A) = \textrm{Vect} \left(
		    \begin{pmatrix}
		    2\\
		    -1
		    \end{pmatrix}
		     \right)$.
		    Pour $v_1  = (2, - 1)$ et $v_2  = ( - 1,0)$ (choisi de sorte que $f(v_2 ) = v_2  + v_1$) on obtient une base $(v_1 ,v_2 )$ dans laquelle la matrice de $f$ est $T = \left( {
		    \begin{array}{cc}
		     1 & 1  \\
		     0 & 1  \\
		    \end{array}
		    } \right)$.

		3.  On a $A = PTP^{ - 1}$ avec $P = \begin{pmatrix}
		     2 & { - 1}  \\
		     { - 1} & 0  \\
		    \end{pmatrix}$.
		    Posons $X =\begin{pmatrix}
		      x  \\
		      y  \\
		    \end{pmatrix}\text{ et }Y = P^{ - 1} X =
		    \begin{pmatrix}
		      a  \\
		      b  \\
		    \end{pmatrix}$.

		    Le système différentiel étudié équivaut à l’équation $X' = AX$ qui équivaut encore , grâce à la linéarité de la dérivation, à l’équation $Y' = TY$.  
		    Cela nous amène à résoudre le système $\begin{cases}
		      a' = a + b \\
		      b' = b \\
		    \end{cases}$ de solution générale $\begin{cases}
		      a(t) = \lambda {\mathrm{e}}^t  + \mu t{\mathrm{e}}^t\\
		      b(t) = \mu {\mathrm{e}}^t\\
		    \end{cases}$.
		    Enfin, par la relation $X = PY$ on obtient la solution générale du système initial: $\left\{
		    \begin{gathered}
		      x(t) = \left( {(2\lambda  - \mu ) + 2\mu t} \right){\mathrm{e}}^t  \\
		      y(t) = \left( {-\lambda  - \mu t} \right){\mathrm{e}}^t  \\
		    \end{gathered}
		     \right.$

!!! exercice "EXERCICE 76 algèbre  "
	=== "Enoncé"  


		Soit $E$ un $\mathbb{R}$-espace vectoriel muni d’un produit scalaire noté $(\:|\:)$.
		On pose $\forall\:x\in E$, $||x||=\sqrt{(x|x)}$.

		1.  1.  Énoncer et démontrer l’inégalité de Cauchy-Schwarz.

		    2.  Dans quel cas a-t-on égalité? Le démontrer.

		2.  Soit $E=\left\lbrace f\in\mathcal{C}\left( \left[ a,b\right] ,\mathbb{R}\right) ,\:\forall\:x\in \left[ a,b\right]\:f(x)>0  \right\rbrace$.
		    Prouver que l’ensemble $\left\lbrace \displaystyle\int_{a}^{b}f(t)\mathrm{d}t\times \displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t\:,\:f\in E\right\rbrace$ admet une borne inférieure $m$ et déterminer la valeur de $m$.


	=== "Corrigé"  


		1.  1.  Soit $E$ un $\mathbb{R}$-espace vectoriel muni d’un produit scalaire noté $\left(\:|\:\right)$.
		        On pose $\forall\:x\in E$, $||x||=\sqrt{(x|x)}$.

		        Inégalité de Cauchy-Schwarz: $\forall (x,y)\in E^2$, $|\left(x|y\right)|\leqslant ||x||\,||y||$
		        Preuve:
		        Soit $(x,y)\in E^2$. Posons $\forall \lambda\in \mathbb{R}$, $P(\lambda)=||x+\lambda y||^2$.
		        On remarque que $\forall \lambda \in \mathbb{R}$, $P(\lambda )\geqslant 0$.
		        De plus, $P(\lambda)=\left(x+\lambda y|x+\lambda y\right)$.
		        Donc, par bilinéarité et symétrie de $\left(\:|\:\right)$, $P(\lambda )=||y||^2\lambda ^2+2\lambda \left(x|y\right)+||x||^2$.
		        On remarque que $P(\lambda)$ est un trinôme en $\lambda$ si et seulement si $||y||^2\neq 0$.
		        **Premier cas**: si $y=0$
		        Alors $|\left(x|y\right)|=0$ et $||x||\,||y||=0$ donc l’inégalité de Cauchy-Schwarz est vérifiée.
		        **Deuxième cas**: $y\neq 0$
		        Alors $||y||=\sqrt{(y|y)}\neq 0$ car $y\neq 0$ et $\left(\:|\:\right)$ est une forme bilinéaire symétrique définie positive.
		        Donc, $P$ est un trinôme du second degré en $\lambda$ qui est positif ou nul.
		        On en déduit que le discriminant réduit $\Delta$ est négatif ou nul.
		        Or $\Delta=\left(x|y\right)^2-||x||^2||y||^2$ donc $\left(x|y\right)^2\leqslant||x||^2||y||^2$.
		        Et donc, $|\left(x|y\right)|\leqslant ||x||\,||y||$.

		    2.  On reprend les notations de 1. .
		        Prouvons que $\forall (x,y)\in E^2$, $|\left(x|y\right)|=||x||\,||y||$ $\Longleftrightarrow$ $x$ et $y$ sont colinéaires.
		        Supposons que $|\left(x|y\right)|=||x||\,||y||$.
		        Premier cas: si $y=0$
		        Alors $x$ et $y$ sont colinéaires.
		        Deuxième cas: si $y\neq 0$
		        Alors le discriminant de $P$ est nul et donc $P$ admet une racine double $\lambda_0$.
		        C’est-à-dire $P(\lambda_0)=0$ et comme $\left(\:|\:\right)$ est définie positive, alors $x+\lambda_0y=0$.
		        Donc $x$ et $y$ sont colinéaires.
		        Supposons que $x$ et $y$ soient colinéaires.
		        Alors $\exists\:\alpha\in\mathbb{R}$ tel que $x=\alpha y$ ou $y=\alpha x$.
		        Supposons par exemple que $x=\alpha y$ (raisonnement similaire pour l’autre cas).
		        $|\left(x|y\right)|=|\alpha|.|\left(y|y\right)|=|\alpha|\,||y||^2$ et $||x||\,||y||=\sqrt{(x|x)}\,||y||=\sqrt{\alpha^2(y|y)}||y||=|\alpha|.||y||^2$.
		        Donc, on a bien l’égalité.

		2.  On considère le produit scalaire classique sur $\mathcal{C}\left( \left[ a,b\right] ,\mathbb{R}\right)$ défini par :
		    $\forall (f,g)\in \mathcal{C}\left( \left[ a,b\right] ,\mathbb{R}\right)$, $(f|g)=\displaystyle\int_{a}^{b}f(t)g(t)dt$.
		    On pose $A=\left\lbrace \displaystyle\int_{a}^{b}f(t)\mathrm{d}t\times \displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t\:,\:f\in E\right\rbrace$.
		    $A\subset \mathbb{R}$.
		    $A\neq \emptyset$ car $(b-a)^2\in A$ ( valeur obtenue pour la fonction $t\longmapsto 1$ de $E$).
		    De plus, $\forall\:f\in E$,$\displaystyle\int_{a}^{b}f(t)\mathrm{d}t\times \displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t\geqslant 0$ donc $A$ est minorée par 0.
		    On en déduit que $A$ admet une borne inférieure et on pose $m=\inf A$.
		    Soit $f\in E$.
		    On considère la quantité $\left( \displaystyle\int_{a}^{b}\sqrt{f(t)}\dfrac{1}{\sqrt{f(t)}}\mathrm{d}t\right) ^2$.
		    D’une part, $\left( \displaystyle\int_{a}^{b}\sqrt{f(t)}\dfrac{1}{\sqrt{f(t)}}\mathrm{d}t\right)  ^2=\left( \displaystyle\int_{a}^{b}1\mathrm{d}t\right) ^2=(b-a)^2.$
		    D’autre part, si on utilise l’inégalité de Cauchy-Schwarz pour le produit scalaire $(\:|\:)$ on obtient:
		    $\left( \displaystyle\int_{a}^{b}\sqrt{f(t)}\dfrac{1}{\sqrt{f(t)}}\mathrm{d}t\right) ^2\leqslant \displaystyle\int_{a}^{b} f(t)\mathrm{d}t\displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t$.
		    On en déduit que $\forall\:f\in E$, $\displaystyle\int_{a}^{b} f(t)\mathrm{d}t\displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t\geqslant (b-a)^2$.
		    Donc $m\geqslant (b-a)^2$.
		    Et, si on considère la fonction $f:t\longmapsto 1$ de $E$, alors $\displaystyle\int_{a}^{b} f(t)\mathrm{d}t\displaystyle\int_{a}^{b}\dfrac{1}{f(t)}\mathrm{d}t= (b-a)^2$.
		    Donc $m=(b-a)^2$.

!!! exercice "EXERCICE 77 algèbre  "
	=== "Enoncé"  


		Soit $E$ un espace euclidien.

		1.  Soit $A$ un sous-espace vectoriel de $E$.
		    Démontrer que $\left( A^{\perp }\right) ^{\perp }=A$.

		2.  Soient $F$ et $G$ deux sous-espaces vectoriels de $E$.

		    1.  Démontrer que  $\left( F+G\right) ^{\perp }=F^{\perp }\cap G^{\perp }$.

		    2.  Démontrer que $\left( F\cap G\right) ^{\perp }=F^{\perp}+G^{\perp }$.


	=== "Corrigé"  


		1.  On a $A \subset \left( {A^ \bot  } \right)^ \bot  \text{ }$.(\*)
		    En effet, $\forall x \in A,\forall y \in A^ \bot  ,(x\mid y) = 0$.
		    C’est-à-dire, $\forall\:x\in A$, $x\in (A^{\perp})^{\perp}$.
		    Comme $E$ est un espace euclidien, $E=A\oplus A^{\perp}$ donc $\dim A= n-\dim A^{\perp}$.
		    De même, $E=A^{\perp}\oplus\left( A^{\perp}\right) ^{\perp}$ donc $\dim \left( A^ \bot\right) ^{\perp}   = n - \dim A^{\perp}$.
		    Donc $\dim \left( {A^ \bot  } \right)^ \bot   = \dim A$. (\*\*)
		    D’après (\*) et (\*\*), $\left( {A^ \bot  } \right)^ \bot   = A$.

		2.  1.  Procédons par double inclusion.
		        Prouvons que $F^{\perp}\cap G^{\perp}\subset \left(F+G \right)^{\perp}$.
		        Soit $x \in F^ \bot   \cap G^ \bot$.
		        Soit $y \in F + G$ .
		        Alors $\exists\:(f,g)\in F\times G$ tel que $y=f+g$.
		        $(x\mid y) = \underbrace{(x\mid f)}_{ \underset{\: \text{car} \:f\in F\:\text{et}\:x\in F^{\perp}}{=0}}+ \underbrace{(x\mid g)}_{ \underset{\: \text{car} \:g\in G \:\text{et}\:x\in G^{\perp}}{=0}} = 0$.
		        Donc $\forall \:y\in (F+G)$, $(x\mid y) =0$.
		        Donc $x \in (F + G)^ \bot$.
		        Prouvons que $\left(F+G \right)^{\perp}\subset F^{\perp}\cap G^{\perp}$.
		        Soit $x \in (F + G)^ \bot$.
		        $\forall \:y \in F$, on a $(x\mid y) = 0$ car $y \in F \subset F + G$.
		        Donc $x \in F^ \bot$.
		        De même, $\forall \:z \in G$, on a $(x\mid z) = 0$ car $z \in G \subset F + G$.
		        Donc $x \in G^ \bot$.
		        On en déduit que $x\in F^{\perp}\cap G^{\perp}$.
		        Finalement, par double inclusion, $\left( {F + G} \right)^ \bot   = F^ \bot   \cap G^ \bot$.

		    2.  D’après 2.(a), appliquée à $F^{\perp}$ et à $G^{\perp}$, on a $\left( F^{\perp} + G^{\perp} \right)^ {\perp  } = \left( F^{\perp}\right) ^{\perp}   \cap\left(  G^ {\perp}\right) ^{\perp}$.
		        Donc, d’après 1., $\left( F^{\perp} + G^{\perp} \right)^ {\perp  } = F   \cap G$.
		        Donc $\left( \left( F^{\perp} + G^{\perp} \right)^ {\perp  }\right) ^{\perp} =\left(  F   \cap G \right) ^{\perp}$.
		        C’est-à-dire, en utilisant 1. à nouveau, $F^{\perp}+G^{\perp }=\left( F\cap G\right) ^{\perp }$.

!!! exercice "EXERCICE 78 algèbre  "
	=== "Enoncé"  


		Soit $E$ un espace euclidien de dimension $n$ et $u$ un endomorphisme de $E$.
		On note $\left( x|y\right)$ le produit scalaire de $x$ et de $y$ et $||.||$ la norme euclidienne associée.

		1.  Soit $u$ un endomorphisme de $E$, tel que: $\forall x \in E, \vert\vert u(x)\vert\vert = \vert\vert x\vert\vert$.

		    1.  Démontrer que: $\forall (x,y)\in E^{2}~(u(x)|u(y)) = (x|y)$.

		    2.  Démontrer que $u$ est bijectif.

		2.  Démontrer que l’ensemble ${\mathcal{O}}(E)$ des isométries vectorielles de $E$ , muni de la loi $\circ$ , est un groupe.

		3.  Soit $u\in\mathcal{L}(E)$. Soit $e=(e_1,e_2,...,e_n)$ une base orthonormée de $E$.
		    Prouver que : $u\in {\mathcal{O}}(E)\Longleftrightarrow$$\left( u(e_1),u(e_2),...,u(e_n)\right)$ est une base orthonormée de $E$.


	=== "Corrigé"  


		1.  Soit $u\in\mathcal{L}(E)$ tel que $\forall (x,y)\in E^{2}$, $||u(x)||=||x||$.

		    1.  Soit $(x,y) \in E^2$.
		        On a, d’une part, $\left\| {u(x + y)} \right\|^2  = \left\| {x + y} \right\|^2  = \left\| x \right\|^2  + 2(x\mid y) + \left\| y \right\|^2$.(\*)
		        D’autre part, $\left\| {u(x + y)} \right\|^2  = \left\| {u(x) + u(y)} \right\|^2  = \left\| {u(x)} \right\|^2  + 2(u(x)\mid u(y)) + \left\| {u(y)} \right\|^2=  \left\| {x} \right\|^2  + 2(u(x)\mid u(y)) + \left\| {y} \right\|^2$.(\*\*)
		        On en déduit, d’après (\*) et (\*\*), que $(u(x)\mid u(y)) = (x\mid y)$.

		    2.  Soit $x \in \mathrm{Ker} u$.
		        Par hypothèse, $0 = \left\| {u(x)} \right\|^2  = \left\| x \right\|^2 \text{ }$.
		        Donc $x=0$.
		        Donc $\mathrm{Ker} u = \left\{ {0_E } \right\}$.
		        Donc $u$ est injectif.
		        Puisque $E$ est de dimension finie, on peut conclure que l’endomorphisme $u$ est bijectif.

		2.  Montrons que l’ensemble ${\mathcal{O}}(E)$ des endomorphismes orthogonaux est un sous-groupe du groupe linéaire $\left( {{\text{GL}}(E), \circ } \right)$.
		    On a ${\mathcal{O}}(E) \subset {\text{GL}}(E)$ en vertu de ce qui précède.
		    On a aussi, évidemment, $\textrm{Id} _E  \in {\mathcal{O}}(E)$. Donc $\mathcal{O}(E)\neq \emptyset$.
		    Soit $(u,v) \in\left(  {\mathcal{O}}(E)\right) ^2$.
		    $\forall \:x \in E$, $\left\| {u \circ v^{ - 1} (x)} \right\| = \left\| {u(v^{ - 1} (x))} \right\| = \left\| {v^{ - 1} (x)} \right\|\text{ car }u \in {\mathcal{O}}(E)$.
		    Et $\left\| {v^{ - 1} (x)} \right\| = \left\| {v(v^{ - 1} (x))} \right\| = \left\| x \right\|\text{ car }v \in {\mathcal{O}}(E)$.
		    Donc $\forall \:x \in E$,$\left\| {u \circ v^{ - 1} (x)} \right\| = \left\| x \right\|$.
		    On en déduit, d’après 1.(a), que $u \circ v^{ - 1}  \in {\mathcal{O}}(E)$.

		3.  Soit $u\in\mathcal{L}(E)$. Soit $e=(e_1,e_2,...,e_n)$ une base orthonormée de $E$.
		    Supposons que $u\in \mathcal{O}(E)$.
		    Soit $(i,j)\in\left( \llbracket1,n\rrbracket\right) ^2$.
		    $u\in \mathcal{O}(E)$ donc $\left(u(e_i)|u(e_j)\right)=\left(e_i|e_j\right)$.
		    Or $e$ est une base orthonormée de $E$ donc $\left(e_i|e_j\right)=\delta_i^j$ où $\delta_i^j$ désigne le symbole de Kronecker.
		    On en déduit que $\forall(i,j)\in\left( \llbracket1,n\rrbracket\right) ^2$,$\left(u(e_i)|u(e_j)\right)=\delta_i^j$.
		    C’est-à-dire $(u(e_1),u(e_2),...,u(e_n))$ est une famille orthonormée de $E$.
		    Donc, c’est une famille libre à $n$ éléments de $E$ avec $\dim E=n$.
		    Donc $(u(e_1),u(e_2),...,u(e_n))$ est une base orthonormée de $E$.
		    Réciproquement, supposons que $(u(e_1),u(e_2),...,u(e_n))$ est une base orthonormée de $E$.
		    Soit $x\in E$.
		    Comme $e$ est une base orthonormée de $E$, $x=\displaystyle\displaystyle\sum\limits_{i=1}^{n}x_ie_i$.
		    $||x||^2=\left(\displaystyle\displaystyle\sum\limits_{i=1}^{n}x_ie_i|\displaystyle\displaystyle\sum\limits_{j=1}^{n}x_je_j\right)=\displaystyle\displaystyle\sum\limits_{i=1}^{n}\displaystyle\displaystyle\sum\limits_{j=1}^{n}x_ix_j\left(e_i|e_j\right)$.
		    Or $e$ est une base orthonormée de $E$ donc $||x||^2=\displaystyle\displaystyle\sum\limits_{i=1}^{n}x_i^2$ .(\*)
		    De même, par linéarité de $u$, $||u(x)||^2=(\displaystyle\displaystyle\sum\limits_{i=1}^{n}x_iu(e_i)|\displaystyle\displaystyle\sum\limits_{j=1}^{n}x_ju(e_j))=\displaystyle\displaystyle\sum\limits_{i=1}^{n}\displaystyle\displaystyle\sum\limits_{j=1}^{n}x_ix_j\left(u(e_i)|u(e_j)\right)$.
		    Or $(u(e_1),u(e_2),...,u(e_n))$ est une base orthonormée de $E$, donc $||u(x)||^2=\displaystyle\displaystyle\sum\limits_{i=1}^{n}x_i^2$.(\*\*)
		    D’après (\*) et (\*\*), $\forall \:x\in E$, $||u(x)||=||x||$.
		    Donc, d’après 1.(a), $u\in  \mathcal{O}(E)$.

!!! exercice "EXERCICE 79 algèbre "
	=== "Enoncé"  


		Soit $a$ et $b$ deux réels tels que $a<b$.

		1.  Soit $h$ une fonction continue et positive de $[a,b]$ dans $\mathbb{R}$.

		    Démontrer que $\displaystyle\int_{a}^{b}h(x)\text{d}x=0\Longrightarrow h=0$ .

		2.  Soit $E$ le $\mathbb{R}$-espace vectoriel des fonctions continues de $[a,b]$ dans $\mathbb{R}$.
		    On pose : $\forall\:(f,g)\in E^2$, $\left( f|g\right) =\displaystyle\displaystyle\int_{a}^{b}f(x)g(x)\text{d}x$.
		    Démontrer que l’on définit ainsi un produit scalaire sur $E$.

		3.  Majorer $\displaystyle\int_{0}^{1}\sqrt{x}e^{-x}\text{d}x$ en utilisant l’inégalité de Cauchy-Schwarz.


	=== "Corrigé"  


		1.  Soit $h$ une fonction continue et positive de $[a,b]$ dans $\mathbb{R}$ telle que $\displaystyle\int_{a}^{b}h(x)\text{d}x=0$.
		    On pose $\forall \:x\in \left[ a,b\right]$, $F(x)=\displaystyle\int_{a}^{x} h(t)dt$.
		    $h$ est continue sur $\left[ a,b\right]$ donc $F$ est dérivable sur $\left[ a,b\right]$.
		    De plus, $\forall\:x\in\left[ a,b\right]$, $F'(x)=h(x)$.
		    Or $h$ est positive sur $\left[ a,b\right]$ donc $F$ est croissante sur $\left[ a,b\right]$.(\*)
		    Or $F(a)=0$ et, par hypothèse, $F(b)=0$. C’est-à-dire $F(a)=F(b)$.(\*\*)
		    D’après (\*) et (\*\*), $F$ est constante sur $\left[ a,b\right]$.
		    Donc $\forall\:x\in \left[ a,b\right]$, $F'(x)=0$.
		    C’est-à-dire, $\forall\:x\in \left[ a,b\right]$, $h(x)=0$.

		2.  On pose $\forall\:(f,g)\in E^2$, $\left( f|g\right) =\displaystyle\int_{a}^{b}f(x)g(x)\text{d}x$.
		    Par linéarité de l’intégrale, $\left(\:|\:\right)$ est linéaire par rapport à sa première variable.
		    Par commutativité du produit sur $\mathbb{R}$, $\left(\:|\:\right)$ est symétrique.
		    On en déduit que $\left(\:|\:\right)$ est une forme bilinéaire symétrique.(\*)
		    Soit $f\in E$. $\left(f|f\right)=\displaystyle\int_{a}^{b}f^2(x)\mathrm{d}x$.
		    Or $x\longmapsto f^2(x)$ est positive sur $\left[ a,b\right]$ et $a<b$ donc $\left(f|f\right)\geqslant 0$.
		    Donc $\left(\:|\:\right)$ est positive.(\*\*)
		    Soit $f\in E$ telle que $\left(f|f\right)=0$.
		    Alors $\displaystyle\int_{a}^{b}f^2(x)\mathrm{d}x=0$.
		    Or $x\longmapsto f^2(x)$ est positive et continue sur $\left[ a,b\right]$ .
		    Donc, d’après 1., $f$ est nulle sur $\left[ a,b\right]$ .
		    Donc $\left(\:|\:\right)$ est définie.(\*\*\*)
		    D’après (\*), (\*\*) et (\*\*\*), $\left(\:|\:\right)$ est un produit scalaire sur $E$.

		3.  L’inégalité de Cauchy-Schwarz donne $\displaystyle\int_{0}^{1} {\sqrt x {\mathrm{e}}^{ - x} \,{\mathrm{d}}x}  \leqslant \sqrt {\displaystyle\int_{0}^{1} {x\,{\mathrm{d}}x} } \sqrt {\displaystyle\int_0^1 {{\mathrm{e}}^{ - 2x} \,{\mathrm{d}}x} }  = \dfrac{{\sqrt {1 - {\mathrm{e}}^{ - 2} } }}{2}$.

!!! exercice "EXERCICE 80 algèbre "
	=== "Enoncé"  


		Soit $E\ $l’espace vectoriel des applications continues et $2\pi$-périodiques de $\mathbb{R}$ dans $\mathbb{R}$.

		1.  Démontrer que $\left( f\ |\ g\right) =\dfrac{1}{2\pi }\displaystyle\int_{0}^{2\pi }f\left( t\right) g\left( t\right) \text{d}t$ définit un produit scalaire sur $E$.

		2.  Soit $F$ le sous-espace vectoriel engendré par $f:x\mapsto \cos x$ et $g:x\mapsto \cos \left( 2x\right)$.

		    Déterminer le projeté orthogonal sur $F$ de la fonction $u:x\mapsto \sin ^{2}x$.


	=== "Corrigé"  


		1.  On pose $\forall\:(f,g)\in E^2$, $\left( f|g\right) =\dfrac{1}{2\pi}\displaystyle\int_{0}^{2\pi}f(t)g(t)\text{d}t$.
		    Par linéarité de l’intégrale, $\left(\:|\:\right)$ est linéaire par rapport à sa première variable.
		    Par commutativité du produit sur $\mathbb{R}$, $\left(\:|\:\right)$ est symétrique.
		    On en déduit que $\left(\:|\:\right)$ est une forme bilinéaire symétrique.(\*)
		    Soit $f\in E$. $\left(f|f\right)=\dfrac{1}{2\pi}\displaystyle\int_{0}^{2\pi}f^2(t)\mathrm{d}t$.
		    Or $t\longmapsto f^2(t)$ est positive sur $\left[ 0,2\pi\right]$ et $0<2\pi$, donc $\left(f|f\right)\geqslant 0$.
		    Donc $\left(\:|\:\right)$ est positive.(\*\*)
		    Soit $f\in E$ telle que $\left(f|f\right)=0$.
		    Alors $\displaystyle\int_{0}^{2\pi}f^2(t)\mathrm{d}t=0$.
		    Or $t\longmapsto f^2(t)$ est positive et continue sur $\left[ 0,2\pi\right]$.
		    Donc, $f$ est nulle sur $\left[ 0,2\pi\right]$.
		    Or $f$ est $2\pi$-périodique donc $f=0$.
		    Donc $\left(\:|\:\right)$ est définie.(\*\*\*)
		    D’après (\*), (\*\*) et (\*\*\*), $\left(\:|\:\right)$ est un produit scalaire sur $E$.

		2.  On a $\forall x \in \mathbb{R},\sin ^2 x = \dfrac{1}{2} -  \dfrac{1}{2}\cos (2x)$.
		    $x\longmapsto -\dfrac{1}{2}\cos (2x)$$\in F$.
		    De plus, si on note $h$ l’application $x \mapsto \dfrac{1}{2}$,
		    $\left(h|f\right)=\dfrac{1}{4\pi}\displaystyle\int_{0}^{2\pi}\cos x \mathrm{d}x=0$ et $\left(h|g\right)=\dfrac{1}{4\pi}\displaystyle\int_{0}^{2\pi}\cos (2x)\mathrm{d}x=0$ donc $h\in F^{\perp}$ (car $F=\mathrm{Vect}(f,g)$).
		    On en déduit que le projeté orthogonal de $u$ sur $F$ est $x\longmapsto -\dfrac{1}{2}\cos (2x)$.

!!! exercice "EXERCICE 81 algèbre "
	=== "Enoncé"  


		On définit dans $\mathcal{M}_{2}\left( \mathbb{R}\right) \times \mathcal{M}_{2}\left( \mathbb{R}\right)$ l’application $\varphi$ par : $\varphi \left( A,A'\right)
		=\text{tr}\left( ^{t}AA'\right)$, où $\text{tr}\left( ^{t}AA'\right)$ désigne la trace du produit de la matrice $^tA$ par la matrice $A'$.
		On admet que $\varphi$ est un produit scalaire sur $\mathcal{M}_{2}\left(
		\mathbb{R}\right)\ .$
		On note $\mathcal{F}=\left\{ \left(
		\begin{array}{cc}
		a & b \\
		-b & a%
		\end{array}%
		\right),\ \left( a,b\right) \in \mathbb{R}^{2}\right\}$.

		1.  Démontrer que $\mathcal{F}$ est un sous-espace vectoriel de $\mathcal{M}_{2}\left( \mathbb{R}\right)$.

		2.  Déterminer une base de $\mathcal{F}^{\perp }$.

		3.  Déterminer le projeté orthogonal de $J=\left(
		    \begin{array}{cc}
		    1 & 1 \\
		    1 & 1%
		    \end{array}%
		    \right)$ sur $\mathcal{F}^{\perp }$ .

		4.  Calculer la distance de $J$ à $\mathcal{F}$.


	=== "Corrigé"  


		1.  On a immédiatement ${\mathcal{F}} = \textrm{Vect} (\mathrm{I}_2,K)$ avec $K = \left( {
		    \begin{array}{cc}
		     0 & 1  \\
		     { - 1} & 0  \\
		    \end{array}
		    } \right)\text{ }$.
		    On peut donc affirmer que ${\mathcal{F}}$ est un sous-espace vectoriel de ${\mathcal{M}}_2 (\mathbb{R})$.

		    ${\mathcal{F}} = \textrm{Vect} (\mathrm{I}_2,K)$ donc $(\mathrm{I}_2,K)$ est une famille génératrice de $\mathcal{F}$.
		    De plus, $\mathrm{I}_2$ et $K$ sont non colinéaires donc la famille $(\mathrm{I}_2,K)$ est libre.
		    On en déduit que $(\mathrm{I}_2,K)$ est une base de $\mathcal{F}$.

		2.  Soit $M=\begin{pmatrix}
		     a&b\\
		     c&d
		     \end{pmatrix}\in{\mathcal{M}}_2\left( \mathbb{R}\right)$.
		    Comme $(\mathrm{I}_2,K)$ est une base de $\mathcal{F}$,
		    $M\in\mathcal{F}^{\perp}\Longleftrightarrow$ $\varphi(M,\mathrm{I}_2)=0$ et $\varphi(M,K)=0$.
		    C’est-à-dire, $M\in\mathcal{F}^{\perp}\Longleftrightarrow$ $a+d=0$ et $b-c=0$.
		    Ou encore, $M\in\mathcal{F}^{\perp}\Longleftrightarrow$ $d=-a$ et $c=b$.
		    On en déduit que $\mathcal{F}^{\perp}=\mathrm{Vect}\left( A,B\right)$ avec $A = \left(
		    \begin{array}{cc}
		     1 & 0  \\
		     0 & { - 1}  \\
		    \end{array}
		     \right)\text{ et }B = \left( {
		    \begin{array}{cc}
		     0 & 1  \\
		     1 & 0  \\
		    \end{array}
		    } \right)$.
		    $(A,B)$ est une famille libre et génératrice de $\mathcal{F}^{\perp}$ donc $(A,B)$ est une base de $\mathcal{F}^{\perp}$.

		3.  On peut écrire $J = \mathrm{I}_2 +  B\text{  avec }\mathrm{I}_2 \in {\mathcal{F}}\text{ et } B \in {\mathcal{F}}^ \bot$.
		    Donc le projeté orthogonal de $J$ sur $\mathcal{F}^{\perp}$ est $B=\begin{pmatrix}
		    0&1\\1&0
		    \end{pmatrix}$.

		4.  On note $d(J,\mathcal{F})$ la distance de $J$ à $\mathcal{F}$.
		    D’après le cours, $d(J,\mathcal{F})=||J-p_{\mathcal{F}}(J)||$ où $p_{\mathcal{F}}(J)$ désigne le projeté orthogonal de $J$ sur $\mathcal{F}$.
		    On peut écrire à nouveau que $J = \mathrm{I}_2 +  B\text{  avec }\mathrm{I}_2 \in {\mathcal{F}}\text{ et } B \in {\mathcal{F}}^ \bot$.
		    Donc $p_{\mathcal{F}}(J)=\mathrm{I}_{2}$.
		    On en déduit que $d(J,\mathcal{F})=||J-p_{\mathcal{F}}(J)||=||J-\mathrm{I}_2||=||B||=\sqrt{2}$.

!!! exercice "EXERCICE 82 algèbre "
	=== "Enoncé"  


		Soit $E$ un espace préhilbertien et $F$ un sous-espace vectoriel de $E$ de dimension finie $n>0$.

		On admet que, pour tout $x\in E$, il existe un élément unique $y_0$ de $F$ tel que $x-y_0$ soit orthogonal à $F$ et que la distance de $x$ à $F$ soit égale à $\left\Vert x-y_0\right\Vert$.

		Pour $A=\begin{pmatrix}
		a & b \\
		c & d%
		\end{pmatrix}$ et $A'=\begin{pmatrix}
		a^{\prime } & b^{\prime } \\
		c^{\prime } & d^{\prime }%
		\end{pmatrix}$, on pose $\left(  A\ |\ A'\right) =aa^{\prime}+bb^{\prime}+cc^{\prime}+dd^{\prime}$.

		1.  Démontrer que $\left( \,.\,|\,.\,\right)$ est un produit scalaire sur $\mathcal{M}_{2}\left( \mathbb{R}\right)$.

		2.  Calculer la distance de la matrice $A=\left(
		    \begin{array}{cc}
		    1 & 0 \\
		    -1 & 2%
		    \end{array}%
		    \right)$ au sous-espace vectoriel $F$ des matrices triangulaires supérieures.


	=== "Corrigé"  


		1.  On pose $E=\mathcal{M}_2(\mathbb{R})$.
		    Pour $A=\begin{pmatrix}
		    a&b\\c&d\\
		    \end{pmatrix}\in E$ et $A'=\begin{pmatrix}
		    a'&b'\\c'&d'\\
		    \end{pmatrix}\in E$ , on pose $\left(A|A'\right)=aa'+bb'+cc'+dd'$.
		    Soit $A=\begin{pmatrix}
		    a&b\\c&d\\
		    \end{pmatrix}\in E$ , $A'=\begin{pmatrix}
		    a'&b'\\c'&d'\\
		    \end{pmatrix}\in E$ , $B=\begin{pmatrix}
		    a''&b''\\c''&d''\\
		    \end{pmatrix}\in E$ . Soit $\alpha\in\mathbb{R}$.
		    $\left(A+A'|B\right)=\left(\begin{pmatrix}
		    a+a'&b+b'\\c+c'&d+d'\\
		    \end{pmatrix}|\begin{pmatrix}
		    a''&b''\\c''&d''\\
		    \end{pmatrix}
		    \right)=(a+a')a''+(b+b')b''+(c+c')c''+(d+d')d''$.
		    Donc $\left(A+A'|B\right)=(aa''+bb''+cc''+dd'')+(a'a''+b'b''+c'c''+d'd'')=\left(A|B\right)+\left(A'|B\right)$.
		    $\left(\alpha A|B\right)=\left(\begin{pmatrix}
		    \alpha a& \alpha b\\
		    \alpha c&\alpha d\\
		    \end{pmatrix}|\begin{pmatrix}
		    a''&b''\\c''&d''
		    \end{pmatrix}\right )= \alpha aa''+\alpha bb''+\alpha cc''+ \alpha dd''=\alpha\left(A|B\right )$.
		    On en déduit que $\left( \,.\,|\,.\,\right)$ est linéaire par rapport à sa première variable.
		    De plus, par commutativité du produit sur $\mathbb{R}$, $\left( \,.\,|\,.\,\right)$ est symétrique.
		    Donc $\left( \,.\,|\,.\,\right)$ est une forme bilinéaire et symétrique.(\*)
		    Soit $A=\begin{pmatrix}
		    a&b\\c&d\\
		    \end{pmatrix}\in E$ .
		    $\left(A|A\right)=a^2+b^2+c^2+d^2\geqslant 0$. Donc $\left( .\:|\:.\right)$ est positive.(\*\*)
		    Soit $A=\begin{pmatrix}
		    a&b\\c&d\\
		    \end{pmatrix}\in E$ telle que $\left(A|A\right)=0$.
		    Alors $a^2+b^2+c^2+d^2= 0$.
		    Comme il s’agit d’une somme de termes tous positifs, on en déduit que $a=b=c=d=0$ donc $A=0$.
		    Donc $\left( \,.\,|\,.\,\right)$ est définie.(\*\*\*)
		    D’après (\*), (\*\*) et (\*\*\*), $\left( \,.\,|\,.\,\right)$ est un produit scalaire sur $E$.

		2.  $A=\begin{pmatrix}
		    1&0\\-1&2
		    \end{pmatrix}$.
		    On a $A= \begin{pmatrix}
		    1&0\\0&2
		    \end{pmatrix}+\begin{pmatrix}
		    0&0\\-1&0
		    \end{pmatrix}$.
		    $\begin{pmatrix}
		    1&0\\0&2
		    \end{pmatrix}\in F$ et $\begin{pmatrix}
		    0&0\\-1&0
		    \end{pmatrix}\in F^{\perp}$ car $\forall (a,b,d)\in\mathbb{R}^3$, $\left(\begin{pmatrix}
		    0&0\\-1&0
		    \end{pmatrix}|
		    \begin{pmatrix}
		    a&b\\0&d
		    \end{pmatrix}\right)=0$.
		    On en déduit que le projeté orthogonal, noté $p_F(A)$, de $A$ sur $F$ est la matrice $\begin{pmatrix}
		    1&0\\0&2
		    \end{pmatrix}$.
		    Ainsi, $d(A,F)=||A-p_F(A)||=||\begin{pmatrix}
		    0&0\\-1&0
		    \end{pmatrix}||=1.$

!!! exercice "Exercice 83 algèbre "
	=== "Enoncé"  


		Soit $u$ et $v$ deux endomorphismes d’un $\mathbb{R}$-espace vectoriel $E$.

		1.  Soit $\lambda$ un réel non nul. Prouver que si $\lambda$ est valeur propre de $u\circ v$, alors $\lambda$ est valeur propre de $v\circ u$.

		2.  On considère, sur $E=\mathbb{R}\left[ X\right]$ les endomorphismes $u$ et $v$ définis par $u$:$P\longmapsto \displaystyle\int_{1}^{X}P$ et $v$:$P\longmapsto P'$ .
		    Déterminer $\mathrm{Ker} (u\circ v)$ et $\mathrm{Ker} (v\circ u)$. Le résultat de la question 1. reste-t-il vrai pour $\lambda=0$?

		3.  Si $E$ est de dimension finie, démontrer que le résultat de la première question reste vrai pour $\lambda=0$.
		    **Indication** : penser à utiliser le déterminant.


	=== "Corrigé"  


		1.  Soit $\lambda\neq0$.
		    Si $\lambda$ valeur propre de $u\circ v$ alors $\exists\:x\in{E\setminus\left\lbrace  0 \right\rbrace }\:/\: (u\circ v)(x)=\lambda x$. ($\ast$)
		    Pour un tel $x$ non nul, on a alors $v(u\circ v(x))=\lambda v(x)$ c’est-à-dire $(v\circ u)(v(x))=\lambda v(x)$ ($\ast\ast$).
		    Si $v(x)=0$ alors, d’après ($\ast$), $\lambda x=0$. Ce qui est impossible car $x\neq 0$ et $\lambda\neq 0$.
		    Donc $v(x)\neq 0$.
		    Donc, d’après ($\ast\ast$), $v(x)$ est un vecteur propre de $v\circ u$ associé à la valeur propre $\lambda$.

		2.  On trouve que $v\circ u=\mathrm{Id}$ et $u\circ v$: $P\longmapsto P(X)-P(1)$.
		    Ainsi $\mathrm{Ker}(v\circ u)=\lbrace0\rbrace$ et $\mathrm{Ker}(u\circ v)=\mathbb{R}_{0}\left[ X\right]$.
		    On observe que 0 est valeur propre de $u\circ v$ mais n’est pas valeur propre de $v\circ u$.
		    On constate donc que le résultat de la question 1. est faux pour $\lambda=0$.

		3.  Si $E$ est de dimension finie, comme $\det (u\circ v)=\det u \:\det v=\det(v\circ u)$ alors :
		    0 est valeur propre de $u\circ v$ $\Longleftrightarrow$ $\det(u\circ v)=0$ $\Longleftrightarrow$ $\det (v\circ u)=0$ $\Longleftrightarrow$ 0 est valeur propre de $v\circ u$.
		    **Remarque 1**: le résultat de la question 1. est vrai pour $\lambda=0$ si et seulement si $E$ est de dimension finie.
		    **Remarque 2**: Si $E$ est de dimension finie, $u\circ v$ et $v\circ u$ ont les mêmes valeurs propres.

!!! exercice "EXERCICE 84 algèbre "
	=== "Enoncé"  


		1.  Donner la définition d’un argument d’un nombre complexe non nul (on ne demande ni l’interprétation géométrique, ni la démonstration de l’existence d’un tel nombre).

		2.  Soit $n\in{\mathbb{N}^{*}}$. Donner, en justifiant, les solutions dans $\mathbb{C}$ de l’équation $z^{n}=1$ et préciser leur nombre.

		3.  En déduire, pour $n\in{\mathbb{N}^{*}}$, les solutions dans $\mathbb{C}$ de l’équation $\left( z+\mathrm{i}\right)^{n}=\left(z-\mathrm{i}\right)^{n}$ et démontrer que ce sont des nombres réels.


	=== "Corrigé"  


		1.  Soit $z$ un complexe non nul. Posons $z=x+\mathrm{i}y$ avec $x$ et $y$ réels.
		    Un argument de $z$ est un réel $\theta$ tel que $\frac{z}{|z|} =\mathrm{e}^{\mathrm{i}\theta}$ avec $|z|=\sqrt{x^{2}+y^{2}}$.

		2.  $z=0$ n’est pas solution de l’équation $z^n=1$.
		    Les complexes solutions s’écriront donc sous la forme $z=r\mathrm{e}^{\mathrm{i}\theta}$ avec $r>0$ et $\theta\in\mathbb{R}$.
		    On a $z^n=1 \: \Longleftrightarrow \:\left\lbrace \begin{array}{c} r^{n}=1 \\
		     \text{et}\\
		      n\theta=0\mod2\pi
		      \end{array}
		      \right.$ $\: \Longleftrightarrow \:\left\lbrace \begin{array}{c} r=1 \\
		     \text{et}\\
		      \theta=\dfrac{2k\pi}{n}\:\text{avec}\:k\in{\mathbb{Z}}
		      \end{array}
		      \right.$
		    Les réels $\dfrac{2k\pi}{n}$, pour $k\in\llbracket0,n-1\rrbracket$, sont deux à deux distincts et $\forall\:k\in\llbracket0,n-1\rrbracket$, $\dfrac{2k\pi}{n} \in\left[0,2\pi \right[$.
		    Or $\begin{array}{lll}
		     \left[0,2\pi \right[&\longrightarrow &\mathbb{C}\\
		     \theta&\longmapsto&\mathrm{e}^{\mathrm{i}\theta}
		     \end{array}$ est injective.
		    Donc, $\left\lbrace  \mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\:\text{avec}\: k\in{\llbracket 0,n-1\rrbracket}\right\rbrace$ est constitué de $n$ solutions distinctes de l’équation $z^n=1$.
		    Les solutions de l’équation $z^n=1$ étant également racines du polynôme $X^n-1$, il ne peut y en avoir d’autres.
		    Finalement, l’ensemble des solutions de l’équation $z^n=1$ est $S=\left\lbrace  \mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\:\text{avec}\: k\in{\llbracket 0,n-1\rrbracket}\right\rbrace$.

		3.  $z=i$ n’étant pas solution de l’équation $\left( z+\mathrm{i}\right)^{n}=\left(z-\mathrm{i}\right)^{n}$,
		    $\begin{array}{lll}
		         \left( z+\mathrm{i}\right)^{n}=\left(z-\mathrm{i}\right)^{n} &  \Longleftrightarrow & \left( \dfrac{z+\mathrm{i}}{z-\mathrm{i}}\right) ^n=1  \\\\
		         &  \Longleftrightarrow &\exists\: k \in{\llbracket 0,n-1\rrbracket} \:\text{tel que}\:\dfrac{z+\mathrm{i}}{z-\mathrm{i}}=\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}} \:\: \\\\
		         &\Longleftrightarrow&\exists\: k \in{\llbracket 0,n-1\rrbracket} \:\text{tel que}\: z\left( 1-\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\right)=-\mathrm{i}\left(1+\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\right) \\\\
		      \end{array}$
		    En remarquant que $z\left( 1-\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\right)=-\mathrm{i}\left(1+\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}\right)$ n’admet pas de solution pour $k=0$, on en déduit que:
		    $\begin{array}{lll}
		         \left( z+\mathrm{i}\right)^{n}=\left(z-\mathrm{i}\right)^{n} &  \Longleftrightarrow &
		        \exists\: k \in{\llbracket 1,n-1\rrbracket} \:\text{tel que}\:  z=\mathrm{i}\:\dfrac{\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}+1}{\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}-1}  
		      \end{array}$
		    En écrivant $\mathrm{i}\:\dfrac{\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}+1}{\mathrm{e}^{\frac{\mathrm{i}2k\pi}{n}}-1}=\mathrm{i}\:\dfrac{\mathrm{e}^{\dfrac{\mathrm{i}k\pi}{n}}+\mathrm{e}^{-\dfrac{\mathrm{i}k\pi}{n}}}{\mathrm{e}^{\dfrac{\mathrm{i}k\pi}{n}}-\mathrm{e}^{-\dfrac{\mathrm{i}k\pi}{n}}}$ $= \mathrm{i}\dfrac{2\cos\left(\dfrac{k\pi}{n}\right)}{2\mathrm{i}\sin\left(\dfrac{k\pi}{n}\right)}= \dfrac{\cos\left(\dfrac{k\pi}{n}\right)}{\sin\left(\dfrac{k\pi}{n}\right)}$ , on voit que les solutions sont des réels.
		    On pouvait aussi voir que si $z$ est solution de l’équation $\left( z+\mathrm{i}\right)^{n}=\left(z-\mathrm{i}\right)^{n}$ alors $|z+\mathrm{i}|=|z-\mathrm{i}|$ et donc le point d’affixe $z$ appartient à la médiatrice de $\left[A,B\right]$, $A$ et $B$ étant les points d’affixes respectives $\mathrm{i}$ et $-\mathrm{i}$, c’est-à-dire à la droite des réels.

!!! exercice "EXERCICE 85 algèbre "
	=== "Enoncé"  


		1.  Soient $n \in{\mathbb{N}^{*}}$, $P\in{\mathbb{R}_{n}\left[ X\right] }$ et $a \in{\mathbb{R}}$.

		    1.  Donner sans démonstration, en utilisant la formule de Taylor, la décomposition de $P(X)$ dans la base $\left( 1, X-a, \left( X-a\right) ^{2}, \cdots, (X-a)^{n}\right)$.

		    2.  Soit $r\in{\mathbb{N}^{*}}$. En déduire que :
		        $a$ est une racine de $P$ d’ordre de multiplicité $r$ si et seulement si $P^{(r)}(a)\neq 0$ et $\forall k \in{\llbracket 0,r-1 \rrbracket}$ , $P^{(k)}(a)=0$.

		2.  Déterminer deux réels $a$ et $b$ pour que $1$ soit racine double du polynôme $P=X^{5}+aX^{2}+bX$ et factoriser alors ce polynôme dans $\mathbb{R}\left[ X\right]$.


	=== "Corrigé"  


		1.  1.  $P(X)=\displaystyle\sum\limits_{k=0}^{n}\dfrac{P^{(k)}(a)}{k!}(X-a)^k$.

				2.  $$\begin{aligned}
				a\ \text{est une racine d'ordre }r\ \text{de }P &\Longleftrightarrow
				&\exists Q\in \mathbb{R}_{n-r}[X]\:\:\text{tel que}\: \:Q\left( a\right) \neq 0\ \text{et\
				\ }P=\left( X-a\right) ^{r}Q \\
				&\Longleftrightarrow &\exists \left( q_{0},\ldots ,q_{n-r}\right) \in
				\mathbb{R}^{n-r+1} \:\:\text{tel que}\:\: q_{0}\neq 0\ \text{et}\ P=\left( X-a\right)
				^{r}\displaystyle\sum\limits_{i=0}^{n-r}q_{i}\left( X-a\right) ^{i} \\
				&\Longleftrightarrow &\exists \left( q_{0},\ldots ,q_{n-r}\right) \in
				\mathbb{R}^{n-r+1}\:\text{tel que}\: \: q_{0}\neq 0\ \ \text{et\ \ }P=\displaystyle\sum%
				\limits_{i=0}^{n-r}q_{i}\left( X-a\right) ^{r+i} \\
				&\Longleftrightarrow &\exists \left( q_{0},\ldots ,q_{n-r}\right) \in
				\mathbb{R}^{n-r+1} \:\text{tel que}\: \: q_{0}\neq 0\ \ \ \text{et\ \ }P=\displaystyle\sum%
				\limits_{k=r}^{n}q_{k-r}\left( X-a\right) ^{k}\end{aligned}$$

				D’après la formule de Taylor (rappelée ci-dessus) et l’unicité de la décomposition de $P$ dans la base $\left( 1,\left( X-a\right)
				,\ldots ,\left( X-a\right) ^{n}\right)$ de $\mathbb{R}_{n}[X]$ il vient enfin :  

				$$
				a\ \text{est une racine d'ordre }r\ \text{de }P\Longleftrightarrow \forall
				k\in \left\{ 0,\ldots ,r-1\right\} \ \ P^{\left( k\right) }\left( a\right)
				=0\ \text{et\ \ }P^{\left( r\right) }\left( a\right) \neq 0\ $$

		2.  D’après la question précédente,

		$$
		\begin{aligned}
		1\ \text{est racine double de }P =X^{5}+aX^{2}+bX&\Longleftrightarrow&
		P\left( 1\right) =P^{\prime }\left( 1\right) =0\ \text{et }P^{\prime \prime
		}\left( 1\right) \neq 0 \\
		&\Longleftrightarrow &\left\{
		\begin{array}{c}
		1+a+b=0 \\
		5+2a+b=0 \\
		20+2a\neq 0%
		\end{array}%
		\right. \\
		&\Longleftrightarrow &a=-4\ \text{et }b=3\end{aligned}
		$$

		On obtient $X^{5}-4X^{2}+3X=X(X-1)^{2}(X^{2}+2X+3)$ et c’est la factorisation cherchée car le discriminant de $X^{2}+2X+3$ est strictement négatif.

!!! exercice "EXERCICE 86 algèbre "
	=== "Enoncé"  


		1.  Soit $(a,b,p)\in{\mathbb{Z}^{3}}$. Prouver que : si $p \wedge a = 1$ et $p\wedge b=1$, alors $p\wedge (ab)=1$.

		2.  Soit $p$ un nombre premier.

		    1.  Prouver que $\forall k \in{\llbracket 1,p-1\rrbracket}$, $p$ divise $\dbinom{p}{k}k!$ puis en déduire que $p$ divise $\dbinom{p}{k}$.

		    2.  Prouver que: $\forall n \in{\mathbb{N}}$, $n^{p}\equiv n \:\mod p$.
		        **Indication**: procéder par récurrence.

		    3.  En déduire, pour tout entier naturel $n$, que : $p \:\text{ne divise pas }\: n \Longrightarrow\: n^{p-1}\equiv 1 \mod p$.


	=== "Corrigé"  


		1.  On suppose $p\wedge a=1$ et $p\wedge b=1$.

		    D’après le théorème de Bézout,
		    $\exists (u_1,v_1)\in{\mathbb{Z}^2}$ tel que $u_1 p+v_1a=1$. (1)
		    $\exists (u_2,v_2)\in{\mathbb{Z}^2}$ tel que $u_2 p+v_2b=1$. (2)
		    En multipliant les équations (1) et (2), on obtient :
		    ${ \underbrace{{\color{black}(u_1u_2p+u_1v_2b+u_2v_1a)}}_{\displaystyle\in\mathbb Z}}p+{\underbrace{{\color{black}(v_1v_2)}}_{\displaystyle\in\mathbb Z}}(ab)=1$.
		    Donc, d’après le théorème de Bézout, $p\wedge (ab)=1$.

		2.  Soit $p$ un nombre premier.

		    1.  Soit $k\in{\llbracket 1,p-1\rrbracket}$. $\left( \begin{array}{c}
		        p\\
		        k
		        \end{array} \right)= \dfrac{{p!}}{{k!(p - k)!}} = \dfrac{{p(p - 1)...(p-k+1)}}{{k!}}$.
		        Donc $\dbinom{p}{k}k!=p(p-1)...(p-k+1)$.
		        donc $p\:|\dbinom{p}{k} k!$. (3)
		        Or, $\forall i \in \llbracket 1,k\rrbracket$, $p\wedge i=1$ (car $p$ est premier) donc, d’après 1., $p\wedge k!=1$.
		        Donc, d’après le lemme de Gauss, (3) $\Longrightarrow p\:|\dbinom{p}{k}$.

		    2.  Procédons par récurrence sur $n$.
		        Pour $n=0$ et pour $n=1$, la propriété est vérifiée.
		        Soit $n\in\mathbb N$.
		        Supposons que la propriété $(P_n)$: $n^{p}\equiv n \:\mod p$ soit vérifiée.
		        Alors, d’après la formule du binôme de Newton, $(n+1)^{p}={n^p} + \displaystyle\sum\limits_{k = 1}^{p - 1} {\dbinom{p}{k}} {n^k} + 1$. (4)
		        Or $\forall k \in{\llbracket 1,p-1\rrbracket}$, $p\:|\dbinom{p}{k}$ donc $p\:| \displaystyle\sum\limits_{k=1}^{p-1}{\dbinom{p}{k}}n^k$ .
		        Donc d’après (4) et $(P_n)$, $(n+1)^{p}\equiv n+1 \:\mod p$ et $(P_{n+1})$ est vraie.

		    3.  Soit $n \in{\mathbb{N}}$ tel que $p \:\text{ne divise pas }\: n$.
		        Comme $p$ est premier, alors $p\wedge n=1$.
		        La question précédente donne $p$ divise $n^p-n=n(n^{p-1}-1)$.
		        Or comme $p$ est premier avec $n$, on en déduit, d’après le lemme de Gauss, que $p$ divise $n^{p-1}-1$.
		        Ce qui signifie que $n^{p-1}\equiv 1 \mod p$. (petit théorème de Fermat).

!!! exercice "EXERCICE 87 algèbre  "
	=== "Enoncé"  


		Soient $a_{0},a_{1},\cdots ,a_{n}$ , $n+1$ réels deux à deux distincts.

		1.  Montrer que si $b_{0},b_{1},\cdots ,b_{n}\;$sont$\;n+1\;$ réels quelconques, alors il existe un unique polynôme $P$ vérifiant

		$$
		\deg P\leqslant n \:\:\text{et}\:\:\forall i\in \llbracket 0,n\rrbracket,\:\:P\left(
		    a_{i}\right) =b_{i}.
		$$

		2.  Soit $k\in \llbracket 0,n \rrbracket$.
		Expliciter ce polynôme $P$, que l’on notera $L_{k}$, lorsque :

		$$
		\forall i\in \llbracket 0 ,n \rrbracket,  \ \ b_{i}=
		\left\{
		\begin{array}{lll}
		0&\text{si}&i\neq k \\
		1&\text{si}&i=k
		\end{array}
		\right.
		$$

		3.  Prouver que $\forall p\in \llbracket 0 ,n \rrbracket$ , $\displaystyle\sum\limits_{k=0}^{n}a_{k}^{p}L_{k}=X^{p}$.


	=== "Corrigé"  


		1.  L’application $u$:$\begin{array} {lll}
		    \mathbb{R}_{n}[X]&\rightarrow &\mathbb{R}^{n+1}\\
		    P& \mapsto & \left( P\left( a_{0}\right) ,P\left( a_{1}\right) ,\cdots
		    ,P\left( a_{n}\right) \right)
		    \end{array}$ est linéaire.
		    Montrons que $\mathrm{Ker}
		    u=\left\{ 0\right\} .$

		    Si $P\in \mathrm{Ker} u$, alors $P\left( a_{0}\right) =P\left( a_{1}\right) =\cdots
		    =P\left( a_{n}\right) =0$ et le polynôme $P$, de degré inférieur ou égal à $n$, admet $n+1$ racines distinctes.
		    Donc $P=0$.
		    Ainsi $u$ est injective et comme $\dim \mathbb{R}_{n}[X]=n+1=\dim \mathbb{R}^{n+1}\ ,$ $u$ est un isomorphisme** **d’espaces vectoriels$\smallskip$.

		    Enfin les conditions recherchées sont équivalentes à : $P\in
		    \mathbb{R}_{n}[X]$ et $u\left( P\right) =\left( b_{0},\ldots ,b_{n}\right)
		    .\smallskip$

		    La bijectivité de $u$ dit que ce problème admet une unique solution $P$ et on a $P=u^{-1}\left(\left( b_{0},\ldots ,b_{n}\right)\right)$.

		2.  Pour ce choix de  $b_{0},b_{1},\cdots ,b_{n}$ le polynôme $L_{k}$ vérifie les conditions :

		    $$\deg L_{k}\leqslant n \:\:\text{et}\: \: \forall i\in \llbracket0,n\rrbracket, \:
		     L_k(a_i)=\left\lbrace
		    \begin{array}{lll}
		    0&\text{si}&i\neq k\\
		    1&\text{si}&i=k
		    \end{array}
		    \right.$$

		    Comme $a_{0},\ldots ,a_{k-1},a_{k+1},\ldots ,a_{n}$ sont $n$ racines distinctes de $L_{k}$ qui est de degré $\leqslant n$, il existe né cessairement $\lambda \in \mathbb{K}$ tel que $$L_{k}=\lambda \prod\limits_{\underset{i\neq k}{i=0}}^{n}\left( X-a_{i}\right)$$

		    La condition supplémentaire $L_{k}\left( a_{k}\right) =1$ donne $\lambda
		    =\dfrac{1}{\displaystyle\prod\limits_{\underset{i\neq k}{i=0}}^{n}\left( a_{k}-a_{i}\right) }$ et finalement : $$L_{k}=\prod\limits_{\underset{i\neq k}{i=0}}^{n}\frac{X-a_{i}}{a_{k}-a_{i}}$$

		3.  Soit $p\in \llbracket 0,n\rrbracket$.
		    Les polynômes $\displaystyle\sum
		    \limits_{k=0}^{n}a_{k}^{p}L_{k}$ et $X^{p}$ vérifient les mêmes conditions d’interpolation : $$\deg P\leqslant n\ \ \text{et}\ \ \ \forall i\in \{0,\cdots
		    ,n\}\;\;\;P\left( a_{i}\right) =a_{i}^{p}$$

		    Par l’unicité vue en première question, on a $\displaystyle\sum\limits_{k=0}^{n}a_{k}^{p}L_{k}=X^{p}$.

!!! exercice "EXERCICE 88 algèbre  "
	=== "Enoncé"  


		1.  Soit $E$ un $\mathbb{K}$-espace vectoriel ($\mathbb{K}=\mathbb{R}$ ou $\mathbb{C}$).
		    Soit $u\in\mathcal{L}(E)$. Soit $P\in\mathbb{K}[X]$.
		    Prouver que si $P$ annule $u$ alors toute valeur propre de $u$ est racine de $P$.

		2.  Soit $n\in\mathbb{N}$ tel que $n\geqslant 2$. On pose $E=\mathcal{M}_n\left( \mathbb{R}\right)$.
		    Soit $A=\left( a_{i,j}\right) _{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}$ la matrice de $E$ définie par $a_{i,j}=\left\lbrace \begin{array}{l}
		    0\:\text{si}\:i=j\\
		    1\:\text{si}\:i\neq j\\
		    \end{array}\right.$ .
		    Soit $u\in\mathcal{L}\left(E\right)$ défini par : $\forall\:M\in E$, $u(M)=M+\mathrm{tr} (M)A$.

		    1.  Prouver que le polynôme $X^2-2X+1$ est annulateur de $u$.

		    2.  $u$ est-il diagonalisable?
		        Justifier votre réponse en utilisant deux méthodes (l’une avec, l’autre sans l’aide de la question 1.).


	=== "Corrigé"  


		1.  Soit $u\in\mathcal{L}(E)$. Soit $P=\displaystyle\sum_{k=0}^{n}a_kX^k\in\mathbb{K}[X]$.
		    On suppose que $P$ annule $u$.
		    Soit $\lambda$ une valeur propre de $u$.
		    Prouvons que $P(\lambda)=0$.
		    $\lambda$ valeur propre de $u$ donc : $\exists\:x\in E\backslash\left\lbrace 0\right\rbrace\:/\:u(x)=\lambda x$.
		    On prouve alors par récurrence que: $\forall\:k\in\mathbb{N}$, $u^{k}(x)=\lambda^kx$.
		    Ainsi : $P(u)(x)=\displaystyle\sum_{k=0}^{n}a_ku^k(x)=\displaystyle\sum_{k=0}^{n}a_k\lambda^k x=P\left(\lambda \right)x$.
		    Or $P(u)=0$ donc $P(u)(x)=0$ donc $P(\lambda)x=0$.
		    Or $x\neq 0$ donc $P(\lambda)=0$.

		2.  Soit $n\in\mathbb{N}$ tel que $n\geqslant 2$. On pose $E=\mathcal{M}_n\left( \mathbb{R}\right)$.

		    Soit $A=\left( a_{i,j}\right) _{\substack{ 1\leqslant i\leqslant n  \\ 1\leqslant j\leqslant n}}$ la matrice de $E$ définie par $a_{i,j}=\left\lbrace \begin{array}{l}
		    0\:\text{si}\:i=j\\
		    1\:\text{si}\:i\neq j\\
		    \end{array}\right.$ .

		    1.  Posons $P=X^2-2X+1$.
		        Prouvons que $P$ est annulateur de $u$ c’est-à-dire que $P(u)=0$.
		        Soit $M\in E$.
		        $u^2(M)=u\circ u(M)=\left(M+\mathrm{tr}(M)A \right)+\mathrm{tr}\left(M+\mathrm{tr} (M)A\right)A$.
		        C’est-à-dire, par linéarité de la trace, $u^2(M)=M+\mathrm{tr}(M)A +\mathrm{tr}(M)A+\mathrm{tr}(M)\mathrm{tr}(A)A$.
		        Or $\mathrm{tr}(A)=0$ donc $u^2(M)=M+2\mathrm{tr}(M)A$.
		        Ainsi $u^2(M)-2u(M)+\mathrm{Id}(M)=M+2\mathrm{tr}(M)A-2M-2\mathrm{tr}(M)A+M=0$.
		        On a donc prouvé que $u^2-2u+\mathrm{Id}=0$.
		        C’est-à-dire $P$ est annulateur de $u$.

		    2.  Notons $\mathrm{I}_n$ la matrice unité de $E$.
		        **Première méthode**:
		        Notons $\mathrm{Spec}(u)$ le spectre de $u$.
		        $P=(X-1)^2$ et $P$ est annulateur de $u$.
		        Donc d’après 1., $\mathrm{Spec}(u)\subset \left\lbrace 1\right\rbrace$.
		        De plus $A\neq 0$ et $u(A)=A$ donc $\mathrm{Spec}(u)= \left\lbrace 1\right\rbrace$.
		        Ainsi, si $u$ était diagonalisable alors on aurait $E=\mathrm{Ker}\left(u-\mathrm{Id} \right)$.
		        C’est-à-dire, on aurait $u=\mathrm{Id}$.
		        Or $u(\mathrm{I_{n}})\neq \mathrm{I_{n}}$ (puisque $\mathrm{tr}(\mathrm{I_{n}})\neq 0$) donc $u\neq\mathrm{Id}$.
		        On obtient donc une contradiction.
		        On en déduit que $u$ n’est pas diagonalisable.
		        **Deuxième méthode** :
		        Notons $P_m$ le polynôme minimal de $u$.
		        $P=(X-1)^2$ est un polynôme annulateur de $u$ donc $P_m|P$.
		        Si $u$ était diagonalisable alors $P_m$ serait scindé à racines simples.
		        On aurait donc $P_m=X-1$.
		        Ce qui impliquerait que $u=\mathrm{Id}$ car $P_m$ est également un polynôme annulateur de $u$.
		        Or $u(\mathrm{I_{n}})\neq \mathrm{I_{n}}$ (puisque $\mathrm{tr}(\mathrm{I_{n}})\neq 0$) donc $u\neq\mathrm{Id}$.
		        On obtient donc une contradiction.
		        On en déduit que $u$ n’est pas diagonalisable.

!!! exercice "EXERCICE 89 algèbre  "
	=== "Enoncé"  


		Soit $n \in {\mathbb{N}}$ tel que $n\geqslant 2$. On pose $z = e^{\mathrm{i}\,\frac{2\pi}{n}}$.

		1.  On suppose $k\in {\llbracket 1,n-1\rrbracket}$.
		    Déterminer le module et un argument du complexe $\:z^k - 1$.

		2.  On pose $\;S = \displaystyle\sum\limits_{k=0}^{n-1} \;\left|z^k - 1\right|$. Montrer que $\:S = \dfrac{2}{\tan\frac{\pi}{2n}}$.


	=== "Corrigé"  


		1.  $z^k-1 = e^{\mathrm{i}\,\dfrac{k2\pi}{n}} - 1
		       = \text{e}^{\mathrm{i}\,\dfrac{k\pi}{n}}\left(\text{e}^{\mathrm{i}\,\dfrac{k\pi}{n}} - \text{e}^{-\mathrm{i}\dfrac{k\pi}{n}}\right)
		       = \text{e}^{\mathrm{i}\dfrac{k\pi}{n}}2\mathrm{i} \sin\left(\frac{k\pi}{n}\right)$
		    c’est-à-dire $z^k-1 = 2 \sin\left(\dfrac{k\pi}{n}\right)\;\text{e}^{\mathrm{i}(\dfrac{k\pi}{n}+ \dfrac{\pi}{2})}$

		    Pour $k\in{\llbracket 1,n-1 \rrbracket}$, on a $0 < \frac{k\,\pi}{n} < \pi$, donc $\sin\left(\dfrac{k\,\pi}{n}\right) > 0$.
		    Donc le module de $z^k - 1$ est $\,2\: \sin\left(\dfrac{k\,\pi}{n}\right)$ et un argument de $\,z^k - 1$ est $\,\dfrac{k\pi}{n}+ \dfrac{\pi}{2}$.

		2.  On remarque que pour $k=0$, $|z^k-1|=0$ et $\sin\left(\dfrac{k\,\pi}{n}\right)=0$.
		    Donc d’après la question précédente, on a $S = 2\:\displaystyle\sum\limits_{k=0}^{n-1} \;\sin\left(\dfrac{k\,\pi}{n}\right).$
		    $S$ est donc la partie imaginaire de $\:T = 2 \displaystyle\sum\limits_{k=0}^{n-1} e^{\mathrm{i}\,\dfrac{k\pi}{n}}$.
		    Or, comme $\mathrm{e}^{\mathrm{i}\dfrac{\pi}{n}}\neq 1$, on a $T   =  2\dfrac{1 - \text{e}^{\mathrm{i}\pi}}{1 -
		    e^{\mathrm{i}\dfrac{\pi}{n}}}=\dfrac{4}{1 -
		    e^{\mathrm{i}\dfrac{\pi}{n}}}$.
		    Or $1-\mathrm{e} ^{\mathrm{i}\dfrac{\pi}{n}}
		    =\mathrm{e}^{\mathrm{i}\dfrac{\pi}{2n}}\left( \mathrm{e}^{-\mathrm{i}\dfrac{\pi}{2n}}-\mathrm{e}^{\mathrm{i}\dfrac{\pi}{2n}}\right)
		    =-2\mathrm{i}\mathrm{e}^{\mathrm{i}\dfrac{\pi}{2n}}\sin\left( \dfrac{\pi}{2n}\right)$.
		    On en déduit que $T=   \dfrac{4\text{e}^{-\mathrm{i}\dfrac{\pi}{2n}}}{-2\mathrm{i}\,\sin \dfrac{\pi}{2n}}
		      =   \dfrac{2}{\sin \dfrac{\pi}{2n}}\;\mathrm{i}\;\text{e}^{-\mathrm{i}\dfrac{\pi}{2n}}$.
		    En isolant la partie imaginaire de $T$, et comme $\cos\left( \dfrac{\pi}{2n}\right)\neq 0$ ($n\geqslant 2$), on en déduit que $S = \dfrac{2}{\tan\dfrac{\pi}{2n}}$.

!!! exercice "EXERCICE 90 algèbre  "
	=== "Enoncé"  


		$\mathbb K$ désigne le corps des réels ou celui des complexes.
		Soient $a_{1}, a_{2}, a_{3}$ trois scalaires distincts donnés de $\mathbb K$.

		1.  Montrer que $\Phi:\begin{array}{ccc}
		    \mathbb K_{2}[X] & \longrightarrow & \mathbb K^{3}\\
		    P & \longmapsto & \big(P(a_{1}),P(a_{2}),P(a_{3})\big)\\
		    \end{array}$ est un isomorphisme d’espaces vectoriels.

		2.  On note $(e_{1}, e_{2}, e_{3})$ la base canonique de $\mathbb K^{3}$ et on pose $\forall k\in\{1,2,3\}, \:L_{k}=\Phi^{-1}(e_{k})$.

		    1.  Justifier que $(L_{1}, L_{2}, L_{3})$ est une base de $\mathbb K_{2}[X]$.

		    2.  Exprimer les polynômes $L_{1}, L_{2}$ et $L_{3}$ en fonction de $a_{1}, a_{2}$ et $a_{3}$.

		3.  Soit $P\in\mathbb K_{2}[X]$. Déterminer les coordonnées de $P$ dans la base $(L_{1}, L_{2}, L_{3})$.

		4.  **Application** : on se place dans $\mathbb R^{2}$ muni d’un repère orthonormé et on considère les trois points $A(0,1), B(1,3), C(2,1)$.

		    Déterminer une fonction polynomiale de degré 2 dont la courbe passe par les points $A$, $B$ et $C$.


	=== "Corrigé"  


		1.  Par linéarité de l’évaluation $P\mapsto P(a)$ (où $a$ est un scalaire fixé), $\Phi$ est linéaire.

		    Soit $P\in\mathbb K_{2}[X]$ tel que $\Phi(P)=0$.
		    Alors $P(a_1)=P(a_2)=P(a_3)=0$, donc $P$ admet trois racines distinctes.
		    Or $P$ est de degré inférieur ou égal à 2; donc $P$ est nul.
		    Ainsi, $\mathrm{Ker}(\Phi)=\{0\}$ i.e. $\Phi$ est injective.

		    Enfin, $\dim\big(\mathbb K_{2}[X]\big)=\dim\big(\mathbb K^{3}\big)=3$ donc $\Phi$ est bijective.

		    Par conséquent, $\Phi$ est un isomorphisme d’espaces vectoriels de $\mathbb{K}_2\left[ X\right]$ dans $\mathbb{K} ^3$.

		2.  1.  $\Phi$ est un isomorphisme donc l’image réciproque d’une base est une base.

		        Ainsi, $(L_{1}, L_{2}, L_{3})$ est une base de $\mathbb K_{2}[X]$.

		    2.  $L_{1}\in\mathbb R_{2}[X]$ et vérifie $\Phi(L_{1})=(1,0,0)$ i.e. $\big(L_{1}(a_{1}),L_{1}(a_{2}),L_{1}(a_{3})\big)=(1,0,0)$.

		        Donc, comme $a_2$ et $a_3$ sont distincts, $(X-a_{2})(X-a_{3})\,|L_{1}$.
		        Or $\deg L_1\leqslant 2$, donc $\exists k\in{\mathbb{K}}$ tel que $L_{1}=k(X-a_{2})(X-a_{3})$.
		        La valeur $L_{1}(a_{1})=1$ donne $k=\dfrac{1}{(a_{1}-a_{2})(a_{1}-a_{3})}$.
		        Donc $L_{1}=\dfrac{(X-a_{2})(X-a_{3})}{(a_{1}-a_{2})(a_{1}-a_{3})}$.

		        Un raisonnement analogue donne $L_{2}=\dfrac{(X-a_{1})(X-a_{3})}{(a_{2}-a_{1})(a_{2}-a_{3})}$ et $L_{3}=\dfrac{(X-a_{1})(X-a_{2})}{(a_{3}-a_{1})(a_{3}-a_{2})}$.

		3.  $(L_1,L_2,L_3)$ base de $\mathbb{K}_2[X]$ donc $\exists(\lambda_1,\lambda_2,\lambda_3)\in{\mathbb{K}^3}$ tel que $P=\lambda_{1}L_{1}+\lambda_{2}L_{2}+\lambda_{3}L_{3}$.
		    Par construction, $\forall(i,j)\in\{1,2,3\}^{2}, L_{i}(a_{j})=\delta_{ij}$ donc $P(a_{j})=\lambda_{j}$.
		    Ainsi, $P=P(a_{1})L_{1}+P(a_{2})L_{2}+P(a_{3})L_{3}$.

		4.  On pose $a_{1}=0$, $a_{2}=1$ et $a_{3}=2$. Ces trois réels sont bien distincts.
		    On cherche $P\in\mathbb R_{2}[X]$ tel que $\big(P(a_{1}),P(a_{2}),P(a_{3})\big)=(1,3,1)$.

		    Par bijectivité de $\Phi$ et d’après 3. , l’unique solution est le polynôme $P=1.L_{1}+3.L_{2}+1.L_{3}$.
		    On a $L_{1}=\dfrac{(X-1)(X-2)}{2}$, $L_{2}=\dfrac{X(X-2)}{-1}$ et $L_{3}=\dfrac{X(X-1)}{2}$.
		    Donc $P=-2X^{2}+4X+1$.

!!! exercice "EXERCICE 91 algèbre "
	=== "Enoncé"  


		On considère la matrice $A=\begin{pmatrix}
		0& 2 & -1 \\
		-1& 3 &-1 \\
		-1& 2 & 0
		\end{pmatrix} \in\mathcal{M}_3(\mathbb{R}).$

		1.  Montrer que $A$ n’admet qu’une seule valeur propre que l’on déterminera.

		2.  La matrice $A$ est-elle inversible ? Est-elle diagonalisable ?

		3.  Déterminer, en justifiant, le polynôme minimal de $A$.

		4.  Soit $n\in\mathbb{N}$. Déterminer le reste de la division euclidienne de $X^n$ par $(X-1)^2$ et en déduire la valeur de $A^n$.


	=== "Corrigé"  


		1.  Déterminons le polynôme caractéristique $\chi_A$ de $A$ :

		$$
		\begin{aligned}
		\chi_A(X)&=&\left|\begin{array}{ccc}
		X & -2 & 1\\
		1&X-3&1\\
		1&-2&X
		\end{array}\right|\underset{
		C_1\leftarrow \displaystyle\sum\limits_{i=1}^3 C_i}{=} \left|\begin{array}{ccc}
		X-1 & -2 & 1\\
		X-1&X-3&1\\
		X-1&-2&X
		\end{array}\right|\\
		&=&(X-1)\left|\begin{array}{ccc}
		1 & -2 & 1\\
		1&X-3&1\\
		1&-2&X
		\end{array}\right|\underset{
		\begin{array}{c}
		L_2\leftarrow L_2-L_1\\
		L_3\leftarrow L_3-L_1
		\end{array}}{=}(X-1)\left|\begin{array}{ccc}
		1 & -2 & 1\\
		0&X-1&0\\
		0&0&X-1
		\end{array}\right|\\
		&=&(X-1)^3\end{aligned}
		$$

		Donc $\chi_A(X)=(X-1)^3$.

		Donc $A$ admet $1$ comme unique valeur propre.

		2.  Puisque $0$ n’est pas valeur propre de $A$, $A$ est inversible.
		    Si $A$ était diagonalisable elle serait semblable à la matrice identité et donc égale à la matrice identité. Puisque ce n’est pas le cas, $A$ n’est pas diagonalisable.

		3.  Notons $P_m$ le polynôme minimal de $A$.
		    $P_m$ divise $\chi_A$ et $P_m$ est un polynôme annulateur de $A$.
		    $A-\mathrm{I}_3\neq 0$ et $(A-\mathrm{I}_3)^2=\begin{pmatrix}
		    -1 & 2 & -1 \\
		    -1 & 2 & -1 \\
		    -1 & 2 & -1
		    \end{pmatrix} \begin{pmatrix}
		    -1 & 2 & -1 \\
		    -1 & 2 & -1 \\
		    -1 & 2 & -1
		    \end{pmatrix}=\begin{pmatrix}
		    0 & 0 & 0 \\
		    0 & 0 & 0 \\
		    0 & 0 & 0
		    \end{pmatrix}$.
		    Donc $P_m=(X-1)^2$.

		4.  Soit $n\in\mathbb{N}$. Par division euclidienne de $X^n$ par $(X-1)^2$, $\exists\:! (Q,R)\in\mathbb{R}[X]\times \mathbb{R}_1[X],\,X^n=(X-1)^2Q+R$$(1)$
		    Or, $\exists (a,b)\in \mathbb{R}^2,\, R=aX+b$ donc $X^n=(X-1)^2Q+aX+b$.
		    Puisque $1$ est racine double de $(X-1)^2$ on obtient : $1=a+b$ et, après dérivation, $n=a$.
		    Donc $R=nX+1-n$.$(2)$
		    $P_m=(X-1)^2$ étant un polynôme annulateur de $A$ on a d’après $(1)$ et $(2)$ : $$\forall n\in\mathbb{N},\,A^n=nA+(1-n)\mathrm{I}_3$$

!!! exercice "EXERCICE 92 algèbre  "
	=== "Enoncé"  


		Soit $n\in\mathbb{N}^*$. On considère $E=\mathcal{M}_n(\mathbb{R})$ l’espace vectoriel des matrices carrées d’ordre $n$.
		On pose : $\forall(A,B)\in E^2$, $\left\langle A\:,B \right\rangle=\mathrm{tr}({}^t\! AB)$ où tr désigne la trace et ${}^t\! A$ désigne la transposée de la matrice $A$.

		1.  Prouver que $\left\langle\:,\right\rangle$ est un produit scalaire sur $E$.

		2.  On note $S_n(\mathbb{R})$ l’ensemble des matrices symétriques de $E$.
		    Une matrice $A$ de $E$ est dite antisymétrique lorsque ${}^t\!A=-A$.
		    On note $A_n(\mathbb{R})$ l’ensemble des matrices antisymétriques de $E$.
		    On admet que $S_n(\mathbb{R})$ et $A_n(\mathbb{R})$ sont des sous-espaces vectoriels de $E$.

		    1.  Prouver que $E=S_n(\mathbb{R})\oplus A_n(\mathbb{R})$.

		    2.  Prouver que $A_n(\mathbb{R})^\perp=S_n(\mathbb{R})$.

		3.  Soit $F$ l’ensemble des matrices diagonales de $E$.
		    Déterminer $F^\perp$.


	=== "Corrigé"  


		1.  $\left\langle\:,\right\rangle$ est linéaire par rapport à sa première variable par linéarité de la trace, de la transposition et par distributivité de la multiplication par rapport à l’addition dans $E$.
		    De plus, une matrice et sa transposée ayant la même trace, on a :
		    $\forall(A,B)\in E^2$,$\left\langle A\:,B\right\rangle=
		    \mathrm{tr}({}^t\! AB)=\mathrm{tr}({}^t\!\left( {}^t\! AB\right) )
		    =\mathrm{tr}({}^t\! BA)=\left\langle B\:,A\right\rangle$.
		    Donc $\left\langle\:,\right\rangle$ est symétrique.
		    On en déduit que $\left\langle\:,\right\rangle$ est bilinéaire et symétrique.(1)
		    Soit $A=\left( A_{i,j}\right)_{ _{1\leqslant i,j \leqslant n}}\in E$.
		    $\left\langle A\:,A\right\rangle
		    =\mathrm{tr}({}^t\! AA)=\displaystyle\displaystyle\sum\limits_{i=1}^{n}({}^t\!AA)_{i,i}
		     =\displaystyle\displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{k=1}^{n}({}^t\!A)_{i,k}A_{k,i}
		     =\displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{k=1}^{n}A_{k,i}^2$ donc $\left\langle A\:,A\right\rangle\geqslant 0$.

		    Donc $\left\langle\:,\right\rangle$ est positive. (2)
		    Soit $A=\left( A_{i,j}\right)_{ _{1\leqslant i,j \leqslant n}}\in E$ telle que $\left\langle A\:,A\right\rangle =0$.
		    Alors $\displaystyle\sum\limits_{i=1}^{n}\sum\limits_{k=1}^{n}A_{k,i}^2=0$. Or, $\forall i\in \llbracket 1,n \rrbracket$, $\forall k\in \llbracket 1,n \rrbracket$, $A_{k,i}^2\geqslant 0$.
		    Donc $\forall i\in \llbracket 1,n \rrbracket$, $\forall k\in \llbracket 1,n \rrbracket$, $A_{k,i}= 0$. Donc $A=0$.
		    Donc $\left\langle\:,\right\rangle$ est définie.(3)
		    D’après (1),(2) et (3), $\left\langle\:,\right\rangle$ est un produit scalaire sur $E$.

		    **Remarque importante**: Soit $(A,B)\in E^2$.
		    On pose $A=\left( A_{i,j}\right)_{ _{1\leqslant i,j \leqslant n}}$ et $B=\left( B_{i,j}\right)_{ _{1\leqslant i,j \leqslant n}}$.
		    Alors $\left\langle A\:,B\right\rangle=
		    \mathrm{tr}({}^t\! AB)
		    =\displaystyle\sum\limits_{i=1}^{n}({}^t\! AB)_{i,i}
		    =\displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{k=1}^{n}\left({}^t\!A\right)_{i,k}B_{k,i}
		    =\displaystyle\sum\limits_{i=1}^{n}\displaystyle\sum\limits_{k=1}^{n}A_{k,i}B_{k,i}$ .
		    Donc $\left\langle\:,\right\rangle$ est le produit scalaire canonique sur $E$.

		2.  1.  Soit $M\in S_n(\mathbb{R}) \cap A_n(\mathbb{R})$.
		        alors ${}^t\!M=M$ et ${}^t\!M=-M$ donc $M=-M$ et $M=0$.
		        Donc $S_n(\mathbb{R}) \cap A_n(\mathbb{R})=\{0\}$.(1)
		        Soit $M\in E$.
		        Posons $S=\dfrac{M+{}^t\!M}{2}$ et $A=\dfrac{M-{}^t\!M}{2}$.
		        On a $M=S+A$.
		        ${}^t\:S={}^t\!\left( \dfrac{M+{}^t\!M}{2}\right)
		         =\dfrac{1}{2}\left({}^t\!M+{}^t({}^t\!M) \right)
		          =\dfrac{1}{2}\left({}^t\!M +M\right)=S$, donc $S\in S_n(\mathbb{R})$.
		        ${}^t\:A={}^t\!\left( \dfrac{M-{}^t\!M}{2}\right)
		         =\dfrac{1}{2}\left({}^t\!M-{}^t({}^t\!M) \right)
		          =\dfrac{1}{2}\left({}^t\!M -M\right)=-A$, donc $A\in A_n(\mathbb{R})$.
		        On en déduit que $E=S_n(\mathbb{R}) + A_n(\mathbb{R})$.(2)
		        D’après (1) et (2), $E=S_n(\mathbb{R}) \oplus A_n(\mathbb{R}).$

		        **Remarque**: on pouvait également procéder par analyse et synthèse pour prouver que $E=S_n(\mathbb{R}) \oplus A_n(\mathbb{R}).$

		    2.  Prouvons que $S_n(\mathbb{R})\subset A_n(\mathbb{R})^\perp$.
		        Soit $S\in S_n(\mathbb{R})$.
		        Prouvons que $\forall \:A\in A_n(\mathbb{R})$, $\left\langle S\:,A\right\rangle=0$.
		        Soit $A\in A_n(\mathbb{R})$.
		        $\left\langle S\:,A\right\rangle=
		        \mathrm{tr}({}^t\! SA)
		        =\mathrm{tr}(SA)
		        =\mathrm{tr}(AS)
		        =\mathrm{tr}(-{}^t\! AS)
		        =-\mathrm{tr}({}^t\! AS)
		        =-\left\langle A\:,S\right\rangle
		        =-\left\langle S\:,A\right\rangle$.
		        Donc $2\left\langle S\:,A\right\rangle=0$ soit $\left\langle S\:,A\right\rangle=0$.
		        On en déduit que $S_n(\mathbb{R})\subset A_n(\mathbb{R})^\perp$ (1)
		        De plus, $\text{dim} \:A_n(\mathbb{R})^\perp=n^2-\text{dim}\:A_n(\mathbb{R})$.
		        Or, d’après 2.(a), $E=S_n(\mathbb{R}) \oplus A_n(\mathbb{R})$ donc $\text{dim}\:S_n(\mathbb{R})=n^2 -\text{dim}\:A_n(\mathbb{R})$.
		        On en déduit que $\text{dim} \:S_n(\mathbb{R})=\text{dim}\:A_n(\mathbb{R})^\perp$.(2)
		        D’après (1) et (2), $S_n(\mathbb{R})=A_n(\mathbb{R})^\perp$.

		3.  On introduit la base canonique de $\mathcal{M}_n(\mathbb{R})$ en posant:
		    $\forall i\in\llbracket 1,n \rrbracket$, $\forall j\in\llbracket 1,n \rrbracket$, $E_{i,j}=\left( e_{k,l}\right)_{ _{1\leqslant k,l \leqslant n}}$ avec $e_{k,l}=\left\lbrace \begin{array}{ll}
		    1&\:\text{si}\: k=i\:\text{et} \:l=j\\
		    0&\:\text{sinon}
		    \end{array}
		    \right.$
		    On a alors $F=\text{Vect}\left( E_{1,1},E_{2,2},...,E_{n,n}\right)$.
		    Soit $M=\left( m_{i,j}\right)_{ _{1\leqslant i,j \leqslant n}}\in E$.
		    Alors, en utilisant la remarque importante de la question 1.,
		    $M\in F^{\perp} \Longleftrightarrow \forall i \in \llbracket 1,n\rrbracket,\: \left\langle M\:,E_{i,i}\right\rangle=0
		    \Longleftrightarrow \forall i \in \llbracket 1,n\rrbracket,\: m_{i,i}=0$.
		    Donc $F^\perp=\text{Vect}\left( E_{i,j}\:\text{telles que}\:(i,j)\in {\llbracket 1,n\rrbracket}^2\:\text{et}\:i\neq j\right)$.
		    En d’autres termes, $F^\perp$ est l’ensemble des matrices comprenant des zéros sur la diagonale.

!!! exercice "EXERCICE 93 algèbre  "
	=== "Enoncé"  


		Soit $E$ un espace vectoriel réel de dimension finie $n>0$ et $u\in{\mathcal{ L}(E)}$ tel que $u^3+u^2+u=0.$
		On notera $\mathrm{Id}$ l’application identité sur $E$.

		1.  Montrer que $\mathrm{Im }u \oplus \mathrm{Ker} u = E.$

		2.  1.  Énoncer le lemme des noyaux pour deux polynômes.

		    2.  En déduire que $\mathrm{Im }u =\mathrm{Ker} (u^2+u+\mathrm{Id})$.

		3.  On suppose que $u$ est non bijectif.
		    Déterminer les valeurs propres de $u$. Justifier la réponse.

		**Remarque**: les questions 1. , 2. et 3. peuvent être traitées indépendamment les unes des autres.


	=== "Corrigé"  


		1.  On a $u^3+u^2+u=0$ (\*)
		    Soit $y\in \mathrm{Im }u\cap\mathrm{Ker} u$.
		    Alors $\exists x\in E$ tel que $y=u(x)$ et $u(y)=0$.
		    Donc, d’après (\*), $0=u^3(x)+u^2(x)+u(x)=\underbrace{u^2(y)}_{=0}+\underbrace{u(y)}_{=0}+y=0$ .
		    Donc $y=0$.
		    Donc $\mathrm{Ker} u \cap\mathrm{Im } u=\left\lbrace0 \right\rbrace$. (1)
		    De plus, $E$ étant de dimension finie, d’après le théorème du rang, $\mathrm{dim}\,E=\mathrm{dim}\,\mathrm{Ker} u+\mathrm{dim}\,\mathrm{Im }u$.(2)
		    Donc, d’après (1) et (2), $E=\mathrm{Ker} u \oplus \mathrm{Im }u$.

		2.  1.  Lemme des noyaux pour deux polynômes:
		        Si $A$ et $B$ sont deux polynômes premiers entre eux, alors $\mathrm{Ker} (AB)(u)=\mathrm{Ker} A(u)\oplus \mathrm{Ker} B(u).$

		    2.  On pose $P=X^3+X^2+X$. $P$ est un polynôme annulateur de $u$ donc $\mathrm{Ker} P(u)=E$.
		        $P=X(X^2+X+1)$. De plus, $X$ et $X^2+X+1$ sont premiers entre eux.
		        Donc, d’après le lemme des noyaux, $E=\mathrm{Ker} u \oplus \mathrm{Ker} (u^2+u+\mathrm{ Id})$.

		        On en déduit que $\dim \mathrm{Ker} (u^2+u+{\mathrm{Id}})=\mathrm{dim}\,E-\mathrm{dim}\,\mathrm{Ker} u= \mathrm{dim}\,\mathrm{Im }u$.(3)
		        Prouvons que $\mathrm{Im }u\subset \mathrm{Ker} (u^2+u+\mathrm{Id})$.
		        Soit $y\in \mathrm{Im }u$.
		        alors $\exists x\in E$ tel que $y=u(x)$.
		        $(u^2+u+\mathrm {Id})(y)=(u^3+u^2+u)(x)=0$ d’après (\*).
		        Donc $y\in \mathrm{Ker} ( u^2+u+\mathrm{Id})$.
		        On a donc prouvé que $\mathrm{Im }u\subset \mathrm{Ker} (u^2+u+\mathrm{Id})$.(4)
		        Donc, d’après (3) et (4), $\mathrm{Im }u = \mathrm{Ker} (u^2+u+\mathrm{Id})$.

		3.  $P=X^3+X^2+X=X(X^2+X+1)$ est un polynôme annulateur de $u$.
		    Donc si on note $\mathrm{sp}(u)$ l’ensemble des valeurs propres de $u$ alors $\mathrm{sp}(u)\subset \left\lbrace {\text{racines réelles de }\:P}\right\rbrace$.
		    Or $\left\lbrace {\text{racines réelles de }\:P}\right\rbrace =\left\lbrace 0\right\rbrace$ donc $\mathrm{sp}(u)\subset\left\lbrace 0\right\rbrace$.(5)
		    Or $u$ est non bijectif donc, comme $u$ est un endomorphisme d’un espace vectoriel de dimension finie, $u$ est non injectif.
		    Donc $\mathrm{Ker} u\neq \left\lbrace 0\right\rbrace$, donc 0 est valeur propre de $u$.(6)
		    On en déduit, d’après (5) et (6), que $\mathrm{sp}(u)=\left\lbrace 0\right\rbrace$ .

!!! exercice "EXERCICE 94 algèbre  "
	=== "Enoncé"  


		1.  Énoncer le théorème de Bézout dans $\mathbb{Z}$.

		2.  Soit $a$ et $b$ deux entiers naturels premiers entre eux.
		    Soit $c\in \mathbb{N}$.
		    Prouver que: $\left( a|c\:\text{et} \:b|c\right)\Longleftrightarrow ab|c$.

		3.  On considère le système $(S)$: $\left\{\begin{array}{lccl}
		     x&\equiv&6& [17]\\
		     x&\equiv&4&[15]
		     \end{array}\right.$ dans lequel l’inconnue $x$ appartient à $\mathbb{Z}$.

		    1.  Déterminer une solution particulière $x_0$ de $(S)$ dans $\mathbb{Z}$.

		    2.  *Déduire des questions précédentes* la résolution dans $\mathbb{Z}$ du système $(S)$.


	=== "Corrigé"  


		1.  Théorème de Bézout:
		    Soit $(a,b)\in\mathbb{Z}^2$.
		    $a\wedge b=1\Longleftrightarrow \exists (u,v)\in \mathbb{Z}^2\:/\:au+bv=1$.

		2.  Soit $(a,b)\in\mathbb{N}^2$. On suppose que $a\wedge b=1$.
		    Soit $c\in \mathbb{N}$.
		    Prouvons que $ab|c\Longrightarrow a|c\: \text{et}\:b|c$.
		    Si $ab|c$ alors $\exists k\in\mathbb{Z}\:/\:c=kab$.
		    Alors, $c=(kb)a$ donc $a|c$ et $c=(ka)b$ donc $b|c$.
		    Prouvons que $\left( a|c\:\text{et} \:b|c\right)\Longrightarrow ab|c$.
		    $a\wedge b=1$ donc $\exists (u,v)\in \mathbb{Z}^2\:/\:au+bv=1$.(1)
		    De plus $a|c$ donc $\exists k_1\in\mathbb{Z}\:/\:c=k_1a$.(2)
		    De même, $b|c$ donc $\exists k_2\in\mathbb{Z}\:/\:c=k_2b$.(3)
		    On multiplie (1) par $c$ et on obtient $cau+cbv=c$.
		    Alors, d’après (2) et (3), $(k_2b)au+(k_1a)bv=c$, donc $(k_2u+k_1v)(ab)=c$ et donc $ab|c$.
		    On a donc prouvé que $\left( a|c\:\text{et} \:b|c\right)\Longleftrightarrow ab|c$.

		3.  1.  **Première méthode** (méthode générale):
		        Soit $x \in\mathbb{Z}$.
		        $\begin{array}{lll}
		        x\: \text{solution  de} (S) &\Longleftrightarrow &\exists(k,k')\in\mathbb{Z}^2\:\text{tel que}\:\left\lbrace \begin{array}{l} x=6+17k\\ x=4+15k'\end{array}\right.\\
		        &\Longleftrightarrow &\exists(k,k')\in\mathbb{Z}^2\:\text{tel que}\:\left\lbrace \begin{array}{l} x=6+17k\\ 6+17k= 4+15k' \end{array}\right.\\
		        \end{array}$
		        Or $6+17k=4+15k' \Longleftrightarrow 15k'-17k=2$.
		        Pour déterminer une solution particulière $x_0$ de $(S)$, il suffit donc de trouver une solution particulière $(k_0,k'_0)$ de l’équation $15k'-17k=2$.
		        Pour cela, cherchons d’abord, une solution de l’équation $15u+17v=1$.
		        17 et 15 sont premiers entre eux.
		        Déterminons alors un couple $(u_0,v_0)$ d’entiers relatifs tel que $15u_0+17v_0=1$.
		        On a : $17=15\times 1+2$ puis $15=7\times 2+1$.
		        Alors $1=15-7\times 2=15-7\times(17-15\times 1)=15-17\times7+15\times 7=15\times 8-17\times 7$
		        Donc $8\times 15 +(-7)\times 17=1$
		        Ainsi, $16\times 15+(-14)\times 17=2$.
		        On peut prendre alors $k'_0=16$ et $k_0=14$.
		        Ainsi, $x_0=6+17\times k_0=6+17\times 14=244$ est une solution particulière de $(S)$.

		        **Deuxième méthode:**
		        En observant le système $(S)$, on peut remarquer que $x_0=-11$ est une solution particulière.
		        Cette méthode est évidemment plus rapide mais ne fonctionne pas toujours.

		    2.  $x_0$ solution particulière de $(S)$ donc $\left\{\begin{array}{lccl}
		         x_0&=&6& [17]\\
		         x_0&=&4&[15]
		         \end{array}\right.$.
		        On en déduit que $x$ solution de $(S)$ si et seulement si $\left\{\begin{array}{lccl}
		         x-x_0&=&0& [17]\\
		         x-x_0&=&0&[15]
		         \end{array}\right.$
		        c’est-à-dire $x$ solution de $(S)$ $\Longleftrightarrow$ $\left( 17|x-x_0 \:\text{et}\: 15|x-x_0\right)$.
		        Or $17\wedge 15=1$ donc d’après 2., $x$ solution de (S) $\Longleftrightarrow (17\times 15) |x-x_0$.
		        Donc l’ensemble des solutions de $(S)$ est $\left\lbrace x_0+17\times 15k,\:k\in \mathbb{Z}\right\rbrace =\left\lbrace 244+255k,\:k\in\mathbb{Z}\right\rbrace$.


!!! exercice "EXERCICE 95 probabilités "
	=== "Enoncé"  


		Une urne contient deux boules blanches et huit boules noires.

		1.  Un joueur tire successivement, avec remise, cinq boules dans cette urne.
		    Pour chaque boule blanche tirée, il gagne 2 points et pour chaque boule noire tirée, il perd 3 points.
		    On note $X$ la variable aléatoire représentant le nombre de boules blanches tirées.
		    On note $Y$ le nombre de points obtenus par le joueur sur une partie.

		    1.  Déterminer la loi de $X$, son espérance et sa variance.

		    2.  Déterminer la loi de $Y$, son espérance et sa variance.

		2.  Dans cette question, on suppose que les cinq tirages successifs se font sans remise.

		    1.  Déterminer la loi de $X$.

		    2.  Déterminer la loi de $Y$.


	=== "Corrigé"  


		1.  1.  L’expérience est la suivante: l’épreuve "le tirage d’une boule dans l’urne" est répétée 5 fois.
		        Comme les tirages se font avec remise, ces 5 épreuves sont indépendantes.
		        Chaque épreuve n’a que deux issues possibles: le joueur tire une boule blanche (succès avec la probabilité $p=\dfrac{2}{10}=\dfrac{1}{5}$) ou le joueur tire une boule noire (échec avec la probabilité $\dfrac{4}{5}$).
		        La variable $X$ considérée représente donc le nombre de succès au cours de l’expérience et suit donc une loi binomiale de paramètre $(5,\dfrac{1}{5})$.
		        C’est-à-dire $X(\Omega)=\llbracket0,5\rrbracket$ et :  $\forall \:k\in \llbracket0,5\rrbracket$, $P(X=k)=\binom{5}{k}(\dfrac{1}{5})^k(\dfrac{4}{5})^{5-k}$.
		        Donc, d’après le cours, $E(X)=5\times\dfrac{1}{5}=1$ et $V(X)=5\times\dfrac{1}{5}\times\left( 1-\dfrac{1}{5}\right)=\dfrac{4}{5}=0,8$.

		    2.  D’après les hypothèses, on a $Y=2X-3(5-X)$, c’est-à-dire $Y=5X-15$.
		        On en déduit que $Y(\Omega)=\left\lbrace 5k-15\:\text{avec}\:k\in\llbracket0,5\rrbracket \right\rbrace$ .
		        Et on a  $\forall \:k\in \llbracket0,5\rrbracket$, $P(Y=5k-15)=
		        P(X=k)=\binom{5}{k}(\dfrac{1}{5})^k(\dfrac{4}{5})^{5-k}$.
		        $Y=5X-15$, donc $E(Y)=5E(X)-15=5-15=-10$.
		        De même, $Y=5X-15$, donc $V(Y)=25V(X)=25\times\dfrac{4}{5}=20$.

		2.  Dans cette question, le joueur tire successivement, sans remise, 5 boules dans cette urne.

		    1.  Comme les tirages se font sans remise, on peut supposer que le joueur tire les 5 boules dans l’urne en une seule fois au lieu de les tirer successivement. Cette supposition ne change pas la loi de $X$.
		        $X(\Omega)=\llbracket0,2 \rrbracket$.
		        Notons $A$ l’ensemble dont les éléments sont les 10 boules initialement dans l’urne.
		        $\Omega$ est constitué de toutes les parties à 5 éléments de $A$. Donc $\mathrm{card}\,\Omega=\dbinom{10}{5}$.
		        Soit $k\in\llbracket0,2\rrbracket$.
		        L’événement $(X=k)$ est réalisé lorsque le joueur tire $k$ boules blanches et $(5-k)$ boules noires dans l’urne. Il a donc $\dbinom{2}{k}$ possibilités pour le choix des boules blanches et $\dbinom{8}{5-k}$ possibilités pour le choix des boules noires.
		        Donc : $\forall\:k\in\llbracket0,2\rrbracket$, $P(X=k)=\dfrac{\dbinom{2}{k}\times\dbinom{8}{5-k}}{\dbinom{10}{5}}$.

		    2.  On a toujours $Y=5X-15$.
		        On en déduit que $Y(\Omega)=\left\lbrace 5k-15\:\text{avec}\:k\in\llbracket0,2\rrbracket \right\rbrace$ .
		        Et on a $\forall \:k\in \llbracket0,2\rrbracket$, $P(Y=5k-15)=
		        P(X=k)=\dfrac{\dbinom{2}{k}\times\dbinom{8}{5-k}}{\dbinom{10}{5}}$.

!!! exercice "EXERCICE 96 probabilités "
	=== "Enoncé"  


		Soit $X$ une variable aléatoire à valeurs dans $\mathbb{N}$, de loi de probabilité donnée par : $\forall n\in \mathbb{N}$, $P(X=n)=p_n$.
		La fonction génératrice de $X$ est notée $G_X$ et elle est définie par $G_X(t)=E[t^X]=\displaystyle \sum_{n=0}^{+\infty}p_nt^n$.

		1.  Prouver que l’intervalle $\left] -1,1\right[$ est inclus dans l’ensemble de définition de $G_X$.

		2.  Soit $X_1$ et $X_2$ deux variables aléatoires indépendantes à valeurs dans $\mathbb{N}$.
		    On pose $S=X_1+X_2$.
		    Démontrer que $\forall t\in \left]-1,1 \right[$, $G_S(t)=G_{X_1} (t)G_{X_2}(t)$:

		    1.  en utilisant le produit de Cauchy de deux séries entières.

		    2.  en utilisant uniquement la définition de la fonction génératrice par $G_X(t)=E[t^X]$.

		    **Remarque**: on admettra, pour la question suivante, que ce résultat est généralisable à $n$ variables aléatoires indépendantes à valeurs dans $\mathbb{N}$.

		3.  Un sac contient quatre boules : une boule numérotée 0, deux boules numérotées 1 et une boule numérotée 2.
		    Soit $n\in\mathbb{N}^{*}$. On effectue $n$ tirages successifs, avec remise, d’une boule dans ce sac.
		    On note $S_n$ la somme des numéros tirés.
		    Soit $t\in \left]-1,1 \right[$.
		    Déterminer $G_{S_n}(t)$ puis en déduire la loi de $S_n$.


	=== "Corrigé"  


		1.  On considère la série entière $\displaystyle\sum p_nt^n$ et on note $R$ son rayon de convergence.
		    La série $\displaystyle\sum p_n$ converge car $\displaystyle\sum_{n=0}^{+\infty}p_n=1$.
		    Donc $\displaystyle\sum p_nt^n$ converge pour $t=1$, donc $R\geqslant 1$.
		    Notons $D_{G_X}$ l’ensemble de définition de $G_{X}$.
		    On a donc :$\left] -1,1 \right[ \subset D_{G_X}$

		2.  Soit $X_1$ et $X_2$ deux variables aléatoires indépendantes à valeurs dans $\mathbb{N}$.
		    On pose $S=X_1+X_2$.
		    Prouvons que $\forall t\in \left]-1,1 \right[$, $G_S(t)=G_{X_1} (t)G_{X_2}(t)$.

		    1.  **En utlisant le produit de Cauchy de deux séries entières**:
		        Notons $R_1$ le rayon de convergence de la série entière $\displaystyle \sum P(X_1=n) t^n$.
		        Notons $R_2$ le rayon de convergence de la série entière $\displaystyle \sum P(X_2=n) t^n$.
		        Notons $R$ le rayon de convergence de la série entière produit $\displaystyle \sum c_n t^n$ avec $c_n =\displaystyle\sum_{k=0}^{n}P(X_1=k)P(X_2=n-k)$.
		        On a, d’après le cours, $R\geqslant \mathrm{min}\left( R_1,R_2\right)$ et :
		        $\forall t\in \left] -R,R\right[$, $\left( \displaystyle \sum _{n=0}^{+\infty}P(X_1=n) t^n \right) \left(  \displaystyle \sum _{n=0}^{+\infty}P(X_2=n) t^n\right) = \displaystyle \sum _{n=0}^{+\infty}\displaystyle\left( \sum_{k=0}^{n}P(X_1=k)P(X_2=n-k)\right) t^n$
		        Or, on a vu dans la question 1. que $R_1\geqslant 1$ et $R_2\geqslant 1$.
		        Donc, $R\geqslant 1$.
		        Donc, par produit de Cauchy pour les séries entières,
		        $\forall t\in \left] -1,1\right[$, $\left( \displaystyle \sum _{n=0}^{+\infty}P(X_1=n) t^n \right) \left(  \displaystyle \sum _{n=0}^{+\infty}P(X_2=n) t^n\right) = \displaystyle \sum _{n=0}^{+\infty}\displaystyle\left( \sum_{k=0}^{n}P(X_1=k)P(X_2=n-k)\right) t^n$.(\*)
		        De plus, pour tout entier naturel $n$,
		        $(S=n)=(X_1+X_2=n)=\displaystyle\bigcup_{k=0}^{n} \left( (X_1=k)\cap (X_2=n-k)\right)$ (union d’événements deux à deux incompatibles).
		        Donc : $P(S=n)=\displaystyle\sum_{k=0}^{n}P(X_1=k)P(X_2=n-k)$.(\*\*)
		        Donc, d’après (\*) et (\*\*), $\forall t\in \left] -1,1\right[$, $G_{X_1}(t)G_{X_2}(t)= \displaystyle \sum _{n=0}^{+\infty}P(S=n) t^n$.
		        C’est-à-dire, $\forall t\in \left] -1,1\right[$, $G_{X_1}(t)G_{X_2}(t)= G_S(t)$.

		    2.  **En utilisant uniquement la définition de de $G_X(t)$**:
		        Soit $t\in \left] -1,1\right[$.
		        D’après 1., $t^{X_{1}}$ et $t^{X_{2}}$ admettent une espérance.
		        De plus, $G_{X_1}(t)=E[t^{X_{1}}]$ et $G_{X_{2}}(t)=E[t^{X_{2}}]$.
		        $X_1$ et $X_2$ sont indépendantes donc $t^{X_{1}}$ et $t^{X_{2}}$ sont indépendantes.
		        Donc $t^{X_{1}}t^{X_{2}}=t^S$ admet une espérance et $E[t^S]=E[t^{X_{1}}]E[t^{X_{2}}]$.
		        C’est-à-dire, $G_S(t)=G_{X_1}(t)G_{X_2}(t)$.

		3.  Soit $S$ variable aléatoire égale à la somme des numéros tirés.
		    Soit $i \in \llbracket 1,n\rrbracket$.
		    On note $X_i$ la variable aléatoire égale au numéro tiré au $i ^{\text{ème}}$ tirage.
		    $X_i\left(\Omega \right)=\left\lbrace 0,1,2\right\rbrace$.
		    De plus, $P(X_i=0)=\frac{1}{4}$, $P(X_i=1)=\frac{2}{4}=\frac{1}{2}$ et $P(X_i=2)=\frac{1}{4}$.
		      Donc, $\forall t\in \left] -1,1\right[$, $G_{X_i} (t)=E[t^{X_i}]= t^0P(X_i=0)+t^1P(X_i=1)+t^2P(X_i=2)$.
		    Donc, $\forall t\in \left] -1,1\right[$, $G_{X_i} (t)=\frac{1}{4}+\frac{1}{2}t+\frac{1}{4}t^2=\frac{1}{4}(t+1)^2$.
		    On a : $S=X_1+X_2+...+X_n$.
		    De plus, les variables aléatoires $X_1,X_2,...,X_n$ sont indépendantes.
		    D’après 2., on en déduit que: $\forall t\in \left] -1,1\right[$, $G_{S} (t)=G_{X_1} (t) G_{X_2} (t)....G_{X_n} (t)$.
		    C’est-à-dire, $\forall t\in \left] -1,1\right[$, $G_{S}(t)=\frac{1}{4^n}\left( 1+t\right) ^{2n}$.
		    Ou encore, $\forall t\in \left] -1,1\right[$, $G_{S}(t)=\displaystyle\sum_{k=0}^{2n} \dbinom{2n}{k}\frac{1}{4^n}t^{k}$.
		    Or, $\forall t\in \left] -1,1\right[$, $G_{S}(t)=\displaystyle\sum_{k=0}^{+\infty} P(S=k)t^{k}$.
		    Donc, par unicité du développement en série entière:
		    $S\left(\Omega \right)=\llbracket 0,2n \rrbracket$ et $\forall k\in\llbracket 0,2n \rrbracket$, $P(S=k)=\dbinom{2n}{k}\frac{1}{4^n}=\dbinom{2n}{k}\left( \frac{1}{2}\right) ^{k} \left( \frac{1}{2}\right) ^{2n-k}$
		    Donc, $S$ suit une loi binomiale de paramètre $(2n,\frac{1}{2})$.

!!! exercice "EXERCICE 97 probabilités "
	=== "Enoncé"  


		Soit $(X,Y)$ un couple de variables aléatoires à valeurs dans $\mathbb{N}^2$ dont la loi est donnée par:
		$\forall (j,k)\in {\mathbb{N}^2}$, $P\left( (X,Y)=(j,k)\right) =\dfrac{(j+k)\left( \dfrac{1}{2}\right) ^{j+k}}{\mathrm{e}\:j!\:k!}$.

		1.  Déterminer les lois marginales de $X$ et de $Y$.
		    Les variables $X$ et $Y$ sont-elles indépendantes?

		2.  Prouver que $E\left[ 2^{X+Y}\right]$ existe et la calculer.


	=== "Corrigé"  


		On rappelle que $\forall x\in{\mathbb{R}}$, $\displaystyle\sum\limits_{}^{} \dfrac{x^{n}}{n!}$ converge et $\displaystyle\sum\limits_{n=0}^{+\infty} \dfrac{x^{n}}{n!}=\text{e}^{x}$.

		1.  $Y(\Omega)=\mathbb{N}$.
		Soit $k\in\mathbb{N}$.
		$P(Y=k)=\displaystyle\sum\limits_{j=0}^{+\infty}P\left( (X,Y)=(j,k)\right) =\displaystyle\sum\limits_{j=0}^{+\infty}\dfrac{(j+k)\left( \dfrac{1}{2}\right) ^{j+k}}{\text{e}\:j!\:k!}$.

		 Or, $\displaystyle\sum\limits_{j\geqslant0}^{}\dfrac{j\left(\dfrac{1}{2} \right) ^{j+k}}{\text{e}\:j!\:k!}=\dfrac{\left(\dfrac{1}{2} \right)^{k+1}}{\text{e}\:k!}\displaystyle\sum\limits_{j\geqslant1}^{}\dfrac{\left(\dfrac{1}{2} \right)^{j-1}}{(j-1)!}$ donc $\displaystyle\sum\limits_{j\geqslant0}^{}\dfrac{j\left(\dfrac{1}{2} \right)^{j+k}}{\text{e}\:j!\:k!}$ converge et

		$$
		\displaystyle\sum\limits_{j=0}^{+\infty}\dfrac{j\left(\dfrac{1}{2} \right)^{j+k}}{\text{e}\:j!\:k!}=\frac{\left(\dfrac{1}{2} \right)^{k+1}}{\text{e}\:k!}\displaystyle\sum\limits_{j=1}^{+\infty}\dfrac{\left(\dfrac{1}{2} \right)^{j-1}}{(j-1)!}=\dfrac{\left(\dfrac{1}{2} \right)^{k+1}}{\text{e}\:k!}\text{e}^{\frac{1}{2}}=\dfrac{\left(\dfrac{1}{2} \right)^{k+1}}{k!\sqrt{\mathrm{e}}}
		\:\:\:\:\:(*)
		\tag{$\star$}.
		$$

		De même, $\displaystyle\sum\limits_{j\geqslant0}^{}\dfrac{k\left(\dfrac{1}{2} \right)^{j+k}}{\text{e}\:j!\:k!}=\frac{k\left(\dfrac{1}{2} \right)^{k}}{\text{e}\:k!}\displaystyle\sum\limits_{j\geqslant0}^{}\dfrac{\left(\dfrac{1}{2} \right)^{j}}{j!}$ donc $\displaystyle\sum\limits_{j\geqslant0}^{}\dfrac{k\left(\dfrac{1}{2} \right)^{j+k}}{\text{e}\:j!\:k!}$ converge et

		$$
		\displaystyle\sum\limits_{j=0}^{+\infty}\dfrac{k\left(\dfrac{1}{2} \right)^{j+k}}{\text{e}\:j!\:k!}=\frac{k\left(\dfrac{1}{2} \right)^{k}}{\text{e}\:k!}\displaystyle\sum\limits_{j=0}^{+\infty}\dfrac{\left(\dfrac{1}{2} \right)^{j}}{j!}=\dfrac{k\left(\dfrac{1}{2} \right)^{k}}{\text{e}\:k!}\text{e}^{\frac{1}{2}}=\dfrac{k\left(\dfrac{1}{2} \right)^{k}}{k!\sqrt{\mathrm{e}}}
		\:\:\:\:\:(**)
		\tag{$\star\star$}.
		$$

!!! exercice "EXERCICE 98 probabilités "
	=== "Enoncé"  


		Une secrétaire effectue, une première fois, un appel téléphonique vers $n$ correspondants distincts.
		On admet que les $n$ appels constituent $n$ expériences indépendantes et que, pour chaque appel, la probabilité d’obtenir le correspondant demandé est $p$ ($p\in{\left]  0,1\right[ }$).
		Soit $X$ la variable aléatoire représentant le nombre de correspondants obtenus.

		1.  Donner la loi de $X$. Justifier.

		2.  La secrétaire rappelle une seconde fois, dans les mêmes conditions, chacun des $n-X$ correspondants qu’elle n’a pas pu joindre au cours de la première série d’appels. On note $Y$ la variable aléatoire représentant le nombre de personnes jointes au cours de la seconde série d’appels.

		    1.  Soit $i\in \llbracket 0,n \rrbracket$. Déterminer, pour $k\in \mathbb{N},$ $P(Y=k|X=i)$.

		    2.  Prouver que $Z=X+Y$ suit une loi binomiale dont on déterminera le paramètre.

		        **Indication** : on pourra utiliser, sans la prouver, l’égalité suivante: $\dbinom{n-i}{k-i}\dbinom{n}{i}=\dbinom{k}{i}\dbinom{n}{k}$.

		    3.  Déterminer l’espérance et la variance de $Z$.


	=== "Corrigé"  


		1.  L’expérience est la suivante: l’épreuve de l’appel téléphonique de la secrétaire vers un correspondant est répétée $n$ fois et ces $n$ épreuves sont mutuellement indépendantes.
		    De plus, chaque épreuve n’a que deux issues possibles: le correspondant est joint avec la probabilité $p$ (succès) ou le correspondant n’est pas joint avec la probabilité $1-p$ (échec).
		    La variable $X$ considérée représente le nombre de succès et suit donc une loi binômiale de paramètres $(n,p)$.
		    C’est-à-dire $X(\Omega)=\llbracket 0,n\rrbracket$ et $\forall k\in{\llbracket 0,n\rrbracket}$ $P(X=k)=\dbinom{n}{k}p^{k}(1-p)^{n-k}$.

		2.  1.  Soit $i$ $\in \llbracket 0,n \rrbracket$. Sous la condition $(X=i)$, la secrétaire rappelle $n-i$ correspondants lors de la seconde série d’appels et donc:

		        $P(Y=k|X=i)=\left\lbrace
		        \begin{array}{l}\dbinom{n-i}{k}p^{k}(1-p)^{n-i-k} \:\:\text{si}\:\: k\in \llbracket 0,n-i \rrbracket\\
		        0 \:\:\text{sinon}
		        \end{array}
		        \right.$

		    2.  $Z(\Omega)=\llbracket 0,n\rrbracket$ et $\forall k\in{\llbracket 0,n\rrbracket}$ $P(Z=k)=\displaystyle\sum\limits_{i=0}^{k}P(X=i\cap Y=k-i)=\displaystyle\sum\limits_{i=0}^{k}P(Y=k-i|X=i)P(X=i)$.

!!! exercice "EXERCICE 99 probabilités "
	=== "Enoncé"  


		1.  Rappeler l’inégalité de Bienaymé-Tchebychev.

		2.  Soit $(Y_n)$ une suite de variables aléatoires mutuellement indépendantes, de même loi et admettant un moment d’ordre 2. On pose $S_n=\displaystyle\sum\limits_{k=1}^{n}Y_k$.
		    Prouver que: $\forall\:a\in \left] 0,+\infty\right[$, $P\left( \left|\dfrac{S_n}{n}-E(Y_1)\right|\geqslant a\right) \leqslant\dfrac{V(Y_1)}{na^2}$.

		3.  **Application**: On effectue des tirages successifs, avec remise, d’une boule dans une urne contenant 2 boules rouges et 3 boules noires.
		    À partir de quel nombre de tirages peut-on garantir à plus de 95% que la proportion de boules rouges obtenues restera comprise entre $0,35$ et $0,45$?

		    **Indication** : considérer la suite $(Y_i)$ de variables aléatoires de Bernoulli où $Y_i$ mesure l’issue du $i^{\text{ème}}$ tirage.


	=== "Corrigé"  


		1.  Soit $a\in{\left]  0,+\infty\right[}$. Pour toute variable aléatoire $X$ admettant un moment d’ordre 2, on a:
		    $P\left( \left|X-E(X)\right|\geqslant a\right) \leqslant\dfrac{V(X)}{a^2}$.

		2.  On pose $X=\dfrac{S_n}{n}$.
		    Par linéarité de l’espérance et comme toutes les variables $Y_i$ ont la même espérance, on a $E(X)=E(Y_1)$.
		    De plus, comme les variables sont mutuellement indépendantes, on a $V(X)=\dfrac{1}{n^2}V(S_n)=\dfrac{1}{n}V(Y_1)$.
		    Alors, en appliquant 1. à $X$, on obtient le résultat souhaité.

		3.  $\forall i\in{\mathbb{N}^*}$, on considère la variable aléatoire $Y_i$ valant 1 si la $i^{\text{ème}}$ boule tirée est rouge et 0 sinon.
		    $Y_i$ suit une loi de Bernoulli de paramètre $p$ avec $p=\dfrac{2}{5}=0,4$ .
		    Les variables $Y_i$ suivent la même loi, sont mutuellement indépendantes et admettent des moments d’ordre 2.
		    On a d’après le cours, $\forall i\in{\mathbb{N}}$, $E(Y_i)=0,4$ et $V(Y_i)=0,4(1-0,4)=0,24$.
		    On pose $S_n=\displaystyle\sum\limits_{i=1}^{n}Y_i$. $S_n$ représente le nombre de boules rouges obtenues au cours de $n$ tirages.
		    Alors $T_n=\dfrac{\displaystyle\sum\limits_{i=1}^{n}Y_i}{n}$ représente la proportion de boules rouges obtenues au cours de $n$ tirages.
		    On cherche à partir de combien de tirages on a $P(0,35\leqslant T_n\leqslant 0,45)>0,95$.
		    Or $P\left(0,35\leqslant T_n\leqslant 0,45\right)=P\left(0,35\leqslant \dfrac{S_n}{n}\leqslant 0,45\right)=P\left(-0,05\leqslant \dfrac{S_n}{n}-E(Y_1)\leqslant 0,05\right)$
		    $=P\left(\left|\dfrac{S_n}{n}-E(Y_1)\right|\leqslant0,05\right)=1-P\left(\left|\dfrac{S_n}{n}-E(Y_1)\right|>0,05\right)$.
		    On a donc $P\left(0,35\leqslant T_n\leqslant 0,45\right)=1-P\left(\left|\dfrac{S_n}{n}-E(Y_1)\right|>0,05\right)$.
		    Or, d’après la question précédente, $P\left( \left|\dfrac{S_n}{n}-E(Y_1)\right|\geqslant 0,05\right) \leqslant\dfrac{0,24}{n(0,05)^{2}}$.
		    Donc $P\left(0,35\leqslant T_n\leqslant 0,45\right)\geqslant 1-\dfrac{0,24}{n(0,05)^{2}}$.
		    Il suffit alors pour répondre au problème de chercher à partir de quel rang $n$, on a $1-\dfrac{0,24}{n(0,05)^{2}}\geqslant 0,95$.
		    La résolution de cette inéquation donne $n\geqslant\dfrac{0,24}{0,05^3}$ c’est-à-dire $n\geqslant 1920$.

!!! exercice "EXERCICE 100 probabilités "
	=== "Enoncé"  


		Soit $\lambda \in{\left] 0,+\infty\right[ }$.
		Soit $X$ une variable aléatoire discrète à valeurs dans $\mathbb{N}^\ast$.
		On suppose que $\forall n\in\mathbb{N}^\ast$, $P(X=n)=\dfrac{\lambda}{n(n+1)(n+2)}$.

		1.  Décomposer en éléments simples la fraction rationnelle $R$ définie par $R(x)=\dfrac{1}{x(x+1)(x+2)}$.

		2.  Calculer $\lambda$.

		3.  Prouver que $X$ admet une espérance, puis la calculer.

		4.  $X$ admet-elle une variance? Justifier.


	=== "Corrigé"  


		1.  On obtient $R(x)= \dfrac{1}{2x}-\dfrac{1}{x+1}+\dfrac{1}{2(x+2)}$.

		2.  Soit $N\in{\mathbb{N}^\ast}$.
		    $P(X\leqslant N)=\lambda\displaystyle\sum\limits_{n=1}^{N}\left(\frac{1}{2n}-\frac{1}{n+1}+\frac{1}{2(n+2)}\right)
		    =\lambda\left(  \dfrac{1}{2}\displaystyle\sum\limits_{n=1}^{N}\dfrac{1}{n}-\displaystyle\sum\limits_{n=2}^{N+1}\dfrac{1}{n}+\frac{1}{2}\displaystyle\sum\limits_{n=3}^{N+2}\dfrac{1}{n}\right)$
		    Et donc, après télescopage, $P(X\leqslant N)=\lambda\left( \dfrac{1}{2}+\dfrac{1}{4}-\dfrac{1}{2}-\dfrac{1}{N+1}+\dfrac{1}{2(N+1)}+\dfrac{1}{2(N+2)}\right)$ c’est-à-dire :
		    $P(X\leqslant N)=\lambda\left( \dfrac{1}{4}-\dfrac{1}{2(N+1)}+\dfrac{1}{2(N+2)}\right)$.(\*)
		    Or $\lim\limits_{N\mapsto+\infty}P(X\leqslant N)=1$.
		    Donc d’après (\*), $\lambda=4$.

		3.  $\displaystyle\sum\limits_{n\geqslant 1}^{}nP(X=n)=\displaystyle\sum\limits_{n\geqslant 1}^{}\frac{4}{(n+1)(n+2)}$ converge car au voisinage de $+\infty$, $\dfrac{4}{(n+1)(n+2)}\underset{+\infty}{\thicksim}\dfrac{4}{n^2}$.
		    Donc $X$ admet une espérance.
		    De plus, $\forall n\in{\mathbb{N}^*}$, $S_n=\displaystyle\sum\limits_{k=1}^{n}kP(X=k)
		    =\displaystyle\sum\limits_{k=1}^{n}\dfrac{4}{(k+1)(k+2)}
		    =4\displaystyle\sum\limits_{k=1}^{n}\left(\dfrac{1}{k+1}-\dfrac{1}{k+2} \right)
		    =4\left( \displaystyle\sum\limits_{k=1}^{n}\dfrac{1}{k+1}-\displaystyle\sum\limits_{k=2}^{n+1}\dfrac{1}{k+1}\right)   =2-\dfrac{4}{n+2}$.
		    Donc $\lim\limits_{n\mapsto+\infty}\displaystyle\sum\limits_{k=1}^{n}kP(X=k)=2$ et $E(X)=2$.

		4.  Comme $E(X)$ existe, $X$ admettra une variance à condition que $X^2$ admette une espérance.
		    $\displaystyle\sum\limits_{n\geqslant 1}n^2P(X=n)=\displaystyle\sum\limits_{n\geqslant 1}\dfrac{4n}{(n+1)(n+2)}$.
		    Or, au voisinage de $+\infty$, $\dfrac{4n}{(n+1)(n+2)}\underset{+\infty}{\thicksim}\dfrac{4}{n}$ et $\displaystyle\sum\limits_{n\geqslant 1}{}\dfrac{1}{n}$ diverge (série harmonique).
		    Donc $\displaystyle\sum\limits_{n\geqslant 1}n^2P(X=n)$ diverge.
		    Donc $X^2$ n’admet pas d’espérance et donc $X$ n’admet pas de variance.

!!! exercice "EXERCICE 101 probabilités "
	=== "Enoncé"  


		Dans une zone désertique, un animal erre entre trois points d’eau $A$, $B$ et $C$.
		À l’instant $t=0$, il se trouve au point A.
		Quand il a épuisé l’eau du point où il se trouve, il part avec équiprobabilité rejoindre l’un des deux autres points d’eau.
		L’eau du point qu’il vient de quitter se régénère alors.
		Soit $n\in\mathbb{N}$.  
		On note $A_n$ l’événement «l’animal est en $A$ après son $n^{\text{ième}}$ trajet».  
		On note $B_n$ l’événement «l’animal est en $B$ après son $n^{\text{ième}}$ trajet».  
		On note $C_n$ l’événement «l’animal est en $C$ après son $n^{\text{ième}}$ trajet».  
		On pose $P(A_n)=a_n$, $P(B_n)=b_n$ et $P(C_n)=c_n$.

		1.  1.  Exprimer, en le justifiant, $a_{n+1}$ en fonction de $a_n$, $b_n$ et $c_n$.

		    2.  Exprimer, de même, $b_{n+1}$ et $c_{n+1}$ en fonction de $a_n$, $b_n$ et $c_n$.

		2.   On considère la matrice $A=\left( {\begin{array}{ccc}
		      0&\frac{1}{2}&\frac{1}{2} \\
		       \frac{1}{2}&0& \frac{1}{2}\\
		      \frac{1}{2}&\frac{1}{2}&0 \\
		    \end{array}} \right)$.

		    1.  Justifier, sans calcul, que la matrice $A$ est diagonalisable.

		    2.  Prouver que $-\dfrac{1}{2}$ est valeur propre de $A$ et déterminer le sous-espace propre associé.

		    3.  Déterminer une matrice $P$ inversible et une matrice $D$ diagonale de $\mathcal{M}_3(\mathbb{R})$ telles que $D=P^{-1}AP$.

		        **Remarque**: le calcul de $P^{-1}$ n’est pas demandé.

		3.  Montrer comment les résultats de la question 2. peuvent être utilisés pour calculer $a_n$, $b_n$ et $c_n$ en fonction de $n$.

		    **Remarque**: aucune expression finalisée de $a_n$, $b_n$ et $c_n$ n’est demandée.


	=== "Corrigé"  


		1.   

		    1.  $(A_n,B_n,C_n)$ est un système complet d’événements donc d’après la formule des probabilités totales:
		        $P(A_{n+1})=P(A_{n+1}|A_n)P(A_n)+P(A_{n+1}|B_n)P(B_n)+P(A_{n+1}|C_n)P(C_n)$.
		        donc ${a_{n + 1}} =0a_n + \frac{1}{2}b_n +\frac{1}{2}c_n$ c’est-à-dire ${a_{n + 1}}= \frac{1}{2}{b_n} + \frac{1}{{2}}{c_n}$.

		    2.  De même, ${b_{n + 1}} = \frac{1}{2}{a_n} + \frac{1}{{2}}{c_n}$ et ${c_{n + 1}} = \frac{1}{2}{a_n} +\frac{1}{{2}}{b_n}$.

		2.  1.  $A$ est symétrique à coefficients réels, donc elle est diagonalisable.

		    2.  $A+\dfrac{1}{2}\mathrm{I}_3=\dfrac{1}{2}\begin{pmatrix}
		        1&1&1\\
		        1&1&1\\
		        1&1&1
		        \end{pmatrix}$ donc $\text{rg}\,\left(A+\dfrac{1}{2}\mathrm{I}_3\right)=1$.
		        Donc $-\frac{1}{2}$ est valeur propre de $A$ et $\dim E_{-\frac{1}{2}}(A)=2$.
		        L’expression de $A+\dfrac{1}{2}\mathrm{I}_3$ donne immédiatement que $E_{-\frac{1}{2}}(A)=\mathrm{Vect}\left(\begin{pmatrix}
		        1\\0\\-1 \end{pmatrix}
		        \begin{pmatrix}
		        0\\1\\-1
		        \end{pmatrix} \right)$.

		    3.  Puisque $\mathrm{tr}\,(A)=0$, on en déduit que $1$ est une valeur propre de $A$ de multiplicité $1$.
		        $A$ étant symétrique réelle, les sous-espaces propres sont supplémentaires sur $\mathbb{R}^3$ et orthogonaux deux à deux.
		        On en déduit que $\mathbb{R}^3=E_{-\frac{1}{2}}(A)\overset{\perp}{\oplus}E_{1}(A)$, donc que $E_{1}(A)=\left( E_{-\frac{1}{2}}(A)\right) ^\perp$ .
		        Donc $E_{1}(A)=\mathrm{Vect}\,\left(\begin{pmatrix}
		        1 \\
		        1\\
		        1
		        \end{pmatrix}\right).$

		        En posant $P=\begin{pmatrix}
		        1 & 1 & 0 \\
		        1 & 0 & 1 \\
		        1 & -1 & -1
		        \end{pmatrix}$ et $D=\begin{pmatrix}
		        1 & 0 & 0 \\
		        0 & -1/2 & 0 \\
		        0 & 0 & -1/2
		        \end{pmatrix}$, on a alors $D=P^{-1}AP$.

		3.  D’après la question 1., $\left(  \begin{array}{c}
		      a_{n+1}\\
		      b_{n+1}\\
		      c_{n+1}
		      \end{array}
		      \right)
		      =A \left(  \begin{array}{c}
		      a_{n}\\
		      b_{n}\\
		      c_{n}
		      \end{array}
		      \right)$
		    Et donc on prouve par récurrence que :
		    $\forall n \in {\mathbb{N}}$, $\left(  \begin{array}{c}
		      a_{n}\\
		      b_{n}\\
		      c_{n}
		      \end{array}
		      \right)=A^{n}\left(  \begin{array}{c}
		      a_{0}\\
		      b_{0}\\
		      c_{0}
		      \end{array}\right)$
		    Or $A=PDP^{-1}$ donc $A^{n}=PD^{n}P^{-1}$.
		    Donc $\left(  \begin{array}{c}
		      a_{n}\\
		      b_{n}\\
		      c_{n}
		      \end{array}
		      \right)=PD^{n}P^{-1}
		      \left(
		      \begin{array}{c}
		      a_{0}\\
		      b_{0}\\
		      c_{0}
		      \end{array}\right)$
		    Or, d’après l’énoncé, $a_0=1$, $b_0=0$ et $c_0=0$ donc :

		     $\left(  \begin{array}{c}
		      a_{n}\\
		      b_{n}\\
		      c_{n}
		      \end{array}
		      \right)=
		      P
		      \left(  \begin{array}{ccc}
		     1&0&0\\
		      0& \left(-\frac{1}{2}\right)^n&0\\
		      0&0& \left(-\frac{1}{2}\right)^n
		      \end{array}
		      \right)
		      P^{-1}
		      \left(  \begin{array}{c}

		      1\\
		      0\\
		      0
		      \end{array}
		      \right).$

!!! exercice "EXERCICE 102 probabilités "
	=== "Enoncé"  


		Soit $N\in\mathbb{N}^*$.
		Soit $p\in\left] 0,1\right[$. On pose $q=1-p$.
		On considère $N$ variables aléatoires $X_1,X_2,\cdots,X_N$ définies sur un même espace probabilisé $\left(\Omega,\mathcal{A},P\right)$, mutuellement indépendantes et de même loi géométrique de paramètre $p$.

		1.  Soit $i\in\llbracket1,N\rrbracket$. Soit $n\in{\mathbb{N}^*}$.
		    Déterminer $P(X_i\leqslant n)$, puis $P(X_i> n)$.

		2.  On considère la variable aléatoire $Y$ définie par $Y=\underset{1\leqslant i\leqslant N}{\min}(X_i)$
		    c’est-à-dire $\forall \omega \in\Omega$, $Y(\omega)=\min\left(X_1(\omega),\cdots,X_N(\omega)\right)$, $\min$ désignant « le plus petit élément de ».

		    1.  Soit $n\in{\mathbb{N}^*}$. Calculer $P(Y>n)$.
		        En déduire $P(Y\leqslant n)$, puis $P(Y=n)$.

		    2.  Reconnaître la loi de $Y$. En déduire $E(Y)$.


	=== "Corrigé"  


		1.  Soit $i\in\llbracket1,N\rrbracket$.
		    $X_i(\Omega)=\mathbb{N}^*$ et $\forall k\in{\mathbb{N}^*}$, $P(X_i=k)=p(1-p)^{k-1}=pq^{k-1}$.
		    Alors on a $P(X_i\leqslant n)=\displaystyle\sum\limits_{k=1}^{n}P(X_i=k)=\displaystyle\sum\limits_{k=1}^{n}pq^{k-1}=p\dfrac{1-q^n}{1-q}=1-q^n$.
		    Donc $P(X_i>n)=1-P(X_i\leqslant n)=q^n$.

		2.  1.  $Y(\Omega)=\mathbb{N}^*$.
		        Soit $n\in{\mathbb{N}^*}$.
		        $P(Y>n)=P((X_1>n)\cap\cdots\cap (X_N>n))$
		        Donc $P(Y>n)=\displaystyle\prod\limits_{i=1}^{N}P(X_i>n)$ car les variables $X_1,\cdots,X_N$ sont mutuellement indépendantes.
		        Donc $P(Y>n)=\displaystyle\prod\limits_{i=1}^{N}q^n=q^{nN}.$
		        Or $P(Y\leqslant n)=1-P(Y>n)$
		        donc $P(Y\leqslant n)=1-q^{nN}$.
		        Calcul de $P(Y=n)$:
		        Premier cas: si $n\geqslant 2$.
		        $P(Y=n)=P(Y\leqslant n)-P(Y\leqslant n-1)$.
		        Donc $P(Y=n)=q^{(n-1)N}(1-q^N)$.
		        Deuxième cas: si $n=1$.
		        $P(Y=n)=P(Y=1)=1-P(Y>1)=1-q^N$.
		        Conclusion: $\forall n\in{\mathbb{N}^*}$, $P(Y=n)=q^{(n-1)N}(1-q^N)$.

		    2.  D’après 2.(a), $\forall n\in{\mathbb{N}^*}$, $P(Y=n)=q^{(n-1)N}(1-q^N)$.
		        C’est-à-dire $\forall n\in{\mathbb{N}^*}$, $P(Y=n)=\left( 1-(1-q^{N})\right) ^{n-1}(1-q^N)$.
		        On en déduit que $Y$ suit une loi géométrique de paramètre $1-q^N$.
		        Donc, d’après le cours, $Y$ admet une espérance et $E(Y)=\dfrac{1}{1-q^N}$.

!!! exercice "EXERCICE 103 probabilités "
	=== "Enoncé"  


		**Remarque**: les questions 1. et 2. sont indépendantes.
		Soit $(\Omega,\mathcal{A},P)$ un espace probabilisé.

		1.  1.  Soit $\left( \lambda_1,\lambda_2 \right) \in \left(\left] 0,+\infty \right[\right) ^2$.
		        Soit $X_1$ et $X_2$ deux variables aléatoires définies sur $(\Omega,\mathcal{A},P)$.
		        On suppose que $X_1$ et $X_2$ sont indépendantes et suivent des lois de Poisson, de paramètres respectifs $\lambda_1$ et $\lambda_2$.
		        Déterminer la loi de $X_1+X_2$.

		    2.  En déduire l’espérance et la variance de $X_1+X_2$.

		2.  Soit $p\in\left]  0,1\right]$. Soit $\lambda \in \left] 0,+\infty \right[$.
		    Soit $X$ et $Y$ deux variables aléatoires définies sur $(\Omega,\mathcal{A},P)$.
		    On suppose que $Y$ suit une loi de Poisson de paramètre $\lambda$.
		    On suppose que $X(\Omega)=\mathbb{N}$ et que, pour tout $m\in\mathbb{N}$, la loi conditionnelle de $X$ sachant $(Y=m)$ est une loi binomiale de paramètre $(m,p)$.
		    Déterminer la loi de $X$.


	=== "Corrigé"  


		1.  1.  $X_1(\Omega)=\mathbb{N}$ et $X_2(\Omega)=\mathbb{N}$ donc $(X_1+X_2)(\Omega)=\mathbb{N}$.
		        Soit $n\in \mathbb{N}$.
		        $(X_1+X_2=n)=\bigcup\limits_{k=0}^{n}\left((X_1=k)\cap (X_2=n-k) \right)$ (union d’évènements deux à deux disjoints).
		        Donc:
		        $$\begin{aligned}
		        P(X_1+X_2=n)&=&\displaystyle\sum\limits_{k=0}^{n} P\left( (X_1=k)\cap(X_2=n-k)\right)  \\
		        & = & \displaystyle\sum\limits_{k=0}^{n} P{(X_1=k)}P(X_2=n-k)\:\text{car}\:X_1 \:\text{et}\: X_2 \:\text{sont indépendantes}.\\
		        & = &\displaystyle\sum\limits_{k=0}^n  e^{-\lambda_1} \dfrac{\lambda_1^k}{k!}\times e^{-\lambda_2}\dfrac{\lambda_2^{n-k}}{(n-k)!}=\dfrac{e^{-(\lambda_1+\lambda_2)}}{n!}\displaystyle\sum\limits_{k=0}^n \dfrac{n!}{k!(n-k)!}\lambda_1^k\lambda_2^{n-k}\\
		         &=&\dfrac{e^{-(\lambda_1+\lambda_2)}}{n!}\displaystyle\sum\limits_{k=0}^n \dbinom{n}{k}\lambda_1^k\lambda_2^{n-k}=e^{-(\lambda_1+\lambda_2)}\dfrac{(\lambda_1+\lambda_2)^n}{n!}\end{aligned}$$ Ainsi $X_1+X_2\leadsto\mathscr{P}(\lambda_1+\lambda_2)$.

		        **Remarque**: cette question peut aussi être traitée en utilisant les fonctions génératrices.

		    2.  $X_1+X_2\leadsto \mathscr{P}(\lambda_1+\lambda_2)$ donc, d’après le cours, $E(X_1+X_2)=\lambda_1+\lambda_2$ et $V(X_1+X_2)=\lambda_1+\lambda_2$.

		2.  Soit $k\in\mathbb{N}$, $P(X=k)=\displaystyle\sum\limits_{m=0}^{+\infty}P\left( (X=k)\cap (Y=m)\right)=\displaystyle\sum\limits_{m=0}^{+\infty}P_{(Y=m)}( X=k)P(Y=m)$.
		    Or, par hypothèse, $$\forall m\in \mathbb{N},\, P_{(Y=m)}(X=k)=\left\lbrace
		    \begin{array}{cc}
		    \dbinom{m}{k} p^k(1-p)^{m-k}&\text{si}\:k\leqslant m\\
		    0&\text{sinon}\\
		    \end{array} \right.$$ Donc : $$\begin{aligned}
		    P(X=k)&=&\displaystyle\sum\limits_{m=k}^{+\infty} \binom{m}{k} p^k(1-p)^{m-k}\mathrm{e}^{-\lambda}\:\frac{\lambda^m}{m!}
		    =\mathrm{e}^{-\lambda}\frac{p^k}{k!}\lambda^k\displaystyle\sum\limits_{m=k}^{+\infty} \frac{\left( \lambda(1-p)\right)^{m-k}}{(m-k)!}\\
		    &=&\mathrm{e}^{-\lambda}\frac{p^k}{k!}\lambda^k\displaystyle\sum\limits_{m=0}^{+\infty} \frac{\left( \lambda(1-p)\right)^{m}}{m!}=\mathrm{e}^{-\lambda}\frac{p^k}{k!}\lambda^k\mathrm{e}^{\lambda(1-p)}\\
		    &=&\mathrm{e}^{-\lambda p}\frac{\left(\lambda p \right) ^k}{k!}\\\end{aligned}$$ Ainsi $X\leadsto \mathscr{P}(\lambda p)$.

!!! exercice "EXERCICE 104 probabilités "
	=== "Enoncé"  


		Soit $n$ un entier naturel supérieur ou égal à 3.
		On dispose de $n$ boules numérotées de $1$ à $n$ et d’une boîte formée de trois compartiments identiques également numérotés de 1 à 3.
		On lance simultanément les $n$ boules.
		Elles viennent toutes se ranger aléatoirement dans les 3 compartiments.
		Chaque compartiment peut éventuellement contenir les $n$ boules.
		On note $X$ la variable aléatoire qui à chaque expérience aléatoire fait correspondre le nombre de compartiments restés vides.

		1.  Préciser les valeurs prises par $X$.

		2.  1.  Déterminer la probabilité $P(X=2)$.

		    2.  Finir de déterminer la loi de probabilité de $X$.

		3.  1.  Calculer $E(X)$.

		    2.  Déterminer $\lim\limits_{n\to +\infty}^{}E(X)$. Interpréter ce résultat.


	=== "Corrigé"  


		1.  $X(\Omega)=\llbracket 0,2\rrbracket$.

		2.  1.  Pour que l’événement $(X=2)$ se réalise, on a $\dbinom{3}{2}$ possibilités pour choisir les 2 compartiments restant vides. Les deux compartiments restant vides étant choisis, chacune des $n$ boules viendra se placer dans le troisième compartiment avec la probabilité $\dfrac{1}{3}$.
		        De plus les placements des différentes boules dans les trois compartiments sont indépendants.
		        Donc $P(X=2)=\dbinom{3}{2}\left( \dfrac{1}{3}\right) ^n=3\left( \dfrac{1}{3}\right) ^n=\left( \dfrac{1}{3}\right) ^{n-1}$.

		    2.  Déterminons $P(X=1)$.
		        Pour que l’événement $(X=1)$ se réalise, on a $\binom{3}{1}$ possibilités pour choisir le compartiment restant vide.
		        Le compartiment restant vide étant choisi, on note $A$ l’événement : «les $n$ boules doivent se placer dans les deux compartiments restants (que nous appellerons compartiment $a$ et compartiment $b$) sans laisser l’un d’eux vide».
		        Soit $k\in\llbracket1,n-1\rrbracket$.
		        On note $A_k$ l’événement : « $k$ boules se placent dans le compartiment $a$ et les $(n-k)$ boules restantes dans le compartiment $b$».
		        On a alors $A=\displaystyle\bigcup \limits_{k=1}^{n-1}A_k$.
		        On a $\forall\:k\in\llbracket1,n-1\rrbracket$, $P(A_k)=\dbinom{n}{k}\left( \dfrac{1}{3}\right) ^k\left( \dfrac{1}{3}\right) ^{n-k}=\dbinom{n}{k}\left( \dfrac{1}{3}\right) ^n$.
		        Donc $P(X=1) = \dbinom{3}{1}P(\displaystyle\bigcup \limits_{k=1}^{n-1}A_k)
		        =3\displaystyle\sum \limits_{k=1}^{n-1}P(A_k)$ car $A_1,A_2,...,A_{n-1}$ sont deux à deux incompatibles.
		        Donc $P(X=1)=3\displaystyle\sum \limits_{k=1}^{n-1}\dbinom{n}{k}\left( \dfrac{1}{3}\right) ^n
		        =\left( \frac{1}{3}\right) ^{n-1}\displaystyle\sum \limits_{k=1}^{n-1}\dbinom{n}{k}
		        =\left( \dfrac{1}{3}\right) ^{n-1}\left(\displaystyle \sum \limits_{k=0}^{n}\dbinom{n}{k}-2\right)
		        = \left( \dfrac{1}{3}\right) ^{n-1}\left( 2^n-2\right).$
		        Donc $P(X=1)=\left( \dfrac{1}{3}\right) ^{n-1}\left( 2^n-2\right).$
		        Enfin, $P(X=0)=1-P(X=2)-P(X=1)$ donc $P(X=0)=1-\left( \frac{1}{3}\right) ^{n-1}-\left( \frac{1}{3}\right) ^{n-1}\left( 2^n-2\right)$.
		        Donc $P(X=0)=1- \left( \dfrac{1}{3}\right) ^{n-1}\left( 2^n-1\right)$.


		 **Autre méthode**:
		    Une épreuve peut être assimilée à une application de $\llbracket1,n\rrbracket$ (ensemble des numéros des boules) dans $\llbracket1,3\rrbracket$ (ensemble des numéros des cases).
		    Notons $\Omega$ l’ensemble de ces applications.
		    On a donc : $\mathrm{card}\:\Omega=3^n$.
		    Les boules vont se "ranger aléatoirement dans les trois compartiments", donc il y a équiprobabilité sur $\Omega$.
		    (a) L’événement $(X=2)$ correspond aux applications dont les images se concentrent sur le même élément de $\llbracket1,3\rrbracket$, c’est-à-dire aux applications constantes.
		    Donc $P(X=2)=\dfrac{3}{3^n}=\dfrac{1}{3^{n-1}}$.
		    (b) Comptons à présent le nombre d’applications correspondant à l’événement $(X=1)$, c’est-à-dire le nombre d’applications dont l’ensemble des images est constitué de deux éléments exactement.
		    On a 3 possibilités pour choisir l’élément de $\llbracket1,3\rrbracket$ qui n’a pas d’antécédent et ensuite, chaque fois, il faut compter le nombre d’applications de $\llbracket1,n\rrbracket$ vers les deux éléments restants de $\llbracket1,3\rrbracket$, en excluant bien sûr les deux applications constantes.
		    On obtient donc $2^n-2$ applications.
		    D’où $P(X=1)=\dfrac{3\times \left( 2^n-2\right) }{3^n}=\dfrac{1}{3^{n-1}}\left( 2^n-2\right)$.
		    Enfin, comme dans la méthode précédente, $P(X=0)=1-P(X=2)-P(X=1)$ donc $P(X=0)=1-\left( \frac{1}{3}\right) ^{n-1}-\left( \frac{1}{3}\right) ^{n-1}\left( 2^n-2\right)$.

		3.  1.  $E(X)=0P(X=0)+1P(X=1)+2P(X=2)=\left( \dfrac{1}{3}\right) ^{n-1}\left( 2^n-2\right)+2\left( \dfrac{1}{3}\right) ^{n-1}$.
		        Donc $E(X)=3\left(\dfrac{2}{3}\right)^{n}.$

		    2.  D’après 3.(a), $\lim\limits_{n\to+\infty}^{}E(X)=\lim\limits_{n\to+\infty}^{}3\left(\dfrac{2}{3}\right)^{n}=0$.
		        Quand le nombre de boules tend vers $+\infty$, en moyenne aucun des trois compartiments ne restera vide.

!!! exercice "EXERCICE 105 probabilités "
	=== "Enoncé"  


		1.  Énoncer et démontrer la formule de Bayes pour un système complet d’événements.

		2.  On dispose de 100 dés dont 25 sont pipés (c’est-à-dire truqués).
		    Pour chaque dé pipé, la probabilité d’obtenir le chiffre 6 lors d’un lancer vaut $\dfrac{1}{2}$.

		    1.  On tire un dé au hasard parmi les 100 dés. On lance ce dé et on obtient le chiffre 6.
		        Quelle est la probabilité que ce dé soit pipé?

		    2.  Soit $n\in\mathbb{N}^*.$
		        On tire un dé au hasard parmi les 100 dés. On lance ce dé $n$ fois et on obtient $n$ fois le chiffre $6$.
		        Quelle est la probabilité $p_n$ que ce dé soit pipé?

		    3.  Déterminer $\lim\limits_{n\to+\infty}^{}p_n$. Interpréter ce résultat.


	=== "Corrigé"  


		1.  Soit $(\Omega,\mathcal{A},P)$ un espace probabilisé.
		    Soit $B$ un événement de probabilité non nulle et $\left( A_i\right) _{i\in I}$ un système complet d’événements de probabilités non nulles.
		    Alors, $\forall\: i_0 \in I$, $P_B(A_{i_0})=\dfrac{P(A_{i_0})P_{A_{i_0}}(B)}{\displaystyle\sum\limits_{i\in I}^{}P(A_i)P_{A_i}(B)}.$


		    **Preuve**: $P_B(A_{i_0})=\dfrac{P(A_{i_0}\cap B)}{P(B)}=\dfrac{P(A_{i_0})P_{A_{i0}}(B)}{P(B)}$.(1)
		    Or $\left( A_i\right) _{i\in I}$ un système complet d’événements donc $P(B)=\displaystyle\sum\limits_{i\in I}^{}P(A_{i}\cap B)$.
		    Donc $P(B)=\displaystyle\sum\limits_{i\in I}^{}P(A_i)P_{A_i}(B)$.(2).
		    (1) et (2) donnent le résultat souhaité.

		2.  1.  On tire au hasard un dé parmi les 100 dés.
		        Notons $T$ l’événement: «le dé choisi est pipé».
		        Notons $A$ l’événement :« On obtient le chiffre 6 lors du lancer ».
		        On demande de calculer $P_A(T)$.
		        Le système $(T,\overline{T})$ est un système complet d’événements de probabilités non nulles.
		        On a d’ailleurs, $P(T)=\dfrac{25}{100}=\dfrac{1}{4}$ et donc $P(\overline{T})=\dfrac{3}{4}$.
		        Alors, d’après la formule de Bayes, on a :
		        $P_A(T)=\dfrac{P(T)P_T(A)}{P_T(A)P(T)+P_{\overline{T}}(A)P(\overline{T})}
		         =\dfrac{\dfrac{1}{4}\times\dfrac{1}{2}}{\dfrac{1}{2}\times\dfrac{1}{4}+\dfrac{1}{6}\times\dfrac{3}{4}}=\dfrac{1}{2}.$

		    2.  Soit $n\in\mathbb{N}^*$.
		        On choisit au hasard un dé parmi les 100 dés.
		        $\forall \:k\in\llbracket1,n\rrbracket$, on note $A_k$ l’événement « on obtient le chiffre 6 au $k^{\text{ième}}$ lancer ».
		        On pose $A=\displaystyle\bigcap \limits_{k=1}^{n}A_k$.
		        On nous demande de calculer $p_n=P_A(T)$.
		        Le système $(T,\overline{T})$ est un système complet d’événements de probabilités non nulles.
		        On a d’ailleurs, $P(T)=\dfrac{25}{100}=\dfrac{1}{4}$ et donc $P(\overline{T})=\frac{3}{4}$.
		        Alors d’après la formule de Bayes, on a :

		        $p_n=P_A(T)=\dfrac{P(T)P_T(A)}{P_T(A)P(T)+P_{\overline{T}}(A)P\left( \overline{T}\right) }$
		        Donc $p_n=\dfrac{\dfrac{1}{4}\times\left( \dfrac{1}{2}\right) ^n}{\left( \dfrac{1}{2}\right) ^n\times\dfrac{1}{4}+\left(\dfrac{1}{6}\right) ^n\times\dfrac{3}{4}}=\dfrac{1}{1+\dfrac{1}{3^{n-1}}}.$

		    3.  $\forall \:n\in\mathbb{N}^*$, $p_n=\dfrac{1}{1+\dfrac{1}{3^{n-1}}}$ Donc $\lim\limits_{n\to +\infty}^{}p_n=1$.
		        Ce qui signifie que, lorsqu’on effectue un nombre élevé de lancers, si on n’obtient que des 6 sur ces lancers alors il y a de fortes chances que le dé tiré au hasard au départ soit pipé.

!!! exercice "EXERCICE 106 probabilités "
	=== "Enoncé"  


		$X$ et $Y$ sont deux variables aléatoires indépendantes et à valeurs dans $\mathbb{N}$.
		Elles suivent la même loi définie par: $\forall\:k\in\mathbb{N}$, $P(X=k)=P(Y=k)=pq^k$ où $p \in \left] 0,1\right[$ et $q=1-p$.
		On considère alors les variables $U$ et $V$ définies par $U=\sup(X,Y)$ et $V=\inf(X,Y)$.

		1.  Déterminer la loi du couple $(U,V)$.

		2.  Déterminer la loi marginale de $U$.
		    On admet que $V(\Omega)=\mathbb{N}$ et que, $\forall\:n\in\mathbb{N}$, $P(V=n)=pq^{2n}(1+q)$.

		3.  Prouver que $W=V+1$ suit une loi géométrique.
		    En déduire l’espérance de $V$.

		4.  $U$ et $V$ sont-elles indépendantes?


	=== "Corrigé"  


		1.  $(U,V)(\Omega)=\left\lbrace (m,n)\in\mathbb{N}^2\:\text{tel que}\: m\geqslant n\right\rbrace$. Soit $(m,n)\in\mathbb{N}^2$ tel que $m\geqslant n$.  

		    **Premier cas: si m=n**
		    $P((U=m)\cap(V=n))=P((X=n)\cap(Y=n))=P(X=n)P(Y=n)$ car $X$ et $Y$ sont indépendantes.
		    Donc $P((U=m)\cap(V=n))=p^2q^{2n}$.  
		    **Deuxième cas: si m\>n**
		    $P((U=m)\cap(V=n))=P(\left[(X=m)\cap(Y=n)\right]\cup \left[(X=n)\cap(Y=m)\right])$.

		    Les événements $\left( (X=m)\cap(Y=n)\right)$ et $\left( (X=n)\cap(Y=m)\right)$ sont incompatibles donc:
		    $P((U=m)\cap(V=n))=P\left( (X=m)\cap(Y=n)\right)+P\left( (X=n)\cap(Y=m)\right)$.
		    Or les variables $X$ et $Y$ suivent la même loi et sont indépendantes donc:
		    $P((U=m)\cap (V=n))=2P (X=m)P(Y=n)=2p^2q^{n+m}$.


		    **Bilan**: $P((U=m)\cap (V=n))=\left\lbrace
		    \begin{array}{ll}
		    p^2q^{2n}&\:\text{ si}\: m=n\\
		    2p^2q^{n+m} &\:\text{si}\: m>n\\
		    0 & \:\text{ sinon}
		    \end{array}
		    \right.$

		2.  $U(\Omega)=\mathbb{N}$ et $V(\Omega)=\mathbb{N}.$ Soit $m\in\mathbb{N}$.
		    $P(U=m)=\displaystyle\sum\limits_{n=0}^{+\infty}P((U=m)\cap(V=n))$. ( loi marginale de $(U,V)$ )
		    Donc d’après 1., $P(U=m)=\displaystyle\sum\limits_{n=0}^{m}P((U=m)\cap(V=n))$ $(*)$

		    **Premier cas**: $m\geqslant 1$
		    D’après $(*)$, $P(U=m)=P((U=m)\cap(V=m))+\displaystyle\sum\limits_{n=0}^{m-1}P((U=m)\cap(V=n))$.
		    Donc $P(U=m)=p^2q^{2m}+\displaystyle\sum\limits_{n=0}^{m-1}2p^2q^{n+m}
		    =p^2q^{2m}+2p^2q^m\displaystyle\sum\limits_{n=0}^{m-1}q^{n}
		    =p^2q^{2m}+2p^2q^m\dfrac{1-q^m}{1-q}=p^2q^{2m}+2pq^m(1-q^m)$
		    Donc $P(U=m)=pq^m(pq^m+2-2q^m)$.

		    **Deuxième cas** : $m=0$
		    D’après $(*)$ et 1., $P(U=0)=P((U=0)\cap (V=0))=p^2$.


		    **Bilan** : $\forall\:m\in\mathbb{N}$, $P(U=m)=pq^m(pq^m+2-2q^m)$.

		3.  $W\left(\Omega \right)=\mathbb{N}^*$.
		    Soit $n\in\mathbb{N}^*$.
		    $P(W=n)=P(V=n-1)=pq^{2(n-1)}(1+q)=(1-q)q^{2(n-1)}(1+q)$.
		    Donc $P(W=n)=(1-q^2)\left( q^2\right) ^{n-1}$.
		    Donc $W$ suit une loi géométrique de paramètre $1-q^2$.
		    Donc, d’après le cours, $E(W)=\dfrac{1}{1-q^2}$. Donc $E(V)=E(W-1)=E(W)-1=\dfrac{q^2}{1-q^2}$.

		4.  $P((U=0)\cap (V=1))=0$ et $P(U=0)P( V=1)=p^3q^2(1+q)\neq 0$. Donc $U$ et $V$ ne sont pas indépendantes.

!!! exercice "EXERCICE 107 probabilités "
	=== "Enoncé"  


		On dispose de deux urnes $U_1$ et $U_2$.
		L’urne $U_1$ contient deux boules blanches et trois boules noires.
		L’urne $U_2$ contient quatre boules blanches et trois boules noires.
		On effectue des tirages successifs dans les conditions suivantes:
		on choisit une urne au hasard et on tire une boule dans l’urne choisie.
		On note sa couleur et on la remet dans l’urne d’où elle provient.
		Si la boule tirée était blanche, le tirage suivant se fait dans l’urne $U_1$.
		Sinon le tirage suivant se fait dans l’urne $U_2$.
		Pour tout $n\in\mathbb{N}^*$, on note $B_n$ l’événement « la boule tirée au $n^{\text{ième}}$ tirage est blanche » et on pose $p_n=P(B_n)$.

		1.  Calculer $p_1$.

		2.  Prouver que : $\forall\:n\in\mathbb{N}^*$, $p_{n+1}=-\dfrac{6}{35}p_n+\dfrac{4}{7}$.

		3.  En déduire, pour tout entier naturel $n$ non nul, la valeur de $p_n$.


	=== "Corrigé"  


		1.  Notons $U_1$ l’événement le premier tirage se fait dans l’urne $U_1$.
		    Notons $U_2$ l’événement le premier tirage se fait dans l’urne $U_2$.
		    $(U_1,U_2)$ est un système complet dévénements.
		    Donc d’après la formule des probabilités totales, $p_1=P(B_1)=P_{U_1}(B_1)P(U_1)+P_{U_2}(B_1)P(U_2)$.
		    Donc $p_1=\dfrac{2}{5}\times\dfrac{1}{2}+\dfrac{4}{7}\times\dfrac{1}{2}=\dfrac{17}{35}$
		    On a donc $p_1=\dfrac{17}{35}$.

		2.  Soit $n\in\mathbb{N}^*.$
		    $(B_n, \overline{B_n})$ est un système complet d’événements.
		    Donc, d’après la formule des probabilités totales, $P(B_{n+1})=P_{B_n}(B_{n+1})P(B_n)+P_{\overline{B_n}}(B_{n+1})P(\overline{B_n})$.
		    Alors en tenant compte des conditions de tirage, on a $p_{n+1}=\dfrac{2}{5}p_n+\dfrac{4}{7}(1-p_n)$.
		    Donc, $\forall\:n\in\mathbb{N}^*.$ $p_{n+1}=-\dfrac{6}{35}p_n+\dfrac{4}{7}$.

		3.  $\forall n\in\mathbb{N}^*$, $p_{n+1}=-\dfrac{6}{35}p_n+\dfrac{4}{7}.$
		    Donc $(p_n)_{n\in\mathbb{N}^*}$ est une suite arithmético-géométrique.
		    On résout l’équation $l=-\dfrac{6}{35}l+\dfrac{4}{7}$ et on trouve $l=\dfrac{20}{41}$.
		    On considère alors la suite $(u_n)_{n\in\mathbb{N}^*}$ définie par : $\forall \:n\in\mathbb{N}^*$ , $u_n=p_n-l$.
		    $(u_n)_{n\in\mathbb{N}^*}$ est géométrique de raison $-\dfrac{6}{35}$, donc, $\forall \:n\in\mathbb{N}^*$, $u_n=\left( -\dfrac{6}{35}\right) ^{n-1}u_1$.
		    Or $u_1=p_1-l=\dfrac{17}{35}-\dfrac{20}{41}=-\dfrac{3}{1435}.$
		    On en déduit que, $\forall \:n\in\mathbb{N}^*$, $p_n=u_n+l$, c’est-à-dire $p_n=-\dfrac{3}{1435}\left( -\dfrac{6}{35}\right) ^{n-1}+\dfrac{20}{41}.$

!!! exercice "EXERCICE 108 probabilités "
	=== "Enoncé"  


		Soient $X$ et $Y$ deux variables aléatoires définies sur un même espace probabilisé $(\Omega,\mathcal{A},P)$ et à valeurs dans $\mathbb{N}$.
		On suppose que la loi du couple $(X,Y)$ est donnée par:
		$\forall\:(i,j)\in\mathbb{N}^2$, $P((X=i)\cap (Y=j))=\dfrac{1}{\mathrm{e}\:2^{i+1}j!}$

		1.  Déterminer les lois de $X$ et de $Y$.

		2.  1.  Prouver que $1+X$ suit une loi géométrique et en déduire l’espérance et la variance de $X$.

		    2.  Déterminer l’espérance et la variance de $Y$.

		3.  Les variables $X$ et $Y$ sont-elles indépendantes?

		4.  Calculer $P(X=Y)$.


	=== "Corrigé"  


		1.  $\forall\:(i,j)\in\mathbb{N}^2$, $P((X=i)\cap (Y=j))=\dfrac{1}{\mathrm{e}\:2^{i+1}j!}$.
		    $X(\Omega)=\mathbb{N}$.
		    Soit $i\in \mathbb{N}$.
		    $\displaystyle\sum\limits_{j\geqslant 0}^{}\dfrac{1}{\mathrm{e}\:2^{i+1}j!}=\dfrac{1}{\mathrm{e}\:2^{i+1}}\displaystyle\sum\limits_{j\geqslant 0}^{}\dfrac{1}{j!}$ converge et $\displaystyle\sum\limits_{j=0}^{+\infty}\dfrac{1}{\mathrm{e}\:2^{i+1}j!}=\dfrac{1}{2^{i+1}}$.
		    Or $P(X=i)=\displaystyle\sum\limits_{j=0}^{+\infty}P((X=i)\cap (Y=j))$ donc $P(X=i)=\displaystyle\sum\limits_{j=0}^{+\infty}\dfrac{1}{\mathrm{e}2^{i+1}j!}
		    =\dfrac{1}{\mathrm{e}2^{i+1}}\displaystyle\sum\limits_{j=0}^{+\infty}\dfrac{1}{j!}
		    =\dfrac{1}{2^{i+1}}.$
		    Conclusion: $\forall\:i\in\mathbb{N}$, $P(X=i)=\dfrac{1}{2^{i+1}}.$
		    $Y(\Omega)=\mathbb{N}$.
		    Soit $j\in \mathbb{N}$.
		    $\displaystyle\sum\limits_{i\geqslant 0}^{}\dfrac{1}{\mathrm{e}\:2^{i+1}j!}=
		      \dfrac{1}{2\mathrm{e}j!}\displaystyle\sum\limits_{i\geqslant 0}^{}\left(\dfrac{1}{2} \right)^i$ converge (série géométrique de raison $\dfrac{1}{2}$) et $\displaystyle\sum\limits_{i=0}^{+\infty}\dfrac{1}{\mathrm{e}\:2^{i+1}j!}=\dfrac{1}{2\mathrm{e}j!}\dfrac{1}{1-\dfrac{1}{2}}=\dfrac{1}{\mathrm{e}j!}$.
		    Or $P(Y=j)=\displaystyle\sum\limits_{i=0}^{+\infty}P((X=i)\cap (Y=j))$.
		    Donc $P(Y=j)=\displaystyle\sum\limits_{i=0}^{+\infty}\dfrac{1}{\text{e}2^{i+1}j!}
		    =\dfrac{1}{2\mathrm{e}j!}\displaystyle\sum\limits_{i=0}^{+\infty}\left(\dfrac{1}{2} \right)^i
		    =\dfrac{1}{2\mathrm{e}j!}\dfrac{1}{1-\dfrac{1}{2}}=\dfrac{1}{\mathrm{e}j!}$.
		    Conclusion: $\forall\:j\in\mathbb{N}$, $P(Y=j)=\dfrac{1}{\mathrm{e}j!}$.

		2.  1.  On pose $Z=X+1$.
		        $Z(\Omega)=N^*$.
		        De plus, $\forall\:n\in\mathbb{N}^*$, $P(Z=n)=P(X=n-1)=\dfrac{1}{2^n}=\dfrac{1}{2}\left( \dfrac{1}{2}\right)^{n-1}$.
		        Donc $Z$ suit une loi géométrique de paramètre $p=\dfrac{1}{2}$.
		        Donc, d’après le cours, $E(Z)=\dfrac{1}{p}=2$ et $V(Z)=\dfrac{1-p}{p^2}=2$.
		        Donc $E(X)=E(Z-1)=E(Z)-1=2-1=1$ et $V(X)=V(Z-1)=V(Z)=2$.
		        C’est-à-dire $E(X)=1$ et $V(X)=2$.

		    2.  $Y$ suit une loi de Poisson de paramètre $\lambda=1$.
		        Donc, d’après le cours, $E(Y)=V(Y)=\lambda=1$.

		3.  On a : $\forall\:(i,j)\in\mathbb{N}^2$, $P((X=i)\cap(Y=j))=P(X=i)P(Y=j)$. Donc les variables $X$ et $Y$ sont indépendantes.

		4.  $(X=Y)=\displaystyle\bigcup\limits_{k\in\mathbb{N}}^{}((X=k)\cap (Y=k))$ et il s’agit d’une union d’événements deux à deux incompatibles donc:
		    $P(X=Y)=\displaystyle\sum\limits_{k=0}^{+\infty}P((X=k)\cap (Y=k))
		    =\displaystyle\sum\limits_{k=0}^{+\infty}\dfrac{1}{\text{e}2^{k+1}}\dfrac{1}{k!}
		    =\dfrac{1}{2\mathrm{e}}\displaystyle\sum\limits_{k=0}^{+\infty}\dfrac{\left( \dfrac{1}{2}\right) ^k}{k!}
		    =\dfrac{1}{2\mathrm{e}}\mathrm{e}^{\frac{1}{2}}$
		    Donc $P(X=Y)=\dfrac{1}{2\sqrt{\mathrm{e}}}.$

!!! exercice "EXERCICE 109 probabilités "
	=== "Enoncé"  


		Soit $n\in\mathbb{N}^*$. Une urne contient $n$ boules blanches numérotées de 1 à $n$ et deux boules noires numérotées 1 et 2.
		On effectue le tirage une à une, sans remise, de toutes les boules de l’urne.
		On note $X$ la variable aléatoire égale au rang d’apparition de la première boule blanche.
		On note $Y$ la variable aléatoire égale au rang d’apparition de la première boule numérotée 1.

		1.  Déterminer la loi de $X$.

		2.  Déterminer la loi de $Y$.


	=== "Corrigé"  


		1.  $X(\Omega)=\llbracket 1,3\rrbracket$.
		    $\forall \: i\in\llbracket1,n\rrbracket$, on note $B_i$ la $i ^{\text{ème}}$ boule blanche.
		    $\forall \: i\in\llbracket1,2\rrbracket$, on note $N_i$ la $i ^{\text{ème}}$ boule noire.
		    On pose $E=\left\lbrace B_1,B_2,...,B_n,N_1,N_2 \right\rbrace$.
		    Alors $\Omega$ est l’ensemble des permutations de $E$ et donc $\text{card}(\Omega)=(n+2)!$.

		    $(X=1)$ correspond aux tirages des $(n+2)$ boules pour lesquels la première boule tirée est blanche.
		    On a donc $n$ possibilités pour le choix de la première boule blanche et donc $(n+1)!$ possibilités pour les tirages restants.
		    Donc $P(X=1)=\dfrac{n\times(n+1)!}{(n+2)!}=\dfrac{n}{n+2}.$
		    $(X=2)$ correspond aux tirages des $(n+2)$ boules pour lesquels la première boule tirée est noire et la seconde est blanche.
		    On a donc 2 possibilités pour la première boule, puis $n$ possibilités pour la seconde boule et enfin $n!$ possibilités pour les tirages restants.
		    Donc $P(X=2)=\dfrac{2\times n\times(n)!}{(n+2)!}=\dfrac{2n}{(n+1)(n+2)}.$
		    $(X=3)$ correspond aux tirages des $(n+2)$ boules pour lesquels la première boule et la seconde boule sont noires.
		    On a donc 2 possibilités pour la première boule, puis une seule possibilité pour la seconde et enfin $n!$ possibilités pour les boules restantes.
		    Donc $P(X=3)=\dfrac{2\times 1\times(n)!}{(n+2)!}=\dfrac{2}{(n+1)(n+2)}.$

		 **Autre méthode**:
		    Dans cette méthode, on ne s’interesse qu’aux "premières" boules tirées, les autres étant sans importance.
		    $X(\Omega)=\llbracket 1,3\rrbracket$.
		    $(X=1)$ est l’événement: "obtenir une boule blanche au premier tirage".
		    Donc $P(X=1)=\dfrac{\text{nombre de boules blanches}}{\text{nombre de boules de l'urne}}=\dfrac{n}{n+2}$.
		    $(X=2)$ est l’événement: " obtenir une boule noire au premier tirage puis une boule blanche au second tirage".
		    D’où $P(X=2)=\dfrac{2}{n+2}\times\dfrac{n}{n+1}=\dfrac{2n}{(n+2)(n+1)}$, les tirages se faisant sans remise.
		    $(X=3)$ est l’événement : "obtenir une boule noire lors de chacun des deux premiers tirages puis une boule blanche au troisième tirage".
		    D’où $P(X=3)=\dfrac{2}{n+2}\times\dfrac{1}{n+1}\times\dfrac{n}{n}=\dfrac{2}{(n+2)(n+1)}$, les tirages se faisant sans remise.

		2.  $Y(\Omega)=\llbracket 1,n+1\rrbracket$.
		    Soit $k\in \llbracket 1,n+1\rrbracket$.
		    L’événement $(Y=k)$ correspond aux tirages des $(n+2)$ boules où les $(k-1)$ premières boules tirées ne sont ni $B_1$ ni $N_1$ et la $k^{\text{ième}}$ boule tirée est $B_1$ ou $N_1$.
		    On a donc, pour les $(k-1)$ premières boules tirées , $\dbinom{n}{k-1}$ choix possibles de ces boules et $(k-1)!$ possibilités pour leur rang de tirage sur les $(k-1)$ premiers tirages, puis 2 possibilités pour le choix de la $k^{\text{ième}}$ boule et enfin $(n+2-k)!$ possibilités pour les rangs de tirage des boules restantes.
		    Donc $P(Y=k)=\dfrac{\dbinom{n}{k-1}\times (k-1)!\times 2\times (n+2-k)!}{(n+2)!}
		    =\dfrac{2\dfrac{n!}{(n-k+1)!}\times(n+2-k)!}{(n+2)!}$
		    Donc $P(Y=k)=\dfrac{2(n+2-k)}{(n+1)(n+2)}$.

		 **Autre méthode**:
		    $Y(\Omega)=\llbracket 1,n+1\rrbracket$.
		    On note $A_k$ l’événement " une boule ne portant pas le numéro 1 est tirée au rang $k$".
		    Soit $k\in \llbracket 1,n+1\rrbracket$.
		    On a : $(Y=k)=A_1\cap A_2\cap....\cap A_{k-1}\cap \overline{A_k}$.
		    Alors, d’après la formule des probabilités composées,
		    $P(Y=k)=P(A_1)P_{A_1}(A_2)...P_{A_1\cap A_2\cap...\cap A_{k-2}}(A_{k-1})P_{A_1\cap A_2\cap...\cap A_{k-1}}(\overline{A_k})$.
		    $P(Y=k)=\dfrac{n}{n+2}\times\dfrac{n-1}{(n+2)-1}\times\dfrac{n-2}{(n+2)-2}\times ...\times\dfrac{n-(k-2)}{(n+2)-(k-2)}\times \dfrac{2}{(n+2)-(k-1)}$
		    $P(Y=k)=\dfrac{n}{n+2}\times\dfrac{n-1}{n+1}\times\dfrac{n-2}{n}\times...\times\dfrac{n-k+2}{n-k+4}\times \dfrac{2}{n-k+3}$.
		    $P(Y=k)=2\:\dfrac{n!}{(n-k+1)!}\times \dfrac{(n-k+2)!}{(n+2)!}$.
		    $P(Y=k)=\dfrac{2(n-k+2)}{(n+2)(n+1)}$.

!!! exercice "EXERCICE 110 probabilités "
	=== "Enoncé"  


		Soit $(\Omega,\mathcal{A},P)$ un espace probabilisé.

		1.  Soit $X$ une variable aléatoire définie sur $(\Omega,\mathcal{A},P)$ et à valeurs dans $\mathbb{N}$.
		    On considère la série entière $\displaystyle\sum t^nP(X=n)$ de variable réelle $t$.
		    On note $R_X$ son rayon de convergence.

		    1.  Prouver que $R_X\geqslant 1$.
		        On pose $G_X(t)=\displaystyle\sum\limits_ {n=0}^{+\infty}t^nP(X=n)$ et on note $D_{G_X}$ l’ensemble de définition de $G_X$.
		        Justifier que $\left[-1,1 \right] \subset D_{G_X}$.
		        Pour tout réel $t$ fixé de $\left[-1,1 \right]$, exprimer $G_X(t)$ sous forme d’une espérance.

		    2.  Soit $k\in\mathbb{N}$. Exprimer, en justifiant la réponse, $P(X=k)$ en fonction de $G_X^{(k)}(0)$.

		2.  1.  On suppose que $X$ suit une loi de Poisson de paramètre $\lambda$.
		        Déterminer $D_{G_X}$ et, pour tout $\:t\in D_{G_X}$, calculer $G_X(t)$.

		    2.  Soit $X$ et $Y$ deux variables aléatoires définies sur un même espace probabilisé, indépendantes et suivant des lois de Poisson de paramètres respectifs $\lambda_1$ et $\lambda_2$.
		        Déterminer, en utilisant les questions précédentes, la loi de $X+Y$.


	=== "Corrigé"  


		1.  1.  $\forall \: n\in\mathbb{N}$, $\forall \:t\in\left[ -1,1\right]$, $|t^nP(X=n)|\leqslant P(X=n)$ et $\displaystyle\sum P(X=n)$ converge ( $\displaystyle\sum\limits_{n=0}^{+\infty}P(X=n)=1$).
		        Donc $\forall \:t\in\left[ -1,1\right]$, $\displaystyle\sum t^nP(X=n)$ converge absolument.
		        On en déduit $R_X\geqslant 1$ et aussi $\left[-1,1 \right] \subset D_{G_X}$. Au surplus, pour tout $t$ dans $\left[-1,1 \right]$, le théorème du transfert assure que la variable aléatoire $t^{X}$ admet une espérance et $E(t^X) = \displaystyle\sum_{n=0}^{+\infty} t^{n} P(X=n)= G_{X}(t)$. $G_X$ est la fonction génératrice de $X$.

		    2.  Soit $k\in\mathbb{N}$.
		        $G_X$ est la somme d’une série entière de rayon de convergence $R_X\geqslant 1$.
		        Donc, d’après le cours, $G_X$ est de classe $C^{\infty}$ sur $\left] -1,1\right[ \subset \left]-R_X,R_X \right[$.
		        De plus, $\forall\: t\in \left] -1,1\right[$, $G_X^{(k)}(t)=\displaystyle\sum\limits_{n=k}^{+\infty}\frac{n!}{(n-k)!}t^{n-k}P(X=n).$
		        En particulier, $G_X^{(k)}(0)=k! P(X=k)$ et donc $P(X=k)=\frac{G_X^{(k)}(0)}{k!}$.

		2.  1.  On suppose que $X$ suit une loi de Poisson de paramètre $\lambda$.
		        $\forall t\in \mathbb{R}$, $\displaystyle\sum t^nP(X=n)=\sum t^n \mathrm{e}^{-\lambda}\frac{\lambda^n}{n!}=\mathrm{e}^{-\lambda}\displaystyle\sum \frac{(\lambda t)^n}{n!}$ converge (série exponentielle) et donc $D_{G_X}=\mathbb{R}$.
		        De plus, $\forall t\in \mathbb{R}$, $G_X(t)=\mathrm{e}^{-\lambda}\displaystyle\sum \limits_{n=0}^{+\infty}\frac{(\lambda t)^n}{n!}
		        =\mathrm{e}^{-\lambda}\mathrm{e}^{\lambda t}=\mathrm{e}^{\lambda(t-1)}$.

		    2.  On suppose que $X$ et $Y$ sont indépendantes et suivent des lois de Poisson de paramètres respectifs $\lambda_1$ et $\lambda_2$.
		        $D_{G_X}=D_{G_Y}=\mathbb{R}$ et, si on pose $Z=X+Y$, alors $\left[ -1,1\right]\subset D_{G_Z}$.
		        Alors, $\forall \: t \in \left[ -1,1\right]$, $G_Z(t)=E(t^{X+Y})=E(t^Xt^Y)=E(t^X)E(t^Y)$ car $X$ et $Y$ sont indépendantes et donc, d’après le cours, $t^X$ et $t^Y$ sont indépendantes.
		        Donc, d’après 2.(a), $G_Z(t)=\mathrm{e}^{\lambda_1(t-1)}\mathrm{e}^{\lambda_2(t-1)}=\mathrm{e}^{(\lambda_1+\lambda_2)(t-1)}$.
		        On reconnaît la fonction génératrice d’une loi de Poisson de paramètre $\lambda_1+\lambda_2$.
		        Donc, d’après 1.(b), comme $Z$ a la même fonction génératrice qu’une loi de Poisson de paramètre $\lambda_1+\lambda_2$, alors $Z=X+Y$ suit une loi de Poisson de paramètre $\lambda_1+\lambda_2$.

!!! exercice "EXERCICE 111 probabilités "
	=== "Enoncé"  


		On admet, dans cet exercice, que: $\forall\:q\in \mathbb{N}$, $\displaystyle\sum\limits_{k\geqslant q}^{}\dbinom {k}{q}x^{k-q}$ converge et $\forall \:x\in \left] -1,1\right[$, $\displaystyle\sum\limits_{k=q}^{+\infty}\dbinom{k}{q}x^{k-q}=\dfrac{1}{(1-x)^{q+1}}.$
		Soit $p\in \left] 0,1\right[$.
		Soit $(\Omega,\mathcal{A},P)$ un espace probabilisé.
		Soit $X$ et $Y$ deux variables aléatoires définies sur $(\Omega,\mathcal{A},P)$ et à valeurs dans $\mathbb{N}$.
		On suppose que la loi de probabilité du couple $(X,Y)$ est donnée par:
		$\forall\:(k,n)\in\mathbb{N}^2$, $P((X=k)\cap (Y=n))=\left\lbrace \begin{array}{l}
		\dbinom{n}{k}\left( \dfrac{1}{2}\right)^np(1-p)^n\:\text{si}\:k\leqslant n\\
		0\:\text{sinon}
		\end{array}
		\right.$

		1.  Vérifier qu’il s’agit bien d’une loi de probabilité.

		2.  1.  Déterminer la loi de $Y$.

		    2.  Prouver que $1+Y$ suit une loi géométrique.

		    3.  Déterminer l’espérance de $Y$.

		3.  Déterminer la loi de $X$.


	=== "Corrigé"  


		1.  On remarque que $\forall\:(k,n)\in\mathbb{N}^2$, $P((X=k)\cap (Y=n))\geqslant 0$.
		    $(X,Y)(\Omega)=\left\lbrace (k,n)\in\mathbb{N}^2\:\text{tel que } k\leqslant n\right\rbrace$.
		    Posons $\forall\:(k,n)\in \mathbb{N}^2$, $p_{k,n}=P((X=k)\cap (Y=n))$.
		    $\forall n\in\mathbb{N}$, $\displaystyle\sum \limits_{k\geqslant0}^{}p_{k,n}$ converge (car un nombre fini de termes non nuls).
		    Et $\displaystyle\sum \limits_{k=0}^{+\infty}p_{k,n}
		     =\displaystyle\sum \limits_{k=0}^{n}\dbinom{n}{k}\left( \dfrac{1}{2}\right)^np(1-p)^n
		     =\left(\dfrac{1}{2} \right)^np(1-p)^n\displaystyle\sum \limits_{k=0}^{n}\dbinom{n}{k}
		     =\left(\dfrac{1}{2} \right)^np(1-p)^n2^n
		     =p(1-p)^n$.
		    De plus, $\displaystyle\sum \limits_{n\geqslant0}p(1-p)^n=p\displaystyle\displaystyle\sum \limits_{n\geqslant0}(1-p)^n$ converge (série géométrique convergente car $(1-p)\in \left] 0,1\right[$).
		    Et $\displaystyle\sum \limits_{n=0}^{+\infty}p(1-p)^n=p\displaystyle\sum \limits_{n=0}^{+\infty}(1-p)^n=p\frac{1}{1-(1-p)}=1$.
		    Donc on définit bien une loi de probabilité.

		2.  1.  $Y(\Omega)=\mathbb{N}$.
		        Soit $n\in\mathbb{N}$.
		        $P(Y=n)=\displaystyle\sum\limits_{k=0}^{+\infty}P((X=k)\cap (Y=n))$ (loi marginale)
		        Donc, d’après les calculs précédents, $P(Y=n)=\displaystyle\sum\limits_{k=0}^{n}\dbinom{n}{k}\left( \dfrac{1}{2}\right)^np(1-p)^n
		        =p(1-p)^n$ .
		        C’est-à-dire, $\forall n\in\mathbb{N}$, $P(Y=n)=p(1-p)^n$.

		    2.  Posons $Z=1+Y$.
		        $Z(\Omega)=\mathbb{N}^*$ et $\forall\:n\in\mathbb{N}^*$, $P(Z=n)=P(Y=n-1)=p(1-p)^{n-1}$.
		        Donc $Z$ suit une loi géométrique de paramètre $p$.

		    3.  D’après la question précédente, $E(Z)=\dfrac{1}{p}$.
		        Or $Y=Z-1$ donc $E(Y)=E(Z)-1$ et donc $E(Y)=\dfrac{1-p}{p}$.

		3.  $X(\Omega)=\mathbb{N}$.
		    Soit $k\in\mathbb{N}$. $P(X=k)=\displaystyle\sum\limits_{n=0}^{+\infty}P((X=k)\cap (Y=n))$ (loi marginale)
		    Donc $P(X=k)=\displaystyle\sum\limits_{n=k}^{+\infty}\dbinom{n}{k}\left( \dfrac{1}{2}\right)^np(1-p)^n
		    = p\left( \dfrac{1}{2}\right)^{k}(1-p)^k\displaystyle\sum\limits_{n=k}^{+\infty}\dbinom{n}{k}\left( \dfrac{1}{2}(1-p)\right)^{n-k} .$
		    Donc, d’après les résultats admis dans l’exercice, $P(X=k)= p\left( \dfrac{1}{2}\right)^{k}(1-p)^k\dfrac{1}{\left( 1-\dfrac{1}{2}(1-p)\right) ^{k+1}}$
		    C’est-à-dire $P(X=k)= p\left( \dfrac{1}{2}\right)^{k}(1-p)^k\dfrac{2^{k+1}}{(1+p)^{k+1}}$.
		    Donc, $\forall\:k\in\mathbb{N}$, $P(X=k)=\dfrac{2p}{1+p}\left( \dfrac{1-p}{1+p}\right) ^k$.

!!! exercice "EXERCICE 112 probabilités "
	=== "Enoncé"  


		Soit $n\in\mathbb{N}^*$ et $E$ un ensemble possédant $n$ éléments.
		On désigne par $\mathcal{P}(E)$ l’ensemble des parties de $E$.

		1.  Déterminer le nombre $a$ de couples $(A,B)\in \left(\mathcal{P}(E) \right)^2$ tels que $A\subset B$.

		2.  Déterminer le nombre $b$ de couples $(A,B)\in \left(\mathcal{P}(E) \right)^2$ tels que $A\cap B=\emptyset$.

		3.  Déterminer le nombre $c$ de triplets $(A,B,C)\in \left(\mathcal{P}(E) \right)^3$ tels que $A$, $B$ et $C$ soient deux à deux disjoints et vérifient $A\cup B\cup C=E$.


	=== "Corrigé"  


		1.  On note $F=\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset B\right\rbrace$.
		Soit $p\in\llbracket 1,n\rrbracket$. On pose $F_p=\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset B\:\text{et}\: \text{card}B=p\right\rbrace$.
		Pour une partie $B$ à $p$ éléments donnée, le nombre de parties $A$ de $E$ telles que $A\subset B$ est $\text{card}\,\mathcal{P}(B)=2^p$.
		De plus, on a $\binom{n}{p}$ possibilités pour choisir une partie $B$ de $E$ à $p$ éléments.
		On en déduit que : $\forall \: p\in\llbracket 0,n\rrbracket$, $\text{card}\,F_p=\binom{n}{p}2^p$.
		Or $F=\bigcup\limits_{p=0}^{n}F_p$ avec $F_0,F_1,...,F_n$ deux à deux disjoints.
		Donc $a=\text{card}\,F=\displaystyle\sum\limits_{p=0}^{n}\text{card}\,F_p=\displaystyle\sum\limits_{p=0}^{n}\binom{n}{p}2^p=3^n$, d’après le binôme de Newton.
		Conclusion: $a=3^n$.   
		**Autre méthode:**
		Le raisonnement suivant (corrigé non détaillé) permet également de répondre à la question 1.
		Notons encore $F=\left\lbrace (A,B)\in\left( \mathcal{P}(E)\right) ^2\:/\:A\subset B\right\rbrace$.
		À tout couple $(A,B)$ de $F$, on peut associer l’application $\varphi_{A,B}$ définie par:
		$\varphi_{A,B}:\begin{array}{lll}
		E&\longrightarrow & \left\lbrace 1,2,3\right\rbrace \\
		x&\longmapsto&
		\left\lbrace \begin{array}{ccc}
		1&\:\text{si}\:&x\in A \\
		2&\:\text{si}\:& x\notin A \:\text{et}\: x \in B \\
		3&\:\text{si} &x \notin B
		\end{array}\right.
		\end{array}$
		On note $\mathcal{A}
		\left( E,\left\lbrace 1,2,3\right\rbrace \right)$ l’ensemble des applications de $E$ dans $\left\lbrace 1,2,3\right\rbrace$.
		Alors l’application $\Theta:
		\begin{array}{lll}
		{F}&\longrightarrow &\mathcal{A}
		\left( E,\left\lbrace 1,2,3\right\rbrace \right) \\
		(A,B)&\longmapsto &\varphi_{A,B}
		\end{array}$ est bijective.
		Le résultat en découle.

		2.  $\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\cap B=\emptyset \right\rbrace
		    =\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset \overline{B}\right\rbrace$.

		    $\begin{array}{lll}
		    \text{Or}\:\:\text{card}\,\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset \overline{B} \right\rbrace
		    &=&\text{card}\, \left\lbrace (A,\overline{B})\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset \overline{B}\right\rbrace \\
		    &=&\text{card}\, \left\lbrace (A,C)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\subset C\right\rbrace\\
		    &=&a.
		    \end{array}$

		    Donc $b =a.$

		3.  Compter tous les triplets $(A,B,C)$ tels que $A$, $B$ et $C$ soient deux à deux disjoints et tels que $A\cup B\cup C=E$ revient à compter tous les couples $(A,B)$ tels que $A\cap B=\emptyset$ car, alors, $C$ est obligatoirement égal à $\overline{A\cup B}$.
		    En d’autres termes, $c=\text{card} \,\left\lbrace (A,B)\in \left(\mathcal{P}(E) \right)^2 \:/\:A\cap B=\emptyset\right\rbrace=b=3^n$.
