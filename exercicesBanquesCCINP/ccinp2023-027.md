!!! exercice "EXERCICE 27 analyse  "
	=== "Enoncé"  


		Pour tout $n\in \mathbb{N}^*$, on pose $f_{n}\left( x\right) =\frac{e^{-x}}{
		1+n^{2}x^{2}}$ et $u_{n}=\displaystyle\int_{0}^{1}f_{n}\left( x\right) \mathrm{d}x$.

		1.  Étudier la convergence simple de la suite de fonctions $\left( f_{n}\right)$ sur $[0,1]$.

		2.  Soit $a\in\left] 0,1 \right[$. La suite de fonctions $\left( f_{n}\right)$ converge-t-elle uniformément sur $\left[a,1 \right]$?

		3.  La suite de fonctions $\left( f_{n}\right)$ converge-t-elle uniformément sur $[0,1]$?

		4.  Trouver la limite de la suite $\left( u_{n}\right) _{n\in \mathbb{N}^*}.$


	=== "Corrigé"  


		1.  Soit $x\in \left[0,1 \right]$.
		    Si $x = 0$, $f_n (0) = 1$.
		    Si $x \in \left] {0,1} \right]$, pour $n$ au voisinage de $+\infty$, $f_n(x)\underset{+\infty}{\sim} \dfrac{\mathrm{e}^{-x}}{x^2}\dfrac{1}{n^2}$, donc $\lim\limits_{n\to +\infty}^{}f_n (x) = 0$.
		    On en déduit que la suite de fonctions $(f_n )$ converge simplement sur $\left[ {0,1} \right]$ vers la fonction $f$ définie par:
		    $f(x) = \left\{ {
		    \begin{array}{ll}
		     0 & {{\text{si }}x \in \left] {0,1} \right]}  \\
		     1 & {{\text{si }}x = 0}  \\
		    \end{array}
		    } \right.$

		2.  Soit $a\in\left] 0;1 \right[$.
		    $\forall\:n\in\mathbb{N}^*$, $\forall\:x\in \left[a,1 \right]$, $|f_n(x)-f(x)|=f_n(x)\leqslant \dfrac{\mathrm{e}^{-a}}{1+n^2a^2}$ (majoration indépendante de $x$).
		    Donc $\underset{t\in \left[a,1 \right]}{\sup }|f_n(t)-f(t)|\leqslant \dfrac{\mathrm{e}^{-a}}{1+n^2a^2}$.
		    Or $\lim\limits_{n\to +\infty}^{}\dfrac{\mathrm{e}^{-a}}{1+n^2a^2}=0$, donc $\lim\limits_{n\to +\infty}^{}\underset{t\in \left[a,1 \right]}{\sup }|f_n(t)-f(t)|=0$
		    On en déduit que $\left( f_{n}\right)$ converge uniformément vers $f$ sur $\left[a,1 \right]$.

		3.  Les fonctions $f_n$ étant continues sur $\left[0,1 \right]$ et la limite simple $f$ ne l’étant pas, on peut assurer qu’il n’y a pas convergence uniforme sur $\left[ {0,1} \right]$.

		4.  i\) Les fonctions $f_n$ sont continues par morceaux sur $\left[ {0,1} \right]$.
		    ii) $(f_n)$ converge simplement vers $f$ sur $\left[ {0,1} \right]$, continue par morceaux sur $\left[ {0,1} \right]$ .
		    iii) De plus, $\forall x \in \left[ {0,1} \right],\left| {f_n (x)} \right| \leqslant {\mathrm{e}}^{ - x}  \leqslant 1 = \varphi (x)$ avec $\varphi :\left[ {0,1} \right] \to \mathbb{R}^ +$ continue par morceaux et intégrable sur $\left[ {0,1} \right]$ .
		    D’après le théorème de convergence dominée, on peut donc affirmer que:
		    $\lim\limits_{n\to +\infty}^{}u_n  = \lim\limits_{n\to +\infty}^{}\displaystyle\int_0^1 {f_n (x)\,{\mathrm{d}}x} =\displaystyle\int_0^1 {f(x)\,{\mathrm{d}}x}  = 0$.

