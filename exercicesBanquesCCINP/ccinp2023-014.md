!!! exercice "EXERCICE 14 analyse  "
	=== "Enoncé"  


		1.  Soit $a$ et $b$ deux réels donnés avec $a<b$.
		    Soit $\left( f_{n}\right)$ une suite de fonctions continues sur $[a,b],$ à valeurs réelles.
		    Démontrer que si la suite $\left( f_{n}\right)$ converge uniformément sur $\left[ a,b\right]$ vers $f$, alors la suite $\left( \displaystyle\int_{a}^{b}f_{n}\left( x\right)\text{d}x\right)_{n\in \mathbb{N}}$ converge vers $\displaystyle\int_{a}^{b}f\left(x\right) \text{d}x$.

		2.  Justifier comment ce résultat peut être utilisé dans le cas des séries de fonctions.

		3.  Démontrer que $\displaystyle\int_{0}^{\frac{1}{2}}\left( \displaystyle\sum_{n=0}^{+\infty}x^{n}\right) \text{d}x=\displaystyle\sum\limits_{n=1}^{+\infty }\dfrac{1}{n2^{n}}~.$


	=== "Corrigé"  


		1.  Comme la suite $(f_n)$ converge uniformément sur $\left[a,b \right]$ vers $f$, et que, $\forall\:n\in\mathbb{N}$, $f_n$ est continue sur $\left[ a,b\right]$, alors $f$ est continue sur $\left[ a,b\right]$.
		    Ainsi, $\forall n\in\mathbb{N}$, $f_n-f$ est continue sur le segment $\left[ a,b\right]$.
		    On pose alors, $\forall n\in\mathbb{N}$, $\left\| {f_n  - f} \right\|_\infty   = \mathop {\sup }\limits_{x \in \left[ {a,b} \right]} \left| {f_n (x) - f(x)} \right|$.
		    On a $\left| {\displaystyle\int_a^b {f_n (x)\,{\mathrm{d}}x}  - \int_a^b {f(x)\,{\mathrm{d}}x} } \right| =\left|\displaystyle \int_a^b\left( f_n(x)-f(x)\right)\mathrm{d}x\right| \leqslant \displaystyle\int_a^b|f_n(x)-f(x)|\mathrm{d}x\leqslant (b - a)\left\| {f_n  - f} \right\|_\infty$.(\*)
		    Or $(f_n)$ converge uniformément vers $f$ sur $\left[ a,b\right]$, donc $\lim\limits_{n\to+\infty}^{}\left\| {f_n  - f} \right\|_\infty =0$.
		    Donc d’après (\*), $\lim\limits_{n\to+\infty}^{}\displaystyle\int_a^b f_n (x)\,{\mathrm{d}}x= \int_a^b {f(x)\,{\mathrm{d}}x}$.

		2.  On suppose que $\forall n\in\mathbb{N}$, $f_n$ est continue sur $\left[ a,b\right]$ et $\displaystyle\sum f_n$ converge uniformément sur $\left[ a,b\right]$.
		    On pose $S_n=\displaystyle\sum\limits_{k=0}^{n}f_k$.
		    $\displaystyle\sum f_n$ converge uniformément sur $\left[ a,b\right]$, donc converge simplement sur $\left[ a,b\right]$.
		    On pose alors, également, $\forall x\in \left[a,b \right]$, $S(x)=\displaystyle\sum\limits_{k=0}^{+\infty}f_k(x)$.
		    $\displaystyle\sum f_n$ converge uniformément sur $\left[ a,b\right]$ signifie que $(S_n)$ converge uniformément sur $\left[ a,b\right]$ vers $S$.
		    De plus, $\forall\:n\in\mathbb{N}$, $S_n$ est continue sur $\left[ a,b\right]$, car $S_n$ est une somme finie de fonctions continues.
		    On en déduit que $S$ est continue sur $\left[ a,b\right]$.
		    Et d’après 1., $\lim\limits_{n\to+\infty}^{}\displaystyle\int_a^b S_n (x)\,{\mathrm{d}}x= \displaystyle\int_a^b {S(x)\,{\mathrm{d}}x}$.
		    Or $\displaystyle\int_{a}^{b}S_n(x)\mathrm{d}x=\displaystyle\int_{a}^{b}\displaystyle\sum\limits_{k=0}^{n}f_k(x)\mathrm{d}x=\displaystyle\sum\limits_{k=0}^{n}\displaystyle\int_{a}^{b}f_k(x)\mathrm{dx}$ car il s’agit d’une somme finie.
		    Donc $\lim\limits_{n\to+\infty}^{}\displaystyle\sum\limits_{k=0}^{n}\displaystyle\int_a^b f_k (x)\,{\mathrm{d}}x= \displaystyle\int_a^b {S(x)\,{\mathrm{d}}x}$.
		    Ou encore $\lim\limits_{n\to+\infty}^{}\displaystyle\sum\limits_{k=0}^{n}\displaystyle\int_a^b f_k (x)\,{\mathrm{d}}x= \displaystyle\int_a^b\displaystyle\sum\limits_{k=0}^{+\infty}f_k(x) {\,{\mathrm{d}}x}$.
		    Ce qui signifie que $\displaystyle\sum\limits_{}^{}\displaystyle\int_a^b f_k (x)\,{\mathrm{d}}x$ converge et $\displaystyle\sum\limits_{k=0}^{+\infty}\displaystyle\int_a^b f_k (x)\,{\mathrm{d}}x= \displaystyle\int_a^b\displaystyle\sum\limits_{k=0}^{+\infty}f_k(x) {\,{\mathrm{d}}x}$.  

		    **Bilan**: La convergence uniforme de la série de fonctions $\displaystyle\sum f_n$ où les $f_n$ sont continues sur $\left[ {a,b} \right]$ permet d’ intégrer terme à terme, c’est-à-dire: $\displaystyle\int_{a}^{b} {\displaystyle\sum\limits_{n = 0}^{ + \infty } {f_n (x)\,{\mathrm{d}}x}  = \displaystyle\sum\limits_{n = 0}^{ + \infty } {\displaystyle\int_a^b {f_n (x)\,{\mathrm{d}}x} } }$.

		3.  La série entière $\displaystyle\sum {x^n }$ est de rayon de convergence $R = 1$ donc cette série de fonctions converge normalement et donc uniformément sur le compact $\left[ {0,\dfrac{1}{2}} \right] \subset \left] { - 1,1} \right[$.
		    De plus, $\forall\: n\in\mathbb{N}$, $x\longmapsto x^n$ est continue sur $\left[0,\dfrac{1}{2} \right]$.
		    On en déduit alors, en utilisant 2., que: $\displaystyle\int_0^{\frac{1}{2}} \left( {\displaystyle\sum\limits_{n = 0}^{ + \infty } {x^n } } \right) \,{\mathrm{d}}x =\displaystyle\sum\limits_{n=0}^{+\infty}\displaystyle\int_{0}^{\frac{1}{2}} x^n\mathrm{d}x=\displaystyle\sum\limits_{n = 0}^{ + \infty } {\dfrac{1}{{n + 1}}\dfrac{1}{{2^{n + 1} }}}
		    =\displaystyle\sum\limits_{n = 1}^{ + \infty } {\dfrac{1}{{n }}\dfrac{1}{{2^{n } }}}.$

