fichier = open("banque.md",'r')

fichier = fichier.read()

exercices = fichier.split("\n### ")
i=0

for exercice in exercices :
	decoupe = exercice.split("####")
	titre = decoupe[0]
	enonce = decoupe[1].replace("\n","\n\t\t")
	corrige = decoupe[2].replace("\n","\n\t\t")
	titre = titre[:titre.find("[")]
	print("!!! exercice \""+titre+"\"")
	print("\t=== \"Enoncé\"  ")
	print(enonce[enonce.find("\n"):])
	print("\t=== \"Corrigé\"  ")
	print(corrige[corrige.find("\n"):])
	

