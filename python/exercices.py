import re
import sys

fichier = sys.argv[1]



CHEMIN_EXERCICES = "../exercices/"


def exercice_to_md(src_exo) :
	debut_enonce = src_exo.find("%%%debut:enonce%%%")+19
	fin_enonce  =src_exo.find("%%%fin:enonce%%%")
	debut_corrige = src_exo.find("%%%debut:corrige%%%")+20
	fin_corrige = src_exo.find("%%%fin:corrige%%%")
	enonce = src_exo[debut_enonce:fin_enonce]
	corrige = src_exo[debut_corrige:fin_corrige]
	return enonce,corrige

def to_md(fichier) :
	src = open(fichier,"r")
	src_md = ""
	liste_exercices = src.read().split("%%%debut:exercice")[1:]
	i=1
	for exercice in liste_exercices :
		enonce,corrige = exercice_to_md(exercice)
		src_md += "!!! exercice \"Exercice "+str(i)+"\"\n\t=== \"Enoncé\"\n\t\t"
		src_md += enonce.replace("\n","\n\t\t")
		src_md += "\n\t\n\t=== \"Corrigé\"\n\t\t"
		src_md += corrige.replace("\n","\n\t\t")
		src_md += "\n\n\n"
		i+=1
	return src_md

def main() :
	print(to_md(CHEMIN_EXERCICES+fichier))

if __name__=="__main__" :
	main()
