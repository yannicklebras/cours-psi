# Construction d'un cours de PSI

Ce dépôt me sert à construire mon cours de PSI* en mathématiques.
En dehors du contenu, j'ai travaillé à modifier quelques admonitions pour numéroter notamment, mettre les preuves en formes... voir le fichier docs/stylesheets/extra.css

Le cours est en construction, les corrections d'exercices ne sont certainement pas parfaites.
